#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nhistogram of B values\n')

#------------------------------------------------------
#Init
#----
Bm_th = 1.00e-2

run,tr,_=wl.Init(sys.argv[1])
ind1,ind2,ind_step = tr
run_name = run.meta['run_name']
nx,ny,_ = run.meta['nnn']
w_ele = run.meta['w_ele']
times = run.meta['time']*run.meta['tmult']
code_name = run.meta['code_name']
#
if code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
elif code_name == 'iPIC':
    run_label = 'iPIC'
    qom_e = run.meta['msQOM'][0]
else:
    print('ERROR: unknown code_name %s'%(code_name))
    sys.exit(-1)
#
opath = run.meta['opath']

out_dir = 'B_hist'
ml.create_path(opath+'/'+out_dir)
#
#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #|B|
    _,B = run.get_EB(ind)
    Bm = np.sqrt( (B[0,...,0]*B[0,...,0] + 
                   B[1,...,0]*B[1,...,0] + 
                   B[2,...,0]*B[2,...,0]) )
    del B
#
    # count undet th.
    num_under_th = np.sum(Bm < Bm_th)
    vol = nx*ny
    perc = float(num_under_th)/float(vol)*100.
#
    # plot hist
    fig,ax = plt.subplots(1,1,figsize=(12,12))
    h,b_e = np.histogram(Bm.flatten(),bins=512,density=False)
    b = b_e[1:] - (b_e[1]-b_e[0])*.5
    ax.plot(b,h)
    ax.set_xlabel('$|B|$')
    ax.set_ylabel('Counts')
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.axvline(x=Bm_th,c='k',ls='--')
    ax.set_title('%s - $t = %f \Omega_p^{-1}$ - num. under th.: %d over %d (%f perc.)'%(run_label,times[ind],num_under_th,vol,perc))
    fig.savefig(opath+'/'+out_dir+'/B_norm_hist_%s_%d.png'%(run_name,ind))
    plt.close()
#
#---> loop over time <---
    print "\r",
    print "t = ",times[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
