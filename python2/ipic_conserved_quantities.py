"""
Plotting energies from iPIC ConservedQuantities.txt

F. Finelli - November 9 2021
"""
# coding: utf-8

import sys
from os.path import join as os_join
from os.path import sep as os_sep
from os import mkdir as os_mkdir
from os.path import isdir as os_isdir

from pandas import read_csv as pd_read_csv
from numpy import zeros as np_zeros
from numpy import where as np_where
from numpy import sqrt as np_sqrt
from numpy import amin as np_min
from numpy import amax as np_max
from numpy import absolute as np_abs
from numpy import argmin as np_argmin
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

sys.path.insert(0, '/work2/finelli/phd_scripts/python2/my_branch')
from iPIC_loader import *

font = 15
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

# Init
wpath = '/work2/finelli/DoubleHarris'
opath = '/work2/finelli/Outputs/ipic_conserved_quantities'
if not os_isdir(opath):
    os_mkdir(opath)
tmax = 400.
#run_dir_list = sys.argv[1:]
run_dir_list = [
                #'run2/data3',
                'run3/data0',
                'run5/data0',
                'run6/data0',
                'run7/data0',
                'run8',
                #'run9',
                'run10',
                'run11/data0'
               ]
df_dict = {}
run_name_list = []

# - run iteration
for run_dir in run_dir_list:
    ipath = os_join(wpath, run_dir)
    run = from_iPIC(ipath)
    run.get_meta()
    file_path = os_join(ipath, 'ConservedQuantities.txt')
    #run_name = ''.join(run_dir.split(os_sep))
    run_name = run_dir.split(os_sep)[0]
    run_name_list.append(run_name)

    # Read file
    df = pd_read_csv(file_path, sep='\t', header=None, index_col=False)
    columns = ['itime', 'Etot', 'momentum', 'EE', 'EM', 'Ekin']
    for s in run.meta['stag']:
        columns.append('Ekin%s'%(s, ))    
    for s in run.meta['stag']:
        columns.append('Ebulk%s'%(s, ))
    del df[1]
    df.columns = columns

    # Remove multiple lines
    repeated = np_zeros(df['itime'].shape, dtype=bool)
    last = -1
    for i, it in enumerate(df['itime']):
        if it > last:
            last = it
            continue
        repeated[i] = True
    df.drop(axis='index', labels=np_where(repeated)[0] ,inplace=True)
    df.reset_index(drop=True, inplace=True)

    # Compute quantities
    # - time
    df['time'] = df['itime']*run.meta['dt']*run.meta['Bx0']
    
    # - electrons and protons kinetic energies
    df['Ekine'] = np_zeros((len(df), ), dtype=float)
    df['Ekinp'] = np_zeros((len(df), ), dtype=float)
    for QOM, s in zip(run.meta['sQOM'], run.meta['stag']):
        if QOM < 0.:
            df['Ekine'] += df['Ekin%s'%(s, )]
        else:
            df['Ekinp'] += df['Ekin%s'%(s, )]

    # - electrons and protons bulk energies
    Ne = 0
    Np = 0
    df['Ebulke'] = np_zeros((len(df), ), dtype=float)
    df['Ebulkp'] = np_zeros((len(df), ), dtype=float)
    for QOM, s, ppp in zip(run.meta['sQOM'], run.meta['stag'], run.meta['sppp']):
        if QOM < 0.:
            Ne += ppp
            df['Ebulke'] += np_sqrt(ppp*df['Ebulk%s'%(s, )])
        else:
            Np += ppp
            df['Ebulkp'] += np_sqrt(ppp*df['Ebulk%s'%(s, )])
    df['Ebulke'] = df['Ebulke']**2/float(Ne)
    df['Ebulkp'] = df['Ebulkp']**2/float(Np)

    # - electrons and protons thermal energies
    df['Ethere'] = df['Ekine'] - df['Ebulke']
    df['Etherp'] = df['Ekinp'] - df['Ebulkp']

    # Plots
    labels = {'Etot':'$E_{\mathrm{tot}}$',
              'EE':'$E_{\mathrm{E.f.}}$',
              'EM':'$E_{\mathrm{M.f.}}$',
              'Ekin':'$E_{\mathrm{kin}}$',
              'Ebulke':'$E_{\mathrm{blk,e}}$',
              'Ebulkp':'$E_{\mathrm{blk,p}}$',
              'Ethere':'$E_{\mathrm{th,e}}$',
              'Etherp':'$E_{\mathrm{th,p}}$'}
    plt.close('all')

    # - totals, fields, plasma
    plt.axhline(y=0., ls=':', c='k')
    for k in ['Etot', 'EE', 'EM', 'Ekin']:
        plt.plot(df['time'], (df[k]-df[k][0])/df['Etot'][0], label=labels[k])
    plt.title('%s - Energy variations: plasma and fields'%(run_name, ))
    plt.xlabel('t [$\Omega_{\mathrm{p}}^{-1}$]')
    plt.ylabel('$[E-E(t=0)]/E_{\mathrm{tot}}(t=0)$')
    plt.legend()
    plt.tight_layout()
    plt.savefig(os_join(opath, '%s_enevar_plasmafields.png'%(run_name)))
    plt.close()

    # - species bulk and thermal
    colors = []
    plt.axhline(y=0., ls=':', c='k')
    for k in ['Ebulke', 'Ebulkp']:
        l, = plt.plot(df['time'], (df[k]-df[k][0])/df['Etot'][0], label=labels[k])
        colors.append(l.get_color())
    for i, k in enumerate(['Ethere', 'Etherp']):
        plt.plot(df['time'], (df[k]-df[k][0])/df['Etot'][0], c=colors[i], ls='--', label=labels[k])
    plt.title('%s - Energy variations: species'%(run_name, ))
    plt.xlabel('t [$\Omega_{\mathrm{p}}^{-1}$]')
    plt.ylabel('$[E-E(t=0)]/E_{\mathrm{tot}}(t=0)$')
    plt.legend()
    plt.tight_layout()
    plt.savefig(os_join(opath, '%s_enevar_species.png'%(run_name)))

    # - closing
    plt.close()

    # Save df
    df_dict[run_name] = df[['time', 'Etot', 'Ethere', 'Etherp']].copy()

# Comparison plots
#colors = plt.cm.viridis(np.linspace(0, 1, len(run_name_list)))

# - total energy
plt.axhline(y=0., ls=':', c='k')
maxval = 0.
minval = 0.
for i, run_name in enumerate(run_name_list):
    tmp_values = (df_dict[run_name]['Etot']-df_dict[run_name]['Etot'][0])/df_dict[run_name]['Etot'][0]
    plt.plot(df_dict[run_name]['time'],
             tmp_values,
             #c = colors[i],
             label=run_name)
    itmax = np_argmin(np_abs(df_dict[run_name]['time'].values-tmax))
    maxval = max(maxval, np_max(tmp_values[:itmax]))
    minval = min(minval, np_min(tmp_values[:itmax]))
plt.title('Runs comparison - Total energy')
plt.xlabel('t [$\Omega_{\mathrm{p}}^{-1}$]')
plt.ylabel('$[E_{\mathrm{tot}}-E_{\mathrm{tot}}(t=0)]/E_{\mathrm{tot}}(t=0)$')
plt.xlim(0., tmax)
plt.ylim(minval, maxval)
plt.legend()
plt.tight_layout()
plt.savefig(os_join(opath, 'runcomp_enetot.png'))
plt.close()

# - thermal energy
tmax = 200.
l_list = []
plt.axhline(y=0., ls=':', c='k')
maxval = 0.
minval = 0.
for i, run_name in enumerate(run_name_list):
    tmp_values = (df_dict[run_name]['Etherp']-df_dict[run_name]['Etherp'][0])/df_dict[run_name]['Etot'][0]
    l, = plt.plot(df_dict[run_name]['time'],
                  tmp_values)#, c = colors[i])
    l_list.append(l)
    itmax = np_argmin(np_abs(df_dict[run_name]['time']-tmax))
    maxval = max(maxval, np_max(tmp_values[:itmax]))
    minval = min(minval, np_min(tmp_values[:itmax]))
for i, run_name in enumerate(run_name_list):
    tmp_values = (df_dict[run_name]['Ethere']-df_dict[run_name]['Ethere'][0])/df_dict[run_name]['Etot'][0]
    plt.plot(df_dict[run_name]['time'],
             tmp_values,
             c=l_list[i].get_color(), ls='--')
    itmax = np_argmin(np_abs(df_dict[run_name]['time'].values-tmax))
    maxval = max(maxval, np_max(tmp_values[:itmax]))
    minval = min(minval, np_min(tmp_values[:itmax]))
plt.title('Runs comparison - Thermal energy')
plt.xlabel('t [$\Omega_{\mathrm{p}}^{-1}$]')
plt.ylabel('$[E_{\mathrm{th}}-E_{\mathrm{th}}(t=0)]/E_{\mathrm{tot}}(t=0)$')
plt.xlim(0., tmax)
plt.ylim(minval, maxval)
l1, = plt.plot([], ls='-', c='k')
l2, = plt.plot([], ls='--', c='k')
legend_save = plt.legend([l1, l2], ['prot.', 'ele.'], ncol=2, loc=3)#1)
plt.legend(l_list, run_name_list, loc=2)
plt.gca().add_artist(legend_save)
plt.tight_layout()
plt.savefig(os_join(opath, 'runcomp_therm.png'))
plt.close()
