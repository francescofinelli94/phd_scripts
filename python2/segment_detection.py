# coding: utf-8
import numpy as np

a = np.random.random(20)

b = np.zeros((a.size+2, ), dtype=int)
b[1:-1] = (a > .5).astype(int)
c = np.diff(b)
segs = np.array([np.where(c==1)[0],np.where(c==-1)[0]]).T

for s0,s1 in segs:
    print a[s0:s1]
    
print a
