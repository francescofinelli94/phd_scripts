#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import pandas as pd
import glob
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

#------------------------------------------------------
#Intent
#------
print('\nWill plot already computed enropies\n')

#------------------------------------------------------
#Init
#----
font = 20#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

opath = '/work2/finelli/Outputs/entropy_v4'

cls = {'fp':'orangered','fe':'dodgerblue','gp':'coral','ge':'mediumspringgreen'}

#------------------------------------------------------
#read
#----
all_file_ext = ['csv','CSV','tsv','TSV','txt','TXT','dat','DAT']
df_files = []
for ext in all_file_ext:
    df_files += glob.glob(opath+'/*/'+'entr_*.'+ext)

print('\nfiles:')
for n,f in zip(range(len(df_files)),df_files):
    print('%d -> %s in %s'%(n,f.split('/')[-1],f.split('/')[-2]))

n = int(input('Choose a file (insert the corresponding number): '))
df_file = df_files[n]

file_ext = df_file.split('.')[-1]
if (file_ext == 'csv') or (file_ext == 'CSV'):
    sep = ','
elif (file_ext == 'tsv') or (file_ext == 'TSV'):
    sep = '\t'
elif (file_ext == 'txt') or (file_ext == 'TXT'):
    sep = '\t'
elif (file_ext == 'dat') or (file_ext == 'DAT'):
    sep = '\t'
else:
    sep = '\t'

df = pd.read_csv(df_file,header=0,sep=sep)

tmp = df_file.split('/')[-1].split('_')[1]
if tmp == 'HVM':
    run_label = 'HVM'
    code_name = 'HVM'
elif tmp == 'LF':
    run_label = 'HVLF'
    code_name = 'HVM'
elif tmp == 'DH':
    run_label = 'iPIC'
    code_name = 'iPIC'
else:
    run_label = 'unknown'
    code_name = 'unknown'
    print('\nWARNING: code name is unknown')
    #sys.exit()

if 'entr_ge' not in df.columns:
    df['entr_ge'] = np.zeros(len(df['entr_gp'].values))
    df['adv_ge']  = np.zeros(len(df['entr_gp'].values))
    df['nPiD_ge'] = np.zeros(len(df['entr_gp'].values))
    df['vhf_ge']  = np.zeros(len(df['entr_gp'].values))
    df['ng_ge']   = np.zeros(len(df['entr_gp'].values))

t = df['time'].values
if code_name == 'iPIC':
    # iPIC time unit is 1/omega_{P,p}, while HVL one is 1/Omega_P
    # For the present simulations, we have:
    #        omega_{P,p}/Omega_P = 100
    # However, in the script generating the dataframe df the time
    # is always converted into HVM unbits.
    # Here, to properly time-derive we switch back to iPIC units
    # (since the rest of the entropy is in iPIC units)
    t *= 100.

dt = t[1] - t[0]

df['rate_fp'] = savgol_filter(df['entr_fp'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='interp')
df['rate_gp'] = savgol_filter(df['entr_gp'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='interp')
df['rate_fe'] = savgol_filter(df['entr_fe'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='interp')
df['rate_ge'] = savgol_filter(df['entr_ge'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='interp')

df['diff_fp'] = df['rate_fp'] - (df['adv_fp'] + df['gPiD_fp'] + df['nPiD_fp'] + df['vhf_fp'])
df['diff_gp'] = df['rate_gp'] - (df['adv_gp'] + df['nPiD_gp'] + df['vhf_gp']  + df['ng_gp'] )
df['diff_fe'] = df['rate_fe'] - (df['adv_fe'] + df['gPiD_fe'] + df['nPiD_fe'] + df['vhf_fe'])
df['diff_ge'] = df['rate_ge'] - (df['adv_ge'] + df['nPiD_ge'] + df['vhf_ge']  + df['ng_ge'] )

if code_name == 'iPIC':
    # Now we switch back the time in HVM units
    t /= 100.
    # The following two lines are now repliced by invOmegap, just for clarity
    #df.loc[:,[x for x in df.columns 
    #          if x not in ['time','entr_fp','entr_gp','entr_fe','entr_ge']]] *= 100.

#------------------------------------------------------
#plot
#----
plt.close('all')

if run_label == 'HVM':
    invS0 = {'fp':1./abs(df['entr_fp'][0]),
             'gp':1./abs(df['entr_gp'][0]),
             'fe':1./abs(df['entr_fe'][0]),
             'ge':1.                      }
else:
    invS0 = {'fp':1./abs(df['entr_fp'][0]),
             'gp':1./abs(df['entr_gp'][0]),
             'fe':1./abs(df['entr_fe'][0]),
             'ge':1./abs(df['entr_ge'][0])}
if code_name == 'iPIC':
    # Once again, the time-derived entropies and their contributes are in [S][t^-1] units
    # The [S] part is normalized with |S(t=0)|, while the [t^-1] is normalized with Omega_p
    # In the HVM case Omega_p = 1, but in the iPIC case Omega_p = 1/100 omega_{P,p}
    invOmegap = 100.
else:
    invOmegap = 1.

nrow = 2
ncol = 3
fig,ax = plt.subplots(nrow,ncol,figsize=(6*ncol,5*nrow))

# -> plot 0 0
#    Entropies: ( S - S(t=0) ) / |S(t=0)|
ax[0,0].plot(t,(df['entr_fp']-df['entr_fp'][0])*invS0['fp'],label='i.p.',color=cls['fp'])
ax[0,0].plot(t,(df['entr_gp']-df['entr_gp'][0])*invS0['gp'],label='a.p.',color=cls['gp'])
ax[0,0].plot(t,(df['entr_fe']-df['entr_fe'][0])*invS0['fe'],label='i.e.',color=cls['fe'])
if run_label == 'HVM':
    ax[0,0].plot([],label='a.e.',color=cls['ge'])
else:
    ax[0,0].plot(t,(df['entr_ge']-df['entr_ge'][0])*invS0['ge'],label='a.e.',color=cls['ge'])
ax[0,0].legend(ncol=2)
ax[0,0].set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax[0,0].set_ylabel('$\Delta S/S_0$')
ax[0,0].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

# -> plot 0 1
#    Entropy variations: partial_t S / |S(t=0)| / Omega_p
ax[0,1].plot(t,df['rate_fp']*(invS0['fp']*invOmegap),label='i.p.',color=cls['fp'])
ax[0,1].plot(t,df['rate_gp']*(invS0['gp']*invOmegap),label='a.p.',color=cls['gp'])
ax[0,1].plot(t,df['rate_fe']*(invS0['fe']*invOmegap),label='i.e.',color=cls['fe'])
if run_label == 'HVM':
    ax[0,1].plot([],label='a.e.',color=cls['ge'])
else:
    ax[0,1].plot(t,df['rate_ge']*(invS0['ge']*invOmegap),label='a.e.',color=cls['ge'])
ax[0,1].legend(ncol=2)
ax[0,1].set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax[0,1].set_ylabel('$\partial_tS\;[S_0\Omega_{\mathrm{p}}]$')
ax[0,1].set_title(run_label)
ax[0,1].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

# -> plot 0 2
#    Advection
ax[0,2].plot(t,df['adv_fp']*(invS0['fp']*invOmegap),label='i.p.',color=cls['fp'])
ax[0,2].plot(t,df['adv_gp']*(invS0['gp']*invOmegap),label='a.p.',color=cls['gp'])
ax[0,2].plot(t,df['adv_fe']*(invS0['fe']*invOmegap),label='i.e.',color=cls['fe'])
if run_label == 'HVM':
    ax[0,2].plot([],label='a.e.',color=cls['ge'])
else:
    ax[0,2].plot(t,df['adv_ge']*(invS0['ge']*invOmegap),label='a.e.',color=cls['ge'])
ax[0,2].legend(ncol=2)
ax[0,2].set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax[0,2].set_ylabel('$-\\boldsymbol{u}\cdot\\boldsymbol{\\nabla}S\;[S_0\Omega_{\mathrm{p}}]$')
ax[0,2].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

# -> plot 1 0
#    Vector heat flux
ax[1,0].plot(t,df['vhf_fp']*(invS0['fp']*invOmegap),label='i.p.',color=cls['fp'])
ax[1,0].plot(t,df['vhf_gp']*(invS0['gp']*invOmegap),label='a.p.',color=cls['gp'])
ax[1,0].plot(t,df['vhf_fe']*(invS0['fe']*invOmegap),label='i.e.',color=cls['fe'])
if run_label == 'HVM':
    ax[1,0].plot([],label='a.e.',color=cls['ge'])
else:
    ax[1,0].plot(t,df['vhf_ge']*(invS0['ge']*invOmegap),label='a.e.',color=cls['ge'])
ax[1,0].legend(ncol=2)
ax[1,0].set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax[1,0].set_ylabel('$-\\boldsymbol{\\nabla}\cdot\\boldsymbol{q}/p_{\perp}\;[S_0\Omega_{\mathrm{p}}]$')
ax[1,0].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

# -> plot 1 1
#    Pi-D
ax[1,1].plot(t,df['gPiD_fp']*(invS0['fp']*invOmegap),color=cls['fp'])
ax[1,1].plot(t,df['nPiD_fp']*(invS0['fp']*invOmegap),color=cls['fp'],linestyle='--')
ax[1,1].plot(t,df['nPiD_gp']*(invS0['gp']*invOmegap),color=cls['gp'],linestyle='--')
ax[1,1].plot(t,df['gPiD_fe']*(invS0['fe']*invOmegap),color=cls['fe'])
if code_name == 'iPIC':
    ax[1,1].plot(t,df['nPiD_fe']*(invS0['fe']*invOmegap),color=cls['fe'],linestyle='--')
    ax[1,1].plot(t,df['nPiD_ge']*(invS0['ge']*invOmegap),color=cls['ge'],linestyle='--')
h1, = ax[1,1].plot([],label='i.p.',color=cls['fp'])
h2, = ax[1,1].plot([],label='a.p.',color=cls['gp'])
h3, = ax[1,1].plot([],label='gyr.',color='k')
h4, = ax[1,1].plot([],label='i.e.',color=cls['fe'])
h5, = ax[1,1].plot([],label='a.e.',color=cls['ge'])
h6, = ax[1,1].plot([],label='n.g.',color='k',linestyle='--')
ax[1,1].legend([h1,h2,h3,h4,h5,h6],['i.p.','a.p.','gyr.','i.e.','a.e.','n.g.'],ncol=2)
ax[1,1].set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax[1,1].set_ylabel('$-\overline{\overline{\\boldsymbol{\Pi}}}:\overline{\overline{\\boldsymbol{D}}}/p_{\perp}\;[S_0\Omega_{\mathrm{p}}]$')
ax[1,1].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

# -> plot 1 2
#    missing part: non-gyrotropy and heat flux tensor
ax[1,2].plot(t,df['diff_fp']*(invS0['fp']*invOmegap),color=cls['fp'])
ax[1,2].plot(t,df['diff_gp']*(invS0['gp']*invOmegap),color=cls['gp'])
ax[1,2].plot(t,df['diff_fe']*(invS0['fe']*invOmegap),color=cls['fe'])
if run_label != 'HVM':
    ax[1,2].plot(t,df['diff_ge']*(invS0['ge']*invOmegap),color=cls['ge'])
ax[1,2].plot(t,df['ng_gp']*(invS0['gp']*invOmegap),color=cls['gp'],linestyle='--')
if code_name == 'iPIC':
    ax[1,2].plot(t,df['ng_ge']*(invS0['ge']*invOmegap),color=cls['ge'],linestyle='--')
h1, = ax[1,1].plot([],label='i.p.',color=cls['fp'])
h2, = ax[1,1].plot([],label='a.p.',color=cls['gp'])
h3, = ax[1,1].plot([],label='rem.',color='k')
h4, = ax[1,1].plot([],label='i.e.',color=cls['fe'])
h5, = ax[1,1].plot([],label='a.e.',color=cls['ge'])
h6, = ax[1,1].plot([],label='n.g.',color='k',linestyle='--')
ax[1,2].legend([h1,h2,h3,h4,h5,h6],['i.p.','a.p.','rem.','i.e.','a.e.','n.g.'],ncol=2)
ax[1,2].set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax[1,2].set_ylabel('Remaining contributions $[S_0\Omega_{\mathrm{p}}]$')
ax[1,2].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

fig.tight_layout()
plt.savefig(df_file.split('.')[0]+'_norm.png')
plt.close()
