import sys
import numpy as np
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#metadata
ipath = '/work1/finelli/LF_3d_DH'
opath = '/work1/finelli/LF_3d_DH/vtk'

#init
ml.create_path(opath)
run = from_HVM(ipath)
run.get_meta()
calc = fibo('calc')
calc.meta=run.meta

#time choose
print('time')
print(run.meta['time'])
t = ml.secure_input('Insert time: ',1.0,True)
ind = ml.index_of_closest(t,run.meta['time'])
t = run.meta['time'][ind]
print('t = '+str(t)+' - ind = '+str(ind))

#field read and compute
run.get_EB(ind,fibo_obj=calc)
run.get_Ion(ind,fibo_obj=calc)
cx,cy,cz = calc.calc_curl(calc.data['B_x_%08.3f'%t],calc.data['B_y_%08.3f'%t],calc.data['B_z_%08.3f'%t])
J = np.array([cx,cy,cz])
del cx,cy,cz
calc.data['J_x_%08.3f'%t] = J[0]
calc.data['J_y_%08.3f'%t] = J[1]
calc.data['J_z_%08.3f'%t] = J[2]
del J

#vtk write

calc.print_vtk_scal(opath,'n_%08.3f'%t,'n_%08.3f'%t)
calc.print_vtk_vect(opath,'E_%08.3f'%t,'E_x_%08.3f'%t,'E_y_%08.3f'%t,'E_z_%08.3f'%t)
calc.print_vtk_vect(opath,'B_%08.3f'%t,'B_x_%08.3f'%t,'B_y_%08.3f'%t,'B_z_%08.3f'%t)
calc.print_vtk_vect(opath,'J_%08.3f'%t,'J_x_%08.3f'%t,'J_y_%08.3f'%t,'J_z_%08.3f'%t)
calc.print_vtk_vect(opath,'ui_%08.3f'%t,'ui_x_%08.3f'%t,'ui_y_%08.3f'%t,'ui_z_%08.3f'%t)
