#--------------------------------
#decide if plot or not
#---------------------
plt_show_flg = False
plt_save_flg = True

#--------------------------------
#imports
#-------
import os
import sys
import glob
import time
import multiprocessing as mp
import numpy as np
import matplotlib as mpl
if not plt_show_flg:
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec
import seaborn as sns

#--------------------------------
#LateX
#-----
font =17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#--------------------------------
#Inputs
#------
user = 'finelli'

out_dir = '/home/finelli/Downloads/PVI-scalograms_v4'

gs_flg = 1 # 0 -> all plots generated ; 1 -> no plot generated
do_gs = False # True -> the gridsearch is run ; False -> plot produced assuming the gridsearch done

ncpu = 6 #os.cpu_count()

in_rm = ~False # remove the inputs (for the LOOPER) files
log_rm = ~False # remove the logs (LOOPER outputs) files
err_rm = False # remove the errors (from this script) file

#--------------------------------
#GridSearch parameters
#---------------------
i1 = [] # {0,1}
i2 = [] # i1==0 => [0.,1.]; i1==1 => [0.,+inf)
i3 = [] # i1==0 => [0.,1.]; i1==1 => [0.,+inf)

#oldsi = [2.,3.,4.,5.,6.,7.] # J
#oldsj = [2.,4.,6.,8.,10.,12.,14.,16.,18.,20.,22.] # PVI
#oldsi = [3.,3.5,4.,4.5,5.,5.5,6.,6.6,7.] # J
#oldsj = [4.,5.,6.,7.,8.,9.,10.,11.,12.] # PVI

si = [float(x)/20.*(8.-3.)+3. for x in range(21)] # J
sj = [float(x)/32.*(12.-4.)+4. for x in range(33)] # PVI

#--------------------------------
#Functions
#---------
def dir_name(n,a,b):
    if n == 0:
        return 'J%dP%d_NOCWT'%(int(a*100),int(b*100))
    elif n == 1:
        return 'J%dsP%ds_NOCWT'%(int(a*100),int(b*100))
    else:
        print('Unknown n=%d value!'%n)
        return None

def F1_score(prec,rec):
    a = 2.*prec*rec
    b = prec+rec
    return np.divide(a,b,out=np.zeros_like(a),where=(b!=0.))

def Precision(TP,FP):
    a = TP.astype(np.float64)
    b = (TP+FP).astype(np.float64)
    return np.divide(a,b,out=np.zeros_like(a),where=(b!=0))

def Recall(TP,FN):
    a = TP.astype(np.float64)
    b = (TP+FN).astype(np.float64)
    return np.divide(a,b,out=np.zeros_like(a),where=(b!=0))

def U_model(n,i1,i2,i3,cdict):
    """
    MODEL:
    TP -> rJ + rP + rJP
    FP -> J + P + JP
    FN -> r

    INPUTS:
    n -> dict
    i1, i2, i3 -> list

    OUTPUTS:
    prec, rec, F1 -> np.ndarray
    """
    TP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FN = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    for ii1,ii2,ii3 in zip(i1,i2,i3):
        TP[cdict[dir_name(ii1,ii2,ii3)]] = ( n['rJ'][dir_name(ii1,ii2,ii3)]  + 
                                             n['rP'][dir_name(ii1,ii2,ii3)]  + 
                                             n['rJP'][dir_name(ii1,ii2,ii3)] )
        FP[cdict[dir_name(ii1,ii2,ii3)]] = ( n['J'][dir_name(ii1,ii2,ii3)]  + 
                                             n['P'][dir_name(ii1,ii2,ii3)]  + 
                                             n['JP'][dir_name(ii1,ii2,ii3)] )
        FN[cdict[dir_name(ii1,ii2,ii3)]] = n['r'][dir_name(ii1,ii2,ii3)]
#
    prec = Precision(TP,FP)
    rec  = Recall(TP,FN)
    F1   = F1_score(prec,rec)
    return prec,rec,F1

def I_model(n,i1,i2,i3,cdict):
    """
    MODEL:
    TP -> rJP
    FP -> JP
    FN -> r + rJ + rP

    INPUTS:
    n -> dict
    i1, i2, i3 -> list

    OUTPUTS:
    prec, rec, F1 -> np.ndarray
    """
    TP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FN = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    for ii1,ii2,ii3 in zip(i1,i2,i3):
        TP[cdict[dir_name(ii1,ii2,ii3)]] = n['rJP'][dir_name(ii1,ii2,ii3)]
        FP[cdict[dir_name(ii1,ii2,ii3)]] = n['JP'][dir_name(ii1,ii2,ii3)]
        FN[cdict[dir_name(ii1,ii2,ii3)]] = ( n['r'][dir_name(ii1,ii2,ii3)]  + 
                                             n['rJ'][dir_name(ii1,ii2,ii3)] + 
                                             n['rP'][dir_name(ii1,ii2,ii3)] )
#
    prec = Precision(TP,FP)
    rec  = Recall(TP,FN)
    F1   = F1_score(prec,rec)
    return prec,rec,F1

def J_model(n,i1,i2,i3,cdict):
    """
    MODEL:
    TP -> rJ + rJP
    FP -> J + JP
    FN -> r + rP

    INPUTS:
    n -> dict
    i1, i2, i3 -> list

    OUTPUTS:
    prec, rec, F1 -> np.ndarray
    """
    TP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FN = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    for ii1,ii2,ii3 in zip(i1,i2,i3):
        TP[cdict[dir_name(ii1,ii2,ii3)]] = ( n['rJ'][dir_name(ii1,ii2,ii3)]  + 
                                             n['rJP'][dir_name(ii1,ii2,ii3)] )
        FP[cdict[dir_name(ii1,ii2,ii3)]] = ( n['J'][dir_name(ii1,ii2,ii3)]  + 
                                             n['JP'][dir_name(ii1,ii2,ii3)] )
        FN[cdict[dir_name(ii1,ii2,ii3)]] = ( n['r'][dir_name(ii1,ii2,ii3)]  +
                                             n['rP'][dir_name(ii1,ii2,ii3)] )
#
    prec = Precision(TP,FP)
    rec  = Recall(TP,FN)
    F1   = F1_score(prec,rec)
    return prec,rec,F1

def P_model(n,i1,i2,i3,cdict):
    """
    MODEL:
    TP -> rP + rJP
    FP -> P + JP
    FN -> r + rJ

    INPUTS:
    n -> dict
    i1, i2, i3 -> list

    OUTPUTS:
    prec, rec, F1 -> np.ndarray
    """
    TP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FP = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    FN = np.zeros((cdict['n2'],cdict['n3']),dtype=np.int)
    for ii1,ii2,ii3 in zip(i1,i2,i3):
        TP[cdict[dir_name(ii1,ii2,ii3)]] = ( n['rP'][dir_name(ii1,ii2,ii3)]  + 
                                             n['rJP'][dir_name(ii1,ii2,ii3)] )
        FP[cdict[dir_name(ii1,ii2,ii3)]] = ( n['P'][dir_name(ii1,ii2,ii3)]  + 
                                             n['JP'][dir_name(ii1,ii2,ii3)] )
        FN[cdict[dir_name(ii1,ii2,ii3)]] = ( n['r'][dir_name(ii1,ii2,ii3)]  +
                                             n['rJ'][dir_name(ii1,ii2,ii3)] )
#
    prec = Precision(TP,FP)
    rec  = Recall(TP,FN)
    F1   = F1_score(prec,rec)
    return prec,rec,F1

def log(logstream, lock, msg):
    msg = "%d: %s"%(os.getpid(),msg)
    with lock:
        logstream.write(msg + '\n')
        logstream.flush()
    return

def mp_task(ii1,ii2,ii3,lock,err_file,out_dir,fext,log_file,in_file,gs_flg,loop_file):
    t1 = time.time()
    log(sys.stdout,lock,'\tNode:\t%d\t%f\t%f'%(ii1,ii2,ii3))
    os.system('echo ---- %d\t%f\t%f ---- >> %s'%(ii1,ii2,ii3,err_file))
    #if len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+'/pvi_*'+fext)) != 0:
    #    t2 = time.time()
    #    log(sys.stdout,lock,'\tThis was already done - time = %f s'%(t2-t1))
    #    return
    log_file_ = log_file[0]+'%d'%(ii1)+log_file[1]+'%f'%(ii2)+log_file[2]+'%f'%(ii3)+log_file[3]
    in_file_ = in_file[0]+'%d'%(ii1)+in_file[1]+'%f'%(ii2)+in_file[2]+'%f'%(ii3)+in_file[3]
    f = open(in_file_,'w')
    f.write('%d\n%d\n%f\n%f\n '%(gs_flg,ii1,ii2,ii3))
    f.close()
    os.system('python %s < %s > %s 2>> %s'%(loop_file,in_file_,log_file_,err_file))
    t2 = time.time()
    log(sys.stdout,lock,'\tDone in %f s'%(t2-t1))
    return

def plot_func(score_name,prec_all,rec_all,F1_all,modlist,figsize=(15,15),annot=False):
    if score_name == 'F1 score':
        score = F1_all
    elif score_name == 'Precision':
        score = prec_all
    elif score_name == 'Recall':
        score = rec_all
    else:
        print('ERROR in plot_func: unknown score_name. Possible values: \'F1 score\', \'Precision\', \'Recall\'.')

    plt.close('all')
    fig = plt.figure(figsize=figsize,constrained_layout=False)
    gs = GridSpec(2,3,figure=fig,height_ratios=[2.,1.],width_ratios=[8.8,.7,.5])

    gs00 = SubSpec(2,2,gs[0,0:2])
    cax = fig.add_subplot(gs[0,2])

    mins = []
    maxs = []
    for i,j in [(i,j) for i in [0,1] for j in [0,1]]:
        mins.append(np.min(score[modlist[i][j]]))
        maxs.append(np.max(score[modlist[i][j]]))

    vmin = min(mins)
    vmax = max(maxs)

    im_list = []
    jm_list = []
    F1_list = []
    prec_list = []
    rec_list = []
    for i,j in [(i,j) for i in [0,1] for j in [0,1]]:
        ax = fig.add_subplot(gs00[i,j])
        _ = sns.heatmap(score[modlist[i][j]],
                        ax=ax,
                        cbar = ((i==0)and(j==0)),
                        cbar_ax = cax if ((i==0)and(j==0)) else None,
                        cmap='PuBuGn',
                        vmin=vmin,#0.,
                        vmax=vmax,#1.,
                        annot=annot,
                        fmt='.2f',
                        annot_kws={'size':15},
                        linewidths=0.5,
                        xticklabels=['%.2f'%s for s in sj] if i==1 else False,
                        yticklabels=['%.2f'%s for s in si] if j==0 else False)
        if i == 1:
            ax.set_xlabel('$\sigma(\int_{\ell}\mathcal{I})$')
        if j == 0:
            ax.set_ylabel('$\sigma(J^2)$')
        ax.set_title(modlist[i][j]+' model - '+score_name)
#
        im,jm = np.unravel_index(score[modlist[i][j]].argmax(),score[modlist[i][j]].shape)
        im_list.append(im)
        jm_list.append(jm)
        F1_list.append(F1_all[modlist[i][j]][im,jm])
        prec_list.append(prec_all[modlist[i][j]][im,jm])
        rec_list.append(rec_all[modlist[i][j]][im,jm])
    
    ax = fig.add_subplot(gs[1,0])
    gc = np.array([1.,2.,3.,4.])
    hw = .1
    lA = gc - hw*3.
    rA = gc - hw
    lB = rA
    rB = lB + 2.*hw
    lC = rB
    rC = lC + 2.*hw
    hA = ax.bar(lA,F1_list,width=2.*hw,align='edge',label='F1 score')
    for x,y in zip(lA+hw,F1_list):
        ax.text(x,y,'%.2f'%y,ha='center',va='top',color='white')
    hB = ax.bar(lB,prec_list,width=2.*hw,align='edge',label='Precision')
    for x,y in zip(lB+hw,prec_list):
        ax.text(x,y,'%.2f'%y,ha='center',va='top',color='white')
    hC = ax.bar(lC,rec_list,width=2.*hw,align='edge',label='Recall')
    for x,y in zip(lC+hw,rec_list):
        ax.text(x,y,'%.2f'%y,ha='center',va='top',color='white')
    ax.set_xlim(lA[0]-(.5-3.*hw),rC[-1]+(.5-3.*hw))
    ax.set_ylim(0.,1.)
    ax.set_xticks(gc)
    modlist_flat = [m for sl in modlist for m in sl]
    ax.set_xticklabels([m+' - %.2f,%.2f'%(si[im],sj[jm]) for m,im,jm in zip(modlist_flat,im_list,jm_list)])
    ax.set_xlabel('Model - $\sigma(J^2),\sigma(\int_{\ell}\mathcal{I})$')
    ax.set_ylabel('Score')
    ax.set_title('Models best-off comparison - '+score_name)

    lax = fig.add_subplot(gs[1,1:])
    lax.axis('off')
    lax.legend(handles=[hA,hB,hC],loc='center')

    plt.tight_layout()

    if plt_show_flg:
        plt.show()

    if plt_save_flg:
        plots = glob.glob(out_dir+'/%s_new_*_.png'%(score_name.replace(' ','')))
        nums = [int(p.split('/')[-1].split('_')[1]) for p in plots]
        if len(nums) == 0:
            num = 0
        else:
            num = max(nums) + 1
        fig.savefig(out_dir+'/%s_new_%d_.png'%(score_name.replace(' ',''),num))

    plt.close()
    return

#--------------------------------
#Init GridSearch
#---------------
cdict = {'n2':len(si),'n3':len(sj)}
for ii,i in enumerate(si):
    for jj,j in enumerate(sj):
        #if (i in oldsi )and (j in oldsj):
        #    continue
        i1.append(1)
        i2.append(i)
        i3.append(j)
        cdict[dir_name(1,i,j)] = (ii,jj)

if gs_flg == 0:
    fext = '.png'
elif gs_flg == 1:
    fext = '.GRIDSEARCH'
else:
    print('ERROR: unknown value for gs_flg. Setting fext = \'UNKNOWN\'')
    fext = '.UNKNOWN'

#--------------------------------
#Parallel GridSearch
#-------------------
if do_gs:
    loop_file = 'LOOPER_pvi_scalogram_NOCWT_v4.py'
    in_file = ('input_%s_*_*_*.dat'%(loop_file.split('.')[0])).split('*')
    log_file = ('log_%s_*_*_*.dat'%(loop_file.split('.')[0])).split('*')
    err_file = 'err_log_%s.dat'%(loop_file.split('.')[0])

    stdoutlock = mp.Lock()
    ntask = len(i1)

    t0 = time.time()
    os.system('echo ----------------GRIDSEARCH START---------------- >> %s'%(err_file))
    log(sys.stdout,stdoutlock,'\nStar of GridSearch\n')

    workers = []
    for ii1,ii2,ii3 in zip(i1,i2,i3):
        workers.append(mp.Process(target=mp_task,args=(ii1,ii2,ii3,stdoutlock,err_file,
                                                       out_dir,fext,log_file,in_file,gs_flg,loop_file)))

    i = 0
    cmd = "dmesg | egrep -i 'killed process' | tail -n 1 | awk '{print $4}' > "
    cnt = 0
    tmpodd = 'tmp_odd.txt'
    tmpeven = 'tmp_even.txt'
    _ = os.system(cmd+tmpodd)
    _ = os.system(cmd+tmpeven)
    someonekilled = False
    while i < ntask:
        if sum([worker.is_alive() for worker in workers]) == ncpu:
            if cnt%2:
                _ = os.system(cmd+tmpodd)
            else:
                _ = os.system(cmd+tmpeven)
            f = open(tmpeven,'r')
            pid1 = int(f.readline())
            f.close()
            f = open(tmpodd,'r')
            pid2 = int(f.readline())
            f.close()
            if pid1 != pid2:
                log(sys.stdout,stdoutlock,'\nSOMEONE WAS KILLED!!!\n')
                someonekilled = True
                break
            cnt += 1
            time.sleep(5)
            continue
        workers[i].start()
        i += 1

    if someonekilled:
        for worker in workers:
            if worker.is_alive():
                worker.terminate()
        time.sleep(5)
        os.system('pkill -U %s python'%user)
    else:
        for worker in workers:
            worker.join()

    t3 = time.time()
    log(sys.stdout,stdoutlock,'\nEnd of GridSearch - time = %f s\n'%(t3-t0))

    if in_rm:
        os.system('rm -f %s'%('*'.join(in_file)))
    if log_rm:
        os.system('rm -f %s'%('*'.join(log_file)))
    if err_rm:
        os.system('rm -f %s'%('*'.join(err_file)))

#--------------------------------
#Results counting
#----------------
n = {
'tot'   : {},
'sites' : {},
'full'  : {},
'r'     : {},
'J'     : {},
'P'     : {},
'rJ'    : {},
'rP'    : {},
'JP'    : {},
'rJP'   : {},
'all_r' : {},
'all_P' : {},
'all_J' : {}
}

for ii1,ii2,ii3 in zip(i1,i2,i3):
    n['tot'][dir_name(ii1,ii2,ii3)]   = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*'+fext))
    n['sites'][dir_name(ii1,ii2,ii3)] = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_scalogram_*'+fext))
    n['full'][dir_name(ii1,ii2,ii3)]  = ( n['tot'][dir_name(ii1,ii2,ii3)] - 
                                          n['sites'][dir_name(ii1,ii2,ii3)] )
    n['r'][dir_name(ii1,ii2,ii3)]     = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f1_*'+fext))
    n['J'][dir_name(ii1,ii2,ii3)]     = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f10_*'+fext))
    n['P'][dir_name(ii1,ii2,ii3)]     = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f100_*'+fext))
    n['rJ'][dir_name(ii1,ii2,ii3)]    = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f11_*'+fext))
    n['rP'][dir_name(ii1,ii2,ii3)]    = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f101_*'+fext))
    n['JP'][dir_name(ii1,ii2,ii3)]    = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f110_*'+fext))
    n['rJP'][dir_name(ii1,ii2,ii3)]   = len(glob.glob(out_dir+'/'+dir_name(ii1,ii2,ii3)+
                                                      '/pvi_*f111_*'+fext))
    n['all_r'][dir_name(ii1,ii2,ii3)] = ( n['r'][dir_name(ii1,ii2,ii3)]  + 
                                          n['rJ'][dir_name(ii1,ii2,ii3)] +
                                          n['rP'][dir_name(ii1,ii2,ii3)] +
                                          n['rJP'][dir_name(ii1,ii2,ii3)] )
    n['all_P'][dir_name(ii1,ii2,ii3)] = ( n['P'][dir_name(ii1,ii2,ii3)]  + 
                                          n['JP'][dir_name(ii1,ii2,ii3)] +
                                          n['rP'][dir_name(ii1,ii2,ii3)] +
                                          n['rJP'][dir_name(ii1,ii2,ii3)] )
    n['all_J'][dir_name(ii1,ii2,ii3)] = ( n['J'][dir_name(ii1,ii2,ii3)]  + 
                                          n['rJ'][dir_name(ii1,ii2,ii3)] +
                                          n['JP'][dir_name(ii1,ii2,ii3)] +
                                          n['rJP'][dir_name(ii1,ii2,ii3)] )

for ii1,ii2,ii3 in zip(i1,i2,i3):
    d_n = dir_name(ii1,ii2,ii3)
    print('\n%.2f , %.2f -> %s\t\t\t%s'%(ii2,ii3,d_n,'' if n['full'][d_n]==17 else 'ERROR!'))
    for k in ['tot','full','sites']:
        print('\t%s\t%d'%(k,n[k][d_n]))

#--------------------------------
#Models scores
#-------------
prec_all = {}
rec_all  = {}
F1_all   = {}

prec_all['U'],rec_all['U'],F1_all['U'] = U_model(n,i1,i2,i3,cdict)
prec_all['I'],rec_all['I'],F1_all['I'] = I_model(n,i1,i2,i3,cdict)
prec_all['J'],rec_all['J'],F1_all['J'] = J_model(n,i1,i2,i3,cdict)
prec_all['P'],rec_all['P'],F1_all['P'] = P_model(n,i1,i2,i3,cdict)

#--------------------------------
#Plotting
#--------
if not (plt_show_flg or plt_save_flg):
    sys.exit()

modlist = [['U','I'],['J','P']]

plot_func('F1 score',prec_all,rec_all,F1_all,modlist)
plot_func('Precision',prec_all,rec_all,F1_all,modlist)
plot_func('Recall',prec_all,rec_all,F1_all,modlist)
