import os
import sys

import numpy as np

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_SAV/my_branch')
from iPIC_loader import *
from HVM_loader import *

run,tr,xr=wl.Init(sys.argv[1])
ind1,ind2,ind_step = tr
time = run.meta['time']

ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
for ind in ind_range:
    t = time[ind]
    with open('inp_tmp.dat', 'w') as f:
        f.write('LF_3d_DH HVM\n')
        f.write('/work2/finelli/LF_3d_DH\n')
        f.write('/work2/finelli/Outputs\n')
        f.write('True %f %f 1\n'%(t, t))
        f.write('True 0. 76. 0. 12.5\n')
        f.write('00\n')
    print('\nStarting t = %f\n'%(t,))
    #os.system('python2 windlike_plot_multitimes_3d.py inp_tmp.dat')
    os.system('python2 fluct_in_wind_multitimes_3d.py inp_tmp.dat')
os.system('rm -f inp_tmp.dat')
