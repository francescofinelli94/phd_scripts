#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
# from fibo_beta import *  # NEEDED???
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of electron larmor radius.\n')

#------------------------------------------------------
#Functions
#---------
def cmap_gen(vmin=0., vmax=1., vmid=.5):
    bit_rgb = np.linspace(0., 1., 256)
    colors = [(255, 0, 0), (128, 128, 128), (0, 0, 255), (0, 255, 0), (255, 255, 255)]
    for iii in range(len(colors)):
        colors[iii] = (bit_rgb[colors[iii][0]],
                       bit_rgb[colors[iii][1]],
                       bit_rgb[colors[iii][2]])

    v0 = (vmid - vmin)/(vmax - vmin)
    v1 = min(.9,2.*v0)
    v2 = min(.95,3.*v0)
    # positions = [0.0, v0, v1, 1.]
    positions = [0.0, v0, v1, v2, 1.]

    cdict = {'red':[], 'green':[], 'blue':[]}
    for pos, color in zip(positions, colors):
        cdict['red'].append((pos, color[0], color[0]))
        cdict['green'].append((pos, color[1], color[1]))
        cdict['blue'].append((pos, color[2], color[2]))

    return mpl.colors.LinearSegmentedColormap('mycmap', cdict, 256)

#------------------------------------------------------
#Init
#----
#input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']
input_files = ['DH_run11_data0.dat']

plt_show_flg = True

run_name_vec = []
run_labels = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind1,ind2,ind_step = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    w_ele = run.meta['w_ele']
    m_e = 1./run.meta['mime']
    x = run.meta['x']
    y = run.meta['y']
    dx = run.meta['ddd'][0]
    times = run.meta['time']*run.meta['tmult']
    code_name = run.meta['code_name']
#
    if code_name == 'HVM':
        smooth_flag = False
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
        out_dir = 'rho_2d_plot'
        ml.create_path(opath+'/'+out_dir)
#
    #---> loop over times <---
    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        #--------------------------------------------------------
        #getting data and computing stuff
        #-------------------------------------
#
        # rle/dx = [sqrt(Pperp_e/(rhoe))/(B*mime)]/dx,
        # 
        # where Pperp_e is the local electron perp pressure,
        # rhoe is the electron mass density =charge density/(qome) (qome=-100),
        # B is the local module of B, mime=100.
#
        # Bn
        _,B = run.get_EB(ind)
        Bn = np.sqrt(B[0]*B[0] + B[1]*B[1] + B[2]*B[2])
#
        # n_e
        if code_name == 'HVM':
            n_e, _ = run.get_Ion(ind)
        elif code_name == 'iPIC':
            n_e, _ = run.get_Ion(ind, qom=qom_e)
        else:
            print('\nWhat code is it?\n')
#
        # pperpe
        if w_ele:
            if code_name == 'HVM':
                _, pperpe = run.get_Te(ind)
                pperpe = tperpe*n_e # pperpe = n_e*tperpe
            elif code_name == 'iPIC':
                Pe = run.get_Press(ind, qom=qom_e)
                _, pperpe = ml.proj_tens_on_vec(Pe, B) # pperpe = Pe:(id - bb)/2
                del Pe
            else:
                print('\nWhat code is it?\n')
        else:
            pperpe = n_e*run.meta['beta']*0.5*run.meta['teti'] # pperpe = Pisoe
        del B
#
        # rle
        rle = np.sqrt(m_e*pperpe/n_e)/Bn
#
        # plot
        z = rle[ixmin:ixmax, iymin:iymax, 0].T/dx
        tmp_cmap = cmap_gen(vmin=np.min(z), vmax=np.max(z), vmid=1.)
        fig, ax0 = plt.subplots(1, 1, figsize=(12, 8))
        im0 = ax0.contourf(x[ixmin:ixmax], y[iymin:iymax], z, 127, cmap=tmp_cmap)
        plt.colorbar(im0,ax=ax0)
        ax0.set_title('$\\rho_{\mathrm{e}}/\Delta x$  - %s - $t = %.0f \Omega_{p}^{-1}$'%(
                      run_labels[run_name], times[ind]))
        ax0.set_xlabel('x [$d_\mathrm{p}$]')
        ax0.set_ylabel('y [$d_\mathrm{p}$]')
        fig_name = 'rhoe_over_dx__'+run_name+'__it%d.png'%(ind,)
        plt.tight_layout()
        plt.savefig(opath+'/'+out_dir+'/'+fig_name)
        plt.close()
#   
    #---> loop over time <---
        print "\r",
        print "t = ",times[ind],
        gc.collect()
        sys.stdout.flush()
    #------------------------
