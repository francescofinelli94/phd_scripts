#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of anis, curv, pressure-drift.\n')

#------------------------------------------------------
#Init
#----
#input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']
input_files = ['LF_2d_DH.dat']

plt_show_flg = True

beta = {'p':{'m':{},'s':{}},'e':{'m':{},'s':{}}}
t = {}

run_name_vec = []
run_labels = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind1,ind2,ind_step = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    code_name = run.meta['code_name']
#
    if code_name == 'HVM':
        smooth_flag = False
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
#
    beta['p']['m'][run_name] = []
    beta['p']['s'][run_name] = []
    beta['e']['m'][run_name] = []
    beta['e']['s'][run_name] = []
    t[run_name] = []
#
    #---> loop over times <---
    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        #--------------------------------------------------------
        #getting data and computing stuff
        #-------------------------------------
        #B^2
        _,B = run.get_EB(ind)
        B2 = (B[0]*B[0] + B[1]*B[1] + B[2]*B[2])[ixmin:ixmax,iymin:iymax,0]
        del B
#
        #Pisop
        Pp = run.get_Press(ind)
        Pisop = ((Pp[0,0] + Pp[1,1] + Pp[2,2])/3.)[ixmin:ixmax,iymin:iymax,0]
        del Pp
#
        #Pisoe
        if w_ele:
            if code_name == 'HVM':
                pparle,pperpe = run.get_Te(ind)
                Pisoe = ((pparle + 2.*pperpe)/3.)[ixmin:ixmax,iymin:iymax,0]
                del pparle,pperpe
            elif code_name == 'iPIC':
                Pe = run.get_Press(ind,qom=qom_e)
                Pisoe = ((Pe[0,0] + Pe[1,1] + Pe[2,2])/3.)[ixmin:ixmax,iymin:iymax,0]
                del Pe
            else:
                print('\nWhat code is it?\n')
        else:
            n_e,_ = run.get_Ion(ind)
            Pisoe = n_e[ixmin:ixmax,iymin:iymax,0]*run.meta['beta']*0.5*run.meta['teti']
            del n_e
#
        #betap, betae
        betap = 2.*np.divide(Pisop,B2)
        betae = 2.*np.divide(Pisoe,B2)
        del B2, Pisop, Pisoe
        if code_name == 'iPIC':
            betap = 4.*np.pi*betap
            betae = 4.*np.pi*betae
        elif code_name != 'HVM':
            print('\nWhat code is it?\n')
#
        fig,(ax0,ax1) = plt.subplots(1,2,figsize=(14,8))
        im0 = ax0.contourf(betap[ixmin:ixmax,iymin:iymax].T,63)
        plt.colorbar(im0,ax=ax0)
        ax0.set_title('beta p - %s - t = %f'%(run_labels[run_name],times[ind]))
        im1 = ax1.contourf(betae[ixmin:ixmax,iymin:iymax].T,63)
        plt.colorbar(im1,ax=ax1)
        ax1.set_title('beta e - %s - t = %f'%(run_labels[run_name],times[ind]))
        plt.show()
        plt.close()
#   
        #--------------------------------------------------------
        #saving data
        #-------------------------------------
        beta['p']['m'][run_name].append(np.mean(betap))
        beta['p']['s'][run_name].append(np.std(betap))
        del betap
#
        beta['e']['m'][run_name].append(np.mean(betae))
        beta['e']['s'][run_name].append(np.std(betae))
        del betae
#
        t[run_name].append(times[ind])
    #---> loop over time <---
        print "\r",
        print "t = ",times[ind],
        gc.collect()
        sys.stdout.flush()
    #------------------------

    beta['p']['m'][run_name] = np.array(beta['p']['m'][run_name])
    beta['p']['s'][run_name] = np.array(beta['p']['s'][run_name])
    beta['e']['m'][run_name] = np.array(beta['e']['m'][run_name])
    beta['e']['s'][run_name] = np.array(beta['e']['s'][run_name])
    t[run_name]              = np.array(t[run_name]             )

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
nplt = len(input_files)
fig,ax = plt.subplots(1,nplt,figsize=(5*nplt,4))

for j,run_name in enumerate(run_name_vec):
    if nplt == 1:
        ax_ = ax
    else:
        ax_ = ax[j]
#
    x = t[run_name]
    y1 = beta['p']['m'][run_name]
    dy1 = beta['p']['s'][run_name]
    ax_.plot(x,y1,'b-',label='Protons')
    ax_.fill_between(x,y1-dy1,y1+dy1,color='blue',alpha=0.5)
#
    y2 = beta['e']['m'][run_name]
    dy2 = beta['e']['s'][run_name]
    ax_.plot(x,y2,'r-',label='Electrons')
    ax_.fill_between(x,y2-dy2,y2+dy2,color='red',alpha=0.5)
#
    ax_.set_xlabel('$t\\;[\\Omega_{c,p}^{-1}]$')
    if j == 0:
        ax_.set_ylabel('$\\langle\\beta_a\\rangle\\pm\\sigma(\\beta_a)$')
    ax_.set_title(run_labels[run_name])
    ax_.legend()
    ax_.set_xlim(x[0],x[-1])
    ax_.set_ylim(min(np.min(y1),np.min(y2)),max(np.max(y1+dy1),np.max(y2+dy2)))
    ax_.set_yscale('log')

if plt_show_flg:
    plt.show()

out_dir = 'beta_over_time'
ml.create_path(opath+'/'+out_dir)
fig_name = 'beta_ovet_time__'
for run_name in run_name_vec:
    fig_name += '__' + run_labels[run_name]

fig_name += '.png'
fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
