import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
sys.path.insert(0,'/work1/califano/VISUAL/PYTHON')
import fibo_beta as fb
import mylib as ml

if len(sys.argv) < 2:
    ipath = ml.secure_input('Insert data path: ','')
else:
    ipath = sys.argv[1]

run = fb.from_HVM(ipath)
try:
    run.get_meta('00')
except:
    ipp = ml.secure_input('Insert path cantaining input_parameters: ','')
    run.get_meta(ipp)

run.segs = ml.dict_keys_sort(run.segs)
time,time2seg,time2exit = ml.time_manage(run.segs)

#get anis at one time
t0 = ml.secure_input('Choose a time: ',1.,True)
ind = ml.index_of_closest(t0,time)
print('Closest time is '+str(time[ind]))

E,B = run.get_EB(time2seg[ind],time2exit[ind])
B[0] = B[0] + run.meta['Bx0']
B[1] = B[1] + run.meta['By0']
B[2] = B[2] + run.meta['Bz0']
#E = None
#gc.collect()

#P = run.get_Press(time2seg[ind],time2exit[ind])

#pparlp,pperpp = ml.proj_tens_on_vec(P,B,True)
#P = None
#gc.collect()

print('B -> mean, min, max')
print(str(np.nanmean(B))+' '+str(np.nanmin(B))+' '+str(np.nanmax(B)))
print('E -> mean, min, max')
print(str(np.nanmean(E))+' '+str(np.nanmin(E))+' '+str(np.nanmax(E)))

errB = np.isnan(B)+np.isinf(B)
errE = np.isnan(E)+np.isinf(E)

f1 = open('where_B_fails_'+time2seg[ind]+'_'+str(time2exit[ind])+'.dat','w')
f2 = open('where_E_fails_'+time2seg[ind]+'_'+str(time2exit[ind])+'.dat','w')

for k in range(run.meta['nz']):
    for j in range(run.meta['ny']):
        for i in range(run.meta['nx']):
            for n in range(3):
                if errB[n,i,j,k]:
                    f1.write('B='+str(B[n,i,j,k])+' in '+str([n,i,j,k]))
                if errE[n,i,j,k]:
                    f2.write('E='+str(E[n,i,j,k])+' in '+str([n,i,j,k]))

f1.close()
f2.close()

#for i in range(run.meta['ny']):
#    plt.close()
#    fig, ax = plt.subplots(nrows=2,ncols=2,figsize=(10,8))
##
#    im = ax[0,0].imshow(B[0,:,i,:])
#    plt.colorbar(im,ax=ax[0,0])
#    ax[0,0].set_title('Bx y='+str(i))
##
#    im = ax[0,1].imshow(B[1,:,i,:])
#    plt.colorbar(im,ax=ax[0,1])
#    ax[0,1].set_title('By')
##
#    im = ax[1,0].imshow(B[2,:,i,:])
#    plt.colorbar(im,ax=ax[1,0])
#    ax[1,0].set_title('Bz')
##
#    plt.tight_layout()
#    im = ax[1,1].imshow(B[2,:,:,i*run.meta['nz']//run.meta['ny']].T)
#    plt.colorbar(im,ax=ax[1,1])
#    ax[1,1].set_title('Bz z='+str(i*run.meta['nz']//run.meta['ny']))
##
#    plt.savefig('y_'+time2seg[ind]+'_'+str(time2exit[ind])+'_'+str(i)+'.png')
#    plt.close()
