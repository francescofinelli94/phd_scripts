import sys
import glob
from collections import OrderedDict as OD

import numpy as np
from matplotlib import use as mpl_use
mpl_use('Agg')
from matplotlib import get_backend as mpl_get_backend
import matplotlib.pyplot as plt
import scipy.ndimage.interpolation as ndm

sys.path.insert(0,'/work2/finelli/Documents_SAV/my_branch')
import mylib as ml
from fibo_beta import *
from HVM_loader import *

#----------------------------------------------------------------------------------------------------
font = 28  # 17 20 23 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#----------------------------------------------------------------------------------------------------
#HARDCODED!!!

sim_path='/work1/VLASOV_SIMS/HVM2D/HVM_3072'
meta_subpath = '01'

pvi_path_ = '/work2/finelli/pvi_series'

t_dict = {}
t_dict['28'] = 247.
t_dict['60'] = 494.

p = 2.0  # number of steps inside each cell
SSx0 = 0.01   # initial positions of the virtual satellite inside the simulation box
SSy0 = 0.01

regions_path_ = '/work2/finelli/pvi_series/verified_regions/regions_verified_reconnection_tTIME.txt'

opath = '/work2/finelli/Outputs'

#----------------------------------------------------------------------------------------------------
#Metadata time

run = from_HVM(sim_path)
run.get_meta(meta_subpath)
run_name = sim_path.split('/')[-1]
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']

calc = fibo('calc')
calc.meta = run.meta

out_dir = 'PVI_maxs_per_scale' 
ml.create_path(opath+'/'+out_dir)

#----------------------------------------------------------------------------------------------------
#PRIMA PARTE: ricostruzione traiettoria, le coordinate finali xco e yco sono in unita' [di]

MAX = {}
ttt = []


# -> what time is it?
segments = sorted([int(tmp.split('_')[-1]) for tmp in glob.glob(pvi_path_+'/new_pvi_*')])
if len(segments) == 0:
    sys.exit("ERROR: no direcory new_pvi_* found in:\n"+pvi_path_)

print('\nsegments:')
print(segments)

for seg in segments:
    seg = str(seg)
    print 'seg=%s'%(seg,)

    MAX[seg] = {}

    t_ = t_dict[seg]
    ind = ml.index_of_closest(t_,run.meta['time'])
    t_ = run.meta['time'][ind]
    ttt.append( t_ )

    # -> angle?
    angles = sorted([int(tmp.split('angle')[-1]) 
        for tmp in glob.glob(pvi_path_+('/new_pvi_%s/angle*')%(seg)) 
        if len(tmp.split('angle')[-1].split('_'))==1])
    if len(angles) == 0:
        sys.exit("ERROR: no direcory angle* found in:\n"+pvi_path_+('/new_pvi_%s')%(seg))

    print('\nangles:')
    print(angles)
    for ang in angles:
        if (seg == '28') and (ang == 38):
            continue
        print 'ang=%d'%(ang,)
        # -> load/construct trajectory
        pvi_path = pvi_path_ + '/new_pvi_%s/angle%d'%(seg,ang)

        pi2 = 2.0*np.pi
        theta_s = float(ang)*pi2/360.0

        files = glob.glob(pvi_path+'/B_1Dcut_pvi_*.dat')
        ll = [] #PVI scale
        cc = [] #is centered?
        for f in files:
            tmp = f.split('pvi_')[-1].split('.')[0]
            ll.append(int(tmp.split('_')[0]))
            cc.append(tmp.split('_')[-1] == 'center')


        files = [tmp for _,tmp in sorted(zip(ll,files))]
        cc = [tmp for _,tmp in sorted(zip(ll,cc))]
        ll = np.array(sorted(ll))

        lstart = ll[0]
        f = open(files[0], 'r')
        lines = f.readlines()
        f.close()
        ns = len(lines)
        sco = np.zeros((ns),dtype=np.float64)
        xco = np.zeros((ns),dtype=np.float64)
        yco = np.zeros((ns),dtype=np.float64)
        PVI = np.full((ns,len(ll)),-1.,dtype=np.float64)
        num_col = len(lines[0].split())
        if num_col == 2:
            DELTAs  = pi2/float(nx)/p
            DELTAsx = DELTAs*np.cos(theta_s)
            DELTAsy = DELTAs*np.sin(theta_s)
            SSx = SSx0
            SSy = SSy0
            SSS = np.sqrt(SSx**2+SSy**2)
            for i in range(ns):
                SSx += DELTAsx
                SSy += DELTAsy
                SSS += DELTAs
                if SSx >= pi2: SSx -= pi2
                if SSy >= pi2: SSy -= pi2
                sco[i]=SSS
                xco[i]=SSx
                yco[i]=SSy
        elif num_col == 4:
            for i in range(ns):
                tr = lines[i].split()
                sco[i] = float(tr[0])
                xco[i] = float(tr[1])
                yco[i] = float(tr[2])
            DELTAs  = sco[1] - sco[0]
            DELTAsx = xco[1] - xco[0]
            DELTAsy = yco[1] - yco[0]
        else:
            sys.exit("ERROR: number of columns is not 2 nor 4, but "+str(num_col)+".")

        sco = sco*xl/pi2
        xco = xco*xl/pi2
        yco = yco*yl/pi2
        DELTAs  = DELTAs*xl/pi2
        DELTAsx = DELTAsx*xl/pi2 
        DELTAsy = DELTAsy*yl/pi2 
        ds = sco[1] - sco[0]

        # -> read PVIs
        AVG = np.empty((len(ll),), dtype=float)
        SIG = np.empty((len(ll),), dtype=float)
        for il,l,c,fname in zip(range(len(ll)),ll,cc,files):
            f = open(fname, 'r')
            lines = f.readlines()
            f.close()
            for i in range(len(lines)):
                PVI[i,il] = float(lines[i].split()[-1])
            if not c:
                PVI[:,il] = np.roll(PVI[:,il],l//2)
            AVG[il] = np.mean(PVI[PVI[:,il]>=0.,il])
            SIG[il] = np.std(PVI[PVI[:,il]>=0.,il])
        
#----------------------------------------------------------------------------------------------------
#SECONDA PARTE: interpolazione

        new_coordsx = xco/dx
        new_coordsy = yco/dy

        # -> where is reconnection?
        tmp = regions_path_.split('TIME') 
        regions_path = tmp[0] + str(int(t_)) + tmp[1]
        regions = np.loadtxt(regions_path)
        regions = (regions > -1.).astype(np.int)
        regions_tmp = np.empty((regions.shape[0]+1, regions.shape[1]+1), dtype=int)
        regions_tmp[:-1, :-1] = regions[:, :]
        regions_tmp[-1, :-1] = regions[0, :]
        regions_tmp[:-1, -1] = regions[:, 0]
        regions_tmp[-1, -1] = regions[0, 0]

        regions_an = ndm.map_coordinates(regions_tmp[:,:].T,np.vstack((new_coordsx,new_coordsy)),
                                         mode='wrap',order=1,prefilter=False)
        del regions, regions_tmp
        regions_an = (regions_an > 0.5).astype(int)

        tmp = np.zeros((regions_an.size+2, ), dtype=int)
        tmp[1:-1] = regions_an[:]
        tmp = np.diff(tmp)
        REC = np.array([np.where(tmp==1)[0],np.where(tmp==-1)[0]]).T
        del regions_an, tmp

#----------------------------------------------------------------------------------------------------
#TERZA PARTE: maxs
        for il,l in enumerate(ll):
            if l not in MAX[seg]:
                MAX[seg][l] = []
            for i0,i1 in REC:
                MAX[seg][l].append( np.max((PVI[max(i0-l,0):min(i1+l,ns),il]-AVG[il])/SIG[il]) )

    print ''

for seg in MAX:
    MAX[seg] = OD(sorted(MAX[seg].items()))
MAX = OD(sorted(MAX.items()))

#----------------------------------------------------------------------------------------------------
#QUARTA PARTE: plot
ths = np.arange(2,20,2)

plt.close('all')
fig, (ax0, ax1) = plt.subplots(2, 1 , figsize=(16, 10), sharex=True)
plt.subplots_adjust(hspace=.0, top=.99, right=.99, left=.08, bottom=.1)

ax0.set_ylabel('$\mathrm{max}_{\mathrm{rr}}\left(\\frac{\mathcal{I}-\mu}{\sigma}\\right)$ - $t=%d$'%(round(ttt[0]),))
ax1.set_ylabel('$\mathrm{max}_{\mathrm{rr}}\left(\\frac{\mathcal{I}-\mu}{\sigma}\\right)$ - $t=%d$'%(round(ttt[1]),))
ax1.set_xlabel('$\Delta s\; [d_i]$')

for ax, seg in zip((ax0, ax1), MAX):
    pos = [val*ds for val in MAX[seg].keys()]
    data = list(MAX[seg].values())
    m = []
    s = []
    l = []
    h = []
    for d in data:
        m.append( np.mean(d) )
        s.append( np.std(d) )
        l.append( np.min(d) )
        h.append( np.max(d) )
    m = np.array(m)
    s = np.array(s)

    #_ = ax.violinplot(data, positions=pos)
    _ = ax.vlines(pos, l, h, lw=1, linestyles=':', color='black')
    _ = ax.vlines(pos, m-s, m+s, lw=4, color='blue', label='$\pm 1$ std. dev.')
    _ = ax.scatter(pos, m, s=50, color='blue', zorder=2, marker='s', label='mean')
    _ = ax.scatter(pos, l, s=50, color='black', zorder=3, marker='v', label='min')
    _ = ax.scatter(pos, h, s=50, color='black', zorder=4, marker='^', label='max')
    for th in ths:
        _ = ax.axhline(y=th, ls='--', lw=1, color='gray')

    if seg == '28':
        ax.legend()

yticks = ax1.yaxis.get_major_ticks()
yticks[-1].label1.set_visible(False)

plt.savefig(opath+'/'+out_dir+'/MAX_PVI_REC.png')
plt.close()

print 'DONE!'
