#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of DBz, E_parl, Jz and Je_perp.\n')

#------------------------------------------------------
#Init
#----
input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']

plt_show_flg = True
zoom_flg = True
rms_flg = False

Jperpe_flg = False

DBz = {}
Eparl = {}
Jz = {}
Jperpe = {}
Psi = {}
t = {}
x = {}
y = {}

run_name_vec = []
run_labels = {}
ind_dict = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind,_,_ = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    ind_dict[run_name] = ind
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    x_ = run.meta['x']
    y_ = run.meta['y']
    code_name = run.meta['code_name']
#
    if code_name == 'HVM':
        smooth_flag = False
        calc = fibo('calc')
        calc.meta = run.meta
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
        B0 = 1.
        n0 = 1.
        V0 = 1.
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
        gfsp = 2
        gfse = 2
        B0 = 0.01
        n0 = 1./(4.*np.pi)
        V0 = 0.01
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
#
    #get data
    #magnetic fields and co.
    E,B = run.get_EB(ind)
#
    Psi_ = wl.psi_2d(B[:-1,...,0],run.meta)
    if smooth_flag:
        Psi_ = gf(Psi_,[gfsp,gfsp],mode='wrap')
    Psi[run_name] = Psi_[ixmin:ixmax,iymin:iymax]/B0
    del Psi_
#
    DBz_ = B[2,:,:,0] - run.meta['Bz0']
    if smooth_flag:
        DBz_ = gf(DBz_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        DBz_rms = np.sqrt( np.mean( DBz_[ixmin:ixmax,iymin:iymax]*DBz_[ixmin:ixmax,iymin:iymax] ) )###
        DBz[run_name] = DBz_[ixmin:ixmax,iymin:iymax]/DBz_rms###
    else:
        DBz[run_name] = DBz_[ixmin:ixmax,iymin:iymax]/B0###
    del DBz_
#
    Eparl_ = np.divide(np.sum(np.multiply(E,B),axis=0),
                       np.sqrt(np.sum(np.multiply(B,B),axis=0)))[...,0]
    del E
    if smooth_flag:
        Eparl_ = gf(Eparl_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        Eparl_rms = np.sqrt(np.mean(Eparl_[ixmin:ixmax,iymin:iymax]*Eparl_[ixmin:ixmax,iymin:iymax]))
        Eparl[run_name] = Eparl_[ixmin:ixmax,iymin:iymax]/Eparl_rms
    else:
        Eparl_rms = np.sqrt(np.mean(Eparl_[ixmin:ixmax,iymin:iymax]*Eparl_[ixmin:ixmax,iymin:iymax]))
        Eparl[run_name] = Eparl_[ixmin:ixmax,iymin:iymax]/Eparl_rms
    del Eparl_
#
    n_p,u_p = run.get_Ion(ind)
    if code_name == 'HVM':
        Jx_,Jy_,Jz_ = calc.calc_curl(B[0],B[1],B[2])
        Je = np.empty(B.shape,dtype=type(B[0,0,0,0]))
        Je[0] = n_p*u_p[0] - Jx_
        Je[1] = n_p*u_p[1] - Jy_
        Je[2] = n_p*u_p[2] - Jz_
        del Jx_,Jy_
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
        Jz_ = n_p*u_p[2] - n_e*u_e[2]
        Je = np.empty(B.shape,dtype=type(B[0,0,0,0]))
        Je[0] = - n_e*u_e[0]
        Je[1] = - n_e*u_e[1]
        Je[2] = - n_e*u_e[2]
        del n_e,u_e
    del n_p,u_p
#
    Jz_ = Jz_[...,0]
    if smooth_flag:
        Jz_ = gf(Jz_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        Jz_rms = np.sqrt( np.mean( Jz_[ixmin:ixmax,iymin:iymax]*Jz_[ixmin:ixmax,iymin:iymax] ) )
        Jz[run_name] = Jz_[ixmin:ixmax,iymin:iymax]/Jz_rms
    else:
        Jz[run_name] = Jz_[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del Jz_
#
    if Jperpe_flg:
        Jparle = (Je[0]*B[0] + Je[1]*B[1] + Je[2]*B[2])/np.sqrt(B[0]*B[0] + B[1]*B[1] + B[2]*B[2])
        Jperpe_ = np.sqrt((Je[0]*Je[0] + Je[1]*Je[1] + Je[2]*Je[2]) - Jparle*Jparle)[...,0]
        del Jparle
    else:
        Jperpe_ = np.sqrt(Je[0]**2+Je[1]**2)[...,0]
    del Je,B
    if smooth_flag:
        Jperpe_ = gf(Jperpe_,[gfse,gfse],mode='wrap')
    if rms_flg:
        Jperpe_rms = np.sqrt( np.mean( 
                       Jperpe_[ixmin:ixmax,iymin:iymax]*Jperpe_[ixmin:ixmax,iymin:iymax] ) )
        Jperpe[run_name] = Jperpe_[ixmin:ixmax,iymin:iymax]/Jperpe_rms
    else:
        Jperpe[run_name] = Jperpe_[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del Jperpe_
#
    t[run_name] =  times[ind]
    del times
#
    x[run_name] = x_[ixmin:ixmax]
    y[run_name] = y_[iymin:iymax]
    del x_,y_
#
    gc.collect()

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
fig,ax = plt.subplots(4,3,figsize=(18,14),sharex=True,sharey=True)
fig.subplots_adjust(hspace=.06,wspace=.03,top=.95,bottom=.1,left=.055,right=1.075)#.5#1.07

vmin = ml.min_dict(DBz)
vmax = ml.max_dict(DBz)
vminDBz = -max(vmax,-vmin)
vmaxDBz = max(vmax,-vmin)

vmin = ml.min_dict(Eparl)
vmax = ml.max_dict(Eparl)
vminEparl = -max(vmax,-vmin)
vmaxEparl = max(vmax,-vmin)

vminJz = ml.min_dict(Jz)
vmaxJz = ml.max_dict(Jz)

vminJperpe = ml.min_dict(Jperpe)
vmaxJperpe = ml.max_dict(Jperpe)

for j,run_name in enumerate(run_name_vec):
    i = 0
    im0 = ax[i,j].pcolormesh(x[run_name],y[run_name],DBz[run_name].T,shading='gouraud',
                           vmin=vminDBz,vmax=vmaxDBz,cmap='PuOr')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[i,j].set_title(run_labels[run_name])
#
    i = 1
    im1 = ax[i,j].pcolormesh(x[run_name],y[run_name],Eparl[run_name].T,shading='gouraud',
                             cmap='seismic',
                             vmin=vminEparl,vmax=vmaxEparl)
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
#
    i = 2
    im2 = ax[i,j].pcolormesh(x[run_name],y[run_name],Jz[run_name].T,shading='gouraud',
                             vmin=vminJz,vmax=vmaxJz,cmap='Reds_r')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
#
    i = 3
    im3 = ax[i,j].pcolormesh(x[run_name],y[run_name],Jperpe[run_name].T,shading='gouraud',
                          vmin=vminJperpe,vmax=vmaxJperpe,cmap='hot')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='white')
    ax[i,j].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')

cb0 = plt.colorbar(im0,ax=ax[0,:],pad=.007)
if rms_flg:
    label_ = '$\Delta B_z / \overline{B_z}$'
else:
    label_ = '$\Delta B_z / B_0$'
cb0.set_label(label_,rotation=270,labelpad=20)#20)

cb1 = plt.colorbar(im1,ax=ax[1,:],pad=.007)
if rms_flg:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
else:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
cb1.set_label(label_,rotation=270,labelpad=35)#40)

cb2 = plt.colorbar(im2,ax=ax[2,:],pad=.007)
if rms_flg:
    label_ = '$J_z / \overline{J_z}$'
else:
    label_ = '$J_z / ( e n_0 v_A )$'
cb2.set_label(label_,rotation=270,labelpad=25)

cb3 = plt.colorbar(im3,ax=ax[3,:],pad=.007)
if Jperpe_flg:
    if rms_flg:
        label_ = '$J_{\perp,\mathrm{e}} / \overline{J_{\perp,\mathrm{e}}}$'
    else:
        label_ = '$J_{\perp,\mathrm{e}} / ( e n_0 v_A )$'
else:
    if rms_flg:
        label_ = '$J_{\mathrm{e}}^{(\\text{plane})} / \overline{J_{\mathrm{e}}^{(\\text{plane})}}$'
    else:
        label_ = '$J_{\mathrm{e}}^{(\\text{plane})} / ( e n_0 v_A )$'
cb3.set_label(label_,rotation=270,labelpad=45)#40)

if plt_show_flg:
    plt.show()

out_dir = 'plot_2d_fields_v2'
ml.create_path(opath+'/'+out_dir)
out_dir += '/comp'
fig_name = 'DBz_'
fig_name += 'Eparl_'
fig_name += 'Jz_'
if Jperpe_flg:
    fig_name += 'Jperpe'
else:
    fig_name += 'Jeplane'
for run_name in run_name_vec:
    out_dir += '__' + run_name
    fig_name += '__' + run_labels[run_name] + '_ind%d'%ind_dict[run_name]

if zoom_flg:
    fig_name += '__zoom'
if rms_flg:
    fig_name += '__rms'
fig_name += '.png'
ml.create_path(opath+'/'+out_dir)
fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
