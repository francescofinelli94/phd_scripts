#-----------------------------------------------------
#importing stuff
#---------------
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
import gc
import glob
from scipy.optimize import curve_fit
from scipy.ndimage import gaussian_filter as gf
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot X-point in 2D space\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
out_dir = 'Xpoint_inspace/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']

ml.create_path(opath+'/'+out_dir)

ipath = opath+'/'+'Psi_field/'+run_name+'/tracked_points'

#read files
xfiles = glob.glob(ipath+'/xpoint_from_*.dat')
ofiles = glob.glob(ipath+'/opoint_from_*.dat')

xnum = len(xfiles)
onum = len(ofiles)

t_ = {}
x_ = {}
y_ = {}
v_ = {}
for i in range(xnum):
    f = open(xfiles[i],'r')
    ll = f.readlines()
    f.close()

    nt = len(ll)
    t_[xfiles[i]] = np.empty(nt,dtype=np.float);   t0 = t_[xfiles[i]][0]
    x_[xfiles[i]] = np.empty(nt,dtype=np.long);    x0 = x_[xfiles[i]][0]
    y_[xfiles[i]] = np.empty(nt,dtype=np.long);    y0 = y_[xfiles[i]][0]
    v_[xfiles[i]] = np.empty(nt,dtype=np.float64); v0 = v_[xfiles[i]][0]
    for j in range(nt):
        l = ll[j].split()
        t_[xfiles[i]][j] = ml.generic_cast(l[0],t0)
        x_[xfiles[i]][j] = ml.generic_cast(l[1],x0)
        y_[xfiles[i]][j] = ml.generic_cast(l[2],y0)
        v_[xfiles[i]][j] = ml.generic_cast(l[3],v0)

for i in range(onum):
    f = open(ofiles[i],'r')
    ll = f.readlines()
    f.close()
    
    nt = len(ll)
    t_[ofiles[i]] = np.empty(nt,dtype=np.float);   t0 = t_[ofiles[i]][0]
    x_[ofiles[i]] = np.empty(nt,dtype=np.long);    x0 = x_[ofiles[i]][0]
    y_[ofiles[i]] = np.empty(nt,dtype=np.long);    y0 = y_[ofiles[i]][0]
    v_[ofiles[i]] = np.empty(nt,dtype=np.float64); v0 = v_[ofiles[i]][0]
    for j in range(nt):
        l = ll[j].split()
        t_[ofiles[i]][j] = ml.generic_cast(l[0],t0)
        x_[ofiles[i]][j] = ml.generic_cast(l[1],x0)
        y_[ofiles[i]][j] = ml.generic_cast(l[2],y0)
        v_[ofiles[i]][j] = ml.generic_cast(l[3],v0)

#plot all
plt.close()
for i in range(xnum):
    plt.plot(x_[xfiles[i]],t_[xfiles[i]],label='X'+str(i))
for i in range(onum):
    plt.plot(x_[ofiles[i]],t_[ofiles[i]],'--',label='O'+str(i))
plt.legend()
plt.xlabel('x');plt.ylabel('t');plt.title('Tracked singular points')
plt.show()
plt.close()

#choose points
ixp = ml.secure_input('Choose X-point (0 -> '+str(xnum-1)+'): ',10,True)
iop = ml.secure_input('Choose O-point (0 -> '+str(onum-1)+'): ',10,True)
if ( (len(t_[xfiles[ixp]]) != len(t_[ofiles[iop]])) or 
     (t_[xfiles[ixp]][0] != t_[ofiles[iop]][0]) or (t_[xfiles[ixp]][-1] != t_[ofiles[iop]][-1]) ):
    print('ERROR: X-point and O-point are tracked for different time ranges')
    sys.exit()

#store data
Xt = t_[xfiles[ixp]]
Xv = v_[xfiles[ixp]]
Xx = x_[xfiles[ixp]]
Xy = y_[xfiles[ixp]]

Ot = t_[ofiles[iop]]
Ov = v_[ofiles[iop]]
Ox = x_[ofiles[iop]]
Oy = y_[ofiles[iop]]

#---> loop over times <---
print " ",
for i_ in range(len(Xt)):
#-------------------------
    t_ = Xt[i_]
    ind = np.argmin(np.abs(time - t_))
    Xv_ = Xv[i_]
    if t_ in Ot:
        Oflag = True
        Ov_ = Ov[i_]
    else:
        Oflag =  False

    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)

    # -> By,Bz
    plt.close()
    fig,ax0 = plt.subplots(1,1,figsize=(15,5))

    im0 = ax0.contourf(x[ixmin:ixmax],y[iymin:iymax],B[2,ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im0,ax=ax0)
    ax0.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='inferno')
    ax0.scatter([x[Xx[i_]]],[y[Xy[i_]]],c='black',marker='X')
    if Oflag:
        ax0.scatter([x[Ox[i_]]],[y[Oy[i_]]],c='black',marker='o')
        ax0.set_title('$%s \quad t=%f\Omega_C^{-1} \quad \Delta_X=%f \quad \Delta_O=%f$'%(run_name.replace('_',' '),t_,Xv_-Psi[Xx[i_],Xy[i_]],Ov_-Psi[Ox[i_],Oy[i_]]))
    else:
        ax0.set_title('$t=%f\Omega_C^{-1} \quad \Delta_X=%f$'%(t_,Xv_-Psi[Xx[i_],Xy[i_]]))
    ax0.set_xlabel('x')
    ax0.set_ylabel('y')


    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'Xpoint_'+run_name+'_'+str(ind)+'.png')
    plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
