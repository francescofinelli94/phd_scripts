261,278c260,267
<     #plt.subplots_adjust(bottom=0.)
<     #for i,l in zip(range(len(ll)),ll):
<     #    ax0.plot(sco[l//2:-(1+l//2)],PVI_sc[:,i][l//2:-(1+l//2)])
<     #ax0.plot(sco,Jn_an_sc,color='black',label='$|J|$ normalized')
<     ax0.plot(sco,Jn_an_sc,color='green')
<     ax0.plot(sco,intPVI_sc,color='blue')
<     for s in sco[rec_ind[:]]:
<         ax0.axvspan(s-ds*0.5,s+ds*0.50,alpha=1.,color='red',lw=3)
<     #ax0.plot(sco[rec_ind[:]],Jn_an_sc[rec_ind[:]],'*b')
<     ax0.axhline(y=thJn,linestyle='--',color='green')
<     ax0.axhline(y=thPVI,linestyle='--',color='blue')
<     #ax0.tick_params(bottom=False,top=True,left=True,right=False,
<     #        direction='out',labelbottom=False,labeltop=True,
<     #        labelleft=True,labelright=False)
<     #ax0.xaxis.set_label_position('top')
<     #ax0.set_xlabel('s [$d_i$]')
<     ax0.set_ylabel('$|J|,\;PVI$')
<     #ax0.legend()
---
>     for i,l in zip(range(len(ll)),ll):
>         ax0.plot(sco[l//2:-(1+l//2)],PVI_sc[:,i][l//2:-(1+l//2)])
>     ax0.plot(sco,Jn_an_sc,color='black',label='$|J|$ normalized')
>     ax0.plot(sco[rec_ind[:]],Jn_an_sc[rec_ind[:]],'*b')
>     ax0.axhline(y=thJn,linestyle='--',color='darkgoldenrod')
>     ax0.set_xlabel('s [$d_i$]')
>     ax0.set_ylabel('$PVI$ normalized')
>     ax0.legend()
288,290c276,278
<     ax1.scatter(xco[rec_ind[:]],yco[rec_ind[:]],s=110,c='red')
<     ax1.scatter(xco[Jn_an_sc>thJn],yco[Jn_an_sc>thJn],s=65,c='green')
<     ax1.scatter(xco[intPVI_sc>thPVI],yco[intPVI_sc>thPVI],s=20,c='blue')
---
>     ax1.scatter(xco[rec_ind[:]],yco[rec_ind[:]],s=110,c='blue')
>     ax1.scatter(xco[Jn_an_sc>thJn],yco[Jn_an_sc>thJn],s=75,c='darkgoldenrod')
>     ax1.scatter(xco[intPVI_sc>thPVI],yco[intPVI_sc>thPVI],s=40,c='green')
293c281
<     #ax1.set_title('$|J|$ and peaks')
---
>     ax1.set_title('$|J|$ and peaks')
297,303c285,291
<     ## -> 2D: PVI scalogram 
<     #ax2 = fig.add_subplot(gs[0:4,4:8])
<     #ax2.contourf(sco[i0:i1],ll,PVI[i0:i1,:].T,63)
<     #ax2.set_ylabel('scales [$ds='+str(round(ds,4))+'\;d_i$]')
<     #ax2.set_title('$PVI$ scalogram')
<     #ax2.set_xlim(sco[i0],sco[i1])
<     #ax2.set_ylim(ll[0],ll[-1])
---
>     # -> 2D: PVI scalogram 
>     ax2 = fig.add_subplot(gs[0:4,4:8])
>     ax2.contourf(sco[i0:i1],ll,PVI[i0:i1,:].T,63)
>     ax2.set_ylabel('scales [$ds='+str(round(ds,4))+'\;d_i$]')
>     ax2.set_title('$PVI$ scalogram')
>     ax2.set_xlim(sco[i0],sco[i1])
>     ax2.set_ylim(ll[0],ll[-1])
306,312c294,300
<     #ax4 = fig.add_subplot(gs[4,4:8])
<     #ax4.plot(sco,intPVI_sc,'g',label='int. $PVI$ (norm.)')
<     #ax4.axhline(y=thPVI,linestyle='--',color='green')
<     #ax4.set_xlabel('s [$d_i$]')
<     #ax4.legend()
<     #ax4.set_xlim(sco[i0],sco[i1])
<     #ax4.set_ylim(0.,1.)
---
>     ax4 = fig.add_subplot(gs[4,4:8])
>     ax4.plot(sco,intPVI_sc,'g',label='int. $PVI$ (norm.)')
>     ax4.axhline(y=thPVI,linestyle='--',color='green')
>     ax4.set_xlabel('s [$d_i$]')
>     ax4.legend()
>     ax4.set_xlim(sco[i0],sco[i1])
>     ax4.set_ylim(0.,1.)
314,315c302
<     #fig.suptitle(run_name.replace('_',' ')+' s%s a%d'%(seg,ang)+th_sup)
<     fig.suptitle('$|J|$ (green), PVI scale-int. (blue), MR (red)')
---
>     fig.suptitle(run_name.replace('_',' ')+' s%s a%d'%(seg,ang)+th_sup)
