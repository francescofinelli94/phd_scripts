#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import pandas as pd
import glob
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

#------------------------------------------------------
#Intent
#------
print('\nWill plot already computed enropies\n')

#------------------------------------------------------
#Init
#----
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

opath = '/work2/finelli/Outputs/entropy_v4'

cls = {'fp':'orangered','fe':'dodgerblue','gp':'coral','ge':'mediumspringgreen'}

#------------------------------------------------------
#read
#----
all_file_ext = ['csv','CSV','tsv','TSV','txt','TXT','dat','DAT']
df_files = []
for ext in all_file_ext:
    df_files += glob.glob(opath+'/*/'+'entr_*.'+ext)

print('\nfiles:')
for n,f in zip(range(len(df_files)),df_files):
    print('%d -> %s in %s'%(n,f.split('/')[-1],f.split('/')[-2]))

n = int(input('Choose a file (insert the corresponding number): '))
df_file = df_files[n]

file_ext = df_file.split('.')[-1]
if (file_ext == 'csv') or (file_ext == 'CSV'):
    sep = ','
elif (file_ext == 'tsv') or (file_ext == 'TSV'):
    sep = '\t'
elif (file_ext == 'txt') or (file_ext == 'TXT'):
    sep = '\t'
elif (file_ext == 'dat') or (file_ext == 'DAT'):
    sep = '\t'
else:
    sep = '\t'

df = pd.read_csv(df_file,header=0,sep=sep)

tmp = df_file.split('/')[-1].split('_')[1]
if tmp == 'HVM':
    run_label = 'HVM'
    code_name = 'HVM'
elif tmp == 'LF':
    run_label = 'HVLF'
    code_name = 'HVM'
elif tmp == 'DH':
    run_label = 'iPIC'
    code_name = 'iPIC'
else:
    run_label = 'unknown'
    code_name = 'unknown'
    print('\nWARNING: code name is unknown')
    #sys.exit()

if 'entr_ge' not in df.columns:
    df['entr_ge'] = np.zeros(len(df['entr_gp'].values))
    df['adv_ge']  = np.zeros(len(df['entr_gp'].values))
    df['nPiD_ge'] = np.zeros(len(df['entr_gp'].values))
    df['vhf_ge']  = np.zeros(len(df['entr_gp'].values))
    df['ng_ge']   = np.zeros(len(df['entr_gp'].values))

t = df['time'].values
if code_name == 'iPIC':
    t *= 100.

dt = t[1] - t[0]

df['rate_fp'] = savgol_filter(df['entr_fp'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='nearest')
df['rate_gp'] = savgol_filter(df['entr_gp'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='nearest')
df['rate_fe'] = savgol_filter(df['entr_fe'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='nearest')
df['rate_ge'] = savgol_filter(df['entr_ge'],window_length=9,polyorder=2,deriv=1,delta=dt,mode='nearest')

df['diff_fp'] = df['rate_fp'] - (df['adv_fp'] + df['gPiD_fp'] + df['nPiD_fp'] + df['vhf_fp'])
df['diff_gp'] = df['rate_gp'] - (df['adv_gp'] + df['nPiD_gp'] + df['vhf_gp']  + df['ng_gp'] )
df['diff_fe'] = df['rate_fe'] - (df['adv_fe'] + df['gPiD_fe'] + df['nPiD_fe'] + df['vhf_fe'])
df['diff_ge'] = df['rate_ge'] - (df['adv_ge'] + df['nPiD_ge'] + df['vhf_ge']  + df['ng_ge'] )

if code_name == 'iPIC':
    t /= 100.
    df.loc[:,[x for x in df.columns 
              if x not in ['time','entr_fp','entr_gp','entr_fe','entr_ge']]] *= 100.

#------------------------------------------------------
#plot
#----
plt.close('all')

# -> plot 0
#           entr_all  PiD_all vhf_all
nrow = 2
ncol = 3
fig,ax = plt.subplots(nrow,ncol,figsize=(6*ncol,5*nrow))

print(df['entr_ge'][0])

ax[0,0].plot(t,-(df['entr_fp']/df['entr_fp'][0]-1.),label='$S_{f,p}$',color=cls['fp'])
ax[0,0].plot(t,-(df['entr_gp']/df['entr_gp'][0]-1.),label='$S_{g,p}$',color=cls['gp'])
ax[0,0].plot(t,-(df['entr_fe']/df['entr_fe'][0]-1.),label='$S_{f,e}$',color=cls['fe'])
ax[0,0].plot(t,-(df['entr_ge']/df['entr_ge'][0]-1.),label='$S_{g,e}$',color=cls['ge'])
ax[0,0].legend(ncol=2)
ax[0,0].set_xlabel('t [$\Omega_c^{-1}$]')
ax[0,0].set_ylabel('$\Delta S/|S(t=0)|$')
ax[0,0].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

ax[0,1].plot(t,df['rate_fp'],label='$\partial_tS_{f,p}$',color=cls['fp'])
ax[0,1].plot(t,df['rate_gp'],label='$\partial_tS_{g,p}$',color=cls['gp'])
ax[0,1].plot(t,df['rate_fe'],label='$\partial_tS_{f,e}$',color=cls['fe'])
ax[0,1].plot(t,df['rate_ge'],label='$\partial_tS_{g,e}$',color=cls['ge'])
ax[0,1].legend(ncol=2)
ax[0,1].set_xlabel('t [$\Omega_c^{-1}$]')
ax[0,1].set_ylabel('Rate of entropy change')
ax[0,1].set_title(run_label)
ax[0,1].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

ax[0,2].plot(t,df['adv_fp'],label='$Adv_{f,p}$',color=cls['fp'])
ax[0,2].plot(t,df['adv_gp'],label='$Adv_{g,p}$',color=cls['gp'])
ax[0,2].plot(t,df['ng_gp'],label='$Ng_{g,p}$',color=cls['gp'],linestyle='--')
ax[0,2].plot(t,df['adv_fe'],label='$Adv_{f,e}$',color=cls['fe'])
ax[0,2].plot(t,df['adv_ge'],label='$Adv_{g,e}$',color=cls['ge'])
ax[0,2].plot(t,df['ng_ge'],label='$Ng_{g,e}$',color=cls['ge'],linestyle='--')
ax[0,2].legend(ncol=2)
ax[0,2].set_xlabel('t [$\Omega_c^{-1}$]')
ax[0,2].set_ylabel('Advection contribution')
ax[0,2].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

ax[1,0].plot(t,df['vhf_fp'],label='$q_{f,p}$',color=cls['fp'])
ax[1,0].plot(t,df['vhf_gp'],label='$q_{g,p}$',color=cls['gp'])
ax[1,0].plot(t,df['vhf_fe'],label='$q_{f,e}$',color=cls['fe'])
ax[1,0].plot(t,df['vhf_ge'],label='$q_{g,e}$',color=cls['ge'])
ax[1,0].legend(ncol=2)
ax[1,0].set_xlabel('t [$\Omega_c^{-1}$]')
ax[1,0].set_ylabel('Vector heat flux contribution')
ax[1,0].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

ax[1,1].plot(t,df['gPiD_fp'],label='$PiD^{(g)}_{f,p}$',color=cls['fp'])
ax[1,1].plot(t,df['nPiD_fp'],label='$PiD^{(n)}_{f,p}$',color=cls['fp'],linestyle='--')
ax[1,1].plot(t,df['nPiD_gp'],label='$PiD^{(n)}_{g,p}$',color=cls['gp'],linestyle='--')
ax[1,1].plot(t,df['gPiD_fe'],label='$PiD^{(g)}_{f,e}$',color=cls['fe'])
ax[1,1].plot(t,df['nPiD_fe'],label='$PiD^{(n)}_{f,e}$',color=cls['fe'],linestyle='--')
ax[1,1].plot(t,df['nPiD_ge'],label='$PiD^{(n)}_{g,e}$',color=cls['ge'],linestyle='--')
ax[1,1].legend(ncol=2)
ax[1,1].set_xlabel('t [$\Omega_c^{-1}$]')
ax[1,1].set_ylabel('PiD contribution')
ax[1,1].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

ax[1,2].plot(t,df['diff_fp'],label='$\Delta_{f,p}$',color=cls['fp'])
ax[1,2].plot(t,df['diff_gp'],label='$\Delta_{g,p}$',color=cls['gp'])
ax[1,2].plot(t,df['diff_fe'],label='$\Delta_{f,e}$',color=cls['fe'])
ax[1,2].plot(t,df['diff_ge'],label='$\Delta_{g,e}$',color=cls['ge'])
ax[1,2].legend(ncol=2)
ax[1,2].set_xlabel('t [$\Omega_c^{-1}$]')
ax[1,2].set_ylabel('Non-gyrotropic contribution')
ax[1,2].ticklabel_format(axis='y',style='sci',scilimits=(-1,2))

fig.tight_layout()
#fig.show()
plt.savefig(df_file+'.png')
plt.close()
