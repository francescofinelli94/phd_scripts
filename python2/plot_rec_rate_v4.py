#-----------------------------------------------------
#importing stuff
#---------------
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.patches import Rectangle as mplRect
import work_lib as wl
import gc
import glob
from scipy.optimize import curve_fit
from scipy.ndimage import gaussian_filter as gf
from scipy.signal import savgol_filter
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot reconnection rate/flux\n')

#------------------------------------------------------
#Init
#------
opath = '/home/finelli/Downloads'
print('opath is set to:')
print(opath)

#insert run names
#run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run7_data0']
#run_label_vec = ['HVM','HVLF','iPIC']
#choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run7_data0':[0,3]}

#run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run8']
#run_label_vec = ['HVM','HVLF','iPIC']
#choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run8':[0,9]}

#run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run9']
#run_label_vec = ['HVM','HVLF','iPIC']
#choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run9':[3,6]}

#run_name_vec = ['DH_run7_data0','DH_run8','DH_run9']
#run_label_vec = ['iPIC 7','iPIC 8','iPIC 9']
#choosen_points = {'DH_run7_data0':[0,3],'DH_run8':[0,9],'DH_run9':[3,6]}

#run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run8','DH_run10']
#run_label_vec = ['HVM','HVLF','iPIC','iPIC10']
#choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run8':[0,9],'DH_run10':[2,1]}

run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run11_data0']
run_label_vec = ['HVM','HVLF','iPIC']
choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run11_data0':[0,3]}

iPIC_run = []
for run_name in run_name_vec:
    if run_name.split('_')[0] == 'DH':
        iPIC_run.append(run_name)

print('\nrun_name_vec is set to:')
print(run_name_vec)

#compute normalization #HARDCODED!!!!!!
all_B0 = {}
all_tmult = {}
for run_name in run_name_vec:
    all_B0[run_name] = [1.,0.,0.25]
    if run_name in iPIC_run:
        all_B0[run_name] = np.divide(all_B0[run_name],100.)
    all_tmult[run_name] = 1.
    if run_name in iPIC_run:
        all_tmult[run_name] = all_tmult[run_name]*0.01

print('\nNormalizations are:')
print('0 -> Bx*Bx/sqrt(4*pi*ni*mi)')
print('1 -> Bx*B/sqrt(4*pi*ni*mi)')
print('2 -> B*B/sqrt(4*pi*ni*mi)')
#inorm = ml.secure_input('choose normalization: ',10,True)
inorm = 0
print('norm -> 0 is selected\n')

#data struct init
time = {}
flux = {}
rate = {}
Xx = {}
Ox = {}

for run_name in run_name_vec:
    out_dir = 'Psi_field/'+run_name
    ipath = opath+'/'+'Psi_field/'+run_name+'/tracked_points'
#
    #read files
    xfiles = glob.glob(ipath+'/xpoint_from_*.dat')
    ofiles = glob.glob(ipath+'/opoint_from_*.dat')
#
    xnum = len(xfiles)
    onum = len(ofiles)
#
    t = {}
    x = {}
    y = {}
    v = {}
    for i in range(xnum):
        f = open(xfiles[i],'r')
        ll = f.readlines()
        f.close()
#
        nt = len(ll)
        t[xfiles[i]] = np.empty(nt,dtype=np.float);   t0 = t[xfiles[i]][0]
        x[xfiles[i]] = np.empty(nt,dtype=np.long);    x0 = x[xfiles[i]][0]
        y[xfiles[i]] = np.empty(nt,dtype=np.long);    y0 = y[xfiles[i]][0]
        v[xfiles[i]] = np.empty(nt,dtype=np.float64); v0 = v[xfiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t[xfiles[i]][j] = ml.generic_cast(l[0],t0)
            x[xfiles[i]][j] = ml.generic_cast(l[1],x0)
            y[xfiles[i]][j] = ml.generic_cast(l[2],y0)
            v[xfiles[i]][j] = ml.generic_cast(l[3],v0)
#
    for i in range(onum):
        f = open(ofiles[i],'r')
        ll = f.readlines()
        f.close()
#
        nt = len(ll)
        t[ofiles[i]] = np.empty(nt,dtype=np.float);   t0 = t[ofiles[i]][0]
        x[ofiles[i]] = np.empty(nt,dtype=np.long);    x0 = x[ofiles[i]][0]
        y[ofiles[i]] = np.empty(nt,dtype=np.long);    y0 = y[ofiles[i]][0]
        v[ofiles[i]] = np.empty(nt,dtype=np.float64); v0 = v[ofiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t[ofiles[i]][j] = ml.generic_cast(l[0],t0)
            x[ofiles[i]][j] = ml.generic_cast(l[1],x0)
            y[ofiles[i]][j] = ml.generic_cast(l[2],y0)
            v[ofiles[i]][j] = ml.generic_cast(l[3],v0)
#
    if choosen_points[run_name] == None:
        #plot all
        plt.close()
        for i in range(xnum):
            plt.plot(x[xfiles[i]],t[xfiles[i]],label='X'+str(i))
        for i in range(onum):
            plt.plot(x[ofiles[i]],t[ofiles[i]],'--',label='O'+str(i))
        plt.legend()
        plt.xlabel('x');plt.ylabel('t');plt.title('Tracked singular points')
        plt.show()
        plt.close()
#
        #choose points
        ixp = ml.secure_input('Choose X-point (0 -> '+str(xnum-1)+'): ',10,True)
        iop = ml.secure_input('Choose O-point (0 -> '+str(onum-1)+'): ',10,True)
        #if ( (len(t[xfiles[ixp]]) != len(t[ofiles[iop]])) or 
        #     (t[xfiles[ixp]][0] != t[ofiles[iop]][0]) or (t[xfiles[ixp]][-1] != t[ofiles[iop]][-1]) ):
        #    print('ERROR: X-point and O-point are tracked for different time ranges')
        #    sys.exit()
    else:
        ixp = choosen_points[run_name][0]
        iop = choosen_points[run_name][1]
#
    #store data
    Xx[run_name] = x[xfiles[ixp]]
    Ox[run_name] = x[ofiles[iop]]
    ERR_FLG = False
    if len(t[xfiles[ixp]]) != len(t[ofiles[iop]]):
        ERR_FLG = True
    if np.max(np.abs(t[xfiles[ixp]]-t[ofiles[iop]])) > 1.e-15:
        ERR_FLG = True
    if ERR_FLG:
        print('ERROR: X-point and O-point time series for %s are not equal!'%(run_name))
        sys.exit()
    time[run_name] = t[xfiles[ixp]]
    if inorm < 2:
        flux[run_name] = np.divide(v[ofiles[iop]] - v[xfiles[ixp]],all_B0[run_name][0])
    else:
        flux[run_name] = np.divide(v[ofiles[iop]] - v[xfiles[ixp]],np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2))
    flux[run_name] = flux[run_name] - flux[run_name][0]
    #if inorm == 0:
    #    rate[run_name] = np.divide(np.gradient(flux[run_name],time[run_name]),all_B0[run_name][0]/all_tmult[run_name])
    #else:
    #    rate[run_name] = np.divide(np.gradient(flux[run_name],time[run_name]),np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2)/all_tmult[run_name])
    tmp = savgol_filter(flux[run_name],window_length=15,polyorder=2,deriv=1,
                        delta=time[run_name][1]-time[run_name][0],mode='nearest')
    if inorm == 0:
        rate[run_name] = np.divide(tmp,all_B0[run_name][0]/all_tmult[run_name])
    else:
        rate[run_name] = np.divide(tmp,np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2)/all_tmult[run_name])

#plot selected
plt.close()
for run_name,run_label in zip(run_name_vec,run_label_vec):
    plt.plot(Xx[run_name],time[run_name],label='X %s'%(run_label))
    plt.plot(Ox[run_name],time[run_name],'--',label='O %s'%(run_label))

plt.legend()
plt.xlabel('x');plt.ylabel('t');plt.title('Tracked singular points')
plt.show()
plt.close()

#------------------------------------------------------------------------

#final plot
for run_name in run_name_vec:
    flux[run_name] = gf(flux[run_name],2,mode='nearest')
#   rate[run_name] = gf(rate[run_name],2,mode='nearest')

#------------------------

def on_pick(event):
    if event.mouseevent.key != "control":
        return
    global x_,y_,i_,tbu
    line = event.artist
    if len(x_[line]) == 3:
        print('selection for %s is full'%(line))
        return
    xdata,ydata = line.get_data()
    ind = event.ind
    last.append(line)
    print('x=%f\ty=%f\ton %s'%(xdata[ind[0]],ydata[ind[0]],line))
    x_[line].append(xdata[ind[0]])
    y_[line].append(ydata[ind[0]])
    i_[line].append(ind[0])
    tbu[line].set_xdata(x_[line])
    tbu[line].set_ydata(y_[line])
    return

def on_key_press(event):
    global fig,tbu,x_,y_,i_
    if event.key == "escape":
        global cid0,cid1,tbu,lines
        for line in lines:
            if len(x_[line]) < 3:
                print('selection incomplete for %s'%(line))
                return
        for line in lines:
            tbu[line].set_xdata([])
            tbu[line].set_ydata([])
        fig.canvas.mpl_disconnect(cid0)
        fig.canvas.mpl_disconnect(cid1)
        fig.canvas.stop_event_loop()
    elif event.key == "ctrl+r":
        fig.canvas.draw_idle()
    elif event.key == "ctrl+z":
        global last
        if len(last) == 0:
            return
        del x_[last[-1]][-1]
        del y_[last[-1]][-1]
        del i_[last[-1]][-1]
        tbu[last[-1]].set_xdata(x_[last[-1]])
        tbu[last[-1]].set_ydata(y_[last[-1]])
        print('removed last point from %s'%(last[-1]))
        del last[-1]
    return

#------------------------

print('\nHOW-TO:')
print(' -> ctrl+mousclick on a line to select a data point')
print(' -> ctrl+z to remove the last point')
print(' -> ctrl+r to refresh the plot if stale (should not happen)')
print(' -> press Esc to exit interactive mode and generate inserts')
print(' -> choose exactly 3 points per line')
print(' -> for each line, the middel point is the one used to shift')
print('    lines in the insert (in each insert, all middel point are alignd)')
print(' -> for each line, first and last point are guidelines for insert axes limits')

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(16,8))
ax = fig.subplots(1,2)

lines = []
lines_0 = []
lines_1 = []
last = []
for run_name,run_label in zip(run_name_vec,run_label_vec):
    lin0, = ax[0].plot(time[run_name],flux[run_name],picker=5,label=run_label+' flux')
    lin1, = ax[1].plot(time[run_name],rate[run_name],picker=5,label=run_label+' rate')
    lines.append(lin0)
    lines.append(lin1)
    lines_0.append(lin0)
    lines_1.append(lin1)

ax[0].set_xlabel('$t\quad [\Omega_{\mathrm{p}}^{-1}]$')
ax[0].set_ylabel('$F/(B_0 d_{\mathrm{p}})$')
ax[0].set_title('Reconnected fluxes')
ax[0].legend()
ax[0].set_yscale('log')
ax[1].set_xlabel('$t\quad [\Omega_{\mathrm{p}}^{-1}]$')
ax[1].set_ylabel('$R/(B_0 v_A)$')
ax[1].set_title('Reconnection rates')
ax[1].legend()
ax[1].set_yscale('log')
fig.tight_layout()

print('\n')
x_ = {}
y_ = {}
i_ = {}
tbu = {}
for line in lines_0:
    x_[line] = []
    y_[line] = []
    i_[line] = []
    tbu[line], = ax[0].plot([],[],'o',color=line.get_color())

for line in lines_1:
    x_[line] = []
    y_[line] = []
    i_[line] = []
    tbu[line], = ax[1].plot([],[],'o',color=line.get_color())

cid0 = fig.canvas.mpl_connect('pick_event',on_pick)
cid1 = fig.canvas.mpl_connect('key_press_event',on_key_press)
fig.canvas.start_event_loop(timeout=-1)

xc_0 = np.max(np.array(x_.values()))
for line in lines_0:
    y_[line] = [xx for _,xx in sorted(zip(x_[line],y_[line]))]
    i_[line] = [xx for _,xx in sorted(zip(x_[line],i_[line]))]
    x_[line] = sorted(x_[line])
    if x_[line][1] < xc_0:
        xc_0 = x_[line][1]

xc_1 = np.max(np.array(x_.values()))
for line in lines_1:
    y_[line] = [xx for _,xx in sorted(zip(x_[line],y_[line]))]
    i_[line] = [xx for _,xx in sorted(zip(x_[line],i_[line]))]
    x_[line] = sorted(x_[line])
    if x_[line][1] < xc_1:
        xc_1 = x_[line][1]

Dx = {}
x_s = {}
for line in lines:
    if line in lines_0:
        Dx[line] = x_[line][1] - xc_0
    elif line in lines_1:
        Dx[line] = x_[line][1] - xc_1
    x_s[line] = np.array(x_[line]) - Dx[line]

axins0 = inset_axes(ax[0],width="45%",height="40%",loc=4)
axins1 = inset_axes(ax[1],width="45%",height="40%",loc=4)

xlim_0 = [np.min([np.min(x_s[line]) for line in lines_0]),
          np.max([np.max(x_s[line]) for line in lines_0])]
ylim_0 = [np.min([np.min(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_0]),
          np.max([np.max(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_0])]
axins0.set_xlim(xlim_0[0],xlim_0[1])
axins0.set_ylim(ylim_0[0],ylim_0[1])
axins0.set_yscale('log')
axins0.set_xticks([])

xlim_1 = [np.min([np.min(x_s[line]) for line in lines_1]),
          np.max([np.max(x_s[line]) for line in lines_1])]
ylim_1 = [np.min([np.min(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_1]),
          np.max([np.max(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_1])]
axins1.set_xlim(xlim_1[0],xlim_1[1])
axins1.set_ylim(ylim_1[0],ylim_1[1])
axins1.set_yscale('log')
axins1.set_xticks([])

for line in lines_0:
    xdata,ydata = line.get_data()
    axins0.plot(xdata-Dx[line],ydata,color=line.get_color())
    inside = np.logical_and(np.logical_and(xlim_0[0]<=xdata-Dx[line],xdata-Dx[line]<=xlim_0[1]),
                            np.logical_and(ylim_0[0]<=ydata,ydata<=ylim_0[1]))
    ax[0].plot(xdata[inside],ydata[inside],color=line.get_color(),linewidth=3.0)

for line in lines_1:
    xdata,ydata = line.get_data()
    axins1.plot(xdata-Dx[line],ydata,color=line.get_color())
    inside = np.logical_and(np.logical_and(xlim_1[0]<=xdata-Dx[line],xdata-Dx[line]<=xlim_1[1]),
                            np.logical_and(ylim_1[0]<=ydata,ydata<=ylim_1[1]))
    ax[1].plot(xdata[inside],ydata[inside],color=line.get_color(),linewidth=3.0)

plt.ioff()
plt.show(1)
#plt.savefig(opath+'/'+'Psi_field/rec_flux_and_rate.png')
plt.close(1)
