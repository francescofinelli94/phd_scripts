#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
#from scipy import stats
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nIs there a linear phase?\n')

#------------------------------------------------------
#Init
#----
#input_files = ['LF_2d_DH.dat','DH_run2_data3.dat','DH_run3_data0.dat',
#               'DH_run5_data0.dat','DH_run7_data0.dat','DH_run8.dat',
#               'DH_run9.dat','DH_run10.dat','DH_run11_data0.dat']
input_files = ['LF_2d_DH.dat','DH_run5_data0.dat','DH_run7_data0.dat','DH_run8.dat',
               'DH_run10.dat','DH_run11_data0.dat']
#input_files = ['LF_2d_DH.dat','DH_run8.dat']

master_in = 0
nmod = 9

Eparl_x0 = {}
y_cut = {}
fftBy = {}
fftBz = {}
fftEy = {}
fftEz = {}
t = {}
dt = {}
times_dict = {}

run_name_vec = []
B0 = {}
E0 = {}
Xx = {}
Tt = {}
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind1,ind2,ind_step = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    nx = run.meta['nnn'][0]
    times = run.meta['time']*run.meta['tmult']
    x = run.meta['x']
    y = run.meta['y']
    code_name = run.meta['code_name']
    if code_name == 'HVM':
        B0[run_name] = 1.
        E0[run_name] = 1.
    elif code_name == 'iPIC':
        B0[run_name] = 0.01
        E0[run_name] = 0.01**2/np.sqrt(4.*np.pi)
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
    if ii == master_in:
        opath = run.meta['opath']
        out_dir = 'tearing_automodes_v2'
        ml.create_path(opath+'/'+out_dir)
        path = opath+'/'+out_dir
        res = glob.glob(path+'/comp*')
        cnt = 0
        for x in res:
            if os.path.isdir(x):
                cnt += 1
        out_dir = out_dir + '/comp%d'%(cnt)
        ml.create_path(opath+'/'+out_dir)
    track_dir = opath+'/'+'Psi_field/'+run_name+'/tracked_points'
#
    Eparl_x0[run_name] = [[],[]]
    y_cut[run_name] = []
    fftBy[run_name] = []
    fftBz[run_name] = []
    fftEy[run_name] = []
    fftEz[run_name] = []
    t[run_name] = []
#
    xfiles = glob.glob(track_dir+'/xpoint_from_*.dat')
    xnum = len(xfiles)
    t_ = {}
    x_ = {}
    y_ = {}
    v_ = {}
    for i in range(xnum):
        f = open(xfiles[i],'r')
        ll = f.readlines()
        f.close()
#
        nt = len(ll)
        t_[xfiles[i]] = np.empty(nt,dtype=np.float);   t0 = t_[xfiles[i]][0]
        x_[xfiles[i]] = np.empty(nt,dtype=np.long);    x0 = x_[xfiles[i]][0]
        y_[xfiles[i]] = np.empty(nt,dtype=np.long);    y0 = y_[xfiles[i]][0]
        v_[xfiles[i]] = np.empty(nt,dtype=np.float64); v0 = v_[xfiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t_[xfiles[i]][j] = ml.generic_cast(l[0],t0)
            x_[xfiles[i]][j] = ml.generic_cast(l[1],x0)
            y_[xfiles[i]][j] = ml.generic_cast(l[2],y0)
            v_[xfiles[i]][j] = ml.generic_cast(l[3],v0)
#
    plt.close()
    for i in range(xnum):
        plt.plot(x_[xfiles[i]],t_[xfiles[i]],label='X'+str(i))
    plt.legend()
    plt.xlim(0.,nx-1.)
    plt.ylim(times[0],times[-1])
    plt.xlabel('x');plt.ylabel('t');plt.title('Tracked X-points')
    plt.show()
    plt.close()
    ixp = ml.secure_input('Choose X-point (0 -> '+str(xnum-1)+'): ',10,True)
    Xx[run_name] = x_[xfiles[ixp]]
    Tt[run_name] = t_[xfiles[ixp]]
#
    #---> loop over times <---
    print " ",
    for ind_,ttt in enumerate(Tt[run_name]):
    #-------------------------
        #DEBUGDEBUGDEBUG --->
        #if (ttt < 10.) or (ttt > 12.5):
        #    continue
        #<--- DEBUGDEBUGDEBUG
        ind = np.argmin(np.abs(times-ttt))
        #get data
        #magnetic fields and co.
        E,B = run.get_EB(ind)
        By = B[1,:,:,0]
        Bz = B[2,:,:,0]
        Ey = E[1,:,:,0]
        Ez = E[2,:,:,0]
        del E,B
#
        ix0 = Xx[run_name][ind_]
        Eparl_x0[run_name][0].append( np.abs(Ez[ix0,iymin:iymax])/E0[run_name] )
        ix0 = Xx[run_name][ind_]+14
        Eparl_x0[run_name][1].append( np.abs(Ez[ix0,iymin:iymax])/E0[run_name] )
        if y_cut[run_name] == []:
            y_cut[run_name] = run.meta['y'][iymin:iymax]
#
        fft_ = np.zeros((nx/2+1),dtype=np.float64)
        for iy in range(iymin,iymax+1):
            fft_ += np.abs(np.fft.rfft(By[:,iy]-np.mean(By[:,iy])))
        fft_[1:] *= 2.
        fft_ /= float(nx)
        fft_ /= np.float(iymax+1-iymin)
        fftBy[run_name].append( fft_/B0[run_name] )
#
        fft_ = np.zeros((nx/2+1),dtype=np.float64)
        for iy in range(iymin,iymax+1):
            fft_ += np.abs(np.fft.rfft(Bz[:,iy]-np.mean(Bz[:,iy])))
        fft_[1:] *= 2.
        fft_ /= float(nx)
        fft_ /= np.float(iymax+1-iymin)
        fftBz[run_name].append( fft_/B0[run_name] )
#
        fft_ = np.zeros((nx/2+1),dtype=np.float64)
        for iy in range(iymin,iymax+1):
            fft_ += np.abs(np.fft.rfft(Ey[:,iy]-np.mean(Ey[:,iy])))
        fft_[1:] *= 2.
        fft_ /= float(nx)
        fft_ /= np.float(iymax+1-iymin)
        fftEy[run_name].append( fft_/B0[run_name] )
#
        fft_ = np.zeros((nx/2+1),dtype=np.float64)
        for iy in range(iymin,iymax+1):
            fft_ += np.abs(np.fft.rfft(Ez[:,iy]-np.mean(Ez[:,iy])))
        fft_[1:] *= 2.
        fft_ /= float(nx)
        fft_ /= np.float(iymax+1-iymin)
        fftEz[run_name].append( fft_/B0[run_name] )
        del fft_
#
        t[run_name].append( times[ind] )
    #---> loop over time <---
        print "\r",
        print run_name," t = ",times[ind],
        gc.collect()
        sys.stdout.flush()
    #------------------------
#
    times_dict[run_name] = times
    dt[run_name] = times[1]-times[0]
    t[run_name] = np.array(t[run_name])
    for i in range(2):
        Eparl_x0[run_name][i] = np.array(Eparl_x0[run_name][i])
    fftBy[run_name] = np.array(fftBy[run_name])
    fftBz[run_name] = np.array(fftBz[run_name])
    fftEy[run_name] = np.array(fftEy[run_name])
    fftEz[run_name] = np.array(fftEz[run_name])

#----------------
#PLOT TIME!!!!!!!
#----------------
mycol = ['red','chartreuse','aquamarine','darkgrey','magenta','chocolate','saddlebrown','teal','navy']
ncol = len(mycol)

tmp = []
for run_name in run_name_vec:
    tmp.append(len(times_dict[run_name]))

i = np.argmax(tmp)
times = times_dict[run_name_vec[i]]
#DEBUGDEBUGDEBUG --->
#times = t['LF_2d_DH']
#<--- DEBUGDEBUGDEBUG

y0 = ml.min_dict(y_cut)
y1 = ml.max_dict(y_cut)
plt.close()
for t_ in times:
    fftEmax = 1.e-12
    fftBmax = 1.e-12
    fig,ax = plt.subplots(2,2,figsize=(20,16))
    for i,run_name in enumerate(run_name_vec):
        try:
            ind_ = np.argmin(np.abs(t[run_name]-t_))
        except:
            continue
        if np.abs(t_-t[run_name][ind_]) > 0.5*dt[run_name]:
            continue
        ax[0,0].plot(y_cut[run_name],Eparl_x0[run_name][0][ind_]/np.mean(Eparl_x0[run_name][0][ind_]),
                   color=mycol[i%ncol],label='%s'%(run_name.replace('_',' ')))
        ax[0,1].plot(y_cut[run_name],Eparl_x0[run_name][1][ind_]/np.mean(Eparl_x0[run_name][1][ind_]),
                   color=mycol[i%ncol])
        ax[1,0].plot(np.arange(1.,len(fftEy[run_name][ind_,:])),fftEy[run_name][ind_,1:],
                   color=mycol[i%ncol])
        tmp = np.max(fftEy[run_name][ind_,1:])
        if tmp > fftEmax:
            fftEmax = tmp
        ax[1,0].plot(np.arange(1.,len(fftEz[run_name][ind_,:])),fftEz[run_name][ind_,1:],
                   '--',color=mycol[i%ncol])
        tmp = np.max(fftEz[run_name][ind_,1:])
        if tmp > fftEmax:
            fftEmax = tmp
        ax[1,1].plot(np.arange(1.,len(fftBy[run_name][ind_,:])),fftBy[run_name][ind_,1:],
                   color=mycol[i%ncol])
        tmp = np.max(fftBy[run_name][ind_,1:])
        if tmp > fftBmax:
            fftBmax = tmp
        #ax[1,1].plot(np.arange(1.,len(fftBz[run_name][ind_,:])),fftBz[run_name][ind_,1:],
        #           '--',color=mycol[i%ncol])
        #tmp = np.max(fftBz[run_name][ind_,1:])
        #if tmp > fftBmax:
        #    fftBmax = tmp
    ax[0,0].legend(loc='upper left')
    ax[0,0].set_ylabel('Eparallel at X-point')
    ax[0,0].set_title('t=%f'%(t_))
    ax[0,0].set_xlim(y0,y1)
    ax[0,0].set_xlabel('y')
    ax[0,1].set_ylabel('Eparallel at X-point + 1.0di')
    ax[0,1].set_xlim(y0,y1)
    ax[0,1].set_xlabel('y')
    ax[1,0].set_xscale('log')
    ax[1,0].set_yscale('log')
    ax[1,0].set_xlabel('m')
    ax[1,0].set_ylabel('modes of Ey(-) and  Ez(- -)')
    ax[1,0].set_xlim(1,nx/2)
    ax[1,0].set_ylim(1.e-12,fftEmax)
    ax[1,1].set_xscale('log')
    ax[1,1].set_yscale('log')
    ax[1,1].set_xlabel('m')
    ax[1,1].set_ylabel('modes of By(-)')# and  Bz(- -)')
    ax[1,1].set_xlim(1,nx/2)
    ax[1,1].set_ylim(1e-12,fftBmax)
    plt.tight_layout()
    #DEBUGDEBUGDEBUG --->
    #plt.show()
    #<--- DEBUGDEBUGDEBUG
    fig.savefig(opath+'/'+out_dir+'/'+'Eparl_cuts_and_spectrum_%f.png'%(t_))
    plt.close()

