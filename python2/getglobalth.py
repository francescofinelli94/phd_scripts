import numpy as np

with open('file.txt','r') as f:
    ll = f.readlines()

data = np.zeros((len(ll),len(ll[0].split())),dtype=np.float64)
for i,l in enumerate(ll):
    data[i,:] = np.array(list(map(float,l.split())))

tind = 0
aind = 1
nind = 2
sJind = 3 
J1ind = 4
J2ind = 5
sPind = 6
P1ind = 7
P2ind = 8

times = list(np.unique(data[:,tind]))
sJ = data[0,sJind]
sP = data[0,sPind]

tracks_th = []
global_th = []
for t in times:
    n  = 0.
    J1 = 0.
    J2 = 0.
    P1 = 0.
    P2 = 0.
    for i in range(data[:,0].size):
        if t == times[0]:
            thJ = np.sqrt( data[i,J1ind]/data[i,nind] + 
                  sJ*np.sqrt( data[i,J2ind]/data[i,nind] - (data[i,J1ind]/data[i,nind])**2 ) )
            thP =        ( data[i,P1ind]/data[i,nind] + 
                  sP*np.sqrt( data[i,P2ind]/data[i,nind] - (data[i,P1ind]/data[i,nind])**2 ) )
            tracks_th.append([thJ,thP])
        if data[i,tind] != t:
            continue
        n  += data[i,nind]
        J1 += data[i,J1ind]
        J2 += data[i,J2ind]
        P1 += data[i,P1ind]
        P2 += data[i,P2ind]
    thJ = np.sqrt( J1/n + sJ*np.sqrt( J2/n - (J1/n)**2 ) )
    thP =        ( P1/n + sP*np.sqrt( P2/n - (P1/n)**2 ) )
    global_th.append([thJ,thP])

print(global_th)
