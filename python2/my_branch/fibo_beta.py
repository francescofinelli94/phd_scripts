# coding: utf-8

#---------------------------------------------------------------------------------------
#-(1)-----21.12.18----FDP:S,F-----------------------------------------------------------
#-(2)-----30.12.18----FDP:S,J,R---------------------------------------------------------
#-(3)-----02.04.19----FDP:S,P,L---------------------------------------------------------
#-(4)-----04.04.19----FDP:S,J,L---------------------------------------------------------
#-(5)-----16.11.19----FDP:S,J,L,F-------------------------------------------------------
#---------------------------------------------------------------------------------------
#-(alpha)-19.07.19----FDP:S,J,L---------------------------------------------------------
#-(beta0)-12.11.19----FDP:S,F-----------------------------------------------------------
#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------

import numpy as np
import os
import matplotlib as mpl
import itertools as itt
import re
import time
import sys
#sys.path.insert(0,'/I_am_a_legit_path') #use this to add path containing libraries to import
from HVM_loader import *
from MFL_loader import *
from VTK_loader import *
from iPIC_loader import *

#---------------------------------------------------------------------------------------
#-----fibo:-standard-object-to-store-your-data------------------------------------------
#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------
class fibo (object):
  """
  fibo is a python object designed to contain your simulation data and perform authomatically simple operations on it 
  all functions are designed for data arrays of the form [nx,ny,nz] (no other indeces, please - this is done to keep routines light and free from for cycles)

  [fibo.data] means [str] in data or [np.ndarray(nx,ny,nz)]
  """
  
  def __init__(self,
      fibo_name):    #name of the set of data on which you work (part of simulation)

    self.fibo_name = str(fibo_name)

    self.data = {}  #dict of  np.ndarray(nx,ny,nz)
    self.pnts = {}  #dict of  np.ndarray(3,points)

    self.meta = {}  #dict of  meta_data - must be copied from the loaders
    self.stat = {}  #dict of  statistical values 


  #------------------------------------------------------------
  def help(self):

    print("Qu'est-ce que c'est que ce bordel ?")

    print('pippo = fb.fibo("pippo")')
    print('dir(fb)')
    print('dir(fb.fibo)')
    print('pippo.data.keys() --> list of data available')
    print('pippo.data["newname"] = pippo.data.pop("oldname")')
    print('np.unravel_index(np.argmax(...),np.shape(...))')


  #-----extra-routines-for-finding-data-and-meta-data----------
  #------------------------------------------------------------
  def get_time_exit(self,
      tar_segs,
      tar_time,
      criteria = 'closer'):
    """
    ------------------------------------------------------------------------------------
      gives you the segment and exact exit number to look at ...
    ------------------------------------------------------------------------------------
    tar_segs                [loader.segs] time segments 
    tar_time                [float] time you want to be 
    criteria = 'closer'     ['after','before','closer'] how to choose your time exit!
    ------------------------------------------------------------------------------------
    to_load                 [str,int,float] seg to load, exit number, time difference
    ------------------------------------------------------------------------------------
    """

    to_load = [' ',0,1000.]

    for seg in tar_segs.keys() :
      for exit_num in range(len(tar_segs[seg])):
        time_diff = float(tar_segs[seg][exit_num]) - time_aim

        if criteria == 'closer' : time_diff = np.absolute(time_diff)
        if criteria == 'before' : time_diff = -time_diff

        if (time_diff >= 0. and time_diff < to_load[2]):
          to_load[0] = seg
          to_load[1] = exit_num
          to_load[2] = time_diff

    return to_load

  #------------------------------------------------------------
  def get_data(self,
      tar_var):
    """
    ------------------------------------------------------------------------------------
      gives you the data array, overrunning fibo privileged data storage ...
    ------------------------------------------------------------------------------------
    tar_var   [str OR np.ndarray OR None]   name in list(fibo.data) OR (nx,ny,nz) array
    ------------------------------------------------------------------------------------
    data      [fibo.data]
    ------------------------------------------------------------------------------------
    """

    if   isinstance(tar_var, basestring) : return self.data[tar_var]
    elif isinstance(tar_var, np.ndarray) : return tar_var
    else : 
      print ('??WTW??')
      return None

  #------------------------------------------------------------
  def get_pnts(self,
      tar_var):
    """
    ------------------------------------------------------------------------------------
      gives you the data array, overrunning fibo privileged data storage ...
    ------------------------------------------------------------------------------------
    tar_var   [str OR np.ndarray OR None]   name in list(fibo.pnts) OR (nx,ny,nz) array
    ------------------------------------------------------------------------------------
    data      [fibo.data]
    ------------------------------------------------------------------------------------
    """

    if   isinstance(tar_var, basestring) : return self.pnts[tar_var]
    elif isinstance(tar_var, np.ndarray) : return tar_var
    else : 
      print ('??WTW??')
      return None

  #-----routines-for-basic-statistics--------------------------
  #------------------------------------------------------------


  def calc_minimal_stats(self,  
      tar_data, 
      tar_pnts = None,
      ave_label = 'ave',
      var_label = 'var',
      p12_label = 'p12',
      p50_label = 'p50',
      p88_label = 'p88'):
    """
    ----------------------------------------------------------------------------
      performs average, variance, and the 12%, 50%, 88% percentiles 
    ----------------------------------------------------------------------------
    tar_data              [fibo.data] 
    tar_pnts = None       [None OR fibo.pnts]
    ave_label = 'ave'     [None OR str] name of average
    var_label = 'var'     [None OR str] name of variance
    p12_label = 'p12'     [None OR str] name of 12% percentile
    p50_label = 'p50'     [None OR str] name of median
    p88_label = 'p88'     [None OR str] name of 88% percentile
    ----------------------------------------------------------------------------
    stat_dict             [dict] dictionary with {label : value}
    ----------------------------------------------------------------------------
    """

    tar_data = self.get_data(tar_data)
    tar_pnts = self.get_pnts(tar_pnts)
    if tar_pnts is None : tar_pnts = self.find_box()
    
    stat_dict = {}
    
    if (ave_label is not None) : stat_dict[ave_label] = np.average(self.get_data(tar_data)[list(tar_pnts)])
    if (var_label is not None) : stat_dict[var_label] = np.var(self.get_pnts(tar_data)[list(tar_pnts)])
    if (p12_label is not None) : stat_dict[p12_label] = np.percentile(self.get_data(tar_data)[list(tar_pnts)], 12)
    if (p50_label is not None) : stat_dict[p50_label] = np.percentile(self.get_data(tar_data)[list(tar_pnts)], 50)
    if (p88_label is not None) : stat_dict[p88_label] = np.percentile(self.get_data(tar_data)[list(tar_pnts)], 88)

    return stat_dict 

  #-----routines-for-basic-extractions-------------------------
  #------------------------------------------------------------




  def calc_axes(self,  
      range_x = None,
      range_y = None,
      range_z = None,
      offset_x = 0,
      offset_y = 0,
      offset_z = 0,
      coor_like = False,
      squa_like = False,
      mesh_like = False):
    """
    ----------------------------------------------------------------------------
      calculates axes values for some range around a given origin
    ----------------------------------------------------------------------------
    range_x = None    [int,int] x coordinates of the first and last point 
    range_y = None    [int,int] y coordinates of the first and last point 
    range_z = None    [int,int] z coordinates of the first and last point
    offset_x = 0  [int] x coordinate of the origin
    offset_y = 0  [int] y coordinate of the origin
    offset_z = 0  [int] z coordinate of the origin
    coor_like = False [bool] do you want axes to be coordinate-like scaled? 
    squa_like = False [bool] do you want axes to measure square of distance from origin?
    mesh_like = False [bool] do you want axes in mesh mode?
    ----------------------------------------------------------------------------
    x_coor [fibo.data OR np.ndarray(nx)]
    y_coor [fibo.data OR np.ndarray(ny)]
    z_coor [fibo.data OR np.ndarray(nz)]
    ----------------------------------------------------------------------------
    """
    
    if range_x is None : range_x = [0,self.meta['nnn'][0]]
    if range_y is None : range_y = [0,self.meta['nnn'][1]]
    if range_z is None : range_z = [0,self.meta['nnn'][2]]

    x_coor = np.linspace(range_x[0], range_x[1], max(range_x[1]-range_x[0],1), endpoint=False) - offset_x
    y_coor = np.linspace(range_y[0], range_y[1], max(range_y[1]-range_y[0],1), endpoint=False) - offset_y
    z_coor = np.linspace(range_z[0], range_z[1], max(range_z[1]-range_z[0],1), endpoint=False) - offset_z

    if coor_like :
      x_coor *= self.meta['ddd'][0]
      y_coor *= self.meta['ddd'][1]
      z_coor *= self.meta['ddd'][2]

    if squa_like :
      x_coor = np.square(x_coor)
      y_coor = np.square(y_coor)
      z_coor = np.square(z_coor)
    
    if mesh_like : 
      x_coor, y_coor, z_coor = np.meshgrid(x_coor,y_coor,z_coor) #y_coor,x_coor,z_coor

    return x_coor, y_coor, z_coor

  #------------------------------------------------------------ 
  def calc_line(self,  
      line_pts,  
      range_x,  
      range_y,  
      range_z):  
    """
    ----------------------------------------------------------------------------
      calculates axis values along a specified line
    ----------------------------------------------------------------------------
    line_pts   [int] length (in points) of the line you ask to extract
    range_x    [int,int] x coordinates of the first and last point 
    range_y    [int,int] y coordinates of the first and last point 
    range_z    [int,int] z coordinates of the first and last point
    ----------------------------------------------------------------------------
    line       [np.ndarray(line_pts)]
    ----------------------------------------------------------------------------
    """
    
    line_len  = (self.meta['dx']*(range_x[1]-range_x[0]))**2
    line_len += (self.meta['dy']*(range_y[1]-range_y[0]))**2
    line_len += (self.meta['dz']*(range_z[1]-range_z[0]))**2
    line_len = np.sqrt(line_len)

    return np.linspace(0, line_len, line_pts, endpoint=True)

  #------------------------------------------------------------
  def extract_line(self,  
      tar_var,  
      line_pts,  
      range_x,  
      range_y,  
      range_z):  
    """
    ----------------------------------------------------------------------------
      extracts array values along a specified line
    ----------------------------------------------------------------------------
    tar_var    [fibo.data] target variable
    line_pts   [int] length (in points) of the line you ask to extract
    range_x    [int,int] x coordinates of the first and last point 
    range_y    [int,int] y coordinates of the first and last point 
    range_z    [int,int] z coordinates of the first and last point
    ----------------------------------------------------------------------------
    line       [np.ndarray(line_pts)]
    ----------------------------------------------------------------------------
    """

    lin_x = np.linspace(range_x[0],range_x[1],line_pts,endpoint=True)
    lin_y = np.linspace(range_y[0],range_y[1],line_pts,endpoint=True)
    lin_z = np.linspace(range_z[0],range_z[1],line_pts,endpoint=True)
    return ndm.map_coordinates(self.get_data(tar_var), np.vstack((lin_x,lin_y,lin_z)))

  #------------------------------------------------------------
  def extract_grid(self,
      tar_var,  
      grid_pts,
      ranges_x,   
      ranges_y,   
      ranges_z):
    """
    ----------------------------------------------------------------------------
      extracts array values in a specified grid
    ----------------------------------------------------------------------------
    tar_var     [fibo.data] target variable
    grid_pts    [int,int] lengths (in points) of the plane you ask to extract
    ranges_x    [int,int,int] x coordinates of the first and two last points 
    ranges_y    [int,int,int] y coordinates of the first and two last points 
    ranges_z    [int,int,int] z coordinates of the first and two last points
    ----------------------------------------------------------------------------
    grid        [np.ndarray(grid_pts[0],grid_pts[1])] 
    ----------------------------------------------------------------------------
    """

    sec_x,sec_xx = np.meshgrid(np.linspace(ranges_x[0],ranges_x[1],grid_pts[0],endpoint=False),np.linspace(ranges_x[0],ranges_x[2],grid_pts[1],endpoint=False))
    sec_y,sec_yy = np.meshgrid(np.linspace(ranges_y[0],ranges_y[1],grid_pts[0],endpoint=False),np.linspace(ranges_y[0],ranges_y[2],grid_pts[1],endpoint=False))
    sec_z,sec_zz = np.meshgrid(np.linspace(ranges_z[0],ranges_z[1],grid_pts[0],endpoint=False),np.linspace(ranges_z[0],ranges_z[2],grid_pts[1],endpoint=False))
    
    sec_x = np.ndarray.flatten(sec_x+sec_xx) - ranges_x[0]
    sec_y = np.ndarray.flatten(sec_y+sec_yy) - ranges_y[0]
    sec_z = np.ndarray.flatten(sec_z+sec_zz) - ranges_z[0]

    return np.transpose(np.reshape(ndm.map_coordinates(self.get_data(tar_var), np.vstack((sec_x,sec_y,sec_z))),(grid_pts[1],grid_pts[0])))

  #------------------------------------------------------------    
  def find_frame_MVA(self,  
      tar_var_x,
      tar_var_y,
      tar_var_z,
      pos_perm = 'LMN',
      pos_z = True):
    """
    ----------------------------------------------------------------------------
      finds MVA frame relative to some dataset (can be any shape)
    ----------------------------------------------------------------------------
    tar_var_x        [fibo.data OR array] x component of the field you consider
    tar_var_y        [fibo.data OR array] y component of the field you consider
    tar_var_z        [fibo.data OR array] z component of the field you consider
    pos_perm = 'LMN' ['LMN' OR 'NML'] positive permutation of the indices
    pos_z = True     [bool] to impose that vec_L's z component is positive 
    ----------------------------------------------------------------------------
    vec_N    [np.ndarray(3)]  #memory tip: NaMeLy
    vec_M    [np.ndarray(3)]  
    vec_L    [np.ndarray(3)]
    vals     [np.ndarray(3)]
    ----------------------------------------------------------------------------
    """

    arrx = np.ndarray.flatten(self.get_data(tar_var_x))
    arry = np.ndarray.flatten(self.get_data(tar_var_y))
    arrz = np.ndarray.flatten(self.get_data(tar_var_z))

    matr = np.zeros([3,3])
    matr[0,0] = np.mean(arrx * arrx) - np.square(np.mean(arrx)) 
    matr[1,1] = np.mean(arry * arry) - np.square(np.mean(arry))
    matr[2,2] = np.mean(arrz * arrz) - np.square(np.mean(arrz))
    matr[0,1] = matr[1,0] = np.mean(arrx * arry) - np.mean(arrx)*np.mean(arry)
    matr[0,2] = matr[2,0] = np.mean(arrx * arrz) - np.mean(arrx)*np.mean(arrz)
    matr[1,2] = matr[2,1] = np.mean(arry * arrz) - np.mean(arry)*np.mean(arrz)

    vals, vecs = np.linalg.eigh(matr)
    vec_N, vec_M, vec_L = vecs[:,0], vecs[:,1], vecs[:,2]
    vec_N = np.reshape(vec_N, (3,1))
    vec_M = np.reshape(vec_M, (3,1))
    vec_L = np.reshape(vec_L, (3,1))

    #post-processing
    if (pos_z and vec_M[2] < 0.) : vec_M = -vec_M
    if pos_perm is 'LMN' : vec_N[0],vec_N[1],vec_N[2] =  self.calc_cross(vec_L[0],vec_M[0],vec_L[1],vec_M[1],vec_L[2],vec_M[2])
    if pos_perm is 'NML' : vec_N[0],vec_N[1],vec_N[2] = -self.calc_cross(vec_L[0],vec_M[0],vec_L[1],vec_M[1],vec_L[2],vec_M[2])

    return vec_N[:,0], vec_M[:,0], vec_L[:,0], vals

  #------------------------------------------------------------    
  def calc_shell_pop(self,  
      tar_var,      
      offset_x,
      offset_y,
      offset_z,
      lvl_num,
      lvl_max,
      squared = False, 
      density = True):  
    """
    -------------------------------------------------------------------------------------
      calculates population in each spherical shell centered in offx, offy, offz
    -------------------------------------------------------------------------------------
    tar_var          [fibo.data] target variable
    offset_x         [int] offset in x for the center of concentric shells
    offset_y         [int] offset in y for the center of concentric shells
    offset_z         [int] offset in z for the center of concentric shells
    lvl_num          [int] number of levels you want
    lvl_max          [float] distance you want to go sempling from the center
    squared = False  [bool] wanna label shells by the square of their distance from center?
    density = True   [bool] wanna divide values in each shell by the number of points in it?
    -------------------------------------------------------------------------------------
    shell_sum        [np.ndarray(lvl_num)]  shell integral OR shell density
    shell_num        [np.ndarray(lvl_num)]  number of points in the shell
    -------------------------------------------------------------------------------------
    """

    #create the levels vector, that determines in which shell each point goes
    dl =  lvl_max/lvl_num
    arrx, arry, arrz = self.calc_axes(offset_x=offset_x,offset_y=offset_y,offset_z=offset_z,coor_like=True,squa_like=True,mesh_like=True)
    levels = ((arrx +arry +arrz)/dl**2).astype(int)
    if not squared : levels = (np.sqrt(levels)).astype(int)

    #create the shell_pop array and fill it
    shell_pop = np.zeros([lvl_num,2])  

    #strange stuff: the following loop is faster on small arrays, but slower on big ones ...
    #  for il in range(lvl_num):
    #    shell_pop[il,0] = np.sum(self.get_data(tar_var)[levels == il])
    #    shell_pop[il,1] = np.sum(self.levels == il)
    #... than these three loops nested one into the other ...
    for iz in range(self.meta['nz']):
      for iy in range(self.meta['ny']):
        for ix in range(self.meta['nx']): 

          if dl*levels[ix,iy,iz] < lvl_max: #rad_max :
            shell_pop[levels[ix,iy,iz],0] += self.get_data(tar_var)[ix,iy,iz]
            shell_pop[levels[ix,iy,iz],1] += 1
    #... how possible?

    if density : 
      shell_pop[shell_pop[:,1] == 0,1] = 0.1
      shell_pop[:,0] /= shell_pop[:,1]

    #print(time.clock() - time_start)
    return shell_pop[:,0], shell_pop[:,1]

  #-------for-manipulating-------------------------------------
  #------------------------------------------------------------  
  def lower_res(self,  
      tar_var,
      ratio_x,
      ratio_y,
      ratio_z):
    """
    -------------------------------------------------------------------------------------
      lowers resolution of array, generating a new array by taking one point every ...
    -------------------------------------------------------------------------------------
    tar_var   [fibo_var] target variable
    ratio_x   [int] ratio of points to be kept in x (one every ratio_x)
    ratio_y   [int] ratio of points to be kept in y (one every ratio_y)
    ratio_z   [int] ratio of points to be kept in z (one every ratio_z)
    -------------------------------------------------------------------------------------
    new_var   [np.ndarray(nx//ratio_x,ny//ratio_y,nz//ratio_z)] to put in a new fibo
    -------------------------------------------------------------------------------------
    """
    
    #determine the shape of the new array
    arr = self.get_data(tar_var)
    ax,ay,az = np.shape(arr)
    nnx = int((ax+ratio_x-1)/ratio_x)
    nny = int((ay+ratio_y-1)/ratio_y)
    nnz = int((az+ratio_z-1)/ratio_z)
    
    #create and fill in the new array
    fff = np.zeros([nnx,nny,nnz], dtype=float)
    
    for iz in range(0,az,ratio_z):
      for iy in range(0,ay,ratio_y):
        for ix in range(0,ax,ratio_x):
          fff[ix//ratio_x,iy//ratio_y,iz//ratio_z] = arr[ix,iy,iz]

    print('done with the new array!')
    return fff

  #------------------------------------------------------------
  def slide_box(self,  
      tar_var,
      offset_x,
      offset_y,
      offset_z):
    """
    -------------------------------------------------------------------------------------
      slides your array so to see the box with another 'origin' ...
    -------------------------------------------------------------------------------------
    tar_var    [fibo_var] target variable
    offset_x   [float] offset we'll introduce in x (move for offx)
    offset_y   [float] offset we'll introduce in y (move for offy)
    offset_z   [float] offset we'll introduce in z (move for offz)
    -------------------------------------------------------------------------------------
    new_var    [fibo.data] same variable but slided
    -------------------------------------------------------------------------------------
    """

    #determine the shape of the array, correct any offset that is bigger than box size
    offset_x = offset_x % self.meta['nx']
    offset_y = offset_y % self.meta['ny']
    offset_z = offset_z % self.meta['nz']

    arr = self.get_data(tar_var)

    if offset_x != 0 : arr = np.tile(arr,(2,1,1))
    if offset_y != 0 : arr = np.tile(arr,(1,2,1))
    if offset_z != 0 : arr = np.tile(arr,(1,1,2))

    return arr[offset_x:self.meta['nx']+offset_x,offset_y:self.meta['ny']+offset_y,offset_z:self.meta['nz']+offset_z]


  #------------------------------------------------------------
  #--the-calc-functions-give-you-fibo.data---------------------
  #------------------------------------------------------------

  #------------------------------------------------------------
  def calc_scalr(self,  
      tar_var_1x,  
      tar_var_2x,  
      tar_var_1y,  
      tar_var_2y,
      tar_var_1z = None,
      tar_var_2z = None):  
    """
    -------------------------------------------------------------------------------------
      calculates scalar product between two vectors
    -------------------------------------------------------------------------------------
    tar_var_1x          [fibo.data]  first target variable: x component
    tar_var_2x          [fibo.data]  second target variable: x component
    tar_var_1y          [fibo.data]  first target variable: y component
    tar_var_2y          [fibo.data]  second target variable: y component
    tar_var_1z = None   [None OR fibo.data]  first target variable: z component
    tar_var_2z = None   [None OR fibo.data]  second target variable: z component
    -------------------------------------------------------------------------------------
    new_var             [fibo.data]  scalar product
    -------------------------------------------------------------------------------------
    """

    tar_arr  = np.multiply(self.get_data(tar_var_1x),self.get_data(tar_var_2x))
    tar_arr += np.multiply(self.get_data(tar_var_1y),self.get_data(tar_var_2y))
    if not (tar_var_1z is None) :
      tar_arr += np.multiply(self.get_data(tar_var_1z),self.get_data(tar_var_2z))

    return tar_arr

  #------------------------------------------------------------
  def calc_cross(self,  
      tar_var_1x,
      tar_var_2x,
      tar_var_1y,  
      tar_var_2y,
      tar_var_1z = None,  
      tar_var_2z = None):  
    """
    -------------------------------------------------------------------------------------
      calculates cross product between two vectors
    -------------------------------------------------------------------------------------
    tar_var_1x          [fibo.data]  first target variable: x component
    tar_var_2x          [fibo.data]  second target variable: x component
    tar_var_1y          [fibo.data]  first target variable: y component
    tar_var_2y          [fibo.data]  second target variable: y component
    tar_var_1z = None   [None OR fibo.data]  first target variable: z component
    tar_var_2z = None   [None OR fibo.data]  second target variable: z component
    -------------------------------------------------------------------------------------
    new_var_x    [None OR fibo.data]  cross product: x component
    new_var_y    [None OR fibo.data]  cross product: y component
    new_var_z    [fibo.data]  cross product: z component
    -------------------------------------------------------------------------------------
    """

    if not (tar_var_1z is None) :
      tar_arr_x  = np.multiply(self.get_data(tar_var_1y),self.get_data(tar_var_2z))
      tar_arr_x -= np.multiply(self.get_data(tar_var_1z),self.get_data(tar_var_2y))
      tar_arr_y  = np.multiply(self.get_data(tar_var_1z),self.get_data(tar_var_2x))
      tar_arr_y -= np.multiply(self.get_data(tar_var_1x),self.get_data(tar_var_2z))
    tar_arr_z  = np.multiply(self.get_data(tar_var_1x),self.get_data(tar_var_2y))
    tar_arr_z -= np.multiply(self.get_data(tar_var_1y),self.get_data(tar_var_2x))

    if not (tar_var_1z is None) : 
      return tar_arr_x, tar_arr_y, tar_arr_z
    else : 
      return None, None, tar_arr_z

  #------------------------------------------------------------
  def calc_project(self,
      tar_var_x,  
      ref_var_1x,  
      ref_var_2x,  
      ref_var_3x,  
      tar_var_y,  
      ref_var_1y,  
      ref_var_2y,  
      ref_var_3y,  
      tar_var_z = None,  
      ref_var_1z = None,  
      ref_var_2z = None,  
      ref_var_3z = None):  
    """
    ------------------------------------------------------------------------------------
      projects a vector field over three vector components
    ------------------------------------------------------------------------------------
    tar_var_x            [fibo.data] target variable: x component  
    ref_var_1x           [fibo.data OR np.float] x component of 1st versor 
    ref_var_2x           [fibo.data OR np.float] x component of 2nd versor 
    ref_var_3x           [fibo.data OR np.float] x component of 3rd versor 
    tar_var_y            [fibo.data] target variable: y component
    ref_var_1y           [fibo.data OR np.float] y component of 1st versor 
    ref_var_2y           [fibo.data OR np.float] y component of 2nd versor 
    ref_var_3y           [fibo.data OR np.float] y component of 3st versor 
    tar_var_z = None     [None OR fibo.data] target variable: z component
    ref_var_1z = None    [None OR fibo.data OR np.float] z component of 1st versor 
    ref_var_2z = None    [None OR fibo.data OR np.float] z component of 2nd versor 
    ref_var_3z = None    [None OR fibo.data OR np.float] z component of 3rd versor 
    ------------------------------------------------------------------------------------
    new_var_x      [fibo.data] projected field: x component
    new_var_y      [fibo.data] projected field: y component
    new_var_z      [None OR fibo.data] projected field: z component
    ------------------------------------------------------------------------------------
    """

    #if the reference is given as single-float array, make it the same shape as the other!  
    if  np.shape(self.get_data(ref_var_1x)) == (1,) :
      arr_shape = np.shape(self.get_data(tar_var_x))

      ref_1x = np.ones(arr_shape) * self.get_data(ref_var_1x)
      ref_2x = np.ones(arr_shape) * self.get_data(ref_var_2x)
      ref_3x = np.ones(arr_shape) * self.get_data(ref_var_3x)
      ref_1y = np.ones(arr_shape) * self.get_data(ref_var_1y)
      ref_2y = np.ones(arr_shape) * self.get_data(ref_var_2y)
      ref_3y = np.ones(arr_shape) * self.get_data(ref_var_3y)
      if tar_var_z is None :
        ref_1z = ref_2z = ref_3z = None
      else :
        ref_1z = np.ones(arr_shape) * self.get_data(ref_var_1z)
        ref_2z = np.ones(arr_shape) * self.get_data(ref_var_2z)
        ref_3z = np.ones(arr_shape) * self.get_data(ref_var_3z)

    #otherwise, just see how the thing projects point by point 
    else : 
      ref_1x = self.get_data(ref_var_1x)
      ref_2x = self.get_data(ref_var_2x)
      ref_3x = self.get_data(ref_var_3x)
      ref_1y = self.get_data(ref_var_1y)
      ref_2y = self.get_data(ref_var_2y)
      ref_3y = self.get_data(ref_var_3y)
      if tar_var_z is None :
        ref_1z = ref_2z = ref_3z = None
      else :
        ref_1z = self.get_data(ref_var_1z)
        ref_2z = self.get_data(ref_var_2z)
        ref_3z = self.get_data(ref_var_3z)

    pro_1 = self.calc_scalr(tar_var_x,ref_1x, tar_var_y,ref_1y, tar_var_z,ref_1z)
    pro_2 = self.calc_scalr(tar_var_x,ref_2x, tar_var_y,ref_2y, tar_var_z,ref_2z)
    pro_3 = self.calc_scalr(tar_var_x,ref_3x, tar_var_y,ref_3y, tar_var_z,ref_3z)
  
    return pro_1, pro_2, pro_3

  #------------------------------------------------------------
  def calc_par_per(self,  
      tar_var_x,  
      ref_var_x,
      tar_var_y,
      ref_var_y,
      tar_var_z = None,
      ref_var_z = None):
    """
    ------------------------------------------------------------------------------------
      calculates parallel and perpendicolar parts of tar_var with respect to ref_var
    ------------------------------------------------------------------------------------
    tar_var_x         [fibo.data] tar_var: x component
    ref_var_x         [fibo.data] ref_var: x component
    tar_var_y         [fibo.data] tar_var: y component
    ref_var_y         [fibo.data] ref_var: y component
    tar_var_z = None  [None OR fibo.data] tar_var: z component
    ref_var_z = None  [None OR fibo.data] ref_var: z component
    ------------------------------------------------------------------------------------
    par_var_x      [fibo.data] parallel part: x component
    par_var_y      [fibo.data] parallel part: y component
    par_var_z      [None OR fibo.data] parallel part: z component
    per_var_x      [fibo.data] perpendicular part: x component
    per_var_y      [fibo.data] perpendicular part: y component
    per_var_z      [None OR fibo.data] perpendicular part: z component
    ------------------------------------------------------------------------------------
    """

    fact  = np.reciprocal(self.calc_scalr(ref_var_x,ref_var_x,ref_var_y,ref_var_y,ref_var_z,ref_var_z))
    fact *= self.calc_scalr(tar_var_x,ref_var_x,tar_var_y,ref_var_y,tar_var_z,ref_var_z)

    if not (tar_var_z is None) : 
      arrx = self.get_data(ref_var_x)*fact
      arry = self.get_data(ref_var_y)*fact
      arrz = self.get_data(ref_var_z)*fact
      return arrx, arry, arrz, self.get_data(tar_var_x)-arrx, self.get_data(tar_var_y)-arry, self.get_data(tar_var_z)-arrz

    else : 
      arrx = self.get_data(ref_var_x)*fact
      arry = self.get_data(ref_var_y)*fact
      return arrx, arry, None, self.get_data(tar_var_x)-arrx, self.get_data(tar_var_y)-arry, None

  #------------------------------------------------------------
  def calc_irr_sol(self,  #calculates irrotational and solenoidal components of a vector field
      tar_var_x,
      tar_var_y,
      tar_var_z = None):
    """
    ------------------------------------------------------------------------------------
      calculates irrotational and solenoidal components of the given vector field
    ------------------------------------------------------------------------------------
    tar_var_x           [fibo.data] target variable: x component
    tar_var_y           [fibo.data] target variable: y component
    tar_var_z = None    [None OR fibo.data] target variable: z component
    ------------------------------------------------------------------------------------
    irr_var_x           [fibo.data] irrotational part: x component
    irr_var_y           [fibo.data] irrotational part: y component
    irr_var_z           [None OR fibo.data] irrotational part: z component
    sol_var_x           [fibo.data] solenoidal part: x component
    sol_var_y           [fibo.data] solenoidal part: y component
    sol_var_z           [None OR fibo.data] solenoidal part: z component
    ------------------------------------------------------------------------------------
      credit: L.R.
    """

    time_start = time.clock()      
    #determine the shape of the array, correct any offset that is bigger than box size
    nx,ny,nz = np.shape(self.get_data(tar_var_x))
    nxm = nx//2
    nym = ny//2
    nzm = nz//2

    myaxes = ()
    if nx > 1 : myaxes = myaxes + (0,)
    if ny > 1 : myaxes = myaxes + (1,)
    if nz > 1 : myaxes = myaxes + (2,)

    #if not (tar_var_1z is None) :     
    G_x = np.fft.fftn(self.get_data(tar_var_x),axes=myaxes)
    G_y = np.fft.fftn(self.get_data(tar_var_y),axes=myaxes)
    G_z = np.fft.fftn(self.get_data(tar_var_z),axes=myaxes)

    #print(myaxes)
    GPsi = np.zeros([nx,ny,nz],dtype='complex128')
    for ix in range(1,nxm):
      GPsi[ix,:,:] += 1j*G_x[ix,:,:]*ix / self.meta['xl']
      GPsi[nx-ix,:,:] += -1j*G_x[nx-ix,:,:]*ix / self.meta['xl']
    for iy in range(1,nym):
      GPsi[:,iy,:] += 1j*G_y[:,iy,:]* iy / self.meta['yl']
      GPsi[:,ny-iy,:] += -1j*G_y[:,ny-iy,:]* iy / self.meta['yl']
    for iz in range(1,nzm):
      GPsi[:,:,iz] += 1j*G_z[:,:,iz]* iz / self.meta['zl']
      GPsi[:,:,nz-iz] += -1j*G_z[:,:,nz-iz]* iz / self.meta['zl']

    del(G_x, G_y, G_z)
    mesh_x = np.arange(0,nx) / self.meta['xl']
    mesh_y = np.arange(0,ny) / self.meta['yl']
    mesh_z = np.arange(0,nz) / self.meta['zl']
    mesh_y, mesh_x, mesh_z = np.meshgrid(mesh_y,mesh_x,mesh_z)   #achtung!  (y,x,z) is correct order!  
    mesh = np.square(mesh_x) + np.square(mesh_y) + np.square(mesh_z)
    mesh[0,0,0] = 1.

    GPsi = -1j* np.multiply(GPsi, np.reciprocal(mesh))

    # back to physical space
    arrx = (np.fft.ifftn((GPsi * mesh_x),axes=myaxes)).real
    arrxx = self.get_data(tar_var_x)-arrx
    arry = (np.fft.ifftn((GPsi * mesh_y),axes=myaxes)).real
    arryy = self.get_data(tar_var_y)-arry
    if (tar_var_z is None) : 
      arrz = None    #print(time.clock()-time_start)
      arrzz = None
    else : #if (myaxes == (0, 1, 2)) :
      arrz = (np.fft.ifftn((GPsi * mesh_z),axes=myaxes)).real
      arrzz = self.get_data(tar_var_z)-arrz
    #else :
    #  arrz = np.zeros([nx,ny,nz])
    #  arrzz = np.zeros([nx,ny,nz])

    return arrx, arry, arrz, arrxx, arryy, arrzz


  #------------------------------------------------------------
  def calc_gradx(self,    
      tar_var,
      xl = None,
      periodic = True,
      der_ord = 1):
    """ 
    ------------------------------------------------------------------------------------
      calculates x gradient component with(out) periodical boundary conditions
    ------------------------------------------------------------------------------------
    tar_var           [fibo.data] target variable of the procedure
    periodic = True   [bool] periodic boundary conditions?
    der_ord = 1       [int] order of derivation (0: no derivative, 1: first derivative, 2: second derivative ...)
    ------------------------------------------------------------------------------------
    xgr_var           [fibo.data]
    ------------------------------------------------------------------------------------
    """

    #determine the coefficients
    nx,ny,nz = self.meta['nnn']

    if nx == 1 : 
      return np.zeros([nx,ny,nz])

    else : 
      oo_6 = self.meta['dx']**(-der_ord) * np.array([1.0,  0.0     ,-49./18.])        
      aa_6 = self.meta['dx']**(-der_ord) * np.array([0.0,  9.0/12.0,  3./2. ]) 
      bb_6 = self.meta['dx']**(-der_ord) * np.array([0.0, -3.0/20.0, -3./20.]) 
      cc_6 = self.meta['dx']**(-der_ord) * np.array([0.0,  1.0/60.0,  1./90.]) 
    
      #create the new vector, fill it
      if periodic :
        ff = np.tile(self.get_data(tar_var),(2,1,1))
        dx_f  = oo_6[der_ord] * ff[0:nx,:,:] 
        dx_f += aa_6[der_ord] *(ff[1:1+nx,:,:] - ff[nx-1:2*nx-1,:,:])
        dx_f += bb_6[der_ord] *(ff[2:2+nx,:,:] - ff[nx-2:2*nx-2,:,:])
        dx_f += cc_6[der_ord] *(ff[3:3+nx,:,:] - ff[nx-3:2*nx-3,:,:])
      else :
        f = self.get_data(tar_var)  
        dx_f  = np.zeros([nx,ny,nz])  
        dx_f[3:nx-3,:,:]  = oo_6[der_ord] * f[3:nx-3,:,:]
        dx_f[3:nx-3,:,:] += aa_6[der_ord] *(f[4:nx-2,:,:] - f[2:nx-4,:,:])
        dx_f[3:nx-3,:,:] += bb_6[der_ord] *(f[5:nx-1,:,:] - f[1:nx-5,:,:])
        dx_f[3:nx-3,:,:] += cc_6[der_ord] *(f[6:nx-0,:,:] - f[0:nx-6,:,:])

      #print('done with the new array!')
      return dx_f

  #------------------------------------------------------------  
  def calc_grady(self,    
      tar_var,
      yl = None,
      periodic = True,
      der_ord = 1): 
    """ 
    ------------------------------------------------------------------------------------
      calculates y gradient component with(out) periodical boundary conditions
    ------------------------------------------------------------------------------------
    tar_var           [fibo.data] target variable of the procedure
    periodic = True   [bool] periodic boundary conditions?
    der_ord = 1       [int] order of derivation (0: no derivative, 1: first derivative, 2: second derivative ...)
    ------------------------------------------------------------------------------------
    ygr_var           [fibo.data]
    ------------------------------------------------------------------------------------
    """

    #determine the coefficients
    nx,ny,nz = self.meta['nnn']

    if ny == 1 : 
      return np.zeros([nx,ny,nz])

    else :
      oo_6 = self.meta['dy']**(-der_ord) * np.array([1.0,  0.0,  -49./18. ])       
      aa_6 = self.meta['dy']**(-der_ord) * np.array([0.0,  9.0/12.0,  3./2. ])
      bb_6 = self.meta['dy']**(-der_ord) * np.array([0.0, -3.0/20.0, -3./20.])
      cc_6 = self.meta['dy']**(-der_ord) * np.array([0.0,  1.0/60.0,  1./90.])
    
      #create the new vector, fill it  (periodic / nonperiodic boundary cases ...)  
      if periodic :
        ff = np.tile(self.get_data(tar_var),(1,2,1))
        dy_f  = oo_6[der_ord] * ff[:,0:ny,:] 
        dy_f += aa_6[der_ord] *(ff[:,1:1+ny,:] - ff[:,ny-1:2*ny-1,:])
        dy_f += bb_6[der_ord] *(ff[:,2:2+ny,:] - ff[:,ny-2:2*ny-2,:])
        dy_f += cc_6[der_ord] *(ff[:,3:3+ny,:] - ff[:,ny-3:2*ny-3,:])
      else :
        f = self.get_data(tar_var)  
        dy_f = np.zeros([nx,ny,nz])  
        dy_f[:,3:ny-3,:]  = oo_6[der_ord] * f[:,3:ny-3,:]
        dy_f[:,3:ny-3,:] += aa_6[der_ord] *(f[:,4:ny-2,:] - f[:,2:ny-4,:])
        dy_f[:,3:ny-3,:] += bb_6[der_ord] *(f[:,5:ny-1,:] - f[:,1:ny-5,:])
        dy_f[:,3:ny-3,:] += cc_6[der_ord] *(f[:,6:ny-0,:] - f[:,0:ny-6,:])

      #print('done with the new array!')
      return dy_f

  #------------------------------------------------------------  
  def calc_gradz(self,
      tar_var,
      zl = None,
      periodic = True,
      der_ord = 1):
    """ 
    ------------------------------------------------------------------------------------
      calculates z gradient component with(out) periodical boundary conditions
    ------------------------------------------------------------------------------------
    tar_var            [fibo.data] target variable of the procedure
    periodic = True    [bool] periodic boundary conditions?
    der_ord = 1        [int] order of derivation (0: no derivative, 1: first derivative, 2: second derivative ...)
    ------------------------------------------------------------------------------------
    zgr_var            [fibo.data]
    ------------------------------------------------------------------------------------
    """

    #determine the coefficients
    nx,ny,nz = self.meta['nnn']

    if nz == 1 :
      return np.zeros([nx,ny,nz])

    else :
      oo_6 = self.meta['dz']**(-der_ord) * np.array([1.0,  0.0, -49./18.])      
      aa_6 = self.meta['dz']**(-der_ord) * np.array([0.0,  9.0/12.0,  3./2. ]) 
      bb_6 = self.meta['dz']**(-der_ord) * np.array([0.0, -3.0/20.0, -3./20.]) 
      cc_6 = self.meta['dz']**(-der_ord) * np.array([0.0,  1.0/60.0,  1./90.]) 

      #create the new vector, fill it
      if periodic :
        ff = np.tile(self.get_data(tar_var),(1,1,2))
        dz_f  = oo_6[der_ord] * ff[:,:,0:nz] 
        dz_f += aa_6[der_ord] *(ff[:,:,1:1+nz] - ff[:,:,nz-1:2*nz-1])
        dz_f += bb_6[der_ord] *(ff[:,:,2:2+nz] - ff[:,:,nz-2:2*nz-2])
        dz_f += cc_6[der_ord] *(ff[:,:,3:3+nz] - ff[:,:,nz-3:2*nz-3])  
      else:
        f = self.get_data(tar_var)  
        dz_f  = np.zeros([nx,ny,nz])  
        dz_f[:,:,3:nz-3]  = oo_6[der_ord] * f[:,:,3:nz-3]
        dz_f[:,:,3:nz-3] += aa_6[der_ord] *(f[:,:,4:nz-2] - f[:,:,2:nz-4])
        dz_f[:,:,3:nz-3] += bb_6[der_ord] *(f[:,:,5:nz-1] - f[:,:,1:nz-5])
        dz_f[:,:,3:nz-3] += cc_6[der_ord] *(f[:,:,6:nz-0] - f[:,:,0:nz-6])

      #print('done with the new array!')
      return dz_f

  #------------------------------------------------------------
  def calc_divr(self,
      tar_var_x,
      tar_var_y,
      tar_var_z,
      perx = True,
      pery = True,
      perz = True):      
    """ 
    ------------------------------------------------------------------------------------
       calculates the divergence
    ------------------------------------------------------------------------------------
    tar_var_x    [fibo.data] target field, x component
    tar_var_y    [fibo.data] target field, y component
    tar_var_z    [fibo.data] target field, z component
    perx = True  [bool] x periodic? (not considered if nx==1)
    pery = True  [bool] y periodic? (not considered if ny==1)
    perz = True  [bool] z periodic? (not considered if nz==1)     
    ------------------------------------------------------------------------------------
    divr_var     [fibo.data]
    ------------------------------------------------------------------------------------
    """

    #create a raw vector 
    nx,ny,nz = self.meta['nnn'] 
    divr = np.zeros([nx,ny,nz])

    if nx != 1 : divr += self.calc_gradx(tar_var_x,periodic=perx)
    if ny != 1 : divr += self.calc_grady(tar_var_y,periodic=pery)
    if nz != 1 : divr += self.calc_gradz(tar_var_z,periodic=perz)

    return divr

  #------------------------------------------------------------
  def calc_curl(self,  
      tar_var_x,
      tar_var_y,
      tar_var_z,
      perx = True,
      pery = True,
      perz = True):   
    """ 
    ------------------------------------------------------------------------------------
       calculates the curl
    ------------------------------------------------------------------------------------
    tar_var_x    [fibo.data] target field, x component
    tar_var_y    [fibo.data] target field, y component
    tar_var_z    [fibo.data] target field, z component
    perx = True  [bool] x periodic? (not considered if nx==1)
    pery = True  [bool] y periodic? (not considered if ny==1)
    perz = True  [bool] z periodic? (not considered if nz==1)
    ------------------------------------------------------------------------------------
    curl_var     [fibo.data]
    ------------------------------------------------------------------------------------
    """

    #create a raw vector field
    nx,ny,nz = self.meta['nnn']
    curl_x = np.zeros([nx,ny,nz])
    curl_y = np.zeros([nx,ny,nz])
    curl_z = np.zeros([nx,ny,nz])

    if nx != 1 : 
      curl_y -= self.calc_gradx(tar_var_z,periodic=perx)
      curl_z += self.calc_gradx(tar_var_y,periodic=perx)
    if ny != 1 : 
      curl_z -= self.calc_grady(tar_var_x,periodic=pery)
      curl_x += self.calc_grady(tar_var_z,periodic=pery)
    if nz != 1 : 
      curl_x -= self.calc_gradz(tar_var_y,periodic=perz)
      curl_y += self.calc_gradz(tar_var_x,periodic=perz)

    return curl_x, curl_y, curl_z

  #------------------------------------------------------------
  def calc_lapl(self,  
      tar_var_x,
      tar_var_y,
      tar_var_z,
      perx = True,
      pery = True,
      perz = True):     
    """ 
    ------------------------------------------------------------------------------------
       calculates the laplacian
    ------------------------------------------------------------------------------------
    tar_var_x    [fibo.data] target field, x component
    tar_var_y    [fibo.data] target field, y component
    tar_var_z    [fibo.data] target field, z component
    perx = True  [bool] x periodic? (not considered if nx==1)
    pery = True  [bool] y periodic? (not considered if ny==1)
    perz = True  [bool] z periodic? (not considered if nz==1)
    ------------------------------------------------------------------------------------
    lapl_var     [fibo.data]
    ------------------------------------------------------------------------------------
    """
    #create a raw vector
    nx,ny,nz = self.meta['nnn']
    lapl = np.zeros([nx,ny,nz])

    if nx != 1 : lapl += self.calc_gradx(tar_var_x,periodic=perx,der_ord=2)
    if ny != 1 : lapl += self.calc_grady(tar_var_y,periodic=pery,der_ord=2)
    if nz != 1 : lapl += self.calc_gradz(tar_var_z,periodic=perz,der_ord=2)

    return lapl

  #------------------------------------------------------------
  def calc_spect(self,  
      tar_var,  
      raw = False):    #if False gives the data re-arranged (equivalent to applying a slide_box of nx//2, ny//2, nz//2)
    """ 
    ------------------------------------------------------------------------------------
       calculates fourier transform of some variable (real-valued)
    ------------------------------------------------------------------------------------
    tar_var      [fibo.data] target field
    raw = False  [bool] if False gives the data re-arranged (equivalent to applying a slide_box of nx//2, ny//2, nz//2)
    ------------------------------------------------------------------------------------
    fou_var      [fibo.data]
    ------------------------------------------------------------------------------------
    """
    #create a raw vector (symmetric part of our variable goes to real, antisymmetric part goes to imaginary, normalization to get integral ...)
    nx,ny,nz = self.meta['nnn']
    fou_data = np.absolute(np.fft.fftn(self.get_data(tar_var))) * 2. / (nx*ny*nz)

    if raw : 
      return fou_data

    else :
      if nx != 1 : fou_data = np.tile(fou_data,(2,1,1))
      if ny != 1 : fou_data = np.tile(fou_data,(1,2,1))
      if nz != 1 : fou_data = np.tile(fou_data,(1,1,2))

      return fou_data[nx//2:nx+nx//2,ny//2:ny+ny//2,nz//2:nz+nz//2]



  #------------------------------------------------------------
  #--the-find-functions-give-you-fibo.pnts---------------------
  #------------------------------------------------------------

  #------------------------------------------------------------
  def find_box(self,
      range_x = None,
      range_y = None,
      range_z = None):
    """
    ----------------------------------------------------------------------------
      gives list of points covering all box specified
    ----------------------------------------------------------------------------
    range_x = None       [int,int] x coordinates of the first and last point 
    range_y = None       [int,int] y coordinates of the first and last point 
    range_z = None       [int,int] z coordinates of the first and last point
    ----------------------------------------------------------------------------
    tar_box              [fibo.pnts]
    ----------------------------------------------------------------------------
    """  

    if range_x == None : range_x = [0,self.meta['nnn'][0]]
    if range_y == None : range_y = [0,self.meta['nnn'][1]]
    if range_z == None : range_z = [0,self.meta['nnn'][2]]

   
    zz, yy, xx = np.meshgrid(range(range_z[0],range_z[1]),range(range_y[0],range_y[1]),range(range_x[0],range_x[1]))
    xx = xx.flatten()
    yy = yy.flatten()
    zz = zz.flatten()         
    
    return np.array([xx,yy,zz])

  #------------------------------------------------------------
  def find_slice(self,
      tar_pnts):
    """
    ----------------------------------------------------------------------------
      from list of vertices gives all points inside 2d polygon in x,y plane
    ----------------------------------------------------------------------------
    tar_pnts        [fibo.pnts] x,y,z vertex coordinates 
    ----------------------------------------------------------------------------
    tar_slice       [fibo.pnts]
    ----------------------------------------------------------------------------
    """
    
    cut_z = tar_pnts[2,0]
    plane_vertices = np.array( tar_data[:,:,cut_z], dtype=np.int32)
    plane_selector = np.zeros( [nx,ny], dtype=np.uint8)
    plane_selector = cv2.fillPoly(plane_selector,plane_vertices,1)

    pnts_xy = np.transpose(np.argwhere(plane_selector==1))
    pnts_z = np.ones(np.shape(plane_pnts)[1]) * cut_z
    
    return np.vstack([pnts_xy,pnts_z])

  #------------------------------------------------------------  
  def find_common_coord(self,
      coo_var_1,
      coo_var_2):
    """ 
    ------------------------------------------------------------------------------------
       finds common points in two sets of coordinates
    ------------------------------------------------------------------------------------
    coo_var_1      [fibo.pnts] first coordinate set
    coo_var_2      [fibo.pnts] second coordinate set
    ------------------------------------------------------------------------------------
    coor_var   [fibo_coor] 
    ------------------------------------------------------------------------------------
    """  
    
    set_a = set(tuple(i) for i in np.transpose(self.get_pnts(coo_var_1)))
    set_b = set(tuple(i) for i in np.transpose(self.get_pnts(coo_var_2)))
    myset = set_a.intersection(set_b)

    return np.transpose(np.array([list(i) for i in myset]))

  #------------------------------------------------------------
  def find_critical_squarely_2d(self,  
      tar_var,
      aa=1, 
      bb=0,    
      thr=1e-1):
    """ 
    ------------------------------------------------------------------------------------
       finds zeros in 2d array by triangulation on neighbors (unrefined version of find_critical_delaunay_2d)
    ------------------------------------------------------------------------------------
    tar_var      [fibo.data] target field
    aa = 1       [int>0] first parameter to determine neighbors' diamond 
    bb = 0       [int>0] second parameter to determine neighbors' diamond
    thr=1e-1     [float>0 ] threshold value for maxima/minima detection 
    ------------------------------------------------------------------------------------
    coord_Oa     [fibo.pnts]
    coord_Ob     [fibo.pnts]
    coord_X      [fibo.pnts]
    ------------------------------------------------------------------------------------
    """
    nx,ny,nz = self.meta['nnn']
    arr = np.tile(self.get_data(tar_var)[:,:,0],(2,2))
    if nz != 1 : print('nz not zero! taking k=zero ...')

    #create eight (nx,ny) arrays with all differences between one value and its neighbours
    neigh = np.zeros([8,nx,ny])
  
    neigh[0,:,:] = (arr[aa:nx+aa, bb:ny+bb] - arr[0:nx, 0:ny])
    neigh[2,:,:] = (arr[nx-bb:2*nx-bb, aa:ny+aa] - arr[0:nx, 0:ny])
    neigh[4,:,:] = (arr[nx-aa:2*nx-aa, ny-bb:2*ny-bb] - arr[0:nx, 0:ny])
    neigh[6,:,:] = (arr[bb:nx+bb, ny-aa:2*ny-aa] - arr[0:nx, 0:ny])

    if aa > bb :
      neigh[1,:,:] = (arr[ aa-bb:nx+aa-bb, aa+bb:ny+aa+bb] - arr[0:nx, 0:ny])
      neigh[3,:,:] = (arr[ nx-aa-bb:2*nx-aa-bb, aa-bb:ny+aa-bb] - arr[0:nx, 0:ny])
      neigh[5,:,:] = (arr[ nx-aa+bb:2*nx-aa+bb, ny-aa-bb:2*ny-aa-bb] - arr[0:nx, 0:ny])
      neigh[7,:,:] = (arr[ aa+bb:nx+aa+bb, ny-aa+bb:2*ny-aa+bb] - arr[0:nx, 0:ny])
    else : 
      neigh[1,:,:] = (arr[ nx+aa-bb:2*nx+aa-bb, aa+bb:ny+aa+bb] - arr[0:nx, 0:ny])
      neigh[3,:,:] = (arr[ nx-aa-bb:2*nx-aa-bb, ny+aa-bb:2*ny+aa-bb] - arr[0:nx, 0:ny])
      neigh[5,:,:] = (arr[-aa+bb:nx-aa+bb, ny-aa-bb:2*ny-aa-bb] - arr[0:nx, 0:ny])
      neigh[7,:,:] = (arr[aa+bb:nx+aa+bb, -aa+bb:ny-aa+bb] - arr[0:nx, 0:ny])

    #calculate Delta plus and minus
    pos_neighs = np.maximum(neigh, np.zeros([8,nx,ny]))
    Delta_plus  = np.sum(pos_neighs,axis=0)
    Delta_minus = np.sum(pos_neighs-neigh,axis=0)

    #select positive neighbors and calculate sign changes
    pos_sel = pos_neighs > 0. 
    sign_change = np.sum(np.diff(pos_sel,axis=0),axis=0) + 1
    sign_change -= sign_change % 2

    #calculate coordinates of Oa points (maxima), Ob points (minima) and X points (saddles)
    coord_Oa = np.transpose(np.argwhere(np.logical_and(Delta_plus < thr*Delta_minus, sign_change == 0)))
    coord_Ob = np.transpose(np.argwhere(np.logical_and(Delta_minus < thr*Delta_plus, sign_change == 0)))
    coord_X = np.transpose(np.argwhere(sign_change > 2)) #np.logical_and(Delta_plus-Delta_minus < thr*(Delta_plus+Delta_minus), sign_change > 2)))

    return coord_Oa, coord_Ob, coord_X

  #------------------------------------------------------------
  def find_critical_delaunay_2d(self, 
      tar_var,   
      aa=1, 
      bb=0,  
      thr=1e-1):
    """ 
    ------------------------------------------------------------------------------------
       finds zeros in 2d array by triangulation on delaunay-selected neighbours (refined version of find_critical_squarely_2d)
    ------------------------------------------------------------------------------------
    tar_var      [fibo_var] target field
    aa = 1       [int>0] first parameter to determine neighbors' diamond 
    bb = 0       [int>0] second parameter to determine neighbors' diamond
    thr=1e-1     [float>0 ] threshold value for maxima/minima detection 
    ------------------------------------------------------------------------------------
    coord_Oa, coord_Ob, coord_X   [fibo_pnts,fibo_pnts,fibo_pnts]
    ------------------------------------------------------------------------------------
    """    

    nx,ny,nz = self.meta['nnn'] 
    arr = np.tile(self.get_data(tar_var)[:,:,0],(2,2))
    if nz != 1 : print('nz not zero! taking k=zero ...')

    #create eight (nx,ny) arrays with all differences between one value and its neighbours
    neigh = np.zeros([8,nx,ny])

    neigh[0,:,:] = (arr[aa:nx+aa, bb:ny+bb] - arr[0:nx, 0:ny])
    neigh[2,:,:] = (arr[nx-bb:2*nx-bb, aa:ny+aa] - arr[0:nx, 0:ny])
    neigh[4,:,:] = (arr[nx-aa:2*nx-aa, ny-bb:2*ny-bb] - arr[0:nx, 0:ny])
    neigh[6,:,:] = (arr[bb:nx+bb, ny-aa:2*ny-aa] - arr[0:nx, 0:ny])

    if aa > bb :
      neigh[1,:,:] = (arr[ aa-bb:nx+aa-bb, aa+bb:ny+aa+bb] - arr[0:nx, 0:ny])
      neigh[3,:,:] = (arr[ nx-aa-bb:2*nx-aa-bb, aa-bb:ny+aa-bb] - arr[0:nx, 0:ny])
      neigh[5,:,:] = (arr[ nx-aa+bb:2*nx-aa+bb, ny-aa-bb:2*ny-aa-bb] - arr[0:nx, 0:ny])
      neigh[7,:,:] = (arr[ aa+bb:nx+aa+bb, ny-aa+bb:2*ny-aa+bb] - arr[0:nx, 0:ny])
    else : 
      neigh[1,:,:] = (arr[ nx+aa-bb:2*nx+aa-bb, aa+bb:ny+aa+bb] - arr[0:nx, 0:ny])
      neigh[3,:,:] = (arr[ nx-aa-bb:2*nx-aa-bb, ny+aa-bb:2*ny+aa-bb] - arr[0:nx, 0:ny])
      neigh[5,:,:] = (arr[-aa+bb:nx-aa+bb, ny-aa-bb:2*ny-aa-bb] - arr[0:nx, 0:ny])
      neigh[7,:,:] = (arr[aa+bb:nx+aa+bb, -aa+bb:ny-aa+bb] - arr[0:nx, 0:ny])

    #calculate which neighbors are to be discarded
    #ne_diags = np.transpose(np.argwhere(np.absolute(neigh[1,:,:]) > np.absolute(neigh[2,:,:] - neigh[0,:,:])))
    #nw_diags = np.transpose(np.argwhere(np.absolute(neigh[3,:,:]) >= np.absolute(neigh[4,:,:] - neigh[2,:,:])))
    #sw_diags = np.transpose(np.argwhere(np.absolute(neigh[5,:,:]) > np.absolute(neigh[6,:,:] - neigh[4,:,:])))
    #se_diags = np.transpose(np.argwhere(np.absolute(neigh[7,:,:]) >= np.absolute(neigh[0,:,:] - neigh[6,:,:])))
    
    plane_params_x = np.zeros([4,nx,ny])
    plane_params_y = np.zeros([4,nx,ny])

    plane_params_x[0,:,:] =  neigh[0,:,:] 
    plane_params_x[1,:,:] =  neigh[1,:,:] -neigh[2,:,:]
    plane_params_x[2,:,:] =  neigh[1,:,:]  
    plane_params_x[3,:,:] = -neigh[3,:,:] +neigh[2,:,:]
    
    plane_params_y[0,:,:] = -neigh[0,:,:] +neigh[1,:,:]
    plane_params_y[1,:,:] =  neigh[2,:,:] 
    plane_params_y[2,:,:] = -neigh[3,:,:] 
    plane_params_y[3,:,:] = -neigh[1,:,:] +neigh[2,:,:]

    plane_params_x /= self.meta['dx'] 
    plane_params_y /= self.meta['dy'] 
    
    cos_index = np.ones([2,nx,ny],dtype=float)
    
    cos_index[0,:,:] += np.multiply(plane_params_x[0,:,:] ,plane_params_x[1,:,:])
    cos_index[0,:,:] += np.multiply(plane_params_y[0,:,:] ,plane_params_y[1,:,:])
    cos_index[0,:,:] = np.divide(cos_index[0,:,:], np.sqrt(np.square(plane_params_x[0,:,:]) + np.square(plane_params_y[0,:,:])))
    cos_index[0,:,:] = np.divide(cos_index[0,:,:], np.sqrt(np.square(plane_params_x[1,:,:]) + np.square(plane_params_y[1,:,:])))
    
    cos_index[1,:,:] += np.multiply(plane_params_x[2,:,:] ,plane_params_x[3,:,:])
    cos_index[1,:,:] += np.multiply(plane_params_y[2,:,:] ,plane_params_y[3,:,:])
    cos_index[1,:,:] = np.divide(cos_index[1,:,:], np.sqrt(np.square(plane_params_x[2,:,:]) + np.square(plane_params_y[2,:,:])))
    cos_index[1,:,:] = np.divide(cos_index[1,:,:], np.sqrt(np.square(plane_params_x[3,:,:]) + np.square(plane_params_y[3,:,:])))
    
    diags_sel = (np.absolute(cos_index[1,:,:]) > np.absolute(cos_index[0,:,:]))
    diags_sel = np.tile(diags_sel,(2,2))
    
    ne_diags = np.transpose(np.argwhere(diags_sel[0:nx,0:ny]))
    nw_diags = np.transpose(np.argwhere(np.invert(diags_sel[nx-1:2*nx-1,0:ny])))
    sw_diags = np.transpose(np.argwhere(diags_sel[nx-1:2*nx-1,ny-1:2*ny-1]))
    se_diags = np.transpose(np.argwhere(np.invert(diags_sel[0:nx,ny-1:2*ny-1])))

    #calculate Delta plus and minus, careful about discarded neighbors
    neigh[1,ne_diags[0],ne_diags[1]] = 0.
    neigh[3,nw_diags[0],nw_diags[1]] = 0.
    neigh[5,sw_diags[0],sw_diags[1]] = 0.
    neigh[7,se_diags[0],se_diags[1]] = 0.

    pos_neighs = np.maximum(neigh, np.zeros([8,nx,ny]))
    Delta_plus  = np.sum(pos_neighs,axis=0)
    Delta_minus = np.sum(pos_neighs-neigh,axis=0)

    #select positive neighbors and calculate sign changes, careful about discarded neighbors
    good_neighs = (neigh > 0)

    good_neighs[1,ne_diags[0],ne_diags[1]] = np.logical_and(good_neighs[0,ne_diags[0],ne_diags[1]],good_neighs[2,ne_diags[0],ne_diags[1]])
    good_neighs[3,nw_diags[0],nw_diags[1]] = np.logical_and(good_neighs[2,nw_diags[0],nw_diags[1]],good_neighs[4,nw_diags[0],nw_diags[1]])
    good_neighs[5,sw_diags[0],sw_diags[1]] = np.logical_and(good_neighs[4,sw_diags[0],sw_diags[1]],good_neighs[6,sw_diags[0],sw_diags[1]])
    good_neighs[7,se_diags[0],se_diags[1]] = np.logical_and(good_neighs[6,se_diags[0],se_diags[1]],good_neighs[0,se_diags[0],se_diags[1]])
    good_neighs = np.diff(good_neighs,axis=0)

    sign_change = np.sum(good_neighs,axis=0) + 1
    sign_change -= sign_change % 2

    #calculate coordinates of Oa points (maxima), Ob points (minima) and X points (saddles)
    coord_Oa = np.transpose(np.argwhere(np.logical_and(Delta_plus < thr*Delta_minus, sign_change == 0)))
    coord_Ob = np.transpose(np.argwhere(np.logical_and(Delta_minus < thr*Delta_plus, sign_change == 0)))
    coord_X = np.transpose(np.argwhere(sign_change > 2)) #np.logical_and(Delta_plus-Delta_minus < thr*(Delta_plus+Delta_minus), sign_change > 2)))

    return coord_Oa, coord_Ob, coord_X

  #------------------------------------------------------------
  def find_critical_starlike_2d(self,  
      tar_var,  
      thr=1e-1):
    """ 
    ------------------------------------------------------------------------------------
       finds zeros in 2d array by triangulation on star of neighbours (strange version of find_critical_squarely_2d)
    ------------------------------------------------------------------------------------
    tar_var      [fibo_var] target field
    thr=1e-1     [float>0 ] threshold value for maxima/minima detection
    ------------------------------------------------------------------------------------
    coord_Oa, coord_Ob, coord_X   [fibo_coor,fibo_coor,fibo_coor]
    ------------------------------------------------------------------------------------
    """    

    nx,ny,nz = self.meta['nnn']
    arr = np.tile(self.get_data(tar_var)[:,:,0],(2,2))
    if nz != 1 : print('nz not zero! taking k=zero ...')
    
    #create sixteen (nx,ny) arrays with all differences between one value and its neighbours
    neigh = np.zeros([16,nx,ny])
    neigh[0,:,:] = (arr[1:nx+1,0:ny] - arr[0:nx,0:ny])
    neigh[2,:,:] = (arr[1:nx+1,1:ny+1] - arr[0:nx,0:ny])
    neigh[4,:,:] = (arr[0:nx,1:ny+1] - arr[0:nx,0:ny])
    neigh[6,:,:] = (arr[nx-1:2*nx-1,1:ny+1] - arr[0:nx,0:ny])
    neigh[8,:,:] = (arr[nx-1:2*nx-1,0:ny] - arr[0:nx,0:ny])
    neigh[10,:,:] = (arr[nx-1:2*nx-1,ny-1:2*ny-1] - arr[0:nx,0:ny])
    neigh[12,:,:] = (arr[0:nx,ny-1:2*ny-1] - arr[0:nx,0:ny])
    neigh[14,:,:] = (arr[1:nx+1,ny-1:2*ny-1] - arr[0:nx,0:ny])
    neigh[1,:,:] = (arr[2:nx+2,1:ny+1] - arr[0:nx,0:ny])
    neigh[3,:,:] = (arr[1:nx+1,2:ny+2] - arr[0:nx,0:ny])
    neigh[5,:,:] = (arr[nx-1:2*nx-1,2:ny+2] - arr[0:nx,0:ny])
    neigh[7,:,:] = (arr[nx-2:2*nx-2,1:ny+1] - arr[0:nx,0:ny])
    neigh[9,:,:] = (arr[nx-2:2*nx-2,ny-1:2*ny-1] - arr[0:nx,0:ny])
    neigh[11,:,:] = (arr[nx-1:2*nx-1,ny-2:2*ny-2] - arr[0:nx,0:ny])
    neigh[13,:,:] = (arr[1:nx+1,ny-2:2*ny-2] - arr[0:nx,0:ny])
    neigh[15,:,:] = (arr[2:nx+2,ny-1:2*ny-1] - arr[0:nx,0:ny])

    #calculate Delta plus and minus
    pos_neighs = np.maximum(neigh, np.zeros([16,nx,ny]))
    Delta_plus  = np.sum(pos_neighs,axis=0)
    Delta_minus = np.sum(pos_neighs-neigh,axis=0)

    #select positive neighbors and calculate sign changes
    pos_sel = pos_neighs > 0. 
    sign_change = np.sum(np.diff(pos_sel,axis=0),axis=0) + 1
    sign_change -= sign_change % 2

    #calculate coordinates of Oa points (maxima), Ob points (minima) and X points (saddles)
    coord_Oa = np.transpose(np.argwhere(np.logical_and(Delta_plus < thr*Delta_minus, sign_change == 0)))
    coord_Ob = np.transpose(np.argwhere(np.logical_and(Delta_minus < thr*Delta_plus, sign_change == 0)))
    coord_X = np.transpose(np.argwhere(sign_change > 2))

    return coord_Oa, coord_Ob, coord_X




  #------------------------------------------------------------  
  def count_perm(self,
      equi_mat):
    """ 
    ------------------------------------------------------------------------------------
       count permutations of 
    ------------------------------------------------------------------------------------
    equi_mat      [matrix] equivalence matrix
    ------------------------------------------------------------------------------------
    perm_number   [int] number of permutations
    ------------------------------------------------------------------------------------
    """  

    num = np.shape(equi_mat)[0]
    
    for r in range(num):
      equi_mat[0,r] = 0
      equi_mat[r,0] = 0
      equi_mat[r,r] = 1

    #-repetita-juvant-
    for rep in range(num//4):
      equi_class = []
      for r in range(num):
        equi_class.append(np.nonzero(equi_mat[r,:])[0])
        #print(equi_class[r])

      nonzeros = np.nonzero(equi_mat)[0]
      for r,c in itt.product(nonzeros,nonzeros):
        row_class = equi_class[r][equi_class[r] > c]
        col_class = equi_class[c][equi_class[c] > r]
        for row,col in itt.product(col_class,row_class):
            equi_mat[row,col] = 1
    #-finis-

    equi_class = []
    for r in range(num):
      equi_class.append(np.nonzero(equi_mat[r,:])[0])
      #print(equi_class[r])

    nonzeros = np.nonzero(equi_mat)[0]
    for r,c in itt.product(nonzeros,nonzeros):
      row_class = equi_class[r][equi_class[r] > c]
      col_class = equi_class[c][equi_class[c] > r]
      for row,col in itt.product(col_class,row_class):
          equi_mat[row,col] = 0

    for r in range(num):
      equi_mat[r,r] = 0

    #return equi_mat
    return num -1- np.sum(equi_mat)//2

  #------------------------------------------------------------  
  def find_spot_number(self,
      tar_spots, 
      periodic = True, 
      struct_ind = [2,2]):
    """ 
    ------------------------------------------------------------------------------------
       finds the number of connected spots in a 2d toroidal environment
    ------------------------------------------------------------------------------------
    tar_spots           [fibo_var] binary mask individuating the spots
    periodic = True     [bool] periodic boundary conditions?
    struct_ind = [2,2]  [int>0,int>0] binary structure indeces to characterize the blurring
    ------------------------------------------------------------------------------------
    spot_num            [int>0] 
    ------------------------------------------------------------------------------------
    """  
    
    nx,ny,nz = self.meta['nnn']
    #nx = np.shape(tar_spots)[0]
    #ny = np.shape(tar_spots)[1]

    if np.all(struct_ind) : 
      #print(struct_ind)
      spot_labels = ndm.measurements.label(tar_spots,structure=ndm.generate_binary_structure(struct_ind[0],struct_ind[1]))
    else : 
      spot_labels = ndm.measurements.label(tar_spots)
    spot_num = spot_labels[1]

    if periodic : 
      sidex_selector = np.zeros((2,nx),dtype=int)
      sidex_selector[0,:] = (spot_labels[0][0,:]).astype(int)
      sidex_selector[1,:] = (spot_labels[0][-1,:]).astype(int)

      sidey_selector = np.zeros((2,ny),dtype=int)
      sidey_selector[0,:] = (spot_labels[0][:,0]).astype(int)
      sidey_selector[1,:] = (spot_labels[0][:,-1]).astype(int)
  
      spot_equi_mat = np.zeros((spot_num+1,spot_num+1),dtype=int)
      for i in range(nx):
        spot_equi_mat[sidex_selector[0,i],sidex_selector[1,i]] = 1
        spot_equi_mat[sidex_selector[1,i],sidex_selector[0,i]] = 1
      for j in range(ny):
        spot_equi_mat[sidey_selector[0,j],sidey_selector[1,j]] = 1
        spot_equi_mat[sidey_selector[1,j],sidey_selector[0,j]] = 1

      return self.count_perm(spot_equi_mat)

    else : 
      return spot_num

  #------------------------------------------------------------  
  def find_region_number(self,
      tar_var,
      levels,
      periodic = True,
      struct_ind = [2,2]):
    """ 
    ------------------------------------------------------------------------------------
       finds the number of connected spots in a 2d toroidal environment
    ------------------------------------------------------------------------------------
    tar_var             [fibo_var] target variable
    levels              [list of floats]  
    periodic = True     [bool] periodic boundary conditions?
    struct_ind = [2,2]  [int>0,int>0] binary structure indeces to characterize the blurring
    ------------------------------------------------------------------------------------
    spot_num            [int>0] 
    ------------------------------------------------------------------------------------
    """

    array = self.get_data(tar_var)[:,:,0]
    reg_num_above = []
    reg_num_below = []

    for lvl in levels:

      #calculate the above bulk selector!
      spot_selector = (array > lvl)
      regions = self.find_spot_number(spot_selector,periodic=periodic,struct_ind=struct_ind)
      reg_num_above.append(regions)

      #calculate the below bulk selector!
      spot_selector = (array < lvl)
      regions = self.find_spot_number(spot_selector,periodic=periodic,struct_ind=struct_ind)
      reg_num_below.append(regions)
    
    return reg_num_above, reg_num_below

  #def find_extrema_number(self, #finds the number of well-separated extrema 
      #coo_ext):     #coordinates of all extremal points you wanna check for being close
  
  
  #--------------------routines-for-plotting--------------------
  #------------------------------------------------------------  
  def draw_canvas(self,
        tar_labs,  
        tar_dims):
    """ 
    ------------------------------------------------------------------------------------
       prepares canvas for drawing
    ------------------------------------------------------------------------------------
    tar_labs    [list of list of str] matrix of labels
    tar_dims    [int,int OR 'line' OR 'cont'] subplot proportions 
    ------------------------------------------------------------------------------------
    multi_sub   [blank figure] 
    ------------------------------------------------------------------------------------
    """ 

    x_fig_num, y_fig_num = np.shape(tar_labs)
    grid = mpl.pyplot.GridSpec(x_fig_num, y_fig_num, wspace=0.2, hspace=0.1)

    if tar_dims is 'line' : multi_fig = mpl.pyplot.figure(figsize=(8*y_fig_num,3*x_fig_num))
    elif tar_dims is 'cont' : multi_fig = mpl.pyplot.figure(figsize=(8*y_fig_num,6*x_fig_num))
    else : multi_fig = mpl.pyplot.figure(figsize=(tar_dims[0]*y_fig_num,tar_dims[1]*x_fig_num))

    multi_fig.patch.set_facecolor('white')

    subn = 0  
    for ii in range(x_fig_num):
      for jj in range(y_fig_num):
        if tar_labs[ii][jj] != None : 
          multi_fig.add_subplot(grid[ii,jj])
          mpl.pyplot.ylabel(tar_labs[ii][jj])
          #mpl.pyplot.set_xlim(len_x*range_x[0]/self.meta['nx'],self.meta['xl']*range_x[1]/self.meta['nx'])
          #mpl.pyplot.set_ylim(len_y*range_y[0]/self.meta['ny'],self.meta['xl']*range_y[1]/self.meta['ny'])

    multi_sub = multi_fig.get_axes()
    return multi_sub

  #------------------------------------------------------------  TO BE TESTED!!!!!!!!
  def draw_lins(self,
        multi_sub,
        tar_vars,
        line_kwargs,
        range_pts = None,
        line_len = None,
        axes_log = [False,False]): 
    """ 
    ------------------------------------------------------------------------------------
       draws classic plots of one-dimensional variables
    ------------------------------------------------------------------------------------
    multi_sub                [list of (sub)plot] to draw on
    tar_vars                 [list of list of fibo_cut] target variables
    line_kwargs              [list of list of str] how you want each line ('ro','g.',...)
    range_pts = None         [None OR int,int] range to extract
    line_len = None,         [None OR float] nominal length of abscisse axis
    axes_log = [False,False] [bool,bool] do
    ------------------------------------------------------------------------------------
    """

    line_pts = len(self.get_data(tar_vars[0][0]))
    if range_pts == None : range_pts = [0,line_pts-1]
    if line_len == None : line_len = float(range_pts[1]-range_pts[0])  

    for plt_num, plts in enumerate(multi_sub):
      for var_num, var in enumerate(tar_vars[plt_num]) :

        x_axis = np.linspace(range_pts[0]*line_len/line_pts,range_pts[1]*line_len/line_pts,range_pts[1]-range_pts[0],endpoint=False)
        y_axis = self.get_data(var)[range_pts[0]:range_pts[1]]
        plts.plot(x_axis,y_axis,line_kwargs[plt_num][var_num])

      plts.set_xlim(x_axis[0],x_axis[-1])
      plts.grid(True,which='both')

      if axes_log[0] : plts.set_xscale('log')
      if axes_log[1] : plts.set_yscale('log')
      else : plts.axhline(0, color='slategrey')
      #if ii+1 != x_fig_num : mpl.pyplot.setp(plts.get_xticklabels(), visible=False)

  #------------------------------------------------------------    TO BE TESTED!!!!!!!! - by the way all these could be 
  def draw_cuts(self,
        multi_sub,
        tar_vars, 
        line_kwargs, 
        range_x = None,
        range_y = None,
        range_z = None,
        line_pts = None,
        axes_log = [False,False]): 
    """ 
    ------------------------------------------------------------------------------------
       draws classic plots of field cuts
    ------------------------------------------------------------------------------------
    multi_sub                 [list of (sub)plot] to draw on
    tar_vars                  [list of list of fibo_var] you want to plot
    line_kwargs               [list of list of str] how do you want each line
    range_x = None            [None OR int,int] x coords of first and last point 
    range_y = None            [None OR int,int] y coords of first and last point
    range_z = None            [None OR int,int] z coords of first and last point
    line_pts = None           [None OR int] number of points sampled
    axes_log = [False,False]  [bool,bool] do you want plot axes log scales?
    ------------------------------------------------------------------------------------
    """  

    nx,ny,nz = self.meta['nnn'] #np.shape(self.get_data(tar_vars[0][0]))
    if range_x == None : range_x = [0,nx-1]
    if range_y == None : range_y = [0,ny-1]
    if range_z == None : range_z = [0,nz-1]
    if line_pts == None : 
      line_pts = int(np.sqrt((range_x[1]-range_x[0])**2+(range_y[1]-range_y[0])**2+(range_z[1]-range_z[0])**2))

    for plt_num, plts in enumerate(multi_sub):
      for var_num, var in enumerate(tar_vars[plt_num]) :

        x_axis = self.calc_line(line_pts,range_x,range_y,range_z)
        y_axis = self.extract_line(var,line_pts,range_x,range_y,range_z)
        plts.plot(x_axis,y_axis,line_kwargs[plt_num][var_num])

      plts.set_xlim(x_axis[0],x_axis[-1])
      plts.grid(True,which='both')

      if axes_log[0] : plts.set_xscale('log')
      if axes_log[1] : plts.set_yscale('log')
      else : plts.axhline(0, color='slategrey')
      #if ii+1 != x_fig_num : mpl.pyplot.setp(plts.get_xticklabels(), visible=False)


  #------------------------------------------------------------  
  def draw_contour(self,    
        multi_sub, 
        tar_vars,  
        range_x = None,
        range_y = None,
        cut_z = 0,
        styles = ['k','-'],  
        levels = None,
        levbar = False, 
        alpha = 0.6):
    """ 
    ------------------------------------------------------------------------------------
       draws empty contours of the chosen fields
    ------------------------------------------------------------------------------------
    multi_sub          [list of (sub)plot] in you are drawing
    tar_vars           [list of fibo_var] you want to plot
    range_x = None     [None OR int,int] x range 
    range_y = None     [None OR int,int] y range
    cut_z = 0          [int] z coordinate of the cut
    styles = ['k','-'] [str,str] how do you want each line?
    levels = None      [None OR list of float]  levels to be drawn
    levbar = False     [bool] do you want the level bar?
    alpha = 0.6        [float] transparency for the colour-filled plots
    ------------------------------------------------------------------------------------
    """  

    nx,ny,nz = np.shape(self.get_data(tar_vars[0]))
    if range_x == None : range_x = [0,nx]
    if range_y == None : range_y = [0,ny]

    len_x = self.meta['xl']
    len_y = self.meta['yl']

    x_coord = np.linspace(len_x*range_x[0]/nx,len_x*range_x[1]/nx,range_x[1]-range_x[0])
    y_coord = np.linspace(len_y*range_y[0]/ny,len_y*range_y[1]/ny,range_y[1]-range_y[0])
    x_coord, y_coord = np.meshgrid(x_coord,y_coord)

    for plt_num, plts in enumerate(multi_sub):

      targ = np.transpose(self.get_data(tar_vars[plt_num])[range_x[0]:range_x[1],range_y[0]:range_y[1],cut_z])
      #if levlog: norm = mpl.colors.LogNorm(vmin=targ.min(), vmax=targ.max())
      #else : norm = mpl.colors.Normalize(vmin=targ.min(), vmax=targ.max())
      cpts = plts.contour(x_coord,y_coord,targ,levels=levels,colors=styles[0],linestyles=styles[1],alpha=alpha)#,norm=norm
      if levbar: mpl.pyplot.colorbar(cpts,aspect=30,ax=plts)

  #------------------------------------------------------------  
  def draw_heatmap(self,    
        multi_sub,
        tar_vars_a,
        tar_vars_b,
        plot_range_a,
        plot_range_b,
        tar_pnts = None,
        bin_number = [100,100],
        cmap = None,
        levbar = False,
        alpha = 0.6):
    """ 
    ------------------------------------------------------------------------------------
       draws heatmap of correlations between 
    ------------------------------------------------------------------------------------
    multi_sub               [list of (sub)plot] in you are drawing
    tar_vars_a              [list of fibo.data] you want to plot
    tar_vars_b              [list of fibo.data] you want to plot
    plot_range_a            [int,int] range of a values in the plot
    plot_range_b            [int,int] range of b values in the plot
    tar_pnts = None         [None OR fibo.pnts] over which you want to work
    bin_number = [100,100]  [int,int] number of bins in x and y
    cmap = None             [colormap] colormap
    levbar = False          [bool] do you want the level bar?
    alpha = 0.6             [float] transparency for the colour-filled plots
    ------------------------------------------------------------------------------------
    """
    
    if tar_pnts is None : tar_pnts = list(self.find_box())
    else : tar_pnts = list(self.get_pnts(tar_pnts))
    
    bins = {}
    bins[0] = np.linspace(plot_range_a[0],plot_range_a[1],bin_number[0])
    bins[1] = np.linspace(plot_range_b[0],plot_range_b[1],bin_number[1])
    
    for plt_num, plts in enumerate(multi_sub):
      allpoints = [ self.get_data(tar_vars_a[plt_num])[tar_pnts] , self.get_data(tar_vars_b[plt_num])[tar_pnts] ]
      allpoints =   np.transpose(np.vstack(allpoints))

      hist, edges = np.histogramdd(allpoints,bins=bins)
      cpts = plts.pcolor(edges[1],edges[0],hist, cmap=cmap,alpha=alpha)

      plts.grid(True,which='both')
      if levbar: mpl.pyplot.colorbar(cpts,aspect=30,ax=plts)


  #------------------------------------------------------------  
  def draw_spotted(self,    
        multi_sub,
        tar_vars,
        range_x = None,
        range_y = None,
        cut_z = 0,
        styles = [None,[None]],
        cmap = None,
        levels = None,
        levbar = False,
        alpha = 0.6):
    """ 
    ------------------------------------------------------------------------------------
       draws filled contours of the chosen fields
    ------------------------------------------------------------------------------------
    multi_sub              [list of (sub)plot] in you are drawing
    tar_vars               [list of fibo_var] you want to plot
    range_x = None         [None OR int,int] x range 
    range_y = None         [None OR int,int] y range 
    cut_z = 0              [int] z coordinate of the cut
    styles = [None,[None]] [color, list of hatches] for your plot
    cmap = None            [colormap] colormap
    levels = None          [None OR list of float] levels to be drawn
    levbar = False         [bool] do you want the level bar?
    alpha = 0.6            [float] transparency for the colour-filled plots
    ------------------------------------------------------------------------------------
    """

    nx,ny,nz = self.meta['nnn']  
    if range_x == None : range_x = [0,nx]
    if range_y == None : range_y = [0,ny]

    #len_x = self.meta['xl']
    #len_y = self.meta['yl']
    #x_coord = np.linspace(len_x*range_x[0]/nx,len_x*range_x[1]/nx,range_x[1]-range_x[0])
    #y_coord = np.linspace(len_y*range_y[0]/ny,len_y*range_y[1]/ny,range_y[1]-range_y[0])
    #x_coord, y_coord = np.meshgrid(x_coord,y_coord)

    x_coord, y_coord, z_coord = self.calc_axes(range_x,range_y,[cut_z,cut_z+1],coor_like=True,mesh_like=True)
    x_coord, y_coord = x_coord[:,:,0], y_coord[:,:,0]

    for plt_num, plts in enumerate(multi_sub):
      targ = np.transpose(self.get_data(tar_vars[plt_num])[range_x[0]:range_x[1],range_y[0]:range_y[1],cut_z])
      #if levlog: norm = mpl.colors.LogNorm(vmin=targ.min(), vmax=targ.max())
      #else : norm = mpl.colors.Normalize(vmin=targ.min(), vmax=targ.max())
      cpts = plts.contourf(x_coord,y_coord,targ,levels=levels,cmap=cmap,colors=styles[0],hatches=styles[1],alpha=alpha,extend='both')#,norm=norm
      if levbar: mpl.pyplot.colorbar(cpts,aspect=30,ax=plts)


  #------------------------------------------------------------  
  def draw_streams(self,    
        multi_sub,
        tar_var_x,
        tar_var_y,
        range_x = None,
        range_y = None,
        cut_z = 0,
        styles = ['w',1],
        density = [5,5],
        alpha = 0.6):
    """
    ------------------------------------------------------------------------------------
       draws streamlines of the chosen fields
    ------------------------------------------------------------------------------------
    multi_sub              [list of (sub)plot] in you are drawing
    tar_var_x              [list of fibo_var] you want to plot x
    tar_var_y              [list of fibo_var] you want to plot y
    range_x = None         [None OR int,int] x range 
    range_y = None         [None OR int,int] y range 
    cut_z = 0              [int] z coordinate of the cut
    styles = ['w',1]       [str,int] for lines, which color do you want? how thick?
    density = [5,5]        [float,float] line density in x,y
    levels = None          [None OR list of float] levels to be drawn
    levbar = False         [bool] do you want the level bar?
    alpha = 0.6            [float] transparency for the colour-filled plots
    ------------------------------------------------------------------------------------
    """

    nx,ny,nz = self.meta['nnn']
    if range_x == None : range_x = [0,nx]
    if range_y == None : range_y = [0,ny]

    #len_x = self.meta['xl']
    #len_y = self.meta['yl']
    #x_coord = np.linspace(len_x*range_x[0]/nx,len_x*range_x[1]/nx,range_x[1]-range_x[0])
    #y_coord = np.linspace(len_y*range_y[0]/ny,len_y*range_y[1]/ny,range_y[1]-range_y[0])
    #x_coord, y_coord = np.meshgrid(x_coord,y_coord)

    x_coord, y_coord, z_coord = self.calc_axes(range_x,range_y,[cut_z,cut_z+1],coor_like=True,mesh_like=True)
    x_coord, y_coord = x_coord[:,:,0], y_coord[:,:,0]

    for plt_num, plts in enumerate(multi_sub):
      tar_x = np.transpose(self.get_data(tar_var_x[plt_num])[range_x[0]:range_x[1],range_y[0]:range_y[1],cut_z])
      tar_y = np.transpose(self.get_data(tar_var_y[plt_num])[range_x[0]:range_x[1],range_y[0]:range_y[1],cut_z])
      cpts = plts.streamplot(x_coord,y_coord,tar_x,tar_y,color=styles[0],linewidth=styles[1],density=density)

  #------------------------------------------------------------  
  def draw_quivers(self,    
        multi_sub,  
        tar_var_x,
        tar_var_y, 
        range_x = None,
        range_y = None,
        cut_z = 0,
        styles = ['k',1.],
        headstyle = [1.,5.,1.],
        density = [50,50],
        alpha = 0.6):
    """
    ------------------------------------------------------------------------------------
       draws arrows to represent the chosen fields
    ------------------------------------------------------------------------------------
    multi_sub               [list of (sub)plot] in you are drawing
    tar_var_x               [list of fibo_var] you want to plot x
    tar_var_y               [list of fibo_var] you want to plot y
    range_x = None          [None OR int,int] x range 
    range_y = None          [None OR int,int] y range 
    cut_z = 0               [int] z coordinate of the cut
    styles = ['k',1.],      [color str, float] arrow color, arrow thickness
    headstyle = [1.,5.,1.]  [float,float,float] parameters for head style
    density = [50,50]       [int,int] horizontal, vertical density 
    alpha = 0.6             [float] arrow transparency 
    ------------------------------------------------------------------------------------
    """

    nx,ny,nz = self.meta['nnn']
    if range_x == None : range_x = [0,nx]
    if range_y == None : range_y = [0,ny]

    x_mesh, y_mesh, z_mesh = self.calc_axes(range_x,range_y,[0,1],coor_like=True,mesh_like=True)
    x_mesh = self.lower_res(x_mesh,density[0],density[1],1)[:,:,0]
    y_mesh = self.lower_res(y_mesh,density[0],density[1],1)[:,:,0]

    for plt_num, plts in enumerate(multi_sub):
      tar_x = self.get_data(tar_var_x[plt_num])[range_x[0]:range_x[1],range_y[0]:range_y[1],cut_z:cut_z+1]
      tar_y = self.get_data(tar_var_y[plt_num])[range_x[0]:range_x[1],range_y[0]:range_y[1],cut_z:cut_z+1]
      tar_x = np.transpose(self.lower_res(tar_x,density[0],density[1],1)[:,:,0])
      tar_y = np.transpose(self.lower_res(tar_y,density[0],density[1],1)[:,:,0])
      cpts = plts.quiver(y_mesh,x_mesh,tar_x,tar_y,color=styles[0],width=styles[1],headwidth=headstyle[0],headlength=headstyle[1],headaxislength=headstyle[2],alpha=alpha,minlength=1e-5)

    #if range_x == None : range_x = [0,nx]
    #if range_y == None : range_y = [0,ny]
    #rx0, rx1 = range_x[0]//density[0] +1, range_x[1]//density[0] +1
    #ry0, ry1 = range_y[0]//density[1] +1, range_y[1]//density[1] +1

    #x_mesh, y_mesh, z_mesh = self.calc_axes(range_z=[0,1],coor_like=True,mesh_like=True)

    #x_mesh = self.lower_res(x_mesh,density[0],density[1],1)
    #x_mesh = x_mesh[rx0:rx1,ry0:ry1,0]
    #y_mesh = self.lower_res(y_mesh,density[0],density[1],1)
    #y_mesh = y_mesh[rx0:rx1,ry0:ry1,0]

    #for plt_num, plts in enumerate(multi_sub):

      #tar_x = self.lower_res(tar_var_x[plt_num],density[0],density[1],1)
      #tar_x = np.transpose(tar_x[rx0:rx1,ry0:ry1,0])
      #tar_y = self.lower_res(tar_var_y[plt_num],density[0],density[1],1)
      #tar_y = np.transpose(tar_y[rx0:rx1,ry0:ry1,0])

      #cpts = plts.quiver(x_mesh,y_mesh,tar_x,tar_y,color=styles[0],width=styles[1],headwidth=headstyle[0],headlength=headstyle[1],headaxislength=headstyle[2],alpha=alpha,minlength=1e-5)

  #------------------------------------------------------------  
  def draw_scatter(self,    
        multi_sub,
        tar_pnts,
        range_x = None,
        range_y = None,
        styles = ['r',8.],
        alpha = 0.6):
    """
    ------------------------------------------------------------------------------------
       draws scatterplots of the chosen coordinates
    ------------------------------------------------------------------------------------
    multi_sub              [list of (sub)plot] in you are drawing
    tar_pnts               [list of fibo.pnts] you want to plot
    range_x = None         [None OR int,int] x range 
    range_y = None         [None OR int,int] y range 
    styles = ['r',8.]      [str,float] points: which color do you want? how big?
    alpha = 0.6            [float] transparency for the colour-filled plots
    ------------------------------------------------------------------------------------
    """

    nx,ny,nz = self.meta['nnn']
    #all_max = max([np.max(self.get_data(tar_coor[sub_plt])) for sub_plt in range(len(multi_sub))])
    if range_x == None : range_x = [0,nx]# all_max]
    if range_y == None : range_y = [0,ny] #all_max]

    dx,dy,dz = self.meta['ddd']
    len_x0, len_x1 = range_x[0]*dx, range_x[1]*dx
    len_y0, len_y1 = range_y[0]*dy, range_y[1]*dy
    
    for plt_num, plts in enumerate(multi_sub):
      all_coor = self.get_pnts(tar_pnts[plt_num])
      all_coor[0] =  all_coor[0]*dx
      all_coor[1] =  all_coor[1]*dy
      all_coor = np.transpose(all_coor)

      box_lims = [np.array([len_x0,len_y0]),np.array([len_x1,len_y1])]
      all_indx = np.all((box_lims[0] <= all_coor) & (all_coor <= box_lims[1]), axis = 1)

      plts.scatter(all_coor[all_indx,0],all_coor[all_indx,1],c=styles[0],s=styles[1],alpha=alpha) 
      plts.set_xlim(len_x0,len_x1)
      plts.set_ylim(len_y0,len_y1)



  #------------------------------------------------------------ 
  #--------------------routines-for-printing--------------------
  #------------------------------------------------------------  
  def print_vtk_scal(self,  
      address,
      tar_name,
      tar_var,
      digits = '%.9f',
      double_y = False, 
      aka = None):
    """ 
    ------------------------------------------------------------------------------------
       prints .vtk file of your tar_var
    ------------------------------------------------------------------------------------
    address           [address] address of printing
    tar_name          [str] name for the printed variable (inside the file)
    tar_var           [fibo.data] target variable of the procedure
    digits = '%.9f'   [format str] format you will use for printing
    double_y = False  [bool] if you want to print twice the box (two boxes close in y)
    aka = None        [None or str] if not None you give the full name of the .vtk file
    ------------------------------------------------------------------------------------
    """  

    #determine the coefficients
    nx,ny,nz = self.meta['nnn'] 
    dx,dy,dz = self.meta['ddd'] 

    #clean the tar_name from possible characters that mess up with paraview's calculator
    tar_name = re.sub('[()!@#$.,*^]', '', tar_name)
    if tar_name[0].isdigit() : tar_name = '_'+tar_name
    #this is the way you create names of the printed files, if you did not mess up with my standard ...
    if (aka == None) :
      filen = os.path.join(address,self.fibo_name+'_'+tar_name+'.vtk')
    else : filen = os.path.join(address,aka+'.vtk')

    if double_y : ny = ny*2

    wf = open(filen, 'w')
    wf.write('# vtk DataFile Version 1.0 \n')
    wf.write(tar_var+' from '+self.fibo_name+'\n')
    wf.write('ASCII'+'\n')
    wf.write('DATASET STRUCTURED_POINTS'+'\n')
    wf.write('DIMENSIONS'+' '+str(nx)+' '+str(ny)+' '+str(nz)+'\n')
    wf.write('ORIGIN  0.0  0.0  0.0'+'\n')
    wf.write('SPACING'+' '+str(dx)+' '+str(dy)+' '+str(dz)+'\n')
    wf.write('                '+'\n')
    wf.write('POINT_DATA'+' '+str(nx*ny*nz)+'\n')
    wf.write('SCALARS    '+tar_name+'    float  1'+'\n')
    wf.write('LOOKUP_TABLE default'+'\n')

    for iz in range(0, nz):
      #print the first time in y
      for iy in range(0, ny):
        for ix in range(0, nx):
          to_write = digits %self.get_data(tar_var)[ix,iy,iz]
          wf.write(to_write+'\n')
          #old way: wf.write(str(vals_MCA[2,ix,iy,iz,it])+' ')
      
      if double_y :

        #print the second time in y (to keep the res spacing you can't start from zero!)
        for iy in range(0, ny):
          for ix in range(0, nx):
            to_write = digits %self.get_data(tar_var)[ix,iy,iz]
            wf.write(to_write+'\n')
            #old way: wf.write(str(vals_MCA[2,ix,iy,iz,it])+' ')
        #wf.write('\n')

    wf.close()
    print('done with the print!')

  #------------------------------------------------------------
  def print_vtk_vect(self,
      address,
      tar_name,
      tar_var_x,
      tar_var_y,
      tar_var_z,
      digits_x = '%.7f',
      digits_y = '%.7f',
      digits_z = '%.7f',
      double_y = False,  
      aka = None):
    """ 
    ------------------------------------------------------------------------------------
       prints .vtk file of your tar_var_x, tar_vr_y, tar_var_z
    ------------------------------------------------------------------------------------
    address           [address] address of printing
    tar_name          [str] name for the printed variable (inside the file)
    tar_var_x         [fibo.data] x target variable of the procedure
    tar_var_y         [fibo.data] y target variable of the procedure
    tar_var_z         [fibo.data] z target variable of the procedure
    digits_x = '%.7f' [format str] format you will use for printing tar_var_x
    digits_y = '%.7f' [format str] format you will use for printing tar_var_y
    digits_z = '%.7f' [format str] format you will use for printing tar_var_z
    double_y = False  [bool] if you want to print twice the box (two boxes close in y)
    aka = None        [None OR str] you can give the full name of the .vtk file
    ------------------------------------------------------------------------------------
    """  

    #determine the coefficients
    nx,ny,nz = self.meta['nnn'] #np.shape(self.get_data(tar_var_x))
    dx,dy,dz = self.meta['ddd'] 

    #clean the tar_name from possible characters that mess up with paraview's calculator
    tar_name = re.sub('[()!@#$.,*^]', '', tar_name)
    if tar_name[0].isdigit() : tar_name = '_'+tar_name
    #this is the way you create names of the printed files, if you did not mess up with my standard ... (and here I hope you really have not)
    if (aka == None) :
      filen = os.path.join(address,self.fibo_name+'_'+tar_name+'.vtk')
    else : filen = os.path.join(address,aka+'.vtk')

    if double_y : ny = ny*2

    wf = open(filen, 'w')
    wf.write('# vtk DataFile Version 1.0 \n')
    wf.write('('+tar_var_x+','+tar_var_y+','+tar_var_z+') from '+self.fibo_name+'\n')
    wf.write('ASCII'+'\n')
    wf.write('DATASET STRUCTURED_POINTS'+'\n')
    wf.write('DIMENSIONS'+' '+str(nx)+' '+str(ny)+' '+str(nz)+'\n')
    wf.write('ORIGIN  0.0  0.0  0.0'+'\n')
    wf.write('SPACING'+' '+str(dx)+' '+str(dy)+' '+str(dz)+'\n')
    wf.write('                '+'\n')
    wf.write('POINT_DATA'+' '+str(nx*ny*nz)+'\n')
    wf.write('VECTORS    '+tar_name+'    float'+'\n')

    for iz in range(0, nz):
      #print the first time in y
      for iy in range(0, ny):
        for ix in range(0, nx):
          to_write_x = digits_x %self.get_data(tar_var_x)[ix,iy,iz]
          to_write_y = digits_y %self.get_data(tar_var_y)[ix,iy,iz]
          to_write_z = digits_z %self.get_data(tar_var_z)[ix,iy,iz]
          wf.write(to_write_x+' \t'+to_write_y+' \t'+to_write_z+'\n')
          #old way: wf.write(str(vals_MCA[2,ix,iy,iz,it])+' ')

      if double_y :

        #print the second time in y (to keep the res spacing you can't start from zero!)
        for iy in range(0, ny):
          for ix in range(0, nx):
            to_write_x = digits_x %self.get_data(tar_var_x)[ix,iy,iz]
            to_write_y = digits_y %self.get_data(tar_var_y)[ix,iy,iz]
            to_write_z = digits_z %self.get_data(tar_var_z)[ix,iy,iz]
            wf.write(to_write_x+' \t'+to_write_y+' \t'+to_write_z+'\n')
            #old way: wf.write(str(vals_MCA[2,ix,iy,iz,it])+' ')
        #wf.write('\n')

    wf.close()
    print('done with the print!')

