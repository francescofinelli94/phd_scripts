#imports

import sys
import numpy as np
import collections
import os

#------------------------------------------------------------------------
#------------------------------------------------------------------------
#functions

#---------------------------
#ERROR MESSAGES HANDLING
#---------------------------
def error_msg(message,head='ERROR: ',terminate=False,where='a place.'):
    """
    This function handles message errors.

    INPUTS:
    message -> [scalar,string] the message you want to be shown
    head -> [scalar,string, optional] the head of the message. Default: \'ERROR: \'
    terminate -> [scalar,bool,optional] if True, python will be terminated
    where -> [scalar,string,optional] if python will be terminate, this will say where it happened

    OUTPUTS:
    None
    """
    if type(message) != type(''):
        error_msg('message should be a string.')
        return None
    if type(head) != type(''):
        error_msg('head should be a string.')
        return None
    if type(terminate) != type(True):
        error_msg('terminate should be a boolean.')
        return None
    if type(where) != type(''):
        error_msg('where should be a string.')
        return None
    print('')
    print(head+message)
    print('')
    if terminate:
        sys.exit('An error occurred in '+where)

#---------------------------
#CASTING TO GENERIC TYPE
#---------------------------
def generic_cast(to_be_casted,type_sample,done_or_gone=True):
    """
    This funtion performs a cast to a general type

    INPUTS:
    to_be_casted -> variable to be casted
    type_sample -> an arbitrary variable of the target type
    done_or_gone -> [scalar,bool,optional] if False, nothing happens if cast is unsuccessful

    OUTPUTS:
    out -> to_be_casted casted to the type of type_sample. If cast fails and done_or_gone is False, out = None
    """
    if to_be_casted == None:
        error_msg('to_be_casted is undefined.','ERROR: ',True,'generic_cast.')
        return None
    if type_sample == None:
        error_msg('type_sample is undefined.','ERROR: ',True,'generic_cast.')
        return None
    if type(done_or_gone) != type(True):
        error_msg('done_or_gone is no a boolean.','ERROR: ',True,'generic_cast.')
        return None
    try:
        tmp = (type(type_sample),to_be_casted)
        out = tmp[0](tmp[1])
        return out
    except:
        if done_or_gone:
            error_msg('casting unsucessfull.','ERROR: ',True,'generic_cast.')
        else:
            error_msg('casting unsucessfull.','WARNING: ',False,'generic_cast.')
        return None

#---------------------------
#A SAFER AND SMARTER VERSION OF INPUT
#---------------------------
def secure_input(request='',type_sample=None,try_cast=False):
    """
    A better and safer version of input

    INPUTS:
    request -> [scalar,string] the question for the user
    type_sample -> [optional] if set, the type of the user input will be confronted with its type
    try_cast -> [scalar,bool,optional] if True, in case of type mismatch a cast will be attempted

    OUTPUTS:
    ans -> the user input
    """
    if type(request) != type(''):
        error_msg('request should be a string.','ERROR: ',True,'secure_input')
        return None
    flag = 0
    while flag == 0:
        try:
            ans = input(request)
            flag = 1
        except:
            flag = 0
            error_msg('input not defined, retry.','WARNING: ',False,'secure_input')
        if flag == 1:
            if (type_sample != None) and (type(ans) != type(type_sample)):
                if try_cast:
                    try_ans = generic_cast(ans,type_sample,False)
                    if try_ans == None:
                        flag = 0
                        error_msg('insert a '+str(type(type_sample))+'.','WARNING: ',False,'secure_input')
                    else:
                        ans = try_ans
                else:
                    error_msg('insert a '+str(type(type_sample))+'.','WARNING: ',False,'secure_input')
                    flag = 0
    return ans

#---------------------------
#CREATE A PATH (I.E., MAKEDIR)
#---------------------------
def create_path(path):
    """
    Crete a new directory given its path. If the directory exists, it does nothing.

    INPUTS:
    path -> [string] the path which identifies the directory

    OUTPUTS:
    None
    """
    if type(path) != type(''):
        error_msg('input must be a string.','ERROR: ',True,'create_path')
    if not os.path.exists(path):
      os.makedirs(path)

#---------------------------
#DICTIONARY SORTING BY KEYS
#---------------------------
def dict_keys_sort(dict_):
    """
    This functions will sort dictionry by keys

    INPUTS:
    dict_ -> [dict] a dictionary

    OUTPUTS:
    collections.OrderedDict(sorted(dict_.items())) -> [collections.OrderedDict]
    """
    if (type(dict_) != type({})) and (type(dict_) != type(collections.OrderedDict(sorted({}.items())))):
        error_msg('input must be a dictionary.','ERROR: ',True,'dict_keys_sort')
    return collections.OrderedDict(sorted(dict_.items()))

#---------------------------
#from_HVM SPECIFIC FUNCTION
#GET time, time2seg, time2exit
#---------------------------
def time_manage(segs):
    """
    Tis function wil produce time, time2segs, and time2exit. Thought for from_HVM class

    INPUTS:
    segs -> [dict or collections.OrderedDict] e.g. {'00':[0.],'01':[0.5,1.,2.],'02':[3.,4.5]}

    OUTPUTS:
    time -> [array(n)] e.g. [0.,0.5,1.,2.,3.,4.5]
    time2seg -> [array(n)] e.g. ['00','01','01','01','02','02']
    time2exit -> [array(n)] e.g. [0,0,1,2,0,1]
    """
    if (type(segs) != type({})) and (type(segs) != type(collections.OrderedDict(sorted({}.items())))):
        error_msg('input must be a dictionary or a collectoins.OrderedDict.','ERROR: ',True,'time_manage')

    time = np.concatenate(segs.values()).astype(float)

    time2seg = []
    for i in range(len(segs.keys())):
        time2seg.append(np.full((len(segs.values()[i])),segs.keys()[i]))
    time2seg = np.concatenate(time2seg)

    time2exit = []
    for i in range(len(segs.keys())):
        time2exit.append(np.arange(0,len(segs.values()[i]),1))
    time2exit = np.concatenate(time2exit)

    return time, time2seg, time2exit

#---------------------------
#GET INDEX OF THE t_vec ELEMENT WHICH VALUE IS CLOSEST TO t
#---------------------------
def index_of_closest(t,t_vec,silent=True):
    """
    This function finds the intex of the element of a given vector closest to a given value

    INPUTS:
    t -> [scalar] target value (you want the index of the element of the given array0 closest to t)
    t_vec -> [array] array of values
    silent -> [scalar,bool, optional] if False, print the closest value found

    OUTPUTS:
    ind -> [scalar,int] the index of the element of t_vec closest in value to t
    """
    ind = np.argmin(np.abs(t_vec-t))
    if not silent:
        print('Closest value is '+str(t_vec[ind]))
    return ind

#---------------------------
#CHECK dt VS E AND B
#---------------------------
def steplim_check(E,B,vpmax_vec,dvp_vec,dgrid_vec,coeff=1.):
    """
    This function recrreate the steplim check of the HVM code

    INPUTS:
    E -> [array(3,:,:,:)] electric field
    B -> [array(3,:,:,:)] magnetic field
    vpmax_vec -> [array(3)] [vpxmax,vpymax,vpzmax], already multiplied by vthp
    dvp_vec -> [array(3)] [dvpx,dvpy,dvpz]
    dgrid_vec -> [array(3)] [dx,dy,dz]
    coeff -> [scalar,optional] maximum dt will be multiplied by coeff

    OUTPUTS:
    None
    """
    vxpmax = vpmax_vec[0]
    vypmax = vpmax_vec[1]
    vzpmax = vpmax_vec[2]

    dx = dgrid_vec[0]
    dy = dgrid_vec[1]
    dz = dgrid_vec[2]

    dtmx = 0.5*dx/vxpmax
    dtmy = 0.5*dy/vypmax
    dtmz = 0.5*dz/vzpmax

    dvxp = dvp_vec[0]
    dvyp = dvp_vec[1]
    dvzp = dvp_vec[2]

    emx = np.max(np.abs(E[0,:,:,:]))
    emy = np.max(np.abs(E[1,:,:,:]))
    emz = np.max(np.abs(E[2,:,:,:]))
    bmx = np.max(np.abs(B[0,:,:,:]))
    bmy = np.max(np.abs(B[1,:,:,:]))
    bmz = np.max(np.abs(B[2,:,:,:]))

    fmx = emx + vypmax*bmz + vzpmax*bmy
    fmy = emy + vzpmax*bmx + vxpmax*bmz
    fmz = emz + vxpmax*bmy + vypmax*bmx

    dtmvx = dvxp/fmx
    dtmvy = dvyp/fmy
    dtmvz = 0.5*dvzp/fmz

    dtmax = np.min([dtmx, dtmy, dtmz, dtmvx, dtmvy, dtmvz])

    print('')
    print('max(Ex)='+str(emx),' max(Ey)='+str(emy),' max(Ez)='+str(emz))
    print('max(Bx)='+str(bmx),' max(By)='+str(bmy),' max(Bz)='+str(bmz))
    print('max(Fx)='+str(fmx),' max(Fy)='+str(fmy),' max(Fz)='+str(fmz))
    print('dtmx='+str(dtmx),' dtmy='+str(dtmy),' dtmz='+str(dtmz))
    print('dtmvx='+str(dtmvx),' dtmvy='+str(dtmvy),' dtmvz='+str(dtmvz))
    print('')
    print('dtmax='+str(dtmax)+' dtmax*coeff='+str(dtmax*coeff))
    print('')

    return None

#----------------------
#PERP AND PARL COMPONENT OF A TENSOR
#----------------------
def proj_tens_on_vec(T,V,is_sym=False):
    """
    This function projects a tensor T on a vector V, returning Tparl e Tperp

    INPUTS:
    T -> [array(3,3,:,:,:)]
    V -> [array(3,:,:,:)]
    is_sym -> [scalar,bool,optional] if True, T will be considered symmetric

    OUTPUTS:
    Tparl -> [scalar]
    Tperp -> [scalar]
    """
    V_norm2 = V[0]*V[0] + V[1]*V[1] +V[2]*V[2]

    if is_sym:
        Tparl = (T[0,0]*V[0]*V[0] + T[1,1]*V[1]*V[1] + T[2,2]*V[2]*V[2] + 
                2.0*(T[0,1]*V[0]*V[1] + T[0,2]*V[0]*V[2] + T[1,2]*V[1]*V[2]))
    else:
        Tparl = np.zeros(np.shape(T[0,0]),dtype=type(T[tuple(np.zeros(len(np.shape(T)),dtype=int))]))
        for i in range(3):
            for j in range(3):
                Tparl = Tparl + V[i]*T[i,j]*V[j]
    Tparl = np.divide(Tparl,V_norm2)
    Tperp = 0.5*(T[0,0] + T[1,1] + T[2,2] - Tparl)

    return Tparl,Tperp

#-----------------------
#CREATE X, Y, and Z VECTORS
#-----------------------
def create_xyz(nxyz,xyzl):
    """
    This function create the axis vector x, y, and z

    INPUTS:
    nxyz -> [array(3),long] [nx,ny,nz]
    xyzl -> [array(3),float] [xl,yl,zl]

    OUTPUTS:
    x -> [array(nx),float] [0, dx, ..., xl-dx]
    y -> [array(ny),float] [0, dy, ..., yl-dy]
    z -> [array(nz),float] [0, dz, ..., zl-dz]
    """
    x = np.arange(0.,xyzl[0],xyzl[0]/float(nxyz[0]))
    try:
        y = np.arange(0.,xyzl[1],xyzl[1]/float(nxyz[1]))
    except:
        y = [0.]
    try:
        z = np.arange(0.,xyzl[2],xyzl[2]/float(nxyz[2]))
    except:
        z = [0.]

    return x, y, z

#-----------------------
#MINIMUM IN DICT
#-----------------------
def min_dict(d,keyout=False):
    """
    This function finds the minimum value in a dict d:
    d = {'k_a':[<val_a_0>,<val_a_1>,...],'k_b':[<val_b_0>,...],...}
    min_dict = min([val_x_n, for x in [a,b,...] for n in [0,1,...]])

    INPUTS:
    d -> [dict] 1 level of keys, numerical lists/arrays as values
    keyout -> [bool, optional] if True secondary output is the keys where
              the min is found

    OUTPUTS:
    m -> [number] the minumum value found
    k_vec -> [list,string] where the minimum is found
    """
    if (type(d) != type({})) and (type(d) != type(collections.OrderedDict(sorted({}.items())))):
        error_msg('input must be a dictionary.','ERROR: ',True,'dict_keys_sort')
#
    m = np.min(np.concatenate(d.values()))
#
    if keyout:
        k_vec = []
        for k in d.keys():
            if m in d[k]:
                k_vec.append(k)
        return m,k_vec
    else:
        return m

#-----------------------
#MAXIMUM IN DICT
#-----------------------
def max_dict(d,keyout=False):
    """
    This function finds the maximum value in a dict d:
    d = {'k_a':[<val_a_0>,<val_a_1>,...],'k_b':[<val_b_0>,...],...}
    max_dict = max([val_x_n, for x in [a,b,...] for n in [0,1,...]])

    INPUTS:
    d -> [dict] 1 level of keys, numerical lists/arrays as values
    keyout -> [bool, optional] if True secondary output is the keys where
              the max is found

    OUTPUTS:
    m -> [number] the maxumum value found
    k_vec -> [list,string] where the maximum is found
    """
    if (type(d) != type({})) and (type(d) != type(collections.OrderedDict(sorted({}.items())))):
        error_msg('input must be a dictionary.','ERROR: ',True,'dict_keys_sort')
#
    m = np.max(np.concatenate(d.values()))
#
    if keyout:
        k_vec = []
        for k in d.keys():
            if m in d[k]:
                k_vec.append(k)
        return m,k_vec
    else:
        return m

