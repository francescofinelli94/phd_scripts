# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 10:37:05 2018

@author: fpucci
"""
import numpy as np
import matplotlib.pyplot as plt
import time
import sys

def der_1D(v,n,delta,print_time=False):
    begin = time.time()
    v_t = np.fft.rfft(v)
#    print 'v_t.shape',v_t.shape
    k = np.fft.rfftfreq(n,d=delta/(2.0*np.pi))
#    print 'k.shape',k.shape
    dv_t = 1j*k*v_t
#    print 'dv_t[-1]', dv_t[-1]
    dv_t[-1] = 0.0 
#    print 'dv_t[-1]', dv_t[-1]
#    print 'dv_t.shape',dv_t.shape
    derv = np.fft.irfft(dv_t)
#    print 'derv.shape', derv.shape
    if print_time:
        print 'Elapsed time', time.time()-begin
    return derv


def oneD_der_on_2D_field(v,nx,ny,dx,dy,axis='x'):
    dv = np.ones(np.shape(v))  
    #print 'v.shape', v.shape
    #print 'nx',nx
    #print 'ny',ny
    if axis=='x':
        for j in range(ny):
                dv[:,j]=der_1D(v[:,j],nx,dx)
    if axis=='y':
        for i in range(nx):
                dv[i,:]=der_1D(v[i,:],ny,dy)
    #print 'where one' , np.where(dv==1.0)                     
    return dv

def oneD_der_on_3D_field(v,nx,ny,nz,dx,dy,dz,axis='x'):
    dv = np.ones(np.shape(v))  
    #print 'v.shape', v.shape
    #print 'nx',nx
    #print 'ny',ny
    #print 'nz',nz
    if axis=='x':
        for j in range(ny):
            for k in range(nz):
                dv[:,j,k]=der_1D(v[:,j,k],nx,dx)
    if axis=='y':
        for i in range(nx):
           for k in range(nz):
                dv[i,:,k]=der_1D(v[i,:,k],ny,dy)
    if axis=='z':
        for i in range(nx):
           for j in range(ny):
                dv[i,j,:]=der_1D(v[i,j,:],nz,dz)
    #print 'where one' , np.where(dv==1.0)                     
    return dv

#compute derivatives
#def compute_derivatives(v,nx,ny,nz,dx,dy,dz,return_k=False):
#    nx = v.shape[0] 
#    ny = v.shape[1] 
#    nz = v.shape[2] 
    #print 'v.shape', v.shape
    #print 'nx,ny,nz', nx,ny,nz
#    dv_k = np.empty((v.shape[0],v.shape[1],v.shape[2]//2+1,3),dtype=complex)
#    dv = np.empty((v.shape[0],v.shape[1],v.shape[2],3))
#    kx = np.fft.fftfreq(nx,d=dx/(2.0*np.pi))
#    ky = np.fft.fftfreq(ny,d=dy/(2.0*np.pi))
#    kz = np.fft.rfftfreq(nz,d=dz/(2.0*np.pi))
#    kxx, kyy, kzz = np.meshgrid(kx,ky,kz,indexing='ij')
#    v_k = np.fft.rfftn(np.squeeze(v))
#    dv_k[:,:,:,0] = 1j*kxx*v_k
#    dv_k[:,:,:,1] = 1j*kyy*v_k
#    dv_k[:,:,:,2] = 1j*kzz*v_k    
    #print 'dv_k.shape',dv_k.shape
#    dv_k[:,:,-1,:] = 0.0  #to make the coeff of frequency Nz/2 real before anti transf    
#    dv[:,:,:,0] = np.fft.irfftn(dv_k[:,:,:,0])
#    dv[:,:,:,1] = np.fft.irfftn(dv_k[:,:,:,1])
#    dv[:,:,:,2] = np.fft.irfftn(dv_k[:,:,:,2])   
#    if return_k:
#        return dv, kx, ky, kz
#    else:    
#        return dv
