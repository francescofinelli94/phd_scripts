# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 16:25:39 2017

@author: fpucci
"""
import numpy as np
import der


def averageCurlSquared(V,return_components=False):
    """ Computes the curl of the quantity, squared and averaged over the domain.
    
    Parameters:
        V - [x, y, z, 3] - a 3D array of vectors curl of which is needed.
        
    """
    C = np.zeros(V.shape)
    
    # grad(Vx)
    grad = np.gradient(V[:,:,:,0])
    C[:, :, :, 1] += grad[2]
    C[:, :, :, 2] -= grad[1]
    
    # grad(Vy)
    grad = np.gradient(V[:,:,:,1])
    C[:, :, :, 0] -= grad[2]
    C[:, :, :, 2] += grad[0]
    
    # grad(Vz)
    grad = np.gradient(V[:,:,:,2])
    C[:, :, :, 0] += grad[1]
    C[:, :, :, 1] -= grad[0]
    
    C = C**2

    if return_components:	 
    	return np.mean(C[:,:,:,0]),np.mean(C[:,:,:,1]),np.mean(C[:,:,:,2])
    else:
	return C.mean()
 		
def average2DCurlSquared(V):
    """ Computes the curl of the quantity, squared and averaged over the domain.
    
    Parameters:
        V - [x, y, 3] - a 2D array of vectors curl of which is needed.
        
    """
    C = np.zeros(V.shape)
    
    # grad(Vx)
    grad = np.gradient(V[:,:,0])
    #C[:, :, 1] += grad[2]
    C[:, :, 2] -= grad[1]
    
    # grad(Vy)
    grad = np.gradient(V[:,:,1])
    #C[:, :, 0] -= grad[2]
    C[:, :, 2] += grad[0]
    
    # grad(Vz)
    grad = np.gradient(V[:,:,2])
    C[:, :, 0] += grad[1]
    C[:, :, 1] -= grad[0]
    
    C = C**2
    return C.mean()


def curl2Dfinitediff(V):
    """ Computes the curl of a 2D vector
    
    Parameters:
        V - [x, y, 3] - a 2D array of vectors curl of which is needed.
        
    """
    C = np.zeros(V.shape)
    
    # grad(Vx)
    grad = np.gradient(V[:,:,0])
    #C[:, :, 1] += grad[2]
    C[:, :, 2] -= grad[1]
    
    # grad(Vy)
    grad = np.gradient(V[:,:,1])
    #C[:, :, 0] -= grad[2]
    C[:, :, 2] += grad[0]
    
    # grad(Vz)
    grad = np.gradient(V[:,:,2])
    C[:, :, 0] += grad[1]
    C[:, :, 1] -= grad[0]
    
    return C

def compute_curl(v,nx,ny,nz,dx,dy,dz):
    """
    This function compute the curl of a 2D (xy) or 3D (xyz) vector using pseudo spectral methods    
    
    inputs 
    - v input vector
    - nx,ny,nz number of points in the three directions
    - dx,dy,dz grid size in the three directions

    output             
    - curlv   the curl of v         

    """
    
    curlv = np.zeros(np.shape(v))
    if (nz==1):     
       # curlv x 
           dvzdy = der.oneD_der_on_2D_field(v[:,:,0,2],nx,ny,dx,dy,axis='y')
           #dvydz = 0
           curlv[:,:,0,0] = dvzdy 
           # curlv y 
           #dvxdz = 0
           dvzdx = der.oneD_der_on_2D_field(v[:,:,0,2],nx,ny,dx,dy,axis='x')
           curlv[:,:,0,1] = -dvzdx 
       # curlv z 
           dvydx = der.oneD_der_on_2D_field(v[:,:,0,1],nx,ny,dx,dy,axis='x')
           dvxdy = der.oneD_der_on_2D_field(v[:,:,0,0],nx,ny,dx,dy,axis='y')
           curlv[:,:,0,2] = dvydx-dvxdy  
    else:
       # curlv x 
           dvzdy = der.oneD_der_on_3D_field(v[:,:,:,2],nx,ny,nz,dx,dy,dz,axis='y')
           dvydz = der.oneD_der_on_3D_field(v[:,:,:,1],nx,ny,nz,dx,dy,dz,axis='z')
           curlv[:,:,:,0] = dvzdy -dvydz
       # curlv y 
           dvxdz = der.oneD_der_on_3D_field(v[:,:,:,0],nx,ny,nz,dx,dy,dz,axis='z')
           dvzdx = der.oneD_der_on_3D_field(v[:,:,:,2],nx,ny,nz,dx,dy,dz,axis='x')
           curlv[:,:,:,1] = dvxdz-dvzdx 
       # curlv z 
           dvydx = der.oneD_der_on_3D_field(v[:,:,:,1],nx,ny,nz,dx,dy,dz,axis='x')
           dvxdy = der.oneD_der_on_3D_field(v[:,:,:,0],nx,ny,nz,dx,dy,dz,axis='y')
           curlv[:,:,:,2] = dvydx-dvxdy 
    print "Curl computed" 
    return curlv
