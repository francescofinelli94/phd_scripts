# coding: utf-8

#---------------------------------------------------------------------------------------
#-(1)-----21.12.18----FDP:S,F-----------------------------------------------------------
#-(2)-----30.12.18----FDP:S,J,R---------------------------------------------------------
#-(3)-----02.04.19----FDP:S,P,L---------------------------------------------------------
#-(4)-----04.04.19----FDP:S,J,L---------------------------------------------------------
#-(5)-----17.11.19----FDP:S,J,L,F-------------------------------------------------------
#---------------------------------------------------------------------------------------
#-(alpha)-19.07.19----FDP:S,J,L---------------------------------------------------------
#-(beta0)-12.11.19----FDP:S,F-----------------------------------------------------------
#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------

import collections 
import numpy as np
import os
import struct

#this class contains all functions to read outputs from 2d simulations with EB, Ion, Press, Q and calculate remaining quantities
#this class could be expanded so to read also outputs of codes with chew-golberg-low and landau closures (explicit electron pressures)
class from_HVM (object):  

  def __init__(self, 
      address):
    """
    ------------------------------------------------------------------------------------
      creates the object to retrieve data from Hybrid-Vlasov-Maxwell codes
    ------------------------------------------------------------------------------------
    address      [address] where your data are (folder with segs inside)
    ------------------------------------------------------------------------------------
    """

    self.address = os.path.normpath(address)
    self.segs = {}
    self.meta = {}

  #------------------------------------------------------------
  def help(self):
    print('For further help, please shout:')
    print('!!!SIIIIIIIIIIIID!!!')

  #------------------------------------------------------------
  def get_meta(self,  #counts lines in file and calls the appropriate function to get the meta data
      extra_address = '00',
      silent = True):
    """
    ------------------------------------------------------------------------------------
      fills the metadata list 
    ------------------------------------------------------------------------------------
    extra_address = ''     [address] to reach any subfolder where your meta-data are
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    with open(os.path.join(self.address,extra_address,'input_parameters'),'r') as foo:
      line_number = len(foo.readlines())

    if line_number == 75 : self.get_meta_A(extra_address)
    elif line_number == 81 : self.get_meta_B(extra_address)
    elif line_number == 91 : self.get_meta_C(extra_address)
    elif line_number == 92 : self.get_meta_C(extra_address) #cm on nobody cares about satellite number ...
    else : print('FDP : unknown input_parameter file! write a new from_HVM.get_meta() for it ...')


    #---------get-dimensions-of-your-simulation----------

    self.meta['dx'] = self.meta['xl']/self.meta['nx']
    self.meta['dy'] = self.meta['yl']/self.meta['ny']
    self.meta['dz'] = self.meta['zl']/self.meta['nz']

    #nx_original = nx    ??? ASK THE BOSS ABOUT ALL THESE !!!
    #nx = int(nx / nwx)    ??? ASK THE BOSS ABOUT ALL THESE !!!
    #ny = int(ny / nwy)    ??? ASK THE BOSS ABOUT ALL THESE !!!
    #nz = int(nz / nwz)    ??? ASK THE BOSS ABOUT ALL THESE !!!
    
    self.meta['nnn'] = (self.meta['nx'], self.meta['ny'], self.meta['nz'])
    self.meta['lll'] = (self.meta['xl'], self.meta['yl'], self.meta['zl'])
    self.meta['ddd'] = (self.meta['dx'], self.meta['dy'], self.meta['dz']) 
    self.meta['ppp'] = (True, True, True)                                      #THIS IS HARDCODED! 3-PERIODICITY IS HARDCODED

    self.meta['ts'] = self.meta['dt']      #this is just for jeremy :)

    self.meta['x'] = np.arange(0.,self.meta['xl'],self.meta['dx'])
    try:
        self.meta['y'] = np.arange(0.,self.meta['yl'],self.meta['dy'])
    except:
        self.meta['y'] = np.array([0.])
    try:
        self.meta['z'] = np.arange(0.,self.meta['zl'],self.meta['dz'])
    except:
        self.meta['z'] = np.array([0.])


    #----------get-time-segment-infos-from all-subdirectories--------- 
    self.meta['name'] = self.address.split('/')[-1]

    segments = [seg for seg in os.listdir(self.address) if os.path.isdir(os.path.join(self.address,seg))]
    segments = [seg for seg in segments if os.path.isfile(os.path.join(self.address,seg,'tempo.dat'))]
    segments = sorted(segments)

    for seg in segments:
      infot = open(os.path.join(self.address,seg,'tempo.dat'),'r')
      nexits = len(infot.readlines())
      self.segs[seg] = []
      infot.seek(0)
      for time_exit_num in range(nexits):
        time_exit = '%.3f' %np.around(float(infot.readline().split()[0]),decimals=3)  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        self.segs[seg].append(time_exit)
        self.segs = collections.OrderedDict(sorted(self.segs.items())) #Fra's ordering :)
      infot.close()
    
    #Fra's addition :)
    self.meta['time'] = np.concatenate(self.segs.values()).astype(float)
    self.meta['times'] = np.concatenate(self.segs.values())
    
    self.meta['time2seg'] = []
    for i in range(len(self.segs.keys())):
        self.meta['time2seg'].append(np.full((len(self.segs.values()[i])),self.segs.keys()[i]))
    self.meta['time2seg'] = np.concatenate(self.meta['time2seg'])
    
    self.meta['time2exit'] = []
    for i in range(len(self.segs.keys())):
        self.meta['time2exit'].append(np.arange(0,len(self.segs.values()[i]),1))
    self.meta['time2exit'] = np.concatenate(self.meta['time2exit'])


    #----------add-informations-on-species-----------------
    #----ACHTUNG: these are hard-coded - change them in future!----------

    self.meta['nss'] = 2     #number of species

    species  = []
    species.append('ion     ')
    species.append('electron')

    charges = np.zeros([self.meta['nss']])
    charges[0] = 1.
    charges[1] = -1.

    masses = np.zeros([self.meta['nss']])
    masses[0] = 1.
    masses[1] = 1./self.meta['mime']

    self.meta['species']  = species
    self.meta['charges']  = { kk:charges[k] for k,kk in enumerate(species)}
    self.meta['masses']   = { kk:masses[k] for k,kk in enumerate(species)}


    #----------print-summary-----------------

    if not silent : 
      print('HVM_'+self.meta['space_dim']+'> cell number               :  ', self.meta['nnn'])
      print('HVM_'+self.meta['space_dim']+'> domain size               :  ', self.meta['lll'])
      print('HVM_'+self.meta['space_dim']+'> mesh spacing              :  ', self.meta['ddd'])
      print('HVM_'+self.meta['space_dim']+'> periodicity               :  ', self.meta['ppp'])
      print('HVM_'+self.meta['space_dim']+'> time step                 :  ', self.meta['ts'])
      print('HVM_'+self.meta['space_dim']+'> species                   :  ', self.meta['species'])
      for i in range(self.meta['nss']):
        print('          '+species[i]+' charge                :  ', self.meta['charges'][species[i]])
        print('          '+species[i]+' mass                  :  ', self.meta['masses'][species[i]])
      print('HVM_'+self.meta['space_dim']+'> teti                      :  ', self.meta['teti'])


  #------------------------------------------------------------
  def get_meta_A(self,
      extra_address = ''):
    """
    ------------------------------------------------------------------------------------
      extra routine, version A 
    ------------------------------------------------------------------------------------
    """

    #get mesh infos from input_parameters (I take the input_parameters from subfolder 01)
    infos = open(os.path.join(self.address,extra_address,'input_parameters'),'r')
    infos.readline()
    self.meta['np_row'] = int(infos.readline().split()[2]) #number of mpi processeses in x direction
    self.meta['np_col'] = int(infos.readline().split()[2]) #number of mpi processeses in y direction
    self.meta['np_pla'] = int(infos.readline().split()[2]) #number of mpi processeses in z direction
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['nx'] = int(infos.readline().split()[2])
    self.meta['ny'] = int(infos.readline().split()[2])
    self.meta['nz'] = int(infos.readline().split()[2])
    self.meta['lvx'] = int(infos.readline().split()[2])
    self.meta['lvy'] = int(infos.readline().split()[2])
    self.meta['lvz'] = int(infos.readline().split()[2])
    self.meta['space_dim'] = infos.readline().split("'")[1]  #
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['model'] = int(infos.readline().split()[2]) #not totally sure: in case substitute this line with an innocent infos.readline()
    self.meta['dt'] = float(infos.readline().split()[2])
    nstep = infos.readline() #
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['xl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['yl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['zl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['vxpmax'] = float(infos.readline().split()[2])
    self.meta['vypmax'] = float(infos.readline().split()[2])
    self.meta['vzpmax'] = float(infos.readline().split()[2])
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    
    #infos.readline()
    noutt = infos.readline() #
    noutx = infos.readline() #
    noutf = infos.readline() #
    noutcheck = infos.readline() #
    outformat = infos.readline() #
    ifd = infos.readline() #
    nwx = infos.readline() #
    nwy = infos.readline() #
    nwz = infos.readline() #
    nwvx = infos.readline() #
    nwvy = infos.readline() #
    nwvz = infos.readline() #
    
    infos.readline()
    infos.readline()
    infos.readline()

    initcond = str(infos.readline().split("'")[1])      
    with_forcing = infos.readline() #
    incompressible_forcing = infos.readline() #
    self.meta['Bx0'] = float(infos.readline().split()[2]) #
    self.meta['By0'] = float(infos.readline().split()[2]) #
    self.meta['Bz0'] = float(infos.readline().split()[2]) #
    rhop = infos.readline() #
    self.meta['beta'] = float(infos.readline().split()[2])
    self.meta['beta_para'] = float(infos.readline().split()[2])
    self.meta['beta_perp'] = float(infos.readline().split()[2])
    self.meta['mime'] = float(infos.readline().split()[2])
    self.meta['teti'] = float(infos.readline().split()[2])
    gamma = float(infos.readline().split()[2])
    amp = infos.readline() #
    hk0 = infos.readline() #
    ux0 = infos.readline() #
    uy0 = infos.readline() #
    uz0 = infos.readline() #

    infos.close()

  #------------------------------------------------------------
  def get_meta_B(self,
      extra_address = ''):
    """
    ------------------------------------------------------------------------------------
      extra routine, version B 
    ------------------------------------------------------------------------------------
    """

    #get mesh infos from input_parameters (I take the input_parameters from subfolder 01)
    infos = open(os.path.join(self.address,extra_address,'input_parameters'),'r')
    infos.readline()
    self.meta['np_row'] = int(infos.readline().split()[2]) #number of mpi processeses in x direction
    self.meta['np_col'] = int(infos.readline().split()[2]) #number of mpi processeses in y direction
    self.meta['np_pla'] = int(infos.readline().split()[2]) #number of mpi processeses in z direction
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['nx'] = int(infos.readline().split()[2])
    self.meta['ny'] = int(infos.readline().split()[2])
    self.meta['nz'] = int(infos.readline().split()[2])
    self.meta['lvx'] = int(infos.readline().split()[2])
    self.meta['lvy'] = int(infos.readline().split()[2])
    self.meta['lvz'] = int(infos.readline().split()[2])
    self.meta['space_dim'] = infos.readline().split("'")[1] #

    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['model'] = int(infos.readline().split()[2]) #not totally sure: in case substitute this line with an innocent infos.readline()
    self.meta['dt'] = float(infos.readline().split()[2])
    nstep = infos.readline() #
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['xl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['yl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['zl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['vxpmax'] = float(infos.readline().split()[2])
    self.meta['vypmax'] = float(infos.readline().split()[2])
    self.meta['vzpmax'] = float(infos.readline().split()[2])
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    
    noutt = infos.readline() #
    noutx = infos.readline() #
    noutf = infos.readline() #
    noutcheck = infos.readline() #
    outformat = infos.readline() #
    ifd = infos.readline() #
    nwx = infos.readline() #
    nwy = infos.readline() #
    nwz = infos.readline() #
    nwvx = infos.readline() #
    nwvy = infos.readline() #
    nwvz = infos.readline() #
    
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['Bx0'] = float(infos.readline().split()[2]) 
    self.meta['By0'] = float(infos.readline().split()[2]) #
    self.meta['Bz0'] = float(infos.readline().split()[2]) #
    rhop = infos.readline() #
    self.meta['beta'] = float(infos.readline().split()[2])
    self.meta['mime'] = float(infos.readline().split()[2])
    self.meta['teti'] = float(infos.readline().split()[2])
    gamma = float(infos.readline().split()[2])
    amp = infos.readline() #
    hk0 = infos.readline() #
    ux0 = infos.readline() #
    uy0 = infos.readline() #
    uz0 = infos.readline() #
    initcond = infos.readline() #
    self.meta['beta_para'] = float(infos.readline().split()[2])
    self.meta['beta_perp'] = float(infos.readline().split()[2])
    with_forcing = infos.readline() #
    incompressible_forcing = infos.readline() #
    mx_max = infos.readline()
    my_max = infos.readline()
    mz_max = infos.readline()
    mf_min = infos.readline()
    mf_max = infos.readline()
    amp_forcing = infos.readline()

    infos.close()

  #------------------------------------------------------------
  def get_meta_C(self,  
      extra_address = ''):    #extra address to get to input_parameters
    """
    ------------------------------------------------------------------------------------
      extra routine, version C 
    ------------------------------------------------------------------------------------
    """

    #get mesh infos from input_parameters (I take the input_parameters from subfolder 01)
    infos = open(os.path.join(self.address,extra_address,'input_parameters'),'r')
    infos.readline()
    self.meta['np_row'] = int(infos.readline().split()[2]) #number of mpi processeses in x direction
    self.meta['np_col'] = int(infos.readline().split()[2]) #number of mpi processeses in y direction
    self.meta['np_pla'] = int(infos.readline().split()[2]) #number of mpi processeses in z direction
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['nx'] = int(infos.readline().split()[2])
    self.meta['ny'] = int(infos.readline().split()[2])
    self.meta['nz'] = int(infos.readline().split()[2])
    self.meta['lvx'] = int(infos.readline().split()[2])
    self.meta['lvy'] = int(infos.readline().split()[2])
    self.meta['lvz'] = int(infos.readline().split()[2])
    self.meta['space_dim'] = infos.readline().split("'")[1] #
    #
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['model'] = int(infos.readline().split()[2])
    self.meta['dt'] = float(infos.readline().split()[2])
    nstep = infos.readline() #
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    self.meta['xl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['yl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['zl'] = float(infos.readline().split()[2]) * 2 * np.pi
    self.meta['vxpmax'] = float(infos.readline().split()[2])
    self.meta['vypmax'] = float(infos.readline().split()[2])
    self.meta['vzpmax'] = float(infos.readline().split()[2])
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    infos.readline()
    
    noutt = infos.readline() #
    noutx = infos.readline() #
    noutf = infos.readline() #
    infos.readline()
    infos.readline()
    infos.readline()
    noutcheck = infos.readline() #
    outformatx = infos.readline() #
    outformatf = infos.readline()
    iclock = infos.readline() #
    nwx = infos.readline() #
    nwy = infos.readline() #
    nwz = infos.readline() #
    nwvx = infos.readline() #
    nwvy = infos.readline() #
    nwvz = infos.readline() #
    
    infos.readline()
    infos.readline()
    infos.readline()
    initcond = infos.readline() #
    with_forcing = infos.readline() #
    incompressible_forcing = infos.readline() #
    self.meta['Bx0'] = float(infos.readline().split()[2])  #
    self.meta['By0'] = float(infos.readline().split()[2])  #
    self.meta['Bz0'] = float(infos.readline().split()[2])  #
    rhop = infos.readline() #
    self.meta['beta'] = float(infos.readline().split()[2])
    self.meta['mime'] = float(infos.readline().split()[2])
    self.meta['alpha_i'] = float(infos.readline().split()[2])
    self.meta['teti'] = float(infos.readline().split()[2])
    self.meta['alpha_e'] = float(infos.readline().split()[2])
    gamma = float(infos.readline().split()[2])
    amp = infos.readline() #
    hk0 = infos.readline() #
    ux0 = infos.readline() #
    uy0 = infos.readline() #
    uz0 = infos.readline() #
    mx_max = infos.readline()
    my_max = infos.readline()
    mz_max = infos.readline()
    mf_min = infos.readline()
    mf_max = infos.readline()

    infos.close()

  #-----routines-for-fields------------------------------------
  #------------------------------------------------------------
  def get_EB(self,
      t_,
      fibo_obj = None,
      silent = True):
    """
    ------------------------------------------------------------------------------------
      gets the E,B fields at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    #create data vectors
    E_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    E_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    E_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    B_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    B_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    B_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'EB.bin')) :    
      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'EB.bin'), 'r')
      rfr = rf.read()
      offset = 0
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        offset += 32 + 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      #if time_exit != self.segs[seg][exit_num] : print('=====WRONG=EXIT=====')
      if not silent: print('reading time:' , time_exit)
      offset += 32

      #fill data vectors
      flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*6)+'d',rfr[offset:offset+6*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(6,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      E_x = np.transpose(arr[0,:,:,:],(2,1,0))
      E_y = np.transpose(arr[1,:,:,:],(2,1,0))
      E_z = np.transpose(arr[2,:,:,:],(2,1,0))
      B_x = np.transpose(arr[3,:,:,:],(2,1,0)) + self.meta['Bx0']
      B_y = np.transpose(arr[4,:,:,:],(2,1,0)) + self.meta['By0']
      B_z = np.transpose(arr[5,:,:,:],(2,1,0)) + self.meta['Bz0']      #I am putting this here since in the codes I know we have this asymmetry ...

    else :
      rf = open(os.path.join(self.address,seg,'EB.dat'), 'r')
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        for il in range(self.meta['nx']*self.meta['ny']*self.meta['nz']) : 
          rf.readline()

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)

      #fill data vectors
      for iz in range(self.meta['nz']):
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            EB_line = rf.readline().split()
            E_x[ix,iy,iz] = float(EB_line[0])
            E_y[ix,iy,iz] = float(EB_line[1])
            E_z[ix,iy,iz] = float(EB_line[2])
            B_x[ix,iy,iz] = float(EB_line[3])
            B_y[ix,iy,iz] = float(EB_line[4])
            B_z[ix,iy,iz] = float(EB_line[5])

    rf.close()

    #copy all these in fibo, or return them! 
    if (fibo_obj != None) :
      time_exit = self.segs[seg][exit_num]
      fibo_obj.data['E_x_'+time_exit] = E_x
      fibo_obj.data['E_y_'+time_exit] = E_y
      fibo_obj.data['E_z_'+time_exit] = E_z
      fibo_obj.data['B_x_'+time_exit] = B_x
      fibo_obj.data['B_y_'+time_exit] = B_y
      fibo_obj.data['B_z_'+time_exit] = B_z
    else: return np.array([E_x, E_y, E_z]), np.array([B_x, B_y, B_z])

    if not silent: print('done with reading E, B!')

  #------------------------------------------------------------
  def get_Ion(self,
      t_,
      fibo_obj = None,
      silent = True):
    """
    ------------------------------------------------------------------------------------
      gets the ni and ui fields at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    #create data vectors
    ui_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    ui_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    ui_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    ni = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'Ion.bin')) :   
      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 4*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'Ion.bin'), 'r')
      rfr = rf.read()
      offset = 0
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        offset += 32 + 4*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] + 4

      #fill data vectors
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)
      offset += 32

      flat_arr = struct.unpack(str(4*self.meta['nx']*self.meta['ny']*self.meta['nz'])+'d',rfr[offset:offset+4*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(4,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      ui_x = np.transpose(arr[0,:,:,:],(2,1,0))
      ui_y = np.transpose(arr[1,:,:,:],(2,1,0))
      ui_z = np.transpose(arr[2,:,:,:],(2,1,0))
      ni = np.transpose(arr[3,:,:,:],(2,1,0))

    else :    
      rf = open(os.path.join(self.address,seg,'Ion.dat'), 'r')
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        for il in range(self.meta['nx']*self.meta['ny']*self.meta['nz']) : 
          rf.readline()

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)

      #fill data vectors
      for iz in range(self.meta['nz']):
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            Ion_line = rf.readline().split()
            ui_x[ix,iy,iz] = float(Ion_line[0])
            ui_y[ix,iy,iz] = float(Ion_line[1])
            ui_z[ix,iy,iz] = float(Ion_line[2])
            ni[ix,iy,iz] = float(Ion_line[3])

    rf.close()
    #ni[np.isnan(ni)] = 0.0

    ni = ni + 1.
    ui_x = np.divide(ui_x , ni)
    ui_y = np.divide(ui_y , ni)
    ui_z = np.divide(ui_z , ni)
    
    if (fibo_obj != None) :
      time_exit = self.segs[seg][exit_num]
      fibo_obj.data['ui_x_'+time_exit] = ui_x 
      fibo_obj.data['ui_y_'+time_exit] = ui_y
      fibo_obj.data['ui_z_'+time_exit] = ui_z
      fibo_obj.data['n_'+time_exit] = ni
    else: return ni, np.array([ui_x, ui_y, ui_z])

    if not silent: print('done with reading ni, ui!')

  #------------------------------------------------------------
  def get_Press(self,
      t_,            #time index or value
      fibo_obj = None,  #fibo object you are considering - if None, Ion values will just be returned 
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the Pi tensor field at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    #create data vectors
    Pi_xx = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_yy = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_zz = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_xy = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_xz = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_yz = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'Press.bin')) :    
      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'Press.bin'), 'r')
      rfr = rf.read()
      offset = 0
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        offset += 32 + 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

      #fill data vectors
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)
      offset += 32

      flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*6)+'d',rfr[offset:offset+6*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(6,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      Pi_xx = np.transpose(arr[0,:,:,:],(2,1,0))
      Pi_yy = np.transpose(arr[1,:,:,:],(2,1,0))
      Pi_zz = np.transpose(arr[2,:,:,:],(2,1,0))
      Pi_xy = np.transpose(arr[3,:,:,:],(2,1,0))
      Pi_xz = np.transpose(arr[4,:,:,:],(2,1,0))
      Pi_yz = np.transpose(arr[5,:,:,:],(2,1,0))

    else :
      rf = open(os.path.join(self.address,seg,'Press.dat'), 'r')
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        for il in range(self.meta['nx']*self.meta['ny']*self.meta['nz']) : 
          rf.readline()

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)

      #fill data vectors
      for iz in range(self.meta['nz']):
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            Press_line = rf.readline().split()
            Pi_xx[ix,iy,iz] = float(Press_line[0])
            Pi_yy[ix,iy,iz] = float(Press_line[1])
            Pi_zz[ix,iy,iz] = float(Press_line[2])
            Pi_xy[ix,iy,iz] = float(Press_line[3])
            Pi_xz[ix,iy,iz] = float(Press_line[4])
            Pi_yz[ix,iy,iz] = float(Press_line[5])

    rf.close()

    if (fibo_obj != None) :
      time_exit = self.segs[seg][exit_num]
      fibo_obj.data['Pi_xx_'+time_exit] = Pi_xx[:,:,:]
      fibo_obj.data['Pi_yy_'+time_exit] = Pi_yy[:,:,:]
      fibo_obj.data['Pi_zz_'+time_exit] = Pi_zz[:,:,:]
      fibo_obj.data['Pi_xy_'+time_exit] = Pi_xy[:,:,:]
      fibo_obj.data['Pi_xz_'+time_exit] = Pi_xz[:,:,:]
      fibo_obj.data['Pi_yz_'+time_exit] = Pi_yz[:,:,:]
    else: return np.array([[Pi_xx, Pi_xy, Pi_xz],[Pi_xy, Pi_yy, Pi_yz],[Pi_xz, Pi_yz, Pi_zz]])

    if not silent: print('done with reading Pi!')

  #------------------------------------------------------------
  def get_Q(self,    #achtung that two different outputs are given, depending on ux - so I will need to post-process it
      t_,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the Qi_per,Qi_par fields OR sQi field at the nth exit of the data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    if os.path.isfile(os.path.join(self.address,seg,'Q.bin')) :
      #create data vectors
      Qi_par_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_par_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_par_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_per_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_per_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_per_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'Q.bin'), 'r')
      rfr = rf.read()
      offset = 0
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        offset += 32 + 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

      #fill data vectors
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)
      offset += 32

      flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*6)+'d',rfr[offset:offset+6*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(6,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      Qi_par_x = np.transpose(arr[0,:,:,:],(2,1,0))
      Qi_par_y = np.transpose(arr[1,:,:,:],(2,1,0))
      Qi_par_z = np.transpose(arr[2,:,:,:],(2,1,0))
      Qi_per_x = np.transpose(arr[3,:,:,:],(2,1,0))
      Qi_per_y = np.transpose(arr[4,:,:,:],(2,1,0))
      Qi_per_z = np.transpose(arr[5,:,:,:],(2,1,0))

      rf.close()

      if (fibo_obj != None) :
        #time_exit = self.segs[seg][exit_num]
        fibo_obj.data['Qi_par_x_'+time_exit] = Qi_par_x[:,:,:]
        fibo_obj.data['Qi_par_y_'+time_exit] = Qi_par_y[:,:,:]
        fibo_obj.data['Qi_par_z_'+time_exit] = Qi_par_z[:,:,:]
        fibo_obj.data['Qi_per_x_'+time_exit] = Qi_per_x[:,:,:]
        fibo_obj.data['Qi_per_y_'+time_exit] = Qi_per_y[:,:,:]
        fibo_obj.data['Qi_per_z_'+time_exit] = Qi_per_z[:,:,:]
      else: return np.array([Qi_par_x, Qi_par_y, Qi_par_z]), np.array([Qi_per_x, Qi_per_y, Qi_per_z])
      if not silent: print('done with reading Qi_par and Qi_per!')

    else :
      #create data vectors
      sQi_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      sQi_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      sQi_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

      rf = open(os.path.join(self.address,seg,'Q.dat'), 'r')
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        for il in range(self.meta['nx']*self.meta['ny']*self.meta['nz']) : 
          rf.readline()

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)

      #fill data vectors
      for iz in range(self.meta['nz']):
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            Q_line = rf.readline().split()
            sQi_x[ix,iy,iz] = float(Q_line[0])
            sQi_y[ix,iy,iz] = float(Q_line[1])
            sQi_z[ix,iy,iz] = float(Q_line[2])


      rf.close()

      if (fibo_obj != None) :
        time_exit = self.segs[seg][exit_num]
        fibo_obj.data['sQi_x_'+time_exit] = sQi_x[:,:,:]
        fibo_obj.data['sQi_y_'+time_exit] = sQi_y[:,:,:]
        fibo_obj.data['sQi_z_'+time_exit] = sQi_z[:,:,:]
      else: return np.array([sQi_x, sQi_y, sQi_z])
      if not silent: print('done with reading sQi!')

  #------------------------------------------------------------
  def get_Te(self,   
      t_,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the Te_per,Te_par fields at the nth exit of the data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    #create data vectors
    Te_par = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Te_per = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

    #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 2*8*nx*ny*nz data, 4 empty) for each time exit
    rf = open(os.path.join(self.address,seg,'Te.bin'), 'r')
    rfr = rf.read()
    offset = 0
    #jump to the correct line in the file
    for l in range(exit_num):
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('jumping time:' , time_exit)
      offset += 32 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

    #fill data vectors
    time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
    time_exit = time_exit.zfill(8)        # ..and three decimal digits
    if not silent: print('reading time:' , time_exit)
    offset += 32

    flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
    arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
    Te_par = np.transpose(arr[0,:,:,:],(2,1,0)) + 0.5 * self.meta['beta'] * self.meta['teti']
    Te_per = np.transpose(arr[1,:,:,:],(2,1,0)) + 0.5 * self.meta['beta'] * self.meta['teti'] * self.meta['alpha_e']

    rf.close()

    if (fibo_obj != None) :
      time_exit = self.segs[seg][exit_num]
      fibo_obj.data['Te_par_'+time_exit] = Te_par[:,:,:]
      fibo_obj.data['Te_per_'+time_exit] = Te_per[:,:,:]
    else: return np.array([Te_par, Te_per])
    if not silent: print('done with reading Te_par and Te_per!')

  #------------------------------------------------------------
  def get_Qe(self,   
      t_,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the qe_per,qe_par fields at the nth exit of the data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    #create data vectors
    qe_par = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    qe_per = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

    #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 2*8*nx*ny*nz data, 4 empty) for each time exit
    rf = open(os.path.join(self.address,seg,'Qe.bin'), 'r')
    rfr = rf.read()
    offset = 0
    #jump to the correct line in the file
    for l in range(exit_num):
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('jumping time:' , time_exit)
      offset += 32 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

    #fill data vectors
    time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
    time_exit = time_exit.zfill(8)        # ..and three decimal digits
    if not silent: print('reading time:' , time_exit)
    offset += 32

    flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
    arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
    qe_par = np.transpose(arr[0,:,:,:],(2,1,0))
    qe_per = np.transpose(arr[1,:,:,:],(2,1,0))

    rf.close()

    if (fibo_obj != None) :
      time_exit = self.segs[seg][exit_num]
      fibo_obj.data['qe_par_'+time_exit] = qe_par[:,:,:]
      fibo_obj.data['qe_per_'+time_exit] = qe_per[:,:,:]
    else: return np.array([qe_par, qe_per])
    if not silent: print('done with reading qe_par and qe_per!')

  #------------------------------------------------------------
  def get_Entropy(self,
      t_,
      fibo_obj = None,
      silent = True):
    """
    ------------------------------------------------------------------------------------
      gets the entropy and entropy_fluct fields at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    t_                     [int/long/float/double] time exit identifier. If int or 
                           long, will be used as time index, or else as time value
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    #processing time input
    if type(t_) != float:
        ind = t_ #if t_ is intended as the (global) time index
    else:
        ind = np.argmin(np.abs(self.meta['time'] - t_)) #if t_ is intended as a time value. 
                                                        #The closest time will be used
    if not silent:
        print('Closest time is: '+str(self.meta['time'][ind]))
    seg = self.meta['time2seg'][ind]
    exit_num = self.meta['time2exit'][ind]

    #create data vectors
    entropy = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    entropy_fluct = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'Entropy.bin')) :    
      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'EB.bin'), 'r')
      rfr = rf.read()
      offset = 0
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        offset += 32 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      #if time_exit != self.segs[seg][exit_num] : print('=====WRONG=EXIT=====')
      if not silent: print('reading time:' , time_exit)
      offset += 32

      #fill data vectors
      flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      entropy = np.transpose(arr[0,:,:,:],(2,1,0))
      entropy_fluct = np.transpose(arr[1,:,:,:],(2,1,0))

    else :
      rf = open(os.path.join(self.address,seg,'Entropy.dat'), 'r')
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('jumping time:' , time_exit)
        for il in range(self.meta['nx']*self.meta['ny']*self.meta['nz']) : 
          rf.readline()

      #check that the data exit corresponds with the correct one
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)

      #fill data vectors
      for iz in range(self.meta['nz']):
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            entropy_line = rf.readline().split()
            entropy[ix,iy,iz] = float(entropy_line[0])
            entropy_fluct[ix,iy,iz] = float(entropy_line[1])

    rf.close()

    #copy all these in fibo, or return them! 
    if (fibo_obj != None) :
      time_exit = self.segs[seg][exit_num]
      fibo_obj.data['entropy_'+time_exit] = entropy
      fibo_obj.data['entropy_fluct_'+time_exit] = entropy_fluct
    else: return np.array(entropy), np.array(entropy_fluct)

    if not silent: print('done with reading entropy, entropy_fluct!')

  #------------------------------------------------------------
  #------------------------------------------------------------
  def get_seg_EB(self,
      seg,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the E,B fields in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nexits = len(self.segs[seg])
    E_x = np.zeros([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    E_y = np.zeros([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    E_z = np.zeros([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    B_x = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    B_y = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    B_z = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'EB.bin')) :    
      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'EB.bin'), 'r')
      rfr = rf.read()
      if len(rfr) != (36 + 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'])*nexits : print('===!!!==wrong=segs=:=correct==!!!===')
      offset = 0

      #jump to the correct line in the file and read data vectors
      for l in range(nexits):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        offset += 32 

        flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*6)+'d',rfr[offset:offset+6*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
        arr = np.reshape(flat_arr,(6,self.meta['nz'],self.meta['ny'],self.meta['nx']))
        E_x[l,:,:,:] = np.transpose(arr[0,:,:,:],(2,1,0))
        E_y[l,:,:,:] = np.transpose(arr[1,:,:,:],(2,1,0))
        E_z[l,:,:,:] = np.transpose(arr[2,:,:,:],(2,1,0))
        B_x[l,:,:,:] = np.transpose(arr[3,:,:,:],(2,1,0))
        B_y[l,:,:,:] = np.transpose(arr[4,:,:,:],(2,1,0))
        B_z[l,:,:,:] = np.transpose(arr[5,:,:,:],(2,1,0)) + 1.0      #I am putting this here since in the codes I know we have this asymmetry ...
        offset += 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

    else :
      rf = open(os.path.join(self.address,seg,'EB.dat'), 'r')

      for l in range(nexits):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        for iz in range(self.meta['nz']):
          for iy in range(self.meta['ny']):
            for ix in range(self.meta['nx']):
              EB_line = rf.readline().split()
              E_x[l,ix,iy,iz] = float(EB_line[0])
              E_y[l,ix,iy,iz] = float(EB_line[1])
              E_z[l,ix,iy,iz] = float(EB_line[2])
              B_x[l,ix,iy,iz] = float(EB_line[3])
              B_y[l,ix,iy,iz] = float(EB_line[4])
              B_z[l,ix,iy,iz] = float(EB_line[5])

    rf.close()

    if (fibo_obj != None) :
      for l in range(nexits):
        time_exit = self.segs[seg][l]
        fibo_obj.data['E_x_'+time_exit] = E_x[l,:,:,:]
        fibo_obj.data['E_y_'+time_exit] = E_y[l,:,:,:]
        fibo_obj.data['E_z_'+time_exit] = E_z[l,:,:,:]
        fibo_obj.data['B_x_'+time_exit] = B_x[l,:,:,:]
        fibo_obj.data['B_y_'+time_exit] = B_y[l,:,:,:]
        fibo_obj.data['B_z_'+time_exit] = B_z[l,:,:,:]
    else: return np.array([E_x, E_y, E_z]), np.array([B_x, B_y, B_z])
    if not silent: print('done with reading E, B!')

  #------------------------------------------------------------
  def get_seg_Ion(self,
      seg,        #segment considered (str)
      fibo_obj = None,  #fibo object you are considering - if None, Ion values will just be returned 
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the ni,ui fields in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nexits = len(self.segs[seg])
    ui_x = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    ui_y = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    ui_z = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    ni = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'Ion.bin')) :    
      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 4*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'Ion.bin'), 'r')
      rfr = rf.read()
      if len(rfr) != (36 + 4*8*self.meta['nx']*self.meta['ny']*self.meta['nz'])*nexits : print('===!!!==wrong=segs=:=correct==!!!===')
      offset = 0

      #jump to the correct line in the file and read data vectors
      for l in range(nexits):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        offset += 32 

        flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*4)+'d',rfr[offset:offset+4*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
        arr = np.reshape(flat_arr,(4,self.meta['nz'],self.meta['ny'],self.meta['nx']))
        ui_x[l,:,:,:] = np.transpose(arr[0,:,:,:],(2,1,0))
        ui_y[l,:,:,:] = np.transpose(arr[1,:,:,:],(2,1,0))
        ui_z[l,:,:,:] = np.transpose(arr[2,:,:,:],(2,1,0))
        ni[l,:,:,:]   = np.transpose(arr[3,:,:,:],(2,1,0))
        offset += 4*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

    else :
      rf = open(os.path.join(self.address,seg,'Ion.dat'), 'r')
  
      for l in range(nexits):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        for iz in range(self.meta['nz']):
          for iy in range(self.meta['ny']):
            for ix in range(self.meta['nx']):
              Ion_line = rf.readline().split()
              ui_x[l,ix,iy,iz] = float(Ion_line[0])
              ui_y[l,ix,iy,iz] = float(Ion_line[1])
              ui_z[l,ix,iy,iz] = float(Ion_line[2])
              ni[l,ix,iy,iz] = float(Ion_line[3])

    rf.close()

    ni = ni + 1.
    ui_x = np.divide(ui_x , ni)
    ui_y = np.divide(ui_y , ni)
    ui_z = np.divide(ui_z , ni)

    if (fibo_obj != None) :
      for l in range(nexits):
        time_exit = self.segs[seg][l]
        fibo_obj.data['ui_x_'+time_exit] = ui_x[l,:,:,:]
        fibo_obj.data['ui_y_'+time_exit] = ui_y[l,:,:,:]
        fibo_obj.data['ui_z_'+time_exit] = ui_z[l,:,:,:]
        fibo_obj.data['n_'+time_exit] = ni[l,:,:,:] 
    else: return ni, np.array([ui_x, ui_y, ui_z])
    if not silent: print('done with reading ni, ui!')

  #------------------------------------------------------------
  def get_seg_Press(self,
      seg,        #segment considered (str)
      fibo_obj = None,  #fibo object you are considering - if None, Ion values will just be returned 
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the Pi tensor field in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nexits = len(self.segs[seg])
    Pi_xx = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_yy = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_zz = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_xy = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_xz = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Pi_yz = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])

    if os.path.isfile(os.path.join(self.address,seg,'Press.bin')) :  
    #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'Press.bin'), 'r')
      rfr = rf.read()
      if len(rfr) != (36 + 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'])*nexits : print('===!!!==wrong=segs=:=correct==!!!===')
      offset = 0
      #jump to the correct line in the file and read data vectors
      for l in range(nexits):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        offset += 32 

        flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*6)+'d',rfr[offset:offset+6*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
        arr = np.reshape(flat_arr,(6,self.meta['nz'],self.meta['ny'],self.meta['nx']))
        Pi_xx[l,:,:,:] = np.transpose(arr[0,:,:,:],(2,1,0))
        Pi_yy[l,:,:,:] = np.transpose(arr[1,:,:,:],(2,1,0))
        Pi_zz[l,:,:,:] = np.transpose(arr[2,:,:,:],(2,1,0))
        Pi_xy[l,:,:,:] = np.transpose(arr[3,:,:,:],(2,1,0))
        Pi_xz[l,:,:,:] = np.transpose(arr[4,:,:,:],(2,1,0))
        Pi_yz[l,:,:,:] = np.transpose(arr[5,:,:,:],(2,1,0)) 
        offset += 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

    else :
      rf = open(os.path.join(self.address,seg,'Press.dat'), 'r')
    
      for l in range(nexits):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        for iz in range(self.meta['nz']):
          for iy in range(self.meta['ny']):
            for ix in range(self.meta['nx']):
              Press_line = rf.readline().split()
              Pi_xx[l,ix,iy,iz] = float(Press_line[0])
              Pi_yy[l,ix,iy,iz] = float(Press_line[1])
              Pi_zz[l,ix,iy,iz] = float(Press_line[2])
              Pi_xy[l,ix,iy,iz] = float(Press_line[3])
              Pi_xz[l,ix,iy,iz] = float(Press_line[4])
              Pi_yz[l,ix,iy,iz] = float(Press_line[5])

    rf.close()

    if (fibo_obj != None) :
      for l in range(nexits):
        time_exit = self.segs[seg][l]
        fibo_obj.data['Pi_xx_'+time_exit] = Pi_xx[l,:,:,:]
        fibo_obj.data['Pi_yy_'+time_exit] = Pi_yy[l,:,:,:]
        fibo_obj.data['Pi_zz_'+time_exit] = Pi_zz[l,:,:,:]
        fibo_obj.data['Pi_xy_'+time_exit] = Pi_xy[l,:,:,:]
        fibo_obj.data['Pi_xz_'+time_exit] = Pi_xz[l,:,:,:]
        fibo_obj.data['Pi_yz_'+time_exit] = Pi_yz[l,:,:,:]
    else: return np.array([[Pi_xx, Pi_xy, Pi_xz],[Pi_xy, Pi_yy, Pi_yz],[Pi_xz, Pi_yz, Pi_zz]])
    if not silent: print('done with reading Pi!')

  #------------------------------------------------------------
  def get_seg_Q(self,  #achtung that silvio's Q is actually more than the heat flux - so I will need to post-process it
      seg,      
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the Qi_par,Qi_per fields OR the sQi vector in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nexits = len(self.segs[seg])

    if os.path.isfile(os.path.join(self.address,seg,'Q.bin')) :  
      Qi_par_x = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_par_y = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_par_z = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_per_x = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_per_y = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      Qi_per_z = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])

      #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 6*8*nx*ny*nz data, 4 empty) for each time exit
      rf = open(os.path.join(self.address,seg,'Q.bin'), 'r')
      rfr = rf.read()
      if len(rfr) != (36 + 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'])*nexits : print('===!!!==wrong=segs=:=correct==!!!===')
      offset = 0

      #jump to the correct line in the file and read data vectors
      for l in range(nexits):
        time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        offset += 32 

        flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*6)+'d',rfr[offset:offset+6*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
        arr = np.reshape(flat_arr,(6,self.meta['nz'],self.meta['ny'],self.meta['nx']))
        Qi_par_x[l,:,:,:] = np.transpose(arr[0,:,:,:],(2,1,0))
        Qi_par_y[l,:,:,:] = np.transpose(arr[1,:,:,:],(2,1,0))
        Qi_par_z[l,:,:,:] = np.transpose(arr[2,:,:,:],(2,1,0))
        Qi_per_x[l,:,:,:] = np.transpose(arr[3,:,:,:],(2,1,0))
        Qi_per_y[l,:,:,:] = np.transpose(arr[4,:,:,:],(2,1,0))
        Qi_per_z[l,:,:,:] = np.transpose(arr[5,:,:,:],(2,1,0)) 
        offset += 6*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

      rf.close()

      if (fibo_obj != None) :  
        fibo_obj.xl = self.meta['xl']
        fibo_obj.yl = self.meta['yl']
        fibo_obj.zl = self.meta['zl']
        fibo_obj.nx = self.meta['nx']
        fibo_obj.ny = self.meta['ny']
        fibo_obj.nz = self.meta['nz']
        for l in range(nexits):
          time_exit = self.segs[seg][l]
          fibo_obj.data['Qi_par_x_'+time_exit] = Qi_par_x[l,:,:,:]
          fibo_obj.data['Qi_par_y_'+time_exit] = Qi_par_y[l,:,:,:]
          fibo_obj.data['Qi_par_z_'+time_exit] = Qi_par_z[l,:,:,:]
          fibo_obj.data['Qi_per_x_'+time_exit] = Qi_per_x[l,:,:,:]
          fibo_obj.data['Qi_per_y_'+time_exit] = Qi_per_y[l,:,:,:]
          fibo_obj.data['Qi_per_z_'+time_exit] = Qi_per_z[l,:,:,:]
      else: return np.array([Qi_par_x, Qi_par_y, Qi_par_z]), np.array([Qi_per_x, Qi_per_y, Qi_per_z])
      if not silent: print('done with reading Qi_par and Qi_per!')

    else :
      #create data vectors
      sQi_x = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      sQi_y = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
      sQi_z = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])

      rf = open(os.path.join(self.address,seg,'Q.dat'), 'r')
  
      for l in range(nexits):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        if not silent: print('reading time:' , time_exit)
        for iz in range(self.meta['nz']):
          for iy in range(self.meta['ny']):
            for ix in range(self.meta['nx']):
              Q_line = rf.readline().split()
              sQi_x[l,ix,iy,iz] = float(Q_line[0])
              sQi_y[l,ix,iy,iz] = float(Q_line[1])
              sQi_z[l,ix,iy,iz] = float(Q_line[2])

      rf.close()

      if (fibo_obj != None) :
        for l in range(nexits):
          time_exit = self.segs[seg][l]
          fibo_obj.data['sQi_x_'+time_exit] = sQi_x[l,:,:,:]
          fibo_obj.data['sQi_y_'+time_exit] = sQi_y[l,:,:,:]
          fibo_obj.data['sQi_z_'+time_exit] = sQi_z[l,:,:,:]
      else: return np.array([sQi_x, sQi_y, sQi_z])
      if not silent: print('done with reading sQi!')

  #------------------------------------------------------------
  def get_seg_Te(self,   
      seg,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the Te_per,Te_par fields in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nexits = len(self.segs[seg])

    Te_par = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])
    Te_per = np.empty([nexits,self.meta['nx'],self.meta['ny'],self.meta['nz']])

    #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 2*8*nx*ny*nz data, 4 empty) for each time exit
    rf = open(os.path.join(self.address,seg,'Te.bin'), 'r')
    rfr = rf.read()
    if len(rfr) != (36 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'])*nexits : print('===!!!==wrong=segs=:=correct==!!!===')
    offset = 0

    #jump to the correct line in the file and read data vectors
    for l in range(nexits):
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)
      offset += 32 

      flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      Te_par[l,:,:,:] = np.transpose(arr[0,:,:,:],(2,1,0)) + 0.5 * self.meta['beta'] * self.meta['teti']
      Te_per[l,:,:,:] = np.transpose(arr[1,:,:,:],(2,1,0)) + 0.5 * self.meta['beta'] * self.meta['teti'] * self.meta['alpha_e']
      offset += 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4

    rf.close()

    if (fibo_obj != None) :
      for l in range(nexits): 
        time_exit = self.segs[seg][l]
        fibo_obj.data['Te_par_'+time_exit] = Te_par[l,:,:,:]
        fibo_obj.data['Te_per_'+time_exit] = Te_per[l,:,:,:]
    else: return np.array([Te_par, Te_per])
    if not silent: print('done with reading Te_par and Te_per!')

  #------------------------------------------------------------
  def get_seg_Qe(self,   
      seg,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the qe_per,qe_par fields at the nth exit of the data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nexits = len(self.segs[seg])

    qe_par = np.empty([nexits, self.meta['nx'],self.meta['ny'],self.meta['nz']])
    qe_per = np.empty([nexits, self.meta['nx'],self.meta['ny'],self.meta['nz']])

    #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 2*8*nx*ny*nz data, 4 empty) for each time exit
    rf = open(os.path.join(self.address,seg,'Qe.bin'), 'r')
    rfr = rf.read()
    if len(rfr) != (36 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'])*nexits : print('===!!!==wrong=segs=:=correct==!!!===')
    offset = 0

    #jump to the correct line in the file
    for l in range(nexits):
      time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      if not silent: print('reading time:' , time_exit)
      offset += 32 

      flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
      arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
      qe_par[l,:,:,:] = np.transpose(arr[0,:,:,:],(2,1,0))
      qe_per[l,:,:,:] = np.transpose(arr[1,:,:,:],(2,1,0))

    rf.close()

    if (fibo_obj != None) :
      for l in range(nexits): 
        time_exit = self.segs[seg][exit_num]
        fibo_obj.data['qe_par_'+time_exit] = qe_par[l,:,:,:]
        fibo_obj.data['qe_per_'+time_exit] = qe_per[l,:,:,:]
    else: return np.array([qe_par, qe_per])
    if not silent: print('done with reading qe_par and qe_per!')


  #------------------------------------------------------------
  #------------------------------------------------------------
  def calc_all(self,
      time_exit,
      fibo_obj = None,
      list_terms = ['mom_1','mom_2','mom_3'],
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      calcs all from E,B,ni,ui,Pi,sQi OR Qi_par,Qi_per ((Te_par,Te_per,qe_par,qe_per)) 
    ------------------------------------------------------------------------------------
    time_exit              [str] time exit (usually format '0000.000')
    fibo_obj = None        [fibo_obj] fibo to fill - if None, you will be insulted
    list_terms = ['mom_1','mom_2','mom_3'] choose how much you want ... 
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    if fibo_obj == None : print('FDP: creer un object fibo SVP')

    #rename ni as n
    #if 'ni_'+time_exit in fibo_obj.data.keys() :
    #  fibo_obj.data['n_'+time_exit] = fibo_obj.data.pop('ni_'+time_exit) 

    #create current density and magnetic field curvature
    iB = np.sqrt(np.reciprocal(fibo_obj.calc_scalr('B_x_'+time_exit,'B_x_'+time_exit,'B_y_'+time_exit,'B_y_'+time_exit,'B_z_'+time_exit,'B_z_'+time_exit)))
    bx, by, bz = iB*fibo_obj.data['B_x_'+time_exit], iB*fibo_obj.data['B_y_'+time_exit], iB*fibo_obj.data['B_z_'+time_exit]

    fibo_obj.data['J_x_'+time_exit], fibo_obj.data['J_y_'+time_exit], fibo_obj.data['J_z_'+time_exit] =  fibo_obj.calc_curl('B_x_'+time_exit,'B_y_'+time_exit,'B_z_'+time_exit)

    fibo_obj.data['C_x_'+time_exit] = bx * fibo_obj.calc_gradx(bx) + by * fibo_obj.calc_grady(bx) + bz * fibo_obj.calc_gradz(bx)
    fibo_obj.data['C_y_'+time_exit] = bx * fibo_obj.calc_gradx(by) + by * fibo_obj.calc_grady(by) + bz * fibo_obj.calc_gradz(by)
    fibo_obj.data['C_z_'+time_exit] = bx * fibo_obj.calc_gradx(bz) + by * fibo_obj.calc_grady(bz) + bz * fibo_obj.calc_gradz(bz)

    #local electromagnetic energy density
    fibo_obj.data['enE_'+time_exit]  = np.square(fibo_obj.data['E_x_'+time_exit])
    fibo_obj.data['enE_'+time_exit] += np.square(fibo_obj.data['E_y_'+time_exit])
    fibo_obj.data['enE_'+time_exit] += np.square(fibo_obj.data['E_z_'+time_exit])
    fibo_obj.data['enE_'+time_exit] *= 0.5

    fibo_obj.data['enB_'+time_exit]  = np.square(fibo_obj.data['B_x_'+time_exit])
    fibo_obj.data['enB_'+time_exit] += np.square(fibo_obj.data['B_y_'+time_exit])
    fibo_obj.data['enB_'+time_exit] += np.square(fibo_obj.data['B_z_'+time_exit])
    fibo_obj.data['enB_'+time_exit] *= 0.5


    if np.any(['mom_1' in list_terms,'mom_2' in list_terms,'mom_3' in list_terms]) :
      #create ue and u
      fibo_obj.data['ue_x_'+time_exit] = fibo_obj.data['ui_x_'+time_exit] - np.divide(fibo_obj.data['J_x_'+time_exit],fibo_obj.data['n_'+time_exit])
      fibo_obj.data['ue_y_'+time_exit] = fibo_obj.data['ui_y_'+time_exit] - np.divide(fibo_obj.data['J_y_'+time_exit],fibo_obj.data['n_'+time_exit])
      fibo_obj.data['ue_z_'+time_exit] = fibo_obj.data['ui_z_'+time_exit] - np.divide(fibo_obj.data['J_z_'+time_exit],fibo_obj.data['n_'+time_exit])
  
      fibo_obj.data['u_x_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_x_'+time_exit] + fibo_obj.data['ue_x_'+time_exit]) / (1. + self.meta['mime'])
      fibo_obj.data['u_y_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_y_'+time_exit] + fibo_obj.data['ue_y_'+time_exit]) / (1. + self.meta['mime'])
      fibo_obj.data['u_z_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_z_'+time_exit] + fibo_obj.data['ue_z_'+time_exit]) / (1. + self.meta['mime'])
  
      #local kinetic energy density
      fibo_obj.data['Ki_'+time_exit]  = np.square(fibo_obj.data['ui_x_'+time_exit])
      fibo_obj.data['Ki_'+time_exit] += np.square(fibo_obj.data['ui_y_'+time_exit])
      fibo_obj.data['Ki_'+time_exit] += np.square(fibo_obj.data['ui_z_'+time_exit])
      fibo_obj.data['Ki_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['Ki_'+time_exit])/2.
  
      fibo_obj.data['Ke_'+time_exit]  = np.square(fibo_obj.data['ue_x_'+time_exit])
      fibo_obj.data['Ke_'+time_exit] += np.square(fibo_obj.data['ue_y_'+time_exit])
      fibo_obj.data['Ke_'+time_exit] += np.square(fibo_obj.data['ue_z_'+time_exit])
      fibo_obj.data['Ke_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['Ke_'+time_exit])/(2.*self.meta['mime'])
  
      fibo_obj.data['K_'+time_exit]  = np.square(fibo_obj.data['u_x_'+time_exit])
      fibo_obj.data['K_'+time_exit] += np.square(fibo_obj.data['u_y_'+time_exit])
      fibo_obj.data['K_'+time_exit] += np.square(fibo_obj.data['u_z_'+time_exit])
      fibo_obj.data['K_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['K_'+time_exit])*(self.meta['mime']+1)/(2.*self.meta['mime'])

      if not silent: print('done with calculating mom_1!')

    if np.any(['mom_2' in list_terms,'mom_3' in list_terms]) :
      #process Pi, create Pe and P
      fibo_obj.data['Pe_xx_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Pe_yy_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Pe_zz_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Pe_xy_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Pe_xz_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Pe_yz_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
  
      if 'Te_par_'+time_exit in fibo_obj.data.keys() :
        myfac = iB*iB*(fibo_obj.data['Te_par_'+time_exit] - fibo_obj.data['Te_per_'+time_exit]) 
  
        fibo_obj.data['Pe_xx_'+time_exit] += fibo_obj.data['B_x_'+time_exit]*fibo_obj.data['B_x_'+time_exit]*myfac + fibo_obj.data['Te_per_'+time_exit]
        fibo_obj.data['Pe_xx_'+time_exit] *= fibo_obj.data['n_'+time_exit]
        fibo_obj.data['Pe_yy_'+time_exit] += fibo_obj.data['B_y_'+time_exit]*fibo_obj.data['B_y_'+time_exit]*myfac + fibo_obj.data['Te_per_'+time_exit]
        fibo_obj.data['Pe_yy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
        fibo_obj.data['Pe_zz_'+time_exit] += fibo_obj.data['B_z_'+time_exit]*fibo_obj.data['B_z_'+time_exit]*myfac + fibo_obj.data['Te_per_'+time_exit]
        fibo_obj.data['Pe_zz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
        fibo_obj.data['Pe_xy_'+time_exit] += fibo_obj.data['B_x_'+time_exit]*fibo_obj.data['B_y_'+time_exit]*myfac
        fibo_obj.data['Pe_xy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
        fibo_obj.data['Pe_xz_'+time_exit] += fibo_obj.data['B_x_'+time_exit]*fibo_obj.data['B_z_'+time_exit]*myfac
        fibo_obj.data['Pe_xz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
        fibo_obj.data['Pe_yz_'+time_exit] += fibo_obj.data['B_y_'+time_exit]*fibo_obj.data['B_z_'+time_exit]*myfac
        fibo_obj.data['Pe_yz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
  
      else : 
        Pe = 0.5 * self.meta['beta'] * self.meta['teti'] * fibo_obj.data['n_'+time_exit] #- 0.5d0*beta*TeTi 
  
        fibo_obj.data['Pe_xx_'+time_exit] += Pe
        fibo_obj.data['Pe_yy_'+time_exit] += Pe
        fibo_obj.data['Pe_zz_'+time_exit] += Pe
  
      fibo_obj.data['P_xx_'+time_exit]  = + np.square(fibo_obj.data['ui_x_'+time_exit]) 
      fibo_obj.data['P_xx_'+time_exit] += + np.square(fibo_obj.data['ue_x_'+time_exit]) / self.meta['mime'] 
      fibo_obj.data['P_xx_'+time_exit] -= + np.square(fibo_obj.data['u_x_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime'] 
      fibo_obj.data['P_yy_'+time_exit]  = + np.square(fibo_obj.data['ui_y_'+time_exit]) 
      fibo_obj.data['P_yy_'+time_exit] += + np.square(fibo_obj.data['ue_y_'+time_exit]) / self.meta['mime']
      fibo_obj.data['P_yy_'+time_exit] -= + np.square(fibo_obj.data['u_y_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
      fibo_obj.data['P_zz_'+time_exit]  = + np.square(fibo_obj.data['ui_z_'+time_exit]) 
      fibo_obj.data['P_zz_'+time_exit] += + np.square(fibo_obj.data['ue_z_'+time_exit]) / self.meta['mime']
      fibo_obj.data['P_zz_'+time_exit] -= + np.square(fibo_obj.data['u_z_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
      fibo_obj.data['P_xy_'+time_exit]  = + np.multiply(fibo_obj.data['ui_x_'+time_exit] , fibo_obj.data['ui_y_'+time_exit])
      fibo_obj.data['P_xy_'+time_exit] += + np.multiply(fibo_obj.data['ue_x_'+time_exit] , fibo_obj.data['ue_y_'+time_exit]) / self.meta['mime']
      fibo_obj.data['P_xy_'+time_exit] -= + np.multiply(fibo_obj.data['u_x_'+time_exit] ,  fibo_obj.data['u_y_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
      fibo_obj.data['P_xz_'+time_exit]  = + np.multiply(fibo_obj.data['ui_x_'+time_exit] , fibo_obj.data['ui_z_'+time_exit])
      fibo_obj.data['P_xz_'+time_exit] += + np.multiply(fibo_obj.data['ue_x_'+time_exit] , fibo_obj.data['ue_z_'+time_exit]) / self.meta['mime']
      fibo_obj.data['P_xz_'+time_exit] -= + np.multiply(fibo_obj.data['u_x_'+time_exit] ,  fibo_obj.data['u_z_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
      fibo_obj.data['P_yz_'+time_exit]  = + np.multiply(fibo_obj.data['ui_y_'+time_exit] , fibo_obj.data['ui_z_'+time_exit])
      fibo_obj.data['P_yz_'+time_exit] += + np.multiply(fibo_obj.data['ue_y_'+time_exit] , fibo_obj.data['ue_z_'+time_exit]) / self.meta['mime']
      fibo_obj.data['P_yz_'+time_exit] -= + np.multiply(fibo_obj.data['u_y_'+time_exit] ,  fibo_obj.data['u_z_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
  
      fibo_obj.data['P_xx_'+time_exit] *= fibo_obj.data['n_'+time_exit]
      fibo_obj.data['P_yy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
      fibo_obj.data['P_zz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
      fibo_obj.data['P_xy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
      fibo_obj.data['P_xz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
      fibo_obj.data['P_yz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
  
      fibo_obj.data['P_xx_'+time_exit] += fibo_obj.data['Pi_xx_'+time_exit] + fibo_obj.data['Pe_xx_'+time_exit]  
      fibo_obj.data['P_yy_'+time_exit] += fibo_obj.data['Pi_yy_'+time_exit] + fibo_obj.data['Pe_yy_'+time_exit]
      fibo_obj.data['P_zz_'+time_exit] += fibo_obj.data['Pi_zz_'+time_exit] + fibo_obj.data['Pe_zz_'+time_exit]
      fibo_obj.data['P_xy_'+time_exit] += fibo_obj.data['Pi_xy_'+time_exit] + fibo_obj.data['Pe_xy_'+time_exit] 
      fibo_obj.data['P_xz_'+time_exit] += fibo_obj.data['Pi_xz_'+time_exit] + fibo_obj.data['Pe_xz_'+time_exit] 
      fibo_obj.data['P_yz_'+time_exit] += fibo_obj.data['Pi_yz_'+time_exit] + fibo_obj.data['Pe_yz_'+time_exit] 
  
      #local internal energy density
      fibo_obj.data['Ui_'+time_exit] = (fibo_obj.data['Pi_xx_'+time_exit] + fibo_obj.data['Pi_yy_'+time_exit] + fibo_obj.data['Pi_zz_'+time_exit]) /2.
      fibo_obj.data['Ue_'+time_exit] = (fibo_obj.data['Pe_xx_'+time_exit] + fibo_obj.data['Pe_yy_'+time_exit] + fibo_obj.data['Pe_zz_'+time_exit]) /2.
      fibo_obj.data['U_'+time_exit]  = (fibo_obj.data['P_xx_'+time_exit] + fibo_obj.data['P_yy_'+time_exit] + fibo_obj.data['P_zz_'+time_exit]) /2.

      if not silent: print('done with calculating mom_2!')

    if ('mom_3' in list_terms) :
      #calculate heat fluxes
      fibo_obj.data['Qe_x_'+time_exit] = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Qe_y_'+time_exit] = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
      fibo_obj.data['Qe_z_'+time_exit] = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
  
      if 'qe_par_'+time_exit in fibo_obj.data.keys() :
        s1 = fibo_obj.data['Te_par_'+time_exit] + 2 * fibo_obj.data['Te_per_'+time_exit]
        s2 = 2 * fibo_obj.data['Te_par_'+time_exit] * (1 - fibo_obj.data['Te_par_'+time_exit] / fibo_obj.data['Te_per_'+time_exit])
        myfac = iB * fibo_obj.data['n_'+time_exit] * fibo_obj.data['Te_per_'+time_exit]
  
        addx = myfac * (fibo_obj.calc_gradx(s1) - fibo_obj.data['C_x_'+time_exit] * s2)
        addy = myfac * (fibo_obj.calc_grady(s1) - fibo_obj.data['C_y_'+time_exit] * s2)
        addz = myfac * (fibo_obj.calc_gradz(s1) - fibo_obj.data['C_z_'+time_exit] * s2)
  
        fibo_obj.data['Qe_x_'+time_exit], fibo_obj.data['Qe_y_'+time_exit], fibo_obj.data['Qe_z_'+time_exit] = fibo_obj.calc_cross(addx,bx,addy,by,addz,bz)
  
        myfac = 2 * fibo_obj.data['qe_per_'+time_exit] + fibo_obj.data['qe_par_'+time_exit]
  
        fibo_obj.data['Qe_x_'+time_exit] += bx * myfac
        fibo_obj.data['Qe_y_'+time_exit] += by * myfac
        fibo_obj.data['Qe_z_'+time_exit] += bz * myfac
  
      #fibo_obj.data['sQe_x_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_x_'+time_exit])
      #fibo_obj.data['sQe_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_x_'+time_exit])
      #fibo_obj.data['sQe_y_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_y_'+time_exit])
      #fibo_obj.data['sQe_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_y_'+time_exit])
      #fibo_obj.data['sQe_z_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_z_'+time_exit])
      #fibo_obj.data['sQe_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_z_'+time_exit])  
      fibo_obj.data['sQe_x_'+time_exit]  = + fibo_obj.data['Qe_x_'+time_exit] 
      fibo_obj.data['sQe_x_'+time_exit] +=   2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_x_'+time_exit])
      fibo_obj.data['sQe_x_'+time_exit] +=   2 * fibo_obj.calc_scalr('Pe_xx_'+time_exit,'ue_x_'+time_exit,'Pe_xy_'+time_exit,'ue_y_'+time_exit,'Pe_xz_'+time_exit,'ue_z_'+time_exit)
      fibo_obj.data['sQe_y_'+time_exit]  = + fibo_obj.data['Qe_y_'+time_exit] 
      fibo_obj.data['sQe_y_'+time_exit] +=   2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_y_'+time_exit])
      fibo_obj.data['sQe_y_'+time_exit] +=   2 * fibo_obj.calc_scalr('Pe_xy_'+time_exit,'ue_x_'+time_exit,'Pe_yy_'+time_exit,'ue_y_'+time_exit,'Pe_yz_'+time_exit,'ue_z_'+time_exit)
      fibo_obj.data['sQe_z_'+time_exit]  = + fibo_obj.data['Qe_z_'+time_exit] 
      fibo_obj.data['sQe_z_'+time_exit] +=   2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_z_'+time_exit])
      fibo_obj.data['sQe_z_'+time_exit] +=   2 * fibo_obj.calc_scalr('Pe_xz_'+time_exit,'ue_x_'+time_exit,'Pe_yz_'+time_exit,'ue_y_'+time_exit,'Pe_zz_'+time_exit,'ue_z_'+time_exit)
  
      if 'sQi_x_'+time_exit in fibo_obj.data.keys() : #from sQi to Qi
        #fibo_obj.data['sQ_x_'+time_exit] = fibo_obj.data['sQi_x_'+time_exit] + fibo_obj.data['sQe_x_'+time_exit]
        #fibo_obj.data['sQ_y_'+time_exit] = fibo_obj.data['sQi_y_'+time_exit] + fibo_obj.data['sQe_y_'+time_exit]
        #fibo_obj.data['sQ_z_'+time_exit] = fibo_obj.data['sQi_z_'+time_exit] + fibo_obj.data['sQe_z_'+time_exit]
  
        fibo_obj.data['Qi_x_'+time_exit]  = + fibo_obj.data['sQi_x_'+time_exit] 
        fibo_obj.data['Qi_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_x_'+time_exit])
        fibo_obj.data['Qi_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xx_'+time_exit,'ui_x_'+time_exit,'Pi_xy_'+time_exit,'ui_y_'+time_exit,'Pi_xz_'+time_exit,'ui_z_'+time_exit)
        fibo_obj.data['Qi_y_'+time_exit]  = + fibo_obj.data['sQi_y_'+time_exit] 
        fibo_obj.data['Qi_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
        fibo_obj.data['Qi_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xy_'+time_exit,'ui_x_'+time_exit,'Pi_yy_'+time_exit,'ui_y_'+time_exit,'Pi_yz_'+time_exit,'ui_z_'+time_exit)
        fibo_obj.data['Qi_z_'+time_exit]  = + fibo_obj.data['sQi_z_'+time_exit]
        fibo_obj.data['Qi_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
        fibo_obj.data['Qi_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xz_'+time_exit,'ui_x_'+time_exit,'Pi_yz_'+time_exit,'ui_y_'+time_exit,'Pi_zz_'+time_exit,'ui_z_'+time_exit)
  
        #fibo_obj.data['Q_x_'+time_exit]  = + fibo_obj.data['sQ_x_'+time_exit] 
        #fibo_obj.data['Q_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_x_'+time_exit])
        #fibo_obj.data['Q_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xx_'+time_exit,'u_x_'+time_exit,'P_xy_'+time_exit,'u_y_'+time_exit,'P_xz_'+time_exit,'u_z_'+time_exit)
        #fibo_obj.data['Q_y_'+time_exit]  = + fibo_obj.data['sQ_y_'+time_exit] 
        #fibo_obj.data['Q_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_y_'+time_exit])
        #fibo_obj.data['Q_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xy_'+time_exit,'u_x_'+time_exit,'P_yy_'+time_exit,'u_y_'+time_exit,'P_yz_'+time_exit,'u_z_'+time_exit)
        #fibo_obj.data['Q_z_'+time_exit]  = + fibo_obj.data['sQ_z_'+time_exit] 
        #fibo_obj.data['Q_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_z_'+time_exit])
        #fibo_obj.data['Q_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xz_'+time_exit,'u_x_'+time_exit,'P_yz_'+time_exit,'u_y_'+time_exit,'P_zz_'+time_exit,'u_z_'+time_exit)
  
      else : #reconstruct Qi, then from Qi to sQi
        fibo_obj.data['Qi_x_'+time_exit] = fibo_obj.data['Qi_par_x_'+time_exit] + 2.*fibo_obj.data['Qi_per_x_'+time_exit]
        fibo_obj.data['Qi_y_'+time_exit] = fibo_obj.data['Qi_par_y_'+time_exit] + 2.*fibo_obj.data['Qi_per_y_'+time_exit]
        fibo_obj.data['Qi_z_'+time_exit] = fibo_obj.data['Qi_par_z_'+time_exit] + 2.*fibo_obj.data['Qi_per_z_'+time_exit]
  
        fibo_obj.data['sQi_x_'+time_exit]  = + fibo_obj.data['Qi_x_'+time_exit] 
        fibo_obj.data['sQi_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_x_'+time_exit])
        fibo_obj.data['sQi_x_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xx_'+time_exit,'ui_x_'+time_exit,'Pi_xy_'+time_exit,'ui_y_'+time_exit,'Pi_xz_'+time_exit,'ui_z_'+time_exit)
        fibo_obj.data['sQi_y_'+time_exit]  = + fibo_obj.data['Qi_y_'+time_exit] 
        fibo_obj.data['sQi_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
        fibo_obj.data['sQi_y_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xy_'+time_exit,'ui_x_'+time_exit,'Pi_yy_'+time_exit,'ui_y_'+time_exit,'Pi_yz_'+time_exit,'ui_z_'+time_exit)
        fibo_obj.data['sQi_z_'+time_exit]  = + fibo_obj.data['Qi_z_'+time_exit]
        fibo_obj.data['sQi_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_z_'+time_exit])
        fibo_obj.data['sQi_z_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xz_'+time_exit,'ui_x_'+time_exit,'Pi_yz_'+time_exit,'ui_y_'+time_exit,'Pi_zz_'+time_exit,'ui_z_'+time_exit)
  
      fibo_obj.data['sQ_x_'+time_exit] = fibo_obj.data['sQi_x_'+time_exit] + fibo_obj.data['sQe_x_'+time_exit]
      fibo_obj.data['sQ_y_'+time_exit] = fibo_obj.data['sQi_y_'+time_exit] + fibo_obj.data['sQe_y_'+time_exit]
      fibo_obj.data['sQ_z_'+time_exit] = fibo_obj.data['sQi_z_'+time_exit] + fibo_obj.data['sQe_z_'+time_exit]
  
      fibo_obj.data['Q_x_'+time_exit]  = + fibo_obj.data['sQ_x_'+time_exit] 
      fibo_obj.data['Q_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_x_'+time_exit])
      fibo_obj.data['Q_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xx_'+time_exit,'u_x_'+time_exit,'P_xy_'+time_exit,'u_y_'+time_exit,'P_xz_'+time_exit,'u_z_'+time_exit)
      fibo_obj.data['Q_y_'+time_exit]  = + fibo_obj.data['sQ_y_'+time_exit] 
      fibo_obj.data['Q_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_y_'+time_exit])
      fibo_obj.data['Q_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xy_'+time_exit,'u_x_'+time_exit,'P_yy_'+time_exit,'u_y_'+time_exit,'P_yz_'+time_exit,'u_z_'+time_exit)
      fibo_obj.data['Q_z_'+time_exit]  = + fibo_obj.data['sQ_z_'+time_exit] 
      fibo_obj.data['Q_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_z_'+time_exit])
      fibo_obj.data['Q_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xz_'+time_exit,'u_x_'+time_exit,'P_yz_'+time_exit,'u_y_'+time_exit,'P_zz_'+time_exit,'u_z_'+time_exit)

      if not silent: print('done with calculating mom_3!')

    if not silent: print('done with calculating all!')

  #------------------------------------------------------------
  def clean_old_vars(self,
      time_exit,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      deletes sQi,sQe,sQ ((Te_par, Te_per, qe_par, qe_per)) 
    ------------------------------------------------------------------------------------
    time_exit              [str] time exit (usually format '0000.000')
    fibo_obj = None        [fibo_obj] fibo to fill - if None, you will be insulted
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    if fibo_obj == None : print('FDP: creer un object fibo SVP')

    if 'sQi_x_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['sQi_x_'+time_exit]
      del fibo_obj.data['sQi_y_'+time_exit]
      del fibo_obj.data['sQi_z_'+time_exit]
    if 'sQe_x_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['sQe_x_'+time_exit]
      del fibo_obj.data['sQe_y_'+time_exit]
      del fibo_obj.data['sQe_z_'+time_exit]
    if 'sQ_x_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['sQ_x_'+time_exit]
      del fibo_obj.data['sQ_y_'+time_exit]
      del fibo_obj.data['sQ_z_'+time_exit]
    
    if 'Te_par_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['Te_par_'+time_exit]
      del fibo_obj.data['Te_per_'+time_exit]
    if 'qe_par_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['qe_par_'+time_exit]
      del fibo_obj.data['qe_per_'+time_exit]

    if not silent: print('done with cleaning sQi,sQe,sQ ((Te_par, Te_per, qe_par, qe_per)) ')

  #------------------------------------------------------------
  def clean_new_vars(self,
      time_exit,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      deletes ue,Pe,Qe,sQe, u,P,Q,sQ, enE,enB, Ki,Ke,K,Ui,Ue,U 
    ------------------------------------------------------------------------------------
    time_exit              [str] time exit (usually format '0000.000')
    fibo_obj = None        [fibo_obj] fibo to fill - if None, you will be insulted
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    if fibo_obj == None : print('FDP: creer un object fibo SVP')

    if 'ue_x_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['ue_x_'+time_exit]
      del fibo_obj.data['ue_y_'+time_exit]
      del fibo_obj.data['ue_z_'+time_exit]
    if 'Pe_xx_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['Pe_xx_'+time_exit]
      del fibo_obj.data['Pe_yy_'+time_exit]
      del fibo_obj.data['Pe_zz_'+time_exit]
      del fibo_obj.data['Pe_xy_'+time_exit]
      del fibo_obj.data['Pe_xz_'+time_exit]
      del fibo_obj.data['Pe_yz_'+time_exit]
    if 'Qe_x_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['Qe_x_'+time_exit]
      del fibo_obj.data['Qe_y_'+time_exit]
      del fibo_obj.data['Qe_z_'+time_exit]
    if 'sQe_x_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['sQe_x_'+time_exit]
      del fibo_obj.data['sQe_y_'+time_exit]
      del fibo_obj.data['sQe_z_'+time_exit]
  
    if 'u_x_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['u_x_'+time_exit]
      del fibo_obj.data['u_y_'+time_exit]
      del fibo_obj.data['u_z_'+time_exit]
    if 'P_xx_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['P_xx_'+time_exit]
      del fibo_obj.data['P_yy_'+time_exit]
      del fibo_obj.data['P_zz_'+time_exit]
      del fibo_obj.data['P_xy_'+time_exit]
      del fibo_obj.data['P_xz_'+time_exit]
      del fibo_obj.data['P_yz_'+time_exit]
    if 'Q_x_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['Q_x_'+time_exit]
      del fibo_obj.data['Q_y_'+time_exit]
      del fibo_obj.data['Q_z_'+time_exit]
    if 'sQ_x_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['sQ_x_'+time_exit]
      del fibo_obj.data['sQ_y_'+time_exit]
      del fibo_obj.data['sQ_z_'+time_exit]
  
  
    del fibo_obj.data['enE_'+time_exit]
    del fibo_obj.data['enB_'+time_exit]
    if 'K_'+time_exit in fibo_obj.data.keys() :  
      del fibo_obj.data['Ki_'+time_exit]
      del fibo_obj.data['Ke_'+time_exit]
      del fibo_obj.data['K_'+time_exit]
    if 'U_'+time_exit in fibo_obj.data.keys() :
      del fibo_obj.data['Ui_'+time_exit]
      del fibo_obj.data['Ue_'+time_exit]
      del fibo_obj.data['U_'+time_exit]

    if not silent: print('done with cleaning ue,Pe,Qe,sQe, u,P,Q,sQ, enE,enB, Ki,Ke,K,Ui,Ue,U ')

