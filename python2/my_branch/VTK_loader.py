# coding: utf-8

#---------------------------------------------------------------------------------------
#-(1)-----21.12.18----FDP:S,F-----------------------------------------------------------
#-(2)-----30.12.18----FDP:S,J,R---------------------------------------------------------
#-(3)-----02.04.19----FDP:S,P,L---------------------------------------------------------
#-(4)-----04.04.19----FDP:S,J,L---------------------------------------------------------
#---------------------------------------------------------------------------------------
#-(alpha)-19.07.19----FDP:S,J,L---------------------------------------------------------
#-(beta0)-12.11.19----FDP:S,F-----------------------------------------------------------
#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------

import numpy as np
import os

#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------
class from_VTK (object):

  def __init__(self, 
      address):
    """ 
    ------------------------------------------------------------------------------------
      creates the object to retrieve data from VTK files
    ------------------------------------------------------------------------------------
    address      [address] where your data are (folder with segs inside)
    ------------------------------------------------------------------------------------
    """
    self.address = address
    self.meta = {}
    
  #------------------------------------------------------------
  def get_scal(self,
      tar_file,
      fibo_obj = None,  
      tar_var = None,  
      double_y = False):
    """ 
    ------------------------------------------------------------------------------------
      reads scalar from .vtk file
    ------------------------------------------------------------------------------------
    tar_name           [str] target file to read (don't include '.vtk')
    fibo_obj = None    [None or fibo] fibo object you want to fill, else returns values 
    tar_var = None         [None or str] name the.variable will be given
    double_y = False   [bool] was your file printed twice in y?
    ------------------------------------------------------------------------------------
    scal               [fibo_var] 
    ------------------------------------------------------------------------------------
    """  

    #create data vector, fill it!
    data_file = open(os.path.join(self.address,tar_file+'.vtk'),'r')
    
    data_file.readline()
    if tar_var == None : tar_var = data_file.readline().split()[0]
    else : data_file.readline()
    data_file.readline()
    data_file.readline()
    self.meta['nx'], self.meta['ny'], self.meta['nz'] = map(int, data_file.readline().split()[1:4])
    data_file.readline()
    self.meta['dx'], self.meta['dy'], self.meta['dz'] = map(float, data_file.readline().split()[1:4])
    data_file.readline()
    data_file.readline()  #NB here you have the nx*ny*nz preduct
    data_file.readline()
    data_file.readline()

    if double_y : self.meta['ny'] = self.meta['ny']/2 #NB here you divide by two the box in y!

    self.meta['xl'] = self.meta['nx']*self.meta['dx'] #(self.n#x-1)*dx
    self.meta['yl'] = self.meta['ny']*self.meta['dy']
    self.meta['zl'] = self.meta['nz']*self.meta['dz']

    self.meta['nnn'] = (self.meta['nx'], self.meta['ny'], self.meta['nz'])
    self.meta['lll'] = (self.meta['xl'], self.meta['yl'], self.meta['zl'])
    self.meta['ddd'] = (self.meta['dx'], self.meta['dy'], self.meta['dz'])

    scal = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    
    for iz in range(self.meta['nz']):
      for iy in range(self.meta['ny']):
        for ix in range(self.meta['nx']):
          scal[ix,iy,iz] = float(data_file.readline().split()[0])
      if double_y :
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            data_file.readline()
    
    data_file.close()

    if (fibo_obj != None) :
      fibo_obj.data[tar_var] = scal
    else: return scal

    #if not silent: print('done with the reading!')

  #------------------------------------------------------------
  def get_vect(self,
      tar_file,
      fibo_obj = None,
      tar_var = None,
      double_y = False):
    """ 
    ------------------------------------------------------------------------------------
      reads vector from .vtk file
    ------------------------------------------------------------------------------------
    tar_file           [str] target file to read (don't include '.vtk')
    fibo_obj = None    [None or fibo] fibo object you want to fill, else returns values 
    tar_var = None     [None or str] name the.variable will be given
    double_y = False   [bool] was your file printed twice in y?
    ------------------------------------------------------------------------------------
    scal               [fibo_var] 
    ------------------------------------------------------------------------------------
    """

    #create data vector, fill it!
    data_file = open(os.path.join(self.address,tar_file+'.vtk'),'r')
    
    data_file.readline()
    if tar_var == None : 
      tar_var_x,tar_var_y,tar_var_z = data_file.readline().split()[0][1:-1].split(',')
    else : 
      tar_var_x = tar_var+'_x'
      tar_var_y = tar_var+'_y'
      tar_var_z = tar_var+'_z'
      data_file.readline()
    data_file.readline()
    data_file.readline()
    self.meta['nx'], self.meta['ny'], self.meta['nz'] = map(int, data_file.readline().split()[1:4])
    data_file.readline()
    self.meta['dx'], self.meta['dy'], self.meta['dz'] = map(float, data_file.readline().split()[1:4])
    data_file.readline()
    data_file.readline()  #NB here you have the nx*ny*nz preduct
    data_file.readline()
    
    if double_y : self.meta['ny'] = self.meta['ny']/2  #NB here you divide by two the box in y!

    self.meta['xl'] = self.meta['nx']*self.meta['dx'] #(self.n#x-1)*dx
    self.meta['yl'] = self.meta['ny']*self.meta['dy']
    self.meta['zl'] = self.meta['nz']*self.meta['dz']

    self.meta['nnn'] = (self.meta['nx'], self.meta['ny'], self.meta['nz'])
    self.meta['lll'] = (self.meta['xl'], self.meta['yl'], self.meta['zl'])
    self.meta['ddd'] = (self.meta['dx'], self.meta['dy'], self.meta['dz'])

    vect_x = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    vect_y = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    vect_z = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
    
    for iz in range(self.meta['nz']):
      for iy in range(self.meta['ny']):
        for ix in range(self.meta['nx']):
          aa, bb, cc = map(float, data_file.readline().split())
          vect_x[ix,iy,iz] = aa
          vect_y[ix,iy,iz] = bb
          vect_z[ix,iy,iz] = cc
      if double_y :
        for iy in range(self.meta['ny']):
          for ix in range(self.meta['nx']):
            data_file.readline()

    data_file.close()

    if (fibo_obj != None) :
      fibo_obj.data[tar_var_x] = vect_x
      fibo_obj.data[tar_var_y] = vect_y
      fibo_obj.data[tar_var_z] = vect_z
    else: return np.array([vect_x, vect_y, vect_z])

    #if not silent: print('done with the reading!')

