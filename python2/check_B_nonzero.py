#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#Hawley colormap
bit_rgb = np.linspace(0,1,256)
colors = [(0,0,127), (0,3,255), (0,255,255), (128,128,128), (255,255,0),(255,0,0),(135,0,0)]
positions = [0.0,0.166667,0.333333,0.5,0.666667,0.833333,1.]
for iii in range(len(colors)):
    colors[iii] = (bit_rgb[colors[iii][0]],
                   bit_rgb[colors[iii][1]],
                   bit_rgb[colors[iii][2]])

cdict = {'red':[], 'green':[], 'blue':[]}
for pos, color in zip(positions, colors):
    cdict['red'].append((pos, color[0], color[0]))
    cdict['green'].append((pos, color[1], color[1]))
    cdict['blue'].append((pos, color[2], color[2]))

Hawley_cmap = mpl.colors.LinearSegmentedColormap('Hawley',cdict,256)

#------------------------------------------------------
#Intent
#------
print('\ncheck that B is not 0\n')

#------------------------------------------------------
#Functions
#---------
def plt_func(loc,hdnx,hdny,x_ax,y_ax,nss,qe,Bm,times,ind,lab,opath,out_dir,
             plt_show_flg,plt_save_flg,run_label,run_name):
    W1 = 3. #length first box
    H1 = 3. #height first box
    H2 = 1.5 #height second box

    w1 = 0.16 #width spacing grids

    l1 = 0.09 #left margin
    r1 = 0.96 #right margin
    b1 = 0.08 #bottom margin
    t1 = 0.92 #top margin

    m1 = 1.6 #multiplier

    for ix,iy in zip(loc[0],loc[1]):
        ix1m = ix + hdnx
        iy1m = iy + hdny
        ix2m = ix + 2*hdnx
        iy2m = iy + 2*hdny

        X,Y = np.meshgrid(x_ax[ix:ix2m+1],y_ax[iy:iy2m+1])

        plt.close()
        fig = plt.figure(figsize=(m1*nss*(W1+H2)*(1.+(nss-1)*w1*0.5)/(r1-l1),m1*(H1+H2)/(t1-b1)),
                         constrained_layout=False)
        gs = GridSpec(1,nss,figure=fig,wspace=w1,left=l1,right=r1,bottom=b1,top=t1)
        ss = []
        for iss in range(nss):
            if nss == 1:
                qe_ = qe
                title = 'qe = Qe:id/2'
            elif nss == 2:
                qe_ = qe[iss]
                if iss == 0:
                    title = 'qparle = Qe:.bbb'
                else:
                    title = 'qperpe = Qe:.(id - bb)b/2'
            else:
                qe_ = qe[iss]
                title = 'qe %d'%(iss)

            ss.append(SubSpec(2,4,gs[iss],width_ratios=[W1,H2/3.,H2/3.,H2/3.],height_ratios=[H2,H1],
                      wspace=0.,hspace=0.))
            
            ax0 = fig.add_subplot(ss[iss][0,0])
            ax0.plot(x_ax[ix:ix2m+1],qe_[ix:ix2m+1,iy1m],'--o')
            ax0.axhline(y=0.,ls='--',c='k')
            ax0.set_xlim(x_ax[ix],x_ax[ix2m])
            ax0.set_ylim(np.nanmin(qe_[ix:ix2m+1,iy1m]),np.nanmax(qe_[ix:ix2m+1,iy1m]))
            ax0.set_title(title)
            ax0.set_ylabel('H-cut')
            ax0.tick_params(bottom=True,top=True,left=True,right=True,
                            direction='inout',labelbottom=False,labeltop=False,
                            labelleft=True,labelright=False)

            ax1 = fig.add_subplot(ss[iss][1,0])
            vmax = max(abs(np.nanmin(qe_[ix:ix2m+1,iy:iy2m+1])),
                       abs(np.nanmax(qe_[ix:ix2m+1,iy:iy2m+1])))
            vmin = - vmax
            im1 = ax1.pcolormesh(X,Y,qe_[ix:ix2m+1,iy:iy2m+1].T,cmap=Hawley_cmap,vmin=vmin,vmax=vmax)
            ax1.axhline(y=y_ax[iy1m],ls='--',c='k')
            ax1.axvline(x=x_ax[ix1m],ls='--',c='k')
            ax1.set_xlim(x_ax[ix],x_ax[ix2m])
            ax1.set_ylim(y_ax[iy],y_ax[iy2m])
            ax1.set_xlabel('x [$d_i$]')
            ax1.set_ylabel('y [$d_i$]')
            ax1.tick_params(bottom=True,top=True,left=True,right=True,
                            direction='inout',labelbottom=True,labeltop=False,
                            labelleft=True,labelright=False)
            xticks = ax1.xaxis.get_major_ticks()
            xticks[-1].label1.set_visible(False)
            yticks = ax1.yaxis.get_major_ticks()
            yticks[-1].label1.set_visible(False)

            ax2 = fig.add_subplot(ss[iss][1,1:4])
            ax2.plot(qe_[ix1m,iy:iy2m+1],y_ax[iy:iy2m+1],'--o')
            ax2.axvline(x=0.,ls='--',c='k')
            ax2.set_xlim(np.nanmin(qe_[ix1m,iy:iy2m+1]),np.nanmax(qe_[ix1m,iy:iy2m+1]))
            ax2.set_ylim(y_ax[iy],y_ax[iy2m])
            ax2.set_xlabel('V-cut')
            ax2.tick_params(bottom=True,top=True,left=True,right=True,
                            direction='inout',labelbottom=True,labeltop=False,
                            labelleft=False,labelright=False)

            ax3 = fig.add_subplot(ss[iss][0,2])
            cb3 = plt.colorbar(im1,cax=ax3)

        plt.suptitle(run_label+' - $t=%.1f$ - $|B|=%.2e$ at $(%.2f,%.2f)$'%(times[ind],
                                                                            Bm[ix,iy],
                                                                            x_ax[ix1m],
                                                                            y_ax[iy1m]))

        if plt_show_flg:
            plt.show()

        if plt_save_flg:
            fig_name = 'check_B_nonzero__'+run_name+'__it%d__qe_at_%s_%d_%d.png'%(ind,lab,ix,iy)
            fig.savefig(opath+'/'+out_dir+'/'+fig_name)

        plt.close()
    return

#------------------------------------------------------
#Init
#----
plt_show_flg = False
plt_save_flg = True

vo_plt_flg = False
loc_plt_flg = True

Bm_th = 1.00e-2

Brisk = {'nan':[],'m_1':[],'m_2':[],'m_3':[]}
t = []

run,tr,_=wl.Init(sys.argv[1])
ind1,ind2,ind_step = tr
run_name = run.meta['run_name']
w_ele = run.meta['w_ele']
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
times = run.meta['time']*run.meta['tmult']
code_name = run.meta['code_name']
#
if code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
elif code_name == 'iPIC':
    run_label = 'iPIC'
    qom_e = run.meta['msQOM'][0]
else:
    print('ERROR: unknown code_name %s'%(code_name))
    sys.exit(-1)
#
opath = run.meta['opath']

if plt_save_flg:
    out_dir = 'check_B_nonzero'
    ml.create_path(opath+'/'+out_dir)
#
if (code_name == 'HVM') and w_ele:
    nss = 2
else:
    nss = 1

if loc_plt_flg:
    dnx = 30
    hdnx = dnx//2
    dnx = hdnx*2 #avoid problem w/ odd numbers
    x_ax = np.empty(nx+dnx,dtype=np.float64)
    x_ax[hdnx:hdnx+nx] = x
    x_ax[0:hdnx] = (np.arange(hdnx,dtype=np.float) - float(hdnx))*dx
    x_ax[hdnx+nx:dnx+nx] = (np.arange(hdnx,dtype=np.float) + float(nx))*dx

    dny = 30
    hdny = dny//2
    dny = hdny*2 #avoid problem w/ odd numbers
    y_ax = np.empty(nx+dnx,dtype=np.float64)
    y_ax[hdny:hdny+ny] = y
    y_ax[0:hdny] = (np.arange(hdny,dtype=np.float) - float(hdny))*dy
    y_ax[hdny+ny:dny+ny] = (np.arange(hdny,dtype=np.float) + float(ny))*dy
# 
#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #|B|
    _,B = run.get_EB(ind)
    Bm = np.sqrt( (B[0,...,0]*B[0,...,0] + 
                   B[1,...,0]*B[1,...,0] + 
                   B[2,...,0]*B[2,...,0]) )
    del B
#
    if loc_plt_flg:
        #qparle,qperpe
        if w_ele:
            if code_name == 'HVM':
                qparle,qperpe = run.get_Qe(ind) # qparle = Qe:.bbb ; qperpe = Qe:.(id - bb)b/2
                qe = [qparle[...,0],qperpe[...,0]]
            elif code_name == 'iPIC':
                qe = run.get_Q(ind,qom=qom_e) # qe = Qe:id/2
                qe = np.sqrt( (qe[0,...,0]*qe[0,...,0] +
                               qe[1,...,0]*qe[1,...,0] +
                               qe[2,...,0]*qe[2,...,0]) )
        else:
            qe = np.zeros((nx,ny),dtype=np.float64) # qe = 0
#
    #--------------------------------------------------------
    #saving data
    #-------------------------------------
    if vo_plt_flg:
        m1 = np.nanmin(Bm)
        m2 = np.nanmin(Bm[Bm > m1])
        m3 = np.nanmin(Bm[Bm > m2])
#
        Brisk['nan'].append([ 0., np.count_nonzero(np.isnan(Bm))])
        Brisk['m_1'].append([ m1, np.count_nonzero(Bm == m1)])
        Brisk['m_2'].append([ m2, np.count_nonzero(Bm == m2)])
        Brisk['m_3'].append([ m3, np.count_nonzero(Bm == m3)])
        t.append(times[ind])
#
    if loc_plt_flg:
        if nss == 1:
            qe = np.pad(qe,((hdnx,hdnx),(hdny,hdny)),'wrap')
        else:
            qe[0] = np.pad(qe[0],((hdnx,hdnx),(hdny,hdny)),'wrap')
            qe[1] = np.pad(qe[1],((hdnx,hdnx),(hdny,hdny)),'wrap')

        loc = np.where(np.isnan(Bm)==True)
        plt_func(loc,hdnx,hdny,x_ax,y_ax,nss,qe,Bm,times,ind,'NAN',opath,out_dir,
                 plt_show_flg,plt_save_flg,run_label,run_name)

        loc = np.where(Bm < Bm_th)
        plt_func(loc,hdnx,hdny,x_ax,y_ax,nss,qe,Bm,times,ind,'th%.2e'%(Bm_th),opath,out_dir,
                 plt_show_flg,plt_save_flg,run_label,run_name)
#
    del Bm
#
#---> loop over time <---
    print "\r",
    print "t = ",times[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

if vo_plt_flg:
    Brisk['nan'] = np.array(Brisk['nan'])
    Brisk['m_1'] = np.array(Brisk['m_1'])
    Brisk['m_2'] = np.array(Brisk['m_2'])
    Brisk['m_3'] = np.array(Brisk['m_3'])
    t = np.array(t)

    #----------------
    #PLOT TIME!!!!!!!
    #----------------
    plt.close('all')
    fig,ax = plt.subplots(1,1,figsize=(9,9))

    im = ax.scatter(Brisk['nan'][:,0],Brisk['nan'][:,1],c=t,marker='o',cmap='tab20',label='nan')
    ax.scatter(Brisk['m_1'][:,0],Brisk['m_1'][:,1],c=t,marker='*',cmap='tab20',label='m 1')
    ax.scatter(Brisk['m_2'][:,0],Brisk['m_2'][:,1],c=t,marker='^',cmap='tab20',label='m 2')
    ax.scatter(Brisk['m_3'][:,0],Brisk['m_3'][:,1],c=t,marker='s',cmap='tab20',label='m 3')

    plt.colorbar(im,ax=ax)
    ax.legend()

    ax.set_xlabel('value')
    ax.set_ylabel('occurency')
    ax.set_title('NANs and lowest three values in '+run_label)

    if plt_show_flg:
        plt.show()

    if plt_save_flg:
        fig_name = 'check_B_nonzero__'+run_name+'.png'
        fig.savefig(opath+'/'+out_dir+'/'+fig_name)

    plt.close()
