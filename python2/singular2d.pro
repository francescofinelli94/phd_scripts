;-----------------------------------------------------------------------------------------------------
; SINGULAR2D.PRO - 24 September 2019 - F. Finelli - francesco.finelli@phd.unipi.it
;-----------------------------------------------------------------------------------------------------
; DESCRIPTION
;   This function find local minima, local maxima and saddle points
;   It make use of the watershed function in order to find ridges and valleys
;   Saddles are intersection between a ridge and a valley
;   Minima are local minima of the valleys
;   Maxima are local maxmima of the ridges
;-----------------------------------------------------------------------------------------------------
; INPUTS
;   field       -> a 2d array of which you want singular points
;   range       -> [xmin,xmax,ymin,ymax], the range of interest
;   /xperiodic  -> [optional] if set, field is considered x-periodic
;   /yperiodic  -> [optional] if set, field is considered y-periodic
;   gs_sigma    -> [optional] [sigma_x,sigma_y] or sigma, the scale(s) for the
;                  gaussian smoothing; if not set, no gaussian smoothing applied
;   dc          -> [optional] 2 components array [dcx,dcy]: maximum x- and y- distance
;                  checked in order to determine if a point is a maximum/minimum/saddle;
;                  if not set, dcx = dcy = 1
;   /indices_gl -> if set, singular points spatial indices are referred to the global box,
;                  e.g. [0:nx-1,0:ny-1]; if note set, they are reffered to the local box,
;                  i.e. [xmin:xmax,ymin:ymax]
;   /clean      -> if not set, first maximum/minimum/saddel point is a fake point at [-700,-700];
;                  see OUTPUTS
;   field_out   -> [optional] if set, smoothed (and scaled in [-1.0,1.0]) field
;                  will be outputed there
;   ridges_out  -> [optional] if set, an array with value 1 on ridges points and 0 eveywhere else
;   valleys_out -> [optional] if set, an array with value 1 on valleys ipoints and 0 eveywhere else
;-----------------------------------------------------------------------------------------------------
; OUTPUTS
;   SngPnt      -> structure containing 3 vectors: .min (minima), .max (maxima), .sad (saddles);
;                  each elemets of these vectors contains: .ix (x position index),
;                  .iy (y position index), .val (field value in the point); each vector has as
;                  first point a fake entry located in [-777,-777], unless the /clean keyword is set;
;                  to access something: e.g., SngPnt.min[2].iy
;   field_out   -> [optional] see INPUTS
;   ridges_out  -> [optional] see INPUTS
;   valleys_out -> [optional] see INPUTS
;-----------------------------------------------------------------------------------------------------
; COMPILE
;   resolve_routine,'singular2d',/is_function
;-----------------------------------------------------------------------------------------------------
; NOTES
;   Periodic border conditions are achieved via padding
;-----------------------------------------------------------------------------------------------------
function singular2d,field,range,xperiodic=xperiodic,yperiodic=yperiodic,$
                    gs_sigma=gs_sigma,dc=dc,indices_gl=indices_gl,clean=clean,$
                    field_out=field_out,ridges_out=ridges_out,valleys_out=valleys_out
  ;structures definitions and allocation
  tmp_str = {point_str, ix:0L, iy:0L, val:0.0d0}
  maxima = replicate({point_str},1)
  minima = replicate({point_str},1)
  saddles = replicate({point_str},1)
  maxima[0].ix = -777L
  maxima[0].iy = -777L
  minima[0].ix = -777L
  minima[0].iy = -777L
  saddles[0].ix = -777L
  saddles[0].iy = -777L

  ;range definition
  xmin = range(0)
  xmax = range(1)
  ymin = range(2)
  ymax = range(3)
  lnx = xmax - xmin + 1
  lny = ymax - ymin + 1
  field = field(xmin:xmax,ymin:ymax)
  if keyword_set(indices_gl) then mult = 1L else mult = 0L

  ;field padding
  if not keyword_set(gs_sigma) then gs_sigma = [0,0]
  if not keyword_set(dc) then dc = [1,1]
  dnx = max([dc(0),gs_sigma(0),6])
  dny = max([dc(0),gs_sigma(1),6])
  field_tmp = dblarr(lnx+2*dnx,lny+2*dny)
  field_tmp(dnx:lnx+dnx-1,dny:lny+dny-1) = field
  if keyword_set(xperiodic) then begin
    field_tmp(0:dnx-1,dny:lny+dny-1) = field(lnx-dnx:lnx-1,*)
    field_tmp(lnx+dnx:lnx+2*dnx-1,dny:lny+dny-1) = field(0:dnx-1,*)
  endif else begin
    for i = 0, dnx-1 do field_tmp(i,dny:lny+dny-1) = field(0,*)
    for i = lnx+dnx, lnx+2*dnx-1 do field_tmp(i,dny:lny+dny-1) = field(lnx-1,*)
  endelse
  if keyword_set(yperiodic) then begin
    field_tmp(dnx:lnx+dnx-1,0:dny-1) = field(*,lny-dny:lny-1)
    field_tmp(dnx:lnx+dnx-1,lny+dny:lny+2*dny-1) = field(*,0:dny-1)
  endif else begin
    for j = 0, dny-1 do field_tmp(dnx:lnx+dnx-1,j) = field(*,0)
    for j = lny+dny, lny+2*dny-1 do field_tmp(dnx:lnx+dnx-1,j) = field(*,lny-1)
  endelse
  if keyword_set(xperiodic) and keyword_set(yperiodic) then begin
    field_tmp(0:dnx-1,0:dny-1) = field(lnx-dnx:lnx-1,lny-dny:lny-1)
    field_tmp(0:dnx-1,lny+dny:lny+2*dny-1) = field(lnx-dnx:lnx-1,0:dny-1)
    field_tmp(lnx+dnx:lnx+2*dnx-1,0:dny-1) = field(0:dnx-1,lny-dny:lny-1)
    field_tmp(lnx+dnx:lnx+2*dnx-1,lny+dny:lny+2*dny-1) = field(0:dnx-1,0:dny-1)
  endif else begin
    for i = 0, dnx-1 do for j = 0, dny-1 do field_tmp(i,j) = field(0,0)
    for i = 0, dnx-1 do for j = lny+dny, lny+2*dny-1 do field_tmp(i,j) = field(0,lny-1)
    for i = lnx+dnx, lnx+2*dnx-1 do for j = 0, dny-1 do field_tmp(i,j) = field(lnx-1,0)
    for i = lnx+dnx, lnx+2*dnx-1 do for j = lny+dny, lny+2*dny-1 do field_tmp(i,j) = field(lnx-1,lny-1)
  endelse

  ;field smoothing and scaling
  field_gs = gauss_smooth(field_tmp,gs_sigma,/edge_truncate)
  field_gs = (field_gs - min(field_gs))/(max(field_gs)-min(field_gs))*2.0-1.0  ; standardize: (field -mean(field))/sigma(field) migliora?
  field_tmp = 0

  ;finding ridges and valleys
  wsp = watershed(bytscl(field_gs),connectivity=8,/long)
  wsm = watershed(bytscl(-field_gs),connectivity=8,/long)
  wsp = (wsp eq 0) ;wsp = 0 on ridges, non 0 everywhere else
  wsm = (wsm eq 0) ;wsm = 0 on valleys, non 0 everywhere else

  ;finding saddles
  tmp_str2 = {point_str2, inherits point_str, lab:0L}
  lab_vec = []
  saddles_tmp = replicate({point_str2},1)
  cnt = 0L
  for j = 1, lny+2*dny-2 do begin
    for i = 1, lnx+2*dnx-2 do begin
      if (wsp(i,j)*wsm(i,j)) eq 1 then begin
        tmp_str2.ix = i - dnx
        tmp_str2.iy = j - dny
        tmp_str2.lab = cnt
        saddles_tmp = [saddles_tmp,tmp_str2]
        lab_vec = [lab_vec,cnt]
        cnt += 1L
      endif
    endfor
  endfor

  ;clustering too close saddles
  for i = 1, n_elements(saddles_tmp)-1 do begin
    for j = 1, n_elements(saddles_tmp)-1 do begin
      if i eq j then continue
      if (abs(saddles_tmp(i).ix - saddles_tmp(j).ix) gt dc(0)) or $
         (abs(saddles_tmp(i).iy - saddles_tmp(j).iy) gt dc(1)) then continue
      if saddles_tmp(i).lab eq saddles_tmp(j).lab then continue
      lab_min = min([saddles_tmp(i).lab,saddles_tmp(j).lab])
      lab_max = max([saddles_tmp(i).lab,saddles_tmp(j).lab])
      for k = 1, n_elements(saddles_tmp)-1 do if saddles_tmp(k).lab eq lab_max then $
                                                 saddles_tmp(k).lab = lab_min
      lab_vec(lab_max) = -1L
    endfor
  endfor

  ;collapsing saddles cluster in one point
  for i = 0, cnt-1 do begin
    if lab_vec(i) eq -1L then continue
    cnt2 = 0L
    ix = 0L
    iy = 0L
    for j = 1, n_elements(saddles_tmp)-1 do begin
      if saddles_tmp(j).lab ne lab_vec(i) then continue
      ix += saddles_tmp(j).ix
      iy += saddles_tmp(j).iy
      cnt2 += 1L
    endfor
    tmp_ix = ix/cnt2
    tmp_iy = iy/cnt2
    if (tmp_ix ge 0) and (tmp_ix lt lnx) and $
       (tmp_iy ge 0) and (tmp_iy lt lny) then begin
      tmp_str.ix  = tmp_ix + mult*xmin
      tmp_str.iy  = tmp_iy + mult*ymin
      tmp_str.val = field(tmp_ix,tmp_iy)
      saddles = [saddles,tmp_str]
    endif
  endfor

  ;finding maxima and minima
  min_tmp = wsm*(-field_gs-min(-field_gs))
  max_tmp = wsp*(field_gs-min(field_gs))
  for j = dny, lny+dny-1 do begin
    for i = dnx, lnx+dnx-1 do begin
      if min_tmp(i,j) ne 0.0 then begin
        tmp1 = max(min_tmp(i-dc(0):i+dc(0),j-dc(1):j+dc(1)),tmp2)
        if tmp2 eq dc(1)*(2*dc(0)+1)+dc(0) then begin
          tmp_str.ix  = i - dnx + mult*xmin
          tmp_str.iy  = j - dny + mult*ymin
          tmp_str.val = field(i-dnx,j-dny)
          minima = [minima,tmp_str]
        endif
      endif
      if max_tmp(i,j) ne 0.0 then begin
        tmp1 = max(max_tmp(i-dc(0):i+dc(0),j-dc(1):j+dc(1)),tmp2)
        if tmp2 eq dc(1)*(2*dc(0)+1)+dc(0) then begin
          tmp_str.ix  = i - dnx + mult*xmin
          tmp_str.iy  = j - dny + mult*ymin
          tmp_str.val = field(i-dnx,j-dny)
          maxima = [maxima,tmp_str]
        endif
      endif
    endfor
  endfor
  min_tmp = 0
  max_tmp = 0
  tmp_str = 0

  ;outputs
  field_out = field_gs(dnx:lnx+dnx-1,dny:lny+dny-1)
  ridges_out = wsp(dnx:lnx+dnx-1,dny:lny+dny-1)
  valleys_out = wsm(dnx:lnx+dnx-1,dny:lny+dny-1)
  if keyword_set(clean) then begin
    npnt = n_elements(minima)
    if npnt gt 1 then minima = minima[1:npnt-1] else minima = list() ;in structures, a variable value
    npnt = n_elements(maxima)                                        ;cannot be set to !NULL (e.g., [])
    if npnt gt 1 then maxima = maxima[1:npnt-1] else maxima = list() ;but a void list has still a length
    npnt = n_elements(saddles)                                       ; equal to 0
    if npnt gt 1 then saddles = saddles[1:npnt-1] else saddles = list()
  endif
  SngPnt = {min:minima,max:maxima,sad:saddles}
  minima = 0
  maxima = 0
  saddles = 0
  return,SngPnt
end
