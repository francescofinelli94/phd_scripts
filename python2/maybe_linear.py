#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
from scipy import stats
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nIs there a linear phase?\n')

#------------------------------------------------------
#Init
#----
#input_files = ['LF_2d_DH.dat','DH_run2_data3.dat','DH_run3_data0.dat',
#               'DH_run5_data0.dat','DH_run7_data0.dat','DH_run8.dat',
#               'DH_run9.dat','DH_run10.dat','DH_run11_data0.dat']
input_files = ['LF_2d_DH.dat','DH_run5_data0.dat','DH_run7_data0.dat','DH_run8.dat','DH_run11_data0.dat','DH_run12_data0.dat','DH_run13_data0.dat']

ix0 = 150
master_in = 0
nmod = 9

rmsBy = {}
Eparl_x0 = {}
Eperp_x0 = {}
Psi_x0 = {}
y_cut = {}
fftBy = {}
t = {}

run_name_vec = []
B0 = {}
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind1,ind2,ind_step = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    nx = run.meta['nnn'][0]
    times = run.meta['time']*run.meta['tmult']
    code_name = run.meta['code_name']
    if code_name == 'HVM':
        B0[run_name] = 1.
    elif code_name == 'iPIC':
        B0[run_name] = 0.01
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
    if ii == master_in:
        opath = run.meta['opath']
        out_dir = 'maybe_linear'
        ml.create_path(opath+'/'+out_dir)
        path = opath+'/'+out_dir
        res = glob.glob(path+'/comp*')
        cnt = 0
        for x in res:
            if os.path.isdir(x):
                cnt += 1
        out_dir = out_dir + '/comp%d'%(cnt)
        ml.create_path(opath+'/'+out_dir)
#
    rmsBy[run_name] = []
    Eparl_x0[run_name] = []
    Eperp_x0[run_name] = []
    Psi_x0[run_name] = []
    y_cut[run_name] = []
    fftBy[run_name] = []
    t[run_name] = []
#
    #---> loop over times <---
    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        #get data
        #magnetic fields and co.
        E,B = run.get_EB(ind)
#
        Psi = wl.psi_2d(B[:-1,...,0],run.meta)
        By = B[1,:,:,0]
        del B
#
        Eparl_x0[run_name].append( np.abs(E[2,ix0,iymin:iymax,0]) )
        Eperp_x0[run_name].append( 
                np.sqrt(E[0,ix0,iymin:iymax,0]**2+E[1,ix0,iymin:iymax,0]**2) )
        del E
        Psi_x0[run_name].append( Psi[ix0,iymin:iymax]/B0[run_name] )
        del Psi
        if y_cut[run_name] == []:
            y_cut[run_name] = run.meta['y'][iymin:iymax]
#
        rmsBy[run_name].append( np.sqrt(np.mean(By[ixmin:ixmax,iymin:iymax]**2))/B0[run_name] )
#
        fftBy_ = np.zeros((nx/2+1),dtype=np.float64)
        for iy in range(iymin,iymax+1):
            fftBy_ += np.abs(np.fft.rfft(By[:,iy]-np.mean(By[:,iy])))
        fftBy_[1:] *= 2.
        fftBy_ /= float(nx)
        fftBy_ /= np.float(iymax+1-iymin)
        fftBy[run_name].append( fftBy_[1:nmod+1]/B0[run_name] )
        del fftBy_
#
        t[run_name].append( times[ind] )
    #---> loop over time <---
        print "\r",
        print run_name," t = ",times[ind],
        gc.collect()
        sys.stdout.flush()
    #------------------------
#

#----------------
#PLOT TIME!!!!!!!
#----------------
mycol = ['red','chartreuse','aquamarine','darkgrey','magenta','chocolate','saddlebrown','teal','navy']
ncol = len(mycol)

#define linear phase according to rmsBy
ind1 = {}
ind2 = {}
for i,run_name in enumerate(run_name_vec):
    t[run_name] = np.array(t[run_name])
#
    plt.close()
    plt.xlim(t[run_name][0],t[run_name][-1])
    plt.ylim(np.min(rmsBy[run_name]),np.max(rmsBy[run_name]))
    plt.yscale('log')
    plt.plot(t[run_name],rmsBy[run_name],label=run_name.replace('_',' '),color=mycol[i%ncol])
    plt.title('$rms(B_y)$ vs. time for %s'%(run_name.replace('_',' ')))
    plt.xlabel('t [$\Omega_c^{-1}$]')
    plt.ylabel('$rms(B_y)/B_0$')
    plt.legend(loc='upper left')
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.show()
#
    flag = 0
    while flag==0:
        flag = 1
        t1 = input("Start of linear phase for run %s: "%(run_name))
        try:
            ind1_tmp = np.argmin(np.abs(t[run_name]-t1))
        except:
            print 'Please, enter a number: '
            flag = 0
    ind1[run_name] = ind1_tmp
#
    flag = 0
    while flag==0:
        flag = 1
        t2 = input("End of linear phase for run %s: "%(run_name))
        try:
            ind2_tmp = np.argmin(np.abs(t[run_name]-t2))
        except:
            print 'Please, enter a number: '
            flag = 0
    ind2[run_name] = ind2_tmp

#plot all rmsBy
plt.close()
plt.xlim(ml.min_dict(t),ml.max_dict(t))
plt.ylim(ml.min_dict(rmsBy),ml.max_dict(rmsBy))
plt.yscale('log')
for i,run_name in enumerate(run_name_vec):
    plt.plot(t[run_name],rmsBy[run_name],label=run_name.replace('_',' '),color=mycol[i%ncol])
    plt.plot(t[run_name][ind1[run_name]:ind2[run_name]+1],
             rmsBy[run_name][ind1[run_name]:ind2[run_name]+1],color=mycol[i%ncol],linewidth=3.0)

plt.title('$rms(B_y)$ vs. time')
plt.xlabel('t [$\Omega_c^{-1}$]')
plt.ylabel('$rms(B_y)/B_0$')
plt.legend(loc='upper left')
fig = plt.gcf()
mng = plt.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
plt.show()
fig.savefig(opath+'/'+out_dir+'/'+'rmsBy_allruns.png')

#define linear phase from FFT
ind1b = {}
ind2b = {}
for run_name in run_name_vec:
    fftBy[run_name] = np.array(fftBy[run_name])
#
    plt.close()
    plt.xlim(t[run_name][0],t[run_name][-1])
    plt.ylim(np.min(fftBy[run_name]),np.max(fftBy[run_name]))
    plt.yscale('log')
    for i in range(nmod):
        plt.plot(t[run_name],fftBy[run_name][:,i],label='m=%d'%(i+1),color=mycol[i%ncol])
    plt.title('Evolution of the firsti %d $B_y$ modes for %s'%(nmod,run_name.replace('_',' ')))
    plt.xlabel('t [$\Omega_c^{-1}$]')
    plt.ylabel('$B_y$ modes amplitude [a.u.]')
    plt.legend(loc='upper left')
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.show()
#
    flag = 0
    while flag==0:
        flag = 1
        t1 = input("Start of linear phase for run %s: "%(run_name.replace('_',' ')))
        try:
            ind1_tmp = np.argmin(np.abs(t[run_name]-t1))
        except:
            print 'Please, enter a number: '
            flag = 0
    ind1b[run_name] = ind1_tmp
#
    flag = 0
    while flag==0:
        flag = 1
        t2 = input("End of linear phase for run %s: "%(run_name))
        try:
            ind2_tmp = np.argmin(np.abs(t[run_name]-t2))
        except:
            print 'Please, enter a number: '
            flag = 0
    ind2b[run_name] = ind2_tmp

#plot all fftBy
for m in range(nmod):
    plt.close()
    plt.xlim(ml.min_dict(t),ml.max_dict(t))
    plt.yscale('log')
    for i,run_name in enumerate(run_name_vec):
        plt.plot(t[run_name],fftBy[run_name][:,m],
                 label='%s m=%d'%(run_name.replace('_',' '),m+1),color=mycol[i%ncol])
        plt.plot(t[run_name][ind1b[run_name]:ind2b[run_name]+1],
                 fftBy[run_name][ind1b[run_name]:ind2b[run_name]+1,m],color=mycol[i%ncol],linewidth=3.0)
    plt.title('Evolution of the $m=%d$ $B_y$ mode'%(m+1))
    plt.xlabel('t [$\Omega_c^{-1}$]')
    plt.ylabel('$B_y$ $m=%d$ mode amplitude [a.u]'%(m+1))
    plt.legend(loc='upper left')
    fig = plt.gcf()
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.show()
    fig.savefig(opath+'/'+out_dir+'/'+'fftBy_m%d_allruns.png'%(m+1))

#choose linear phase and compute growth rates
ind1_ = {}
ind2_ = {}
slope_vec = {}
for run_name in run_name_vec:
    plt.close()
    fig,ax = plt.subplots(2,1)
#
    ax[0].set_xlim(t[run_name][0],t[run_name][-1])
    ax[0].set_ylim(np.min(rmsBy[run_name]),np.max(rmsBy[run_name]))
    ax[0].set_yscale('log')
    ax[0].plot(t[run_name],rmsBy[run_name])
    ax[0].axvline(x=t[run_name][ind1[run_name]],c='red',ls=':')
    ax[0].axvline(x=t[run_name][ind2[run_name]],c='red',ls='--')
    ax[0].set_title('$rms(B_y)$ vs. time for %s'%(run_name.replace('_',' ')))
    ax[0].set_xlabel('t [$\Omega_c^{-1}$]')
    ax[0].set_ylabel('$rms(B_y)/B_0$')
#
    ax[1].set_xlim(t[run_name][0],t[run_name][-1])
    ax[1].set_ylim(np.min(fftBy[run_name]),np.max(fftBy[run_name]))
    ax[1].set_yscale('log')
    for i in range(nmod):
        ax[1].plot(t[run_name],fftBy[run_name][:,i],label='m=%d'%(i+1),color=mycol[i%ncol])
    ax[1].axvline(x=t[run_name][ind1b[run_name]],c='red',ls=':')
    ax[1].axvline(x=t[run_name][ind2b[run_name]],c='red',ls='--')
    ax[1].set_title('Evolution of the firsti %d $B_y$ modes for %s'%(nmod,run_name.replace('_',' ')))
    ax[1].set_xlabel('t [$\Omega_c^{-1}$]')
    ax[1].set_ylabel('$B_y$ modes amplitude [a.u.]')
    ax[1].legend(loc='upper left')
#
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.show()
    fig.savefig(opath+'/'+out_dir+'/'+'rmsBy_fftBy_%dmodes_%s.png'%(nmod,run_name))
#
    flag = 0
    while flag==0:
        flag = 1
        t1 = input("Start of linear phase: ")
        try:
            ind1_tmp = np.argmin(np.abs(t[run_name]-t1))
        except:
            print 'Please, enter a number: '
            flag = 0
    ind1_[run_name] = ind1_tmp
#
    flag = 0
    while flag==0:
        flag = 1
        t2 = input("End of linear phase for run %s: "%(run_name))
        try:
            ind2_tmp = np.argmin(np.abs(t[run_name]-t2))
        except:
            print 'Please, enter a number: '
            flag = 0
    ind2_[run_name] = ind2_tmp
#
    slope_vec[run_name] = []
    xx = t[run_name][ind1_[run_name]:ind2_[run_name]+1]
    for i in range(nmod):
        yy = np.log(fftBy[run_name][ind1_[run_name]:ind2_[run_name]+1,i])
        slope, intercept, r_value, p_value, std_err = stats.linregress(xx,yy)
        slope_vec[run_name].append(slope)

#plot growth rate vs. m
plt.close()
plt.xlim(1,nmod)
plt.ylim(ml.min_dict(slope_vec),ml.max_dict(slope_vec))
for i,run_name in enumerate(run_name_vec):
    plt.plot(np.arange(1,nmod+1),slope_vec[run_name],'-o',
             label='%s'%(run_name.replace('_',' ')),color=mycol[i%ncol])

plt.title('Growth rates vs. modes')
plt.xlabel('m')
plt.ylabel('$\gamma_m$')
plt.legend(loc='upper left')
fig = plt.gcf()
mng = plt.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
plt.show()
fig.savefig(opath+'/'+out_dir+'/'+'growth_rates_allruns.png')

#cuts
t_vec = []
nt = input('Insert the number of times for when the cuts must be done: ')
for i in range(nt):
    tmp = input('time step number %d: '%(i))
    t_vec.append(tmp)

y0 = ml.min_dict(y_cut)
y1 = ml.max_dict(y_cut)
for t_ in t_vec:
    plt.close()
    fig,ax = plt.subplots(3,1)
#
    for run_name in run_name_vec:
        it_ = np.argmin(np.abs(t[run_name]-t_))
        if abs(t[run_name][it_]-t_) > 2.*(t[run_name][1]-t[run_name][0]):
            continue
        ax[0].plot(y_cut[run_name],Eparl_x0[run_name][it_]/np.max(Eparl_x0[run_name][it_]),
                   label=run_name.replace('_',' '))
        ax[1].plot(y_cut[run_name],Eperp_x0[run_name][it_]/np.max(Eperp_x0[run_name][it_]),
                   label=run_name.replace('_',' '))
        ax[2].plot(y_cut[run_name],Psi_x0[run_name][it_],label=run_name.replace('_',' '))
#
    ax[0].set_xlim(y0,y1)
    ax[0].set_title('Eparl at ix=%d t=%f'%(ix0,t_))
    ax[0].set_xlabel('y [$d_i$]')
    ax[0].set_ylabel('Eparl [a.u.]')
    ax[0].legend(loc='upper left')
#
    ax[1].set_xlim(y0,y1)
    ax[1].set_title('Eperp at ix=%d t=%f'%(ix0,t_))
    ax[1].set_xlabel('y [$d_i$]')
    ax[1].set_ylabel('Eperp [a.u.]')
    ax[1].legend(loc='upper left')
#
    ax[2].set_xlim(y0,y1)
    ax[2].set_title('Psi at ix=%d t=%f'%(ix0,t_))
    ax[2].set_xlabel('y [$d_i$]')
    ax[2].set_ylabel('Psi [a.u.]')
    ax[2].legend(loc='upper left')
#
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.show()
    fig.savefig(opath+'/'+out_dir+'/'+'Eparl_Eperp_Psi_cuts_%f.png'%(t_))

#write on file
f = open(opath+'/'+out_dir+'/'+'intervals.dat','w')
f.write('run_name\tinterval\tistart\tiend\ttstart\ttend\n')
for run_name in run_name_vec:
    f.write('%s\t%s\t%d\t%d\t%f\t%f\n'%(run_name,'rmsBy',ind1[run_name],ind2[run_name],
                               t[run_name][ind1[run_name]],t[run_name][ind2[run_name]]))
    f.write('%s\t%s\t%d\t%d\t%f\t%f\n'%(run_name,'fftBy',ind1b[run_name],ind2b[run_name],
                               t[run_name][ind1b[run_name]],t[run_name][ind2b[run_name]]))
    f.write('%s\t%s\t%d\t%d\t%f\t%f\n'%(run_name,'final',ind1_[run_name],ind2_[run_name],
                               t[run_name][ind1_[run_name]],t[run_name][ind2_[run_name]]))

f.close()
