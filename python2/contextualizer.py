#-------------------------------------------------------
#IMPORTS
#-------
import sys
import os
import gc
import glob
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.ndimage import gaussian_filter as gf
import pandas as pd
import seaborn as sns
from sklearn import preprocessing

import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from HVM_loader import *
from iPIC_loader import *

#-------------------------------------------------------
#SETTINGS
#--------
#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#outdirmode = fill, seq
outdirmode = 'seq'

#smoothing = True, False
smooth_flag = True
gfsp = 2 #0, 1, ...
gfse = 2 #0, 1, ...

#logscale = True, False
logscale_flg = True

#scaling = 'norm', 'std', None
preprocessing_opt = 'norm'#'std'#None

#-------------------------------------------------------
#INTENT
#------
print('\nThis script will try to separate in a 4D variable space\n'+
      'the different physical regions forming during magnetic reconnection.\n')

#-------------------------------------------------------
#INITIALIZATION
#--------------
run,_,xr=wl.Init(sys.argv[1])

ixmin,ixmax,iymin,iymax = xr
run_name = run.meta['run_name']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
times = run.meta['time']*run.meta['tmult']
nx,ny,_ = run.meta['nnn']
m_p = 1.
m_e = m_p/run.meta['mime']

if code_name == 'iPIC':
    run_label = 'iPIC%s'%(run_name.split('run')[1].split('_')[0])
    qom_e = run.meta['msQOM'][0]
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

opath = run.meta['opath']
out_dir = 'contestualization'
ml.create_path(opath+'/'+out_dir)
path = opath+'/'+out_dir
res = glob.glob(path+'/try*')
if outdirmode == 'fill':
    flg = True
    cnt = 0
    while flg:
        if path+'/try%d'%(cnt) not in res:
            out_dir = out_dir + '/try%d'%(cnt)
            flg = False
        cnt += 1
elif outdirmode == 'seq':
    tries = []
    for r in res:
        if os.path.isdir(r):
            try:
                try_num = int(r.split('try')[-1])
            except:
                continue
            tries.append(try_num)
    if len(tries) == 0:
        out_dir = out_dir + '/try%d'%(0)
    else:
        out_dir = out_dir + '/try%d'%(np.max(tries)+1)
else:
    print('ERROR: unknown outdirmode!')
    sys.exit(-1)

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

#-------------------------------------------------------
#LOAD DATA AND COMPUTE
#---------------------
#time
print('\nTimes:')
print(times)
t = ml.secure_input('\nChoose a time: ',1.,True)
ind = np.argmin(np.abs(times-t))
print('Selected time:\nt=%f\nind=%d\ntimes[ind]=%f'%(t,ind,times[ind]))

#and relative dimension in space
x = run.meta['x']
y = run.meta['y']

#fields and currents
E,B = run.get_EB(ind)

Psi = wl.psi_2d(B[:-1,...,0],run.meta)
if smooth_flag:
    Psi = gf(Psi,[gfsp,gfsp],mode='wrap')

n_p,u_p = run.get_Ion(ind)
J = np.empty(u_p.shape,dtype=type(u_p[0,0,0,0]))
if code_name == 'HVM':
    calc.meta=run.meta
    J[0],J[1],J[2] = calc.calc_curl(B[0],B[1],B[2])
    #n_e = n_p.copy() # n_e = n_p = n (no charge separation)
    u_e = np.empty(u_p.shape,dtype=type(u_p[0,0,0,0])) # u_e = u_p - J/n
    for i in range(3):
        u_e[i] = u_p[i] - np.divide(J[i],n_p)
elif code_name == 'iPIC':
    n_e,u_e = run.get_Ion(ind,qom=qom_e)
    for i in range(3):
        J[i] = np.multiply(n_p,u_p[i]) - np.multiply(n_e,u_e[i])
    del n_e
else:
    print('ERROR: unknown code_name!')
    sys.exit(-1)

#anisotropy
Pp = run.get_Press(ind)
pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
anisp = np.divide(pperpp,pparlp)
if smooth_flag:
    anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')

if code_name == 'iPIC':
    Pe = run.get_Press(ind,qom=qom_e)
    pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
    anise = np.divide(pperpe,pparle)
elif (code_name == 'HVM') and w_ele:
    pparle,pperpe = run.get_Te(ind)
    pparle = np.multiply(pparle,n_p)
    pperpe = np.multiply(pperpe,n_p)
    del n_p
    anise = np.divide(pperpe,pparle)

if smooth_flag and w_ele:
    anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')

#beta parl
B2 = np.sum(np.square(B),axis=0)
if code_name == 'iPIC':
    bmult = 4.*np.pi
elif code_name == 'HVM':
    bmult = 1.

bparlp = 2.*np.divide(pparlp,B2)*bmult
if smooth_flag:
    bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')

if w_ele:
    bparle = 2.*np.divide(pparle,B2)*bmult
    if smooth_flag:
        bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')

del B2

#nongyrotropy
sqrtQp = np.sqrt(np.divide(np.square(Pp[0,1])+np.square(Pp[0,2])+np.square(Pp[1,2]),
                           np.square(pperpp)+2.*np.multiply(pperpp,pparlp)))
del Pp,pparlp,pperpp
if code_name == 'iPIC':
    sqrtQe = np.sqrt(np.divide(np.square(Pe[0,1])+np.square(Pe[0,2])+np.square(Pe[1,2]),
                               np.square(pperpe)+2.*np.multiply(pperpe,pparle)))
    del Pe,pparle,pperpe
elif w_ele:
    del pparle,pperpe

#JdotEprime
absJEp = np.abs(np.sum(np.multiply(J,E+np.cross(u_p,B,axis=0)),axis=0))
absJEe = np.abs(np.sum(np.multiply(J,E+np.cross(u_e,B,axis=0)),axis=0))
del B,E,J,u_p,u_e
if smooth_flag:
    absJEp[:,:,0] = gf(absJEp[:,:,0],[gfsp,gfsp],mode='wrap')
    absJEe[:,:,0] = gf(absJEe[:,:,0],[gfse,gfse],mode='wrap')

#cleaning
gc.collect()

print('\nData loaded and variables computed!')

#-------------------------------------------------------
#PREPROCESSING
#-------------
X,Y = np.meshgrid(x,y)
#df = pd.DataFrame( {'x':     np.array(X.T.flat),
#                    'y':     np.array(Y.T.flat),
#                    'anisp': np.array(anisp.flat ),
#                    'anise': np.array(anise.flat ),
#                    'bparlp':np.array(bparlp.flat),
#                    'bparle':np.array(bparle.flat),
#                    'sqrtQp':np.array(sqrtQp.flat),
#                    'sqrtQe':np.array(sqrtQe.flat),
#                    'absJEp':np.array(absJEp.flat),
#                    'absJEe':np.array(absJEe.flat)} )
df = pd.DataFrame( {'x':     X.T.flatten()   ,
                    'y':     Y.T.flatten()   ,
                    'anisp': anisp.flatten() ,
                    'anise': anise.flatten() ,
                    'bparlp':bparlp.flatten(),
                    'bparle':bparle.flatten(),
                    'sqrtQp':sqrtQp.flatten(),
                    'sqrtQe':sqrtQe.flatten(),
                    'absJEp':absJEp.flatten(),
                    'absJEe':absJEe.flatten()} )
df = df[['x','y',
         'anisp','bparlp','sqrtQp','absJEp',
         'anise','bparle','sqrtQe','absJEe']]
del X,Y
vars_ = [v for v in df.columns if v != 'x' and v != 'y']
print('\nDataFrame created!')

#logscale for all
if logscale_flg:
    df[vars_] = np.log(df[vars_])
    vars_ = ['ln' + v for v in vars_]
    df.columns = ['x','y'] + vars_
    print('\nVariables log-scaled!')

#correlation matrix
corr = df[vars_].corr()
plt.close('all')
fig, ax = plt.subplots(figsize=(15,8))
hm = sns.heatmap(corr,
                 ax=ax,
                 cmap='coolwarm',
                 vmin=-1.0,
                 vmax=1.0,
                 annot=True,
                 fmt='.2f',
                 annot_kws={'size':15},
                 linewidths=0.5)
ax.set_title('Correlation matrix')
plt.show()
fig.savefig(opath+'/'+out_dir+'/correlation_matrix_'+run_name+'_'+str(ind)+'.png')
plt.close()

#select data
print('\n')
for i,v in enumerate(vars_):
    print('%s \t->\t%d'%(v,i))

var_ind = input('Input the selected variables as a list of integers: ')
myvars = []
for iv in var_ind:
    myvars.append(vars_[iv])

if len(myvars) == 0:
    print('\nNo variables selected; exiting.')
    sys.exit(0)

wdf = df.loc[:,myvars]
print('\nData selected!')

#scale data
if not logscale_flg:
    print('\n')
    for i,v in enumerate(myvars):
        print('%s \t->\t%d'%(v,i))
    var_ind = input('Input the selected variables as a list of integers: ')
    for iv in var_ind:
        wdf[myvars[iv]] = np.log(myvars[iv])

if preprocessing_opt == 'norm':
    # ->-> normalize data
    min_max_scaler = preprocessing.MinMaxScaler()
    tmp = wdf.values
    tmp_scaled = min_max_scaler.fit_transform(tmp)
    wdf = pd.DataFrame(tmp_scaled)
    wdf.columns = myvars
    del tmp,tmp_scaled
    print('\nData normalized!')
elif preprocessing_opt == 'std':
    # ->-> stndardize data
    std_scaler = preprocessing.StandardScaler()
    tmp = wdf.values
    tmp_scaled = std_scaler.fit_transform(tmp)
    wdf = pd.DataFrame(tmp_scaled)
    wdf.columns = myvars
    del tmp,tmp_scaled
    print('\nData standardized!')
elif preprocessing_opt == None:
    print('\nData are not scaled/standardized/etc...')
else:
    print('\nERROR: unkown value for preprocessing_opt. Exiting...')
    sys.exit(-1)

#plot
plt.close()
fig,ax_ = plt.subplots(len(myvars),1,figsize=(8,3*len(myvars)))
if len(myvars) == 1:
    ax = [ax_]
else:
    ax = ax_

for i in range(len(myvars)):
    ax[i].contourf(x[ixmin:ixmax],y[iymin:iymax],
                   wdf[myvars[i]].values.reshape(nx,ny)[ixmin:ixmax,iymin:iymax].T,63)
    ax[i].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,colors='black')
    ax[i].set_xlabel('x')
    ax[i].set_ylabel('y')
    ax[i].set_title(myvars[i])

plt.tight_layout()
plt.show()
fig.savefig(opath+'/'+out_dir+'/myvars_2Dplot_'+run_name+'_'+str(ind)+'.png')
plt.close()

#-------------------------------------------------------
#REGIONS SELECTION
#-----------------
print('\n')
for i,v in enumerate(myvars):
    print('%s \t->\t%d'%(v,i))

iv = ml.secure_input('Select the quantity to plot for regioin selection: ',10,True)

#interactive plot
# -> functions
def on_click(event):
    global selection_flag,focus,sel,ax
    if not selection_flag:
        return
    if focus == None:
        return
    global ix, iy
    ix, iy = event.xdata, event.ydata
    sel[focus][0].append(ix)
    sel[focus][1].append(iy)
    poly_draw(sel[focus],ax)
    return

def on_key_press(event):
    global fig,sel,focus,ax
    if event.key == "control":
        global selection_flag
        selection_flag = True
    elif event.key == "escape":
        global cid0, cid1, cid2
        fig.canvas.mpl_disconnect(cid0)
        fig.canvas.mpl_disconnect(cid1)
        fig.canvas.mpl_disconnect(cid2)
        print('\nNow you can close the plot.')
        fig.canvas.stop_event_loop()
    elif event.key == "ctrl+z":
        if focus == None:
            return
        if len(sel[focus][0]) == 0:
            return
        del sel[focus][0][-1]
        del sel[focus][1][-1]
        poly_draw(sel[focus],ax)
    elif event.key == "ctrl+r":
        fig.canvas.draw_idle()
    elif event.key == "ctrl+n":
        sname = input('\nInsert new selection name: ')
        sel[sname] = [[],[],None,None]
        focus = sname
        print('Focus on %s.'%focus)
    elif event.key == "ctrl+c":
        if len(sel.keys()) == 0:
            return
        print('\nAviable selections are:')
        for i,sn in enumerate(sel.keys()):
            print('%s \t->\t%d'%(sn,i))
        i = input('Choose one to focus on: ')
        if (i < 0) or (i > len(sel.keys())-1):
            focus = None
            print('Invalid choice, focus set to None.')
            return
        focus = sel.keys()[i]
        print('Focus on %s.'%(focus))
    elif event.key == "ctrl+d":
        if len(sel.keys()) == 0:
            return
        print('\nAviable selections are:')
        for i,sn in enumerate(sel.keys()):
            print('%s \t->\t%d'%(sn,i))
        i = input('Choose one to delete: ')
        if (i < 0) or (i > len(sel.keys())-1):
            focus = None
            print('Invalid choice, focus set to None.')
            return
        if sel[sel.keys()[i]][2] != None:
            sel[sel.keys()[i]][2].remove()
        del sel[sel.keys()[i]]
        focus = None
        print('Focus on None.')
    elif event.key == "ctrl+m":
        how_to()
    elif event.key == "ctrl+F":
        if focus == None:
            print('\nFocus on None.')
        else:
            print('\nFocus on %s.'%(focus))
    return

def on_key_release(event):
    if event.key == "control":
        global selection_flag
        selection_flag = False
    return

def how_to():
    print('\nHOW TO...')
    print(' - in order for key/mouse commands to work the plot window must be selected;')
    print(' - ctrl+m to re-print this commands list;')
    print(' - ctrl+n to create a new selection,')
    print(' - - you have to name it via command line')
    print(' - - and focus will be put on it;')
    print(' - ctrl+c to change the selection on focus,')
    print(' - - a number should be given via command line;')
    print(' - ctrl+mouseclick to add a point to the focused selection;')
    print(' - ctrl+z to remove the last point in the focused selection;')
    print(' - ctrl+d to delete a selection,')
    print(' - - a number should be given via command line,')
    print(' - - focus will be set on None;')
    print(' - ctrl+F to show current focus;')
    print(' - in case of input error, focus will be set on None.')
    print(' - ctrl+r to re-draw the plot, if stale (should not happen);')
    print(' - escape to close the interactive session,')
    print(' - - the plot will remain opened in show mode.')
    return

def poly_draw(sel_,ax_):
    if sel_[3] == None:
        if len(sel_[0]) == 0:
            return
        if len(sel_[0]) == 1:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'o')
        elif len(sel_[0]) == 2:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'-')
        else:
            sel_[2], = ax_.fill(sel_[0],sel_[1],alpha=0.5)
        sel_[3] = sel_[2].get_color()
    else:
        if sel_[2] != None:
                sel_[2].remove()
        if len(sel_[0]) == 0:
            sel_[2] = None
        elif len(sel_[0]) == 1:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'o',color=sel_[3])
        elif len(sel_[0]) == 2:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'-',color=sel_[3])
        else:
            sel_[2], = ax_.fill(sel_[0],sel_[1],alpha=0.5,color=sel_[3])
    return

# -> plot
how_to()

sel = {}
focus = None
selection_flag = False

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(16,8))
ax = fig.subplots(1,1)

im = ax.contourf(x[ixmin:ixmax],y[iymin:iymax],
                 wdf[myvars[iv]].values.reshape(nx,ny)[ixmin:ixmax,iymin:iymax].T,63)
plt.colorbar(im,ax=ax)
ax.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,colors='black')
ax.set_xlim(x[ixmin],x[ixmax])
ax.set_ylim(y[iymin],y[iymax])
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_title(myvars[iv])
plt.tight_layout()

cid0 = fig.canvas.mpl_connect('button_press_event',on_click)
cid1 = fig.canvas.mpl_connect('key_release_event',on_key_release)
cid2 = fig.canvas.mpl_connect('key_press_event',on_key_press)
fig.canvas.start_event_loop(timeout=-1)

plt.ioff()
plt.show(1)
fig.savefig(opath+'/'+out_dir+'/selected_regions_'+run_name+'_'+str(ind)+'.png')
plt.close(1)

print('\nRegions selected!')

# -> binary masks
X,Y = np.meshgrid(x,y)
X,Y = X.flatten(),Y.flatten()
points = np.vstack((X,Y)).T
del X,Y

masks = {}
plt.close()
fig = plt.figure(1,figsize=(14,8))
for k in sel.keys():
    print('%s starts'%k)
    masks[k] = [ sel[k][2].get_path().contains_points(points).reshape(ny,nx).T,
                 sel[k][3],
                 0 ]
    plt.contourf(x[ixmin:ixmax],y[iymin:iymax],masks[k][0][ixmin:ixmax,iymin:iymax].T,
                 1,colors=['#00000000',masks[k][1]+'80'])
    plt.plot([],[],color=masks[k][1],label=k)
    print('%s done'%k)

plt.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,colors='black')
plt.title('Selections - sub-masks')
plt.xlim(x[ixmin],x[ixmax])
plt.ylim(y[iymin],y[iymax])
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.show()
fig.savefig(opath+'/'+out_dir+'/sub-masks_'+run_name+'_'+str(ind)+'.png')
plt.close()

del sel,points
print('\nSub-masks generated!')

print('\n')
for i,v in enumerate(masks.keys()):
    print('%s (lvl=%d)\t->\t%d'%(v,masks[v][2],i))

flg = ml.secure_input('Do you want to merge some masks? (True/False): ',True,True)
while flg:
    mask_ind = input('Input the selected masks as a list of integers: ')
    mymasks = []
    for im in mask_ind:
        mymasks.append(masks.keys()[im])
    if len(mymasks) > 1:
        lvls = []
        for m in mymasks:
            lvls.append(masks[m][2])
        if np.max(lvls) == np.min(lvls):
            lvl = lvls[0]
            col = masks[mymasks[0]][1]
            for m in mymasks:
                masks[m][2] += 1
        else:
            lvl = np.min(lvls)
            col = None
            for m in mymasks:
                if masks[m][2] == lvl:
                    masks[m][2] += 1
                    if col == None:
                        col = masks[m][1]
        mname = input('Insert the merged-mask name: ')
        masks[mname] = [ np.zeros((nx,ny),dtype=np.bool),
                         col,
                         lvl ]
        for m in mymasks:
            masks[mname][0] += masks[m][0]
    print('\n')
    for i,v in enumerate(masks.keys()):
        print('%s (lvl=%d)\t->\t%d'%(v,masks[v][2],i))
    flg = ml.secure_input('Do you want to merge other masks? (True/False): ',True,True)

plt.close()
fig = plt.figure(1,figsize=(14,8))
for k in masks.keys():
    if masks[k][2] != 0:
        continue
    plt.contourf(x[ixmin:ixmax],y[iymin:iymax],masks[k][0][ixmin:ixmax,iymin:iymax].T,
                 1,colors=['#00000000',masks[k][1]+'80'])
    plt.plot([],[],color=masks[k][1],label=k)

plt.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,colors='black')
plt.title('Selections - masks')
plt.xlim(x[ixmin],x[ixmax])
plt.ylim(y[iymin],y[iymax])
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.show()
fig.savefig(opath+'/'+out_dir+'/masks_'+run_name+'_'+str(ind)+'.png')
plt.close()

print('\nSub-masks merged!')

#-------------------------------------------------------
#CHECK FOR SEPARATION
#--------------------
#add masks to the dataframe
mymasks = []
for m in masks.keys():
    if masks[m][2] != 0:
        continue
    wdf[m] = masks[m][0].flatten()
    mymasks.append(m)

wdf['region'] = 'none'
for m in mymasks:
    wdf.loc[wdf[m],'region'] = m

sdf = wdf.loc[np.sum(wdf[mymasks],axis=1).astype(np.bool),myvars+['region']]
print('\nMasks added to the dataframe!')

#plotting histograms
def pairgrid_heatmap(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    plt.hist2d(x,y,cmap=cmap,cmin=1,**kws)
    return

def pairgrid_kde(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    sns.kdeplot(x,y,cmap=cmap)
    return

plt.close()

grid = sns.PairGrid(sdf,size=4,hue='region')
grid = grid.map_lower(plt.scatter,s=10)
grid = grid.map_diag(plt.hist,bins=50,histtype="step",linewidth=3)
#grid = grid.map_upper(pairgrid_heatmap,bins=50,norm=LogNorm())
grid = grid.map_upper(pairgrid_kde)
grid = grid.add_legend(bbox_to_anchor=(0.135,0.9))

for i in range(len(myvars)):
    grid.axes[i,i].set_xlim(0.,1.)
    grid.axes[i,i].set_ylim(0.,1.)

#plt.suptitle('Histograms - %s - t=%f'%(run_label,times[ind]))
plt.suptitle('KDE - %s - t=%f'%(run_label,times[ind]))
fig = plt.gcf()
plt.show()
#fig.savefig(opath+'/'+out_dir+'/histograms_'+run_name+'_'+str(ind)+'.png')
fig.savefig(opath+'/'+out_dir+'/kde_'+run_name+'_'+str(ind)+'.png')
plt.close()
