#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/work2/finelli/Documents_SAV/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
sys.path.append('/work2/finelli/Documents_SAV/templates')
from histABC_wrapped import hist2d_wrap as h2dw
import thresholds as th

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#seqential CyBu colormap
bit_rgb = np.linspace(0,1,256)
colors = [(127,255,255),(0,255,255),(0,127,255),(0,0,255),(0,0,127)]
positions = np.linspace(0.,1.,len(colors))
for iii in range(len(colors)):
    colors[iii] = (bit_rgb[colors[iii][0]],
                   bit_rgb[colors[iii][1]],
                   bit_rgb[colors[iii][2]])

cdict = {'red':[], 'green':[], 'blue':[]}
for pos, color in zip(positions, colors):
    cdict['red'].append((pos, color[0], color[0]))
    cdict['green'].append((pos, color[1], color[1]))
    cdict['blue'].append((pos, color[2], color[2]))

CyBu = mpl.colors.LinearSegmentedColormap('CyBu',cdict,256)

#seqential BuCy colormap
bit_rgb = np.linspace(0,1,256)
colors = [(  0,  0,127),
          (  0,  0,255),
          (  0,127,255),
          (  0,255,255),
          (127,255,255)]
positions = np.linspace(0.,1.,len(colors))
for iii in range(len(colors)):
    colors[iii] = (bit_rgb[colors[iii][0]],
                   bit_rgb[colors[iii][1]],
                   bit_rgb[colors[iii][2]])

cdict = {'red':[], 'green':[], 'blue':[]}
for pos, color in zip(positions, colors):
    cdict['red'].append((pos, color[0], color[0]))
    cdict['green'].append((pos, color[1], color[1]))
    cdict['blue'].append((pos, color[2], color[2]))

BuCy = mpl.colors.LinearSegmentedColormap('BuCy',cdict,256)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of fluctuation in anisotrpy vs parallel beta space, cumulated over different times, in 3d.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
cmap_name = 'BuCy'#'cividis'#'cool'#'winter'#'copper'#'CyBu'#
cmap_dict = {'BuCy':BuCy, 'cividis':'cividis', 'cool':'cool', 'winter':'winter', 'copper':'copper', 'CyBu':CyBu} 
out_dir = 'fluct_in_wind_multitimes_3d'#_%s'%(cmap_name,)
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

calc = fibo('calc')
calc.meta=run.meta

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#plot type
mode = 'ABCacc'
logscale_flg = True

#B0
B0_as_time_avg = True

#mask
msk_flg = True # True -> 'cutout'; False -> 'CS1'
if msk_flg:
    mask_name = 'cutout'
    mask = np.ones((nx,ny),dtype=bool)
    for ix in range(nx):
        for iy in range(cut1,cut2+1):
            mask[ix,iy] = False
else:
    mask_name = 'CS1'
    mask = np.zeros((nx,ny),dtype=bool)
    for ix in range(nx):
        for iy in range(int(12.5/run.meta['ddd'][0])):
            mask[ix,iy] = True

#-------------------------------------------------------
#plot function
#-------------
def fluct_plot_func_masked( ax, xdata, ydata, mask, field, field_name, species_vars,
                            xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                            legend_flag=False, run_label=run_label,
                            mode=mode,logscale_flg=logscale_flg,vmin=None,vmax=None,
                            h_old=None, c_pass=0, n_pass=1 ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    mask         -> numpy.ndarray(nx,ny) - island mask
    field        -> numpy.ndarray(nx,ny) - field
    field_name   -> string - name of the field
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] or [array,array] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    run_label    -> [optional, set by default] string - run label
    mode         -> [optional, set by default] string - 'ABC' or 'ABCmax' or 'standard'
    logscale_flg -> [optional, set by default] boolean - if colormap is logscaled
    vmin         -> float or None - minimum value in the colorbar
    vmax         -> float or none - maximum value in the colorbar
    h_old        -> [optional] None or np.array - default=None - 
                    where histograms are cumulated
    c_pass       -> [optional] int - default=0 - passage index
    n_pass       -> [optional] int - default=1 - number of cumulating passages

    OUTPUTS:
    if c_pass < n_pass-1:
        h            -> np.array - cumulated 2D histogram
    if c_pass == n_pass-1:
        handles      -> [lines.Line2D, ...] - list of handles for the legend
    if ERROR:
        -1           -> int - ERROR exit
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    if xdata_range == None:
        xdata_range = [np.min(xdata),np.max(xdata)]
    if ydata_range == None:
        ydata_range = [np.min(ydata),np.max(ydata)]

    xdata_masked = xdata[mask]
    ydata_masked = ydata[mask]
    field_masked = field[mask]
    h,xbin,ybin = h2dw(np.log10(xdata_masked),np.log10(ydata_masked),field_masked,nbins_xy=n_bins,
                       x_range=list(np.log10(xdata_range)),y_range=list(np.log10(ydata_range)),
                       mode=mode)
    del field_masked,xdata_masked,ydata_masked
    if type(h_old) != type(None):
        if h_old.shape != h.shape:
            print('\nh_old.shape=%s is different from h.shape=%s\n'%(str(h_old.shape),str(h.shape)))
            return -1
        h = h + h_old
    if c_pass == n_pass-1:
        tmp_xbin = np.power(10.,xbin)
        tmp_ybin = np.power(10.,ybin)
        X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
        with np.errstate(divide='ignore', invalid='ignore'):
            h = np.divide(h[n_bins[0]:,:],h[:n_bins[0],:])
        h[np.isnan(h)] = 0.
        if logscale_flg:
            if vmin == None:
                vmin = np.min(h[h>0.])
            if vmax == None:
                vmax = np.max(h[h>0.])
            im = ax.pcolormesh( X,Y,h.T,cmap=cmap_dict[cmap_name],
                                norm=mpl.colors.LogNorm(vmin=vmin,vmax=vmax) )
        else:                                   
            if vmin == None:                    
                vmin = np.min(h)                
            if vmax == None:                    
                vmax = np.max(h)                
            h[h==0.] = np.nan                   
            im = ax.pcolormesh(X,Y,h.T,cmap=cmap_dict[cmap_name],vmin=vmin,vmax=vmax)
        del X,Y,h

        xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),len(xbin)-1)
        del xbin,ybin,tmp_xbin,tmp_ybin

        if species_tag == 'p':
            p,f = th.params['L2014']['pC']['1.0e-2Op']['bM']
            lC, = ax.plot(xvals,th.func[f](xvals,*p),'darkorange',lw=2,label="Cyclotron (CI)")

            p,f = th.params['M2012']['pM']['1.0e-2Op']['bM']
            lM, = ax.plot(xvals,th.func[f](xvals,*p),'red',lw=2,label="Mirror (MI)")

            p,f = th.params['A2016']['pPF']['1.0e-2Op']['bM']
            lP, = ax.plot(xvals,th.func[f](xvals,*p),'forestgreen',lw=2,label="parallel Firehose (PFHI)")

            p,f = th.params['A2016']['pOF']['1.0e-2Op']['bM']
            lO, = ax.plot(xvals,th.func[f](xvals,*p),'darkviolet',lw=2,label="oblique Firehose (OFHI)")

        if species_tag == 'e':
            p,f = th.params['L2013']['eC']['1.0e-2Oe']['bM']
            lC, = ax.plot(xvals,th.func[f](xvals,*p),'darkorange',lw=2,label="Cyclotron (CI)")

            p,f = th.params['G2006']['eM']['1.0e-3Oe']['bM']
            lM, = ax.plot(xvals,th.func[f](xvals,*p),'red',lw=2,label="Mirror (MI)")

            p,f = th.params['G2003']['ePF']['1.0e-2Op']['bM']
            lP, = ax.plot(xvals,th.func[f](xvals,*p),'forestgreen',lw=2,label="parallel Firehose (PFHI)")

            p,f = th.params['H2014']['eOF']['1.0e-3Oe']['bM']
            lO, = ax.plot(xvals,th.func[f](xvals,*p),'darkviolet',lw=2,label="oblique Firehose (OFHI)")

        lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',lw=2,label='$\\beta_{\parallel}^{-1}$')

        ax.set_xlabel('$\\beta_{\parallel,\mathrm{%s}}$'%(species_tag))
        ax.set_ylabel('$p_{\perp,\mathrm{%s}}/p_{\parallel,\mathrm{%s}}$'%(species_tag,species_tag))
        ax.set_xlim(xdata_range[0],xdata_range[1])
        ax.set_ylim(ydata_range[0],ydata_range[1])
        if legend_flag:
            ax.legend()
        title_str = run_label+' - '+species_name+' - '+field_name
        if t != None:
            title_str += ' - $t = %s$'%(t)
        ax.set_title(title_str)
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.colorbar(im,ax=ax)

        return [lC,lM,lP,lO,lb]
    elif c_pass < n_pass-1:
        return h
    else:
        print('\nc_pass=%d is greater than n_pass-1=%d\n'%(c_pass,n_pass-1))
        return -1

def first_step():
    hp = None
    he = None
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
    return fig,ax,hp,he

def second_step( ax,field,field_name,mask,p_vars,e_vars,w_ele,n_bins,
                 hp,he,c_pass,n_pass,bparlp,anisp,bparle,anise,
                 betap_lim=None,anisp_lim=None,betae_lim=None,anise_lim=None,
                 vminp=None,vmaxp=None,vmine=None,vmaxe=None ):
    hp = fluct_plot_func_masked(ax[0],bparlp,anisp,mask,field,field_name,p_vars,
                                xdata_range=betap_lim,ydata_range=anisp_lim,n_bins=[n_bins,n_bins],
                                vmin=vminp,vmax=vmaxp,h_old=hp,c_pass=c_pass,n_pass=n_pass)
    if type(hp) == np.int:
        if hp == -1:
            print('\nSomething went wrong in windlike_plot_func (protons)\n')
#
    if w_ele:
        he = fluct_plot_func_masked(ax[1],bparle,anise,mask,field,field_name,e_vars,
                                    xdata_range=betae_lim,ydata_range=anise_lim,n_bins=[n_bins,n_bins],
                                    vmin=vmine,vmax=vmaxe,h_old=he,c_pass=c_pass,n_pass=n_pass)
        if type(he) == np.int:
            if he == -1:
                print('\nSomething went wrong in windlike_plot_func (electrons)\n')
    return hp,he

def third_step( fig,ax,w_ele,hp,n_bins,field_label,
                logscale_flg=logscale_flg,opath=opath,out_dir=out_dir,time_avg_flg=B0_as_time_avg,
                run_name=run_name,ind1=ind1,ind2=ind2,mask_name=mask_name,mode=mode ):
    if not w_ele:
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        ax[1].legend(handles=hp,loc='center',borderaxespad=0)
    #
    fig.tight_layout()
    title = opath+'/'+out_dir+'/'+'fluct_in_wind_multitimes_'+mask_name+'_'+mode
    if logscale_flg:
        title += '_logscale'
    title += '_'+run_name+'_'+field_label
    if time_avg_flg:
        title += '_B0timeavg'
    title += '_'+str(ind1)+'-'+str(ind2)+'_b'+str(n_bins)
    fig.savefig(title+'.png')
    plt.close(fig)
    return

#-------------------------
#B0
if B0_as_time_avg:
    #as time average
    #---> loop over times <---
    cnt = 0.

    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        _,B = run.get_EB(ind)
        if ind == ind1:
            Beq = B
        else:
            Beq += B
        cnt += 1.

    Beq /= cnt
else:
    #as initial field
    if code_name == 'HVM':
        Bx0 = 0.718722
        Bz0 = 0.695297
    elif code_name == 'iPIC':
        Bx0 = 0.718722*0.01
        Bz0 = 0.695297*0.01
    A_1 = - Bx0
    A_2 = - A_1

    _,Y,_ = np.meshgrid(x,y,z)
    Y = np.rollaxis(Y, 1, 0)
    Bxeq = ( - A_1*np.tanh( (Y - y1)/L1 )
             - A_2*np.tanh( (Y - y2)/L2 )
             - np.sign(A_1)*(A_1 - A_2)*0.5 )
    del Y

    Beq = np.array([Bxeq,np.zeros((nx,ny,nz),dtype=float),
                   np.full((nx,ny,nz),Bz0,dtype=float)]).reshape(3,nx,ny,nz)
    del Bxeq

Beq2 = Beq[0]*Beq[0] + Beq[1]*Beq[1] + Beq[2]*Beq[2]
#---> loop over times <---
n_bins = 25*( int(np.sqrt(float(nx*ny*nz)/(1024.*512.))) )*int(round(np.sqrt(ind2-ind1+1)))
        #50
ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
n_pass = len(ind_range)

plt.close('all')

field_names = {'fluct_B':'$\delta B/\overline{B}$',
               'fluct_Bperp':'$\delta B_{\perp}/\overline{B}$',
               'anis_dB':'$\delta B_{\parallel}/\delta B_{\perp}$'}
fg_dict = {}
ax_dict = {}
hp_dict = {}
he_dict = {}
keys = list(field_names.keys())
for k in keys:
    fg_dict[k],ax_dict[k],hp_dict[k],he_dict[k] = first_step()

fields = {}
v = {'min':{'p':{},'e':{}},
     'max':{'p':{},'e':{}}}
for k in keys:
    v['min']['p'][k] = None
    v['max']['p'][k] = None
    v['min']['e'][k] = None
    v['max']['e'][k] = None

#vminvmax - frtom 265 to 290
#v['min']['e']['fluct_B'] = 2.e-2
#v['max']['e']['fluct_B'] = 1.
#v['min']['e']['fluct_Bperp'] = 7.e-3
#v['max']['e']['fluct_Bperp'] = 9.e-1
#v['min']['e']['anis_dB'] = 2.e-2
#v['max']['e']['anis_dB'] = 3.e+1

#NEW vminvmax OLD - as windlike w/ error
#v['min']['e']['fluct_B'] = 3.5e+0
#v['max']['e']['fluct_B'] = 3.5e-2
#v['min']['e']['fluct_Bperp'] = 1.5e-2
#v['max']['e']['fluct_Bperp'] = 9.5e-1
#v['min']['e']['anis_dB'] = 1.5e-2
#v['max']['e']['anis_dB'] = 7.5e+1

#NEW vminvmax - as windlike
#v['min']['e']['fluct_B'] = 3.5e-2
#v['max']['e']['fluct_B'] = 1.0e+0
#v['min']['e']['fluct_Bperp'] = 7.0e-2
#v['max']['e']['fluct_Bperp'] = 8.0e-1
#v['min']['e']['anis_dB'] = 4.0e-1
#v['max']['e']['anis_dB'] = 2.5e+1

#for k in keys:
#    v['min']['p'][k] = v['min']['e'][k]
#    v['max']['p'][k] = v['max']['e'][k]

#NEW NEW vminvmax - <F> sync
#v['min']['e']['fluct_B'] = 1.5e-2
#v['max']['e']['fluct_B'] = 2.0e+0
#v['min']['e']['fluct_Bperp'] = 4.5e-3
#v['max']['e']['fluct_Bperp'] = 9.5e-1
#v['min']['e']['anis_dB'] = 3.5e-3
#v['max']['e']['anis_dB'] = 5.5e+1

#NEW NEW NEW vminvmax - <F> sync w/ some colorbar adjustment
#v['min']['e']['fluct_B'] = 1.5e-2
#v['max']['e']['fluct_B'] = 2.0e+0
#v['min']['e']['fluct_Bperp'] = 4.5e-3
#v['max']['e']['fluct_Bperp'] = 9.5e-1
#v['min']['e']['anis_dB'] = 3.5e-3
#v['max']['e']['anis_dB'] = 5.0e+1

print " ",
for c_pass,ind in enumerate(ind_range):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)

    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp = gf(anisp,[gfsp,gfsp,gfsp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            n_p,_ = run.get_Ion(ind)
            pparle,pperpe = run.get_Te(ind)
            pparle *= n_p
            pperpe *= n_p
            del n_p
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise = gf(anise,[gfse,gfse,gfse],mode='wrap')
        del pperpe
    else:
        anise = None
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp = gf(bparlp,[gfsp,gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle = gf(bparle,[gfse,gfse,gfse],mode='wrap')
        del pparle
    else:
        bparle = None
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #fluctuations
    dB = B - Beq
#
    b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    for i in range(3):
        b[i] = B[i]/np.sqrt(B2)
    del B2,B
    dBparl = np.abs(dB[0]*b[0] + dB[1]*b[1] + dB[2]*b[2])
    dB2 = dB[0]*dB[0] + dB[1]*dB[1] + dB[2]*dB[2]
    dBperp = np.sqrt(dB2 - dBparl*dBparl)
#
    #dxbx = calc.calc_gradx(b[0])
    #dybx = calc.calc_grady(b[0])
    #dxby = calc.calc_gradx(b[1])
    #dyby = calc.calc_grady(b[1])
    #dxbz = calc.calc_gradx(b[2])
    #dybz = calc.calc_grady(b[2])
    #b_Nb = np.array([b[0]*dxbx+b[1]*dybx, b[0]*dxby + b[1]*dyby, b[0]*dxbz + b[1]*dybz])
    #del b,dxbx,dybx,dxby,dyby,dxbz,dybz
    #fields['curv_b'] = np.sqrt(b_Nb[0]**2 + b_Nb[1]**2 + b_Nb[2]**2)###
    #del b_Nb
#
    fields['fluct_B'] = np.divide(np.sqrt(dB2),np.sqrt(Beq2))###
    fields['fluct_Bperp'] = np.divide(dBperp,np.sqrt(Beq2))###
    fields['anis_dB'] = np.divide(dBparl,dBperp)###
    del dB2,dBperp,dBparl
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    for k in keys:
        hp_dict[k],he_dict[k] = second_step(ax_dict[k],fields[k],field_names[k],
                                            mask,p_vars,e_vars,w_ele,n_bins,
                                            hp_dict[k],he_dict[k],c_pass,n_pass,
                                            bparlp,anisp,bparle,anise,
                                            betap_lim=betap_lim,anisp_lim=anisp_lim,
                                            betae_lim=betae_lim,anise_lim=anise_lim,
                                            vminp=v['min']['p'][k],vmaxp=v['max']['p'][k],
                                            vmine=v['min']['e'][k],vmaxe=v['max']['e'][k])
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

for k in keys:
    third_step(fg_dict[k],ax_dict[k],w_ele,hp_dict[k],n_bins,k)
