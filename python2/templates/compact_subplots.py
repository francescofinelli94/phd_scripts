#------------------------------
#imports
#-------
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

#------------------------------
#functions
#---------
def get_ax(ax,i,j):
    if type(ax) != np.ndarray:
        return ax
    if len(ax.shape) == 2:
        return ax[i,j]
    else:
        return ax[i+j]

#------------------------------
#inputs
#------
fig_num = 1
nrow = 3
ncol = 2
figsize = (8,8)
sharex = 'col' # True, False, 'col'
sharey = False # True, False, 'row'
labels = ['LAB0','LAB1','LAB2']
linestyles = ['-','-','-']
title = 'My title'
xlabels = [['x top 0',   'x top 1'   ],
           ['dummy',     'dummy'     ],
           ['x bottom 0','x bottom 1']]
ylabels = [['y left 0','y right 0'],
           ['y left 1','y right 1'],
           ['y left 2','y right 2']]

#------------------------------
#axes
#----
plt.close('all')
fig = plt.figure(fig_num,figsize=figsize)
ax = fig.subplots(nrow,ncol,sharex=sharex,sharey=sharey)

#------------------------------
#legend
#------
ax_ = get_ax(ax,0,0)
lines = {}
for lab,ls in zip(labels,linestyles):
    lin, = ax_.plot([],[],linestyle=ls)
    lines[lab] = lin
fig.legend(lines.values(),labels,
        ncol=3,mode="expand",borderaxespad=0.,framealpha=0.,
        bbox_to_anchor=(0.125,0.9,0.775,0.9),loc='lower left')

#------------------------------
#plots
#-----
n = 200
x = [np.linspace(0.,2.*np.pi,n),np.linspace(0.,4.*np.pi,n)]
for (i,j) in [(i,j) for i in range(nrow) for j in range(ncol)]:
    ax_ = get_ax(ax,i,j)
    y_all = []
    for k,lab in enumerate(labels):
        y = (1.+float(i))*np.cos(x[j]+np.pi*0.5/2.*float(k))+np.random.random(n)
        ax_.plot(x[j],y,color=lines[lab].get_color(),
                 linestyle=lines[lab].get_linestyle())
        y_all.append(y)
    ax_.set_xlim(x[j][0],x[j][-1])
    ax_.set_ylim(np.min(y_all),np.max(y_all))

#------------------------------
#ticks and labels
#----------------
if sharex != False:
    top_ext = 0.05
else:
    top_ext = 0.
fig.subplots_adjust(hspace=.0,wspace=.0,top=0.85+top_ext)
for (i,j) in [(i,j) for i in range(nrow) for j in range(ncol)]:
    ax_ = get_ax(ax,i,j)
    ax_.tick_params(bottom=True,top=True,left=True,right=True,
            direction='in',labelbottom=False,labeltop=False,
            labelleft=False,labelright=False)
    if j == 0:
        ax_.tick_params(labelleft=True)
        ax_.yaxis.set_major_formatter(FormatStrFormatter("%.1e"))
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label1.set_visible(False)
    if i == nrow-1:
        ax_.tick_params(labelbottom=True)
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label1.set_visible(False)
    if (j == ncol-1) and (sharey == False):
        ax_.tick_params(labelright=True)
        ax_.yaxis.set_label_position("right")
        ax_.yaxis.set_major_formatter(FormatStrFormatter("%.1e"))
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label2.set_visible(False)
    if (i == 0) and (sharex == False):
        ax_.tick_params(labeltop=True)
        ax_.xaxis.set_label_position("top")
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label2.set_visible(False)
fig.suptitle(title)

#------------------------------
#draw
#----
plt.show(fig_num)
plt.close(fig_num)
