import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

#---------------------------------------

def on_click_trace(event):
    global selection_flag
    if not selection_flag:
        return
#
    global ix, iy
    ix, iy = event.xdata, event.ydata
#
    # assign global variable to access outside of function
    global x_, y_, cnt_vec, tbu0
    x_.append(ix)
    y_.append(iy)
    cnt_vec.append(len(cnt_vec))
    y_ = [xx for _,xx in sorted(zip(x_,y_))]
    cnt_vec = [xx for _,xx in sorted(zip(x_,cnt_vec))]
    x_ = sorted(x_)
    tbu0.set_xdata(x_)
    tbu0.set_ydata(y_)
#
    #interpolate
    if len(x_) > 2:
        global x_tr_max, x_tr, y_tr
        x_tr = x_tr_max[np.logical_and(x_tr_max>=x_[0],x_tr_max<=x_[-1])]
        f_tmp = interpolate.interp1d(x_,y_,kind='quadratic')
        y_tr = f_tmp(x_tr)
        global tbu1
        tbu1.set_xdata(x_tr)
        tbu1.set_ydata(y_tr)
#
    return

def on_key_press(event):
    global fig
    if event.key == "control":
        global selection_flag
        selection_flag = True
    elif event.key == "escape":
        global cid0, cid1, cid2
        fig.canvas.mpl_disconnect(cid0)
        fig.canvas.mpl_disconnect(cid1)
        fig.canvas.mpl_disconnect(cid2)
        fig.canvas.stop_event_loop()
    elif event.key == "ctrl+z":
        global x_, y_, cnt_vec
        global tbu0, tbu1
        del x_[np.argmax(cnt_vec)]
        del y_[np.argmax(cnt_vec)]
        del cnt_vec[np.argmax(cnt_vec)]
        tbu0.set_xdata(x_)
        tbu0.set_ydata(y_)
#
        #interpolate
        if len(x_) > 2:
            global x_tr_max, x_tr, y_tr
            x_tr = x_tr_max[np.logical_and(x_tr_max>=x_[0],x_tr_max<=x_[-1])]
            f_tmp = interpolate.interp1d(x_,y_,kind='quadratic')
            y_tr = f_tmp(x_tr)
            tbu1.set_xdata(x_tr)
            tbu1.set_ydata(y_tr)
        else:
            tbu1.set_xdata([])
            tbu1.set_ydata([])
    elif event.key == "ctrl+r":
        fig.canvas.draw_idle()
#
    return

def on_key_release(event):
    if event.key == "control":
        global selection_flag
        selection_flag = False
#
    return

#---------------------------------------

nx = 512
ny = 256
xl = 2.*np.pi*4.
yl = 2.*np.pi*2.

x = np.linspace(0.,xl,nx,dtype=np.float32)
y = np.linspace(0.,yl,ny,dtype=np.float32)
field = np.empty((nx,ny),dtype=np.float32)
for i in range(nx):
    for j in range(ny):
        field[i,j] = np.cos(x[i]) + np.sin(y[j])

ixmin = 0
ixmax = nx - 1
iymin = 0
iymax = ny - 1

#---------------------------------------

f_tmp = interpolate.interp1d(np.arange(len(x)),x)
res_mult = 2
x_tr_max = f_tmp(np.linspace(0.,len(x)-1.,(len(x)-1)*res_mult+1))

#---------------------------------------

print('\nHOW-TO:')
print(' -> ctrl+mousclick to select a point')
print(' -> ctrl+z to remove the last point')
print(' -> ctrl+r to refresh the plot if stale (should not happen)')
print(' -> press Esc to close the interactive mode (non-interactive plot will be shown)\n')

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(12,5))
ax = fig.subplots(1,1)

ax.contour(x[ixmin:ixmax],y[iymin:iymax],field[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_xlim(x[ixmin],x[ixmax])
ax.set_ylim(y[iymin],y[iymax])
ax.set_title('field')

fig.tight_layout()

x_ = []
y_ = []
x_tr = []
y_tr = []
cnt_vec = []
selection_flag = False
tbu0, = ax.plot(x_,y_,'or')
tbu1, = ax.plot(x_tr,y_tr,'--r')
cid0 = fig.canvas.mpl_connect('button_press_event', on_click_trace)
cid1 = fig.canvas.mpl_connect('key_release_event', on_key_release)
cid2 = fig.canvas.mpl_connect('key_press_event', on_key_press)
fig.canvas.start_event_loop(timeout=-1)

plt.ioff()
plt.show(1)
plt.close(1)
