import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

font = 17
mpl.rc('text',usetex=True)
mpl.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
mpl.rc('font',family ='serif',size=font)

phi = (1.+5.**0.5)*0.5
conj_phi = phi - 1.

def rainbow_text(x, y, strings, colors, orientation='horizontal',
                 ax=None, **kwargs):
    """
    Take a list of *strings* and *colors* and place them next to each
    other, with text strings[i] being shown in colors[i].

    Parameters
    ----------
    x, y : float
        Text position in data coordinates.
    strings : list of str
        The strings to draw.
    colors : list of color
        The colors to use.
    orientation : {'horizontal', 'vertical'}
    ax : Axes, optional
        The Axes to draw into. If None, the current axes will be used.
    **kwargs
        All other keyword arguments are passed to plt.text(), so you can
        set the font size, family, etc.
    """
    if ax is None:
        ax = plt.gca()
    t = ax.transData
    canvas = ax.figure.canvas

    assert orientation in ['horizontal', 'vertical']
    if orientation == 'vertical':
        kwargs.update(rotation=90, verticalalignment='bottom')

    for s, c in zip(strings, colors):
        text = ax.text(x, y, s, color=c, transform=t, **kwargs)

        # Need to draw to update the text position.
        text.draw(canvas.get_renderer())
        ex = text.get_window_extent()
        if orientation == 'horizontal':
            t = mpl.transforms.offset_copy(
                text.get_transform(), x=ex.width, units='dots')
        else:
            t = mpl.transforms.offset_copy(
                text.get_transform(), y=ex.height, units='dots')

W1 = 2 #length first box
H1 = W1*phi #height first box
H2 = W1*conj_phi**1 #height second box
H3 = H2*conj_phi #height third box

l1 = 0.11 #left margin
r1 = 0.90 #right margin
b1 = 0.08 #bottom margin
t1 = 0.92 #top margin

m1 = 1.6 #multiplier

plt.close()
fig = plt.figure(figsize=(m1*(W1+H2+H3)/(r1-l1),m1*(H1+H2+H3)/(t1-b1)),
                 constrained_layout=False)
gs = GridSpec(3,3,figure=fig,width_ratios=[W1,H2,H3],height_ratios=[H3,H2,H1],
              wspace=0.,hspace=0.,left=l1,right=r1,bottom=b1,top=t1)


ax0 = fig.add_subplot(gs[1,0])
ax0.set_title('title 1 0')
ax0.set_ylabel('$|J|,\;\int_\ell\mathcal{I}$ (norm.)')
ax0.set_xlim(15.5,15.5+60.*0.0511)
ax0.set_ylim(0.,1.)
rainbow_text(15.5-0.235*60.*0.0511,0.09,['$|J|$',',','l','$\int_\ell\mathcal{I}$ (norm.)'],
             ['green','black','white','blue'],orientation='vertical',
             ax=ax0,size=font)
ax0.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=False,labeltop=False,
                labelleft=True,labelright=False)

ax1 = fig.add_subplot(gs[0:2,1:3])
ax1.xaxis.set_label_position("top")
ax1.set_xlabel('xlabel 01 12')
ax1.yaxis.set_label_position("right")
ax1.set_ylabel('ylabel 01 12')
ax1.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=False,labeltop=True,
                labelleft=False,labelright=True)

ax2 = fig.add_subplot(gs[2,0])
ax2.set_xlabel('xlabel 2 0')
ax2.set_ylabel('ylabel 2 0')
ax2.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=True,labeltop=False,
                labelleft=True,labelright=False)
xticks = ax2.xaxis.get_major_ticks()
xticks[-1].label1.set_visible(False)
yticks = ax2.yaxis.get_major_ticks()
yticks[-1].label1.set_visible(False)

ax3 = fig.add_subplot(gs[2,1])
ax3.set_xlabel('xlabel 2 1')
ax3.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=True,labeltop=False,
                labelleft=False,labelright=False)

ax4 = fig.add_subplot(gs[0,0])
ax4.axis('off')
ax4.text(0.5,0.5,'TITLE TITLE',ha='center',va='center',size=font)

plt.show()
