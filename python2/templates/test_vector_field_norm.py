import numpy as np
from numba import njit
import timeit

#

n = 10000 #number of repetition per timing test
nx = 1024
ny = 512
A = np.ones((3,nx,ny),dtype=np.float64)
a1 = 1./np.sqrt(6.)
a2 = 1./np.sqrt(3.)
a3 = 1./np.sqrt(2.)
A[0] *= a1
A[1] *= a2
A[2] *= a3

#

def test(name,B,nx=nx,ny=ny):
    print('\ntesting %s...'%name)
    print('passed' if (B==1.).sum() == nx*ny else 'failed')

#

def naive(A):
    return np.sqrt( A[0]**2 + A[1]**2 + A[2]**2 )

@njit('float64[:,:](float64[:,:,:])')
def naive_njit(A):
    return np.sqrt( A[0]**2 + A[1]**2 + A[2]**2 )

def linalg(A):
    return np.linalg.norm( A, axis=0 )

#@njit('float64[:,:](float64[:,:,:])')
#def linalg_njit(A):
#    return np.linalg.norm( A, axis=0 )
#axis not present as argument in np.linalg.norm numba implementation

def naive2(A):
    return np.sqrt( (A*A).sum( axis=0 ) )
    
@njit('float64[:,:](float64[:,:,:])')
def naive2_njit(A):
    return np.sqrt( (A*A).sum( axis=0 ) )
    
def naive2_5(A):
    return np.sqrt( np.multiply( A, A ).sum( axis=0 ) )
    
@njit('float64[:,:](float64[:,:,:])')
def naive2_5_njit(A):
    return np.sqrt( np.multiply( A, A ).sum( axis=0 ) )
    
def einsum(A):
    return np.sqrt( np.einsum( 'i...,i...', A, A ) )

#@njit('float64[:,:](float64[:,:,:])')
#def einsum(A):
#    return np.sqrt( np.einsum( 'i...,i...', A, A ) )
#np.einsum not implemented in numba

#

print('\n========================\n')

print('\nTESTING:')
test('naive',naive(A))
test('naive_njit',naive_njit(A))
test('linalg',linalg(A))
test('naive2',naive(A))
test('naive2_njit',naive_njit(A))
test('naive2_5',naive(A))
test('naive2_5_njit',naive_njit(A))
test('einsum',einsum(A))
print('\nDONE\n')

print('\n------------------------\n')

print('\nTIMING (%d repetitions):'%n)
print('\ntiming naive...\n',timeit.timeit(lambda: naive(A),number=n),'s')
print('\ntiming naive_njit...\n',timeit.timeit(lambda: naive_njit(A),number=n),'s')
print('\ntiming linalg...\n',timeit.timeit(lambda: linalg(A),number=n),'s')
print('\ntiming naive2...\n',timeit.timeit(lambda: naive2(A),number=n),'s')
print('\ntiming naive2_njit...\n',timeit.timeit(lambda: naive2_njit(A),number=n),'s')
print('\ntiming naive2_5...\n',timeit.timeit(lambda: naive2_5(A),number=n),'s')
print('\ntiming naive2_5_njit...\n',timeit.timeit(lambda: naive2_5_njit(A),number=n),'s')
print('\ntiming einsum...\n',timeit.timeit(lambda: einsum(A),number=n),'s')
print('\nDONE\n')

print('\n========================\n')
