import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec

import numpy as np

nx = 512
ny = 512
x = np.linspace(0.,2.*np.pi*5.,nx)
y = np.linspace(1.,5.,ny)
X,Y = np.meshgrid(x,y)
z = np.divide(np.cos(X),Y)

font = 17
mpl.rc('text',usetex=True)
mpl.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
mpl.rc('font',family ='serif',size=font)

W1 = 3. #length first box
H1 = 3. #height first box
H2 = 1.5 #height second box

w1 = 0.14 #width spacing grids

l1 = 0.09 #left margin
r1 = 0.96 #right margin
b1 = 0.08 #bottom margin
t1 = 0.92 #top margin

m1 = 1.6 #multiplier

nss = 2

plt.close()
fig = plt.figure(figsize=(m1*nss*(W1+H2)*(1.+(nss-1)*w1*0.5)/(r1-l1),m1*(H1+H2)/(t1-b1)),
                 constrained_layout=False)
gs = GridSpec(1,nss,figure=fig,wspace=w1,left=l1,right=r1,bottom=b1,top=t1)
ss = []
for iss in range(nss):
    ss.append(SubSpec(2,4,gs[iss],width_ratios=[W1,H2/3.,H2/3.,H2/3.],height_ratios=[H2,H1],
              wspace=0.,hspace=0.))

    ax0 = fig.add_subplot(ss[iss][0,0])
    ax0.plot(x,z.T[:,ny//2])
    ax0.set_xlim(x[0],x[-1])
    ax0.set_title('title 0 0')
    ax0.set_ylabel('ylabel 0 0')
    ax0.tick_params(bottom=True,top=True,left=True,right=True,
                    direction='inout',labelbottom=False,labeltop=False,
                    labelleft=True,labelright=False)

    ax1 = fig.add_subplot(ss[iss][1,0])
    im1 = ax1.pcolormesh(X,Y,z,shading='auto')
    ax1.axvline(x=x[nx//2],ls='--')
    ax1.axhline(y=y[ny//2],ls='--')
    ax1.set_xlim(x[0],x[-1])
    ax1.set_ylim(y[0],y[-1])
    ax1.set_xlabel('xlabel 1 0')
    ax1.set_ylabel('ylabel 1 0')
    ax1.tick_params(bottom=True,top=True,left=True,right=True,
                    direction='inout',labelbottom=True,labeltop=False,
                    labelleft=True,labelright=False)
    xticks = ax1.xaxis.get_major_ticks()
    xticks[-1].label1.set_visible(False)
    yticks = ax1.yaxis.get_major_ticks()
    yticks[-1].label1.set_visible(False)

    ax2 = fig.add_subplot(ss[iss][1,1:4])
    ax2.plot(z.T[nx//2,:],y)
    ax2.set_ylim(y[0],y[-1])
    ax2.set_xlabel('xlabel 1 1')
    ax2.tick_params(bottom=True,top=True,left=True,right=True,
                    direction='inout',labelbottom=True,labeltop=False,
                    labelleft=False,labelright=False)

    ax3 = fig.add_subplot(ss[iss][0,2])
    cb3 = plt.colorbar(im1,cax=ax3)

plt.suptitle('THIS IS QUITE A LONG SUPTITLE, INDEED')

plt.show()
