import os
import sys
import time
from random import random as r_rand

workpath = '/home/finelli/Documents'

fname_c2c = 'control2client.txt'
fname_c2s = 'control2server.txt'

fname_cbuffer = 'client_buffer.txt'
fname_sbuffer = 'server_buffer.txt'

STOP_MSG = 'STOP'
WAITING_TIME = 1 #seconds

CLIENT_TAG = 'client'
SERVER_TAG = 'server'

dots = ['   ','.  ','.. ','...']

#make control file
f = open('%s/%s'%(workpath,fname_c2s),'w')
f.write(CLIENT_TAG)
f.close()

run_flag = True
while run_flag:
    #wait for control
    wait_flag = True
    print "   ",
    cnt = 0
    while wait_flag:
        f = open('%s/%s'%(workpath,fname_c2s),'r')
        tag = f.readline()
        f.close()
        if tag == SERVER_TAG:
            wait_flag = False
        else:
            print "\r",
            print "waiting for control%s"%(dots[cnt%4]),
            sys.stdout.flush()
            cnt += 1
            time.sleep(WAITING_TIME)
    print "   "

    #read file contenit
    f = open('%s/%s'%(workpath,fname_sbuffer),'r')
    msg = f.readline()
    f.close()
    print('Message is: %s'%(msg))
    if msg == STOP_MSG:
        print('Exiting...')
        run_flag = False
        continue

    #generate random number
    r = r_rand()
    print('Generated random number: %f'%(r))

    #write message to client
    f = open('%s/%s'%(workpath,fname_sbuffer),'w')
    f.write(str(r))
    f.close()

    #give back control to client
    f = open('%s/%s'%(workpath,fname_c2s),'w')
    f.write(CLIENT_TAG)
    f.close()

    print('')

print('End of program.')
