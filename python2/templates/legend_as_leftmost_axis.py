import matplotlib.pyplot as plt

def f(n,tight_layout_flag=True):
    plt.close()
    fig,ax = plt.subplots(1,n,figsize=(10*n,8))
#
    if n > 1:
        ax0 = ax[0]
    else:
        ax0 = ax
    lA, = ax0.plot([0,1],label='A',c='gray')
    lB, = ax0.plot([1,0],label='B')
    lC, = ax0.plot([.5,.5],label='C')
    if n > 2:
        for i in range(n-2):
            ax_ = ax[i+1]
            ax_.plot([0,1],'--k')
            ax_.plot([1,0],'--k')
            ax_.plot([.5,.5],'--k')
#
    if n > 1:
        axl = ax[-1]
        axl.spines['top'].set_visible(False)
        axl.spines['right'].set_visible(False)
        axl.spines['bottom'].set_visible(False)
        axl.spines['left'].set_visible(False)
        axl.tick_params(bottom=False,left=False,labelbottom=False,labelleft=False,)
    else:
        axl = ax
#
    axl.legend(handles=(lA,lB,lC),loc='upper right',borderaxespad=0)#,bbox_to_anchor=(n*1.+(n-1)*0.0,1.)
    if tight_layout_flag:
        plt.tight_layout()
    plt.show()

