import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

font = 17
mpl.rc('text',usetex=True)
mpl.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
mpl.rc('font',family ='serif',size=font)

W1 = 3 #length first box
H1 = 3 #height first box
H2 = 1.5 #height second box

l1 = 0.10 #left margin
r1 = 0.90 #right margin
b1 = 0.08 #bottom margin
t1 = 0.92 #top margin

m1 = 1.6 #multiplier

plt.close()
fig = plt.figure(figsize=(m1*(W1+H2)/(r1-l1),m1*(H1+H2)/(t1-b1)),
                 constrained_layout=False)
gs = GridSpec(2,2,figure=fig,width_ratios=[W1,H2],height_ratios=[H2,H1],
              wspace=0.,hspace=0.,left=l1,right=r1,bottom=b1,top=t1)

ax0 = fig.add_subplot(gs[0,0])
ax0.set_title('title 0 0')
ax0.set_ylabel('ylabel 0 0')
ax0.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=False,labeltop=False,
                labelleft=True,labelright=False)

ax1 = fig.add_subplot(gs[0,1])
ax1.xaxis.set_label_position("top")
ax1.set_xlabel('xlabel 0 1')
ax1.yaxis.set_label_position("right")
ax1.set_ylabel('ylabel 0 1')
ax1.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=False,labeltop=True,
                labelleft=False,labelright=True)

ax2 = fig.add_subplot(gs[1,0])
ax2.set_xlabel('xlabel 1 0')
ax2.set_ylabel('ylabel 1 0')
ax2.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=True,labeltop=False,
                labelleft=True,labelright=False)
xticks = ax2.xaxis.get_major_ticks()
xticks[-1].label1.set_visible(False)
yticks = ax2.yaxis.get_major_ticks()
yticks[-1].label1.set_visible(False)

ax3 = fig.add_subplot(gs[1,1])
ax3.set_xlabel('xlabel 1 1')
ax3.tick_params(bottom=True,top=True,left=True,right=True,
                direction='in',labelbottom=True,labeltop=False,
                labelleft=False,labelright=False)

plt.show()
