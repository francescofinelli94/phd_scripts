from glob import glob as g_glob

CPU_PER_NODE = 68
TOT_CPU_TIME = 0

def get_t_in_sec(t_string):
    h,m,s = map(int,t_string.split('.'))
    return s + 60*m + 3600*h

#def get_t_string(t_sec):
#    h = t_sec//3600
#    m = (t_sec - h*3600)//60
#    s = t_sec - 3600*h - 60*m
#    return '%d.%d.%d'%(h,m,s)

segs = sorted(g_glob('[0-9][0-9]'))
for seg in segs:
    with open('./%s/start'%seg,'r') as f:
        seg_line = f.readline().split(',')[-2].strip()
    ts_in_sec = get_t_in_sec(seg_line)
    with open('./%s/end'%seg,'r') as f:
        seg_line = f.readline().split(',')[-2].strip()
    te_in_sec = get_t_in_sec(seg_line)
    dt_in_sec = te_in_sec - ts_in_sec
    dt_in_sec = dt_in_sec + 24*3600 if dt_in_sec < 0 else dt_in_sec
    with open('./%s/job.out'%seg,'r') as f:
        n_nodes = int(f.readlines()[2].split(' ')[2].strip())
    TOT_CPU_TIME += dt_in_sec*n_nodes*CPU_PER_NODE

print('TOT_CPU_TIME = %d s CPU = %d Kh CPU'%(TOT_CPU_TIME,round(float(TOT_CPU_TIME)/(3600*1000))))
