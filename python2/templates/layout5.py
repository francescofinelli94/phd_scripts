import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec

font =17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

n = 5
z = np.random.random((5,5))

A = [.1,.2,.3,.4]
B = [.4,.3,.2,.1]
C = [.1,.2,.1,.2]

gc = np.array([1.,2.,3.,4.])
hw = .1
lA = gc - hw*3.
rA = gc - hw
lB = rA
rB = lB + 2.*hw
lC = rB
rC = lC + 2.*hw

modlist = ['mod1','mod2','mod3','mod4']

plt.close('all')
fig = plt.figure(figsize=(12,12),constrained_layout=False)
gs = GridSpec(2,3,figure=fig,height_ratios=[2.,1.],width_ratios=[8.8,.7,.5])

gs00 = SubSpec(2,2,gs[0,0:2])
cax = fig.add_subplot(gs[0,2])
for i,j in [(i,j) for i in [0,1] for j in [0,1]]:
    ax = fig.add_subplot(gs00[i,j])
    _ = sns.heatmap(z,
                    ax=ax,
                    cbar = ((i==0)and(j==0)),
                    cbar_ax = cax if ((i==0)and(j==0)) else None,
                    cmap='PuBuGn',
                    vmin=0.,
                    vmax=1.,
                    annot=True,
                    fmt='.2f',
                    annot_kws={'size':15},
                    linewidths=0.5,
                    xticklabels=[s for s in range(5)] if i==1 else False,
                    yticklabels=[s for s in range(5)] if j==0 else False)
    if i == 1:
        ax.set_xlabel('$\sigma(\int_{\ell}\mathcal{I})$')
    if j == 0:
        ax.set_ylabel('$\sigma(J^2)$')
    ax.set_title('%d %d'%(i,j))

ax = fig.add_subplot(gs[1,0])
hA = ax.bar(lA,A,width=2.*hw,align='edge',label='F1 score')
for x,y in zip(lA+hw,A):
    ax.text(x,y,str(y),ha='center',va='top',c='white')
hB = ax.bar(lB,B,width=2.*hw,align='edge',label='Precision')
for x,y in zip(lB+hw,B):
    ax.text(x,y,str(y),ha='center',va='top',c='white')
hC = ax.bar(lC,C,width=2.*hw,align='edge',label='Recall')
for x,y in zip(lC+hw,C):
    ax.text(x,y,str(y),ha='center',va='top',c='white')
ax.set_xlim(lA[0]-(.5-3.*hw),rC[-1]+(.5-3.*hw))
ax.set_ylim(0.,1.)
ax.set_xticks(gc)
ax.set_xticklabels(modlist)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_title('title')

lax = fig.add_subplot(gs[1,1:3])
lax.axis('off')
lax.legend(handles=[hA,hB,hC],loc='center')

fig.tight_layout()
plt.show()
plt.close()
