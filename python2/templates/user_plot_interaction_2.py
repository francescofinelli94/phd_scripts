import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#---------------------------------------

x = np.linspace(0.,2.*np.pi)
y = np.cos(x)
z = np.sin(x)

#---------------------------------------

def on_pick(event):
    if event.mouseevent.key != "control":
        return
    global x_,y_,i_,tbu
    line = event.artist
    if len(x_[line]) == 3:
        print('selection for %s is full'%(line))
        return
    xdata,ydata = line.get_data()
    ind = event.ind
    last.append(line)
    print('x=%f\ty=%f\ton %s'%(xdata[ind[0]],ydata[ind[0]],line))
    x_[line].append(xdata[ind[0]])
    y_[line].append(ydata[ind[0]])
    i_[line].append(ind[0])
    tbu[line].set_xdata(x_[line])
    tbu[line].set_ydata(y_[line])
    return

def on_key_press(event):
    global fig,tbu,x_,y_,i_
    if event.key == "escape":
        global cid0,cid1,tbu,lines
        for line in lines:
            if len(x_[line]) < 3:
                print('selection incomplete for %s'%(line))
                return
        for line in lines:
            tbu[line].set_xdata([])
            tbu[line].set_ydata([])
        fig.canvas.mpl_disconnect(cid0)
        fig.canvas.mpl_disconnect(cid1)
        fig.canvas.stop_event_loop()
    elif event.key == "ctrl+r":
        fig.canvas.draw_idle()
    elif event.key == "ctrl+z":
        global last
        if len(last) == 0:
            return
        del x_[last[-1]][-1]
        del y_[last[-1]][-1]
        del i_[last[-1]][-1]
        tbu[last[-1]].set_xdata(x_[last[-1]])
        tbu[last[-1]].set_ydata(y_[last[-1]])
        print('removed last point from %s'%(last[-1]))
        del last[-1]
    return

#---------------------------------------

print('\nHOW-TO:')
print(' -> ctrl+mousclick on a line to select a data point')
print(' -> ctrl+z to remove the last point')
print(' -> ctrl+r to refresh the plot if stale (should not happen)')
print(' -> press Esc to exit interactive mode and generate inserts')
print(' -> choose exactly 3 points per line')
print(' -> for each line, the middel point is the one used to shift')
print('    lines in the insert (in each insert, all middel point are alignd)')
print(' -> for each line, first and last point are guidelines for insert axes limits')

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(16,8))
ax = fig.subplots(1,2)

lin0, = ax[0].plot(x,y,'r',picker=5,label='cos 0')
lin1, = ax[0].plot(x,z,'b',picker=5,label='sin 0')
ax[0].set_xlim(x[0],x[-1])
ax[0].set_ylim(-1.,2.5)
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].set_title('panel 0')
ax[0].legend(loc=2)

lin2, = ax[1].plot(x,y,'g',picker=5,label='cos 1')
lin3, = ax[1].plot(x,z,'y',picker=5,label='sin 1')
ax[1].set_xlim(x[0],x[-1])
ax[1].set_ylim(-1.,2.5)
ax[1].set_xlabel('x')
ax[1].set_ylabel('y')
ax[1].set_title('panel 1')
ax[1].legend(loc=2)

fig.tight_layout()

lines = [lin0,lin1,lin2,lin3]
lines_0 = [lin0,lin1]
lines_1 = [lin2,lin3]
last = []

print('\n')
x_ = {}
y_ = {}
i_ = {}
tbu = {}
for line in lines_0:
    x_[line] = []
    y_[line] = []
    i_[line] = []
    tbu[line], = ax[0].plot([],[],'o',color=line.get_color())

for line in lines_1:
    x_[line] = []
    y_[line] = []
    i_[line] = []
    tbu[line], = ax[1].plot([],[],'o',color=line.get_color())

cid0 = fig.canvas.mpl_connect('pick_event',on_pick)
cid1 = fig.canvas.mpl_connect('key_press_event',on_key_press)
fig.canvas.start_event_loop(timeout=-1)

xc_0 = np.max(np.array(x_.values()))
for line in lines_0:
    y_[line] = [xx for _,xx in sorted(zip(x_[line],y_[line]))]
    i_[line] = [xx for _,xx in sorted(zip(x_[line],i_[line]))]
    x_[line] = sorted(x_[line])
    if x_[line][1] < xc_0:
        xc_0 = x_[line][1]

xc_1 = np.max(np.array(x_.values()))
for line in lines_1:
    y_[line] = [xx for _,xx in sorted(zip(x_[line],y_[line]))]
    i_[line] = [xx for _,xx in sorted(zip(x_[line],i_[line]))]
    x_[line] = sorted(x_[line])
    if x_[line][1] < xc_1:
        xc_1 = x_[line][1]

Dx = {}
x_s = {}
for line in lines:
    if line in lines_0:
        Dx[line] = x_[line][1] - xc_0
    elif line in lines_1:
        Dx[line] = x_[line][1] - xc_1
    x_s[line] = np.array(x_[line]) - Dx[line]

axins0 = inset_axes(ax[0],width="40%",height="40%",loc=1)
axins1 = inset_axes(ax[1],width="40%",height="40%",loc=1)

xlim_0 = [np.min([np.min(x_s[line]) for line in lines_0]),
          np.max([np.max(x_s[line]) for line in lines_0])]
ylim_0 = [np.min([np.min(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_0]),
          np.max([np.max(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_0])]
axins0.set_xlim(xlim_0[0],xlim_0[1])
axins0.set_ylim(ylim_0[0],ylim_0[1])
axins0.set_xticks([])
axins0.set_ylabel(ax[0].get_ylabel())

xlim_1 = [np.min([np.min(x_s[line]) for line in lines_1]),
          np.max([np.max(x_s[line]) for line in lines_1])]
ylim_1 = [np.min([np.min(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_1]),
          np.max([np.max(line.get_data()[1][i_[line][0]:i_[line][2]]) for line in lines_1])]
axins1.set_xlim(xlim_1[0],xlim_1[1])
axins1.set_ylim(ylim_1[0],ylim_1[1])
axins1.set_xticks([])
axins1.set_ylabel(ax[1].get_ylabel())

for line in lines_0:
    xdata,ydata = line.get_data()
    axins0.plot(xdata-Dx[line],ydata,color=line.get_color())
    inside = np.logical_and(np.logical_and(xlim_0[0]<=xdata-Dx[line],xdata-Dx[line]<=xlim_0[1]),
                            np.logical_and(ylim_0[0]<=ydata,ydata<=ylim_0[1]))
    ax[0].plot(xdata[inside],ydata[inside],color=line.get_color(),linewidth=3.5)

for line in lines_1:
    xdata,ydata = line.get_data()
    axins1.plot(xdata-Dx[line],ydata,color=line.get_color())
    inside = np.logical_and(np.logical_and(xlim_1[0]<=xdata-Dx[line],xdata-Dx[line]<=xlim_1[1]),
                            np.logical_and(ylim_1[0]<=ydata,ydata<=ylim_1[1]))
    ax[1].plot(xdata[inside],ydata[inside],color=line.get_color(),linewidth=3.5)

plt.ioff()
plt.show(1)
plt.close(1)
