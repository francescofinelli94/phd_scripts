import os
import sys
import time

workpath = '/home/frafin/Documents'
server_workpath = '/home/finelli/Documents'
server_script = 'ssh_coord_server.py'

uname = 'finelli'
hname = 'plasmi1.df.unipi.it'

fname_c2c = 'control2client.txt'
fname_c2s = 'control2server.txt'

fname_cbuffer = 'client_buffer.txt'
fname_sbuffer = 'server_buffer.txt'

STOP_MSG = 'STOP'
WAITING_TIME = 0 #seconds

CLIENT_TAG = 'client'
SERVER_TAG = 'server'

dots = ['   ','.  ','.. ','...']

#start server
cmd = "gnome-terminal -- sh -c \"ssh -t %s@%s 'python %s/%s'\""%(uname,hname,
                                                                 server_workpath,server_script)
os.system(cmd)

#make control file
f = open('%s/%s'%(workpath,fname_c2c),'w')
f.write(CLIENT_TAG)
f.close()

run_flag = True
while run_flag:
    #ask for message
    msg = input('Write a message (write %s to exit): '%(STOP_MSG))
    
    #write message to server
    f = open('%s/%s'%(workpath,fname_cbuffer),'w')
    f.write(msg)
    f.close()
    os.system('scp -q %s/%s %s@%s:%s/%s'%(workpath,fname_cbuffer,uname,hname,
                                          server_workpath,fname_sbuffer))

    #give control to server
    f = open('%s/%s'%(workpath,fname_c2c),'w')
    f.write(SERVER_TAG)
    f.close()
    os.system('scp -q %s/%s %s@%s:%s/%s'%(workpath,fname_c2c,uname,hname,server_workpath,fname_c2s))

    if msg == STOP_MSG:
        print('Exiting...')
        run_flag = False
        continue

    #wait for get control back
    wait_flag = run_flag
    cnt = 0
    while wait_flag:
        os.system('scp -q %s@%s:%s/%s %s/%s'%(uname,hname,server_workpath,fname_c2s,workpath,fname_c2c))
        f = open('%s/%s'%(workpath,fname_c2c),'r')
        tag = f.readline()
        f.close()
        if tag == CLIENT_TAG:
            wait_flag = False
        else:
            print('waiting for control%s'%(dots[cnt%4]),end='\r')
            sys.stdout.flush()
            cnt += 1
            time.sleep(WAITING_TIME)
    print('')

    #read file content
    os.system('scp -q %s@%s:%s/%s %s/%s'%(uname,hname,server_workpath,fname_sbuffer,
                                                      workpath,fname_cbuffer))
    f = open('%s/%s'%(workpath,fname_cbuffer))
    r = float(f.readline())
    f.close()
    print('Random number is: %f'%(r))

    print('')

print('End of program.')
