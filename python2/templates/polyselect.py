#--------------------------------
#imports
#--------------------------------
import numpy as np
import matplotlib.pyplot as plt

#--------------------------------
#interactive functions
#--------------------------------
def on_click(event):
    global selection_flag,focus,sel,ax
    if not selection_flag:
        return
    if focus == None:
        return
    global ix, iy
    ix, iy = event.xdata, event.ydata
    sel[focus][0].append(ix)
    sel[focus][1].append(iy)
    poly_draw(sel[focus],ax)
    return

def on_key_press(event):
    global fig,sel,focus,ax
    if event.key == "control":
        global selection_flag
        selection_flag = True
    elif event.key == "escape":
        global cid0, cid1, cid2
        fig.canvas.mpl_disconnect(cid0)
        fig.canvas.mpl_disconnect(cid1)
        fig.canvas.mpl_disconnect(cid2)
        print('\nNow you can close the plot.')
        fig.canvas.stop_event_loop()
    elif event.key == "ctrl+z":
        if focus == None:
            return
        if len(sel[focus][0]) == 0:
            return
        del sel[focus][0][-1]
        del sel[focus][1][-1]
        poly_draw(sel[focus],ax)
    elif event.key == "ctrl+r":
        fig.canvas.draw_idle()
    elif event.key == "ctrl+n":
        sname = input('\nInsert new selection name: ')
        sel[sname] = [[],[],None,None]
        focus = sname
        print('Focus on %s.'%focus)
    elif event.key == "ctrl+c":
        if len(sel.keys()) == 0:
            return
        print('\nAviable selections are:')
        for i,sn in enumerate(sel.keys()):
            print('%s \t->\t%d'%(sn,i))
        i = input('Choose one to focus on: ')
        if (i < 0) or (i > len(sel.keys())-1):
            focus = None
            print('Invalid choice, focus set to None.')
            return
        focus = sel.keys()[i]
        print('Focus on %s.'%(focus))
    elif event.key == "ctrl+d":
        if len(sel.keys()) == 0:
            return
        print('\nAviable selections are:')
        for i,sn in enumerate(sel.keys()):
            print('%s \t->\t%d'%(sn,i))
        i = input('Choose one to delete: ')
        if (i < 0) or (i > len(sel.keys())-1):
            focus = None
            print('Invalid choice, focus set to None.')
            return
        if sel[sel.keys()[i]][2] != None:
            sel[sel.keys()[i]][2].remove()
        del sel[sel.keys()[i]]
        focus = None
        print('Focus on None.')
    elif event.key == "ctrl+m":
        how_to()
    elif event.key == "ctrl+F":
        if focus == None:
            print('\nFocus on None.')
        else:
            print('\nFocus on %s.'%(focus))
    return

def on_key_release(event):
    if event.key == "control":
        global selection_flag
        selection_flag = False
    return

#--------------------------------
#other functions
#--------------------------------
#how to
def how_to():
    print('\nHOW TO...')
    print(' - in order for key/mouse commands to work the plot window must be selected;')
    print(' - ctrl+m to re-print this commands list;')
    print(' - ctrl+n to create a new selection,')
    print(' - - you have to name it via command line')
    print(' - - and focus will be put on it;')
    print(' - ctrl+c to change the selection on focus,')
    print(' - - a number should be given via command line;')
    print(' - ctrl+mouseclick to add a point to the focused selection;')
    print(' - ctrl+z to remove the last point in the focused selection;')
    print(' - ctrl+d to delete a selection,')
    print(' - - a number should be given via command line,')
    print(' - - focus will be set on None;')
    print(' - ctrl+F to show current focus;')
    print(' - in case of input error, focus will be set on None.')
    print(' - ctrl+r to re-draw the plot, if stale (should not happen);')
    print(' - escape to close the interactive session,')
    print(' - - the plot will remain opened in show mode.')
    return

#draw function
def poly_draw(sel_,ax_):
    if sel_[3] == None:
        if len(sel_[0]) == 0:
            return
        if len(sel_[0]) == 1:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'o')
        elif len(sel_[0]) == 2:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'-')
        else:
            sel_[2], = ax_.fill(sel_[0],sel_[1],alpha=0.5)
        sel_[3] = sel_[2].get_color()
    else:
        if sel_[2] != None:
                sel_[2].remove()
        if len(sel_[0]) == 0:
            sel_[2] = None
        elif len(sel_[0]) == 1:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'o',color=sel_[3])
        elif len(sel_[0]) == 2:
            sel_[2], = ax_.plot(sel_[0],sel_[1],'-',color=sel_[3])
        else:
            sel_[2], = ax_.fill(sel_[0],sel_[1],alpha=0.5,color=sel_[3])
    return

#--------------------------------
#interactive plotting
#--------------------------------
x = np.linspace(0.,1.,128*4)
y = np.linspace(0.,1.,128*4)

how_to()

sel = {}
focus = None
selection_flag = False

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(8,8))
ax = fig.subplots(1,1)
ax.set_xlim(x[0],x[-1])
ax.set_ylim(y[0],y[-1])
ax.tick_params(left=False,labelleft=False,bottom=False,labelbottom=False)
cid0 = fig.canvas.mpl_connect('button_press_event',on_click)
cid1 = fig.canvas.mpl_connect('key_release_event',on_key_release)
cid2 = fig.canvas.mpl_connect('key_press_event',on_key_press)
fig.canvas.start_event_loop(timeout=-1)

plt.ioff()
plt.show(1)
plt.close(1)

#--------------------------------
#binary masks
#--------------------------------
X,Y = np.meshgrid(x,y)
X,Y = X.flatten(),Y.flatten()
points = np.vstack((X,Y)).T
nx = len(x)
ny = len(y)

masks = {}
plt.close()
for k in sel.keys():
    masks[k] = ( sel[k][2].get_path().contains_points(points).reshape(ny,nx).T,
                 sel[k][3] )
    plt.contourf(x,y,masks[k][0].T,1,colors=['white',masks[k][1]],alpha=0.5)
    plt.plot([],[],color=masks[k][1],label=k)

plt.title('Selections')
plt.legend()
plt.xlim(x[0],x[-1])
plt.ylim(y[0],y[-1])
plt.tick_params(left=False,labelleft=False,bottom=False,labelbottom=False)
plt.show()
plt.close()

print('\nMasks generated!')
