# importing stuff
import multiprocessing as mp
import time
import os
import sys
import numpy as np

# task of random length
def task(i,t1,t2,t3):
    mypid = os.getpid() # get the process ID of the process (child/worker) running this task
    time.sleep(t1)
    print('%d: Task #%d 1/3 done!'%(mypid,i))
    time.sleep(t2)                         
    print('%d: Task #%d 2/3 done!'%(mypid,i))
    time.sleep(t3)                         
    print('%d: Task #%d 3/3 done!'%(mypid,i))
    return

# write stuff w/o concurrency problems
def log(logstream, lock, msg):
    msg = "%d: %s"%(os.getpid(),msg)
    with lock:
        logstream.write(msg + '\n')
        logstream.flush()
    return

# same task but w/ log instead of print
def mp_task(i,t1,t2,t3,lock):
    time.sleep(t1)
    log(sys.stdout,lock,'Task #%d 1/3 done!'%i)
    time.sleep(t2)
    log(sys.stdout,lock,'Task #%d 2/3 done!'%i)
    time.sleep(t3)
    log(sys.stdout,lock,'Task #%d 3/3 done!'%i)
    return

# parameters
ntask = 10
t_mult = 1.

WTIME = 0.001
ncpu = os.cpu_count() # sometimes doesn't work!!!! replace whit a positive integer 
                      # (in a shell, try lscpu)

# get parent PID
pid = os.getpid() # get the process ID of the main process (parent/manager???)

# serial processing
print('\n%d: Serial processing'%pid)
t0 = time.time()
for i in range(10):
    t1,t2,t3 = np.random.random(3)*t_mult
    task(i,t1,t2,t3)
t1 = time.time()
print('%d: Elapsed time: %.2f'%(pid,t1-t0))

# Parallel processing
print('\n%d: Parallel processing'%pid)
t0 = time.time()
stdoutlock = mp.Lock() # create a 'lock' to prevent concurrency during outputs
workers = []
for i in range(ntask):
    t1,t2,t3 = np.random.random(3)*t_mult
    workers.append(mp.Process(target=mp_task,args=(i,t1,t2,t3,stdoutlock))) # give a task to each worker

i = 0
while i < ntask:
    if sum([worker.is_alive() for worker in workers]) == ncpu: # up to ncpu active workers at time
        time.sleep(WTIME)
        continue
    workers[i].start() # the worker starts its task
    i += 1


for worker in workers:
    worker.join() # it is a barrier, waits for the worker

t1 = time.time()
log(sys.stdout,stdoutlock,'Elapsed time: %.2f'%(t1-t0))
