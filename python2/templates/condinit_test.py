#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill follow island in windlike plot.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

run_name = run.meta['run_name']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#--------------------------------------------------------
#getting data and computing stuff
#-------------------------------------
ind = 0

_,B = run.get_EB(ind)

n_p,_ = run.get_Ion(ind)
#if w_ele:
#    if code_name == 'HVM':
#        n_e = n_p
#    elif code_name == 'iPIC':
#        n_e,_ = run.get_Ion(ind,qom=qom_e)
#    else:
#        print('\nWhat code is it?\n')

#initial fields
if code_name == 'HVM':
    Bx0 = 1.
    Bz0 = 0.25
    n_ = 1.
elif code_name == 'iPIC':
    Bx0 = 0.01
    Bz0 = 0.25*0.01
    n_ = 0.25/np.pi
beta = 1.
A_1 = - Bx0
A_2 = - A_1
Ti = 0.5*beta
n1 = A_1*A_1/(2.*Ti*(1.+teti))/Bx0/Bx0#MODIFIED!!!
nb0 = 1.

X,Y = np.meshgrid(x,y)
Y = Y.T
X = X.T
Bxeq = ( - A_1*np.tanh( (Y - y1)/L1 )
         - A_2*np.tanh( (Y - y2)/L2 )
         - np.sign(A_1)*(A_1 - A_2)*0.5 )#MODIFIED!!!
neq = ( ( n1/( np.cosh( (Y - y1)/L1 )**2 ) + nb0 )*n_ ).reshape(nx,ny,1)#MODIFIED!!!

Beq = np.array([Bxeq,np.zeros((nx,ny),dtype=type(B[0,0,0,0])),
               np.full((nx,ny),Bz0,dtype=type(B[0,0,0,0]))]).reshape(3,nx,ny,1)#MODIFIED!!!
del Bxeq

#plot 2D
fig,ax = plt.subplots(2,2,figsize=(16,12),sharex=True,sharey=True)

im00 = ax[0,0].pcolormesh(X[:,:ny//2],Y[:,:ny//2],(B[0,:,:ny//2,0]-Beq[0,:,:ny//2,0]))
plt.colorbar(im00,ax=ax[0,0])
ax[0,0].set_title('$B_x(t=0)-B_{x,eq}$')
#ax[0,0].set_xlabel('x')
ax[0,0].set_ylabel('y')

im01 = ax[0,1].pcolormesh(X[:,:ny//2],Y[:,:ny//2],(B[1,:,:ny//2,0]-Beq[1,:,:ny//2,0]))
plt.colorbar(im01,ax=ax[0,1])
ax[0,1].set_title('$B_y(t=0)-B_{y,eq}$')
#ax[0,1].set_xlabel('x')
#ax[0,1].set_ylabel('y')

im10 = ax[1,0].pcolormesh(X[:,:ny//2],Y[:,:ny//2],(B[2,:,:ny//2,0]-Beq[2,:,:ny//2,0]))
plt.colorbar(im10,ax=ax[1,0])
ax[1,0].set_title('$B_z(t=0)-B_{z,eq}$')
ax[1,0].set_xlabel('x')
ax[1,0].set_ylabel('y')

im11 = ax[1,1].pcolormesh(X[:,:ny//2],Y[:,:ny//2],(n_p[:,:ny//2,0]-neq[:,:ny//2,0]))
plt.colorbar(im11,ax=ax[1,1])
ax[1,1].set_title('$n(t=0)-n_{eq}$')
ax[1,1].set_xlabel('x')
#ax[1,1].set_ylabel('y')

plt.suptitle(run_label+' equilibrium check')
plt.show()

#plot 1D
fig,ax = plt.subplots(2,2,figsize=(16,12),sharex=True,sharey=False)

ax[0,0].plot(y[:ny//2],B[0,0,:ny//2,0],label='$B_x(t=0,x=0)$')
ax[0,0].plot(y[:ny//2],Beq[0,0,:ny//2,0],label='$B_{x,eq}(x=0)$')
ax[0,0].legend()
#ax[0,0].set_xlabel('y')

ax[0,1].plot(y[:ny//2],B[1,0,:ny//2,0],label='$B_y(t=0,x=0)$')
ax[0,1].plot(y[:ny//2],Beq[1,0,:ny//2,0],label='$B_{y,eq}(x=0)$')
ax[0,1].legend()
#ax[0,1].set_xlabel('y')

ax[1,0].plot(y[:ny//2],B[2,0,:ny//2,0],label='$B_z(t=0,x=0)$')
ax[1,0].plot(y[:ny//2],Beq[2,0,:ny//2,0],label='$B_{z,eq}(x=0)$')
ax[1,0].legend()
ax[1,0].set_xlabel('y')

ax[1,1].plot(y[:ny//2],n_p[0,:ny//2,0],label='$n(t=0,x=0)$')
ax[1,1].plot(y[:ny//2],neq[0,:ny//2,0],label='$n_{eq}(x=0)$')
ax[1,1].legend()
ax[1,1].set_xlabel('y')

plt.suptitle(run_label+' equilibrium check - cuts')
plt.show()

