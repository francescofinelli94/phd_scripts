import numpy as np
from numba import njit
import matplotlib as mpl
import matplotlib.pyplot as plt

#------------
@njit
def hist2d_njit(tracks, bins, ranges):
    H = np.zeros((bins[0], bins[1]), dtype=np.uint64)
    delta = 1/((ranges[:,1] - ranges[:,0]) / bins)

    for t in range(tracks.shape[1]):
        i = (tracks[0,t] - ranges[0,0]) * delta[0]
        j = (tracks[1,t] - ranges[1,0]) * delta[1]
        if 0 <= i < bins[0] and 0 <= j < bins[1]:
            H[int(i),int(j)] += 1

    return H

@njit
def histABC_njit(tracks, bins, ranges):
    H = np.zeros((bins[0], bins[1]), dtype=np.uint64)
    S = np.zeros((bins[0], bins[1]), dtype=np.float64)
    delta = 1/((ranges[:,1] - ranges[:,0]) / bins)

    for t in range(tracks.shape[1]):
        i = (tracks[0,t] - ranges[0,0]) * delta[0]
        j = (tracks[1,t] - ranges[1,0]) * delta[1]
        if 0 <= i < bins[0] and 0 <= j < bins[1]:
            H[int(i),int(j)] += 1
            S[int(i),int(j)] += tracks[2,t]

    return np.divide(S,H.astype(np.float64))

@njit
def histABCmax_njit(tracks, bins, ranges):
    S = np.zeros((bins[0], bins[1]), dtype=np.float64)
    delta = 1/((ranges[:,1] - ranges[:,0]) / bins)

    for t in range(tracks.shape[1]):
        i = (tracks[0,t] - ranges[0,0]) * delta[0]
        j = (tracks[1,t] - ranges[1,0]) * delta[1]
        if 0 <= i < bins[0] and 0 <= j < bins[1]:
            if tracks[2,t] > S[int(i),int(j)]:
                S[int(i),int(j)] = tracks[2,t]

    return S

def hist2d_wrap(xdata,ydata,zdata=None,nbins_xy=None,x_range=None,y_range=None,mode='standard'):
    """
    DESCRIPTION:
    If mode='standard', this function will build a 2D histogram from quantities xdata and ydata
    (similar to np.histogram2D, but implemented with Numba).
    If mode='ABC' or mode='ABCmax', this function will build something similar to a 2D histogram,
    but the bins contnent is not the count of the entries in each bin. Instead, the bins contnent
    is the average (if 'ABC') or max (if 'ABCmax') of zdata computed over the entries for each bin.

    INPUTS:
    xdata    -> numpy.ndarray - some data
    ydata    -> numpy.ndarray - same size of xdata - some data
    zdata    -> numpy.ndarray - same size of xdata - optional/ignored if mode='standard' - some data
    nbins_xy -> numpy.ndarray, list, or tuple - 2 elements: numer of bins for the 'x' and 'y' dimensions
                optional: default in int(np.sqrt(float(xdata.size)/32.)) bins foe each direction
    x_range  -> numpy.ndarray, list, or tuple - 2 elements: range of xdata to be considered
                - optional: default are the extremes of xdata
    y_range  -> numpy.ndarray, list, or tuple - 2 elements: range of ydata to be considered
                - optional: default are the extremes of ydata
    mode     -> string - 'standard', 'ABC', or 'ABCmax' - optional: default is 'standard'

    OUTPUTS:
    h     -> numpy.ndarray with shape tuple(nbins_xy) - the histogram array
    xbins -> numpy.ndarray with shape (nbins_xy[0]+1) - bins edge for x direction
    ybins -> numpy.ndarray with shape (nbins_xy[1]+1) - bins edge for y direction

    ERROR OUTPUTS:
    -1,-1,-1

    INFO:
    Written by Francesco Finelli (francesco.finelli@phd.unipi.it)
    Date: 24 Oct. 2020
    Acknowledgment: https://iscinumpy.gitlab.io/post/histogram-speeds-in-python/
    """
#
    if type(xdata) != np.ndarray:
        print('\nERROR: xdata type ('+str(type(xdata))+') is not a numpy.ndarray.\n')
        return -1,-1,-1
    if type(ydata) != np.ndarray:
        print('\nERROR: ydata type ('+str(type(ydata))+') is not a numpy.ndarray.\n')
        return -1,-1,-1
#
    if xdata.size != ydata.size:
        print('\nERROR: xdata (%d) and ydata (%d) have different size.\n'%(xdata.size,ydata.size))
        return -1,-1,-1
    if xdata.shape != ydata.shape:
        print('\nWARNING: xdata '+str(xdata.shape)+' and ydata '+
               str(ydata.shape)+' have different shape.\n')
#
    if nbins_xy == None:
        nbins = int(np.sqrt(float(xdata.size)/32.))
        nbins_xy = [nbins,nbins]
    else:
        try:
            if len(nbins_xy) != 2:
                print('\nERROR: nbins_xy should have lenght 2, not %d.\n'%len(nbins_xy))
                return -1,-1,-1
        except:
            print('\nERROR: ill-defined nbins_xy ('+str(type(nbins_xy))+').\n')
            return -1,-1,-1
#
    if x_range == None:
        x_range = [np.min(xdata),np.max(xdata)]
    else:
        try:
            if len(x_range) != 2:
                print('\nERROR: x_range should have lenght 2, not %d.\n'%len(x_range))
                return -1,-1,-1
        except:
            print('\nERROR: ill-defined x_range ('+str(type(x_range))+').\n')
            return -1,-1,-1
#
    if y_range == None:
        y_range = [np.min(ydata),np.max(ydata)]
    else:
        try:
            if len(y_range) != 2:
                print('\nERROR: y_range should have lenght 2, not %d.\n'%len(y_range))
                return -1,-1,-1
        except:
            print('\nERROR: ill-defined y_range ('+str(type(y_range))+').\n')
            return -1,-1,-1
    if type(mode) != str:
        print('\nERROR: mode shoud be a string.\n')
        return -1,-1,-1
#
    if (mode == 'ABC') or (mode == 'ABCmax'):
        if type(zdata) != np.ndarray:
            print('\nERROR: zdata type ('+str(type(zdata))+') is not a numpy.ndarray.\n')
            return -1,-1,-1
        if (zdata.size != xdata.size) or (zdata.size != ydata.size):
            print('\nERROR: zdata (%d) has different size from '%(zdata.size)+
                  'xdata (%d) and/or ydata (%d).\n'%(xdata.size,ydata.size))
            return -1,-1,-1
        if (zdata.shape != xdata.shape) or (zdata.shape != ydata.shape):
            print('\nWARNING: zdata '+str(zdata.shape)+' has different shape from xdata '+
                  str(xdata.shape)+' and/or ydata '+str(ydata.shape)+'.\n')
#
    xbins = np.linspace(x_range[0],x_range[1],nbins_xy[0]+1)
    ybins = np.linspace(y_range[0],y_range[1],nbins_xy[1]+1)
#
    if mode == 'standard':
        h = hist2d_njit(np.array([xdata.flatten(),ydata.flatten()],dtype=np.float64),
                        np.array(nbins_xy,dtype=np.int64),
                        np.array([x_range,y_range],dtype=np.float64))
    elif mode == 'ABC':
        h = histABC_njit(np.array([xdata.flatten(),ydata.flatten(),zdata.flatten()],dtype=np.float64),
                         np.array(nbins_xy,dtype=np.int64),
                         np.array([x_range,y_range],dtype=np.float64))
    elif mode == 'ABCmax':
        h = histABCmax_njit(np.array([xdata.flatten(),ydata.flatten(),zdata.flatten()],dtype=np.float64),
                            np.array(nbins_xy,dtype=np.int64),
                            np.array([x_range,y_range],dtype=np.float64))
    else:
        print('\nERROR: mode %s is unknown.'%mode)
        return -1,-1,-1
#
    return h,xbins,ybins

#------------
nx = 1024
ny = 512
nbins = [100,100]

m1 = 1.
s1 = 1.
m2 = 0.
s2 = 2.
A_range = [-9.,9.]
B_range = [-9.,9.]

#------------
A = np.random.normal(loc=m1,scale=s1,size=nx*ny).reshape(nx,ny)
B = np.random.normal(loc=m2,scale=s2,size=nx*ny).reshape(nx,ny)
C = np.random.rand(nx,ny)

#------------
fig,ax = plt.subplots(1,3,figsize=(20,6))

h,xbins,ybins = hist2d_wrap(A,B,nbins_xy=nbins)
Xb,Yb = np.meshgrid(xbins,ybins)
im = ax[0].pcolormesh(Xb,Yb,h.T,norm=mpl.colors.LogNorm())
plt.colorbar(im,ax=ax[0])
ax[0].set_xlabel('A')
ax[0].set_ylabel('B')
ax[0].set_xlim(A_range[0],A_range[1])
ax[0].set_ylim(B_range[0],B_range[1])
ax[0].set_title('2D histogram')

h,_,_ = hist2d_wrap(A,B,C,nbins_xy=nbins,mode='ABC')
im = ax[1].pcolormesh(Xb,Yb,h.T,cmap='RdBu')
plt.colorbar(im,ax=ax[1])
ax[1].set_xlabel('A')
ax[1].set_ylabel('B')
ax[1].set_xlim(A_range[0],A_range[1])
ax[1].set_ylim(B_range[0],B_range[1])
ax[1].set_title('ABC histogram')

h,_,_ = hist2d_wrap(A,B,C,nbins_xy=nbins,mode='ABCmax')
im = ax[2].pcolormesh(Xb,Yb,h.T,cmap='Blues')
plt.colorbar(im,ax=ax[2])
ax[2].set_xlabel('A')
ax[2].set_ylabel('B')
ax[2].set_xlim(A_range[0],A_range[1])
ax[2].set_ylim(B_range[0],B_range[1])
ax[2].set_title('ABC-max histogram')


plt.tight_layout()
plt.show()
plt.close()
