import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec

font = 17
mpl.rc('text',usetex=True)
mpl.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
mpl.rc('font',family ='serif',size=font)

def rainbow_text(x, y, strings, colors, orientation='horizontal',
                 ax=None, **kwargs):
    """
    Take a list of *strings* and *colors* and place them next to each
    other, with text strings[i] being shown in colors[i].

    Parameters
    ----------
    x, y : float
        Text position in data coordinates.
    strings : list of str
        The strings to draw.
    colors : list of color
        The colors to use.
    orientation : {'horizontal', 'vertical'}
    ax : Axes, optional
        The Axes to draw into. If None, the current axes will be used.
    **kwargs
        All other keyword arguments are passed to plt.text(), so you can
        set the font size, family, etc.
    """
    if ax is None:
        ax = plt.gca()
    t = ax.transData
    canvas = ax.figure.canvas

    assert orientation in ['horizontal', 'vertical']
    if orientation == 'vertical':
        kwargs.update(rotation=90, verticalalignment='bottom')

    for s, c in zip(strings, colors):
        text = ax.text(x, y, s, color=c, transform=t, **kwargs)

        # Need to draw to update the text position.
        text.draw(canvas.get_renderer())
        ex = text.get_window_extent()
        if orientation == 'horizontal':
            t = mpl.transforms.offset_copy(
                text.get_transform(), x=ex.width, units='dots')
        else:
            t = mpl.transforms.offset_copy(
                text.get_transform(), y=ex.height, units='dots')

W1 = 4 #length first box
H1 = 4 #height first box
W2 = 3 #length second box
H2 = 1 #height second box

w1 = 0.14 #width spacing grids

l1 = 0.05 #left margin
r1 = 0.98 #right margin
b1 = 0.08 #bottom margin
t1 = 0.95 #top margin

m1 = 1.6 #multiplier

plt.close()
fig = plt.figure(figsize=(m1*(W1+W2+H2)*(1.+w1*0.5)/(r1-l1),m1*H1/(t1-b1)),
                 constrained_layout=False)
gs = GridSpec(1,2,figure=fig,width_ratios=[W1,W2+H2],
              wspace=w1,left=l1,right=r1,bottom=b1,top=t1)
gs1 = SubSpec(2,2,gs[1],width_ratios=[W2,H2],height_ratios=[H2,H1-H2],
              wspace=0.,hspace=0.)

ax0 = fig.add_subplot(gs[0])
ax0.set_title('title 0')
ax0.set_xlabel('xlabel 0')
ax0.set_ylabel('ylabel 0')
ax0.tick_params(bottom=True,top=True,left=True,right=True,
                direction='inout',labelbottom=True,labeltop=False,
                labelleft=True,labelright=False)

ax1 = fig.add_subplot(gs1[0,0])
ax1.set_title('title 0 0')
ax1.set_ylabel('$|J|,\;\int_\ell\mathcal{I}$ (norm.)')
ax1.set_xlim(0.,2750.)
ax1.set_ylim(0.,1.)
rainbow_text(-0.14*2750.,0.01,['$|J|$',',','l','$\int_\ell\mathcal{I}$ (norm.)'],
             ['green','black','white','blue'],orientation='vertical',
             ax=ax1,size=font)
ax1.tick_params(bottom=True,top=True,left=True,right=True,
                direction='inout',labelbottom=False,labeltop=False,
                labelleft=True,labelright=False)

ax2 = fig.add_subplot(gs1[1,0])
ax2.set_xlabel('xlabel 1 0')
ax2.set_ylabel('ylabel 1 0')
ax2.tick_params(bottom=True,top=True,left=True,right=True,
                direction='inout',labelbottom=True,labeltop=False,
                labelleft=True,labelright=False)
xticks = ax2.xaxis.get_major_ticks()
xticks[-1].label1.set_visible(False)
yticks = ax2.yaxis.get_major_ticks()
yticks[-1].label1.set_visible(False)

ax3 = fig.add_subplot(gs1[1,1])
ax3.set_xlabel('xlabel 1 1')
ax3.tick_params(bottom=True,top=True,left=True,right=True,
                direction='inout',labelbottom=True,labeltop=False,
                labelleft=False,labelright=False)

plt.show()
