#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/work2/finelli/Documents_SAV/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
import thresholds as th

#latex fonts
font = 20#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Init
#------
if len(sys.argv) < 2:
    print('ERROR: 1 arguments are required!')
    print('FILE\t\tthe input file')
    sys.exit()

run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
#ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'rho_in_CS'
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']
mime = run.meta['mime']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = max(int((y1-L1)/run.meta['dy']), 0)
cut2 = min(int((y1+L1)/run.meta['dy']), ny-1)
cut3 = max(int((y2-L2)/run.meta['dy']), 0)
cut4 = min(int((y2+L2)/run.meta['dy']), ny-1)

#---> loop over times <---
t = []
rho = {'p':{}, 'e':{}}
for k in rho:
    rho[k] = {'m':[], 's':[]}

ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
print " ",
for ind in ind_range:
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    _,B = run.get_EB(ind)
#
    Pp = run.get_Press(ind)
    _, pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
#
    #n
    n,_ = run.get_Ion(ind)
#
    if w_ele:
        if code_name == 'HVM':
            _, pperpe = run.get_Te(ind)
            pperpe *= n
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            _, pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
#
    #beta
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    del B
    bperpp = 2.*np.divide(pperpp,B2)
    if w_ele:
        bperpe = 2.*np.divide(pperpe,B2)
        del pperpe
    del B2,pperpp
    if code_name == 'iPIC':
        bperpp = 4.*np.pi*bperpp
        if w_ele:
            bperpe = 4.*np.pi*bperpe
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #rhop
    rhop = np.sqrt(.5*bperpp/n)[:, cut1:cut2+1, :] # in code-dp
    rhoe = np.sqrt(.5*bperpe/n/mime)[:, cut1:cut2+1, :] # in code-dp
#
    #save
    rho['p']['m'].append(np.mean(rhop))
    rho['p']['s'].append(np.std(rhop))
    rho['e']['m'].append(np.mean(rhoe))
    rho['e']['s'].append(np.std(rhoe))
    t.append(time[ind])
    del rhop, rhoe
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
#
t = np.array(t)
for k in rho:
    for kk in rho[k]:
        rho[k][kk] = np.array(rho[k][kk])

#plot
plt.close('all')
plt.subplot(211)
plt.plot(t, rho['p']['m'], '-k',label='$<\\rho_{\mathrm{p}}>$')
plt.plot(t, rho['p']['m']+rho['p']['s'], '--k', label='$\pm 1 \sigma$')
plt.plot(t, rho['p']['m']-rho['p']['s'], '--k')
plt.xlabel('$t$ $[\Omega_{\mathrm(p)}^{-1}]$')
plt.ylabel('$\\rho_{\mathrm{p}}$ $[d_{\mathrm{p}}]$')
plt.subplot(212)
plt.plot(t, rho['e']['m'], '-k',label='$<\\rho_{\mathrm{e}}>$')
plt.plot(t, rho['e']['m']+rho['e']['s'], '--k', label='$\pm 1 \sigma$')
plt.plot(t, rho['e']['m']-rho['e']['s'], '--k')
plt.xlabel('$t$ $[\Omega_{\mathrm(p)}^{-1}]$')
plt.ylabel('$\\rho_{\mathrm{e}}$ $[d_{\mathrm{p}}]$')
plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'rho_in_CS_'+run_name+'_'+
            str(ind1)+'-'+str(ind2)+'.png')
plt.close()
