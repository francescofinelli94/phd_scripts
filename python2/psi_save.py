#-----------------------------------------------------
#importing stuff
#---------------
import numpy as np
import work_lib as wl
import gc
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill save Psi fields on file\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

wl.Can_I()

run_name = run.meta['run_name']
opath = run.meta['opath']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']

out_dir = 'Psi_field/'+run_name
ml.create_path(opath+'/'+out_dir)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------

    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    del B

    f = open(opath+'/'+out_dir+'/Psi_'+run_name+'_'+str(ind)+'.dat','w')
    f.write('{:d}\t{:d}\t{:d}\t{:f}\n'.format(nx,ny,nz,time[ind]))
    for iy in range(ny):
        for ix in range(nx):
            f.write('{:.10e}\n'.format(Psi[ix,iy]))
    f.close()

    del Psi

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

