#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import collections

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import work_lib as wl
#sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot energy budget distribution\n')

#------------------------------------------------------
#Init
#----
flg_ipic_comp = False
if flg_ipic_comp:
    input_files = [
                   #'DH_run3_data0.dat',
                   #'DH_run5_data0.dat',
                   ##'DH_run6_data0.dat',
                   #'DH_run7_data0.dat',
                   #'DH_run8.dat',
                   'DH_run10.dat',
                   'DH_run11_data0.dat'
                  ]
else:
    #input_files = [
    #               'HVM_2d_DH.dat',
    #               'LF_2d_DH.dat',
    #               'DH_run11_data0.dat'
    #              ]
    input_files = [
                   'LF_3d_DH.dat',
                  ]
master_in = 0
save_flag = True  # remove mpol.use('Agg') to show plots

F = {'E':{},'M':{}}
K = {'p':{},'e':{},'parlp':{},'perpp':{},'parle':{},'perpe':{}}
I = {'p':{},'e':{},'parlp':{},'perpp':{},'parle':{},'perpe':{}}
Etot = {}
D = {'p':{},'e':{}}
E0 = {}
t = {}

tstart = {}
istart = {}
run_name_vec = []
run_labels = collections.OrderedDict()
for ii, i_f in enumerate(input_files):
    run, tr, _ = wl.Init(i_f)
    ind1, ind2, ind_step = tr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    code_name = run.meta['code_name']
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    m_p = 1.
    m_e = m_p/run.meta['mime']
    if code_name == 'iPIC':
        if flg_ipic_comp:
            #run_label = 'iPIC%s'%(run_name.split('run')[1].split('_')[0])
            run_label = run_name.split('.')[0].split('_')[1]
        else:
            run_label = 'iPIC'
        qom_e = run.meta['msQOM'][0]
        tstart[run_name] = 2.5
    elif code_name == 'HVM':
        if w_ele:
            run_label = 'HVLF'
        else:
            run_label = 'HVM'
        tstart[run_name] = 2.5
    else:
        run_label = 'unknown'
    run_labels[run_name] = run_label
    if ii == master_in:
        opath = run.meta['opath']
        if flg_ipic_comp:
            out_dir = 'energy_budget_fullbox_v4_ipiccomp'
        else:
            out_dir = 'energy_budget_fullbox_v4'
        ml.create_path(opath+'/'+out_dir)
        calc = fibo('calc')
#
    for k in F.keys():
        F[k][run_name] = []
    for k in K.keys():
        K[k][run_name] = []
    for k in I.keys():
        I[k][run_name] = []
    for k in D.keys():
        D[k][run_name] = []
    Etot[run_name] = []
    t[run_name] = []
# 
    #---> loop over times <---
    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        #get data
        #magnetic fields and co.
        if code_name == 'iPIC':
            E, B = run.get_EB(ind)
            E2 = E[0]*E[0] + E[1]*E[1] + E[2]*E[2]
            del E
        elif code_name == 'HVM':
            _,B = run.get_EB(ind)
            E2 = np.array([0.])
        B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
        Bn = np.sqrt(B2)
        b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
        for i in range(3):
            b[i] = np.divide(B[i], Bn)
        del Bn
#
        #densities and currents
        n_p, u_p = run.get_Ion(ind)
        if code_name == 'HVM':
            calc.meta = run.meta
            cx, cy, cz = calc.calc_curl(B[0], B[1], B[2])
            J = np.array([cx, cy, cz]) # J = rot(B)
            del cx, cy, cz
            n_e = n_p.copy() # n_e = n_p = n (no charge separation)
            u_e = np.empty(u_p.shape,dtype=type(u_p[0,0,0,0])) # u_e = u_p - J/n
            u_e[0] = u_p[0] - np.divide(J[0], n_p)
            u_e[1] = u_p[1] - np.divide(J[1], n_p)
            u_e[2] = u_p[2] - np.divide(J[2], n_p)
            del J
        elif code_name == 'iPIC':
            n_e,u_e = run.get_Ion(ind,qom=qom_e)
        u_p2 = u_p[0]*u_p[0] + u_p[1]*u_p[1] + u_p[2]*u_p[2]
        u_e2 = u_e[0]*u_e[0] + u_e[1]*u_e[1] + u_e[2]*u_e[2]
        u_parlp2 = (u_p[0]*b[0] + u_p[1]*b[1] + u_p[2]*b[2])**2
        u_perpp2 = u_p2 - u_parlp2
        u_parle2 = (u_e[0]*b[0] + u_e[1]*b[1] + u_e[2]*b[2])**2
        u_perpe2 = u_e2 - u_parle2
        del u_p, u_e, b
#
        #Pressures
        Pp = run.get_Press(ind)
        pparlp, pperpp = ml.proj_tens_on_vec(Pp,B) # pparlp = Pp:bb
#                                                 # pperpp = Pp:(id - bb)/2
        Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3. # Pisop = Tr(Pp)/3
        del Pp
        if not w_ele:
            Pisoe = n_e*run.meta['beta']*0.5*run.meta['teti'] # Pisoe = n_e * T_e,0
            pparle = Pisoe # ppalre = Pisoe
            pperpe = pparle # pperpe = Pisoe
        else:
            if code_name == 'HVM':
                tparle, tperpe = run.get_Te(ind)
                pparle = tparle*n_e # pparle = n_e*tparle
                pperpe = tperpe*n_e # pperpe = n_e*tperpe
                del tparle, tperpe
                Pisoe = (pparle + 2.*pperpe)/3. # Pisoe  = (pparle + 2*pperpe)/3
            elif code_name == 'iPIC':
                Pe = run.get_Press(ind,qom=qom_e)
                pparle, pperpe = ml.proj_tens_on_vec(Pe, B) # pparle = Pe:bb
                                                            # pperpe = Pe:(id - bb)/2
                Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.    # Pisoe = Tr(Pe)/3
                del Pe
        del B
#
        #energies
        if code_name == 'iPIC':
            FEmult = 1./(4.*np.pi)
            FBmult = 1./(4.*np.pi)
        elif code_name == 'HVM':
            FEmult = 0.
            FBmult = 1.
        else:
            print('\nERROR: code name unknown!')
            sys.exit()
        F['E'][run_name].append(np.mean( 0.5*E2*FEmult ))
        F['M'][run_name].append(np.mean( 0.5*B2*FBmult ))
        K['p'][run_name].append(np.mean( 0.5*m_p*n_p*u_p2 ))
        K['e'][run_name].append(np.mean( 0.5*m_e*n_e*u_e2 ))
        K['parlp'][run_name].append(np.mean( 0.5*m_p*n_p*u_parlp2 ))
        K['perpp'][run_name].append(np.mean( 0.5*m_p*n_p*u_perpp2 ))
        K['parle'][run_name].append(np.mean( 0.5*m_e*n_e*u_parle2 ))
        K['perpe'][run_name].append(np.mean( 0.5*m_e*n_e*u_perpe2 ))
        I['p'][run_name].append(np.mean( 1.5*Pisop ))
        I['e'][run_name].append(np.mean( 1.5*Pisoe ))
        I['parlp'][run_name].append(np.mean( 0.5*pparlp ))
        I['perpp'][run_name].append(np.mean( 1.0*pperpp ))
        I['parle'][run_name].append(np.mean( 0.5*pparle ))
        I['perpe'][run_name].append(np.mean( 1.0*pperpe ))
        Etot[run_name].append( F['M'][run_name][-1] + F['E'][run_name][-1] +
                               K['p'][run_name][-1] + K['e'][run_name][-1] +
                               I['p'][run_name][-1] + I['e'][run_name][-1] )
        D['p'][run_name].append(np.mean( n_p ))
        D['e'][run_name].append(np.mean( n_e ))
        t[run_name].append(times[ind])
    #---> loop over time <---
        print "\r",
        print run_name," t = ",times[ind],m_p,m_e,
        gc.collect()
        sys.stdout.flush()
    #------------------------
#
    #initial budget
    E0[run_name] = Etot[run_name][0]
#
    #make inito ndarray
    F['E'][run_name]     = np.array(F['E'][run_name])
    F['M'][run_name]     = np.array(F['M'][run_name])
    K['p'][run_name]     = np.array(K['p'][run_name])
    K['e'][run_name]     = np.array(K['e'][run_name])
    K['parlp'][run_name] = np.array(K['parlp'][run_name])
    K['perpp'][run_name] = np.array(K['perpp'][run_name])
    K['parle'][run_name] = np.array(K['parle'][run_name])
    K['perpe'][run_name] = np.array(K['perpe'][run_name])
    I['p'][run_name]     = np.array(I['p'][run_name])
    I['e'][run_name]     = np.array(I['e'][run_name])
    I['parlp'][run_name] = np.array(I['parlp'][run_name])
    I['perpp'][run_name] = np.array(I['perpp'][run_name])
    I['parle'][run_name] = np.array(I['parle'][run_name])
    I['perpe'][run_name] = np.array(I['perpe'][run_name])
    Etot[run_name]       = np.array(Etot[run_name])
    D['p'][run_name]     = np.array(D['p'][run_name])
    D['e'][run_name]     = np.array(D['e'][run_name])
    t[run_name]          = np.array(t[run_name])
#
    istart[run_name] = np.argmin(np.abs(t[run_name]-tstart[run_name]))

#----------------
#PLOT TIME!!!!!!!
#----------------
#functions
def get_ax(ax,i,j):
    if type(ax) != np.ndarray:
        return ax
    if len(ax.shape) == 2:
        return ax[i,j]
    else:
        return ax[i+j]

# Variables names:
#
# 'Etot'   : '$E_{\mathrm{tot}}$'   -> Total energy
# 'EE'     : '$E_{\mathrm{E.f.}}$'  -> Electric energy
# 'EM'     : '$E_{\mathrm{M.f.}}$'  -> Magnetic energy
# 'Ekin'   : '$E_{\mathrm{kin}}$'   -> Plasma energy
# 'Ebulke' : '$E_{\mathrm{blk,e}}$' -> Electrons bulk energy
# 'Ebulkp' : '$E_{\mathrm{blk,p}}$' -> Protons bulk energy
# 'Ethere' : '$E_{\mathrm{th,e}}$'  -> Electrons thermal energy
# 'Etherp' : '$E_{\mathrm{th,p}}$'  -> Protons thermal energy

#PLOT F,K,I
#inputs
nrow = 5
ncol = 2
ndat = len(run_name_vec)
sharex = True # True, False, 'col'
sharey = False # True, False, 'row'
title = 'Energy budget distribution'
xlabels = [['dummy',                    'dummy'                   ],
           ['dummy',                    'dummy'                   ],
           ['dummy',                    'dummy'                   ],
           ['dummy',                    'dummy'                   ],
           ['$t\quad [\Omega_c^{-1}]$', '$t\quad [\Omega_c^{-1}]$']]
ylabels = [['$\Delta E_{\mathrm{M.f.}}/E_0$',            '$\Delta E_{\mathrm{E.f.}}/E_0$'           ],
           ['$\Delta E_{\mathrm{blk,\parallel,p}}/E_0$', '$\Delta E_{\mathrm{blk,\parallel,e}}/E_0$'],
           ['$\Delta E_{\mathrm{blk,\perp,p}}/E_0$',     '$\Delta E_{\mathrm{blk,\perp,e}}/E_0$'    ],
           ['$\Delta E_{\mathrm{th,\parallel,p}}/E_0$',  '$\Delta E_{\mathrm{th,\parallel,e}}/E_0$' ],
           ['$\Delta E_{\mathrm{th,\perp,p}}/E_0$',      '$\Delta E_{\mathrm{th,\perp,e}}/E_0$'     ]]

#axes
plt.close('all')
fig = plt.figure(1,figsize=(12,10))
ax = fig.subplots(nrow,ncol,sharex=sharex,sharey=sharey)

#legend
ax_ = get_ax(ax,0,0)
lines = {}
for run_name in run_name_vec:
    lin, = ax_.plot([],[],linestyle='-')
    lines[run_labels[run_name]] = lin

fig.legend(lines.values(),lines.keys(),
        ncol=ndat,mode="expand",borderaxespad=0.,framealpha=0.,
        bbox_to_anchor=(0.125,0.9,0.775,0.9),loc='lower left')

#plots
ax_ = get_ax(ax,0,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(F['M'][run_name][i:] - F['M'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,0,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(F['E'][run_name][i:] - F['E'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
Emin = np.min(ymin_all)
Emax = np.max(ymax_all)
if Emin == Emax:
    Emin -= 0.001
    Emax += 0.001
ax_.set_ylim(Emin,Emax)

ax_ = get_ax(ax,1,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(K['parlp'][run_name][i:] - K['parlp'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,1,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(K['parle'][run_name][i:] - K['parle'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,2,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(K['perpp'][run_name][i:] - K['perpp'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,2,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(K['perpe'][run_name][i:] - K['perpe'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,3,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(I['parlp'][run_name][i:] - I['parlp'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,3,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(I['parle'][run_name][i:] - I['parle'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,4,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(I['perpp'][run_name][i:] - I['perpp'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,4,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(I['perpe'][run_name][i:] - I['perpe'][run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

#ticks and labels
if sharex != False:
    top_ext = 0.05
else:
    top_ext = 0.

fig.subplots_adjust(hspace=.0,wspace=.0,top=0.85+top_ext)
for (i,j) in [(i,j) for i in range(nrow) for j in range(ncol)]:
    ax_ = get_ax(ax,i,j)
    ax_.tick_params(bottom=True,top=True,left=True,right=True,
            direction='in',labelbottom=False,labeltop=False,
            labelleft=False,labelright=False)
    if j == 0:
        ax_.tick_params(labelleft=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label1.set_visible(False)
    if i == nrow-1:
        ax_.tick_params(labelbottom=True)
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label1.set_visible(False)
    if (j == ncol-1) and (sharey == False):
        ax_.tick_params(labelright=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.yaxis.set_label_position("right")
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label2.set_visible(False)
    if (i == 0) and (sharex == False):
        ax_.tick_params(labeltop=True)
        ax_.xaxis.set_label_position("top")
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label2.set_visible(False)

#fig.suptitle(title)

#draw
fname = opath+'/'+out_dir+'/'+'energy_parlperp'
for key in run_labels.keys():
    lab = run_labels[key]
    ist = istart[key]
    fname = fname+'_%s-%d'%(lab,ist)
if save_flag:
    plt.savefig(fname+'.png')
else:
    plt.show(1)

plt.close(1)

#PLOT Etot and densities
#inputs
nrow = 2
ncol = 1
ndat = len(run_name_vec)
sharex = True # True, False, 'col'
sharey = False # True, False, 'row'
title = 'Conservation of total energy and densities'
xlabels = [['dummy'                   ],
           ['$t\quad [\Omega_c^{-1}]$']]
ylabels = [['$\Delta E_{tot}/E_0$'                    ],
           ['$\Delta M_{\mathrm{a}}/M_{\mathrm{a},0}$']]

#axes
plt.close('all')
fig = plt.figure(1,figsize=(11,6))
ax = fig.subplots(nrow,ncol,sharex=sharex,sharey=sharey)

#legend
ax_ = get_ax(ax,0,0)
lines = {}
for run_name in run_name_vec:
    lin, = ax_.plot([],[],linestyle='-')
    lines[run_labels[run_name]] = lin

fig.legend(lines.values(),lines.keys(),
        ncol=ndat,mode="expand",borderaxespad=0.,framealpha=0.,
        bbox_to_anchor=(0.125,0.9,0.775,0.9),loc='lower left')

#plots
ax_ = get_ax(ax,0,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(Etot[run_name][i:] - Etot[run_name][i],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,1,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    i = istart[run_name]
    XDATA = t[run_name][i:]
    YDATA = np.divide(D['p'][run_name][i:],D['p'][run_name][i]) - 1.
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle='-')
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))
#
    XDATA = t[run_name][i:]
    YDATA = np.divide(D['e'][run_name][i:],D['e'][run_name][i]) - 1.
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle='--')
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))
lines_tmp = {}
lin, = ax_.plot([],[],linestyle='-',color='black')
lines_tmp['Protons'] = lin
lin, = ax_.plot([],[],linestyle='--',color='black')
lines_tmp['Electrons'] = lin
ax_.legend(lines_tmp.values(),lines_tmp.keys(),
        ncol=1,framealpha=0.,loc='best')

#ticks and labels
if sharex != False:
    top_ext = 0.05
else:
    top_ext = 0.

fig.subplots_adjust(hspace=.0,wspace=.0,top=0.85+top_ext)#,right=0.9,left=0.125,bottom=0.1)
for (i,j) in [(i,j) for i in range(nrow) for j in range(ncol)]:
    ax_ = get_ax(ax,i,j)
    ax_.tick_params(bottom=True,top=True,left=True,right=True,
            direction='in',labelbottom=False,labeltop=False,
            labelleft=False,labelright=False)
    if j == 0:
        ax_.tick_params(labelleft=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label1.set_visible(False)
    if i == nrow-1:
        ax_.tick_params(labelbottom=True)
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label1.set_visible(False)
    if (j == ncol-1) and (sharey == False):
        ax_.tick_params(labelright=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.yaxis.set_label_position("right")
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label2.set_visible(False)
    if (i == 0) and (sharex == False):
        ax_.tick_params(labeltop=True)
        ax_.xaxis.set_label_position("top")
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label2.set_visible(False)

#fig.suptitle(title)

#draw
fname = opath+'/'+out_dir+'/'+'energy_density_conservation'
for key in run_labels.keys():
    lab = run_labels[key]
    ist = istart[key]
    fname = fname+'_%s-%d'%(lab,ist)

if save_flag:
    plt.savefig(fname+'.png')
else:
    plt.show(1)

plt.close(1)
