"""
some usefule function to compute quantities

m. sisti, f. finelli, s. fadanelli
mail to: francesco.finelli@phd.unipi.it

18/11/2020
"""
import numpy as np
#from numba import njit
#
##----------
##Norm of a vector field with 3 components
#@njit('float64[:,:](float64[:,:,:])') # x5 speed-up
#def norm_xyz(A):
#    return np.sqrt( A[0]**2 + A[1]**2 + A[2]**2 )
#
##----------
##Norm of a vector field with 2 components
#@njit('float64[:,:](float64[:,:,:])')
#def norm_xy(A):
#    return np.sqrt( A[0]**2 + A[1]**2 )
#
##----------
##Scalar product of 2 vector field with 3 components each
#@njit('float64[:,:](float64[:,:,:],float64[:,:,:])') # x1.5 speed-up
#def scalar_xyz(A,B):
#    return A[0]*B[0] + A[1]*B[1] + A[2]*B[2]
#
##----------
##Scalar product of 2 vector field with 2 components each
#@njit('float64[:,:](float64[:,:,:],float64[:,:,:])')
#def scalar_xy(A,B):
#    return A[0]*B[0] + A[1]*B[1]
#
##----------
##Cross product of 2 vector field with 3 components each
#@njit('float64[:,:,:](float64[:,:,:],float64[:,:,:])') # x2 speed-up
#def cross_xyz(A,B):
#    C = np.empty(A.shape,dtype=np.float64)
#    C[0] =   A[1]*B[2] - A[2]*B[1]
#    C[1] = - A[0]*B[2] + A[2]*B[0]
#    C[2] =   A[0]*B[1] - A[1]*B[0]
#    return C
#
##----------
##Cross product of 2 vector field with 2 components each
#@njit('float64[:,:](float64[:,:,:],float64[:,:,:])')
#def cross_xy(A,B):
#    return A[0]*B[1] - A[1]*B[0]

#----------
#Finite difference derivative along x for a scalar field
#def derivx(A,dx,accuracy_order,periodic): 
def derivx(B,dx,accuracy_order,periodic): 
    invdx = 1./dx
    #nx = A.shape[0]
    nx = B.shape[0]
    A = B.T.T.T#np.transpose(np.transpose(B))
    dAdx = np.empty(A.shape,dtype=np.float64)
    if accuracy_order == 2:
        c1 =  1./2.  *invdx
        if periodic:
            dAdx[  0  ,:] =     c1*( A[  1  ,:] - A[ -1  ,:] )
            dAdx[ 1:-1,:] =     c1*( A[ 2:  ,:] - A[  :-2,:] )
            dAdx[ -1  ,:] =     c1*( A[  0  ,:] - A[ -2  ,:] )
        else:
            f0 = - 3./2. *invdx
            f1 =   2.    *invdx
            f2 = - 1./2. *invdx
            dAdx[  0  ,:] = (   f0*A[  0  ,:]
                              + f1*A[  1  ,:]
                              + f2*A[  2  ,:] )
            dAdx[ 1:-1,:] = c1*( A[ 2:  ,:] - A[  :-2,:] )
            dAdx[ -1  ,:] = ( - f0*A[ -1  ,:]
                              - f1*A[ -2  ,:]
                              - f2*A[ -3  ,:] )
    elif accuracy_order == 4:
        c1 =  2./3.  *invdx
        c2 = -1./12. *invdx
        if periodic:
            dAdx[  0  ,:] = (   c1*( A[  1  ,:] - A[ -1  ,:] )
                              + c2*( A[  2  ,:] - A[ -2  ,:] ) )
            dAdx[  1  ,:] = (   c1*( A[  2  ,:] - A[  0  ,:] ) 
                              + c2*( A[  3  ,:] - A[ -1  ,:] ) )
            dAdx[ 2:-2,:] = (   c1*( A[ 3:-1,:] - A[ 1:-3,:] )
                              + c2*( A[ 4:  ,:] - A[  :-4,:] ) )
            dAdx[ -2  ,:] = (   c1*( A[ -1  ,:] - A[ -3  ,:] ) 
                              + c2*( A[  0  ,:] - A[ -4  ,:] ) )
            dAdx[ -1  ,:] = (   c1*( A[  0  ,:] - A[ -2  ,:] )
                              + c2*( A[  1  ,:] - A[ -3  ,:] ) )
        else:
            f0 = -25./12.*invdx
            f1 =   4.    *invdx
            f2 = - 3.    *invdx
            f3 =   4./3. *invdx
            f4 = - 1./4. *invdx
            dAdx[  : 2,:]  = (   f0*A[  : 2,:]
                               + f1*A[ 1: 3,:]
                               + f2*A[ 2: 4,:]
                               + f3*A[ 3: 5,:]
                               + f4*A[ 4: 6,:] )
            dAdx[ 2:-2,:]  = (   c1*( A[ 3:-1,:] - A[ 1:-3,:] )
                               + c2*( A[ 4:  ,:] - A[  :-4,:] ) )
            dAdx[-2:  ,:]  = ( - f0* A[-2:  ,:]
                               - f1* A[-3:-1,:]
                               - f2* A[-4:-2,:]
                               - f3* A[-5:-3,:]
                               - f4* A[-6:-4,:] )
    elif accuracy_order == 6:
        c1 =   3./4. *invdx
        c2 = - 3./20.*invdx
        c3 =   1./60.*invdx
        if periodic:
            dAdx[  0  ,:] = (   c1*( A[  1  ,:] - A[ -1  ,:] )
                              + c2*( A[  2  ,:] - A[ -2  ,:] )
                              + c3*( A[  3  ,:] - A[ -3  ,:] ) )
            dAdx[  1  ,:] = (   c1*( A[  2  ,:] - A[  0  ,:] )
                              + c2*( A[  3  ,:] - A[ -1  ,:] )
                              + c3*( A[  4  ,:] - A[ -2  ,:] ) )
            dAdx[  2  ,:] = (   c1*( A[  3  ,:] - A[  1  ,:] )
                              + c2*( A[  4  ,:] - A[  0  ,:] )
                              + c3*( A[  5  ,:] - A[ -1  ,:] ) )
            dAdx[ 3:-3,:] = (   c1*( A[ 4:-2,:] - A[ 2:-4,:] )
                              + c2*( A[ 5:-1,:] - A[ 1:-5,:] )
                              + c3*( A[ 6:  ,:] - A[  :-6,:] ) )
            dAdx[ -3  ,:] = (   c1*( A[ -2  ,:] - A[ -4  ,:] )
                              + c2*( A[ -1  ,:] - A[ -5  ,:] )
                              + c3*( A[  0  ,:] - A[ -6  ,:] ) )
            dAdx[ -2  ,:] = (   c1*( A[ -1  ,:] - A[ -3  ,:] )
                              + c2*( A[  0  ,:] - A[ -4  ,:] )
                              + c3*( A[  1  ,:] - A[ -5  ,:] ) )
            dAdx[ -1  ,:] = (   c1*( A[  0  ,:] - A[ -2  ,:] )
                              + c2*( A[  1  ,:] - A[ -3  ,:] )
                              + c3*( A[  2  ,:] - A[ -4  ,:] ) )
        else:
            f0 = -49./20.*invdx
            f1 =   6.    *invdx
            f2 = -15./2. *invdx
            f3 =  20./3. *invdx
            f4 = -15./4. *invdx
            f5 =   6./5. *invdx
            f6 = - 1./6. *invdx
            dAdx[  : 3,:]  = (   f0*A[  : 3,:]
                               + f1*A[ 1: 4,:]
                               + f2*A[ 2: 5,:]
                               + f3*A[ 3: 6,:]
                               + f4*A[ 4: 7,:]
                               + f5*A[ 5: 8,:]
                               + f6*A[ 6: 9,:] )
            dAdx[ 3:-3,:]  = (   c1*( A[ 4:-2,:] - A[ 2:-4,:] )
                               + c2*( A[ 5:-1,:] - A[ 1:-5,:] )
                               + c3*( A[ 6:  ,:] - A[  :-6,:] ) )
            dAdx[-3:  ,:]  = ( - f0*A[-3:  ,:]
                               - f1*A[-4:-1,:]
                               - f2*A[-5:-2,:]
                               - f3*A[-6:-3,:]
                               - f4*A[-7:-4,:]
                               - f5*A[-8:-5,:]
                               - f6*A[-9:-6,:] )
    else:
        msg  = '\nERROR in derivx -> accuracy_order in not 2,4 or 6.'
        msg += '\nReturning un-initialized array!!!\n'
        print(msg)
#
    del A
    return dAdx.T

#----------
#Finite difference derivative along y for a scalar field
def derivy(B,dy,accuracy_order,periodic):
    invdy = 1./dy
    ny = B.shape[1]
    #A = B.T
    A = np.transpose(B)
    dAdy = np.empty(A.shape,dtype=np.float64)
    if accuracy_order == 2:
        c1 =  1./2.  *invdy
        if periodic:
            dAdy[  0  ,:] =     c1*( A[  1  ,:] - A[ -1  ,:] )
            dAdy[ 1:-1,:] =     c1*( A[ 2:  ,:] - A[  :-2,:] )
            dAdy[ -1  ,:] =     c1*( A[  0  ,:] - A[ -2  ,:] )
        else:
            f0 = - 3./2. *invdy
            f1 =   2.    *invdy
            f2 = - 1./2. *invdy
            dAdy[  0  ,:] = (   f0*A[  0  ,:]
                              + f1*A[  1  ,:]
                              + f2*A[  2  ,:] )
            dAdy[ 1:-1,:] = c1*( A[ 2:  ,:] - A[  :-2,:] )
            dAdy[ -1  ,:] = ( - f0*A[ -1  ,:]
                              - f1*A[ -2  ,:]
                              - f2*A[ -3  ,:] )
    elif accuracy_order == 4:
        c1 =  2./3.  *invdy
        c2 = -1./12. *invdy
        if periodic:
            dAdy[  0  ,:] = (   c1*( A[  1  ,:] - A[ -1  ,:] )
                              + c2*( A[  2  ,:] - A[ -2  ,:] ) )
            dAdy[  1  ,:] = (   c1*( A[  2  ,:] - A[  0  ,:] ) 
                              + c2*( A[  3  ,:] - A[ -1  ,:] ) )
            dAdy[ 2:-2,:] = (   c1*( A[ 3:-1,:] - A[ 1:-3,:] )
                              + c2*( A[ 4:  ,:] - A[  :-4,:] ) )
            dAdy[ -2  ,:] = (   c1*( A[ -1  ,:] - A[ -3  ,:] ) 
                              + c2*( A[  0  ,:] - A[ -4  ,:] ) )
            dAdy[ -1  ,:] = (   c1*( A[  0  ,:] - A[ -2  ,:] )
                              + c2*( A[  1  ,:] - A[ -3  ,:] ) )
        else:
            f0 = -25./12.*invdy
            f1 =   4.    *invdy
            f2 = - 3.    *invdy
            f3 =   4./3. *invdy
            f4 = - 1./4. *invdy
            dAdy[  : 2,:]  = (   f0*A[  : 2,:]
                               + f1*A[ 1: 3,:]
                               + f2*A[ 2: 4,:]
                               + f3*A[ 3: 5,:]
                               + f4*A[ 4: 6,:] )
            dAdy[ 2:-2,:]  = (   c1*( A[ 3:-1,:] - A[ 1:-3,:] )
                               + c2*( A[ 4:  ,:] - A[  :-4,:] ) )
            dAdy[-2:  ,:]  = ( - f0* A[-2:  ,:]
                               - f1* A[-3:-1,:]
                               - f2* A[-4:-2,:]
                               - f3* A[-5:-3,:]
                               - f4* A[-6:-4,:] )
    elif accuracy_order == 6:
        c1 =   3./4. *invdy
        c2 = - 3./20.*invdy
        c3 =   1./60.*invdy
        if periodic:
            dAdy[  0  ,:] = (   c1*( A[  1  ,:] - A[ -1  ,:] )
                              + c2*( A[  2  ,:] - A[ -2  ,:] )
                              + c3*( A[  3  ,:] - A[ -3  ,:] ) )
            dAdy[  1  ,:] = (   c1*( A[  2  ,:] - A[  0  ,:] )
                              + c2*( A[  3  ,:] - A[ -1  ,:] )
                              + c3*( A[  4  ,:] - A[ -2  ,:] ) )
            dAdy[  2  ,:] = (   c1*( A[  3  ,:] - A[  1  ,:] )
                              + c2*( A[  4  ,:] - A[  0  ,:] )
                              + c3*( A[  5  ,:] - A[ -1  ,:] ) )
            dAdy[ 3:-3,:] = (   c1*( A[ 4:-2,:] - A[ 2:-4,:] )
                              + c2*( A[ 5:-1,:] - A[ 1:-5,:] )
                              + c3*( A[ 6:  ,:] - A[  :-6,:] ) )
            dAdy[ -3  ,:] = (   c1*( A[ -2  ,:] - A[ -4  ,:] )
                              + c2*( A[ -1  ,:] - A[ -5  ,:] )
                              + c3*( A[  0  ,:] - A[ -6  ,:] ) )
            dAdy[ -2  ,:] = (   c1*( A[ -1  ,:] - A[ -3  ,:] )
                              + c2*( A[  0  ,:] - A[ -4  ,:] )
                              + c3*( A[  1  ,:] - A[ -5  ,:] ) )
            dAdy[ -1  ,:] = (   c1*( A[  0  ,:] - A[ -2  ,:] )
                              + c2*( A[  1  ,:] - A[ -3  ,:] )
                              + c3*( A[  2  ,:] - A[ -4  ,:] ) )
        else:
            f0 = -49./20.*invdy
            f1 =   6.    *invdy
            f2 = -15./2. *invdy
            f3 =  20./3. *invdy
            f4 = -15./4. *invdy
            f5 =   6./5. *invdy
            f6 = - 1./6. *invdy
            dAdy[  : 3,:]  = (   f0*A[  : 3,:]
                               + f1*A[ 1: 4,:]
                               + f2*A[ 2: 5,:]
                               + f3*A[ 3: 6,:]
                               + f4*A[ 4: 7,:]
                               + f5*A[ 5: 8,:]
                               + f6*A[ 6: 9,:] )
            dAdy[ 3:-3,:]  = (   c1*( A[ 4:-2,:] - A[ 2:-4,:] )
                               + c2*( A[ 5:-1,:] - A[ 1:-5,:] )
                               + c3*( A[ 6:  ,:] - A[  :-6,:] ) )
            dAdy[-3:  ,:]  = ( - f0*A[-3:  ,:]
                               - f1*A[-4:-1,:]
                               - f2*A[-5:-2,:]
                               - f3*A[-6:-3,:]
                               - f4*A[-7:-4,:]
                               - f5*A[-8:-5,:]
                               - f6*A[-9:-6,:] )
    else:
        msg  = '\nERROR in derivy -> accuracy_order in not 2,4 or 6.'
        msg += '\nReturning un-initialized array!!!\n'
        print(msg)
#
    del A
    #return dAdy.T
    return np.transpose(dAdy)


#----------
#Finite difference derivative along x for scalar, vector, or p-tensorial field, on a single point
def derivx_on_point(A,ij,dx,accuracy_order=6,periodic=True):
    i,j = ij
    nx = A.shape[-2]
    invdx = 1./dx
    if accuracy_order == 2:
        c1 =  1./2.  *invdx
#
        LC = i < 1
        RC = i > (nx-2)
        if not (LC or RC):
            dAdx = c1*( A[...,i+1,j] - A[...,i-1,j] )
        elif periodic:
            dAdx = c1*( A[...,(i+1)%nx,j] - A[...,(i-1)%nx,j] )
        else:
            f0 = - 3./2. *invdx
            f1 =   2.    *invdx
            f2 = - 1./2. *invdx
            if LC:
                dAdx =   f0*A[...,i,j] + f1*A[...,i+1,j] + f2*A[...,i+2,j]
            else:
                dAdx = - f0*A[...,i,j] - f1*A[...,i-1,j] - f2*A[...,i-2,j]
#
    elif accuracy_order == 4:
        c1 =  2./3.  *invdx
        c2 = -1./12. *invdx
#
        LC = i < 2
        RC = i > (nx-3)
        if not (LC or RC):
            dAdx = ( c1*( A[...,i+1,j] - A[...,i-1,j] ) + 
                     c2*( A[...,i+2,j] - A[...,i-2,j] ) )
        elif periodic:
            dAdx = ( c1*( A[...,(i+1)%nx,j] - A[...,(i-1)%nx,j] ) + 
                     c2*( A[...,(i+2)%nx,j] - A[...,(i-2)%nx,j] ) )
        else:
            f0 = -25./12.*invdx
            f1 =   4.    *invdx
            f2 = - 3.    *invdx
            f3 =   4./3. *invdx
            f4 = - 1./4. *invdx
            if LC:
                dAdx = (   f0*A[...,i,j]   + f1*A[...,i+1,j] + f2*A[...,i+2,j]
                         + f3*A[...,i+3,j] + f4*A[...,i+4,j] )
            else:
                dAdx = ( - f0*A[...,i,j]   - f1*A[...,i-1,j] - f2*A[...,i-2,j]
                         - f3*A[...,i-3,j] - f4*A[...,i-4,j] )
#
    elif accuracy_order == 6:
        c1 =   3./4. *invdx
        c2 = - 3./20.*invdx
        c3 =   1./60.*invdx
#
        LC = i < 3
        RC = i > (nx-4)
        if not (LC or RC):
            dAdx = ( c1*( A[...,i+1,j] - A[...,i-1,j] ) +
                     c2*( A[...,i+2,j] - A[...,i-2,j] ) +
                     c3*( A[...,i+3,j] - A[...,i-3,j] ) )
        elif periodic:
            dAdx = ( c1*( A[...,(i+1)%nx,j] - A[...,(i-1)%nx,j] ) +
                     c2*( A[...,(i+2)%nx,j] - A[...,(i-2)%nx,j] ) +
                     c3*( A[...,(i+3)%nx,j] - A[...,(i-3)%nx,j] ) )
        else:
            f0 = -49./20.*invdx
            f1 =   6.    *invdx
            f2 = -15./2. *invdx
            f3 =  20./3. *invdx
            f4 = -15./4. *invdx
            f5 =   6./5. *invdx
            f6 = - 1./6. *invdx
            if LC:
                dAdx = (   f0*A[...,i,j]   + f1*A[...,i+1,j] + f2*A[...,i+2,j]
                         + f3*A[...,i+3,j] + f4*A[...,i+4,j] + f5*A[...,i+5,j]
                         + f6*A[...,i+6,j] )
            else:
                dAdx = ( - f0*A[...,i,j]   - f1*A[...,i-1,j] - f2*A[...,i-2,j]
                         - f3*A[...,i-3,j] - f4*A[...,i-4,j] - f5*A[...,i-5,j] 
                         - f6*A[...,i-6,j] )
#
    else:
        msg  = '\nERROR in derivx_on_point -> accuracy_order in not 2,4 or 6.'
        msg += '\nReturning un-initialized array!!!\n'
        print(msg)
#
    return dAdx

#----------
#Finite difference derivative along y for scalar, vector, or p-tensorial field, on a single point
def derivy_on_point(A,ij,dy,accuracy_order=6,periodic=True):
    i,j = ij
    ny = A.shape[-1]
    invdy = 1./dy
    if accuracy_order == 2:
        c1 =  1./2.  *invdy
#
        LC = j < 1
        RC = j > (ny-2)
        if not (LC or RC):
            dAdy = c1*( A[...,i,j+1] - A[...,i,j-1] )
        elif periodic:
            dAdy = c1*( A[...,i,(j+1)%ny] - A[...,i,(j-1)%ny] )
        else:
            f0 = - 3./2. *invdy
            f1 =   2.    *invdy
            f2 = - 1./2. *invdy
            if LC:
                dAdy =   f0*A[...,i,j] + f1*A[...,i,j+1] + f2*A[...,i,j+2]
            else:
                dAdy = - f0*A[...,i,j] - f1*A[...,i,j-1] - f2*A[...,i,j-2]
#
    elif accuracy_order == 4:
        c1 =  2./3.  *invdy
        c2 = -1./12. *invdy
#
        LC = j < 2
        RC = j > (ny-3)
        if not (LC or RC):
            dAdy = ( c1*( A[...,i,j+1] - A[...,i,j-1] ) + 
                     c2*( A[...,i,j+2] - A[...,i,j-2] ) )
        elif periodic:
            dAdy = ( c1*( A[...,i,(j+1)%ny] - A[...,i,(j-1)%ny] ) + 
                     c2*( A[...,i,(j+2)%ny] - A[...,i,(j-2)%ny] ) )
        else:
            f0 = -25./12.*invdy
            f1 =   4.    *invdy
            f2 = - 3.    *invdy
            f3 =   4./3. *invdy
            f4 = - 1./4. *invdy
            if LC:
                dAdy = (   f0*A[...,i,j]   + f1*A[...,i,j+1] + f2*A[...,i,j+2]
                         + f3*A[...,i,j+3] + f4*A[...,i,j+4] )
            else:
                dAdy = ( - f0*A[...,i,j]   - f1*A[...,i,j-1] - f2*A[...,i,j-2]
                         - f3*A[...,i,j-3] - f4*A[...,i,j-4] )
#
    elif accuracy_order == 6:
        c1 =   3./4. *invdy
        c2 = - 3./20.*invdy
        c3 =   1./60.*invdy
#
        LC = j < 3
        RC = j > (ny-4)
        if not (LC or RC):
            dAdy = ( c1*( A[...,i,j+1] - A[...,i,j-1] ) +
                     c2*( A[...,i,j+2] - A[...,i,j-2] ) +
                     c3*( A[...,i,j+3] - A[...,i,j-3] ) )
        elif periodic:
            dAdy = ( c1*( A[...,i,(j+1)%ny] - A[...,i,(j-1)%ny] ) +
                     c2*( A[...,i,(j+2)%ny] - A[...,i,(j-2)%ny] ) +
                     c3*( A[...,i,(j+3)%ny] - A[...,i,(j-3)%ny] ) )
        else:
            f0 = -49./20.*invdy
            f1 =   6.    *invdy
            f2 = -15./2. *invdy
            f3 =  20./3. *invdy
            f4 = -15./4. *invdy
            f5 =   6./5. *invdy
            f6 = - 1./6. *invdy
            if LC:
                dAdy = (   f0*A[...,i,j]   + f1*A[...,i,j+1] + f2*A[...,i,j+2]
                         + f3*A[...,i,j+3] + f4*A[...,i,j+4] + f5*A[...,i,j+5]
                         + f6*A[...,i,j+6] )
            else:
                dAdy = ( - f0*A[...,i,j]   - f1*A[...,i,j-1] - f2*A[...,i,j-2]
                         - f3*A[...,i,j-3] - f4*A[...,i,j-4] - f5*A[...,i,j-5] 
                         - f6*A[...,i,j-6] )
#
    else:
        msg  = '\nERROR in derivy_on_point -> accuracy_order in not 2,4 or 6.'
        msg += '\nReturning un-initialized array!!!\n'
        print(msg)
#
    return dAdy

#----------
#Hessian of the field computed in given grid points, whith its eigenvalues and eigenvectors
def hessian_and_eig_on_points(A,ij_arr,dx,dy,accuracy_order=6,periodic=True,normalization=True):
    npts = ij_arr.shape[0]
    field_shape = list(A.shape[:-2])
    sh = tuple(field_shape + [npts])
    ddAdxdx = np.empty(sh,dtype=np.float64)
    ddAdxdy = np.empty(sh,dtype=np.float64)
    ddAdydy = np.empty(sh,dtype=np.float64)
    for ip in range(npts):
        ij = ij_arr[ip,:]
        i,j = ij
        #dAdx = derivx_on_point(A,ij,dx,accuracy_order=accuracy_order,periodic=periodic)
        #dAdy = derivy_on_point(A,ij,dy,accuracy_order=accuracy_order,periodic=periodic)
        dAdx = derivx(A,dx,accuracy_order=accuracy_order,periodic=periodic)
        dAdy = derivy(A,dy,accuracy_order=accuracy_order,periodic=periodic)
        #ddAdxdx[...,ip] = derivx_on_point(dAdx,ij,dx,
        #                                  accuracy_order=accuracy_order,periodic=periodic)
        #ddAdxdy[...,ip] = derivx_on_point(dAdy,ij,dx,
        #                                  accuracy_order=accuracy_order,periodic=periodic)
        #ddAdydy[...,ip] = derivy_on_point(dAdy,ij,dy,
        #                                  accuracy_order=accuracy_order,periodic=periodic)
        ddAdxdx[...,ip] = derivx(dAdx,dx,accuracy_order=accuracy_order,periodic=periodic)[...,i,j]
        ddAdxdy[...,ip] = derivx(dAdy,dx,accuracy_order=accuracy_order,periodic=periodic)[...,i,j]
        ddAdydy[...,ip] = derivy(dAdy,dy,accuracy_order=accuracy_order,periodic=periodic)[...,i,j]
    del dAdx,dAdy
#
    sh = tuple(field_shape + [2,2,npts])
    H = np.empty(sh,dtype=np.float64)
    if normalization:
        for ip in range(npts):
            norm = np.abs(A[...,ij_arr[ip,0],ij_arr[ip,1]])
            H[...,0,0,ip] = np.divide(ddAdxdx[...,ip],norm)
            H[...,0,1,ip] = np.divide(ddAdxdy[...,ip],norm)
            H[...,1,0,ip] = H[...,0,1,ip]
            H[...,1,1,ip] = np.divide(ddAdydy[...,ip],norm)
        del norm
    else:
        for ip in range(npts):
            H[...,0,0,ip] = ddAdxdx[...,ip]
            H[...,0,1,ip] = ddAdxdy[...,ip]
            H[...,1,0,ip] = H[...,0,1,ip]
            H[...,1,1,ip] = ddAdydy[...,ip]
    del ddAdxdx,ddAdxdy,ddAdydy
#
    flatten_field_shape = 1
    for dim_len in field_shape:
        flatten_field_shape *= dim_len
    sh = tuple([flatten_field_shape] + [2,npts])
    eig_val = np.empty(sh,dtype=np.float64)
    sh = tuple([flatten_field_shape] + [2,2,npts])
    eig_vec = np.empty(sh,dtype=np.float64)
    H = H.reshape(flatten_field_shape,2,2,npts)
    for ii in range(flatten_field_shape):
        for ip in range(npts):
            eig_val[ii,:,ip],eig_vec[ii,:,:,ip] = np.linalg.eig(H[ii,:,:,ip])
    sh = tuple(field_shape + [2,npts])
    eig_val = eig_val.reshape(sh)
    sh = tuple(field_shape + [2,2,npts])
    eig_vec = eig_vec.reshape(sh)
    H = H.reshape(sh)
#
    return H,eig_val,eig_vec

# i j Jnorm Ve_xynorm Omega_Ve, aEdec aB_inplane JE
