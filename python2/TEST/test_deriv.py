import numpy as np
import utils_2D as u2D
import random
import timeit
import sys
#sys.path.insert(0,'/home/frafin/Documents/aidapy/aidapy/WIP-unsupervised_reconnection/2.x/utils')
#import utilities_unsup as ut

nx = 1024*4
xl = 4.*np.pi
dx = xl/float(nx)
x = np.linspace(0.,xl-dx,nx)
ny = 1024*4
yl = 4.*np.pi
dy = yl/float(ny)
y = np.linspace(0.,yl-dy,ny)
X,Y = np.meshgrid(x,y)
X = X.T
Y = Y.T
z    =   np.sin(X)*np.cos(Y)
dzdx =   np.cos(X)*np.cos(Y)
dzdy = - np.sin(X)*np.sin(Y)

periodic = True
accuracy_order = 6
number = 10

#print('\ncalc_gradx')
#print(timeit.timeit(lambda: ut.calc_gradx(z.reshape(nx,ny,1),
#                                          nx,ny,1,[dx,dy],periodic=periodic),number=number))
#dzdx_ = ut.calc_gradx(z.reshape(nx,ny,1),nx,ny,1,[dx,dy],periodic=periodic).reshape(nx,ny)
#print('Max err =',np.max(np.abs(dzdx - dzdx_)))
#
#print('\ncalc_grady')
#print(timeit.timeit(lambda: ut.calc_grady(z.reshape(nx,ny,1),
#                                          nx,ny,1,[dx,dy]),number=number))
#dzdy_ = ut.calc_grady(z.reshape(nx,ny,1),nx,ny,1,[dx,dy]).reshape(nx,ny)
#print('Max err =',np.max(np.abs(dzdy - dzdy_)))

print('\nderivy')
print(timeit.timeit(lambda: u2D.derivy(z,dy,accuracy_order=accuracy_order,periodic=periodic),
                    number=number))
dzdy_ = u2D.derivy(z,dy,accuracy_order=accuracy_order,periodic=periodic)
print('Max err =',np.max(np.abs(dzdy - dzdy_)))

print('\nderivx')
print(timeit.timeit(lambda: u2D.derivx(z,dx,accuracy_order=accuracy_order,periodic=periodic),
                    number=number))
dzdx_ = u2D.derivx(z,dx,accuracy_order=accuracy_order,periodic=periodic)
print('Max err =',np.max(np.abs(dzdx - dzdx_)))

