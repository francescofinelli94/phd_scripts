import sys
import os
import time
import numpy as np
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#ssh coord
workpath = '/home/finelli/Documents'

fname_c2c = 'control2client.txt'
fname_c2s = 'control2server.txt'

fname_cbuffer = 'client_buffer.txt'
fname_sbuffer = 'server_buffer.txt'

STOP_MSG = 'STOP'
WAITING_TIME = 1 #seconds

CLIENT_TAG = 'client'
SERVER_TAG = 'server'

dots = ['   ','.  ','.. ','...']

#make control file
f = open('%s/%s'%(workpath,fname_c2s),'w')
f.write(SERVER_TAG)
f.close()

#metadata
ipath = '/work1/finelli/LF_3d_DH'
opath_vtk = '/work1/finelli/LF_3d_DH/vtk'

#init
ml.create_path(opath_vtk)
run = from_HVM(ipath)
run.get_meta()
calc = fibo('calc')
calc.meta=run.meta

#segments loop
segs_list = list(run.segs.keys())
for seg in segs_list[30:]:
    print('\nSegment %s'%(seg))

    #unzippig
    print('unzipping')
    os.system('gunzip '+ipath+'/'+seg+'/EB.bin.gz')
    os.system('gunzip '+ipath+'/'+seg+'/Ion.bin.gz')

    #times loop
    l = 'time_list = ['
    for t in [float(ts) for ts in run.segs[seg]]:
        print('\tTime %f'%(t))
        ind = ml.index_of_closest(t,run.meta['time'])

        #field read and compute
        run.get_EB(ind,fibo_obj=calc)
        del calc.data['E_x_%08.3f'%t], calc.data['E_y_%08.3f'%t], calc.data['E_z_%08.3f'%t]
        run.get_Ion(ind,fibo_obj=calc)
        cx,cy,cz = calc.calc_curl(calc.data['B_x_%08.3f'%t],calc.data['B_y_%08.3f'%t],
                                  calc.data['B_z_%08.3f'%t])
        del calc.data['B_x_%08.3f'%t], calc.data['B_y_%08.3f'%t], calc.data['B_z_%08.3f'%t]
        calc.data['J_x_%08.3f'%t] = cx
        calc.data['J_y_%08.3f'%t] = cy
        calc.data['J_z_%08.3f'%t] = cz
        del cx,cy,cz

        #vtk write
        print '\t\tWriting n ... ',
        sys.stdout.flush()
        calc.print_vtk_scal(opath_vtk,'n_%08.3f'%t,'n_%08.3f'%t)
        print '\t\tWriting J ... ',
        sys.stdout.flush()
        calc.print_vtk_vect(opath_vtk,'J_%08.3f'%t,'J_x_%08.3f'%t,'J_y_%08.3f'%t,'J_z_%08.3f'%t)
        print '\t\tWriting ui ... ',
        sys.stdout.flush()
        calc.print_vtk_vect(opath_vtk,'ui_%08.3f'%t,'ui_x_%08.3f'%t,'ui_y_%08.3f'%t,'ui_z_%08.3f'%t)

        del calc.data['J_x_%08.3f'%t], calc.data['J_y_%08.3f'%t], calc.data['J_z_%08.3f'%t]
        del calc.data['ui_x_%08.3f'%t], calc.data['ui_y_%08.3f'%t], calc.data['ui_z_%08.3f'%t]
        del calc.data['n_%08.3f'%t]
        
        l += '%f,'%(t)

    #time_list file
    l = l[:-1]
    l += ']'

    f = open('%s/%s'%(workpath,fname_sbuffer),'w')
    f.write(l)
    f.close

    #give control
    f = open('%s/%s'%(workpath,fname_c2s),'w')
    f.write(CLIENT_TAG)
    f.close()

    #zipping
    print('zipping')
    os.system('gzip '+ipath+'/'+seg+'/EB.bin')
    os.system('gzip '+ipath+'/'+seg+'/Ion.bin')

    #request control
    wait_flag = True
    print "   ",
    cnt = 0
    while wait_flag:
        f = open('%s/%s'%(workpath,fname_c2s),'r')
        tag = f.readline()
        f.close()
        if tag == SERVER_TAG:
            wait_flag = False
        else:
            print "\r",
            print "waiting for control%s"%(dots[cnt%4]),
            sys.stdout.flush()
            cnt += 1
            time.sleep(WAITING_TIME)
    print "   "

    #vtk delete
    os.system('rm -f %s/*.vtk'%(opath_vtk))

#STOP file
f = open('%s/%s'%(workpath,fname_sbuffer),'w')
f.write(STOP_MSG)
f.close

#give control
f = open('%s/%s'%(workpath,fname_c2s),'w')
f.write(CLIENT_TAG)
f.close()

print('\nDone!')
