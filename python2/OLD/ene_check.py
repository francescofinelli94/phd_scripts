#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
import from_iPIC as fi
sys.path.insert(0,'/work1/califano/VISUAL/PYTHON')
import mylib as ml
import fourier as fl

switch = False

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
ipath = l1[0]
opath = l2[0]

run = fi.from_iPIC(ipath)
try:
    run.get_meta()
except:
    ipp = ml.secure_input('Insert the name of the directory containing parameters file: ','')
    run.get_meta(ipp)

x,y,z = ml.create_xyz(run.meta['nnn'],run.meta['lll'])

w_ele = ((run.meta['model'] > 1) or (run.meta['model'] < 0))

print(run.meta['Bx0'])
run.meta['time'] *= run.meta['Bx0']

ml.create_path(opath+'/ene_check')

print('\nMetadata loaded\n')

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,run.meta['time'])
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,run.meta['time'])
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(run.meta['time'][ind1])+' to '+str(run.meta['time'][ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

print(run.meta['time'])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

qom_i = run.meta['msQOM'][1]
qom_e = run.meta['msQOM'][0]
print('\nqom_i = '+str(qom_i)+'\nqom_e = '+str(qom_e)+'\n')

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = run.meta['time'][ind1:ind2+ind_step:ind_step]

int_E_2_bulk = []
int_E_2_ther = []
int_E_6_bulk = []
int_E_6_ther = []

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing anisotropy
    #-------------------------------------

    seg = run.meta['time2seg'][ind]
    t = run.meta['time'][ind]
    print('Now @ seg = '+seg+' for time = '+str(t)+'\n')

    Pi = run.get_Press(ind)
    TrPi = Pi[0,0] + Pi[1,1] + Pi[2,2]
    del Pi
    Pe = run.get_Press(ind,qom=qom_e)
    TrPe = Pe[0,0] + Pe[1,1] + Pe[2,2]
    del Pe
    E_2_ther = 0.5*TrPi + 0.5*TrPe
    del TrPi
    del TrPe

    ni,ui = run.get_Ion(ind)
    ui2 = ui[0]*ui[0] + ui[1]*ui[1] + ui[2]*ui[2]
    del ui
    ne,ue = run.get_Ion(ind,qom=qom_e)
    ue2 = ue[0]*ue[0] + ue[1]*ue[1] + ue[2]*ue[2]
    del ue
    E_2_bulk = 0.5*(ni/abs(qom_i))*ui2 + 0.5*(ne/abs(qom_e))*ue2
    del ni
    del ui2
    del ne
    del ue2

    flag = True
    for st in run.meta['stag']:
        i = run.meta['stag'].index(st)
        qom_s = run.meta['sQOM'][i]
        Ps = run.get_Pspec(ind,st)
        TrPs = Ps[0,0] + Ps[1,1] + Ps[2,2]
        del Ps
        ns,us = run.get_Spec(ind,st)
        us2 = us[0]*us[0] + us[1]*us[1] + us[2]*us[2]
        del us
        if flag:
            flag = False
            E_6_ther = 0.5*TrPs
            E_6_bulk = 0.5*(ns/abs(qom_s))*us2
        else:
            E_6_ther += 0.5*TrPs
            E_6_bulk += 0.5*(ns/abs(qom_s))*us2
        del TrPs
        del ns
        del us2

    #----------------------------------------------------
    #plots 2d
    #---------------
    if switch:
        plt.close()
        fig, ax = plt.subplots(nrows=3,ncols=2,figsize=(20,12))
#       
        im = ax[0,0].contourf(x,y,E_2_bulk[:,:,0].T)
        plt.colorbar(im,ax=ax[0,0])
        ax[0,0].set_title('E_2_bulk - t='+str(t))
#       
        im = ax[0,1].contourf(x,y,E_2_ther[:,:,0].T)
        plt.colorbar(im,ax=ax[0,1])
        ax[0,1].set_title('E_2_ther')
#       
        im = ax[1,0].contourf(x,y,E_6_bulk[:,:,0].T)
        plt.colorbar(im,ax=ax[1,0])
        ax[1,0].set_title('E_6_bulk')
#       
        im = ax[1,1].contourf(x,y,E_6_ther[:,:,0].T)
        plt.colorbar(im,ax=ax[1,1])
        ax[1,1].set_title('E_6_ther')
#       
#        im = ax[2,0].contourf(x,y,E_2_bulk[:,:,0].T+E_2_ther[:,:,0].T)
#        plt.colorbar(im,ax=ax[2,0])
#        ax[2,0].set_title('E_2 = E_2_bulk + E_2_ther')
        im = ax[2,0].contourf(x,y,E_2_bulk[:,:,0].T-E_6_bulk[:,:,0].T)
        plt.colorbar(im,ax=ax[2,0])
        ax[2,0].set_title('E_2_bulk - E_6_bulk')
#       
#        im = ax[2,1].contourf(x,y,E_2_bulk[:,:,0].T+E_2_ther[:,:,0].T-E_6_bulk[:,:,0].T-E_6_ther[:,:,0].T)
#        plt.colorbar(im,ax=ax[2,1])
#        ax[2,1].set_title('E_2 - E_6 (shold be 0)')
        im = ax[2,1].contourf(x,y,E_2_ther[:,:,0].T-E_6_ther[:,:,0].T)
        plt.colorbar(im,ax=ax[2,1])
        ax[2,1].set_title('E_2_ther - E_6_ther')
#       
        for axs in ax.flat:
            axs.set(xlabel='x [d_i]', ylabel='y [d_i]', 
                    xlim=(x[ixmin],x[ixmax]), ylim=(y[iymin],y[iymax]))
#       
        for axs in ax.flat:
            axs.label_outer()
#       
        plt.tight_layout()
        plt.savefig(opath+'/ene_check/'+'ene_check_'+run_name+'_'+str(ind)+'_diff.png')
        plt.close()

    #-----------------------------------------------------
    #get integral of energy
    #-----------------------------
    if not switch:
        dVol = run.meta['dx']*run.meta['dy']*run.meta['dz']
        int_E_2_bulk.append(np.sum(E_2_bulk)*dVol)
        int_E_2_ther.append(np.sum(E_2_ther)*dVol)
        int_E_6_bulk.append(np.sum(E_6_bulk)*dVol)
        int_E_6_ther.append(np.sum(E_6_ther)*dVol)

#---> loop over time <---
    act_cnt = act_cnt + 1
    print "\r",
    print "t = ",run.meta['time'][ind],
    sys.stdout.flush()
#------------------------

#------------------------------------------------------
#integral of energy vs. time
#-------------------------------
if  not switch:
    int_E_2_bulk = np.array(int_E_2_bulk)
    int_E_2_ther = np.array(int_E_2_ther)
    int_E_6_bulk = np.array(int_E_6_bulk)
    int_E_6_ther = np.array(int_E_6_ther)

    plt.close()
    fig, ax = plt.subplots(nrows=1,ncols=1,figsize=(10,8))

    im = ax.plot(act_time,int_E_2_bulk,label='E_2_bulk',ls='--',c='b')
    im = ax.plot(act_time,int_E_2_ther,label='E_2_ther',ls=':',c='b')
    im = ax.plot(act_time,int_E_2_bulk+int_E_2_ther,label='E_2',ls='-',c='b')

    im = ax.plot(act_time,int_E_6_bulk,label='E_6_bulk',ls='--',c='r')
    im = ax.plot(act_time,int_E_6_ther,label='E_6_ther',ls=':',c='r')
    im = ax.plot(act_time,int_E_6_bulk+int_E_6_ther,label='E_6',ls='-',c='r')

#    ax.set_yscale('log')
    ax.set_title('E_2 and E_6 - t = '+str(run.meta['time'][ind1])+' -> '+str(run.meta['time'][ind2]))
    ax.set_xlabel('t [ion cyclotron times]')
    ax.set_ylabel('energy [a.u.]')

    plt.tight_layout()
    plt.legend()
    plt.savefig(opath+'/ene_check/'+'ene_check_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'_TEST.png')
    plt.close()
