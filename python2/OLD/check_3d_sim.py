#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
#
#------------------------------------------------------
#loading metadata
#----------------
run_name = 'LF_3d_DH'
code_name = 'HVM'
ipath = '/work1/finelli/LF_3d_DH'
opath = '/home/finelli/Downloads'
#
if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')

run.get_meta()
#
w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)
#
time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
#
if code_name == 'HVM':
    mime = run.meta['mime']
elif code_name == 'iPIC':
    mime = abs(run.meta['msQOM'][0]/run.meta['msQOM'][1])
else:
    print('\nWhat code is this?\n')
#
if code_name == 'HVM':
    teti = run.meta['teti']
elif code_name == 'iPIC':
    P0 = run.get_Pspec(1,'0') #HARDCODED stag
    P3 = run.get_Pspec(1,'3') #HARDCODED stag
    teti = np.mean([np.mean(np.divide(P0[0,0],P3[0,0])),
                    np.mean(np.divide(P0[1,1],P3[1,1])),
                    np.mean(np.divide(P0[2,2],P3[2,2]))])
    del P0,P3
else:
    print('\nWhat code is this?\n')
#
calc = fibo('calc')
calc.meta=run.meta

print('\nMetadata loaded\n')
#
print('Segments: ')
print(run.meta['time2seg'])
print('Times: ')
print(time)
#
#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
t = ml.secure_input('Choose time: ',1.0,True)
ind = ml.index_of_closest(t,time)
print('Selected time is '+str(time[ind])+' (index '+str(ind)+')')

#-------------------------------------------------------
#hardcoded inputs
#----------------
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
iy1 = int((y1)/run.meta['dy'])
iy2 = int((y2)/run.meta['dy'])
#
print("\ny1 = y("+str(iy1)+") = "+str(y1))
print("y2 = y("+str(iy2)+") = "+str(y2)+"\n")
#
#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)
#
#--------------------------------------------------------
#getting data and computing stuff
#-------------------------------------
E,B = run.get_EB(ind)
del E

cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
J = np.array([cx,cy,cz])
del cx,cy,cz

n_p,up = run.get_Ion(ind)

if code_name == 'HVM':
    n_e = n_p
    ue = np.zeros((3,run.meta['nx'],run.meta['ny'],run.meta['nz']),dtype=np.float64)
    ue[0] = up[0] - np.divide(J[0],n_p)
    ue[1] = up[1] - np.divide(J[1],n_p)
    ue[2] = up[2] - np.divide(J[2],n_p)
elif code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]
    n_e,ue = run.get_Ion(ind,qom=qom_e)
else:
    print('\nWhat code is it?\n')

modJ = np.sqrt(J[0]*J[0]+J[1]*J[1]+J[2]*J[2])

flag = True
while flag:
    x1,x2 = input('\nRange of points in x (as [x1,x2]): ')
    y1,y2 = input('Range of points in y (as [y1,y2]): ')
    z1,z2 = input('Range of points in z (as [z1,z2]): ')
#
    modJcut = modJ[x1:x2+1,y1:y2+1,z1:z2+1]
    ixcut = np.array(range(x2-x1+1)) + x1
    iycut = np.array(range(y2-y1+1)) + y1
    izcut = np.array(range(z2-z1+1)) + z1
#
    plt.close('all')
    fig,ax = plt.subplots(2,2,figsize=(8,8))
#
    im = ax[0,0].contourf(ixcut,iycut,np.mean(modJcut,axis=2).T,32)
    plt.colorbar(im,ax=ax[0,0])
    ax[0,0].set_xlabel('ix');ax[0,0].set_ylabel('iy');ax[0,0].set_title('(x,y)')
#
    im = ax[0,1].contourf(ixcut,izcut,np.mean(modJcut,axis=1).T,32)
    plt.colorbar(im,ax=ax[0,1])
    ax[0,1].set_xlabel('ix');ax[0,1].set_ylabel('iz');ax[0,1].set_title('(x,z)')
#
    im = ax[1,0].contourf(iycut,izcut,np.mean(modJcut,axis=0).T,32)
    plt.colorbar(im,ax=ax[1,0])
    ax[1,0].set_xlabel('iy');ax[1,0].set_ylabel('iz');ax[1,0].set_title('(y,z)')
#
    plt.tight_layout()
    plt.show()
    plt.close()
#
    flag = ml.secure_input('Again? (bool): ',True,True)
