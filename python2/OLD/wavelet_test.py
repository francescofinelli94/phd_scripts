import numpy as np
import pywt
import matplotlib.pyplot as plt

dx = 0.01
x = np.arange(0.,2.*np.pi,dx)
n=len(x)
#y = np.sin(5.*x) + np.sin(17.*x)
#y = np.sin(5*x*(np.sin(x)+1.))
y = np.sin(10.*x)
y[:n//3] = 0.
y[2*n//3:] = 0.

#wlist = pywt.wavelist()
#for w in wlist:
#    try:
#        coef,freq=pywt.cwt(y,np.arange(1,n//2+1,1),w,sampling_period=dx)
#        plt.contourf(x,freq*2.*np.pi,np.abs(coef),64)
#        plt.title(w);plt.xlabel('x');plt.ylabel('k');plt.yscale('log')
#        plt.savefig(w+'_test.png')
#    except:
#        print(w+' failed')

coef,freq=pywt.cwt(y,np.logspace(np.log10(20),np.log10(110),100),'cgau8',sampling_period=dx)
plt.contourf(x,freq*2.*np.pi,np.abs(coef),64);plt.yscale('log');plt.show()

