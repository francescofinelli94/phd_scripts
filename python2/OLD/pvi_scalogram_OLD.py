import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
import scipy.ndimage.interpolation as ndm
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

ipath='/work1/califano/HVM2D/HVM_large'
pvi_path = '/home/sisti/python_routines/VIRTUAL_SATELLITE'
run = from_HVM(ipath)
run.get_meta('01')
run_name = 'HVM_large'
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']

calc = fibo('calc')
calc.meta=run.meta

t_ = ml.secure_input('Choose time (float): ',1.,True) #247. #494.
ind = ml.index_of_closest(t_,run.meta['time'])

#----------------------------------------------------------------------------------------------------

#PRIMA PARTE: ricostruzione traiettoria, le coordinate finali xco e yco sono in unita' [di]

input_angle = ml.secure_input('Choose angle (integer): ',10,True) #5 #8 #13 #75 #80 #85
input_folder = run.meta['time2seg'][ind]
pvi_path = pvi_path + '/new_pvi_%s/angle%.d'%(input_folder,input_angle)

pi2 = 2.0*np.pi
theta_s = float(input_angle)*pi2/360.0

import glob
files = glob.glob(pvi_path+'/B_1Dcut_pvi_*.dat')
ll = []
for f in files: ll.append(int(f.split('_')[-1].split('.')[0]))
files = [tmp for _,tmp in sorted(zip(ll,files))]
ll = sorted(ll)

lstart = ll[0]
for il,l,fname in zip(range(len(ll)),ll,files):
    f = open(fname, 'r')
    NN = len(f.readlines())
    f.seek(0)
    if (l == lstart):
        sco = np.zeros((NN),dtype=np.float64)
        xco = np.zeros((NN),dtype=np.float64)
        yco = np.zeros((NN),dtype=np.float64)
        PVI = np.full((NN,len(ll)),-1.,dtype=np.float64)
    for i in range(NN):
        tr = f.readline().split()
        if (l == lstart):
            s_tmp = float(tr[0])
            sco[i] = s_tmp
            x_tmp = s_tmp*np.cos(theta_s)
            if x_tmp >= pi2:
                x_tmp -= int(x_tmp/pi2)*pi2
            xco[i] = x_tmp
            y_tmp = s_tmp*np.sin(theta_s)
            if y_tmp >= pi2:
                y_tmp -= int(y_tmp/pi2)*pi2
            yco[i] = y_tmp
        PVI[i,il] = float(tr[1])
    f.close()

sco = sco*xl/pi2
xco = xco*xl/pi2
yco = yco*yl/pi2

ds = sco[1] - sco[0]

for i,l in zip(range(len(ll)),ll):
    PVI[:,i] = np.roll(PVI[:,i],l//2)

#----------------------------------------------------------------------------------------------------

#SECONDA PARTE: interpolazione

#calcolare dati
_,B = run.get_EB(ind)

cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
J = np.array([cx,cy,cz])
del cx,cy,cz

#Bn = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
Jn = np.sqrt(J[0]**2 + J[1]**2 + J[2]**2)

#interpolazione vera e propria
new_coordsx = xco/dx
new_coordsy = yco/dy

Bx_an = ndm.map_coordinates(B[0,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
By_an = ndm.map_coordinates(B[1,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
Bz_an = ndm.map_coordinates(B[2,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
#Bn_an = ndm.map_coordinates(Bn[:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')

Jn_an = ndm.map_coordinates(Jn[:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')

#----------------------------------------------------------------------------------------------------

#TERZA PARTE: esempio di plot

plt.close()
fig, ((ax0),(ax1)) = plt.subplots(2,1,figsize=(8,10),gridspec_kw={'height_ratios':[1,4]})

im0 = ax0.plot(sco,Jn_an/np.max(Jn_an),color='black',label='$|J|$ normalized')
for i,l in zip(range(len(ll)),ll):
    im0 = ax0.plot(sco[l//2:-(1+l//2)],(PVI[:,i]/np.max(PVI[:,i]))[l//2:-(1+l//2)])
#,label='scale=%.f$d_i$'%(i*0.5))
ax0.set_xlabel('s [$d_i$]');ax0.set_ylabel('PVI normalized');ax0.legend()
ax0.set_xlim(sco[0],sco[-1]);ax0.set_ylim(0.,1.)

im1 = ax1.contourf(x,y,Jn[:,:,0],32)
im1 = ax1.scatter(xco,yco,s=1,c='red')
ax1.set_xlabel('x [$d_i$]');ax1.set_ylabel('y [$d_i$]');ax1.set_title('$|J|$ and PVI peaks')
ax1.set_xlim(x[0],x[-1]);ax1.set_ylim(y[0],y[-1])

plt.tight_layout()
fig.show()
plt.close()

#----------------------------------------------------------------------------------------------------

import scaleogram as scg 
import pywt

regions = np.loadtxt('/home/sisti/python_routines/regions_possible_reconnection_t%.f.txt'%int(t_))
regions_an = ndm.map_coordinates(regions[:,:].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
rec_ind = np.where(regions_an[:] > 0.)[0]

# choose default wavelet function for the entire notebook
# Try these ones to see various compromises between scales and time resolution 
#scg.set_default_wavelet('cmor1-1.5')
#scg.set_default_wavelet('cgau5')
#scg.set_default_wavelet('cgau1')
#scg.set_default_wavelet('shan0.5-2')
#scg.set_default_wavelet('mexh')
#scg.set_default_wavelet('morl')

print pywt.wavelist()

MWname = 'gaus1' #'morl'

scg.set_default_wavelet(MWname)

#my_scales = np.linspace(1.,600.,200) #np.array(ll) #np.logspace(np.log10(ll[0]),np.log10(ll[-1]),200)
my_scales = np.logspace(np.log10(2),np.log10(300),200)
scales = scg.periods2scales(my_scales)

#

cwt = scg.CWT(Jn_an-np.mean(Jn_an),scales=scales)

plt.close()
fig, ((ax0),(ax1),(ax2)) = plt.subplots(3,1,figsize=(8,2*(1+4+4)),gridspec_kw={'height_ratios':[1,4,4]})

im0 = ax0.plot(sco,Jn_an,color='black')
im0 = ax0.plot(sco[rec_ind[:]],Jn_an[rec_ind[:]],'*b')
ax0.set_xlim(sco[0],sco[-1]);ax0.set_ylim(0.,np.max(Jn_an))
ax0.set_xlabel('s [$d_i$]');ax0.set_ylabel('$|Jn|$')

ax1.contourf(PVI[:,:].T,64)

im2 = ax2.contourf(x,y,Jn[:,:,0],32)
im2 = ax2.scatter(xco,yco,s=1,c='red')
im2 = ax2.scatter(xco[rec_ind[:]],yco[rec_ind[:]],s=50,c='darkgoldenrod')
im2 = ax2.scatter(xco[Jn_an>0.25],yco[Jn_an>0.25],s=25,c='blue')
ax2.set_xlabel('x [$d_i$]');ax2.set_ylabel('y [$d_i$]');ax2.set_title('$|J|$ and PVI peaks')
ax2.set_xlim(x[0],x[-1]);ax2.set_ylim(y[0],y[-1])

ax3 = scg.cws(cwt,figsize=(8,2*4), xlabel='s',ylabel='scales')#,yscale='log')#,clim=(0.,1.1))

plt.tight_layout()
plt.show()
plt.close()

#

#f = pywt.scale2frequency(MWname, scales)/ds
cwtmatr, freqs = pywt.cwt(Jn_an-np.mean(Jn_an),scales,MWname,sampling_period=ds)
cwtmatrsq = np.abs(cwtmatr)

plt.close()
fig, ((ax0),(ax1),(ax2)) = plt.subplots(3,1,figsize=(8,2*(1+4+4)),gridspec_kw={'height_ratios':[1,4,4]})

im0 = ax0.plot(sco,Jn_an,color='black')
im0 = ax0.plot(sco[rec_ind[:]],Jn_an[rec_ind[:]],'*b')
ax0.set_xlim(sco[0],sco[-1]);ax0.set_ylim(0.,np.max(Jn_an))
ax0.set_xlabel('s [$d_i$]');ax0.set_ylabel('$|Jn|$')

ax1.contourf(PVI[:,:].T,64)

#im2 = ax2.pcolormesh(sco, freqs, cwtmatr, vmin=0, cmap = "inferno" )
#plt.colorbar(im2,ax=ax2)
#ax2.set_xlim(sco[0],sco[-1]);ax2.set_ylim(freqs[-1],freqs[0])
#ax2.set_ylabel('1/scales')
#ax2.set_xlabel('s')
#ax2.set_yscale('log')

im2 = ax2.pcolormesh(sco,my_scales,cwtmatrsq,vmin=0,cmap="inferno")
#plt.colorbar(im2,ax=ax2)
ax2.set_xlim(sco[0],sco[-1]);ax2.set_ylim(my_scales[0],my_scales[-1])
ax2.set_ylabel('scales')
ax2.set_xlabel('s')

plt.tight_layout()
plt.show()
plt.close()

#

#f = pywt.scale2frequency(MWname, scales)/ds
cwtmatr1,_ = pywt.cwt(Bx_an-np.mean(Bx_an),scales,MWname,sampling_period=ds)
cwtmatr2,_ = pywt.cwt(By_an-np.mean(By_an),scales,MWname,sampling_period=ds)
cwtmatr3,_ = pywt.cwt(Bz_an-np.mean(Bz_an),scales,MWname,sampling_period=ds)
cwtmatrsq = np.sqrt(np.abs(cwtmatr1)**2 + np.abs(cwtmatr2)**2 + np.abs(cwtmatr3)**2)

sco0 = sco[0] #2030. #sco[0]
sco1 = sco[-1] #2080. #sco[-1]
i0 = np.argmin(np.abs(sco-sco0))
i1 = np.argmin(np.abs(sco-sco1))

plt.close()
fig, ((ax0),(ax1),(ax2)) = plt.subplots(3,1,figsize=(1.5*4,1.5*(1+4+4)),gridspec_kw={'height_ratios':[1,4,4]})

im0 = ax0.plot(sco,Jn_an,color='black')
im0 = ax0.plot(sco[rec_ind[:]],Jn_an[rec_ind[:]],'*b')
#ax0.set_xlim(sco[0],sco[-1]);
ax0.set_ylim(0.,np.max(Jn_an))
#ax0.set_xlabel('s [$d_i$]');
ax0.set_ylabel('$|Jn|$')
ax0.set_title('$|J|$ from virt. sat.')
ax0.set_xlim(sco[i0],sco[i1])

ax1.contourf(sco[i0:i1],ll,PVI[i0:i1,:].T,64)
#ax1.set_xlabel('s [$d_i$]');
ax1.set_ylabel('scales [ds='+str(round(ds,4))+' $d_i$]')
ax1.set_title('PVI scalogram')
ax1.set_xlim(sco[i0],sco[i1])

im2 = ax2.pcolormesh(sco[i0:i1],my_scales,cwtmatrsq[:,i0:i1],vmin=0,cmap="inferno")
#plt.colorbar(im2,ax=ax2)
#ax2.set_xlim(sco[0],sco[-1]);
ax2.set_ylim(my_scales[0],my_scales[-1])
ax2.set_ylabel('scales [ds='+str(round(ds,4))+' $d_i$]')
ax2.set_xlabel('s [$d_i$]')
ax2.set_title(MWname+' scalogram')
ax2.set_yscale('log')
ax2.set_xlim(sco[i0],sco[i1])

plt.tight_layout()
plt.show()
plt.close()

"""
#
from scipy.signal import spectrogram

fig, ax3 = plt.subplots(1,1, sharex = True, figsize = (13.2,4))

data = Bz_cut_normed.copy() 
f_s = 1./dx
Dx = 256*dx # window length in [di]
Nw = np.int(2**np.round(np.log2(Dx * f_s))) # Number of datapoints within window
f, t_, Sxx = spectrogram(data, f_s, window='hanning', nperseg=Nw, noverlap = None, detrend=False, scaling='spectrum')
Df  =  f[1] - f[0]
Dt_ = t_[1] - t_[0]
t2  = t_ + t[0] - Dt_

im = ax3.pcolormesh(t2, f + Df/2, np.sqrt(2*Sxx), cmap = "inferno_r")#, alpha = 0.5)
ax3.grid(True)
ax3.set_ylabel("Frequency in [Hz]")
ax3.set_ylim(0, 10)
ax3.set_xlim(np.min(t2),np.max(t2))
ax3.set_title("Spectrogram using FFT and Hanning window")
ax3.set_yscale('log')

plt.show()

#

fs = 100.
f, x_, Sxx = spectrogram(data, fs)

plt.close()
fig,ax = plt.subplots(2,1,figsize=(12,4))
ax[0].plot(x,data)

ax[1].pcolormesh(x_,f,Sxx)

plt.show()
plt.close()


#

fs = nx//100#10e3
N = nx#1e5
amp = 2 * np.sqrt(2)
noise_power = 0.01 * fs / 2
time = np.arange(N) / float(fs)
mod = 50*np.cos(2*np.pi*0.25*time)
carrier = amp * np.sin(2*np.pi*3e2*time + mod)
noise = np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
noise *= np.exp(-time/5)
x = carrier + noise

f, t, Sxx = spectrogram(x, fs)
plt.pcolormesh(t, f, Sxx)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()

#

scales = np.linspace(2,nx//2,128)
data_mean = np.zeros((nx),dtype=np.float64)
coefs_mean = np.zeros((scales.shape[0],nx),dtype=np.cdouble)
cnt = 0
for y0 in range(170):
    data = B[2,:,y0,0]
    data -= np.mean(data)
    data = np.roll(data,-300)
    data_mean += data
#
    coefs,freq = pywt.cwt(data,scales,'cmor25.-0.1',sampling_period=int(1./dx))
    coefs_mean += coefs
    cnt += 1


data_mean = np.divide(data_mean,float(cnt))
coefs_mean = np.divide(coefs_mean,float(cnt))

plt.close()
fig,ax = plt.subplots(2,1,figsize=(12,8))
ax[0].plot(x,data_mean)
im = ax[1].contourf(x,scales*dx,np.real(coefs_mean)**2+np.imag(coefs_mean)**2,64)
#plt.colorbar(im,ax=ax[1])
plt.show()
"""
