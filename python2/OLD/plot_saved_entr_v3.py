#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill plot already computed enropies\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

wl.Can_I()

run_name = run.meta['run_name']
in_dir = 'entropy_v3'
out_dir = 'entropy_manual_plot'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']

#declare arrays
t_vec = []

m_Sfp = []
r_m_Sfp = []
m_adv_Sfp = []
m_gPiD_Sfp = []
m_nPiD_Sfp = []
m_vhf_Sfp = []
diff_Sfp = []

m_Sfe = []
r_m_Sfe = []
m_adv_Sfe = []
m_gPiD_Sfe = []
m_nPiD_Sfe = []
m_vhf_Sfe = []
diff_Sfe = []

m_Sgp = []
r_m_Sgp = []
m_adv_Sgp = []
m_nPiD_Sgp = []
m_vhf_Sgp = []
m_ng_Sgp = []
diff_Sgp = []

if w_ele:
    m_Sge = []
    r_m_Sge = []
    m_adv_Sge = []
    m_nPiD_Sge = []
    m_vhf_Sge = []
    m_ng_Sge = []
    diff_Sge = []

#read from file
f = open(opath+'/'+in_dir+'/'+run_name+'/'+'entr_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.txt','r')
ll = f.readlines()
f.close()
for i in range(len(ll)):
    l = ll[i].split()

    t_vec.append(float(l[0]))

    m_Sfp.append(float(l[1]))
    r_m_Sfp.append(float(l[2]))
    m_adv_Sfp.append(float(l[3]))
    m_gPiD_Sfp.append(float(l[4]))
    m_nPiD_Sfp.append(float(l[5]))
    m_vhf_Sfp.append(float(l[6]))
    diff_Sfp.append(float(l[7]))

    m_Sfe.append(float(l[8]))
    r_m_Sfe.append(float(l[9]))
    m_adv_Sfe.append(float(l[10]))
    m_gPiD_Sfe.append(float(l[11]))
    m_nPiD_Sfe.append(float(l[12]))
    m_vhf_Sfe.append(float(l[13]))
    diff_Sfe.append(float(l[14]))

    m_Sgp.append(float(l[15]))
    r_m_Sgp.append(float(l[16]))
    m_adv_Sgp.append(float(l[17]))
    m_nPiD_Sgp.append(float(l[18]))
    m_vhf_Sgp.append(float(l[19]))
    m_ng_Sgp.append(float(l[20]))
    diff_Sgp.append(float(l[21]))

    if w_ele:
        m_Sge.append(float(l[22]))
        r_m_Sge.append(float(l[23]))
        m_adv_Sge.append(float(l[24]))
        m_nPiD_Sge.append(float(l[25]))
        m_vhf_Sge.append(float(l[26]))
        m_ng_Sge.append(float(l[27]))
        diff_Sge.append(float(l[28]))
del ll
del l

#from list to numpy.array
t_vec = np.array(t_vec)

m_Sfp = np.array(m_Sfp)
r_m_Sfp = np.array(r_m_Sfp)
m_adv_Sfp = np.array(m_adv_Sfp)
m_gPiD_Sfp = np.array(m_gPiD_Sfp)
m_nPiD_Sfp = np.array(m_nPiD_Sfp)
m_vhf_Sfp = np.array(m_vhf_Sfp)
diff_Sfp = np.array(diff_Sfp)

m_Sfe = np.array(m_Sfe)
r_m_Sfe = np.array(r_m_Sfe)
m_adv_Sfe = np.array(m_adv_Sfe)
m_gPiD_Sfe = np.array(m_gPiD_Sfe)
m_nPiD_Sfe = np.array(m_nPiD_Sfe)
m_vhf_Sfe = np.array(m_vhf_Sfe)
diff_Sfe = np.array(diff_Sfe)

m_Sgp = np.array(m_Sgp)
r_m_Sfe = np.array(r_m_Sfe)
m_adv_Sgp = np.array(m_adv_Sgp)
m_nPiD_Sgp = np.array(m_nPiD_Sgp)
m_vhf_Sgp = np.array(m_vhf_Sgp)
m_ng_Sgp = np.array(m_ng_Sgp)
diff_Sgp = np.array(diff_Sgp)

if w_ele:
    m_Sge = np.array(m_Sge)
    r_m_Sge = np.array(r_m_Sge)
    m_adv_Sge = np.array(m_adv_Sge)
    m_nPiD_Sge = np.array(m_nPiD_Sge)
    m_vhf_Sge = np.array(m_vhf_Sge)
    m_ng_Sge = np.array(m_ng_Sge)
    diff_Sge = np.array(diff_Sge)

#plot
ml.create_path(opath+'/'+out_dir)

plt.close('all')
fig,ax = plt.subplots(2,2,figsize=(4*2,4*2))

ax[0,0].plot(t_vec,r_m_Sfp,label='rate')
ax[0,0].plot(t_vec,m_adv_Sfp,label='adv')
ax[0,0].plot(t_vec,m_gPiD_Sfp,label='gPiD')
ax[0,0].plot(t_vec,m_nPiD_Sfp,label='nPiD')
ax[0,0].plot(t_vec,m_vhf_Sfp,label='vhf')
#ax[0,0].plot(t_vec,diff_Sfp,label='diff')
ax[0,0].set_xlabel('t')
ax[0,0].set_title('Isotropic protons entropy')
ax[0,0].legend()

ax[0,1].plot(t_vec,r_m_Sfe,label='rate')
ax[0,1].plot(t_vec,m_adv_Sfe,label='adv')
ax[0,1].plot(t_vec,m_gPiD_Sfe,label='gPiD')
ax[0,1].plot(t_vec,m_nPiD_Sfe,label='nPiD')
ax[0,1].plot(t_vec,m_vhf_Sfe,label='vhf')
#ax[0,1].plot(t_vec,diff_Sfe,label='diff')
ax[0,1].set_xlabel('t')
ax[0,1].set_title('Isotropic electrons entropy')
ax[0,1].legend()

ax[1,0].plot(t_vec,r_m_Sgp,label='rate')
ax[1,0].plot(t_vec,m_adv_Sgp,label='adv')
ax[1,0].plot([],[])
ax[1,0].plot(t_vec,m_nPiD_Sgp,label='nPiD')
ax[1,0].plot(t_vec,m_vhf_Sgp,label='vhf')
ax[1,0].plot(t_vec,m_ng_Sgp,label='ng')
#ax[1,0].plot(t_vec,diff_Sgp,label='diff')
ax[1,0].set_xlabel('t')
ax[1,0].set_title('anisotropic protons entropy')
ax[1,0].legend()

if w_ele:
    ax[1,1].plot(t_vec,r_m_Sge,label='rate')
    ax[1,1].plot(t_vec,m_adv_Sge,label='adv')
    ax[1,1].plot([],[])
    ax[1,1].plot(t_vec,m_nPiD_Sge,label='nPiD')
    ax[1,1].plot(t_vec,m_vhf_Sge,label='vhf')
    ax[1,1].plot(t_vec,m_ng_Sge,label='ng')
#    ax[1,1].plot(t_vec,diff_Sge,label='diff')
    ax[1,1].set_xlabel('t')
    ax[1,1].set_title('anisotropic electrons entropy')
    ax[1,1].legend()

plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'entr_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
#plt.show()
plt.close()

#plot norm
ml.create_path(opath+'/'+out_dir)

plt.close('all')
fig,ax = plt.subplots(2,2,figsize=(4*2,4*2))

ax[0,0].plot(t_vec,np.divide(r_m_Sfp,r_m_Sfp[0]),label='rate')
ax[0,0].plot(t_vec,np.divide(m_adv_Sfp,r_m_Sfp[0]),label='adv')
ax[0,0].plot(t_vec,np.divide(m_gPiD_Sfp,r_m_Sfp[0]),label='gPiD')
ax[0,0].plot(t_vec,np.divide(m_nPiD_Sfp,r_m_Sfp[0]),label='nPiD')
ax[0,0].plot(t_vec,np.divide(m_vhf_Sfp,r_m_Sfp[0]),label='vhf')
ax[0,0].set_xlabel('t')
ax[0,0].set_title('Isotropic protons entropy')
ax[0,0].legend()

ax[0,1].plot(t_vec,np.divide(r_m_Sfe,r_m_Sfe[0]),label='rate')
ax[0,1].plot(t_vec,np.divide(m_adv_Sfe,r_m_Sfe[0]),label='adv')
ax[0,1].plot(t_vec,np.divide(m_gPiD_Sfe,r_m_Sfe[0]),label='gPiD')
ax[0,1].plot(t_vec,np.divide(m_nPiD_Sfe,r_m_Sfe[0]),label='nPiD')
ax[0,1].plot(t_vec,np.divide(m_vhf_Sfe,r_m_Sfe[0]),label='vhf')
ax[0,1].set_xlabel('t')
ax[0,1].set_title('Isotropic electrons entropy')
ax[0,1].legend()

ax[1,0].plot(t_vec,np.divide(r_m_Sgp,r_m_Sgp[0]),label='rate')
ax[1,0].plot(t_vec,np.divide(m_adv_Sgp,r_m_Sgp[0]),label='adv')
ax[1,0].plot([],[])
ax[1,0].plot(t_vec,np.divide(m_nPiD_Sgp,r_m_Sgp[0]),label='nPiD')
ax[1,0].plot(t_vec,np.divide(m_vhf_Sgp,r_m_Sgp[0]),label='vhf')
ax[1,0].plot(t_vec,np.divide(m_ng_Sgp,r_m_Sgp[0]),label='ng')
ax[1,0].set_xlabel('t')
ax[1,0].set_title('anisotropic protons entropy')
ax[1,0].legend()

if w_ele:
    ax[1,1].plot(t_vec,np.divide(r_m_Sge,r_m_Sge[0]),label='rate')
    ax[1,1].plot(t_vec,np.divide(m_adv_Sge,r_m_Sge[0]),label='adv')
    ax[1,1].plot([],[])
    ax[1,1].plot(t_vec,np.divide(m_nPiD_Sge,r_m_Sge[0]),label='nPiD')
    ax[1,1].plot(t_vec,np.divide(m_vhf_Sge,r_m_Sge[0]),label='vhf')
    ax[1,1].plot(t_vec,np.divide(m_ng_Sge,r_m_Sge[0]),label='ng')
    ax[1,1].set_xlabel('t')
    ax[1,1].set_title('anisotropic electrons entropy')
    ax[1,1].legend()

plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'entr_norm_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
#plt.show()
plt.close()

#plot entr norm
ml.create_path(opath+'/'+out_dir)

plt.close('all')
fig,ax = plt.subplots(1,1,figsize=(4*2,4*2))

ax.plot(t_vec,np.divide(m_Sfp-m_Sfp[0],m_Sfp[0]),label='Sfp')
ax.plot(t_vec,np.divide(m_Sfe-m_Sfe[0],m_Sfe[0]),label='Sfe')
ax.plot(t_vec,np.divide(m_Sgp-m_Sgp[0],m_Sgp[0]),label='Sgp')
if w_ele:
    ax.plot(t_vec,np.divide(m_Sge-m_Sge[0],m_Sge[0]),label='Sge')
ax.set_xlabel('t')
ax.set_title('norm. variation of entropy')
ax.legend()

plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'entr_variation_norm_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
#plt.show()
plt.close()

