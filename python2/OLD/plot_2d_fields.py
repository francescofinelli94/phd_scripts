#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of Jz, Bz, E_parl and Ve in plane.\n')

#------------------------------------------------------
#Init
#----
input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']

plt_show_flg = True
zoom_flg = True
rms_flg = False
eparl_plane_flg = True
Veperp_flg = True

Bz = {}
Jz = {}
eparl = {}
Psi = {}
Veplane = {}
t = {}
x = {}
y = {}

run_name_vec = []
run_labels = {}
ind_dict = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind,_,_ = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    ind_dict[run_name] = ind
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    x_ = run.meta['x']
    y_ = run.meta['y']
    code_name = run.meta['code_name']
#
    if code_name == 'HVM':
        smooth_flag = False
        calc = fibo('calc')
        calc.meta = run.meta
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
        B0 = 1.
        n0 = 1.
        V0 = 1.
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
        gfsp = 2
        gfse = 2
        B0 = 0.01
        n0 = 1./(4.*np.pi)
        V0 = 0.01
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
#
    #get data
    #magnetic fields and co.
    E,B = run.get_EB(ind)
#
    Psi_ = wl.psi_2d(B[:-1,...,0],run.meta)
    if smooth_flag:
        Psi_ = gf(Psi_,[gfsp,gfsp],mode='wrap')
    Psi[run_name] = Psi_[ixmin:ixmax,iymin:iymax]/B0
    del Psi_
#
    Bz_ = B[2,:,:,0] - run.meta['Bz0']
    if smooth_flag:
        Bz_ = gf(Bz_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        Bz_rms = np.sqrt( np.mean( Bz_[ixmin:ixmax,iymin:iymax]*Bz_[ixmin:ixmax,iymin:iymax] ) )###
        Bz[run_name] = Bz_[ixmin:ixmax,iymin:iymax]/Bz_rms###
    else:
        Bz[run_name] = Bz_[ixmin:ixmax,iymin:iymax]/B0###
    del Bz_
#
    if eparl_plane_flg:
        eparl_ = np.divide(E[0]*B[0] + E[1]*B[1],
                           np.sqrt(B[0]*B[0] + B[1]*B[1]))[...,0]
        #eparl_ = np.divide(E[0]*B[0] + E[1]*B[1],
        #                   np.sqrt(B[0]*B[0] + B[1]*B[1] + B[2]*B[2]))[...,0]
    else:
        eparl_ = np.divide(np.sum(np.multiply(E,B),axis=0),
                           np.sqrt(np.sum(np.multiply(B,B),axis=0)))[...,0]
    del E
    if smooth_flag:
        eparl_ = gf(eparl_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        eparl_rms = np.sqrt(np.mean(eparl_[ixmin:ixmax,iymin:iymax]*eparl_[ixmin:ixmax,iymin:iymax]))
        eparl[run_name] = eparl_[ixmin:ixmax,iymin:iymax]/eparl_rms
    else:
        eparl_rms = np.sqrt(np.mean(eparl_[ixmin:ixmax,iymin:iymax]*eparl_[ixmin:ixmax,iymin:iymax]))
        eparl[run_name] = eparl_[ixmin:ixmax,iymin:iymax]/eparl_rms
    del eparl_
#
    n_p,u_p = run.get_Ion(ind)
    if code_name == 'HVM':
        Jx_,Jy_,Jz_ = calc.calc_curl(B[0],B[1],B[2])
        u_e = np.empty(B.shape,dtype=type(B[0,0,0,0]))
        u_e[0] = u_p[0] - np.divide(Jx_,n_p)
        u_e[1] = u_p[1] - np.divide(Jy_,n_p)
        u_e[2] = u_p[2] - np.divide(Jz_,n_p)
        n_e = n_p[:,:,:]
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
        Jz_ = n_p*u_p[2] - n_e*u_e[2]
        #del n_e
    del n_p,u_p
#
    Jz_ = Jz_[...,0]
    if smooth_flag:
        Jz_ = gf(Jz_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        Jz_rms = np.sqrt( np.mean( Jz_[ixmin:ixmax,iymin:iymax]*Jz_[ixmin:ixmax,iymin:iymax] ) )###
        Jz[run_name] = Jz_[ixmin:ixmax,iymin:iymax]/Jz_rms###
    else:
        Jz[run_name] = Jz_[ixmin:ixmax,iymin:iymax]/(n0*V0)###
    del Jz_
#
    if Veperp_flg:
        u_eparl = np.divide(np.sum(np.multiply(u_e,B),axis=0),
                             np.sqrt(np.sum(np.multiply(B,B),axis=0)))
        u_eplane = (n_e*np.sqrt(np.sum(np.multiply(u_e,u_e),axis=0)-
                           np.multiply(u_eparl,u_eparl)))[...,0]
        del u_eparl
    else:
        u_eplane = np.sqrt(u_e[0]**2+u_e[1]**2)[...,0]
    del u_e,B
    if smooth_flag:
        u_eplane = gf(u_eplane,[gfse,gfse],mode='wrap')
    if rms_flg:
        u_eplane_rms = np.sqrt( np.mean( 
                       u_eplane[ixmin:ixmax,iymin:iymax]*u_eplane[ixmin:ixmax,iymin:iymax] ) )###
        Veplane[run_name] = u_eplane[ixmin:ixmax,iymin:iymax]/u_eplane_rms###
    else:
        Veplane[run_name] = u_eplane[ixmin:ixmax,iymin:iymax]/(n0*V0)###
    del u_eplane
#
    t[run_name] =  times[ind]
    del times
#
    x[run_name] = x_[ixmin:ixmax]
    y[run_name] = y_[iymin:iymax]
    del x_,y_
#
    gc.collect()

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
fig,ax = plt.subplots(4,3,figsize=(18,14),sharex=True,sharey=True)
fig.subplots_adjust(hspace=.06,wspace=.03,top=.95,bottom=.1,left=.055,right=1.075)#.5#1.07

vminJz = ml.min_dict(Jz)
vmaxJz = ml.max_dict(Jz)
vmin = ml.min_dict(Bz)
vmax = ml.max_dict(Bz)
vminBz = -max(vmax,-vmin)
vmaxBz = max(vmax,-vmin)
vmin = ml.min_dict(eparl)
vmax = ml.max_dict(eparl)
vmineparl = -max(vmax,-vmin)
vmaxeparl = max(vmax,-vmin)
vminVeplane = ml.min_dict(Veplane)
vmaxVeplane = ml.max_dict(Veplane)*0.+1.6
for j,run_name in enumerate(run_name_vec):
    i = 0
    im0 = ax[i,j].pcolormesh(x[run_name],y[run_name],Bz[run_name].T,shading='gouraud',
                           vmin=vminBz,vmax=vmaxBz,cmap='PuOr')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[i,j].set_title(run_labels[run_name])
#
    i = 1
    im1 = ax[i,j].pcolormesh(x[run_name],y[run_name],eparl[run_name].T,shading='gouraud',
                             cmap='seismic',
                             vmin=vmineparl,vmax=vmaxeparl)
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
#
    i = 2
    im2 = ax[i,j].pcolormesh(x[run_name],y[run_name],Jz[run_name].T,shading='gouraud',
                             vmin=vminJz,vmax=vmaxJz,cmap='Reds_r')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
#
    i = 3
    im3 = ax[i,j].pcolormesh(x[run_name],y[run_name],Veplane[run_name].T,shading='gouraud',
                          vmin=vminVeplane,vmax=vmaxVeplane,cmap='hot')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    ax[i,j].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')

cb0 = plt.colorbar(im0,ax=ax[0,:],pad=.007)
if rms_flg:
    label_ = '$\Delta B_z / \overline{B_z}$'
else:
    label_ = '$\Delta B_z / B_0$'
cb0.set_label(label_,rotation=270,labelpad=20)#20)

cb1 = plt.colorbar(im1,ax=ax[1,:],pad=.007)
if rms_flg:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
else:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
cb1.set_label(label_,rotation=270,labelpad=35)#40)

cb2 = plt.colorbar(im2,ax=ax[2,:],pad=.007)
if rms_flg:
    label_ = '$J_z / \overline{J_z}$'
else:
    label_ = '$J_z / ( e n_0 v_A )$'
cb2.set_label(label_,rotation=270,labelpad=25)

cb3 = plt.colorbar(im3,ax=ax[3,:],pad=.007)
if rms_flg:
    label_ = '$u_{\mathrm{e}}^{(\\text{plane})} / \overline{u_e^{(\\text{plane})}}$'
else:
    label_ = '$u_{\mathrm{e}}^{(\\text{plane})} / v_A$'
cb3.set_label(label_,rotation=270,labelpad=45)#40)

if plt_show_flg:
    plt.show()

out_dir = 'plot_2d_fields'
ml.create_path(opath+'/'+out_dir)
out_dir += '/comp'
fig_name = 'DBz_'
if eparl_plane_flg:
    fig_name += 'Eparlplane_'
else:
    fig_name += 'Eparl_'
fig_name += 'Jz_'
if Veperp_flg:
    fig_name += 'Veperp'
else:
    fig_name += 'Veplane'
for run_name in run_name_vec:
    out_dir += '__' + run_name
    fig_name += '__' + run_labels[run_name] + '_ind%d'%ind_dict[run_name]

if zoom_flg:
    fig_name += '__zoom'
if rms_flg:
    fig_name += '__rms'
fig_name += '.png'
ml.create_path(opath+'/'+out_dir)
fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
