#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
#
#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy increments at a given scale langth, both in parallel and perpendicular direction (relative to the local field), averaged over some time exits.\n')
print('\nThen, some delta_Anis. ranges can be selected to be traced back in x-y and beta_parl-anis. spaces.\n')
#
#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()
input_file.close()
#
#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
code_name = l0[1]
ipath = l1[0]
opath = l2[0]
#
if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')
try:
    run.get_meta()
except:
    run.get_meta(l5[0])
#
w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)
#
time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
#
if code_name == 'HVM':
    mime = run.meta['mime']
elif code_name == 'iPIC':
    mime = abs(run.meta['msQOM'][0]/run.meta['msQOM'][1])
else:
    print('\nWhat code is this?\n')
#
if code_name == 'HVM':
    teti = run.meta['teti']
elif code_name == 'iPIC':
    P0 = run.get_Pspec(1,'0') #HARDCODED stag
    P3 = run.get_Pspec(1,'3') #HARDCODED stag
    teti = np.mean([np.mean(np.divide(P0[0,0],P3[0,0])),
                    np.mean(np.divide(P0[1,1],P3[1,1])),
                    np.mean(np.divide(P0[2,2],P3[2,2]))])
    del P0,P3
else:
    print('\nWhat code is this?\n')
#
print('\nMetadata loaded\n')
#
print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)
#
#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#-------------------------------------------------------
#hardcoded inputs
#----------------
lvec = np.array([4,8,16,32,64])*run.meta['dx']
mar = ['o','^','s','x','*']
col = ['b','g','r','c','m','y','k']
dth = np.pi/20
m_def = 5
hmin = 3
out_dir = 'anis_incr_backtracking'
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])
#
print("\nlvec = "+str(lvec))
print("dth = "+str(dth))
print("m_def = "+str(m_def))
print("hmin = "+str(hmin))
print("out_dir = "+out_dir)
print("cut1 = y("+str(cut1)+") = "+str(y2-5.))
print("cut2 = y("+str(cut2)+") = "+str(y2+5.)+"\n")
#
#see Hellinger et al. 2006
a_PC = 0.43; b_PC = 0.42; b0_PC =-0.0004
a_MR = 0.77; b_MR = 0.76; b0_MR =-0.0160
a_PF =-0.47; b_PF = 0.53; b0_PF = 0.5900
a_OF =-1.40; b_OF = 1.00; b0_OF =-0.1100
#
#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)
#
#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]
#
#------------------------------------------------------
#pdf extraction function
#-----------------------
@njit
def fill_hist_function_2d(field,B,lvec,dth,dd,nn,cut1,cut2):
    dx = dd[0]
    dy = dd[1]
    nx = nn[0]
    ny = nn[1]
    parl = []
    perp = []
#
    nl = len(lvec)
    lgridx = np.array([int(x/dx) for x in lvec])
    lgridy = np.array([int(x/dy) for x in lvec])
    distx = np.array([0 for x in range(nl+1)])
    distx[1:] = lgridx
    disty = np.array([0 for x in range(nl+1)])
    disty[1:] = lgridy
#
    parl.append([-1.,-1.])
    perp.append([-1.,-1.])
    for (i,j) in [(i,j) for i in range(nx) for j in range(ny)]:
        for (di,dj) in [(di,dj) for di in distx for dj in disty if (di*dj == 0 and di+dj != 0)]:
            if (j > cut1 and j < cut2):
                continue
            ii = (i+di+nx)%nx
            jj = (j+dj+ny)%ny
            if (jj > cut1 and jj < cut2):
                continue
            if (j-cut1)*(jj-cut1) < 0:
                continue
#
            b = np.array([0.5*(B[0,i,j] + B[0,ii,jj]),
                          0.5*(B[1,i,j] + B[1,ii,jj]),
                          0.5*(B[2,i,j] + B[2,ii,jj])])
            l = np.array([di*dx,dj*dy,0.])
#
            th = np.arctan2(np.linalg.norm(np.cross(l,b)),np.abs(np.dot(l,b)))
#
            if th < dth:
                lnorm = np.linalg.norm(l)
                parl.append([lnorm,field[i,j]-field[ii,jj]])
            elif th > (0.5*np.pi-dth):
                lnorm = np.linalg.norm(l)
                perp.append([lnorm,field[i,j]-field[ii,jj]])
            else:
                continue
#
    return np.array(parl),np.array(perp)
#
#------------------------------------------------------
#pdf backtracking function
#-----------------------
@njit
def backtrack_hist_function_2d(field,B,lvec,dth,dd,nn,cut1,cut2,ranges):
    dx = dd[0]
    dy = dd[1]
    nx = nn[0]
    ny = nn[1]
    xparl = []
    yparl = []
    xperp = []
    yperp = []
#   
    nl = len(lvec)
    lgridx = np.array([int(x/dx) for x in lvec])
    lgridy = np.array([int(x/dy) for x in lvec])
    distx = np.array([0 for x in range(nl+1)])
    distx[1:] = lgridx
    disty = np.array([0 for x in range(nl+1)])
    disty[1:] = lgridy
#
    xparl.append([-1.,-1.,-1.,-1.])
    yparl.append([-1.,-1.,-1.,-1.])
    xperp.append([-1.,-1.,-1.,-1.])
    yperp.append([-1.,-1.,-1.,-1.])
    for (i,j) in [(i,j) for i in range(nx) for j in range(ny)]:
        for (di,dj) in [(di,dj) for di in distx for dj in disty if (di*dj == 0 and di+dj != 0)]:
            if (j > cut1 and j < cut2):
                continue
            ii = (i+di+nx)%nx
            jj = (j+dj+ny)%ny
            if (jj > cut1 and jj < cut2):
                continue
            if (j-cut1)*(jj-cut1) < 0:
                continue
#
            b = np.array([0.5*(B[0,i,j] + B[0,ii,jj]),
                          0.5*(B[1,i,j] + B[1,ii,jj]),
                          0.5*(B[2,i,j] + B[2,ii,jj])])
            l = np.array([di*dx,dj*dy,0.])
#
            th = np.arctan2(np.linalg.norm(np.cross(l,b)),np.abs(np.dot(l,b)))
#
            if (th >= dth) and (th <= (0.5*np.pi-dth) ):
                continue
#
            dA = field[i,j]-field[ii,jj]
            flag = False
            r = -1.
            for i in range(len(ranges[:,0])):
                if flag:
                    continue
                if (dA > ranges[i,0]) and (dA < ranges[i,1]):
                    flag = True
                    r = float(i)
            if not flag:
                continue
#
            if th < dth:
                lnorm = np.linalg.norm(l)
                xparl.append([r,lnorm,float(i),float(ii)])
                yparl.append([r,lnorm,float(j),float(jj)])
            elif th > (0.5*np.pi-dth):
                lnorm = np.linalg.norm(l)
                xperp.append([r,lnorm,float(i),float(ii)])
                yperp.append([r,lnorm,float(j),float(jj)])
            else:
                continue
#
    return np.array(xparl),np.array(yparl),np.array(xperp),np.array(yperp)
#
#--------------------------------------------------------
#to-fit function
#----------------
def f(x,a,b):
    return a + b*x*x
#
#--------------------------------------------------------
#plotting function
#-----------------
def plot_func(axs,data,hmin,title,num_bins,m=m_def):
    h0 = 10000
    h1 = 0
    b0 = 0
    b1 = 0
    flag = False
    for l in lvec:
        h,b = np.histogram(data[data[:,0]==l,1],num_bins)
        b = b[:-1]
        b = b + 0.5*(b[1]-b[0])
        b = b[h[:] >= hmin]
        h = h[h[:] >= hmin]
        n = len(h)
        if n > 2*m_def:
            flag = True
            im = axs.plot(b,h,label=str(round(l,2))+'di')
            hmax = np.max(h)
            bmin = np.min(b)
            bmax = np.max(b)
            h0 = min(h0,np.min(h))
            h1 = max(h1,hmax)
            b0 = min(b0,bmin)
            b1 = max(b1,bmax)
            popt,pcov = cfit(f,b[(m//2)*n//m:(m//2+1)*n//m],np.log(h[(m//2)*n//m:(m//2+1)*n//m]))
            axs.plot(b,np.exp(f(b,*popt)),linestyle=':',color=im[-1].get_color())
    if flag:
        axs.legend()
        axs.set_yscale('log')
        axs.set_xlim(b0,b1)
        axs.set_ylim(h0,h1)
    axs.set_xlabel('Delta_A')
    axs.set_ylabel('counts')
    axs.set_title(title)
    return None
#
#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b
#
#-------------------------------------------------------
#windlike plot function
#-------------
def windlike_plot(ax,xdata,ydata,species_name,species_tag,b0_mult,t=None):
    h,xbin,ybin=np.histogram2d(np.log(np.array(xdata.flat)),np.log(np.array((ydata+1.).flat)),250)
    X,Y = np.meshgrid(np.exp(xbin),np.exp(ybin))
    im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_PC,b_PC,b0_PC*b0_mult),label='Prot. cycl.')
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_MR,b_MR,b0_MR*b0_mult),label='Mirror')
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_PF,b_PF,b0_PF*b0_mult),label='Parl. firehose')
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_OF,b_OF,b0_OF*b0_mult),label='Obl. firehose')
    ax.set_ylim(np.exp(ybin[0]),np.exp(ybin[-1]));ax.legend()
    ax.set_xlabel('Parallel '+species_name+' beta')
    ax.set_ylabel('P_perp_'+species_tag+'/P_parl_'+species_tag)
    if t != None:
        ax.set_title(species_name+' anisotropy '+t)
    else:
        ax.set_title(species_name+' anisotropy')
    ax.set_xscale('log');ax.set_yscale('log');plt.colorbar(im,ax=ax)
#
#-------------------------------------------------------
#select over threshold points
#-------------
@njit
def over_th(anis,bparl,nx,ny,cut1,cut2,b0_mult):
    a_PC = 0.43; b_PC = 0.42; b0_PC =-0.0004
    a_MR = 0.77; b_MR = 0.76; b0_MR =-0.0160
    a_PF =-0.47; b_PF = 0.53; b0_PF = 0.5900
    a_OF =-1.40; b_OF = 1.00; b0_OF =-0.1100
    isoverMR = (anis+1.) > th_func(bparl,a_MR,b_MR,b0_MR*b0_mult)
    isoverOF = (anis+1.) < th_func(bparl,a_OF,b_OF,b0_OF*b0_mult)
    xMR = []
    yMR = []
    xOF = []
    yOF = []
    for i in range(nx):
        for j in range(ny):
            if (j > cut1) and (j < cut2):
                continue
            if isoverMR[i,j,0]:
                xMR.append(i)
                yMR.append(j)
            if isoverOF[i,j,0]:
                xOF.append(i)
                yOF.append(j)
    return np.array(xMR),np.array(yMR),np.array(xOF),np.array(yOF)
#
#---> loop over times <---
print " ",
anisp_parl = np.array([[-1.,-1.]])
anisp_perp = np.array([[-1.,-1.]])
anisp_tot = []
bparlp_tot = []
if w_ele:
    anise_parl = np.array([[-1.,-1.]])
    anise_perp = np.array([[-1.,-1.]])
    anise_tot = []
    bparle_tot = []
one_time_flag = True
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    E,B = run.get_EB(ind)
    del E
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp) - 1.
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        del pparle
    del B2
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #averaged fields
    if one_time_flag:
        one_time_flag = False
        B_avg = B
        anisp_avg = anisp
        bparlp_avg = bparlp
        if w_ele:
            anise_avg = anise
            bparle_avg = bparle
        cnt_ind = 1
    else:
        B_avg += B
        anisp_avg += anisp
        bparlp_avg += bparlp
        if w_ele:
            anise_avg += anise
            bparle_avg += bparle
        cnt_ind += 1
#
    #------------------------------------------------------
    #pdf of anis increments
    #----------------------
    tmp1,tmp2 = fill_hist_function_2d(anisp[...,0],B[...,0],lvec,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2],cut1,cut2)
    anisp_parl = np.concatenate((anisp_parl,tmp1))
    anisp_perp = np.concatenate((anisp_perp,tmp2))
    if w_ele:
        tmp1,tmp2 = fill_hist_function_2d(anise[...,0],B[...,0],lvec,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2],cut1,cut2)
        anise_parl = np.concatenate((anise_parl,tmp1))
        anise_perp = np.concatenate((anise_perp,tmp2))
#
    del B
    #------------------------------------------------------
    #data for windlike plot
    #-----------------------
    anisp_tot.append(np.concatenate((anisp[:,:cut1,0],anisp[:,cut2+1:,0]),axis=1))
    bparlp_tot.append(np.concatenate((bparlp[:,:cut1,0],bparlp[:,cut2+1:,0]),axis=1))
    if w_ele:
        anise_tot.append(np.concatenate((anise[:,:cut1,0],anise[:,cut2+1:,0]),axis=1))
        bparle_tot.append(np.concatenate((bparle[:,:cut1,0],bparle[:,cut2+1:,0]),axis=1))
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
del tmp1
del tmp2
anisp_parl = anisp_parl[1:,...]
anisp_perp = anisp_perp[1:,...]
B_avg = B_avg/cnt_ind
anisp_avg = anisp_avg/cnt_ind
bparlp_avg = bparlp_avg/cnt_ind
del anisp
del bparlp
if w_ele:
    anise_parl = anise_parl[1:,...]
    anise_perp = anise_perp[1:,...]
    anise_avg = anise_avg/cnt_ind
    bparle_avg = bparle_avg/cnt_ind
    del anise
    del bparle
#
#------------------------------------------------------
#plot pdf of anis increments
#---------------------------
ml.create_path(opath+'/'+out_dir)
plt.close('all')
if w_ele:
    nplot = 2
else:
    nplot = 1
fig,ax = plt.subplots(nplot,2,figsize=(8,4*nplot))
#
if code_name == 'iPIC':
    tmult = 0.01
else:
    tmult = 1.
#
if w_ele:
    axs = ax[0,0]
else:
    axs = ax[0]
plot_func(axs,anisp_parl,hmin,'Prot. anis. parl. incr. - cum. '+str(time[ind1]*tmult)+'->'+str(time[ind2]*tmult),50)
#
if w_ele:
    axs = ax[0,1]
else:
    axs = ax[1]
plot_func(axs,anisp_perp,hmin,'Prot. anis. perp. incr.',100)
#
if w_ele:
    axs = ax[1,0]
    plot_func(axs,anise_parl,hmin,'Elec. anis. parl. incr.',50)
#
    axs = ax[1,1]
    plot_func(axs,anise_perp,hmin,'Elec. anis. perp. incr.',100)
#
plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+out_dir+'/'+'anis_incr_'+run_name+'_cum_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
#
#------------------------------------------------------
#choose ranges
#-------------
ranges = []
print("\n")
num_r = ml.secure_input('number of ranges? ',10,True)
for i in range(num_r):
    ranges.append(input('range #'+str(i)+' (as: [r1,r2]): '))
ranges = np.array(ranges)
#
#------------------------------------------------------
#save plot pdf of anis increments
#---------------------------
ml.create_path(opath+'/'+out_dir)
plt.close('all')
if w_ele:
    nplot = 2
else:
    nplot = 1
fig,ax = plt.subplots(nplot,2,figsize=(8,4*nplot))
#
if code_name == 'iPIC':
    tmult = 0.01
else:
    tmult = 1.
#
if w_ele:
    axs = ax[0,0]
else:
    axs = ax[0]
for i in range(len(ranges[:,0])):
    axs.axvline(x=ranges[i,0],color=col[i])
    axs.axvline(x=ranges[i,1],color=col[i])
plot_func(axs,anisp_parl,hmin,'Prot. anis. parl. incr. - cum. '+str(time[ind1]*tmult)+'->'+str(time[ind2]*tmult),50)
#
if w_ele:
    axs = ax[0,1]
else:
    axs = ax[1]
for i in range(len(ranges[:,0])):
    axs.axvline(x=ranges[i,0],color=col[i])
    axs.axvline(x=ranges[i,1],color=col[i])
plot_func(axs,anisp_perp,hmin,'Prot. anis. perp. incr.',100)
#
if w_ele:
    axs = ax[1,0]
    for i in range(len(ranges[:,0])):
        axs.axvline(x=ranges[i,0],color=col[i])
        axs.axvline(x=ranges[i,1],color=col[i])
    plot_func(axs,anise_parl,hmin,'Elec. anis. parl. incr.',50)
#
    axs = ax[1,1]
    for i in range(len(ranges[:,0])):
        axs.axvline(x=ranges[i,0],color=col[i])
        axs.axvline(x=ranges[i,1],color=col[i])
    plot_func(axs,anise_perp,hmin,'Elec. anis. perp. incr.',100)
#
plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'anis_incr_'+run_name+'_cum_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
#
del anisp_parl
del anisp_perp
if w_ele:
    del anise_parl
    del anise_perp
#
#------------------------------------------------------
#backtrack points
#----------------
xpparl,ypparl,xpperp,ypperp = backtrack_hist_function_2d(anisp_avg[...,0],B_avg[...,0],lvec,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2],cut1,cut2,ranges)
if w_ele:
    xeparl,yeparl,xeperp,yeperp = backtrack_hist_function_2d(anise_avg[...,0],B_avg[...,0],lvec,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2],cut1,cut2,ranges)
#
#------------------------------------------------------
#windlike plot
#-------------
ml.create_path(opath+'/'+out_dir)
plt.close()
if w_ele:
    nplot = 2
else:
    nplot = 1
fig,ax = plt.subplots(1*2,nplot,figsize=(5*nplot,4*2))
#
if w_ele:
    ax0 = ax[0,0]
    ax1 = ax[1,0]
else:
    ax0 = ax[0]
    ax1 = ax[1]
windlike_plot(ax0,np.array(bparlp_tot),np.array(anisp_tot),'Protons + parl B.T.','p',1.,t='cum. '+str(time[ind1]*tmult)+'->'+str(time[ind2]*tmult))
for r in range(len(ranges[:,0])):
    xttparl = xpparl[xpparl[:,0]==float(r),1:]
    yttparl = ypparl[ypparl[:,0]==float(r),1:]
    for l in lvec:
        xtparl = xttparl[xttparl[:,0]==l,1:]
        ytparl = yttparl[yttparl[:,0]==l,1:]
        nparl = len(xtparl[:,0])
        for i in range(nparl):
            xt = [bparlp_avg[int(xtparl[i,0]),int(ytparl[i,0]),0],bparlp_avg[int(xtparl[i,1]),int(ytparl[i,1]),0]]
            yt = [anisp_avg[int(xtparl[i,0]),int(ytparl[i,0]),0]+1.,anisp_avg[int(xtparl[i,1]),int(ytparl[i,1]),0]+1.]
            if i == 0:
                ax0.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
            else:
                ax0.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
plt.legend()
#
windlike_plot(ax0,np.array(bparlp_tot),np.array(anisp_tot),'Protons + perp B.T.','p',1.,t='cum. '+str(time[ind1]*tmult)+'->'+str(time[ind2]*tmult))
for r in range(len(ranges[:,0])):
    xttperp = xpperp[xpperp[:,0]==float(r),1:]
    yttperp = ypperp[ypperp[:,0]==float(r),1:]
    for l in lvec:
        xtperp = xttperp[xttperp[:,0]==l,1:]
        ytperp = yttperp[yttperp[:,0]==l,1:]
        nperp = len(xtperp[:,0])
        for i in range(nperp):
            xt = [bparlp_avg[int(xtperp[i,0]),int(ytperp[i,0]),0],bparlp_avg[int(xtperp[i,1]),int(ytperp[i,1]),0]]
            yt = [anisp_avg[int(xtperp[i,0]),int(ytperp[i,0]),0]+1.,anisp_avg[int(xtperp[i,1]),int(ytperp[i,1]),0]+1.]
            if i == 0:
                ax1.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
            else:
                ax1.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
plt.legend()
#
if w_ele:
    windlike_plot(ax[0,1],np.array(bparle_tot),np.array(anise_tot),'Electrons + parl B.T.','e',teti)
    for r in range(len(ranges[:,0])):
        xttparl = xpparl[xpparl[:,0]==float(r),1:]
        yttparl = ypparl[ypparl[:,0]==float(r),1:]
        for l in lvec:
            xtparl = xttparl[xttparl[:,0]==l,1:]
            ytparl = yttparl[yttparl[:,0]==l,1:]
            nparl = len(xtparl[:,0])
            for i in range(nparl):
                xt = [bparlp_avg[int(xtparl[i,0]),int(ytparl[i,0]),0],bparlp_avg[int(xtparl[i,1]),int(ytparl[i,1]),0]]
                yt = [anisp_avg[int(xtparl[i,0]),int(ytparl[i,0]),0]+1.,anisp_avg[int(xtparl[i,1]),int(ytparl[i,1]),0]+1.]
                if i == 0:
                    ax[0,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
                else:
                    ax[0,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
    plt.legend()
#
    windlike_plot(ax[1,1],np.array(bparle_tot),np.array(anise_tot),'Electrons + perp B.T.','e',teti)
    for r in range(len(ranges[:,0])):
        xttperp = xeperp[xeperp[:,0]==float(r),1:]
        yttperp = yeperp[yeperp[:,0]==float(r),1:]
        for l in lvec:
            xtperp = xttperp[xttperp[:,0]==l,1:]
            ytperp = yttperp[yttperp[:,0]==l,1:]
            nperp = len(xtperp[:,0])
            for i in range(nperp):
                xt = [bparle_avg[int(xtperp[i,0]),int(ytperp[i,0]),0],bparle_avg[int(xtperp[i,1]),int(ytperp[i,1]),0]]
                yt = [anise_avg[int(xtperp[i,0]),int(ytperp[i,0]),0]+1.,anise_avg[int(xtperp[i,1]),int(ytperp[i,1]),0]+1.]
                if i == 0:
                    ax[1,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
                else:
                    ax[1,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
    plt.legend()

#
plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'anis_windlike_bt_'+run_name+'_cum_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
#
del bparlp_tot
del anisp_tot
if w_ele:
    del bparle_tot
    del anise_tot
#
#------------------------------------------------------
#plot 2d over threshold
#----------------------
ml.create_path(opath+'/'+out_dir)
plt.close()
if w_ele:
    nplot = 2
else:
    nplot = 1
fig,ax = plt.subplots(1*2,nplot,figsize=(5*nplot,4*2))
#
nothing = np.log(bparlp_avg[...,0]*0-1)
#
if w_ele:
    ax0 = ax[0,0]
    ax1 = ax[1,0]
else:
    ax0 = ax[0]
    ax1 = ax[1]
xMR,yMR,xOF,yOF=over_th(anisp_avg,bparlp_avg,run.meta['nx'],run.meta['ny'],cut1,cut2,1.)
ax0.contourf(nothing.T)
ax0.scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
ax0.scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
MRperc = 100.*float(len(xMR))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
OFperc = 100.*float(len(xOF))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
ax0.set_title('Prot. + parl B.T. - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
for r in range(len(ranges[:,0])):
    xttparl = xpparl[xpparl[:,0]==float(r),1:]
    yttparl = ypparl[ypparl[:,0]==float(r),1:]
    for l in lvec:
        xtparl = xttparl[xttparl[:,0]==l,1:]
        ytparl = yttparl[yttparl[:,0]==l,1:]
        nparl = len(xtparl[:,0])
        for i in range(nparl):
            xt = [int(xtparl[i,0]),int(xtparl[i,1])]
            yt = [int(ytparl[i,0]),int(ytparl[i,1])]
            if i == 0:
                ax0.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
            else:
                ax0.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
ax0.legend()
#
xMR,yMR,xOF,yOF=over_th(anisp_avg,bparlp_avg,run.meta['nx'],run.meta['ny'],cut1,cut2,1.)
ax1.contourf(nothing.T)
ax1.scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
ax1.scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
MRperc = 100.*float(len(xMR))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
OFperc = 100.*float(len(xOF))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
ax1.set_title('Prot. + perp B.T. - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
for r in range(len(ranges[:,0])):
    xttperp = xpperp[xpperp[:,0]==float(r),1:]
    yttperp = ypperp[ypperp[:,0]==float(r),1:]
    for l in lvec:
        xtperp = xttperp[xttperp[:,0]==l,1:]
        ytperp = yttperp[yttperp[:,0]==l,1:]
        nperp = len(xtperp[:,0])
        for i in range(nperp):
            xt = [int(xtperp[i,0]),int(xtperp[i,1])]
            yt = [int(ytperp[i,0]),int(ytperp[i,1])]
            if i == 0:
                ax1.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
            else:
                ax1.plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
ax1.legend()
#
if w_ele:
    xMR,yMR,xOF,yOF=over_th(anise_avg,bparle_avg,run.meta['nx'],run.meta['ny'],cut1,cut2,teti)
    ax[0,1].contourf(nothing.T)
    ax[0,1].scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
    ax[0,1].scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
    MRperc = 100.*float(len(xMR))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
    OFperc = 100.*float(len(xOF))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
    ax[0,1].set_title('Ele. + parl B.T. - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
    for r in range(len(ranges[:,0])):
        xttparl = xeparl[xeparl[:,0]==float(r),1:]
        yttparl = yeparl[yeparl[:,0]==float(r),1:]
        for l in lvec:
            xtparl = xttparl[xttparl[:,0]==l,1:]
            ytparl = yttparl[yttparl[:,0]==l,1:]
            nparl = len(xtparl[:,0])
            for i in range(nparl):
                xt = [int(xtparl[i,0]),int(xtparl[i,1])]
                yt = [int(ytparl[i,0]),int(ytparl[i,1])]
                if i == 0:
                    ax[0,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
                else:
                    ax[0,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
    ax[0,1].legend()
#
    xMR,yMR,xOF,yOF=over_th(anise_avg,bparle_avg,run.meta['nx'],run.meta['ny'],cut1,cut2,teti)
    ax[1,1].contourf(nothing.T)
    ax[1,1].scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
    ax[1,1].scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
    MRperc = 100.*float(len(xMR))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
    OFperc = 100.*float(len(xOF))/float(run.meta['nx']*(run.meta['ny']-(cut2-cut1+1)))
    ax[1,1].set_title('Ele. + perp B.T. - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
    for r in range(len(ranges[:,0])):
        xttperp = xeperp[xeperp[:,0]==float(r),1:]
        yttperp = yeperp[yeperp[:,0]==float(r),1:]
        for l in lvec:
            xtperp = xttperp[xttperp[:,0]==l,1:]
            ytperp = yttperp[yttperp[:,0]==l,1:]
            nperp = len(xtperp[:,0])
            for i in range(nperp):
                xt = [int(xtperp[i,0]),int(xtperp[i,1])]
                yt = [int(ytperp[i,0]),int(ytperp[i,1])]
                if i == 0:
                    ax[1,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)],label=str(l))
                else:
                    ax[1,1].plot(xt,yt,color=col[r],marker=mar[list(lvec).index(l)])
    ax[1,1].legend()
#
plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'anis_overth_bt_'+run_name+'_cum_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
#
del anisp_avg
del bparlp_avg
if w_ele:
    del anise_avg
    del bparle_avg
#
