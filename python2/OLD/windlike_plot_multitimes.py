#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta cumulated over different times.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'windlike_plot_multitimes'
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#see Hellinger et al. 2006
#    Astfalk and Jenko 2016
#    Lazar et al. 2015
params = {}
params['H2006'] = {}
params['A2016'] = {}
params['L2015'] = {}

params['H2006']['PC'] = {}
params['H2006']['PC']['10m3'] = {}
params['H2006']['PC']['10m3']['bM'] = [ 0.43,0.42,-0.0004]

params['L2015']['EC'] = {}
params['L2015']['EC']['e05'] = {}
params['L2015']['EC']['e05']['10m2'] = {}
params['L2015']['EC']['e05']['10m2']['k2'] = {}
params['L2015']['EC']['e05']['10m2']['k2']['T'] = [0.54,0.21,0.14,1.28]
params['L2015']['EC']['e05']['10m2']['k2']['C'] = [0.26,-0.01,1.18,0.56]
params['L2015']['EC']['e05']['10m2']['k6'] = {}
params['L2015']['EC']['e05']['10m2']['k6']['T'] = [0.43,0.17,0.02,2.04]
params['L2015']['EC']['e05']['10m2']['k6']['C'] = [0.19,-0.09,1.11,0.63]
params['L2015']['EC']['e05']['10m2']['bM'] = {}
params['L2015']['EC']['e05']['10m2']['bM']['T'] = [0.40,0.10,0.05,1.26]
params['L2015']['EC']['e05']['10m2']['bM']['C'] = [0.17,-0.11,1.18,0.65]
params['L2015']['EC']['e05']['10m3'] = {}
params['L2015']['EC']['e05']['10m3']['k2'] = {}
params['L2015']['EC']['e05']['10m3']['k2']['T'] = [0.10,0.30,0.09,1.22]
params['L2015']['EC']['e05']['10m3']['k2']['C'] = [0.01,-0.46,19.1,0.97]
params['L2015']['EC']['e05']['10m3']['k6'] = {}
params['L2015']['EC']['e05']['10m3']['k6']['T'] = [0.08,0.38,0.003,2.54]
params['L2015']['EC']['e05']['10m3']['k6']['C'] = [0.005,-0.52,14.6,1.07]
params['L2015']['EC']['e05']['10m3']['bM'] = {}
params['L2015']['EC']['e05']['10m3']['bM']['T'] = [0.07,0.31,0.02,1.23]
params['L2015']['EC']['e05']['10m3']['bM']['C'] = [0.003,-0.67,21.3,1.23]
params['L2015']['EC']['e01'] = {}
params['L2015']['EC']['e01']['10m2'] = {}
params['L2015']['EC']['e01']['10m2']['k2'] = {}
params['L2015']['EC']['e01']['10m2']['k2']['T'] = [1.74,0.05,0.04,1.29]
params['L2015']['EC']['e01']['10m2']['k2']['C'] = [1.35,-0.04,0.30,0.60]
params['L2015']['EC']['e01']['10m2']['k6'] = {}
params['L2015']['EC']['e01']['10m2']['k6']['T'] = [1.54,0.06,0.003,1.86]
params['L2015']['EC']['e01']['10m2']['k6']['C'] = [1.27,-0.03,0.16,0.62]
params['L2015']['EC']['e01']['10m2']['bM'] = {}
params['L2015']['EC']['e01']['10m2']['bM']['T'] = [1.47,0.05,0.002,2.03]
params['L2015']['EC']['e01']['10m2']['bM']['C'] = [1.23,-0.03,0.14,0.63]
params['L2015']['EC']['e01']['10m3'] = {}
params['L2015']['EC']['e01']['10m3']['k2'] = {}
params['L2015']['EC']['e01']['10m3']['k2']['T'] = [0.21,0.12,0.04,1.28]
params['L2015']['EC']['e01']['10m3']['k2']['C'] = [0.11,-0.07,0.84,0.60]
params['L2015']['EC']['e01']['10m3']['k6'] = {}
params['L2015']['EC']['e01']['10m3']['k6']['T'] = [0.18,0.14,0.001,2.21]
params['L2015']['EC']['e01']['10m3']['k6']['C'] = [0.11,-0.06,0.47,0.63]
params['L2015']['EC']['e01']['10m3']['bM'] = {}
params['L2015']['EC']['e01']['10m3']['bM']['T'] = [0.17,0.15,0.0001,2.83]
params['L2015']['EC']['e01']['10m3']['bM']['C'] = [0.11,-0.05,0.40,0.64]

params['H2006']['MR'] = {}
params['H2006']['MR']['10m3'] = {}
params['H2006']['MR']['10m3']['bM'] = [ 0.77,0.76,-0.0160]

params['H2006']['PF'] = {}
params['H2006']['PF']['10m3'] = {}
params['H2006']['PF']['10m3']['bM'] = [-0.47,0.53, 0.5900]

params['H2006']['OF'] = {}
params['H2006']['OF']['10m3'] = {}
params['H2006']['OF']['10m3']['bM'] = [-1.40,1.00,-0.1100]

params['A2016']['PF'] = {}
params['A2016']['PF']['10m3'] = {}
params['A2016']['PF']['10m3']['bM']  = [-0.487,0.537, 0.560]
params['A2016']['PF']['10m3']['k12'] = [-0.438,0.475, 0.503]
params['A2016']['PF']['10m3']['k8']  = [-0.429,0.486, 0.423]
params['A2016']['PF']['10m3']['k6']  = [-0.417,0.498, 0.350]
params['A2016']['PF']['10m3']['k4']  = [-0.387,0.518, 0.226]
params['A2016']['PF']['10m3']['k2']  = [-0.274,0.536, 0.042]
params['A2016']['PF']['10m2'] = {}
params['A2016']['PF']['10m2']['bM']  = [-0.701,0.623, 0.599]
params['A2016']['PF']['10m2']['k12'] = [-0.656,0.596, 0.567]
params['A2016']['PF']['10m2']['k8']  = [-0.623,0.579, 0.569]
params['A2016']['PF']['10m2']['k6']  = [-0.625,0.585, 0.501]
params['A2016']['PF']['10m2']['k4']  = [-0.625,0.593, 0.379]
params['A2016']['PF']['10m2']['k2']  = [-0.632,0.589, 0.139]
params['A2016']['PF']['10m1'] = {}
params['A2016']['PF']['10m1']['bM']  = [-0.872,0.495, 1.233]
params['A2016']['PF']['10m1']['k12'] = [-0.899,0.502, 1.213]
params['A2016']['PF']['10m1']['k8']  = [-0.937,0.509, 1.097]
params['A2016']['PF']['10m1']['k6']  = [-0.947,0.505, 1.088]
params['A2016']['PF']['10m1']['k4']  = [-0.977,0.496, 1.068]
params['A2016']['PF']['10m1']['k2']  = [-1.230,0.464, 1.206]

params['A2016']['OF'] = {}
params['A2016']['OF']['10m3'] = {}
params['A2016']['OF']['10m3']['bM']  = [-1.371,0.996,-0.083]
params['A2016']['OF']['10m3']['k12'] = [-1.444,0.995,-0.070]
params['A2016']['OF']['10m3']['k8']  = [-1.484,0.994,-0.061]
params['A2016']['OF']['10m3']['k6']  = [-1.525,0.993,-0.052]
params['A2016']['OF']['10m3']['k4']  = [-1.613,0.990,-0.026]
params['A2016']['OF']['10m2'] = {}
params['A2016']['OF']['10m2']['bM']  = [-1.371,0.980,-0.049]
params['A2016']['OF']['10m2']['k12'] = [-1.440,0.979,-0.034]
params['A2016']['OF']['10m2']['k8']  = [-1.477,0.978,-0.024]
params['A2016']['OF']['10m2']['k6']  = [-1.514,0.976,-0.012]
params['A2016']['OF']['10m2']['k4']  = [-1.594,0.973, 0.017]

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

#see Lazar et al. 2015
@njit
def th_func_2(bparl,a,b,c,d):
    return 1. + (a/bparl**b)*(1.+c/bparl**d)

#-------------------------------------------------------
#plot function
#-------------
def windlike_plot_func( ax, xdata, ydata, species_vars,
                        xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                        legend_flag=False, params=params, run_label=run_label,
                        h_old=None, c_pass=0, n_pass=1 ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] or [array,array] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    params       -> [optional, set by default] dict -
                    dict containing all thresholds parameters
    run_label    -> [optional, set by default] string - run label
    h_old        -> [optional] None or np.array - default=None - 
                    where histograms are cumulated
    c_pass       -> [optional] int - default=0 - passage index
    n_pass       -> [optional] int - default=1 - number of cumulating passages

    OUTPUTS:
    if c_pass < n_pass-1:
        h            -> np.array - cumulated 2D histogram
    if c_pass == n_pass-1:
        handles      -> [lines.Line2D, ...] - list of handles for the legend
    if ERROR:
        -1           -> int - ERROR exit
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    h,xbin,ybin=np.histogram2d(np.log10(xdata).flatten(),np.log10(ydata).flatten(),n_bins)
    if type(h_old) != type(None):
        if h_old.shape != h.shape:
            print('\nh_old.shape=%s is different from h.shape=%s\n'%(str(h_old.shape),str(h.shape)))
            return -1
        h = h + h_old
    if c_pass == n_pass-1:
        tmp_xbin = np.power(10.,xbin)
        tmp_ybin = np.power(10.,ybin)
        X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
        im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())
        del X,Y,h

        if xdata_range == None:
            xdata_range = [np.min(tmp_xbin),np.max(tmp_xbin)]
        if ydata_range == None:
            ydata_range = [np.min(tmp_ybin),np.max(tmp_ybin)]
        xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),len(xbin)-1)
        del xbin,ybin,tmp_xbin,tmp_ybin

        if species_tag == 'p':
            p = params['H2006']['PC']['10m3']['bM'] # ProtonCyclotron - bi-Maxwellian
            lC, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label="PCI")

        p = params['H2006']['MR']['10m3']['bM'] # Mirror - bi-Maxwellian
        lM, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label="MI")

        if species_tag == 'e':
            p = params['L2015']['EC']['e05']['10m2']['bM']['T'] # ElectronCyclotron - bi-Maxwellian
            lC, = ax.plot(xvals,th_func_2(xvals,p[0],p[1],p[2],p[3]),'-k',label="ECI")

        p = params['A2016']['PF']['10m2']['bM'] # ParallelFirehose - bi-Maxwellian
        lP, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label="PFHI")

        p = params['A2016']['OF']['10m2']['bM'] # ObliqueFirehose - bi-Maxwellian
        lO, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label="OFHI")

        p = params['A2016']['OF']['10m2']['k4'] # ObliqueFirehose - bi-kappa
        lOk, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label="OFHI $\kappa=4$")

        lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',label='$\\beta_{\parallel}^{-1}$') # CGL expansion
        
        ax.set_xlabel('$\\beta_{\parallel,%s}$'%(species_tag))
        ax.set_ylabel('$p_{\perp,%s}/p_{\parallel,%s}$'%(species_tag,species_tag))
        ax.set_xlim(xdata_range[0],xdata_range[1])
        ax.set_ylim(ydata_range[0],ydata_range[1])
        if legend_flag:
            ax.legend()
        title_str = run_label+' - '+species_name
        if t != None:
            title_str += ' - $t = %s$'%(t)
        ax.set_title(title_str)
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.colorbar(im,ax=ax)

        return [lC,lM,lP,lO,lOk,lb]
    elif c_pass < n_pass-1:
        return h
    else:
        print('\nc_pass=%d is greater than n_pass-1=%d\n'%(c_pass,n_pass-1))
        return -1

#---> loop over times <---
hp = None
he = None
n_bins = 50*int(round(np.sqrt(ind2-ind1+1)))
xbinp = np.linspace(np.log10(betap_lim[0]),np.log10(betap_lim[1]),n_bins+1) # WIP
ybinp = np.linspace(np.log10(anisp_lim[0]),np.log10(anisp_lim[1]),n_bins+1) # WIP
xbine = np.linspace(np.log10(betae_lim[0]),np.log10(betae_lim[1]),n_bins+1) # WIP
ybine = np.linspace(np.log10(anise_lim[0]),np.log10(anise_lim[1]),n_bins+1) # WIP

plt.close()
fig,ax = plt.subplots(1,2,figsize=(10*2,8))

ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
n_pass = len(ind_range)

print " ",
for c_pass,ind in enumerate(ind_range):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n,_ = run.get_Ion(ind)
            pparle *= n
            pperpe *= n
            del n
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    del B
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')
        del pparle
    del B2,pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    b_cut = np.concatenate((bparlp[:,:cut1,0],bparlp[:,cut2+1:,0]),axis=1)
    a_cut = np.concatenate(( anisp[:,:cut1,0], anisp[:,cut2+1:,0]),axis=1)
    ###handles_p = windlike_plot_func(ax[0],b_cut,a_cut,p_vars,
    ###                               xdata_range=betap_lim,ydata_range=anisp_lim)
    hp = windlike_plot_func(ax[0],b_cut,a_cut,p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                            n_bins=[xbinp,ybinp],h_old=hp,c_pass=c_pass,n_pass=n_pass)
    if type(hp) == np.int:
        if hp == -1:
            print('\nSomething went wrong in windlike_plot_func (protons)\n')
#
    if w_ele:
        b_cut = np.concatenate((bparle[:,:cut1,0],bparle[:,cut2+1:,0]),axis=1)
        a_cut = np.concatenate(( anise[:,:cut1,0], anise[:,cut2+1:,0]),axis=1)
        ###handles_e = windlike_plot_func(ax[1],b_cut,a_cut,e_vars,
        ###                               xdata_range=betae_lim,ydata_range=anise_lim)
        he = windlike_plot_func(ax[1],b_cut,a_cut,e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                n_bins=[xbine,ybine],h_old=he,c_pass=c_pass,n_pass=n_pass)
        if type(he) == np.int:
            if he == -1:
                print('\nSomething went wrong in windlike_plot_func (electrons)\n')
#
    del b_cut,a_cut
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
if not w_ele:
    he = hp[:]
    he[0], =  ax[1].plot([],[],color='black',label='ECI')
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].spines['bottom'].set_visible(False)
    ax[1].spines['left'].set_visible(False)
    ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
    handles = [hp[0],#[lC,lM,lP,lO,lOk,lb]
               he[0],
               hp[1],
               hp[2],
               hp[3],
               hp[4],
               hp[5]]
    ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'anis_windlike_multitimes_'+run_name+'_'+
            str(ind1)+'-'+str(ind2)+'_b'+str(n_bins)+'.png')
plt.close()
