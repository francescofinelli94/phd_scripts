#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import pandas as pd
import glob
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.ndimage import gaussian_filter as gf
import scipy.ndimage.interpolation as ndm
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill compute contrubutes to fluid (isotropic and gyrotropic) entropy variation\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'entropy_v4' + '/' + run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

smooth_flag = False
smooth_flag_post = False
gfs = 2
if smooth_flag:
    out_dir = out_dir + '_gfPRE%d'%(gfs)

if smooth_flag_post:
    out_dir = out_dir + '_gfPOST%d'%(gfs)

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#------------------------------------------------------
#Functions
#---------
def onclick(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
#
    # assign global variable to access outside of function
    global coords
    coords.append((ix, iy))
#
    # Disconnect after 2 clicks
    global npoints
    if len(coords) == npoints:
        fig.canvas.mpl_disconnect(cid)
        plt.close(1)
    return

#------------------------------------------------------
#get data
#--------
ind = 152

# -> magnetic fields and co.
E,B = run.get_EB(ind)
if code_name == 'iPIC' and smooth_flag:
    for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')

Psi = wl.psi_2d(B[:-1,...,0],run.meta)
if code_name == 'iPIC' and (smooth_flag or smooth_flag_post):
    Psi = gf(Psi,[gfs,gfs],mode='wrap')

if code_name == 'HVM':
    cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
    J = np.array([cx,cy,cz]) # J = rot(B)
    del cx,cy,cz

# -> densities and currents
n_p,u_p = run.get_Ion(ind)
if code_name == 'iPIC' and smooth_flag:
    n_p[:,:,0] = gf(n_p[:,:,0],[gfs,gfs],mode='wrap')
    for i in range(3): u_p[i,:,:,0] = gf(u_p[i,:,:,0],[gfs,gfs],mode='wrap')

if code_name == 'HVM':
    n_e = n_p.copy() # n_e = n_p = n (no charge separation)
    u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
    u_e[0] = u_p[0] - np.divide(J[0],n_p)
    u_e[1] = u_p[1] - np.divide(J[1],n_p)
    u_e[2] = u_p[2] - np.divide(J[2],n_p)
elif code_name == 'iPIC':
    n_e,u_e = run.get_Ion(ind,qom=qom_e)
    if smooth_flag:
        n_e[:,:,0] = gf(n_e[:,:,0],[gfs,gfs],mode='wrap')
        for i in range(3): u_e[i,:,:,0] = gf(u_e[i,:,:,0],[gfs,gfs],mode='wrap')
    J = n_p*u_p - n_e*u_e

e_demag = np.empty((3,nx,ny,nz),dtype=np.float64)
e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)

p_demag = np.empty((3,nx,ny,nz),dtype=np.float64)
p_demag[0] = E[0] + u_p[1]*B[2] - u_p[2]*B[1]
p_demag[1] = E[1] + u_p[2]*B[0] - u_p[0]*B[2]
p_demag[2] = E[2] + u_p[0]*B[1] - u_p[1]*B[0]
p_demag_n = np.sqrt(p_demag[0]**2 + p_demag[1]**2 + p_demag[2]**2)

#------------------------------------------------------
#traslate fields
#---------------
# -> plot Psi and select center
print('\nFirst click -> A')
print('Second click -> B')
print('A wll be translated in B')

plt.close('all')
fig = plt.figure(1,figsize=(12,5))
ax = fig.subplots(1,1)

ax.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_title(run_name+' - Psi - t=%s'%(time[ind]))

fig.tight_layout()

coords = []
npoints=2
cid = fig.canvas.mpl_connect('button_press_event', onclick)
plt.show(1)

xc,yc = coords[0]
xo,yo = coords[1]

# -> traslation
Dx = xo - xc
Dy = yo - yc
Dnx = int(Dx/dx)
Dny = int(Dy/dy)
Dx = float(Dnx)*dx
Dy = float(Dny)*dy

x = np.roll(x,Dnx)
for i in range(Dnx): x[i] = -dx*float(Dnx-i)

y = np.roll(y,Dny)
for i in range(Dny): y[i] = -dy*float(Dny-i)

E = np.roll(E,(Dnx,Dny),axis=(1,2))
B = np.roll(B,(Dnx,Dny),axis=(1,2))
Psi = np.roll(Psi,(Dnx,Dny),axis=(0,1))
J = np.roll(J,(Dnx,Dny),axis=(1,2))
n_p = np.roll(n_p,(Dnx,Dny),axis=(0,1))
u_p = np.roll(u_p,(Dnx,Dny),axis=(1,2))
n_e = np.roll(n_e,(Dnx,Dny),axis=(0,1))
u_e = np.roll(u_e,(Dnx,Dny),axis=(1,2))
e_demag = np.roll(e_demag,(Dnx,Dny),axis=(1,2))
e_demag_n = np.roll(e_demag_n,(Dnx,Dny),axis=(0,1))
p_demag = np.roll(p_demag,(Dnx,Dny),axis=(1,2))
p_demag_n = np.roll(p_demag_n,(Dnx,Dny),axis=(0,1))

#------------------------------------------------------
#trace trajectory
#----------------
# -> plot demag, Psi, and flow, and select key points
qrx = 70
qry = 15

plt.close('all')
fig = plt.figure(1,figsize=(15,10))
ax = fig.subplots(2,1)

im0 = ax[0].contourf(x[ixmin:ixmax],y[iymin:iymax],e_demag_n[ixmin:ixmax,iymin:iymax,0].T,63)
fig.colorbar(im0,ax=ax[0])
ax[0].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_e[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_e[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].set_title(run_name+' - e_demag - t=%s'%(time[ind]))

im1 = ax[1].contourf(x[ixmin:ixmax],y[iymin:iymax],p_demag_n[ixmin:ixmax,iymin:iymax,0].T,63)
fig.colorbar(im1,ax=ax[1])
ax[1].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_p[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_p[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[1].set_xlabel('x')
ax[1].set_ylabel('y')
ax[1].set_title(run_name+' - p_demag - t=%s'%(time[ind]))

fig.tight_layout()

coords = []
npoints = 20
cid = fig.canvas.mpl_connect('button_press_event', onclick)
plt.show(1)

x0 = []
y0 = []
for c in coords:
    x0.append(c[0])
    y0.append(c[1])

y0 = [x_ for _,x_ in sorted(zip(x0,y0))]
x0 = sorted(x0)
x0 = np.array(x0)
y0 = np.array(y0)

#define trajectory
#tck  = interpolate.splrep(x0,y0,s=0)
f_tmp = interpolate.interp1d(np.arange(len(x)),x)
res_mult = 2
x_tr = f_tmp(np.linspace(0.,len(x)-1.,(len(x)-1)*res_mult+1))
#y_tr = interpolate.splev(x_tr,tck,der=0)
x_tr = x_tr[np.logical_and(x_tr>=x0[0],x_tr<=x0[-1])]
f_tmp = interpolate.interp1d(x0,y0,kind='quadratic')#kind in [‘linear’, ‘nearest’, ‘previous’, ‘next’,
                                        #splines -> #         ‘zero’, ‘slinear’, ‘quadratic’, ‘cubic’]
y_tr = f_tmp(x_tr)

#interpolate fields
B_tr = np.empty((3,len(x_tr)),dtype=np.float64)
for i in range(3):
    B_tr[i] = ndm.map_coordinates(B[i,:,:,0].T,np.vstack((x_tr/dx,y_tr/dy)),mode='wrap')

#plot demag, Psi, flow, and trajectory
qrx = 70
qry = 15

plt.close()
fig,ax = plt.subplots(2,1,figsize=(15,10))

im0 = ax[0].contourf(x[ixmin:ixmax],y[iymin:iymax],e_demag_n[ixmin:ixmax,iymin:iymax,0].T,63)
plt.colorbar(im0,ax=ax[0])
ax[0].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_e[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_e[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[0].plot(x_tr,y_tr,'--r')
ax[0].plot(x0,y0,'or')
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].set_xlim(x[ixmin],x[ixmax])
ax[0].set_ylim(y[iymin],y[iymax])
ax[0].set_title(run_name+' - e_demag - t=%s'%(time[ind]))

im1 = ax[1].contourf(x[ixmin:ixmax],y[iymin:iymax],p_demag_n[ixmin:ixmax,iymin:iymax,0].T,63)
plt.colorbar(im1,ax=ax[1])
ax[1].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_p[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_p[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[1].plot(x_tr,y_tr,'--r')
ax[1].plot(x0,y0,'or')
ax[1].set_xlabel('x')
ax[1].set_ylabel('y')
ax[1].set_xlim(x[ixmin],x[ixmax])
ax[1].set_ylim(y[iymin],y[iymax])
ax[1].set_title(run_name+' - p_demag - t=%s'%(time[ind]))

plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+out_dir+'/'+'Sfp_'+run_name+'_'+str(ind)+'.png')
plt.close()
