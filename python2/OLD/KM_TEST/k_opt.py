#---------------------------------------------------
#LIBRARIES
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import glob

from sklearn.metrics import silhouette_score
from sklearn.metrics import davies_bouldin_score
from sklearn.cluster import KMeans as KM

#---------------------------------------------------
#FUNCTIONS
def my_normalize(data):
    data_ = np.array(data)
    M = np.max(data_)
    m = np.min(data_)
    return np.divide(data_ - m, M - m)

#---------------------------------------------------
#INITIALIZATION (MOSTLY HARDCODED)
rnd_state = 666

df_path = '/home/finelli/Documents/KM_TEST'
df_name = 'Solar_wind.csv'

#vars_ = ["J","x","y","Dummy","V","EV","EVB","CM","Jep"]
#myvars = ["J","V","EV","EVB","CM","Jep"]

#
# initialize parallelization, if needed
#

#---------------------------------------------------
#READ AND PREPROCESS DATA
# -> choose the file
df_files = glob.glob(df_path+'/'+df_name)
if len(df_files) == 0:
    sys.exit("ERROR: no file %s found in:\n"%(df_name)+pvi_path)

print('\nfiles:')
for n,f in zip(range(len(df_files)),df_files):
    print('%d -> %s'%(n,f.split('/')[-1]))

n = int(input('Choose a file (insert the corresponding number): '))
df_file = df_files[n]

# -> read
df = pd.read_csv(df_file,header=0)
#df.columns = ["J","x","y","Dummy","V","EV","EVB","CM","Jep"]
print('\nAll data:')
print(df.head())

# -> preprocess
X = df.drop('class', axis = 1)
#X = df[["J","V","EV","EVB","CM","Jep"]]
print('\nSelected data:')
print(X.head())



# -> correlation matrix w/ plot
corr = df.corr()

plt.close('all')
fig, ax = plt.subplots(figsize=(15,8))
hm = sns.heatmap(corr,
                 ax=ax,
                 cmap='coolwarm',
                 vmin=-1.0,
                 vmax=1.0,
                 annot=True,
                 fmt='.2f',
                 annot_kws={'size':15},
                 linewidths=0.5)
ax.set_title('Correlation matrix')
plt.show()
plt.close()

#---------------------------------------------------
#SCORES FOR K OPTIMIZATION
# -> compute scores
distortion = []
silhouette = []
davies_bouldin = []
print('\nStarting k-cycle:')
for i in range(2, 11):
    print('\nk = %d'%(i))
    km = KM(n_clusters=i, init='k-means++', random_state=rnd_state)
    print('fitting...')
    km.fit(X)
    print('predicting...')
    y_kmi = km.predict(X)
    print('computing distortion...')
    distortion.append(km.inertia_)
    print('computing silhouette...')
    S = silhouette_score(X, y_kmi, metric='euclidean')
    silhouette.append(S)
    print('Computing Davies-Bouldin...')
    D = davies_bouldin_score(X,y_kmi)
    davies_bouldin.append(D)

print('\nk-cycle done!')

distortion_norm = my_normalize(distortion)
silhouette_norm = my_normalize(silhouette)
davies_bouldin_norm = my_normalize(davies_bouldin)

# -> plot scores
fig, ax1 = plt.subplots(figsize = (8,5))

ax1.plot(range(2,11),distortion_norm,marker='o',label='Distortion (norm.)')
ax1.plot(range(2,11),silhouette_norm,marker='o',label='Silhouette (norm.)')
ax1.plot(range(2,11),davies_bouldin_norm,marker='o',label='Davies-Bouldin (norm.)')

ax1.set_xlabel('Number of clusters')
ax1.set_ylabel('Score')
ax1.set_title('Scores for k optimization')
ax1.legend()

ax1.grid(b=None, axis = 'y')
plt.show()
plt.close()
