#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import pandas as pd
import glob
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.ndimage import gaussian_filter as gf
import scipy.ndimage.interpolation as ndm
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nTool to generate a virtual trajectory in a 2D simultion frame\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'entropy_v4' + '/' + run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

smooth_flag = False
smooth_flag_post = True
gfs = 2
if smooth_flag:
    out_dir = out_dir + '_gfPRE%d'%(gfs)

if smooth_flag_post:
    out_dir = out_dir + '_gfPOST%d'%(gfs)

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#------------------------------------------------------
#Functions
#---------
def onclick_trasl(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
#
    # assign global variable to access outside of function
    global x_, y_
    if len(x_) < 2:
        x_.append(ix)
        y_.append(iy)
#
    #update plot
    global fig, tbu
    tbu.set_xdata(x_)
    tbu.set_ydata(y_)
    fig.canvas.draw()
#
    return

def onclick_trace(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
#
    # assign global variable to access outside of function
    global x_, y_
    global fig, tbu00, tbu10
    x_.append(ix)
    y_.append(iy)
    y_ = [xx for _,xx in sorted(zip(x_,y_))]
    x_ = sorted(x_)
    tbu00.set_xdata(x_)
    tbu00.set_ydata(y_)
    tbu10.set_xdata(x_)
    tbu10.set_ydata(y_)
    fig.canvas.draw()
#
    #interpolate
    if len(x_) > 2:
        global x_tr_max, x_tr, y_tr
        x_tr = x_tr_max[np.logical_and(x_tr_max>=x_[0],x_tr_max<=x_[-1])]
        f_tmp = interpolate.interp1d(x_,y_,kind='quadratic')
        y_tr = f_tmp(x_tr)
        global tbu01, tbu11
        tbu01.set_xdata(x_tr)
        tbu01.set_ydata(y_tr)
        tbu11.set_xdata(x_tr)
        tbu11.set_ydata(y_tr)
        fig.canvas.draw()
#
    return

def onclick_close(event):
    global fig, cid, cid2
    fig.canvas.mpl_disconnect(cid)
    fig.canvas.mpl_disconnect(cid2)
    fig.canvas.stop_event_loop()
    plt.close(1)
    return

def field_interp(field,xcoord,ycoord):
    if len(field.shape) == 4:
        field_tr = np.empty((3,len(xcoord)),dtype=np.float64)
        for i in range(3):
            field_tr[i] = ndm.map_coordinates(field[i,:,:,0].T,np.vstack((xcoord,ycoord)),mode='wrap')
    elif len(field.shape) == 3:
        field_tr = ndm.map_coordinates(field[:,:,0].T,np.vstack((xcoord,ycoord)),mode='wrap')
    elif len(field.shape) == 2:
        field_tr = ndm.map_coordinates(field[:,:].T,np.vstack((xcoord,ycoord)),mode='wrap')
    elif len(field.shape) == 1:
        field_tr = ndm.map_coordinates(field,xcoord,mode='wrap')
    else:
        field_tr = []
#
    return field_tr

#------------------------------------------------------
#get data
#--------
ind = 152

# -> magnetic fields and co.
E,B = run.get_EB(ind)
if code_name == 'iPIC' and smooth_flag:
    for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')

Psi = wl.psi_2d(B[:-1,...,0],run.meta)
if code_name == 'iPIC' and (smooth_flag or smooth_flag_post):
    Psi = gf(Psi,[gfs,gfs],mode='wrap')

if code_name == 'HVM':
    cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
    J = np.array([cx,cy,cz]) # J = rot(B)
    del cx,cy,cz

# -> densities and currents
n_p,u_p = run.get_Ion(ind)
if code_name == 'iPIC' and smooth_flag:
    n_p[:,:,0] = gf(n_p[:,:,0],[gfs,gfs],mode='wrap')
    for i in range(3): u_p[i,:,:,0] = gf(u_p[i,:,:,0],[gfs,gfs],mode='wrap')

if code_name == 'HVM':
    n_e = n_p.copy() # n_e = n_p = n (no charge separation)
    u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
    u_e[0] = u_p[0] - np.divide(J[0],n_p)
    u_e[1] = u_p[1] - np.divide(J[1],n_p)
    u_e[2] = u_p[2] - np.divide(J[2],n_p)
elif code_name == 'iPIC':
    n_e,u_e = run.get_Ion(ind,qom=qom_e)
    if smooth_flag:
        n_e[:,:,0] = gf(n_e[:,:,0],[gfs,gfs],mode='wrap')
        for i in range(3): u_e[i,:,:,0] = gf(u_e[i,:,:,0],[gfs,gfs],mode='wrap')
    J = n_p*u_p - n_e*u_e

# -> Pressures
Pp = run.get_Press(ind)
if code_name == 'iPIC' and smooth_flag:
    for i in range(3):
        for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[gfs,gfs],mode='wrap')

pparlp,pperpp = ml.proj_tens_on_vec(Pp,B) # pparlp = Pp:bb
del Pp                                    # pperpp = Pp:(id - bb)/2

if not w_ele:
    pparle = n_e*run.meta['beta']*0.5*run.meta['teti'] # pparle = Pisoe = n_e * T_e,0
    pperpe = pparle[:] # pperpe = Pisoe
else:
    if code_name == 'HVM':
        tparle,tperpe = run.get_Te(ind)
        pparle = tparle*n_e # pparle = n_e*tparle
        pperpe = tperpe*n_e # pperpe = n_e*tperpe
        del tparle,tperpe
    elif code_name == 'iPIC':
        Pe = run.get_Press(ind,qom=qom_e)
        if smooth_flag:
            for i in range(3):
                for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[gfs,gfs],mode='wrap')
        pparle,pperpe = ml.proj_tens_on_vec(Pe,B) # pparle = Pe:bb
        del Pe                                    # pperpe = Pe:(id - bb)/2

# -> demag
e_demag = np.empty(E.shape,dtype=np.float64)
e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
if code_name == 'iPIC' and smooth_flag_post:
    for i in range(3): e_demag[i,:,:,0] = gf(e_demag[i,:,:,0],[gfs,gfs],mode='wrap')

e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)

p_demag = np.empty(E.shape,dtype=np.float64)
p_demag[0] = E[0] + u_p[1]*B[2] - u_p[2]*B[1]
p_demag[1] = E[1] + u_p[2]*B[0] - u_p[0]*B[2]
p_demag[2] = E[2] + u_p[0]*B[1] - u_p[1]*B[0]
if code_name == 'iPIC' and smooth_flag_post:
    for i in range(3): p_demag[i,:,:,0] = gf(p_demag[i,:,:,0],[gfs,gfs],mode='wrap')

p_demag_n = np.sqrt(p_demag[0]**2 + p_demag[1]**2 + p_demag[2]**2)

# -> anis and bparl
anisp = np.divide(pperpp,pparlp)
del pperpp
if code_name == 'iPIC' and smooth_flag_post:
    anisp[:,:,0] = gf(anisp[:,:,0],[gfs,gfs],mode='wrap')

anise = np.divide(pperpe,pparle)
del pperpe
if code_name == 'iPIC' and smooth_flag_post:
    anise[:,:,0] = gf(anise[:,:,0],[gfs,gfs],mode='wrap')

B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
bparlp = 2.*np.divide(pparlp,B2)
if code_name == 'iPIC' and smooth_flag_post:
    bparlp[:,:,0] = gf(bparlp[:,:,0],[gfs,gfs],mode='wrap')

del pparlp

bparle = 2.*np.divide(pparle,B2)
if code_name == 'iPIC' and smooth_flag_post:
    bparle[:,:,0] = gf(bparle[:,:,0],[gfs,gfs],mode='wrap')

del pparle, B2

if code_name == 'iPIC':
    bparlp = 4.*np.pi*bparlp
    bparle = 4.*np.pi*bparle

#------------------------------------------------------
#traslate fields
#---------------
# -> plot Psi and select center
print('\nFirst click -> A')
print('Second click -> B')
print('A wll be translated in B')
print('Press any key to exit')

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(12,5))
ax = fig.subplots(1,1)

ax.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_xlim(x[ixmin],x[ixmax])
ax.set_ylim(y[iymin],y[iymax])
ax.set_title(run_name+' - Psi - t=%s'%(time[ind]))

fig.tight_layout()

x_ = []
y_ = []
tbu, = ax.plot(x_,y_,'--or')
cid = fig.canvas.mpl_connect('button_press_event', onclick_trasl)
cid2 = fig.canvas.mpl_connect('key_press_event', onclick_close)
fig.canvas.draw()
fig.canvas.start_event_loop(timeout=-1)

xc = x_[0]
yc = y_[0]
xo = x_[1]
yo = y_[1]

# -> traslation
Dx = xo - xc
Dy = yo - yc
Dnx = int(Dx/dx)
Dny = int(Dy/dy)
Dx = float(Dnx)*dx
Dy = float(Dny)*dy

x = np.roll(x,Dnx)
for i in range(Dnx): x[i] = -dx*float(Dnx-i)

y = np.roll(y,Dny)
for i in range(Dny): y[i] = -dy*float(Dny-i)

E = np.roll(E,(Dnx,Dny),axis=(1,2))
B = np.roll(B,(Dnx,Dny),axis=(1,2))
Psi = np.roll(Psi,(Dnx,Dny),axis=(0,1))
J = np.roll(J,(Dnx,Dny),axis=(1,2))
n_p = np.roll(n_p,(Dnx,Dny),axis=(0,1))
u_p = np.roll(u_p,(Dnx,Dny),axis=(1,2))
n_e = np.roll(n_e,(Dnx,Dny),axis=(0,1))
u_e = np.roll(u_e,(Dnx,Dny),axis=(1,2))
e_demag = np.roll(e_demag,(Dnx,Dny),axis=(1,2))
p_demag = np.roll(p_demag,(Dnx,Dny),axis=(1,2))
e_demag_n = np.roll(e_demag_n,(Dnx,Dny),axis=(0,1))
p_demag_n = np.roll(p_demag_n,(Dnx,Dny),axis=(0,1))
anisp = np.roll(anisp,(Dnx,Dny),axis=(0,1))
anise = np.roll(anise,(Dnx,Dny),axis=(0,1))
bparlp = np.roll(bparlp,(Dnx,Dny),axis=(0,1))
bparle = np.roll(bparle,(Dnx,Dny),axis=(0,1))

#------------------------------------------------------
#trace trajectory
#----------------
f_tmp = interpolate.interp1d(np.arange(len(x)),x)
res_mult = 2
x_tr_max = f_tmp(np.linspace(0.,len(x)-1.,(len(x)-1)*res_mult+1))

# -> plot demag, Psi, and flow, and select key points
qrx = 70
qry = 15

plt.close('all')
plt.ion()
fig = plt.figure(1,figsize=(15,10))
ax = fig.subplots(2,1)

im0 = ax[0].contourf(x[ixmin:ixmax],y[iymin:iymax],e_demag[2,ixmin:ixmax,iymin:iymax,0].T,63)
fig.colorbar(im0,ax=ax[0])
ax[0].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_e[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_e[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].set_title(run_name+' - e_demag - t=%s'%(time[ind]))

im1 = ax[1].contourf(x[ixmin:ixmax],y[iymin:iymax],p_demag[2,ixmin:ixmax,iymin:iymax,0].T,63)
fig.colorbar(im1,ax=ax[1])
ax[1].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_p[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_p[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[1].set_xlabel('x')
ax[1].set_ylabel('y')
ax[1].set_title(run_name+' - p_demag - t=%s'%(time[ind]))

fig.tight_layout()

x_ = []
y_ = []
x_tr = []
y_tr = []
tbu00, = ax[0].plot(x_,y_,'or')
tbu01, = ax[0].plot(x_tr,y_tr,'--r')
tbu10, = ax[1].plot(x_,y_,'or')
tbu11, = ax[1].plot(x_tr,y_tr,'--r')
npoints = 20
cid = fig.canvas.mpl_connect('button_press_event', onclick_trace)
cid2 = fig.canvas.mpl_connect('key_press_event', onclick_close)
fig.canvas.draw()
fig.canvas.start_event_loop(timeout=-1)

#------------------------------------------------------
#fields inetrpolation
#--------------------
E_tr = field_interp(E,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
B_tr = field_interp(B,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
Psi_tr = field_interp(Psi,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
J_tr = field_interp(J,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
n_p_tr = field_interp(n_p,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
u_p_tr = field_interp(u_p,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
n_e_tr = field_interp(n_e,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
u_e_tr = field_interp(u_e,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
e_demag_tr = field_interp(e_demag,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
p_demag_tr = field_interp(p_demag,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
e_demag_n_tr = field_interp(e_demag_n,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
p_demag_n_tr = field_interp(p_demag_n,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
anisp_tr = field_interp(anisp,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
anise_tr = field_interp(anise,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
bparlp_tr = field_interp(bparlp,(x_tr-x[0])/dx,(y_tr-y[0])/dy)
bparle_tr = field_interp(bparle,(x_tr-x[0])/dx,(y_tr-y[0])/dy)

#------------------------------------------------------
#final plots
#-----------
# -> plot demag, Psi, flow, and trajectory
qrx = 70
qry = 15

plt.close('all')
plt.ioff()
fig,ax = plt.subplots(2,1,figsize=(15,10))

im0 = ax[0].contourf(x[ixmin:ixmax],y[iymin:iymax],e_demag_n[ixmin:ixmax,iymin:iymax,0].T,63)
plt.colorbar(im0,ax=ax[0])
ax[0].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_e[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_e[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[0].plot(x_tr,y_tr,'--r')
ax[0].plot(x_,y_,'or')
ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].set_xlim(x[ixmin],x[ixmax])
ax[0].set_ylim(y[iymin],y[iymax])
ax[0].set_title(run_name+' - e_demag - t=%s'%(time[ind]))

im1 = ax[1].contourf(x[ixmin:ixmax],y[iymin:iymax],p_demag_n[ixmin:ixmax,iymin:iymax,0].T,63)
plt.colorbar(im1,ax=ax[1])
ax[1].quiver(x[ixmin:ixmax:qrx],y[iymin:iymax:qry],
        u_p[0,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,u_p[1,ixmin:ixmax:qrx,iymin:iymax:qry,0].T,
        color='w',width=0.002)
ax[1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
ax[1].plot(x_tr,y_tr,'--r')
ax[1].plot(x_,y_,'or')
ax[1].set_xlabel('x')
ax[1].set_ylabel('y')
ax[1].set_xlim(x[ixmin],x[ixmax])
ax[1].set_ylim(y[iymin],y[iymax])
ax[1].set_title(run_name+' - p_demag - t=%s'%(time[ind]))

plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+out_dir+'/'+'plot_'+run_name+'_'+str(ind)+'.png')
plt.close()

# -> 1D B plot
plt.close('all')
plt.ioff()
fig,ax = plt.subplots(1,1,figsize=(10,5))

for i in range(3):
    ax.plot(x_tr,B_tr[i],label='B_%d'%(i))

ax.set_xlabel('x')
ax.set_ylabel('B_i')
ax.set_xlim(x_tr[0],x_tr[-1])
ax.set_title(run_name+' - B - t=%s'%(time[ind]))
ax.legend()

plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+out_dir+'/'+'1D_'+run_name+'_'+str(ind)+'.png')
plt.close()

# -> anis-bparl scatter
plt.close('all')
plt.ioff()
fig,ax = plt.subplots(1,1,figsize=(10,10))

ax.scatter(bparlp_tr,anisp_tr,c='g',label='p')
ax.scatter(bparle_tr,anise_tr,c='b',label='e')
ax.axhline(y=1.,c='k')
ax.set_xlabel('b_parl')
ax.set_ylabel('T_perp/T_parl')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_title(run_name+' - anis vs. bparl - t=%s'%(time[ind]))
ax.legend()

plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+out_dir+'/'+'scatter_'+run_name+'_'+str(ind)+'.png')
plt.close()
