#-----------------------------------------------------
#importing stuff
#---------------
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
import gc
import glob
from scipy.optimize import curve_fit
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot reconnection rate/flux\n')

#------------------------------------------------------
#Init
#------
opath = '/home/finelli/Downloads'
print('opath is set to:')
print(opath)

#insert run names
run_name_vec = ['LF_2d_DH','HVM_2d_DH','DH_run5_data0']
iPIC_run = []
for run_name in run_name_vec:
    if run_name.split('_')[0] == 'DH':
        iPIC_run.append(run_name)
print('\nrun_name_vec is set to:')
print(run_name_vec)

#compute normalization #HARDCODED!!!!!!
all_B0 = {}
all_tmult = {}
for run_name in run_name_vec:
    all_B0[run_name] = [1.,0.,0.25]
    if run_name in iPIC_run:
        all_B0[run_name] = np.divide(all_B0[run_name],100.)
    all_tmult[run_name] = 1.
    if run_name in iPIC_run:
        all_tmult[run_name] = all_tmult[run_name]*0.01

print('\nNormalizations are:')
print('0 -> Bx*Bx/sqrt(4*pi*ni*mi)')
print('1 -> Bx*B/sqrt(4*pi*ni*mi)')
print('2 -> B*B/sqrt(4*pi*ni*mi)')
#inorm = ml.secure_input('choose normalization: ',10,True)
inorm = 0
print('norm -> 0 is selected\n')

#data struct init
time = {}
flux = {}
rate = {}

for run_name in run_name_vec:
    out_dir = 'Psi_field/'+run_name
    ipath = opath+'/'+'Psi_field/'+run_name+'/tracked_points'

    #read files
    xfiles = glob.glob(ipath+'/xpoint_from_*.dat')
    ofiles = glob.glob(ipath+'/opoint_from_*.dat')

    xnum = len(xfiles)
    onum = len(ofiles)

    t = {}
    x = {}
    y = {}
    v = {}
    for i in range(xnum):
        f = open(xfiles[i],'r')
        ll = f.readlines()
        f.close()

        nt = len(ll)
        t[xfiles[i]] = np.empty(nt,dtype=np.float);   t0 = t[xfiles[i]][0]
        x[xfiles[i]] = np.empty(nt,dtype=np.long);    x0 = x[xfiles[i]][0]
        y[xfiles[i]] = np.empty(nt,dtype=np.long);    y0 = y[xfiles[i]][0]
        v[xfiles[i]] = np.empty(nt,dtype=np.float64); v0 = v[xfiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t[xfiles[i]][j] = ml.generic_cast(l[0],t0)
            x[xfiles[i]][j] = ml.generic_cast(l[1],x0)
            y[xfiles[i]][j] = ml.generic_cast(l[2],y0)
            v[xfiles[i]][j] = ml.generic_cast(l[3],v0)

    for i in range(onum):
        f = open(ofiles[i],'r')
        ll = f.readlines()
        f.close()
        
        nt = len(ll)
        t[ofiles[i]] = np.empty(nt,dtype=np.float);   t0 = t[ofiles[i]][0]
        x[ofiles[i]] = np.empty(nt,dtype=np.long);    x0 = x[ofiles[i]][0]
        y[ofiles[i]] = np.empty(nt,dtype=np.long);    y0 = y[ofiles[i]][0]
        v[ofiles[i]] = np.empty(nt,dtype=np.float64); v0 = v[ofiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t[ofiles[i]][j] = ml.generic_cast(l[0],t0)
            x[ofiles[i]][j] = ml.generic_cast(l[1],x0)
            y[ofiles[i]][j] = ml.generic_cast(l[2],y0)
            v[ofiles[i]][j] = ml.generic_cast(l[3],v0)

    #plot all
    plt.close()
    for i in range(xnum):
        plt.plot(x[xfiles[i]],t[xfiles[i]],label='X_'+str(i))
    for i in range(onum):
        plt.plot(x[ofiles[i]],t[ofiles[i]],'--',label='O_'+str(i))
    plt.legend()
    plt.xlabel('x');plt.ylabel('t');plt.title('Tracked singular points')
    plt.show()
    plt.close()

    #choose points
    ixp = ml.secure_input('Choose X-point (0 -> '+str(xnum-1)+'): ',10,True)
    iop = ml.secure_input('Choose O-point (0 -> '+str(onum-1)+'): ',10,True)
    if ( (len(t[xfiles[ixp]]) != len(t[ofiles[iop]])) or 
         (t[xfiles[ixp]][0] != t[ofiles[ixp]][0]) or (t[xfiles[ixp]][-1] != t[ofiles[ixp]][-1]) ):
        print('ERROR: X-point and O-point are tracked for different time ranges')
        sys.exit()

    #store data
    time[run_name] = t[xfiles[ixp]]
    if inorm < 2:
        flux[run_name] = np.divide(v[ofiles[iop]] - v[xfiles[ixp]],all_B0[run_name][0])
    else:
        flux[run_name] = np.divide(v[ofiles[iop]] - v[xfiles[ixp]],np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2))
    flux[run_name] = flux[run_name] - flux[run_name][0]
    if inorm == 0:
        rate[run_name] = np.divide(np.gradient(flux[run_name],time[run_name]),all_B0[run_name][0]/all_tmult[run_name])
    else:
        rate[run_name] = np.divide(np.gradient(flux[run_name],time[run_name]),np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2)/all_tmult[run_name])

#final plot
def to_fit(x,F0,g,C):
    return F0*(np.exp(g*(x-C))-1.)

g1 = {}
g2 = {}

flux_th0 = flux[run_name_vec[0]][40]
flux_th1 = flux[run_name_vec[0]][65]
it0 = {}
it1 = {}
for run_name in run_name_vec:
    it0[run_name] = ml.index_of_closest(flux_th0,flux[run_name])
    it1[run_name] = ml.index_of_closest(flux_th1,flux[run_name])

plt.close()
fig,ax = plt.subplots(1,2,figsize=(16,8))
for run_name in run_name_vec:
    i0 = it0[run_name]
    i1 = it1[run_name]
    ax[0].plot(time[run_name],flux[run_name],label=run_name)
    ax[1].plot(time[run_name],rate[run_name],label=run_name)
    p = np.polyfit(time[run_name][i0:i1],np.log(flux[run_name][i0:i1]),1)
    g2[run_name] = p[0]
    popt,_ = curve_fit(lambda x,F0,g: to_fit(x,F0,g,C=time[run_name][0]),time[run_name][0:i1],flux[run_name][0:i1],
                       p0=[flux[run_name][i0]/(np.exp(p[0]*(time[run_name][i0]-time[run_name][0]))-1),p[0]])#,time[run_name][0]])
    g1[run_name] = popt[1]
    ax[0].plot(time[run_name][0:i1],to_fit(time[run_name][0:i1],*popt,C=time[run_name][0]),'--r',
               label='$\gamma_{lin}=$'+str(round(popt[1],4)))
    ax[0].plot(time[run_name][i0:i1],np.exp(time[run_name][i0:i1]*p[0]+p[1]),'--k',
               label='$\gamma_{lin}=$'+str(round(p[0],4)))
ax[0].set_xlabel('$t\quad [\Omega_C^{-1}$]');ax[0].set_ylabel('$flux/B_0 [d_i]$')
ax[0].set_title('reconnected fluxes');ax[0].legend()
#ax[0].set_yscale('log')
ax[1].set_xlabel('$t\quad [\Omega_C^{-1}$]');ax[1].set_ylabel('$rate/B_0/v_A$')
ax[1].set_title('reconnection rates');ax[1].legend()
ax[1].set_yscale('log')
plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+'Psi_field/rec_flux_and_rate.png')
plt.close()

offset = {}
tmp = []
for run_name in run_name_vec:
    i0 = it0[run_name]
    i1 = it1[run_name]
    this_time = time[run_name]-time[run_name][i0]
    popt,_ = curve_fit(to_fit,time[run_name][0:i1],flux[run_name][0:i1],
                       p0=[np.abs(flux[run_name][0]),p[0],0.])
    tmp.append(to_fit(time[run_name][i0],*popt))
cnt = 0
for run_name in run_name_vec:
    offset[run_name] = np.max(tmp) - tmp[cnt]
    cnt += 1

plt.close()
fig,ax = plt.subplots(1,2,figsize=(16,8))
for run_name in run_name_vec:
    i0 = it0[run_name]
    i1 = it1[run_name]
    this_time = time[run_name]-time[run_name][i0]
    ax[0].plot(this_time,flux[run_name]+offset[run_name],label=run_name+' - $t_0=$'+str(time[run_name][i0]))
    ax[1].plot(this_time,rate[run_name],label=run_name)
    popt,_ = curve_fit(to_fit,time[run_name][0:i1],flux[run_name][0:i1],
                       p0=[np.abs(flux[run_name][0]),p[0],0.])
    ax[0].plot(this_time[i0:i1],to_fit(time[run_name][i0:i1],*popt)+offset[run_name],'--k',
               label='$\gamma_{lin}=$'+str(round(popt[1],4)))
ax[0].set_xlabel('$\Delta t=t-t_0\quad [\Omega_C^{-1}]$');ax[0].set_ylabel('$F/B_0\quad [d_i]$')
ax[0].set_title('reconnected fluxes');ax[0].legend()
ax[0].set_yscale('log')
ax[1].set_xlabel('$\Delta t=t-t_0\quad [\Omega_C^{-1}]$');ax[1].set_ylabel('$R/B_0/v_A$')
ax[1].set_title('reconnection rates');ax[1].legend()
ax[1].set_yscale('log')
plt.tight_layout()
plt.show()
#plt.savefig(opath+'/'+'Psi_field/rec_flux_and_rate.png')
plt.close()
