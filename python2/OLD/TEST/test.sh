#!/bin/bash

MYPATH=$(pwd)

thJ=0.25
thP=0.25
thC=0.25

seg=28
ang=13

FILE=$MYPATH'/fast_input_'$seg'_'$ang'.dat'
cat > $FILE <<EOF
$seg
$ang
'gaus1'
0
100000
$thJ
$thP
$thC
1
'n'

EOF

python pvi_scalogram.py < $FILE

rm -f $FILE
