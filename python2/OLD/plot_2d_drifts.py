#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of some drifts.\n')

#------------------------------------------------------
#Init
#----
input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']

plt_show_flg = True
zoom_flg = True
rms_flg = True

EXB_dict = {} # - e * n * E x B / B^2
PrD_dict = {} # B x Nabla Pperpe / B^2
CrD_dict = {} # B x [ ( Pparle - Pperpe - B^2 ) * b . Nabla b ] / B^2
Psi_dict = {} # B = Bz * ez - ez x Nabla Psi
t = {}
x = {}
y = {}

run_name_vec = []
run_labels = {}
ind_dict = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind,_,_ = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    ind_dict[run_name] = ind
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    x_ = run.meta['x']
    y_ = run.meta['y']
    code_name = run.meta['code_name']
#
    if code_name == 'HVM':
        smooth_flag = False
        calc = fibo('calc')
        calc.meta = run.meta
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
        B0 = 1.
        n0 = 1.
        V0 = 1.
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
        gfsp = 2
        gfse = 2
        B0 = 0.01
        n0 = 1./(4.*np.pi)
        V0 = 0.01
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
#
    #get data
    #magnetic fields and co.
    E,B = run.get_EB(ind)
    B2 = np.sum(np.square(B),axis=0)
    b = np.divide(B,np.sqrt(B2))
#
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Psi_dict[run_name] = Psi[ixmin:ixmax,iymin:iymax]/B0
    del Psi
#
    if code_name == 'HVM':
        n_e,_ = run.get_Ion(ind)
    elif code_name == 'iPIC':
        n_e,_ = run.get_Ion(ind,qom=qom_e)
    else:
        print('ERROR: unknown code_name!')
        sys.exit(-1)
#
    if not w_ele:
        pperpe = n_e*run.meta['beta']*0.5*run.meta['teti'] # pperpe = Pisoe = n_e * T_e,0
        pparle = pperpe # pparle = pperpe
    else:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            pparle = np.multiply(pparle,n_e) # pparle = n_e*tparle
            pperpe = np.multiply(pperpe,n_e) # pperpe = n_e*tperpe
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B) # pparle = Pe:bb
            del Pe                                    # pperpe = Pe:(id - bb)/2
#
    EXBx = - n_e*(E[1]*B[2] - E[2]*B[1])/B2
    EXBy = - n_e*(E[2]*B[0] - E[0]*B[2])/B2
    EXBplane = np.sqrt(np.square(EXBx[...,0])+np.square(EXBy[...,0]))
    del EXBx,EXBy
    if smooth_flag:
        EXBplane = gf(EXBplane,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        EXB_rms = np.sqrt(np.mean( 
                  EXBplane[ixmin:ixmax,iymin:iymax]*EXBplane[ixmin:ixmax,iymin:iymax] ))
        EXB_dict[run_name] = EXBplane[ixmin:ixmax,iymin:iymax]/EXB_rms
    else:
        EXB_dict[run_name] = EXBplane[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del EXBplane
#
    gradpperpe = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    gradpperpe[0] = np.gradient(pperpe,x_,axis=0,edge_order=2)
    gradpperpe[1] = np.gradient(pperpe,y_,axis=1,edge_order=2)
    #gradpperpe[2] = np.zeros(pperpe.shape,dtype=type(pperpe[0,0,0]))
    PrDx = -B[2]*gradpperpe[1]/B2#(B[1]*gradpperpe[2] - B[2]*gradpperpe[1])/B2
    PrDy =  B[2]*gradpperpe[0]/B2#(B[2]*gradpperpe[0] - B[0]*gradpperpe[2])/B2
    del gradpperpe
    PrDplane = np.sqrt(np.square(PrDx[...,0])+np.square(PrDy[...,0]))
    del PrDx,PrDy
    if smooth_flag:
        PrDplane = gf(PrDplane,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        PrD_rms = np.sqrt(np.mean(
                  PrDplane[ixmin:ixmax,iymin:iymax]*PrDplane[ixmin:ixmax,iymin:iymax] ))
        PrD_dict[run_name] = PrDplane[ixmin:ixmax,iymin:iymax]/PrD_rms
    else:
        PrD_dict[run_name] = PrDplane[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del PrDplane
# b . Nabla b = bi * Di bj = [ (bx * Dx + by * Dy) bx , (bx * Dx + by * Dy) by , (bx * Dx + by * Dy) bz ]
#                          = [  b[0]*dxbx + b[1]*dybx ,  b[0]*dxby + b[1]*dyby ,  b[0]*dxbz + b[1]*dybz ]  
    dxbx = np.gradient(b[0],x_,axis=0,edge_order=2)
    dybx = np.gradient(b[0],y_,axis=1,edge_order=2)
    dxby = np.gradient(b[1],x_,axis=0,edge_order=2)
    dyby = np.gradient(b[1],y_,axis=1,edge_order=2)
    dxbz = np.gradient(b[2],x_,axis=0,edge_order=2)
    dybz = np.gradient(b[2],y_,axis=1,edge_order=2)
    b_Nb = np.array([b[0]*dxbx+b[1]*dybx, b[0]*dxby + b[1]*dyby, b[0]*dxbz + b[1]*dybz])
    del dxbx,dybx,dxby,dyby,dxbz,dybz
    P_b_Nb = (pparle - pperpe - B2)*b_Nb
    CrDx = (B[1]*P_b_Nb[2] - B[2]*P_b_Nb[1])/B2
    CrDy = (B[2]*P_b_Nb[0] - B[0]*P_b_Nb[2])/B2
    del P_b_Nb
    CrDplane = np.sqrt(np.square(CrDx[...,0])+np.square(CrDy[...,0]))
    del CrDx,CrDy
    if smooth_flag:
        CrDplane = gf(CrDplane,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        CrD_rms = np.sqrt(np.mean(
                  CrDplane[ixmin:ixmax,iymin:iymax]*CrDplane[ixmin:ixmax,iymin:iymax] ))
        CrD_dict[run_name] = CrDplane[ixmin:ixmax,iymin:iymax]/PrD_rms
    else:
        CrD_dict[run_name] = CrDplane[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del CrDplane
#
    x[run_name] = x_[ixmin:ixmax]
    y[run_name] = y_[iymin:iymax]
    del x_,y_
#
    gc.collect()

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
fig,ax = plt.subplots(3,3,figsize=(16,10),sharex=True,sharey=True)
fig.subplots_adjust(hspace=.06,wspace=.03,top=.95,bottom=.1,left=.055,right=1.075)#.5#1.07

vminEXB = ml.min_dict(EXB_dict)
vmaxEXB = ml.max_dict(EXB_dict)
vminPrD = ml.min_dict(PrD_dict)
vmaxPrD = ml.max_dict(PrD_dict)
vminCrD = ml.min_dict(CrD_dict)
vmaxCrD = ml.max_dict(CrD_dict)
for j,run_name in enumerate(run_name_vec):
    i = 0
    im1 = ax[i,j].pcolormesh(x[run_name],y[run_name],EXB_dict[run_name].T,shading='gouraud',
                             vmin=vminEXB,vmax=vmaxEXB)
    ax[i,j].contour(x[run_name],y[run_name],Psi_dict[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
    ax[i,j].set_title(run_labels[run_name])
#
    i = 1
    im2 = ax[i,j].pcolormesh(x[run_name],y[run_name],PrD_dict[run_name].T,shading='gouraud',
                           vmin=vminPrD,vmax=vmaxPrD)
    ax[i,j].contour(x[run_name],y[run_name],Psi_dict[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
#
    i = 2
    im3 = ax[i,j].pcolormesh(x[run_name],y[run_name],CrD_dict[run_name].T,shading='gouraud',
                          vmin=vminCrD,vmax=vmaxCrD)
    ax[i,j].contour(x[run_name],y[run_name],Psi_dict[run_name].T,8,colors='white')
    ax[i,j].set_xlabel('$x\quad [d_i]$')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')

cb1 = plt.colorbar(im1,ax=ax[0,:],pad=.007)
if rms_flg:
    label_ = '$E\\times B\\text{-drift}\quad [r.m.s. norm.]$'
else:
    label_ = '$J_z / ( e n_0 v_A )$'
cb1.set_label(label_,rotation=270,labelpad=25)

cb2 = plt.colorbar(im2,ax=ax[1,:],pad=.007)
if rms_flg:
    label_ = '$\\nabla p_{\perp,e}\\text{-drift}\quad [r.m.s. norm.]$'
else:
    label_ = '$\\nabla p_{\perp,e}\\text{-drift} / ( e n_0 v_A )$'
cb2.set_label(label_,rotation=270,labelpad=20)#20)

cb3 = plt.colorbar(im3,ax=ax[2,:],pad=.007)
if rms_flg:
    label_ = '$b\cdot\\nabla b\\text{-drift}\quad [r.m.s. norm.]$'
else:
    label_ = '$b\cdot\\nabla b\\text{-drift} / ( e n_0 v_A )$'
cb3.set_label(label_,rotation=270,labelpad=45)#40)

if plt_show_flg:
    plt.show()

out_dir = 'plot_2d_drifts'
ml.create_path(opath+'/'+out_dir)
out_dir += '/comp'
fig_name = 'difts'
for run_name in run_name_vec:
    out_dir += '__' + run_name
    fig_name += '__' + run_labels[run_name] + '_ind%d'%ind_dict[run_name]

if zoom_flg:
    fig_name += '__zoom'
if rms_flg:
    fig_name += '__rms'
fig_name += '.png'
ml.create_path(opath+'/'+out_dir)
fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
