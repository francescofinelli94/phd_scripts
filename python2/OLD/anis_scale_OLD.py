#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy jump at a given scale langth, both in parallel and perpendicular direction (relative to the local field).\n')

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
code_name = l0[1]
ipath = l1[0]
opath = l2[0]

if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')
try:
    run.get_meta()
except:
    run.get_meta(l5[0])

w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)

time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']

calc = fibo('calculator')
calc.meta = run.meta

print('\nMetadata loaded\n')

print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#-------------------------------------------------------
#Ask for stuff
#-------------
print('\ndx,dy,dz [d_i]')
print(str(run.meta['ddd']))
#lmin = 0. #ml.secure_input('l_min [d_i]: ',1.0,True)
lmax = ml.secure_input('l_max [d_i]: ',1.0,True)
#dl   = ml.secure_input('dl [d_i]: ',1.0,True)
dth  = ml.secure_input('dth [rad]: ',1.0,True)
#nbin = ml.secure_input('nbin: ',10,True)

#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]

@njit
def fill_hist_function_2d(field,B,lmax,dth,dd,nn):
    """
    field -> array[nx,ny]
    B -> array[2,nx,ny] (only B in plane)
    parl -> array[n] [...,[l_parl,deta_A],...]
    perp -> array[n] [...,[l_perp,deta_A],...]
    """
    dx = dd[0]
    dy = dd[1]
    nx = nn[0]
    ny = nn[1]
#
    lxgrid = int(lmax/dx)
    lygrid = int(lmax/dy)
    lgrid2 = lxgrid*lxgrid + lygrid*lygrid
#
    distx = range(-lxgrid,lxgrid+1)
    disty = range(-lygrid,lygrid+1)
#    
    parl = []
    perp = []
    for (i,j) in [(i,j) for i in range(nx) for j in range(ny)]:
        xsub = [(i+n+nx)%nx for n in distx]
        ysub = [(j+n+ny)%ny for n in disty]
        for (di,dj) in [(di,dj) for di in distx for dj in disty]:
            if ((di*di+dj*dj) > lgrid2) or ((di*di+dj*dj) == 0):
                continue
            ii = (i+di+nx)%nx
            jj = (j+dj+ny)%ny
#
            bx = 0.5*(B[0,i,j] + B[0,ii,jj])
            by = 0.5*(B[1,i,j] + B[1,ii,jj])
#
            if bx != 0.:
                th1 = np.arctan(by/bx)
            else:
                th1 = np.pi*0.5
            if di != 0.:
                th2 = np.arctan(dj*dy/(di*dx))
            else:
                th2 = np.pi*0.5
            th = np.abs(th1-th2)
            th = min(th,np.pi-th)
#
            if th < dth:
                parl.append([np.sqrt(di*di*dx*dx+dj*jj*dy*dy),np.abs(field[i,j]-field[ii,jj])])
            elif th > (0.5*np.pi-dth):
                perp.append([np.sqrt(di*di*dx*dx+dj*dj*dy*dy),np.abs(field[i,j]-field[ii,jj])])
            else:
                continue
    return parl,perp

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    E,B = run.get_EB(ind)
    del E
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPiC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')

    #hist
    anisp = np.divide(pperpp,pparlp) - 1.
    anisp_parl,anisp_perp = fill_hist_function_2d(anisp[...,0],B[0:2,...,0],lmax,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2])
    del anisp
    if w_ele:
        anise = np.divide(pperpe,pparle) - 1.
        anise_parl,anise_perp = fill_hist_function_2d(anise[...,0],B[0:2,...,0],lmax,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2])
        del anise

    hp_parl,lbinp_parl,abinp_parl = plt.hist2d(anisp_parl[0],anisp_parl[1])
    hp_perp,lbinp_perp,abinp_perp = plt.hist2d(anisp_perp[0],anisp_perp[1])
    he_parl,lbine_parl,abine_parl = plt.hist2d(anise_parl[0],anise_parl[1])
    he_perp,lbine_perp,abine_perp = plt.hist2d(anise_perp[0],anise_perp[1])

    #plot
    ml.create_path(opath+'/anis_pdf')
    plt.cose()
    fig,ax = plt.subplot(2,2,figsize=(10,10))

    axs = ax[0,0]
    im = axs.contourf(lbinp_parl,abinp_parl,hp_parl.T)
    plt.colorbar(im,ax=axs)
    axs.set_title('Proton parl - t='+str(time[ind]))
    axs.set_xlabel('l_parl [d_i]')
    axs.set_ylabel('delta_a [u.aa]')

    axs = ax[0,1]
    im = axs.contourf(lbine_parl,abine_parl,he_parl.T)
    plt.colorbar(im,ax=axs)
    axs.set_title('Electron parl')
    axs.set_xlabel('l_parl [d_i]')
    axs.set_ylabel('delta_a [u.aa]')

    axs = ax[1,0]
    im = axs.contourf(lbinp_perp,abinp_perp,hp_perp.T)
    plt.colorbar(im,ax=axs)
    axs.set_title('Proton perp')
    axs.set_xlabel('l_perp [d_i]')
    axs.set_ylabel('delta_a [u.aa]')

    axs = ax[1,1]
    im = axs.contourf(lbine_perp,abine_perp,he_perp.T)
    plt.colorbar(im,ax=axs)
    axs.set_title('Electron perp')
    axs.set_xlabel('l_perp [d_i]')
    axs.set_ylabel('delta_a [u.aa]')

    plt.tight_layout()
    plt.savefig(opath+'/anis_pdf/'+'anis_pdf_'+run_name+'_'+str(ind)+'.png')
    plt.close()

#---> loop over time <---
    act_cnt = act_cnt + 1
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
