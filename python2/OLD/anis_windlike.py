#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta.\n')

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()
input_file.close()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
code_name = l0[1]
ipath = l1[0]
opath = l2[0]

if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')
try:
    run.get_meta()
except:
    run.get_meta(l5[0])

w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)

time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']

if code_name == 'HVM':
    mime = run.meta['mime']
elif code_name == 'iPIC':
    mime = abs(run.meta['msQOM'][0]/run.meta['msQOM'][1])
else:
    print('\nWhat code is this?\n')

if code_name == 'HVM':
    teti = run.meta['teti']
elif code_name == 'iPIC':
    P0 = run.get_Pspec(1,'0') #HARDCODED stag
    P3 = run.get_Pspec(1,'3') #HARDCODED stag
    teti = np.mean([np.mean(np.divide(P0[0,0],P3[0,0])),
                    np.mean(np.divide(P0[1,1],P3[1,1])),
                    np.mean(np.divide(P0[2,2],P3[2,2]))])
    del P0,P3
else:
    print('\nWhat code is this?\n')

print('\nMetadata loaded\n')

print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#-------------------------------------------------------
#hardcoded inputs
#----------------
CS_cut = 170

#see Hellinger et al. 2006
a_PC = 0.43; b_PC = 0.42; b0_PC =-0.0004
a_MR = 0.77; b_MR = 0.76; b0_MR =-0.0160
a_PF =-0.47; b_PF = 0.53; b0_PF = 0.5900
a_OF =-1.40; b_OF = 1.00; b0_OF =-0.1100

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

#-------------------------------------------------------
#plot function
#-------------
def windlike_plot(ax,xdata,ydata,species_name,species_tag,b0_mult,t=None):
    h,xbin,ybin=np.histogram2d(np.log(np.array(xdata.flat)),np.log(np.array((ydata+1.).flat)),100)
    X,Y = np.meshgrid(np.exp(xbin),np.exp(ybin))
    im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_PC,b_PC,b0_PC*b0_mult),label='Prot. cycl.')
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_MR,b_MR,b0_MR*b0_mult),label='Mirror')
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_PF,b_PF,b0_PF*b0_mult),label='Parl. firehose')
    ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),a_OF,b_OF,b0_OF*b0_mult),label='Obl. firehose')
    ax.set_ylim(np.exp(ybin[0]),np.exp(ybin[-1]));ax.legend()
    ax.set_xlabel('Parallel '+species_name+' beta')
    ax.set_ylabel('P_perp_'+species_tag+'/P_parl_'+species_tag)
    if t != None:
        ax.set_title(species_name+' anisotropy t = '+str(t))
    else:
        ax.set_title(species_name+' anisotropy')
    ax.set_xscale('log');ax.set_yscale('log');plt.colorbar(im,ax=ax)

#-------------------------------------------------------
#select over threshold points
#-------------
@njit
def over_th(anis,bparl,nx,ny,CS_cut,CS,b0_mul):
    a_PC = 0.43; b_PC = 0.42; b0_PC =-0.0004
    a_MR = 0.77; b_MR = 0.76; b0_MR =-0.0160
    a_PF =-0.47; b_PF = 0.53; b0_PF = 0.5900
    a_OF =-1.40; b_OF = 1.00; b0_OF =-0.1100
    isoverMR = (anis+1.) > th_func(bparl,a_MR,b_MR,b0_MR*b0_mult)
    isoverOF = (anis+1.) < th_func(bparl,a_OF,b_OF,b0_OF*b0_mult)
    xMR = []
    yMR = []
    xOF = []
    yOF = []
    if CS == 1:
        jrange = range(CS_cut)
        toadd = 0
    elif CS == 2:
        jrange = range(ny-CS_cut)
        toadd = CS_cut
    for i in range(nx):
        for j in jrange:
            if isoverMR[i,j+toadd,0]:
                xMR.append(i)
                yMR.append(j+toadd)
            if isoverOF[i,j+toadd,0]:
                xOF.append(i)
                yOF.append(j+toadd)
    return np.array(xMR),np.array(yMR),np.array(xOF),np.array(yOF)

#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    E,B = run.get_EB(ind)
    del E
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp) - 1.
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        del pparle
    del B2
    del B
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    ml.create_path(opath+'/anis_windlike')
    plt.close()
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig,ax = plt.subplots(1*3,nplot,figsize=(5*nplot,4*3))
#
    if w_ele:
        ax0 = ax[0,0]
        ax1 = ax[1,0]
        ax2 = ax[2,0]
    else:
        ax0 = ax[0]
        ax1 = ax[1]
        ax2 = ax[2]
    windlike_plot(ax0,bparlp,anisp,'Protons','p',1.,t=time[ind])
    windlike_plot(ax1,bparlp[:,:CS_cut,:],anisp[:,:CS_cut,:],'Protons CS1','p',1.)
    windlike_plot(ax2,bparlp[:,CS_cut:,:],anisp[:,CS_cut:,:],'Protons CS2','p',1.)
#
    if w_ele:
        windlike_plot(ax[0,1],bparle,anise,'Electrons','e',teti)
        windlike_plot(ax[1,1],bparle[:,:CS_cut,:],anise[:,:CS_cut,:],'Electrons CS1','e',teti)
        windlike_plot(ax[2,1],bparle[:,CS_cut:,:],anise[:,CS_cut:,:],'Electrons CS2','e',teti)
#
    plt.tight_layout()
    plt.savefig(opath+'/anis_windlike/'+'anis_windlike_'+run_name+'_'+str(ind)+'.png')
    plt.close()
#
    #------------------------------------------------------
    #plot 2d over threshold
    #----------------------
    ml.create_path(opath+'/anis_overth')
    plt.close()
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig,ax = plt.subplots(1*2,nplot,figsize=(5*nplot,4*2))
#
    nothing = np.log(bparlp[...,0]*0-1)
#
    if w_ele:
        ax0 = ax[0,0]
        ax1 = ax[1,0]
    else:
        ax0 = ax[0]
        ax1 = ax[1]
    xMR,yMR,xOF,yOF=over_th(anisp,bparlp,run.meta['nx'],run.meta['ny'],CS_cut,1,1.)
    ax0.contourf(nothing[:,:CS_cut].T)
    ax0.scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
    ax0.scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
    MRperc = 100.*float(len(xMR))/float(run.meta['nx']*CS_cut)
    OFperc = 100.*float(len(xOF))/float(run.meta['nx']*CS_cut)
    ax0.set_title('Prot. CS1 - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
    ax0.legend()
#
    xMR,yMR,xOF,yOF=over_th(anisp,bparlp,run.meta['nx'],run.meta['ny'],CS_cut,2,1.)
    ax1.contourf(range(run.meta['nx']),np.array(range(run.meta['ny']-CS_cut))+CS_cut,nothing[:,CS_cut:].T)
    ax1.scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
    ax1.scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
    MRperc = 100.*float(len(xMR))/float(run.meta['nx']*(run.meta['ny']-CS_cut))
    OFperc = 100.*float(len(xOF))/float(run.meta['nx']*(run.meta['ny']-CS_cut))
    ax1.set_title('Prot. CS2 - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
    ax1.legend()
#
    if w_ele:
        xMR,yMR,xOF,yOF=over_th(anise,bparle,run.meta['nx'],run.meta['ny'],CS_cut,1,teti)
        ax[0,1].contourf(nothing[:,:CS_cut].T)
        ax[0,1].scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
        ax[0,1].scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
        MRperc = 100.*float(len(xMR))/float(run.meta['nx']*CS_cut)
        OFperc = 100.*float(len(xOF))/float(run.meta['nx']*CS_cut)
        ax[0,1].set_title('Ele. CS1 - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
        ax[0,1].legend()
    #
        xMR,yMR,xOF,yOF=over_th(anise,bparle,run.meta['nx'],run.meta['ny'],CS_cut,2,teti)
        ax[1,1].contourf(range(run.meta['nx']),np.array(range(run.meta['ny']-CS_cut))+CS_cut,nothing[:,CS_cut:].T)
        ax[1,1].scatter(xOF,yOF,c='b',s=0.5,label='Obl. firehose inst.')
        ax[1,1].scatter(xMR,yMR,c='r',s=0.5,label='Mirror inst.')
        MRperc = 100.*float(len(xMR))/float(run.meta['nx']*(run.meta['ny']-CS_cut))
        OFperc = 100.*float(len(xOF))/float(run.meta['nx']*(run.meta['ny']-CS_cut))
        ax[1,1].set_title('Ele. CS2 - MR='+str(round(MRperc,4))+'% OF='+str(round(OFperc,4))+'%')
        ax[1,1].legend()
#
    plt.tight_layout()
    plt.savefig(opath+'/anis_overth/'+'anis_overth_'+run_name+'_'+str(ind)+'.png')
    plt.close()
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
