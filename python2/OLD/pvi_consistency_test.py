import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import glob
import work_lib as wl
import scipy.ndimage.interpolation as ndm
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)


ipath='/work1/califano/HVM2D/HVM_large'
pvi_path = '/home/sisti/python_routines/VIRTUAL_SATELLITE'
run = from_HVM(ipath)
run.get_meta('01')
run_name = 'HVM_large'
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']

calc = fibo('calc')
calc.meta=run.meta

#t_ = ml.secure_input('Choose time (float): ',1.,True) #247. #494.
t_ = 247. #494.
ind = ml.index_of_closest(t_,run.meta['time'])

#----------------------------------------------------------------------------------------------------

#PRIMA PARTE: ricostruzione traiettoria, le coordinate finali xco e yco sono in unita' [di]

#input_angle  = ml.secure_input('Choose angle (integer): ',10,True) #5 #8 #13 #75 #80 #85
input_angle = 5 #8 #13 #75 #80 #85
input_folder = run.meta['time2seg'][ind]
npvi_path = pvi_path + '/new_pvi_%s/angle%.d'%(input_folder,input_angle)
cpvi_path = pvi_path + '/pvi_%s'%(input_folder)

pi2 = 2.0*np.pi
theta_s = float(input_angle)*pi2/360.0

ll ={}
sco = {}
xco = {}
yco = {}
PVI = {}
ds  = {}
files = {}
files['n'] = glob.glob(npvi_path+'/B_1Dcut_pvi_*.dat')
files['c'] = glob.glob(cpvi_path+'/B_1Dcut_pvi_*_center.dat')
for key in ['n','c']:
    if key == 'n':
        jj = 1
    else:
        jj = 2
    ll[key] = []
    for f in files[key]: ll[key].append(int(f.split('_')[-jj].split('.')[0]))
    files[key] = [tmp for _,tmp in sorted(zip(ll[key],files[key]))]
    ll[key] = sorted(ll[key])

    lstart = ll[key][0]
    for il,l,fname in zip(range(len(ll[key])),ll[key],files[key]):
        f = open(fname, 'r')
        NN = len(f.readlines())
        f.seek(0)
        if (l == lstart):
            sco[key] = np.zeros((NN),dtype=np.float64)
            xco[key] = np.zeros((NN),dtype=np.float64)
            yco[key] = np.zeros((NN),dtype=np.float64)
            PVI[key] = np.full((NN,len(ll[key])),-1.,dtype=np.float64)
        for i in range(NN):
            tr = f.readline().split()
            if (l == lstart):
                s_tmp = float(tr[0])
                sco[key][i] = s_tmp
                x_tmp = s_tmp*np.cos(theta_s)
                if x_tmp >= pi2:
                    x_tmp -= int(x_tmp/pi2)*pi2
                xco[key][i] = x_tmp
                y_tmp = s_tmp*np.sin(theta_s)
                if y_tmp >= pi2:
                    y_tmp -= int(y_tmp/pi2)*pi2
                yco[key][i] = y_tmp
            PVI[key][i,il] = float(tr[1])
        f.close()

    sco[key] = sco[key]*xl/pi2
    xco[key] = xco[key]*xl/pi2
    yco[key] = yco[key]*yl/pi2

    ds[key] = sco[key][1] - sco[key][0]

ll['c'] = [2**val for val in ll['c']]

for i,l in zip(range(len(ll['n'])),ll['n']):
    PVI['n'][:,i] = np.roll(PVI['n'][:,i],l//2)


#----------------------------------------------------------------------------------------------------

#SECONDA PARTE: interpolazione

#calcolare dati
_,B = run.get_EB(ind)

cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
J = np.array([cx,cy,cz])
del cx,cy,cz

Jn = np.sqrt(J[0]**2 + J[1]**2 + J[2]**2)

#interpolazione vera e propria
new_coordsx = {}
new_coordsy = {}

for key in ['n','c']:
    new_coordsx[key] = xco[key]/dx
    new_coordsy[key] = yco[key]/dy

Bx_an = {}
By_an = {}
Bz_an = {}

Jn_an = {}

for key in ['n','c']:
    Bx_an[key] = ndm.map_coordinates(B[0,:,:,0].T,np.vstack((new_coordsx[key],new_coordsy[key])),mode='wrap')
    By_an[key] = ndm.map_coordinates(B[1,:,:,0].T,np.vstack((new_coordsx[key],new_coordsy[key])),mode='wrap')
    Bz_an[key] = ndm.map_coordinates(B[2,:,:,0].T,np.vstack((new_coordsx[key],new_coordsy[key])),mode='wrap')
    Jn_an[key] = ndm.map_coordinates(Jn[:,:,0].T,np.vstack((new_coordsx[key],new_coordsy[key])),mode='wrap')

dBx_ds = {}
dBy_ds = {}
dBz_ds = {}

for key in ['n','c']:
    dBx_ds[key] = np.gradient(Bx_an[key],sco[key])
    dBy_ds[key] = np.gradient(By_an[key],sco[key])
    dBz_ds[key] = np.gradient(Bz_an[key],sco[key])

#----------------------------------------------------------------------------------------------------

lllist = [2] #[2,4]
ill = {}
for key in ['c','n']:
    ill[key] = []
    for l in lllist:
        ill[key].append(np.argmin(np.abs(np.array(ll[key])-l)))


xr = [500.,525.] #[512.,514.] #[min(sco['c'][0],sco['n'][0]),max(sco['c'][-1],sco['n'][-1])]

plt.close()
fig, ((ax0),(ax1),(ax2)) = plt.subplots(3,1,figsize=(8,4*(1+1+1)),gridspec_kw={'height_ratios':[1,1,1]})

for n in range(len(lllist)):
    im0 = ax0.plot(sco['c'],PVI['c'][:,ill['c'][n]]/np.max(PVI['c'][:,ill['c'][n]]),label='PVI'+str(ll['c'][ill['c'][n]])+' auto-centering')
    im0 = ax0.plot(sco['n'],PVI['n'][:,ill['n'][n]]/np.max(PVI['n'][:,ill['n'][n]]),'--',label='PVI'+str(ll['n'][ill['n'][n]])+' manual-centering')
ax0.set_xlim(*xr)
ax0.set_ylim(0.,1.)

ax0.set_ylabel('$PVI$')
ax0.legend()

im1 = ax1.plot(sco['c'],dBx_ds['c'],label='$dB_x/ds$')
im1 = ax1.plot(sco['c'],dBy_ds['c'],label='$dB_y/ds$')
im1 = ax1.plot(sco['c'],dBz_ds['c'],label='$dB_z/ds$')
#im1 = ax1.plot(sco['c'],np.sqrt(dBx_ds['c']**2+dBy_ds['c']**2+dBz_ds['c']**2),label='$|dB/ds|$')
ax1.set_xlim(*xr)
ax1.set_ylabel('$dB_i/ds$')
ax1.legend()

im2 = ax2.plot(sco['c'],Jn_an['c'],label='$|J|$')
ax2.set_xlim(*xr)
ax2.set_ylim(0.,max(np.max(Jn_an['c']),np.max(Jn_an['n'])))
ax2.set_ylabel('$J$')
ax2.set_xlabel('$s\quad [d_i]$')
ax2.legend()

plt.tight_layout()
plt.show()
plt.close()

