#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta for one time.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'windlike_plot_v5'
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#see Hellinger et al. 2006
#    Astfalk and Jenko 2016
#    Lazar et al. 2015
params = {}
params['H2006'] = {}
params['A2016'] = {}
params['L2015'] = {}

params['H2006']['PC'] = {}
params['H2006']['PC']['10m3'] = {}
params['H2006']['PC']['10m3']['bM'] = [ 0.43,0.42,-0.0004]

params['L2015']['EC'] = {}
params['L2015']['EC']['e05'] = {}
params['L2015']['EC']['e05']['10m2'] = {}
params['L2015']['EC']['e05']['10m2']['k2'] = {}
params['L2015']['EC']['e05']['10m2']['k2']['T'] = [0.54,0.21,0.14,1.28]
params['L2015']['EC']['e05']['10m2']['k2']['C'] = [0.26,-0.01,1.18,0.56]
params['L2015']['EC']['e05']['10m2']['k6'] = {}
params['L2015']['EC']['e05']['10m2']['k6']['T'] = [0.43,0.17,0.02,2.04]
params['L2015']['EC']['e05']['10m2']['k6']['C'] = [0.19,-0.09,1.11,0.63]
params['L2015']['EC']['e05']['10m2']['bM'] = {}
params['L2015']['EC']['e05']['10m2']['bM']['T'] = [0.40,0.10,0.05,1.26]
params['L2015']['EC']['e05']['10m2']['bM']['C'] = [0.17,-0.11,1.18,0.65]
params['L2015']['EC']['e05']['10m3'] = {}
params['L2015']['EC']['e05']['10m3']['k2'] = {}
params['L2015']['EC']['e05']['10m3']['k2']['T'] = [0.10,0.30,0.09,1.22]
params['L2015']['EC']['e05']['10m3']['k2']['C'] = [0.01,-0.46,19.1,0.97]
params['L2015']['EC']['e05']['10m3']['k6'] = {}
params['L2015']['EC']['e05']['10m3']['k6']['T'] = [0.08,0.38,0.003,2.54]
params['L2015']['EC']['e05']['10m3']['k6']['C'] = [0.005,-0.52,14.6,1.07]
params['L2015']['EC']['e05']['10m3']['bM'] = {}
params['L2015']['EC']['e05']['10m3']['bM']['T'] = [0.07,0.31,0.02,1.23]
params['L2015']['EC']['e05']['10m3']['bM']['C'] = [0.003,-0.67,21.3,1.23]
params['L2015']['EC']['e01'] = {}
params['L2015']['EC']['e01']['10m2'] = {}
params['L2015']['EC']['e01']['10m2']['k2'] = {}
params['L2015']['EC']['e01']['10m2']['k2']['T'] = [1.74,0.05,0.04,1.29]
params['L2015']['EC']['e01']['10m2']['k2']['C'] = [1.35,-0.04,0.30,0.60]
params['L2015']['EC']['e01']['10m2']['k6'] = {}
params['L2015']['EC']['e01']['10m2']['k6']['T'] = [1.54,0.06,0.003,1.86]
params['L2015']['EC']['e01']['10m2']['k6']['C'] = [1.27,-0.03,0.16,0.62]
params['L2015']['EC']['e01']['10m2']['bM'] = {}
params['L2015']['EC']['e01']['10m2']['bM']['T'] = [1.47,0.05,0.002,2.03]
params['L2015']['EC']['e01']['10m2']['bM']['C'] = [1.23,-0.03,0.14,0.63]
params['L2015']['EC']['e01']['10m3'] = {}
params['L2015']['EC']['e01']['10m3']['k2'] = {}
params['L2015']['EC']['e01']['10m3']['k2']['T'] = [0.21,0.12,0.04,1.28]
params['L2015']['EC']['e01']['10m3']['k2']['C'] = [0.11,-0.07,0.84,0.60]
params['L2015']['EC']['e01']['10m3']['k6'] = {}
params['L2015']['EC']['e01']['10m3']['k6']['T'] = [0.18,0.14,0.001,2.21]
params['L2015']['EC']['e01']['10m3']['k6']['C'] = [0.11,-0.06,0.47,0.63]
params['L2015']['EC']['e01']['10m3']['bM'] = {}
params['L2015']['EC']['e01']['10m3']['bM']['T'] = [0.17,0.15,0.0001,2.83]
params['L2015']['EC']['e01']['10m3']['bM']['C'] = [0.11,-0.05,0.40,0.64]

params['H2006']['MR'] = {}
params['H2006']['MR']['10m3'] = {}
params['H2006']['MR']['10m3']['bM'] = [ 0.77,0.76,-0.0160]

params['H2006']['PF'] = {}
params['H2006']['PF']['10m3'] = {}
params['H2006']['PF']['10m3']['bM'] = [-0.47,0.53, 0.5900]

params['H2006']['OF'] = {}
params['H2006']['OF']['10m3'] = {}
params['H2006']['OF']['10m3']['bM'] = [-1.40,1.00,-0.1100]

params['A2016']['PF'] = {}
params['A2016']['PF']['10m3'] = {}
params['A2016']['PF']['10m3']['bM']  = [-0.487,0.537, 0.560]
params['A2016']['PF']['10m3']['k12'] = [-0.438,0.475, 0.503]
params['A2016']['PF']['10m3']['k8']  = [-0.429,0.486, 0.423]
params['A2016']['PF']['10m3']['k6']  = [-0.417,0.498, 0.350]
params['A2016']['PF']['10m3']['k4']  = [-0.387,0.518, 0.226]
params['A2016']['PF']['10m3']['k2']  = [-0.274,0.536, 0.042]
params['A2016']['PF']['10m2'] = {}
params['A2016']['PF']['10m2']['bM']  = [-0.701,0.623, 0.599]
params['A2016']['PF']['10m2']['k12'] = [-0.656,0.596, 0.567]
params['A2016']['PF']['10m2']['k8']  = [-0.623,0.579, 0.569]
params['A2016']['PF']['10m2']['k6']  = [-0.625,0.585, 0.501]
params['A2016']['PF']['10m2']['k4']  = [-0.625,0.593, 0.379]
params['A2016']['PF']['10m2']['k2']  = [-0.632,0.589, 0.139]
params['A2016']['PF']['10m1'] = {}
params['A2016']['PF']['10m1']['bM']  = [-0.872,0.495, 1.233]
params['A2016']['PF']['10m1']['k12'] = [-0.899,0.502, 1.213]
params['A2016']['PF']['10m1']['k8']  = [-0.937,0.509, 1.097]
params['A2016']['PF']['10m1']['k6']  = [-0.947,0.505, 1.088]
params['A2016']['PF']['10m1']['k4']  = [-0.977,0.496, 1.068]
params['A2016']['PF']['10m1']['k2']  = [-1.230,0.464, 1.206]

params['A2016']['OF'] = {}
params['A2016']['OF']['10m3'] = {}
params['A2016']['OF']['10m3']['bM']  = [-1.371,0.996,-0.083]
params['A2016']['OF']['10m3']['k12'] = [-1.444,0.995,-0.070]
params['A2016']['OF']['10m3']['k8']  = [-1.484,0.994,-0.061]
params['A2016']['OF']['10m3']['k6']  = [-1.525,0.993,-0.052]
params['A2016']['OF']['10m3']['k4']  = [-1.613,0.990,-0.026]
params['A2016']['OF']['10m2'] = {}
params['A2016']['OF']['10m2']['bM']  = [-1.371,0.980,-0.049]
params['A2016']['OF']['10m2']['k12'] = [-1.440,0.979,-0.034]
params['A2016']['OF']['10m2']['k8']  = [-1.477,0.978,-0.024]
params['A2016']['OF']['10m2']['k6']  = [-1.514,0.976,-0.012]
params['A2016']['OF']['10m2']['k4']  = [-1.594,0.973, 0.017]

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

#see Lazar et al. 2015
@njit
def th_func_2(bparl,a,b,c,d):
    return 1. + (a/bparl**b)*(1.+c/bparl**d)

#-------------------------------------------------------
#plot function
#-------------
def windlike_plot_func( ax, xdata, ydata, species_vars,
                        xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                        legend_flag=False, params=params, run_label=run_label ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    params       -> [optional, set by default] dict -
                    dict containing all thresholds parameters
    run_label    -> [optional, set by default] string - run label

    OUTPUTS:
    handles      -> [lines.Line2D, ...] - list of handles for the legend
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    h,xbin,ybin=np.histogram2d(np.log(xdata).flatten(),np.log(ydata).flatten(),n_bins)
    X,Y = np.meshgrid(np.exp(xbin),np.exp(ybin))
    im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())
    del X,Y

    if xdata_range == None:
        tmp = np.exp(xbin)
        xdata_range = [np.min(tmp),np.max(tmp)]
        del tmp
    if ydata_range == None:
        tmp = np.exp(ybin)
        ydata_range = [np.min(tmp),np.max(tmp)]
        del tmp
    xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),100)

    if species_tag == 'p':
        p = params['H2006']['PC']['10m3']['bM'] # ProtonCyclotron - bi-Maxwellian
        lC, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label="PCI")

    p = params['H2006']['MR']['10m3']['bM'] # Mirror - bi-Maxwellian
    lM, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label="MI")

    if species_tag == 'e':
        p = params['L2015']['EC']['e05']['10m2']['bM']['T'] # ElectronCyclotron - bi-Maxwellian
        lC, = ax.plot(xvals,th_func_2(xvals,p[0],p[1],p[2],p[3]),'-k',label="ECI")

    p = params['A2016']['PF']['10m2']['bM'] # ParallelFirehose - bi-Maxwellian
    lP, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label="PFHI")

    p = params['A2016']['OF']['10m2']['bM'] # ObliqueFirehose - bi-Maxwellian
    lO, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label="OFHI")

    p = params['A2016']['OF']['10m2']['k4'] # ObliqueFirehose - bi-kappa
    lOk, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label="OFHI $\kappa=4$")

    lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',label='$\\beta_{\parallel}^{-1}$') # CGL expansion
    
    ax.set_xlabel('$\\beta_{\parallel,%s}$'%(species_tag))
    ax.set_ylabel('$p_{\perp,%s}/p_{\parallel,%s}$'%(species_tag,species_tag))
    ax.set_xlim(xdata_range[0],xdata_range[1])
    ax.set_ylim(ydata_range[0],ydata_range[1])
    if legend_flag:
        ax.legend()
    title_str = run_label+' - '+species_name
    if t != None:
        title_str += ' - $t = %s$'%(t)
    ax.set_title(title_str)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.colorbar(im,ax=ax)

    return [lC,lM,lP,lO,lOk,lb]

def outofth_plot_func( ax, xdata, ydata, Psi, species_vars,
                       ix_range=None, iy_range=None, t=None, x=x, y=y,
                       legend_flag=False, params=params, run_label=run_label ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray(nx,ny) - first variable
    ydata        -> numpy.ndarray(nx,ny) - second variable
    Psi          -> numpy.ndarray(nx,ny) - Psi field
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    ix_range     -> [optional] [int,int] - index range for x axis
    iy_range     -> [optional] [int,int] - index range for y axis
    t            -> [optional] float - if set, the time to be shown in the title
    x            -> [optional, set by default] numpy.ndarray(nx) -
                    x axis values
    y            -> [optional, set by default] numpy.ndarray(ny) -
                    y axis values
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    params       -> [optional, set by default] dict -
                    dict containing all thresholds parameters
    run_label    -> [optional, set by default] string - run label

    OUTPUTS:
    None (a plot will be generated in axis ax)
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    if ix_range == None:
        ix_range = [0L,len(x)-1L]
    if iy_range == None:
        iy_range = [0L,len(y)-1L]

    if species_tag == 'p':
        p_C = params['H2006']['PC']['10m3']['bM'] # ProtonCyclotron - bi-Maxwellian
        col_C = 'blue'
        label_C = 'PC'
        p_C[-1] *= b0_mult

    if species_tag == 'e':
        p_C = params['L2015']['EC']['e05']['10m2']['bM']['T'] # ElectronCyclotron - bi-Maxwellian
        col_C = 'black'
        label_C = 'EC'
    p_C[-1]

    p_M = params['H2006']['MR']['10m3']['bM'] # Mirror - bi-Maxwellian
    col_M = 'green'
    label_M = 'MR'
    p_M[-1] *= b0_mult

    p_O = params['A2016']['OF']['10m2']['bM'] # ObliqueFirehose - bi-Maxwellian
    col_O = 'yellow'
    label_O = 'OFH'
    p_O[-1] *= b0_mult

    th_M = th_func(xdata,*p_M)
    if species_tag == 'p':
        th_C = th_func(xdata,*p_C)
    elif species_tag == 'e':
        th_C = th_func_2(xdata,*p_C)
    mask_Cfirst = th_C < th_M

    masks = {}
    masks['O'] = ydata < th_func(xdata,*p_O)
    masks['C'] = np.logical_and(ydata > th_C,mask_Cfirst)
    masks['M'] = np.logical_and(ydata > th_M,np.logical_not(mask_Cfirst))

    del mask_Cfirst,th_C,th_M
    
    ax.contour( x[ix_range[0]:ix_range[1]],y[iy_range[0]:iy_range[1]],
                Psi[ix_range[0]:ix_range[1],iy_range[0]:iy_range[1]].T,
                8,colors='gray')
    ax.contourf(x[ix_range[0]:ix_range[1]],y[iy_range[0]:iy_range[1]],
                masks['O'][ix_range[0]:ix_range[1],iy_range[0]:iy_range[1]].T,
                1,colors=['#00000000',col_O])
    ax.contourf(x[ix_range[0]:ix_range[1]],y[iy_range[0]:iy_range[1]],
                masks['C'][ix_range[0]:ix_range[1],iy_range[0]:iy_range[1]].T,
                1,colors=['#00000000',col_C])
    ax.contourf(x[ix_range[0]:ix_range[1]],y[iy_range[0]:iy_range[1]],
                masks['M'][ix_range[0]:ix_range[1],iy_range[0]:iy_range[1]].T,
                1,colors=['#00000000',col_M])
    if legend_flag:
        ax.plot([],[],color=col_O,label=label_O)
        ax.plot([],[],color=col_C,label=label_C)
        ax.plot([],[],color=col_M,label=label_M)
        ax.legend()

    ax.set_xlabel('$x \quad [d_i]$')
    ax.set_ylabel('$y \quad [d_i]$')
    ax.set_xlim(x[ix_range[0]],x[ix_range[1]])
    ax.set_ylim(y[iy_range[0]],y[iy_range[1]])
    title_str = run_label+' - '+species_name
    if t != None:
        title_str += ' - $t = %s$'%(t)
    ax.set_title(title_str)

    return

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n,_ = run.get_Ion(ind)
            pparle *= n
            pperpe *= n
            del n
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    del B
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')
        del pparle
    del B2,pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    b_cut = np.concatenate((bparlp[:,:cut1,0],bparlp[:,cut2+1:,0]),axis=1)
    a_cut = np.concatenate(( anisp[:,:cut1,0], anisp[:,cut2+1:,0]),axis=1)
    handles_p = windlike_plot_func(ax[0],b_cut,a_cut,p_vars,
                                   xdata_range=betap_lim,ydata_range=anisp_lim)
#
    if w_ele:
        b_cut = np.concatenate((bparle[:,:cut1,0],bparle[:,cut2+1:,0]),axis=1)
        a_cut = np.concatenate(( anise[:,:cut1,0], anise[:,cut2+1:,0]),axis=1)
        handles_e = windlike_plot_func(ax[1],b_cut,a_cut,e_vars,
                                       xdata_range=betae_lim,ydata_range=anise_lim)
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4],
                   handles_p[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/'+'anis_windlike_'+run_name+'_'+str(ind)+'.png')
    plt.close()
    del b_cut,a_cut
#
    #------------------------------------------------------
    #plot out of threshold
    #---------------------
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    outofth_plot_func(ax[0],bparlp[...,0],anisp[...,0],Psi,
                      p_vars,ix_range=[ixmin,ixmax],iy_range=[iymin,iymax])
#
    if w_ele:
        outofth_plot_func(ax[1],bparle[...,0],anise[...,0],Psi,
                          e_vars,ix_range=[ixmin,ixmax],iy_range=[iymin,iymax])
    else:
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],# lC
                   handles_p[1],# lM
                   handles_p[3]]# lO
        leg = ax[1].legend(handles=handles,loc='center',borderaxespad=0)
        for h in leg.legendHandles:
            h.set_linewidth(10.)
#
    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/'+'out_of_threshold_'+run_name+'_'+str(ind)+'.png')
    plt.close()
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
