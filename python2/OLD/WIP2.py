import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

ipath = '/work1/califano/HVM2D/LF_2d_DH'
run = from_HVM(ipath)
run.get_meta()


ind = 295.
_,B = run.get_EB(ind)

#
#import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
#import seaborn as sns

# currently (03/2019) scaleogram needs install via https://github.com/alsauve/scaleogram
# because an old PyWavelets (0.5.2) is installend and PyWavelets > 1.0 is needed
import scaleogram as scg 
import pywt

# choose default wavelet function for the entire notebook
scg.set_default_wavelet('mexh')

# Try these ones to see various compromises between scales and time resolution 
#scg.set_default_wavelet('cmor1-1.5')
#scg.set_default_wavelet('cgau5')
#scg.set_default_wavelet('cgau1')
#scg.set_default_wavelet('shan0.5-2')
#scg.set_default_wavelet('mexh')

x = run.meta['x'] #instead of datetime
y = run.meta['y'] 
nx = run.meta['nx']
dx = run.meta['dx']
y0_ind = 85
Bz_cut = B[2,:,y0_ind,0] #instead of births
Bz_cut_normed = np.roll(Bz_cut,-300) - np.mean(Bz_cut)

x_lim = [np.min(x),np.max(x)]

#

fig = plt.figure(figsize=(12,2))
lines = plt.plot(x, Bz_cut_normed, '-')
plt.xlim(x_lim)

plt.ylabel('Bz[:,'+str(y0_ind)+']'); plt.title('Bz cut at y = '+str(y[y0_ind]));

#scales = scg.periods2scales(np.arange(1, 1024//2, 1))
#ax = scg.cws(Bz_cut_normed, scales=scales, figsize=(13.2, 2), xlabel='x', ylabel='wavelength', clim=(0.,2.), yscale='log')

scales = scg.periods2scales(np.logspace(np.log10(2), np.log10(nx//2)),200)
cwt = scg.CWT(Bz_cut_normed, scales=scales)
ax  = scg.cws(cwt, figsize=(13.2, 4), xlabel='x', ylabel='wavelength', yscale='log')#, clim=(0.,3.))

#plt.show()

#

fig, ax4 = plt.subplots(1,1, sharex = True, figsize = (13.2,4))

scales = scg.periods2scales(np.arange(1, 1024//2, 1))
f = pywt.scale2frequency('morl', scales)/dx
cwtmatr, freqs = pywt.cwt(Bz_cut_normed, scales, 'morl', sampling_period = dx)
im2 = ax4.pcolormesh(x, freqs, cwtmatr, vmin=0, cmap = "inferno" )
#ax4.set_ylim(0,dx*nz//2)
plt.colorbar(im2,ax=ax4)
ax4.set_ylabel("???")
ax4.set_xlabel("x")
ax4.set_yscale('log')
ax4.set_title("Scaleogram using wavelet GAUS1")

plt.show()

#
from scipy.signal import spectrogram

fig, ax3 = plt.subplots(1,1, sharex = True, figsize = (13.2,4))

data = Bz_cut_normed.copy() 
f_s = 1./dx
Dx = 256*dx # window length in [di]
Nw = np.int(2**np.round(np.log2(Dx * f_s))) # Number of datapoints within window
f, t_, Sxx = spectrogram(data, f_s, window='hanning', nperseg=Nw, noverlap = None, detrend=False, scaling='spectrum')
Df  =  f[1] - f[0]
Dt_ = t_[1] - t_[0]
t2  = t_ + t[0] - Dt_

im = ax3.pcolormesh(t2, f + Df/2, np.sqrt(2*Sxx), cmap = "inferno_r")#, alpha = 0.5)
ax3.grid(True)
ax3.set_ylabel("Frequency in [Hz]")
ax3.set_ylim(0, 10)
ax3.set_xlim(np.min(t2),np.max(t2))
ax3.set_title("Spectrogram using FFT and Hanning window")
ax3.set_yscale('log')

plt.show()

#

fs = 100.
f, x_, Sxx = spectrogram(data, fs)

plt.close()
fig,ax = plt.subplots(2,1,figsize=(12,4))
ax[0].plot(x,data)

ax[1].pcolormesh(x_,f,Sxx)

plt.show()
plt.close()


#

fs = nx//100#10e3
N = nx#1e5
amp = 2 * np.sqrt(2)
noise_power = 0.01 * fs / 2
time = np.arange(N) / float(fs)
mod = 50*np.cos(2*np.pi*0.25*time)
carrier = amp * np.sin(2*np.pi*3e2*time + mod)
noise = np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
noise *= np.exp(-time/5)
x = carrier + noise

f, t, Sxx = spectrogram(x, fs)
plt.pcolormesh(t, f, Sxx)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()

#

scales = np.linspace(2,nx//2,128)
data_mean = np.zeros((nx),dtype=np.float64)
coefs_mean = np.zeros((scales.shape[0],nx),dtype=np.cdouble)
cnt = 0
for y0 in range(170):
    data = B[2,:,y0,0]
    data -= np.mean(data)
    data = np.roll(data,-300)
    data_mean += data
#
    coefs,freq = pywt.cwt(data,scales,'cmor25.-0.1',sampling_period=int(1./dx))
    coefs_mean += coefs
    cnt += 1


data_mean = np.divide(data_mean,float(cnt))
coefs_mean = np.divide(coefs_mean,float(cnt))

plt.close()
fig,ax = plt.subplots(2,1,figsize=(12,8))
ax[0].plot(x,data_mean)
im = ax[1].contourf(x,scales*dx,np.real(coefs_mean)**2+np.imag(coefs_mean)**2,64)
#plt.colorbar(im,ax=ax[1])
plt.show()

