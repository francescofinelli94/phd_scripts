import matplotlib.pyplot as plt
import sys
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
from HVM_loader import *

ipath = '/work1/califano/HVM2D/LF_2d_DH'#'/work1/califano/HVM2D/HVM_large'#
run = from_HVM(ipath)
run.get_meta('01')

t_ = 200.#247.#
_,B = run.get_EB(t_)
Psi = wl.psi_2d(B[:-1,...,0],run.meta)

#------------------

from scipy.ndimage import gaussian_filter as gf
import numpy as np

def my_FFT_smooth(field,keep_percentage):
    nx,ny = field.shape
#
    mx_cut = int(round(float(nx//2+1)*keep_percentage[0]))
    my_cut = int(round(float(ny//2+1)*keep_percentage[1]))
#
    f = np.fft.rfft2(field)
    f[mx_cut:nx//2+1,:] = 0.
    if nx%2 == 0:
        f[nx//2+1:nx//2+1+nx//2-mx_cut,:] = 0.
    else:
        f[nx//2+1:nx//2+1+nx//2-mx_cut+1,:] = 0.
    f[:,my_cut:] = 0.
    field_ = np.fft.irfft2(f)
#
    return field_

sPsi = gf(Psi,[2,2],mode='wrap')

#---------------------

import h2o
from h2o.estimators import H2OIsolationForestEstimator
from sklearn.cluster import DBSCAN
import pandas as pd

import shift_and_flood as saf

def my_clust(mask,radius,min_num=1):
    h2o.init()
#
    xx,yy = np.where(mask)
    d = {'x': xx, 'y': yy}
    coords_pd = pd.DataFrame(data=d)
    coords = h2o.H2OFrame(coords_pd)
    coord_array = coords.as_data_frame().as_matrix()
#
    clustering = DBSCAN(eps=radius, min_samples=min_num).fit(coord_array)
    clustering.labels_
#
    to_pandas = pd.DataFrame(clustering.labels_)
    clusterized = h2o.H2OFrame(to_pandas, column_names=['clusterization'])
    coords = coords.cbind(clusterized)
#
    mat = np.zeros((nx,ny),dtype=np.int)
    for i in range(0,len(clustering.labels_)):
        mat[xx[i],yy[i]] = clustering.labels_[i] + 1
#
    h2o.cluster().shutdown()
#
    return mat

dx,dy = run.meta['ddd'][:2]

field = sPsi
periodic = True

#def my_singulars(field,dx,dy,periodic=True,p1=0.01,p2=0.002,r1=100,r2=25,test_plot=False):
nx,ny = field.shape
#
if periodic:
    gmode = 'wrap'
else:
    gmode = 'reflect'

field_ = np.pad(field,((2,2),(2,2)),mode=gmode)
#
#------
#
der_x,  der_y  = np.gradient(field_,dx,dy)
der_xx, der_xy = np.gradient(der_x, dx,dy)
_,      der_yy = np.gradient(der_y, dx,dy)
del field_
der_x  = der_x[2:-2,2:-2]
der_y  = der_y[2:-2,2:-2]
der_xx = der_xx[2:-2,2:-2]
der_xy = der_xy[2:-2,2:-2]
der_yy = der_yy[2:-2,2:-2]
#
#-----
#
der_n = np.sqrt(der_x**2 + der_y**2)
h,b=np.histogram(der_n,int(2.**(2.+np.log10(float(nx*ny)))))
A = np.sum(h)*(b[1]-b[0])
h = np.divide(h,A)
ch = np.empty((len(h)),dtype=np.float)
for i in range(len(h)):
    ch[i] = np.sum(h[:i+1])*(b[1]-b[0])

p1 = 0.01
cut_ind = max(np.sum(ch <= p1),1)
mask = der_n < b[cut_ind]
#
mask_sadd = np.full((nx,ny),False,dtype=np.bool)
mask_extr = np.full((nx,ny),False,dtype=np.bool)
eig0      = np.full((nx,ny),np.nan,dtype=np.float64)
eig_prod  = np.full((nx,ny),np.nan,dtype=np.float64)
for (ix,iy) in [(ix,iy) for ix in range(nx) for iy in range(ny)]:
    if not mask[ix,iy]:
        continue
    eig = np.linalg.eigvals([[der_xx[ix,iy],der_xy[ix,iy]],[der_xy[ix,iy],der_yy[ix,iy]]])
    eig_prod[ix,iy] = eig[0]*eig[1]
    eig0[ix,iy] = eig[0]
    if eig[0]*eig[1] < 0.:
        mask_sadd[ix,iy] = True
    else:
        mask_extr[ix,iy] = True


reg_sadd = my_clust(mask_sadd,10)
reg_extr = my_clust(mask_extr,10)
nreg_sadd = np.max(reg_sadd)
nreg_extr = np.max(reg_extr)

sx = []
sy = []
st = []
test_field = der_n*np.exp(eig_prod)
for i in range(nreg_sadd):
    tmp=np.ma.masked_where(reg_sadd!=(i+1),test_field)
    ix,iy = np.where(test_field==np.min(tmp))
    for i in range(len(ix)):
        sx.append(ix[i])
        sy.append(iy[i])
        st.append(2)


test_field = der_n*np.exp(-eig_prod)
for i in range(nreg_extr):
    tmp=np.ma.masked_where(reg_extr!=(i+1),test_field)
    ix,iy = np.where(test_field==np.min(tmp))
    for i in range(len(ix)):
        sx.append(ix[i])
        sy.append(iy[i])
        if eig0[ix[i],iy[i]] > 0.:
            st.append(0)
        else:
            st.append(1)

mask = np.zeros((nx,ny),dtype=np.bool)
for i in range(len(st)):
    mask[sx[i],sy[i]] = True

reg = my_clust(mask,10)
nreg = np.max(reg)

sx = []
sy = []
st = []
for i in range(nreg):
    ix,iy = np.where(reg==i+1)
    val = []
    tx = []
    ty = []
    tt = []
    for i in range(len(ix)):
        tx.append(ix[i])
        ty.append(iy[i])
        if eig_prod[ix[i],iy[i]] < 0.:
            val.append(der_n[ix[i],iy[i]])#*np.exp(eig_prod[ix[i],iy[i]]))
            tt.append(2)
        else:
            val.append(der_n[ix[i],iy[i]])#*np.exp(-eig_prod[ix[i],iy[i]]))
            if eig0[ix[i],iy[i]] > 0.:
                tt.append(0)
            else:
                tt.append(1)
    m = np.min(val)
    ii = np.where(val==m)
    for i in range(len(ii)):
        sx.append(tx[i])
        sy.append(ty[i])
        st.append(tt[i])




#return sx,sy,st


#-----------------

sx,sy,st = my_singulars(sPsi,dx,dy,p1=0.01,p2=0.1,r1=5,r2=5,test_plot=True)

plt.contour(Psi.T,64)
for i in range(len(st)):
    if st[i] == 0:
        plt.plot(sx[i],sy[i],'ok')
    elif st[i] == 1:
        plt.plot(sx[i],sy[i],'dk')
    else:
        plt.plot(sx[i],sy[i],'xk')


plt.show()
