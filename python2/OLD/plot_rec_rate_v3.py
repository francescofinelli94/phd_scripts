#-----------------------------------------------------
#importing stuff
#---------------
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
import gc
import glob
from scipy.optimize import curve_fit
from scipy.ndimage import gaussian_filter as gf
from scipy.signal import savgol_filter
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot reconnection rate/flux\n')

#------------------------------------------------------
#Init
#------
opath = '/home/finelli/Downloads'
print('opath is set to:')
print(opath)

#insert run names
#run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run7_data0']
#run_label_vec = ['HVM','HVLF','iPIC']
#choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run7_data0':[0,3]}

run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run8']
run_label_vec = ['HVM','HVLF','iPIC']
choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run8':[0,9]}

#run_name_vec = ['HVM_2d_DH','LF_2d_DH','DH_run9']
#run_label_vec = ['HVM','HVLF','iPIC']
#choosen_points = {'HVM_2d_DH':[0,0],'LF_2d_DH':[0,1],'DH_run9':[3,6]}

#run_name_vec = ['DH_run7_data0','DH_run8','DH_run9']
#run_label_vec = ['iPIC 7','iPIC 8','iPIC 9']
#choosen_points = {'DH_run7_data0':[0,3],'DH_run8':[0,9],'DH_run9':[3,6]}

iPIC_run = []
for run_name in run_name_vec:
    if run_name.split('_')[0] == 'DH':
        iPIC_run.append(run_name)
print('\nrun_name_vec is set to:')
print(run_name_vec)

#compute normalization #HARDCODED!!!!!!
all_B0 = {}
all_tmult = {}
for run_name in run_name_vec:
    all_B0[run_name] = [1.,0.,0.25]
    if run_name in iPIC_run:
        all_B0[run_name] = np.divide(all_B0[run_name],100.)
    all_tmult[run_name] = 1.
    if run_name in iPIC_run:
        all_tmult[run_name] = all_tmult[run_name]*0.01

print('\nNormalizations are:')
print('0 -> Bx*Bx/sqrt(4*pi*ni*mi)')
print('1 -> Bx*B/sqrt(4*pi*ni*mi)')
print('2 -> B*B/sqrt(4*pi*ni*mi)')
#inorm = ml.secure_input('choose normalization: ',10,True)
inorm = 0
print('norm -> 0 is selected\n')

#data struct init
time = {}
flux = {}
rate = {}
Xx = {}
Ox = {}

for run_name in run_name_vec:
    out_dir = 'Psi_field/'+run_name
    ipath = opath+'/'+'Psi_field/'+run_name+'/tracked_points'

    #read files
    xfiles = glob.glob(ipath+'/xpoint_from_*.dat')
    ofiles = glob.glob(ipath+'/opoint_from_*.dat')

    xnum = len(xfiles)
    onum = len(ofiles)

    t = {}
    x = {}
    y = {}
    v = {}
    for i in range(xnum):
        f = open(xfiles[i],'r')
        ll = f.readlines()
        f.close()

        nt = len(ll)
        t[xfiles[i]] = np.empty(nt,dtype=np.float);   t0 = t[xfiles[i]][0]
        x[xfiles[i]] = np.empty(nt,dtype=np.long);    x0 = x[xfiles[i]][0]
        y[xfiles[i]] = np.empty(nt,dtype=np.long);    y0 = y[xfiles[i]][0]
        v[xfiles[i]] = np.empty(nt,dtype=np.float64); v0 = v[xfiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t[xfiles[i]][j] = ml.generic_cast(l[0],t0)
            x[xfiles[i]][j] = ml.generic_cast(l[1],x0)
            y[xfiles[i]][j] = ml.generic_cast(l[2],y0)
            v[xfiles[i]][j] = ml.generic_cast(l[3],v0)

    for i in range(onum):
        f = open(ofiles[i],'r')
        ll = f.readlines()
        f.close()
        
        nt = len(ll)
        t[ofiles[i]] = np.empty(nt,dtype=np.float);   t0 = t[ofiles[i]][0]
        x[ofiles[i]] = np.empty(nt,dtype=np.long);    x0 = x[ofiles[i]][0]
        y[ofiles[i]] = np.empty(nt,dtype=np.long);    y0 = y[ofiles[i]][0]
        v[ofiles[i]] = np.empty(nt,dtype=np.float64); v0 = v[ofiles[i]][0]
        for j in range(nt):
            l = ll[j].split()
            t[ofiles[i]][j] = ml.generic_cast(l[0],t0)
            x[ofiles[i]][j] = ml.generic_cast(l[1],x0)
            y[ofiles[i]][j] = ml.generic_cast(l[2],y0)
            v[ofiles[i]][j] = ml.generic_cast(l[3],v0)

    if choosen_points[run_name] == None:
        #plot all
        plt.close()
        for i in range(xnum):
            plt.plot(x[xfiles[i]],t[xfiles[i]],label='X'+str(i))
        for i in range(onum):
            plt.plot(x[ofiles[i]],t[ofiles[i]],'--',label='O'+str(i))
        plt.legend()
        plt.xlabel('x');plt.ylabel('t');plt.title('Tracked singular points')
        plt.show()
        plt.close()

        #choose points
        ixp = ml.secure_input('Choose X-point (0 -> '+str(xnum-1)+'): ',10,True)
        iop = ml.secure_input('Choose O-point (0 -> '+str(onum-1)+'): ',10,True)
        #if ( (len(t[xfiles[ixp]]) != len(t[ofiles[iop]])) or 
        #     (t[xfiles[ixp]][0] != t[ofiles[iop]][0]) or (t[xfiles[ixp]][-1] != t[ofiles[iop]][-1]) ):
        #    print('ERROR: X-point and O-point are tracked for different time ranges')
        #    sys.exit()
    else:
        ixp = choosen_points[run_name][0]
        iop = choosen_points[run_name][1]

    #store data
    Xx[run_name] = x[xfiles[ixp]]
    Ox[run_name] = x[ofiles[iop]]
    ERR_FLG = False
    if len(t[xfiles[ixp]]) != len(t[ofiles[iop]]):
        ERR_FLG = True
    if np.max(np.abs(t[xfiles[ixp]]-t[ofiles[iop]])) > 1.e-15:
        ERR_FLG = True
    if ERR_FLG:
        print('ERROR: X-point and O-point time series for %s are not equal!'%(run_name))
        sys.exit()
    time[run_name] = t[xfiles[ixp]]
    if inorm < 2:
        flux[run_name] = np.divide(v[ofiles[iop]] - v[xfiles[ixp]],all_B0[run_name][0])
    else:
        flux[run_name] = np.divide(v[ofiles[iop]] - v[xfiles[ixp]],np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2))
    flux[run_name] = flux[run_name] - flux[run_name][0]
    #if inorm == 0:
    #    rate[run_name] = np.divide(np.gradient(flux[run_name],time[run_name]),all_B0[run_name][0]/all_tmult[run_name])
    #else:
    #    rate[run_name] = np.divide(np.gradient(flux[run_name],time[run_name]),np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2)/all_tmult[run_name])
    tmp = savgol_filter(flux[run_name],window_length=15,polyorder=2,deriv=1,
                        delta=time[run_name][1]-time[run_name][0],mode='nearest')
    if inorm == 0:
        rate[run_name] = np.divide(tmp,all_B0[run_name][0]/all_tmult[run_name])
    else:
        rate[run_name] = np.divide(tmp,np.sqrt(all_B0[run_name][0]**2+all_B0[run_name][1]**2+all_B0[run_name][2]**2)/all_tmult[run_name])

#plot selected
plt.close()
for run_name,run_label in zip(run_name_vec,run_label_vec):
    plt.plot(Xx[run_name],time[run_name],label='X %s'%(run_label))
    plt.plot(Ox[run_name],time[run_name],'--',label='O %s'%(run_label))
plt.legend()
plt.xlabel('x');plt.ylabel('t');plt.title('Tracked singular points')
plt.show()
plt.close()

#final plot
for run_name in run_name_vec:
    flux[run_name] = gf(flux[run_name],2,mode='nearest')
#   rate[run_name] = gf(rate[run_name],2,mode='nearest')

#def to_fit(x,F0,g,C):
#    return F0*(np.exp(g*(x-C))-1.)

#flux_th0 = flux[run_name_vec[1]][40]#7.e-3#
#flux_th1 = flux[run_name_vec[1]][65]#3.e-2#
#it0 = {}
#it1 = {}
#for run_name in run_name_vec:
#    it0[run_name] = ml.index_of_closest(flux_th0,flux[run_name])
#    it1[run_name] = ml.index_of_closest(flux_th1,flux[run_name])

#plt.close()
#fig,ax = plt.subplots(1,2,figsize=(16,8))
#for run_name,run_label in zip(run_name_vec,run_label_vec):
#    i0 = it0[run_name]
#    i1 = it1[run_name]
#    t_ = time[run_name][i0]*0.
#    this_time = time[run_name]-t_
#    p = np.polyfit(time[run_name][i0:i1],np.log(flux[run_name][i0:i1]),1)
#    popt,_ = curve_fit(to_fit,time[run_name][0:i1],flux[run_name][0:i1],
#                       p0=[np.abs(flux[run_name][0]),p[0],0.])
#    ax[0].plot(this_time,flux[run_name],
#            label=run_label+' ($t_0='+str(round(t_,1))+'$ $\gamma='+str(round(popt[1],4))+'$)')
#    ax[0].plot(this_time[i0:i1],to_fit(time[run_name][i0:i1],*popt),'--k')
#    ax[1].plot(this_time,rate[run_name])
#ax[0].set_xlabel('$\Delta t=t-t_0\quad [\Omega_C^{-1}]$')
#ax[0].set_ylabel('$F/B_0\quad [d_i]$')
#ax[0].set_title('Reconnected fluxes')
#ax[0].legend()
#ax[0].set_yscale('log')
#ax[1].set_xlabel('$\Delta t=t-t_0\quad [\Omega_C^{-1}]$')
#ax[1].set_ylabel('$R/B_0/v_A$')
#ax[1].set_title('Reconnection rates')
#ax[1].set_yscale('log')
#fig.tight_layout()
#plt.show()
##plt.savefig(opath+'/'+'Psi_field/rec_flux_and_rate.png')
#plt.close()

plt.close()
fig,ax = plt.subplots(1,2,figsize=(16,8))
for run_name,run_label in zip(run_name_vec,run_label_vec):
    this_time = time[run_name]
    ax[0].plot(this_time,flux[run_name],
            label=run_label)
    ax[1].plot(this_time,rate[run_name])
ax[0].set_xlabel('$t [\Omega_C^{-1}]$')
ax[0].set_ylabel('$F/B_0\quad [d_i]$')
ax[0].set_title('Reconnected fluxes')
ax[0].legend()
ax[0].set_yscale('log')
ax[1].set_xlabel('$t [\Omega_C^{-1}]$')
ax[1].set_ylabel('$R/B_0/v_A$')
ax[1].set_title('Reconnection rates')
ax[1].set_yscale('log')
fig.tight_layout()
plt.show()
#plt.savefig(opath+'/'+'Psi_field/rec_flux_and_rate.png')
plt.close()
