#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
import thresholds as th

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta for one time.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'windlike_plot_TEST'
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#-------------------------------------------------------
#plot function
#-------------
def windlike_plot_func( ax, xdata, ydata, species_vars,
                        xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                        legend_flag=False, run_label=run_label ): # CH
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    run_label    -> [optional, set by default] string - run label

    OUTPUTS:
    handles      -> [lines.Line2D, ...] - list of handles for the legend
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    h,xbin,ybin=np.histogram2d(np.log(xdata).flatten(),np.log(ydata).flatten(),n_bins)
    X,Y = np.meshgrid(np.exp(xbin),np.exp(ybin))
    im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())
    del X,Y

    if xdata_range == None:
        tmp = np.exp(xbin)
        xdata_range = [np.min(tmp),np.max(tmp)]
        del tmp
    if ydata_range == None:
        tmp = np.exp(ybin)
        ydata_range = [np.min(tmp),np.max(tmp)]
        del tmp
    xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),100)

    if species_tag == 'p':
        p,f = th.params['H2006']['pC']['1.0e-3Op']['bM'] # CH
        lC, = ax.plot(xvals,th.func[f](xvals,p[0],p[1],p[2]*b0_mult),'-b',label="PCI") # CH

    p,f = th.params['H2006']['pM']['1.0e-3Op']['bM'] # CH
    lM, = ax.plot(xvals,th.func[f](xvals,p[0],p[1],p[2]*b0_mult),'-g',label="MI") # CH

    if species_tag == 'e':
        p,f = th.params['L2015']['eC']['e05']['1.0e-2Oe']['bM']['T'] # CH
        lC, = ax.plot(xvals,th.func[f](xvals,p[0],p[1],p[2],p[3]),':y',label="ECI") # CH

    p,f = th.params['A2016']['pPF']['1.0e-2Op']['bM']  # CH
    lP, = ax.plot(xvals,th.func[f](xvals,p[0],p[1],p[2]*b0_mult),'-m',label="PFHI") # CH

    p,f = th.params['A2016']['pOF']['1.0e-2Op']['bM']  # CH
    lO, = ax.plot(xvals,th.func[f](xvals,p[0],p[1],p[2]*b0_mult),'-y',label="OFHI") # CH

#####

    if species_tag == 'p':
        p,f = th.params['L2014']['pC']['1.0e-2Op']['bM']
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--b',label="PCI")

        p,f = th.params['M2012']['pM']['1.0e-2Op']['bM'] 
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--g',label="MI")
        
        p,f = th.params['A2016']['pPF']['1.0e-2Op']['bM']  
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--m',label="PFHI")

        p,f = th.params['A2016']['pOF']['1.0e-2Op']['bM']  
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--y',label="OFHI")

    if species_tag == 'e':
        p,f = th.params['L2015']['eC']['e05']['1.0e-3Oe']['bM']['T']
        _, = ax.plot(xvals,th.func[f](xvals,p[0],p[1],p[2],p[3]),'-.y',label="ECI")

        p,f = th.params['G1996']['eC']['1.0e-1Oe']['bM']
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--b',label="ECI")

        p,f = th.params['G1996']['eC']['1.0e-2Oe']['bM']
        _, = ax.plot(xvals,th.func[f](xvals,*p),':b',label="ECI")

        for k,s in zip(['1.0e-1Oe','1.0e-2Oe','1.0e-3Oe'],['--',':','-.']):
            p,f = th.params['L2013']['eC'][k]['bM']
            _, = ax.plot(xvals,th.func[f](xvals,*p),s+'m',label="ECI")
                
            p,f = th.params['G2006']['eC'][k]['bM']
            _, = ax.plot(xvals,th.func[f](xvals,*p),s+'k',label="ECI")
        
            p,f = th.params['G2006']['eM'][k]['bM']
            _, = ax.plot(xvals,th.func[f](xvals,*p),s+'g',label="MI")

        p,f = th.params['G2003']['ePF']['1.0e-2Op']['bM']
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--m',label="PFHI")

        p,f = th.params['G2003']['eOF']['1.0e-2Oe']['bM']
        _, = ax.plot(xvals,th.func[f](xvals,*p),'--y',label="OFHI")

        p,f = th.params['H2014']['eOF']['1.0e-3Oe']['bM']
        _, = ax.plot(xvals,th.func[f](xvals,*p),':y',label="OFHI")
        
#####

    lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',label='$\\beta_{\parallel}^{-1}$')
    
    ax.set_xlabel('$\\beta_{\parallel,%s}$'%(species_tag))
    ax.set_ylabel('$p_{\perp,%s}/p_{\parallel,%s}$'%(species_tag,species_tag))
    ax.set_xlim(xdata_range[0],xdata_range[1])
    ax.set_ylim(ydata_range[0],ydata_range[1])
    if legend_flag:
        ax.legend()
    title_str = run_label+' - '+species_name
    if t != None:
        title_str += ' - $t = %s$'%(t)
    ax.set_title(title_str)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.colorbar(im,ax=ax)

    return [lC,lM,lP,lO,lb] # CH

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n,_ = run.get_Ion(ind)
            pparle *= n
            pperpe *= n
            del n
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    del B
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')
        del pparle
    del B2,pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    b_cut = np.concatenate((bparlp[:,:cut1,0],bparlp[:,cut2+1:,0]),axis=1)
    a_cut = np.concatenate(( anisp[:,:cut1,0], anisp[:,cut2+1:,0]),axis=1)
    handles_p = windlike_plot_func(ax[0],b_cut,a_cut,p_vars,
                                   xdata_range=betap_lim,ydata_range=anisp_lim)
#
    if w_ele:
        b_cut = np.concatenate((bparle[:,:cut1,0],bparle[:,cut2+1:,0]),axis=1)
        a_cut = np.concatenate(( anise[:,:cut1,0], anise[:,cut2+1:,0]),axis=1)
        handles_e = windlike_plot_func(ax[1],b_cut,a_cut,e_vars,
                                       xdata_range=betae_lim,ydata_range=anise_lim)
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4]] # CH 
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    plt.show()
    #plt.savefig(opath+'/'+out_dir+'/'+'anis_windlike_'+run_name+'_'+str(ind)+'.png')
    plt.close()
    del b_cut,a_cut
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
