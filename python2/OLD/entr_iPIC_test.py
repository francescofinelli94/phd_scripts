#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill check enropies\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init()

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

w_ele = run.meta['w_ele']
teti = run.meta['teti']
mime = run.meta['mime']
tmult = run.meta['tmult']
opath = run.meta['opath']
run_name = run.meta['run_name']
x = run.meta['x']
y = run.meta['y']

qom_e = run.meta['msQOM'][0]

wl.Can_I()

out_dir = 'entropy'

g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

#---> loop over times <---
print " ",
t_vec = []
#iSkp_vec = []
#idSkp_vec = []
iSfp_vec = []
iSfe_vec = []
iSparlp_vec = []
iSperpp_vec = []
iSgp_vec = []
if w_ele:
    iSparle_vec = []
    iSperpe_vec = []
    iSge_vec = []

vol = run.meta['nx']*run.meta['ny']*run.meta['nz']

for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data
    #------------
    n_p,tmp = run.get_Ion(ind)
    del tmp
    n_e,tmp = run.get_Ion(ind,qom=qom_e)
    del tmp

    tmp,B = run.get_EB(ind)
    del tmp

    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3.
    del Pp
    Pe = run.get_Press(ind,qom=qom_e)
    pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
    Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.
    del Pe

    Bn = np.sqrt(B[0]**2+B[1]**2+B[2]**2)
    del B

    #--------------------------------------------------------
    #computing stuff
    #---------------

#    Skp,dSkp = run.get_Entropy(ind)
#    iSkp = np.sum(Skp)
#    idSkp = np.sum(dSkp)

    Sfp = (3./2.)*np.log(Pisop/(n_p**g_adiab))
    Sfe = (3./2.)*np.log(Pisoe/(n_e**g_adiab))
    iSfp = np.sum(Sfp)
    iSfe = np.sum(Sfe)

    Sparlp = 0.5*np.log(pparlp*(Bn**2.)/(n_p**g_parl))
    Sperpp = 1.*np.log(pperpp/(n_p*Bn))
    Sgp = (3./2.)*np.log((pparlp**(1./3.))*(pperpp**(2./3.))/(n_p**g_adiab))
    iSparlp = np.sum(Sparlp)
    iSperpp = np.sum(Sparlp)
    iSgp = np.sum(Sgp)
    if w_ele:
        Sparle = 0.5*np.log(pparle*(Bn**2.)/(n_e**g_parl))
        Sperpe = 1.*np.log(pperpe/(n_e*Bn))
        Sge = (3./2.)*np.log((pparle**(1./3.))*(pperpe**(2./3.))/(n_e**g_adiab))
        iSparle = np.sum(Sparle)
        iSperpe = np.sum(Sparle)
        iSge = np.sum(Sge)

    #--------------------------------------------------------
    #saving stuff
    #---------------
    t_vec.append(run.meta['time'][ind]*run.meta['tmult'])
#    iSkp_vec.append(iSkp)
#    idSkp_vec.append(idSkp)
    iSfp_vec.append(iSfp)
    iSfe_vec.append(iSfe)
    iSparlp_vec.append(iSparlp)
    iSperpp_vec.append(iSperpp)
    iSgp_vec.append(iSgp)
    if w_ele:
        iSparle_vec.append(iSparle)
        iSperpe_vec.append(iSperpe)
        iSge_vec.append(iSge)

    #--------------------------------------------------------
    #2d plots
    #--------
    ml.create_path(opath+'/'+out_dir)
    plt.close('all')

#   fig,ax = plt.subplots(1,1,figsize=(6*1,4*1))

#   im = ax.contourf(x,y[:170],Skp[:,:170,0].T,32)
#   plt.colorbar(im,ax=ax)
#   ax.set_xlabel('x'),ax.set_ylabel('y'),ax.set_title('Skp'+' t='+str(run.meta['time'][ind]*run.meta['tmult']))

#   plt.savefig(opath+'/'+out_dir+'/'+'entropy_Skp_'+run_name+'_'+str(ind)+'.png')
#   plt.close()

    #--------

#    fig,ax = plt.subplots(1,1,figsize=(6*1,4*1))
#
#    im = ax.contourf(x,y[:170],dSkp[:,:170,0].T,32)
#    plt.colorbar(im,ax=ax)
#    ax.set_xlabel('x'),ax.set_ylabel('y'),ax.set_title('dSkp'+' t='+str(run.meta['time'][ind]*run.meta['tmult']))
#
#    plt.savefig(opath+'/'+out_dir+'/'+'entropy_dSkp_'+run_name+'_'+str(ind)+'.png')
#    plt.close()

    #--------

    fig,ax = plt.subplots(1,1,figsize=(6*1,4*1))

    im = ax.contourf(x,y[:170],Sfp[:,:170,0].T,32)
    plt.colorbar(im,ax=ax)
    ax.set_xlabel('x'),ax.set_ylabel('y'),ax.set_title('Sfp'+' t='+str(run.meta['time'][ind]*run.meta['tmult']))

    plt.savefig(opath+'/'+out_dir+'/'+'entropy_Sfp_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #--------

    fig,ax = plt.subplots(1,1,figsize=(6*1,4*1))

    im = ax.contourf(x,y[:170],Sfe[:,:170,0].T,32)
    plt.colorbar(im,ax=ax)
    ax.set_xlabel('x'),ax.set_ylabel('y'),ax.set_title('Sfe'+' t='+str(run.meta['time'][ind]*run.meta['tmult']))

    plt.savefig(opath+'/'+out_dir+'/'+'entropy_Sfe_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #--------

    fig,ax = plt.subplots(1,1,figsize=(6*1,4*1))

    im = ax.contourf(x,y[:170],Sgp[:,:170,0].T,32)
    plt.colorbar(im,ax=ax)
    ax.set_xlabel('x'),ax.set_ylabel('y'),ax.set_title('Sgp'+' t='+str(run.meta['time'][ind]*run.meta['tmult']))

    plt.savefig(opath+'/'+out_dir+'/'+'entropy_Sgp_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #--------

    if w_ele:
        fig,ax = plt.subplots(1,1,figsize=(6*1,4*1))

        im = ax.contourf(x,y[:170],Sge[:,:170,0].T,32)
        plt.colorbar(im,ax=ax)
        ax.set_xlabel('x'),ax.set_ylabel('y'),ax.set_title('Sge'+' t='+str(run.meta['time'][ind]*run.meta['tmult']))

        plt.savefig(opath+'/'+out_dir+'/'+'entropy_Sge_'+run_name+'_'+str(ind)+'.png')
        plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
#iSkp_vec = np.array(iSkp_vec)/vol
#idSkp_vec = np.array(idSkp_vec)/vol
iSfp_vec = np.array(iSfp_vec)/vol
iSfe_vec = np.array(iSfe_vec)/vol
iSparlp_vec = np.array(iSparlp_vec)/vol
iSperpp_vec = np.array(iSperpp_vec)/vol
iSgp_vec = np.array(iSgp_vec)/vol
if w_ele:
    iSparle_vec = np.array(iSparle_vec)/vol
    iSperpe_vec = np.array(iSperpe_vec)/vol
    iSge_vec = np.array(iSge_vec)/vol

#--------------------------------------------------------
#line plot    
#---------
ml.create_path(opath+'/'+out_dir)
plt.close('all')

fig,ax = plt.subplots(1,1,figsize=(4*1,4*1))

ax.axhline(y=0,color='k')
ax.plot(t_vec,iSfp_vec-iSfp_vec[0],'b-',label='Sfp')
ax.plot(t_vec,iSfe_vec-iSfe_vec[0],'r-',label='Sfe')

ax.plot(t_vec,iSgp_vec-iSgp_vec[0],'b--',label='Sgp')
if w_ele:
    ax.plot(t_vec,iSge_vec-iSge_vec[0],'r--',label='Sge')

#ax.plot(t_vec,iSkp_vec-iSkp_vec[0],'b:',label='Skp')
#ax.plot(t_vec,idSkp_vec-idSkp_vec[0],'b-.',label='dSkp')
ax.set_xlabel('t');ax.set_ylabel('Delta S')
ax.legend()

plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'entropy_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
#plt.show()
plt.close()

