#-------------------------------------------------------
#imports
#----------------
import sys
import gc
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.signal as scisi

import work_lib as wl
import shift_and_flood as saf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#-------------------------------------------------------
#functions
#----------------
def mode_ampl_1d(field_,dt):
    field = field_ - np.mean(field_)
    N = len(field)
    xt      = np.fft.rfft(field)/float(N)  # fourier transform of x
    freq    = np.fft.rfftfreq(N,d=dt)      # frequencies involved
    # compute modes amplitude
    mode_ampl = np.abs(xt)
    mode_ampl[1:] *= 2.
    if N % 2 == 0 :    #correct the last term in case N is even
        mode_ampl[-1] /= 2.
    return freq,mode_ampl

#-------------------------------------------------------
#init
#----------------
run,tr,_=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'windowed_island_static'
opath = run.meta['opath']
code_name = run.meta['code_name']
nx,ny,_ = run.meta['nnn']
dx = run.meta['dx']
dy = run.meta['dy']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

#-------------------------------------------------------
#inputs
#----------------
#cuts
L1 = 0.85 #HARDCODED
L2 = 1.7 #HARDCODED
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
exclusion_width = 5. #HARDCODED
cut1 = (int((y2-exclusion_width)/run.meta['dy'])+ny)%ny
cut2 = int((y2+exclusion_width)/run.meta['dy'])%ny
y0 = int(y1/run.meta['dy'])
yr = int(min(abs(y1-y2),run.meta['yl']-abs(y1-y2))/2./run.meta['dy'])
perm_sh_y = y0 - yr

#number of points to fit for "instantaneous" growth rate
p2f = 10 #HARDCODED

#threshold parameter: threshold = min(field)*th_param ; min(field) < 0
th_param = ml.secure_input('Choose threshold parameter. Recommended: 0.98 : ',1.,True)

#window
print('Choose window dimensions. Recommended: 512x128 or 256x128.')
nwx = ml.secure_input('x dimension: ',10,True)
nwy = ml.secure_input('y dimension: ',10,True)

    #'boxcar', 'hamming', 'hann', 'blackman', 'flattop', 'nuttall', 'blackmanharris'
    # (+) <-  FREQ. RES.  <- (-)
    # (-) ->  DYN. RANGE  -> (+)
print('Suggested windows:')
print('boxcar, hamming, hann, blackman, flattop, nuttall, blackmanharris')
print(' (+) <-  FREQ. RES.  <- (-)')
print(' (-) ->  DYN. RANGE  -> (+)')
wname = ml.secure_input('chosen window: ','',True)

#time interval overwrite
print('TIME INTERVAL OVERWRITE!')
print('Max time range: '+str(np.min(time))+' -> '+str(np.max(time)))
t_start = ml.secure_input('insert starting time: ',1.,True)
t_stop  = ml.secure_input('insert stopping time: ',1.,True)
ind1 = ml.index_of_closest(t_start,time)
ind2 = ml.index_of_closest(t_stop,time)

#subpath
out_dir = ( out_dir + '_gr/' + run_name + '/' + wname + '_' +
            str(nwx) + '_' + str(nwy)+ '_' + str(ind1)+ '_' + str(ind2) )
ml.create_path(opath+'/'+out_dir)

#it's ok?
wl.Can_I()

#-------------------------------------------------------
#main loop
#----------------
island_width   = []
island_length  = []
island_surface = []
island_center  = []

MAx_0 = [] #full box
MAy_0 = []
MAx_1 = [] #primary sub-box
MAy_1 = []
MAx_2 = [] #secondary sub-box
MAy_2 = []

mx0 = []
my0 = []
mx1 = []
my1 = []
mx2 = []
my2 = []

t_vec = []

B_flag = False
B_sh = 0
#---> loop over time <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#------------------------
    #get the data
    _,B = run.get_EB(ind)
    B_sav = B.copy()
    B_sav = np.roll(B_sav,ny-1-cut2,axis=2)
    if ind == ind1:
        cut1 = (cut1 + ny-1-cut2)%ny
    if perm_sh_y < 0:
        B = np.roll(B,-perm_sh_y,axis=2)
        y0 = y0 - perm_sh_y
        B_sh = perm_sh_y
        perm_sh_y = 0
        B_flag = True
    if B_flag:
        B = np.roll(B,-B_sh,axis=2)
    Psi = wl.psi_2d(B[0:2,:,:,0],run.meta)

    #find the island
    reg,nreg = saf.my_flood(-Psi,-np.max(Psi)*th_param,periodicity=[True,True])

    v0 = 0
    cnt = 0
    for n in range(nreg):
        c = np.sum(reg == n+1)
        if c > cnt:
            cnt = c
            v0 = n + 1

    island_surface.append(cnt)

    reg = reg == v0
    for ix in range(nx):
        if np.sum(reg[ix,:]) > 0:
            x1 = ix
            break
    for ix in range(nx):
        if ix < x1:
            continue
        if np.sum(reg[ix,:]) == 0:
            x2 = ix - 1 
            break
    if x1 == 0:
        try:
            for ix in range(nx):
                if ix <= x2:
                    continue
                if np.sum(reg[ix,:]) > 0:
                    x1 = ix
                    break
        except:
            x2 = nx -1

    for iy in range(ny):
        if np.sum(reg[:,iy]) > 0:
            y1 = iy
            break
    for iy in range(ny):
        if iy < y1:
            continue
        if np.sum(reg[:,iy]) == 0:
            y2 = iy - 1
            break
    if y1 == 0:
        try:
            for iy in range(ny):
                if iy <= y2:
                    continue
                if np.sum(reg[:,iy]) > 0:
                    y1 = iy
                    break
        except:
            y2 = ny - 1

    if x1 > x2:
        sh = x2 + 1
        B = np.roll(B,-sh,axis=1)
        Psi = np.roll(Psi,-sh,axis=0)
        x1 = x1 - sh
        x2 = nx - 1
    if y1 > y2:
        sh = y2 + 1
        B = np.roll(B,-sh,axis=2)
        Psi = np.roll(Psi,-sh,axis=1)
        y1 = y1 - sh
        y2 = ny - 1

    island_length.append(max(x2-x1+1,y2-y1+1))
    island_width.append(min(x2-x1+1,y2-y1+1))

    xg = 0
    yg = 0
    cnt = 0
    for ix in range(x2-x1+1):
        for iy in range(y2-y1+1):
            if reg[ix+x1,iy+y1]:
                cnt += 1
                xg += ix + x1
                yg += iy + y1
    xg = int(float(xg)/float(cnt))
    yg = int(float(yg)/float(cnt))
    island_center.append([xg,yg])

    x1 = (xg - (nwx-1)//2 + nx)%nx
    x2 = (xg + (nwx)//2)%nx
    y1 = (yg - (nwy-1)//2 + ny)%ny
    y2 = (yg + (nwy)//2)%ny

    if x1 > x2:
        sh = x2 + 1
        B = np.roll(B,-sh,axis=1)
        Psi = np.roll(Psi,-sh,axis=0)
        x1 = x1 - sh
        x2 = nx - 1
    if y1 > y2:
        sh = y2 + 1
        B = np.roll(B,-sh,axis=2)
        Psi = np.roll(Psi,-sh,axis=1)
        y1 = y1 - sh
        y2 = ny - 1

    winx = scisi.windows.get_window(wname,nwx)
    winy = scisi.windows.get_window(wname,nwy)
    win0 = scisi.windows.get_window(wname,cut1)

    #0: full box
    #---> wfft
    data = B[:,:,:cut1,0].copy()
    MAx = []
    MAy = []
    for i in range(3):
        for iy in range(cut1):
            kx_,tmp = mode_ampl_1d(data[i,:,iy],dx)
            if MAx == []:
                MAx = np.zeros((3,len(kx_)),dtype=np.float64)
            MAx[i] += tmp
        for ix in range(nx):
            ky_,tmp = mode_ampl_1d(data[i,ix,:]*win0,dy)
            if MAy == []:
                MAy = np.zeros((3,len(ky_)),dtype=np.float64)
            MAy[i] += tmp

    MAx = MAx/float(cut1)
    MAy = MAy/float(nx)
    mx = kx_/kx_[1]
    my = ky_/ky_[1]*float(ny)/float(cut1)

    #---> save modes at this time
    MAx_0.append(MAx)
    MAy_0.append(MAy)
    if mx0 == []:
        mx0 = mx
    if my0 == []:
        my0 = my

    #1: primary sub-box
    #---> wfft
    data = B[:,x1:x2+1,y1:y2+1,0].copy()
    MAx = []
    MAy = []
    for i in range(3):
        for iy in range(nwy):
            kx_,tmp = mode_ampl_1d(data[i,:,iy]*winx,dx)
            if MAx == []:
                MAx = np.zeros((3,len(kx_)),dtype=np.float64)
            MAx[i] += tmp
        for ix in range(nwx):
            ky_,tmp = mode_ampl_1d(data[i,ix,:]*winy,dy)
            if MAy == []:
                MAy = np.zeros((3,len(ky_)),dtype=np.float64)
            MAy[i] += tmp

    MAx = MAx/float(nwy)
    MAy = MAy/float(nwx)
    mx = kx_/kx_[1]*float(nx)/float(nwx)
    my = ky_/ky_[1]*float(ny)/float(nwy)

    #---> save modes at this time
    MAx_1.append(MAx)
    MAy_1.append(MAy)
    if mx1 == []:
        mx1 = mx
    if my1 == []:
        my1 = my

    #2: secondary sub-box
    #---> it's shift time!
    B = np.roll(B,nx//2,axis=1)

    #---> wfft
    data = B[:,x1:x2+1,y1:y2+1,0].copy()
    MAx = []
    MAy = []
    for i in range(3):
        for iy in range(nwy):
            kx_,tmp = mode_ampl_1d(data[i,:,iy]*winx,dx)
            if MAx == []:
                MAx = np.zeros((3,len(kx_)),dtype=np.float64)
            MAx[i] += tmp
        for ix in range(nwx):
            ky_,tmp = mode_ampl_1d(data[i,ix,:]*winy,dy)
            if MAy == []:
                MAy = np.zeros((3,len(ky_)),dtype=np.float64)
            MAy[i] += tmp

    MAx = MAx/float(nwy)
    MAy = MAy/float(nwx)
    mx = kx_/kx_[1]*float(nx)/float(nwx)
    my = ky_/ky_[1]*float(ny)/float(nwy)

    #---> save modes at this time
    MAx_2.append(MAx)
    MAy_2.append(MAy)
    if mx2 == []:
        mx2 = mx
    if my2 == []:
        my2 = my

    #save current time
    t_vec.append(time[ind])

#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
t_vec = np.array(t_vec,dtype=np.float64)
nt = t_vec.shape[0]

island_width   = np.array(island_width,dtype=np.float64)*dy
island_length  = np.array(island_length,dtype=np.float64)*dx
island_surface = np.array(island_surface,dtype=np.float64)*dx*dy
island_center = np.array(island_center,dtype=np.float64)
island_center[:,0] *= dx
island_center[:,1] *= dy

nmx0 = len(mx0)
nmy0 = len(my0)

MAx0 = np.empty((nt,3,nmx0),dtype=np.float64)
MAy0 = np.empty((nt,3,nmy0),dtype=np.float64)
for i in range(nt):
    MAx0[i] = MAx_0[i]
    MAy0[i] = MAy_0[i]

nmx1 = len(mx1)
nmy1 = len(my1)
MAx1 = np.empty((nt,3,nmx1),dtype=np.float64)
MAy1 = np.empty((nt,3,nmy1),dtype=np.float64)
for i in range(nt):
    MAx1[i] = MAx_1[i]
    MAy1[i] = MAy_1[i]

nmx2 = len(mx2)
nmy2 = len(my2)
MAx2 = np.empty((nt,3,nmx2),dtype=np.float64)
MAy2 = np.empty((nt,3,nmy2),dtype=np.float64)
for i in range(nt):
    MAx2[i] = MAx_2[i]
    MAy2[i] = MAy_2[i]

#time to growth #GROWTH RATE DI COSA??? MODI? SPETTRO?
#          time  component   mode
#MAx0 -> (nt,   3,          nmx0) #full box
#mx0   -> (                  nmx0) |
#MAy0 -> (nt,   3,          nmy0) |
#my0   -> (                  nmy0) |
#-----------------------------------
#MAx1 -> (nt,   3,          nmx1) #primary sub-box
#mx1   -> (                  nmx1) |
#MAy1 -> (nt,   3,          nmy1) |
#my1   -> (                  nmy1) |
#-----------------------------------
#MAx2 -> (nt,   3,          nmx2) #secondary sub-box
#mx2   -> (                  nmx2) |
#MAy2 -> (nt,   3,          nmy2) |
#my2   -> (                  nmy2) |
#-----------------------------------
nnt = nt - p2f//2 - (p2f-1)//2
grx0 = np.empty((nnt,3,nmx0),dtype=np.float64)
gry0 = np.empty((nnt,3,nmy0),dtype=np.float64)
grx1 = np.empty((nnt,3,nmx1),dtype=np.float64)
gry1 = np.empty((nnt,3,nmy1),dtype=np.float64)
grx2 = np.empty((nnt,3,nmx2),dtype=np.float64)
gry2 = np.empty((nnt,3,nmy2),dtype=np.float64)
t_gr = np.empty(nnt,dtype=np.float64)

for i in range(nnt):
    t_gr[i] = t_vec[i+p2f//2]
    for j in range(3):
        for k in range(nmx0):
            xx = t_vec[i:i+p2f]
            yy = np.log(MAx0[i:i+p2f,j,k])
            gr,_ = np.polyfit(xx,yy,1)
            grx0[i,j,k] = gr
        for k in range(nmy0):
            xx = t_vec[i:i+p2f]
            yy = np.log(MAy0[i:i+p2f,j,k])
            gr,_ = np.polyfit(xx,yy,1)
            gry0[i,j,k] = gr
        for k in range(nmx1):
            xx = t_vec[i:i+p2f]
            yy = np.log(MAx1[i:i+p2f,j,k])
            gr,_ = np.polyfit(xx,yy,1)
            grx1[i,j,k] = gr
        for k in range(nmy1):
            xx = t_vec[i:i+p2f]
            yy = np.log(MAy1[i:i+p2f,j,k])
            gr,_ = np.polyfit(xx,yy,1)
            gry1[i,j,k] = gr
        for k in range(nmx2):
            xx = t_vec[i:i+p2f]
            yy = np.log(MAx2[i:i+p2f,j,k])
            gr,_ = np.polyfit(xx,yy,1)
            grx2[i,j,k] = gr
        for k in range(nmy2):
            xx = t_vec[i:i+p2f]
            yy = np.log(MAy2[i:i+p2f,j,k])
            gr,_ = np.polyfit(xx,yy,1)
            gry2[i,j,k] = gr

B_names = ['Bx','By','Bz']
for i in range(3):
    plt.close()
    fig,ax =plt.subplots(3,2,figsize=(18,12))

    im00 = ax[0,0].contourf(t_gr,mx0[1:],grx0[:,i,1:].T,32)
    plt.colorbar(im00,ax=ax[0,0])
    ax[0,0].set_yscale('log')
    ax[0,0].set_ylim(mx0[1],mx0[-1])
    ax[0,0].set_xlabel('t')
    ax[0,0].set_ylabel('mx')
    ax[0,0].set_title('g.r. vs. mx vs. t - '+B_names[i]+' - full box')

    im01 = ax[0,1].contourf(t_gr,my0[1:],gry0[:,i,1:].T,32)
    plt.colorbar(im01,ax=ax[0,1])
    ax[0,1].set_yscale('log')
    ax[0,1].set_xlabel('t')
    ax[0,1].set_ylabel('my')
    ax[0,1].set_title('g.r. vs. my vs. t - '+B_names[i]+' - full box')

    im10 = ax[1,0].contourf(t_gr,mx1[1:],grx1[:,i,1:].T,32)
    plt.colorbar(im10,ax=ax[1,0])
    ax[1,0].set_yscale('log')
    ax[1,0].set_xlabel('t')
    ax[1,0].set_ylabel('mx')
    ax[1,0].set_title('g.r. vs. mx vs. t - '+B_names[i]+' - primary box')

    im11 = ax[1,1].contourf(t_gr,my1[1:],gry1[:,i,1:].T,32)
    plt.colorbar(im11,ax=ax[1,1])
    ax[1,1].set_yscale('log')
    ax[1,1].set_xlabel('t')
    ax[1,1].set_ylabel('my')
    ax[1,1].set_title('g.r. vs. my vs. t - '+B_names[i]+' - primary box')

    im20 = ax[2,0].contourf(t_gr,mx2[1:],grx2[:,i,1:].T,32)
    plt.colorbar(im20,ax=ax[2,0])
    ax[2,0].set_yscale('log')
    ax[2,0].set_xlabel('t')
    ax[2,0].set_ylabel('mx')
    ax[2,0].set_title('g.r. vs. mx vs. t - '+B_names[i]+' - secondary box')

    im21 = ax[2,1].contourf(t_gr,my2[1:],gry2[:,i,1:].T,32)
    plt.colorbar(im21,ax=ax[2,1])
    ax[2,1].set_yscale('log')
    ax[2,1].set_xlabel('t')
    ax[2,1].set_ylabel('my')
    ax[2,1].set_title('g.r. vs. my vs. t - '+B_names[i]+' - secondary box')

    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/'+'windowed_island_gr_'+B_names[i]+'_'+wname+'_'+run_name+'.png')

