#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill plot ion and electron anisotropy, vorticity (out-of-plane), current (out-of-plane).\nWill plot max and min anisotropy over time.\n')

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
code_name = l0[1]
ipath = l1[0]
opath = l2[0]

if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')
try:
    run.get_meta()
except:
    run.get_meta(l5[0])

w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)

time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']

calc = fibo('calculator')
calc.meta = run.meta

print('\nMetadata loaded\n')

print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]

anisp_min = np.zeros(act_nt)
anisp_max = np.zeros(act_nt)
if w_ele:
    anise_min = np.zeros(act_nt)
    anise_max = np.zeros(act_nt)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    E,B = run.get_EB(ind)
    del E
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp

    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')

    anisp = np.divide(pperpp,pparlp) - 1.
    if w_ele:
        anise = np.divide(pperpe,pparle) - 1.

    #vorticity and current
    rhop,up = run.get_Ion(ind)
    curl_x,curl_y,curl_z = calc.calc_curl(up[0],up[1],up[2])
    curl_up = np.array([curl_x,curl_y,curl_z])
    Jpz = rhop*up[2]
    del curl_x,curl_y,curl_z

    if code_name == 'HVM':
        curl_x,curl_y,curl_z = calc.calc_curl(B[0],B[1],B[2])
        J = np.array([curl_x,curl_y,curl_z])
        rhoe = rhop
        ue = up
        ue[0] -= np.divide(J[0],rhop)
        ue[1] -= np.divide(J[1],rhop)
        ue[2] -= np.divide(J[2],rhop)
        del J
        del curl_x,curl_y,curl_z
    elif code_name == 'iPIC':
        rhoe,ue = run.get_Ion(ind,qom=qom_e)
    else:
        print('\nWhat code is it?\n')
    curl_x,curl_y,curl_z = calc.calc_curl(ue[0],ue[1],ue[2])
    curl_ue = np.array([curl_x,curl_y,curl_z])
    Jez = rhoe*ue[2]
    del rhop
    del up
    del rhoe
    del ue
    del curl_x,curl_y,curl_z
    del B

    #----------------------------------------------------
    #plotting anis, vorticity, J_z
    #-----------------------------
    ml.create_path(opath+'/anis_vort_jz')
    nlev = 32
    plt.close()
    fig, ax = plt.subplots(nrows=3,ncols=2,figsize=(20,12))

    axs = ax[0,0]
    im = axs.contourf(x,y,anisp[:,:,0].T,nlev)
    plt.colorbar(im,ax=axs)
    axs.set_title('pperpp/pparlp-1 - t='+str(time[ind]))

    if w_ele:
        axs = ax[0,1]
        im = axs.contourf(x,y,anise[:,:,0].T,nlev)
        plt.colorbar(im,ax=axs)
        axs.set_title('pperpe/pparle-1')

    axs = ax[1,0]
    im = axs.contourf(x,y,curl_up[2,:,:,0].T,nlev)
    plt.colorbar(im,ax=axs)
    axs.set_title('curl_up_z')

    axs = ax[1,1]
    im = axs.contourf(x,y,curl_ue[2,:,:,0].T,nlev)
    plt.colorbar(im,ax=axs)
    axs.set_title('curl_ue_z')

    axs = ax[2,0]
    im = axs.contourf(x,y,Jpz[:,:,0].T,nlev)
    plt.colorbar(im,ax=axs)
    axs.set_title('Jpz')

    axs = ax[2,1]
    im = axs.contourf(x,y,Jez[:,:,0].T,nlev)
    plt.colorbar(im,ax=axs)
    axs.set_title('Jez')

    for axs in ax.flat:
        axs.set_xlabel('x [d_i]')
        axs.set_ylabel('y [d_i]')
        axs.set_xlim(x[ixmin],x[ixmax])
        axs.set_ylim(y[iymin],y[iymax])

    plt.tight_layout()
    plt.savefig(opath+'/anis_vort_jz/'+'anis_vort_jz_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #-----------------------------------------------------
    #get min and max of anisotropy
    #-----------------------------
    anisp_min[act_cnt] = np.min(anisp)
    anisp_max[act_cnt] = np.max(anisp)
    if w_ele:
        anise_min[act_cnt] = np.min(anise)
        anise_max[act_cnt] = np.max(anise)

#---> loop over time <---
    act_cnt = act_cnt + 1
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

#------------------------------------------------------
#max and min anisotropy vs. time
#-------------------------------
ml.create_path(opath+'/anis_minmax')
plt.close()
fig, ax = plt.subplots(nrows=1,ncols=1,figsize=(10,8))
im = ax.plot(act_time,anisp_max,'b',label='protons')
im = ax.plot(act_time,anisp_min,'b')
if w_ele:
    im = ax.plot(act_time,anise_max,'r',label='electrons')
    im = ax.plot(act_time,anise_min,'r')
ax.set_title('Max and min anisotropy - t = '+str(time[ind1])+' -> '+str(time[ind2]))
ax.set_xlabel('t [ion cyclotron times]')
ax.set_ylabel('pperp/pparl - 1 ')
plt.tight_layout()
plt.legend()
plt.savefig(opath+'/anis_minmax/'+'anis_minmax_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
