import matplotlib.pyplot as plt
import sys
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
from HVM_loader import *

ipath = '/work1/califano/HVM2D/HVM_large'#'/work1/califano/HVM2D/LF_2d_DH'#
run = from_HVM(ipath)
run.get_meta('01')

t_ = 247.#300.#
_,B = run.get_EB(t_)
Psi = wl.psi_2d(B[:-1,...,0],run.meta)

#------------------

from scipy.ndimage import gaussian_filter as gf
import numpy as np

def my_FFT_smooth(field,keep_percentage):
    nx,ny = field.shape
#
    mx_cut = int(round(float(nx//2+1)*keep_percentage[0]))
    my_cut = int(round(float(ny//2+1)*keep_percentage[1]))
#
    f = np.fft.rfft2(field)
    f[mx_cut:nx//2+1,:] = 0.
    if nx%2 == 0:
        f[nx//2+1:nx//2+1+nx//2-mx_cut,:] = 0.
    else:
        f[nx//2+1:nx//2+1+nx//2-mx_cut+1,:] = 0.
    f[:,my_cut:] = 0.
    field_ = np.fft.irfft2(f)
#
    return field_

sPsi = gf(Psi,[2,2],mode='wrap')

#---------------------

import h2o
from h2o.estimators import H2OIsolationForestEstimator
from sklearn.cluster import DBSCAN
import pandas as pd

dx,dy = run.meta['ddd'][:2]

field = sPsi

def my_singulars(field,dx,dy,periodic=True,p1=0.01,p2=0.002,r1=100,r2=25,test_plot=False):
    nx,ny = field.shape
#
    if periodic:
        gmode = 'wrap'
    else:
        gmode = 'reflect'
    field_ = np.pad(field,((2,2),(2,2)),mode=gmode)
#
#------
#
    der_x,  der_y  = np.gradient(field_,dx,dy)
    der_xx, der_xy = np.gradient(der_x, dx,dy)
    _,      der_yy = np.gradient(der_y, dx,dy)
    del field_
    der_x  = der_x[2:-2,2:-2]
    der_y  = der_y[2:-2,2:-2]
    der_xx = der_xx[2:-2,2:-2]
    der_xy = der_xy[2:-2,2:-2]
    der_yy = der_yy[2:-2,2:-2]
#
#-----
#
    der_n = np.sqrt(der_x**2 + der_y**2)
    h,b=np.histogram(der_n,int(2.**(2.+np.log10(float(nx*ny)))))
    A = np.sum(h)*(b[1]-b[0])
    h = np.divide(h,A)
    ch = np.empty((len(h)),dtype=np.float)
    for i in range(len(h)):
        ch[i] = np.sum(h[:i+1])*(b[1]-b[0])
    cut_ind = max(np.sum(ch <= p1),1)
    mask = der_n < b[cut_ind]
#
    h2o.init()
#
    xx,yy = np.where(mask)
    d = {'x': xx, 'y': yy}
    coords_pd = pd.DataFrame(data=d)
    coords = h2o.H2OFrame(coords_pd)
    coord_array = coords.as_data_frame().as_matrix()
#
    clustering = DBSCAN(eps=r1, min_samples=1).fit(coord_array)
    clustering.labels_
#
    to_pandas = pd.DataFrame(clustering.labels_)
    clusterized = h2o.H2OFrame(to_pandas, column_names=['clusterization'])
    coords = coords.cbind(clusterized)
#
    mat = np.zeros((nx,ny))
    mat[:,:] = -1
    for i in range(0,len(clustering.labels_)):
        mat[xx[i],yy[i]] = clustering.labels_[i]
#
    if test_plot:
        plt.contour(Psi.T,64)
        for i in range(0,np.amax(clustering.labels_)):
            xxx,yyy = np.where(mat[:,:]==i)
            plt.plot(xxx,yyy,'.')
            tmp=np.ma.masked_where(mat[:,:]!=i,der_n)
            xxx,yyy = np.where(der_n==np.min(tmp))
            plt.plot(xxx,yyy,'xk')
#        
        plt.show()
#
#
#----
#
    new_der_n = np.full(der_n.shape,np.max(der_n)/np.mean(der_n))
    for i in range(0,np.amax(clustering.labels_)):
        loc_mean = np.mean(np.ma.masked_where(mat[:,:]!=i,der_n))
        xxx,yyy = np.where(mat[:,:]==i)
        for j in range(len(xxx)):
            new_der_n[xxx[j],yyy[j]] = der_n[xxx[j],yyy[j]]/loc_mean
#
#
    h,b=np.histogram(new_der_n,int(2.**(2.+np.log10(float(nx*ny)))))
    A = np.sum(h)*(b[1]-b[0])
    h = np.divide(h,A)
    ch = np.empty((len(h)),dtype=np.float)
    for i in range(len(h)):
        ch[i] = np.sum(h[:i+1])*(b[1]-b[0])
    cut_ind = max(np.sum(ch <= p2),1)
    mask = new_der_n < b[cut_ind]
#
    xx,yy = np.where(mask)
    d = {'x': xx, 'y': yy}
    coords_pd = pd.DataFrame(data=d)
    coords = h2o.H2OFrame(coords_pd)
    coord_array = coords.as_data_frame().as_matrix()
#
    clustering = DBSCAN(eps=r2, min_samples=1).fit(coord_array)
    clustering.labels_
#
    to_pandas = pd.DataFrame(clustering.labels_)
    clusterized = h2o.H2OFrame(to_pandas, column_names=['clusterization'])
    coords = coords.cbind(clusterized)
#
    mat = np.zeros((nx,ny))
    mat[:,:] = -1
    for i in range(0,len(clustering.labels_)):
        mat[xx[i],yy[i]] = clustering.labels_[i]
#
#
    if test_plot:
        plt.contour(Psi.T,64)
        for i in range(0,np.amax(clustering.labels_)):
            xxx,yyy = np.where(mat[:,:]==i)
            plt.plot(xxx,yyy,'.')
            tmp=np.ma.masked_where(mat[:,:]!=i,new_der_n)
            xxx,yyy = np.where(new_der_n==np.min(tmp))
            plt.plot(xxx,yyy,'xk')
#        
        plt.show()
#
#----
#
    sx = []
    sy = []
    st = []
    for i in range(0,np.amax(clustering.labels_)):
        tmp=np.ma.masked_where(mat[:,:]!=i,new_der_n)
        ix,iy = np.where(new_der_n==np.min(tmp))
        ix = ix[0]
        iy = iy[0]
        sx.append(ix)
        sy.append(iy)
        eig = np.linalg.eigvals([[der_xx[ix,iy],der_xy[ix,iy]],[der_xy[ix,iy],der_yy[ix,iy]]])
        if eig[0]*eig[1] < 0.:
            st.append(2)
        else:
            if eig[0]+eig[1] < 0.:
                st.append(1)
            else:
                st.append(0)
#
#
#
#
#----
#
    return sx,sy,st


#-----------------

sx,sy,st = my_singulars(sPsi,dx,dy,p1=0.01,p2=0.1,r1=5,r2=5,test_plot=True)

plt.contour(Psi.T,64)
for i in range(len(st)):
    if st[i] == 0:
        plt.plot(sx[i],sy[i],'ok')
    elif st[i] == 1:
        plt.plot(sx[i],sy[i],'dk')
    else:
        plt.plot(sx[i],sy[i],'xk')


plt.show()
