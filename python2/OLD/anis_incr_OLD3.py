#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy increments at a given scale langth, both in parallel and perpendicular direction (relative to the local field)\n')

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()
input_file.close()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
code_name = l0[1]
ipath = l1[0]
opath = l2[0]

if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')
try:
    run.get_meta()
except:
    run.get_meta(l5[0])

w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)

time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']

if code_name == 'HVM':
    mime = run.meta['mime']
elif code_name == 'iPIC':
    mime = abs(run.meta['msQOM'][0]/run.meta['msQOM'][1])
else:
    print('\nWhat code is this?\n')

print('\nMetadata loaded\n')

print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#-------------------------------------------------------
#hardcoded inputs
#----------------
lvec = np.array([4,8,16,32,64])*run.meta['dx']
dth = np.pi/20
CS_cut = 512
hmin = 3
m_def = 5
out_dir = 'test'

print("lvec = "+str(lvec))
print("dth = "+str(dth))
print("CS_cut = "+str(CS_cut))
print("hmin = "+str(hmin))
print("m_def = "+str(m_def))
print("out_dir = "+out_dir)

#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]

#------------------------------------------------------
#pdf extraction function
#-----------------------
@njit
def fill_hist_function_2d(field,B,lvec,dth,dd,nn,CS_cut):
    dx = dd[0]
    dy = dd[1]
    nx = nn[0]
    ny = nn[1]
    parl = []
    perp = []
#    xparl = []
#    yparl = []
#    xperp = []
#    yperp = []
#   
    nl = len(lvec)
    lgridx = np.array([int(x/dx) for x in lvec])
    lgridy = np.array([int(x/dy) for x in lvec])
    distx = np.array([0 for x in range(nl+1)])
    distx[1:] = lgridx
    disty = np.array([0 for x in range(nl+1)])
    disty[1:] = lgridy
#
    parl.append([-1.,-1.,-1.])
    perp.append([-1.,-1.,-1.])
#    xparl.append([-1.,-1.,-1.,-1])
#    yparl.append([-1.,-1.,-1.,-1])
#    xperp.append([-1.,-1.,-1.,-1])
#    yperp.append([-1.,-1.,-1.,-1])
    for (i,j) in [(i,j) for i in range(nx) for j in range(ny)]:
        for (di,dj) in [(di,dj) for di in distx for dj in disty if (di*dj == 0 and di+dj != 0)]:
            ii = (i+di+nx)%nx
            jj = (j+dj+ny)%ny
#
            b = np.array([0.5*(B[0,i,j] + B[0,ii,jj]),
                          0.5*(B[1,i,j] + B[1,ii,jj]),
                          0.5*(B[2,i,j] + B[2,ii,jj])])
            l = np.array([di*dx,dj*dy,0.])
#
            th = np.arctan2(np.linalg.norm(np.cross(l,b)),np.abs(np.dot(l,b)))
#
            lnorm = np.linalg.norm(l)
            if (j < CS_cut) and ((i+jj)//2 < CS_cut):
                CS = 0
            elif (j >= CS_cut) and ((i+jj)//2 >= CS_cut):
                CS = 1
            else:
                continue
#
            if th < dth:
                parl.append([CS,lnorm,field[i,j]-field[ii,jj]])
#                xparl.append([CS,lnorm,i,ii])
#                yparl.append([CS,lnorm,j,jj])
            elif th > (0.5*np.pi-dth):
                perp.append([CS,lnorm,field[i,j]-field[ii,jj]])
#                xperp.append([CS,lnorm,i,ii])
#                yperp.append([CS,lnorm,j,jj])
            else:
                continue
#
    return np.array(parl),np.array(perp)#,np.array(xparl),np.array(yparl),np.array(xperp),np.array(yperp)
#
#--------------------------------------------------------
#to-fit function
#----------------
def f(x,a,b):
    return a + b*x*x
#
#--------------------------------------------------------
#plotting function
#-----------------
def plot_func(axs,data,hmin,title,m=m_def):
    h0 = 10000
    h1 = 0
    b0 = 0
    b1 = 0
    flag = False
    for l in lvec:
        h,b = np.histogram(data[data[:,0]==l,1],500)
        b = b[:-1]
        b = b + 0.5*(b[1]-b[0])
        b = b[h[:] >= hmin]
        h = h[h[:] >= hmin]
        n = len(h)
        if n > 2*m_def:
            flag = True
            im = axs.plot(b,h,label=str(round(l,2))+'di')
            hmax = np.max(h)
            bmin = np.min(b)
            bmax = np.max(b)
            h0 = min(h0,np.min(h))
            h1 = max(h1,hmax)
            b0 = min(b0,bmin)
            b1 = max(b1,bmax)
            popt,pcov = cfit(f,b[(m//2)*n//m:(m//2+1)*n//m],np.log(h[(m//2)*n//m:(m//2+1)*n//m]))
            axs.plot(b,np.exp(f(b,*popt)),linestyle=':',color=im[-1].get_color())
    if flag:
        axs.legend()
        axs.set_yscale('log')
        axs.set_xlim(b0,b1)
        axs.set_ylim(h0,h1)
    axs.set_xlabel('Delta_A')
    axs.set_ylabel('counts')
    axs.set_title(title)
    return None
#
#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    E,B = run.get_EB(ind)
    del E
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp) - 1.
    del pperpp
    del pparlp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        del pperpe
        del pparle
#
    #------------------------------------------------------
    #pdf of anis increments
    #----------------------
    anisp_parl,anisp_perp = fill_hist_function_2d(anisp[...,0],B[...,0],lvec,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2],CS_cut)
    if w_ele:
        anise_parl,anise_perp = fill_hist_function_2d(anise[...,0],B[...,0],lvec,dth,run.meta['ddd'][0:2],run.meta['nnn'][0:2],CS_cut)
#
    #------------------------------------------------------
    #plot pdf of anis increments
    #---------------------------
    ml.create_path(opath+'/'+out_dir)
    plt.close('all')
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig,ax = plt.subplots(nplot,4,figsize=(16,4*nplot))
#
    if code_name == 'iPIC':
        tmult = 0.01
    else:
        tmult = 1.
#
    if w_ele:
        axs = ax[0,0]
    else:
        axs = ax[0]
    tmp = anisp_parl[anisp_parl[:,0]==0,1:]
    plot_func(axs,tmp,hmin,'Prot. anis. parl. incr. CS1 - '+str(time[ind]*tmult))
#
    if w_ele:
        axs = ax[0,1]
    else:
        axs = ax[1]
    tmp = anisp_perp[anisp_perp[:,0]==0,1:]
    plot_func(axs,tmp,hmin,'Prot. anis. perp. incr. CS1')
#
    if w_ele:
        axs = ax[0,2]
    else:
        axs = ax[2]
    tmp = anisp_parl[anisp_parl[:,0]==1,1:]
    plot_func(axs,tmp,hmin,'Prot. anis. parl. incr. CS2')
#
    if w_ele:
        axs = ax[0,3]
    else:
        axs = ax[3]
    tmp = anisp_perp[anisp_perp[:,0]==1,1:]
    plot_func(axs,tmp,hmin,'Prot. anis. perp. incr. CS2')
#
    if w_ele:
        axs = ax[1,0]
        tmp = anise_parl[anise_parl[:,0]==0,1:]
        plot_func(axs,tmp,hmin,'Elec. anis. parl. incr. CS1')
#
        axs = ax[1,1]
        tmp = anise_perp[anise_perp[:,0]==0,1:]
        plot_func(axs,tmp,hmin,'Elec. anis. perp. incr. CS1')
#
        axs = ax[1,2]
        tmp = anise_parl[anise_parl[:,0]==1,1:]
        plot_func(axs,tmp,hmin,'Elec. anis. parl. incr. CS2')
#
        axs = ax[1,3]
        tmp = anise_perp[anise_perp[:,0]==1,1:]
        plot_func(axs,tmp,hmin,'Elec. anis. perp. incr. CS2')
#
    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/'+'anis_incr_'+run_name+'_'+str(ind)+'.png')
    plt.close()
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
