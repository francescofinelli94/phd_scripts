import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import glob

from sklearn.metrics import silhouette_score
from sklearn.metrics import davies_bouldin_score
from collections import Counter
from sklearn.cluster import KMeans as KM

#-----------------------------------------------------
#HARRDCODED
df_path = '/home/sisti/python_routines'
df_name = 'NEW_alldata_t*.txt'

vars_ = ["J","x","y","Dummy","V","EV","EVB","CM","Jep"]
myvars = ["J","V","EV","EVB","CM","Jep"]

#-----------------------------------------------------
#INIT
# -> RNG seed
rnd_state = 666

#
# initialize parallelization, if needed
#

#-----------------------------------------------------
#LOAD AND PRE-PRECESS DATA
# -> choose the file
df_files = glob.glob(df_path+'/'+df_name)
if len(df_files) == 0:
    sys.exit("ERROR: no file %s found in:\n"%(df_name)+pvi_path)


print('\nfiles:')
for n,f in zip(range(len(df_files)),df_files):
    print('%d -> %s'%(n,f.split('/')[-1]))


n = int(input('Choose a file (insert the corresponding number): '))
df_file = df_files[n]

# -> read data
df = pd.read_csv(df_file,sep="\t",header=None)
df.columns = ["J","x","y","Dummy","V","EV","EVB","CM","Jep"]
print('\nAll data:')
print(df.head())

# -> select columns
sim_df = df[["J","V","EV","EVB","CM","Jep"]]
print('\nUsed data:')
print(sim_df.head())

# -> correlation
corr = sim_df.corr()

plt.close()
fig, ax = plt.subplots(figsize=(15,8))
im = sns.heatmap(corr,
                 ax=ax,
                 cmap='coolwarm',
                 vmin=-1.0,
                 vmax=1.0,
                 annot=True,
                 fmt='.2f',
                 annot_kws={'size':15},
                 linewidths=0.5)
plt.suptitle('Correlation matrix')
plt.show()
tmp = input('\nEnter anything to continue: ')
plt.close()

#-----------------------------------------------------
#FIND OPTIMAL k
# ask for k range
kmin = int(input('\nMinimum value for k: '))
kmax = int(input('\nMaximum value for k: '))

# -> test different k
distortion = []
silhouette = []
davies_bouldin = []
for i in range(kmin,kmax+1):
    km = KM(n_clusters=i,init='k-means++',random_state=rnd_state)
    km.fit(sim_df)
    y_kmi = km.predict(sim_df)
    distortion.append(km.inertia_)
    S = silhouette_score(sim_df,y_kmi,metric='euclidean')
    silhouette.append(S)
    D = davies_bouldin_score(sim_df,y_kmi)
    davies_bouldin.append(D)


# -> plot
plt.close()
fig, ax1 = plt.subplots(figsize = (8,5))

ax1.plot(range(kmin,kmax+1),distortion,marker='o',label='Distortion - inertia')
ax1.plot(range(kmin,kmax+1),silhouette,marker='o',label='Silhouette index')
ax1.plot(range(kmin,kmax+1),davies_bouldin,marker='o',label='Davies Bouldin index')

ax1.set_xlabel('Number of clusters')
ax1.set_ylabel('Value of the indicator')
ax1.legend()

ax1.grid(b=None,axis='y')
plt.show()
tmp = input('\nEnter anything to continue: ')
plt.close()
