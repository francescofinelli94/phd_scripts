#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill check enropies\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init()

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

w_ele = run.meta['w_ele']
teti = run.meta['teti']
mime = run.meta['mime']
tmult = run.meta['tmult']
opath = run.meta['opath']
run_name = run.meta['run_name']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']

wl.Can_I()

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #computing stuff
    #---------------
    Skp,dSkp = run.get_Entropy(ind)
    Skp_nan = np.isnan(Skp).sum()
    dSkp_nan = np.isnan(dSkp).sum()
    print('NAN @t='+str(time[ind])+': Skp->'+str(Skp_nan)+' dSkp->'+str(dSkp_nan))

#---> loop over time <---
    #print "\r",
    #print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
