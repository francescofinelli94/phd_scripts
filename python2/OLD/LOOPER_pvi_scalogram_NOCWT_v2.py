import sys
import glob
import os
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml

#----------------------------------------------------------------------------------------------------
#HARDCODED!!!
pvi_path = '/home/sisti/python_routines/VIRTUAL_SATELLITE'

input_rm = True
log_rm = True
err_log_rm = False

#----------------------------------------------------------------------------------------------------
os.system('echo ----------------RUN START---------------- >> err_log_pvi_scalogram_NOCWT.dat')

ans = ml.secure_input('Thresholds:\n\tmanual -> 0\n\tstat. -> 1\n: ',1,True)
if ans == 0:
    thJ = ml.secure_input('|J| threshold (0 -> 1): ',1.,True)
    thP = ml.secure_input('PVI threshold (0 -> 1): ',1.,True)
elif ans == 1:
    thJ = ml.secure_input('Number of st. dev. for |J| threshold (>0.): ',1.,True)
    thP = ml.secure_input('Number of st. dev. for PVI threshold (>0.): ',1.,True)
else:
    print('ans value %d not recognized.'%ans)
    sys.exit(-1)

# -> what time is it?
segments = sorted([int(tmp.split('_')[-1]) for tmp in glob.glob(pvi_path+'/new_pvi_*')])
if len(segments) == 0:
    sys.exit("ERROR: no direcory new_pvi_* found in:\n"+pvi_path)
print('\nsegments:')
print(segments)

for seg in segments:
    angles = sorted([int(tmp.split('angle')[-1]) 
        for tmp in glob.glob(pvi_path+('/new_pvi_%d/angle*')%(seg)) 
        if len(tmp.split('angle')[-1].split('_'))==1])
    if len(angles) == 0:
        sys.exit("ERROR: no direcory angle* found in:\n"+pvi_path+('/new_pvi_%d')%(seg))
    print('\nangles:')
    print(angles)

    for ang in angles:
        if (seg == 28) and (ang == 38):
            continue
        os.system('echo ---- seg %d ang %d ---- >> err_log_pvi_scalogram_NOCWT.dat'%(seg,ang))
        fname = 'fast_input_pvi_scalogram_%d_%d_NOCWT.dat'%(seg,ang)
        logname = 'log_pvi_scalogram_%d_%d_NOCWT.dat'%(seg,ang)
        f = open(fname,'w')
        f.write('%d\n%d\n0\n100000\n%d\n%f\n%f\n1\n\'n\'\n '%(seg,ang,ans,thJ,thP))
        f.close()
        os.system('python pvi_scalogram_NOCWT_v3.py < %s > %s 2>> err_log_pvi_scalogram_NOCWT.dat'%(fname,logname))
        print('\n---> SEG %d ANG %d DONE!!! <---\n'%(seg,ang))

if input_rm:
    os.system('rm -f fast_input_pvi_scalogram_*_NOCWT.dat')
if log_rm:
    os.system('rm -f log_pvi_scalogram_*_NOCWT.dat')
if err_log_rm:
    os.system('rm -f err_log_pvi_scalogram_NOCWT.dat')
