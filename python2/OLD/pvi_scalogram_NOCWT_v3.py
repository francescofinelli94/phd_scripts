import sys
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec
import scipy.ndimage.interpolation as ndm
from scipy.integrate import simps
#import pywt
#import scaleogram as scg 
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from HVM_loader import *

font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#----------------------------------------------------------------------------------------------------
#HARDCODED!!!

sim_path='/work1/califano/HVM2D/HVM_large'
meta_subpath = '01'

pvi_path = '/home/sisti/python_routines/VIRTUAL_SATELLITE'

t_dict = {}
t_dict['28'] = 247.
t_dict['60'] = 494.

p = 2.0  # number of steps inside each cell
SSx0 = 0.01   # initial positions of the virtual satellite inside the simulation box
SSy0 = 0.01

regions_path = '/home/sisti/python_routines/regions_verified_reconnection_tTIME.txt'

opath = '/home/finelli/Downloads'

#----------------------------------------------------------------------------------------------------
#Metadata time

run = from_HVM(sim_path)
run.get_meta(meta_subpath)
run_name = sim_path.split('/')[-1]
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']

calc = fibo('calc')
calc.meta = run.meta

out_dir = 'PVI-scalograms' 

#----------------------------------------------------------------------------------------------------
#PRIMA PARTE: ricostruzione traiettoria, le coordinate finali xco e yco sono in unita' [di]

# -> what time is it?
segments = sorted([int(tmp.split('_')[-1]) for tmp in glob.glob(pvi_path+'/new_pvi_*')])
if len(segments) == 0:
    sys.exit("ERROR: no direcory new_pvi_* found in:\n"+pvi_path)

print('\nsegments:')
print(segments)
seg = ml.secure_input('Choose a segment: ',10,True)
#                                    WAIT FOR INPUT
if seg not in segments:
    sys.exit("ERROR: choosen segment not found.")

seg = str(seg)
if seg in t_dict.keys():
    t_ = t_dict[seg]
    print("\nt="+str(t_))
else:
    t_ = ml.secure_input('Choose time (float): ',1.,True)
#                                    WAIT FOR INPUT

ind = ml.index_of_closest(t_,run.meta['time'])

# -> angle?
angles = sorted([int(tmp.split('angle')[-1]) 
    for tmp in glob.glob(pvi_path+('/new_pvi_%s/angle*')%(seg)) 
    if len(tmp.split('angle')[-1].split('_'))==1])
if len(angles) == 0:
    sys.exit("ERROR: no direcory angle* found in:\n"+pvi_path+('/new_pvi_%s')%(seg))

print('\nangles:')
print(angles)
ang = ml.secure_input('Choose an angle: ',10,True)
#                                    WAIT FOR INPUT
if ang not in angles:
    sys.exit("ERROR: choosen angle not found.")

# -> load/construct trajectory
pvi_path = pvi_path + '/new_pvi_%s/angle%d'%(seg,ang)

pi2 = 2.0*np.pi
theta_s = float(ang)*pi2/360.0

files = glob.glob(pvi_path+'/B_1Dcut_pvi_*.dat')
ll = [] #PVI scale
cc = [] #is centered?
for f in files:
    tmp = f.split('pvi_')[-1].split('.')[0]
    ll.append(int(tmp.split('_')[0]))
    cc.append(tmp.split('_')[-1] == 'center')


files = [tmp for _,tmp in sorted(zip(ll,files))]
cc = [tmp for _,tmp in sorted(zip(ll,cc))]
ll = sorted(ll)

lstart = ll[0]
f = open(files[0], 'r')
lines = f.readlines()
f.close()
ns = len(lines)
sco = np.zeros((ns),dtype=np.float64)
xco = np.zeros((ns),dtype=np.float64)
yco = np.zeros((ns),dtype=np.float64)
PVI = np.full((ns,len(ll)),-1.,dtype=np.float64)
num_col = len(lines[0].split())
if num_col == 2:
    DELTAs  = pi2/float(nx)/p
    DELTAsx = DELTAs*np.cos(theta_s)
    DELTAsy = DELTAs*np.sin(theta_s)
    SSx = SSx0
    SSy = SSy0
    SSS = np.sqrt(SSx**2+SSy**2)
    for i in range(ns):
        SSx += DELTAsx
        SSy += DELTAsy
        SSS += DELTAs
        if SSx >= pi2: SSx -= pi2
        if SSy >= pi2: SSy -= pi2
        sco[i]=SSS
        xco[i]=SSx
        yco[i]=SSy
elif num_col == 4:
    for i in range(ns):
        tr = lines[i].split()
        sco[i] = float(tr[0])
        xco[i] = float(tr[1])
        yco[i] = float(tr[2])
    DELTAs  = sco[1] - sco[0]
    DELTAsx = xco[1] - xco[0]
    DELTAsy = yco[1] - yco[0]
else:
    sys.exit("ERROR: number of columns is not 2 nor 4, but "+str(num_col)+".")

sco = sco*xl/pi2
xco = xco*xl/pi2
yco = yco*yl/pi2
DELTAs  = DELTAs*xl/pi2
DELTAsx = DELTAsx*xl/pi2 
DELTAsy = DELTAsy*yl/pi2 
ds = sco[1] - sco[0]

# -> read PVIs
for il,l,c,fname in zip(range(len(ll)),ll,cc,files):
    f = open(fname, 'r')
    lines = f.readlines()
    f.close()
    for i in range(len(lines)):
        PVI[i,il] = float(lines[i].split()[-1])
    if not c:
        PVI[:,il] = np.roll(PVI[:,il],l//2)

# -> PVIs scaled
PVI_sc = np.empty(PVI.shape,dtype=np.float64)
for il in range(len(ll)):
    PVI_sc[:,il] = np.divide(PVI[:,il],np.max(PVI[:,il]))

# -> integrated PVI
intPVI = np.full((ns),-1.,dtype=np.float64)
for i in range(ns):
    if i < ll[-1]//2 or i >= ns-1-ll[-1]//2:
        continue
    intPVI[i] = simps(PVI[i,:],ll)

intPVI_sc = np.divide(intPVI,np.max(intPVI))

#----------------------------------------------------------------------------------------------------
#SECONDA PARTE: interpolazione

# -> calcolare dati
_,B = run.get_EB(ind)

cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
J = np.array([cx,cy,cz])
del cx,cy,cz
Jn = np.sqrt(J[0]**2 + J[1]**2 + J[2]**2)
del J

# -> interpolazione vera e propria
new_coordsx = xco/dx
new_coordsy = yco/dy

Bx_an = ndm.map_coordinates(B[0,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
By_an = ndm.map_coordinates(B[1,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
Bz_an = ndm.map_coordinates(B[2,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
Jn_an = ndm.map_coordinates( Jn[:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
del B

# -> J scaled
Jn_an_sc = np.divide(Jn_an,np.max(Jn_an))

# -> where is reconnection?
tmp = regions_path.split('TIME') 
regions_path = tmp[0] + str(int(t_)) + tmp[1]
regions = np.loadtxt(regions_path)
regions = (regions > -1.).astype(np.int)
regions_an = ndm.map_coordinates(regions[:,:].T,np.vstack((new_coordsx,new_coordsy)),
                                 mode='wrap',order=1,prefilter=False)
rec_ind = np.where(regions_an[:] > 0.5)[0]

#----------------------------------------------------------------------------------------------------
#PLOT GENERAL

plot_flag = True
while plot_flag:
    print("\ntrajectory range: %f -> %f"%(sco[0],sco[-1]))
    sco0 = ml.secure_input('start from: ',1.,True)
    sco1 = ml.secure_input('arrive at: ',1.,True)
    i0 = np.argmin(np.abs(sco-sco0))
    i1 = np.argmin(np.abs(sco-sco1))
#
    ans = ml.secure_input('Thresholds:\n\tmanual -> 0\n\tstat. -> 1\n: ',1,True)
    if ans == 0:
        thJn  = ml.secure_input('|J| threshold (0 -> 1): ',1.,True)
        thPVI = ml.secure_input('PVI threshold (0 -> 1): ',1.,True)
#
        th_sup = ' - J%d P%d'%(int(thJn*100),int(thPVI*100))
        th_file = 'J%dP%d_NOCWT'%(int(thJn*100),int(thPVI*100))
    elif ans == 1:
        sigJn = ml.secure_input('Number of st. dev. for |J| threshold (>0.)',1.,True)
        J2 = Jn_an_sc**2
        thJn = np.sqrt( np.mean(J2) + sigJn*np.std(J2) )
        del J2
        sigPVI = ml.secure_input('Number of st. dev. for PVI threshold (>0.)',1.,True)
        thPVI = np.mean(intPVI_sc) + sigPVI*np.std(intPVI_sc)
#
        th_sup = ' - J%ds P%ds'%(int(sigJn*100),int(sigPVI*100))
        th_file = 'J%dsP%ds_NOCWT'%(int(sigJn*100),int(sigPVI*100))
    else:
        print('ans value %d not recognized.'%ans)
        sys.exit(-1)
#
    sav_flg = ml.secure_input('show (0) or save (1,...): ',1,True)
#
    plt.close()
    fig = plt.figure(figsize=(16,10),constrained_layout=True)
    gs = GridSpec(5,8,figure=fig)
#
    # -> 1D: PVIs (colors), J (black), rec (*b)
    ax0 = fig.add_subplot(gs[0,0:4])
    for i,l in zip(range(len(ll)),ll):
        ax0.plot(sco[l//2:-(1+l//2)],PVI_sc[:,i][l//2:-(1+l//2)])
    ax0.plot(sco,Jn_an_sc,color='black',label='$|J|$ normalized')
    ax0.plot(sco[rec_ind[:]],Jn_an_sc[rec_ind[:]],'*b')
    ax0.axhline(y=thJn,linestyle='--',color='darkgoldenrod')
    ax0.set_xlabel('s [$d_i$]')
    ax0.set_ylabel('$PVI$ normalized')
    ax0.legend()
    ax0.set_xlim(sco[i0],sco[i1])
    ax0.set_ylim(0.,1.)
#
    # -> 2D: J (heat), trajextory (red), rec (blue), J/Jmax>thJn (darkgoldenrod), 
    #        (int. PVI)/(int. PVI)max>thPVI (green)
    ax1 = fig.add_subplot(gs[1:5,0:4])
    ax1.contourf(y,x,Jn[:,:,0],63)
    ax1.scatter(xco[i0:i1],yco[i0:i1],s=1,c='darkgray')
    ax1.scatter(xco[rec_ind[:]],yco[rec_ind[:]],s=110,c='blue')
    ax1.scatter(xco[Jn_an_sc>thJn],yco[Jn_an_sc>thJn],s=75,c='darkgoldenrod')
    ax1.scatter(xco[intPVI_sc>thPVI],yco[intPVI_sc>thPVI],s=40,c='green')
    ax1.set_ylabel('x [$d_i$]')
    ax1.set_xlabel('y [$d_i$]')
    ax1.set_title('$|J|$ and peaks')
    ax1.set_ylim(x[0],x[-1])
    ax1.set_xlim(y[0],y[-1])
#
    # -> 2D: PVI scalogram 
    ax2 = fig.add_subplot(gs[0:4,4:8])
    ax2.contourf(sco[i0:i1],ll,PVI[i0:i1,:].T,63)
    ax2.set_ylabel('scales [$ds='+str(round(ds,4))+'\;d_i$]')
    ax2.set_title('$PVI$ scalogram')
    ax2.set_xlim(sco[i0],sco[i1])
    ax2.set_ylim(ll[0],ll[-1])
#
    # -> 1D: scaled integrated PVI (green)
    ax4 = fig.add_subplot(gs[4,4:8])
    ax4.plot(sco,intPVI_sc,'g',label='int. $PVI$ (norm.)')
    ax4.axhline(y=thPVI,linestyle='--',color='green')
    ax4.set_xlabel('s [$d_i$]')
    ax4.legend()
    ax4.set_xlim(sco[i0],sco[i1])
    ax4.set_ylim(0.,1.)
#
    fig.suptitle(run_name.replace('_',' ')+' s%s a%d'%(seg,ang)+th_sup)
#
    if sav_flg == 0:
        fig.show()
    else:
        th_descriptor = th_file
        ml.create_path(opath+'/'+out_dir+'/'+th_descriptor)
        fig.savefig(opath+'/'+out_dir+'/'+th_descriptor+'/pvi_cwt_'+run_name+'_'+
                th_descriptor+'_%d_%d_s%sa%d'%(i0,i1,seg,ang)+'.png')
#
    ans = ml.secure_input('\nPlot again? (Y/n): ','',True)
    if ans == 'n': plot_flag = False
plt.close()

#----------------------------------------------------------------------------------------------------
#FINDING SITES

lm = ll[-1]//2
lp = 1 + ll[-1]//2

flg = {}
col = {}
mlt = {}
flg['r'] = regions_an > 0.5
flg['J'] = Jn_an_sc > thJn
flg['P'] = intPVI_sc > thPVI
col['r'] = 'blue'
col['J'] = 'darkgoldenrod'
col['P'] = 'green'
mlt['r'] = 1
mlt['J'] = 10
mlt['P'] = 100

sco_seg = {}
sco_seg['r'] = np.zeros((ns),dtype=np.int)
sco_seg['J'] = np.zeros((ns),dtype=np.int)
sco_seg['P'] = np.zeros((ns),dtype=np.int)

for i in range(ns):
    for th in ['r','J','P']:#,'C']:
        if flg[th][i]:
            ii = i
            while flg[th][ii] and (ii < ns):
                flg[th][ii] = False
                ii += 1
            ii = (i + (ii - 1))//2
            sco_seg[th][max(ii-lm,lm):min(ii+lp,ns-lp+1)] = 1

del flg

sco_segs = ( sco_seg['r']*mlt['r'] + sco_seg['J']*mlt['J'] + 
        sco_seg['P']*mlt['P'] )# + sco_seg['C']*mlt['C'] )
del sco_seg
segs = []
ii = -1
for i in range(ns):
    if i <= ii:
        continue
    if sco_segs[i] != 0:
        ii = i
        while (sco_segs[ii] != 0) and (ii < ns):
            ii += 1
        ii -= 1
        maxval = np.max(sco_segs[i:ii+1])
        jj = 0
        cnt = 0
        for j_ in range(ii+1-i):
            if sco_segs[j_+i] == maxval:
                cnt += 1
                jj += j_ + i
        jj = jj//cnt
        segs.append([max(jj-lm,lm),min(jj+lp,ns-lp+1),maxval])

#----------------------------------------------------------------------------------------------------
#PLOT SITES
dnx = 500 # approx. 50di added, 25 per side
hdnx = dnx//2
dnx = hdnx*2 #avoid problem w/ odd numbers
x_ax = np.empty(nx+dnx,dtype=np.float64)
x_ax[hdnx:hdnx+nx] = x
x_ax[0:hdnx] = (np.arange(hdnx,dtype=np.float) - float(hdnx))*dx
x_ax[hdnx+nx:dnx+nx] = (np.arange(hdnx,dtype=np.float) + float(nx))*dx

dny = 500 # approx. 50di added, 25 per side
hdny = dny//2
dny = hdny*2 #avoid problem w/ odd numbers
y_ax = np.empty(nx+dnx,dtype=np.float64)
y_ax[hdny:hdny+ny] = y
y_ax[0:hdny] = (np.arange(hdny,dtype=np.float) - float(hdny))*dy
y_ax[hdny+ny:dny+ny] = (np.arange(hdny,dtype=np.float) + float(ny))*dy

Jn = np.pad(Jn[...,0],((hdnx,hdnx),(hdny,hdny)),'wrap')

for s in segs:
    i0 = s[0]
    i1 = s[1] - 1
#
    th_flg = [(s[2]%(n*10))//n for n in [1,10,100,1000]]
#
    plt.close()
    fig = plt.figure(figsize=(16,10),constrained_layout=False)
    gs = GridSpec(1,2,figure=fig,wspace=0.3)
    gs0 = SubSpec(2,1,gs[0],height_ratios=[1,4],hspace=0.5)
    gs1 = SubSpec(2,2,gs[1],width_ratios=[3,1],height_ratios=[1,3],wspace=0.,hspace=0.)
#
    # -> 1D: PVIs (colors), J (black), rec (*b)
    ax0 = fig.add_subplot(gs0[0])
    every_n = 3
    for i,l in zip(range(len(ll)),ll):
        if i%every_n != 0:
            continue
        ax0.plot(sco[l//2:-(1+l//2)],PVI_sc[:,i][l//2:-(1+l//2)])
    ax0.plot(sco,Jn_an_sc,color='black',label='$|J|$ normalized')
    ax0.plot(sco[rec_ind[:]],Jn_an_sc[rec_ind[:]],'*b')
    ax0.axhline(y=thJn,linestyle='--',color='darkgoldenrod')
    ax0.set_xlabel('s [$d_i$]')
    ax0.set_ylabel('$PVI$ normalized')
    ax0.legend()
    ax0.set_xlim(sco[i0],sco[i1])
    ax0.set_ylim(0.,1.)
#
    # -> 2D: J (heat), trajectory (darkgray), rec (blue), site (red square)
    ax1 = fig.add_subplot(gs0[1])
    ic = (i0 + i1)//2
    xc = xco[ic]
    yc = yco[ic]
    x0 = xc - 25.
    x1 = xc + 25.
    y0 = yc - 25.
    y1 = yc + 25.
    ix0 = np.argmin(np.abs(x_ax-y0))
    ix1 = np.argmin(np.abs(x_ax-y1))
    iy0 = np.argmin(np.abs(y_ax-x0))
    iy1 = np.argmin(np.abs(y_ax-x1))
    ax1.contourf(y_ax[iy0:iy1+1],x_ax[ix0:ix1+1],Jn[ix0:ix1+1,iy0:iy1+1],63)
    ax1.scatter([xc+DELTAsx*(i_-(i1+1-i0)//2) for i_ in range(i1+1-i0)],
                [yc+DELTAsy*(i_-(i1+1-i0)//2) for i_ in range(i1+1-i0)],s=1,c='darkgray')
    ax1.scatter([xc],[yc],s=5,c='red')
    ax1.set_ylabel('x [$d_i$]')
    ax1.set_xlabel('y [$d_i$]')
    ax1.set_title('$|J|$ and trajectory')
    ax1.set_ylim(x_ax[ix0],x_ax[ix1])
    ax1.set_xlim(y_ax[iy0],y_ax[iy1])
#
    # -> 2D: PVI scalogram 
    ax2 = fig.add_subplot(gs1[1,0])
    ax2.contourf(sco[i0:i1+1],ll,PVI[i0:i1+1,:].T,63)
    ax2.set_ylabel('scales [$ds='+str(round(ds,4))+'\;d_i$]')
    ax2.set_xlabel('s [$d_i$]')
    ax2.set_xlim(sco[i0],sco[i1])
    ax2.set_ylim(ll[0],ll[-1])
    ax2.tick_params(bottom=True,top=True,left=True,right=True,
         direction='out',labelbottom=True,labeltop=False,
         labelleft=True,labelright=False)
    xticks = ax2.xaxis.get_major_ticks()
    xticks[-1].label1.set_visible(False)
    yticks = ax2.yaxis.get_major_ticks()
    yticks[-1].label1.set_visible(False)
#
    # -> 1D: scaled integrated PVI (green)
    ax4 = fig.add_subplot(gs1[0,0],sharex=ax2)
    ax4.plot(sco,intPVI_sc,'g',label='int. $PVI$ (norm.)')
    ax4.axhline(y=thPVI,linestyle='--',color='green')
    ax4.set_xlim(sco[i0],sco[i1])
    ax4.set_ylim(0.,1.)
    ax4.tick_params(bottom=True,top=True,left=True,right=True,
         direction='out',labelbottom=False,labeltop=False,
         labelleft=True,labelright=False)
    ax4.set_ylabel('$<PVI>_{scales}$ norm')

#
    # -> 1D: PVI
    ax1 = fig.add_subplot(gs1[1,1],sharey=ax2)
    tmp = np.empty((len(ll)),dtype=np.float64)
    for il in range(len(ll)):
        tmp[il] = simps(PVI[i0:i1+1,il],sco[i0:i1+1])
    tmp = np.divide(tmp,np.max(tmp))
    ax1.plot(tmp,ll,label='$<PVI>_s$ norm.')
    ax1.set_ylim(ll[0],ll[-1])
    ax1.set_xlim(0.,1.)
    ax1.tick_params(bottom=True,top=True,left=True,right=True,
         direction='out',labelbottom=True,labeltop=False,
         labelleft=False,labelright=False)
    ax1.set_xlabel('$<PVI>_{space}$ norm')
#
    ax5 = fig.add_subplot(gs1[0,1])
    ax5.axis('off')
    ax5.invert_yaxis()
    ax5.text(0.1,0.5,'$PVI$ scalogram',verticalalignment='top')
#
    th = ''
    if th_flg[0] == 1:
        th = th + 'r'
    else:
        th = th + '-'
    if th_flg[1] == 1:
        th = th + 'J'
    else:
        th = th + '-'
    if th_flg[2] == 1:
        th = th + 'P'
    else:
        th = th + '-'
    if th_flg[3] == 1:
        th = th + 'C'
    else:
        th = th + '-'
    fig.suptitle(run_name.replace('_',' ')+' s%s a%d'%(seg,ang)+
            th_sup+' - %d-%d'%(i0,i1)+' - th '+th)
#
    th_descriptor = th_file
    ml.create_path(opath+'/'+out_dir+'/'+th_descriptor)
    fig.savefig(opath+'/'+out_dir+'/'+th_descriptor+'/pvi_cwt_1Dscalogram_'+run_name+'_'+
           th_descriptor+'tf%d_%d_%d_s%sa%d'%(s[2],i0,i1,seg,ang)+'.png')
    #fig.show()
    #tmp = input('whatever ')
    #break
plt.close()
