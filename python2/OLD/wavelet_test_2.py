import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.signal as scisi
import pywt

import work_lib as wl
import shift_and_flood as saf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

ipath = '/work1/califano/HVM2D/LF_2d_DH'#'/work1/fpucci/DoubleHarris/run3/data0'#'/work1/califano/HVM2D/HVM_2d_DH'
run = from_HVM(ipath)
#run = from_iPIC(ipath)
run.get_meta()

nx = run.meta['nx']
ny = run.meta['ny']
dx = run.meta['dx']
dy = run.meta['dy']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])
y0 = int(y1/run.meta['dy'])
yr = int(min(abs(y1-y2),run.meta['yl']-abs(y1-y2))/2./run.meta['dy'])
perm_sh_y = y0 - yr

#island growth (the smaller this, the bigger the island)
igx = 5.
igy = 0.4#0.8

#threshold parameter (closer to 1 this, the smaller the the island)
th_param = 0.98
ind = 120 #80 ,120
_,B = run.get_EB(ind)
if perm_sh_y < 0:
    B = np.roll(B,-perm_sh_y,axis=2)
    y0 = y0 - perm_sh_y

Psi = wl.psi_2d(B[0:2,:,:,0],run.meta)
reg,nreg = saf.my_flood(-Psi,-np.max(Psi)*th_param,periodicity=[True,True])

reg_sav = reg

v0 = 0
cnt = 0
for n in range(nreg):
    c = np.sum(reg == n+1)
    if c > cnt:
        cnt = c
        v0 = n + 1

reg = reg == v0
for ix in range(nx):
    if np.sum(reg[ix,:]) > 0:
        x1 = ix
        break

for ix in range(nx):
    if ix < x1:
        continue
    if np.sum(reg[ix,:]) == 0:
        x2 = ix - 1
        break

if x1 == 0:
    for ix in range(nx):
        if ix <= x2:
            continue
        if np.sum(reg[ix,:]) > 0:
            x1 = ix
            break

for iy in range(ny):
    if np.sum(reg[:,iy]) > 0:
        y1 = iy
        break

for iy in range(ny):
    if iy < y1:
        continue
    if np.sum(reg[:,iy]) == 0:
        y2 = iy - 1
        break

if y1 == 0:
    for iy in range(ny):
        if iy <= y2:
            continue
        if np.sum(reg[:,iy]) > 0:
            y1 = iy
            break

if x1 > x2:
    sh = x2 + 1
    B = np.roll(B,-sh,axis=1)
    Psi = np.roll(Psi,-sh,axis=0)
    x1 = x1 - sh
    x2 = nx - 1

if y1 > y2:
    sh = y2 + 1
    B = np.roll(B,-sh,axis=2)
    Psi = np.roll(Psi,-sh,axis=1)
    y1 = y1 - sh
    y2 = ny - 1

xg = 0
yg = 0
cnt = 0
for ix in range(x2-x1+1):
    for iy in range(y2-y1+1):
        if reg[ix+x1,iy+y1]:
            cnt += 1
            xg += ix + x1
            yg += iy + y1

xg = int(float(xg)/float(cnt))
yg = int(float(yg)/float(cnt))

nwx = 512
nwy = 128

x1 = (xg - nwx//2 + nx)%nx
x2 = (xg + (nwx+1)//2)%nx
y1 = (yg - nwy//2 + ny)%ny
y2 = (yg + (nwy+1)//2)%ny
if x1 > x2:
    sh = x2 + 1
    B = np.roll(B,-sh,axis=1)
    Psi = np.roll(Psi,-sh,axis=0)
    x1 = x1 - sh
    x2 = nx - 1

if y1 > y2:
    sh = y2 + 1
    B = np.roll(B,-sh,axis=2)
    Psi = np.roll(Psi,-sh,axis=1)
    y1 = y1 - sh
    y2 = ny - 1

scales=np.logspace(np.log10(1),np.log10(nx//2),nx//2)
coef = np.zeros((len(scales),nx),dtype=np.cdouble)
for i in np.linspace(y1,y2,y2-y1+1,dtype=np.int):
     tmp,freq=pywt.cwt(B[2,:,i,0],scales,'cgau8',sampling_period=dx)
     coef += tmp
 
coef /= float(y2-y1+1)

plt.close()
fig,ax = plt.subplots(3,1,figsize=(12,12))

im0 = ax[0].contourf(x,y[y1:y2+1],B[2,:,y1:y2+1,0].T,64)
plt.colorbar(im0,ax=ax[0])
ax[0].set_xlabel('x');ax[0].ylabel['y']

im1 = ax[1].contourf(x,freq*2.*np.pi,np.abs(coef),64)
plt.colorbar(im1,ax=ax[1])
ax[1].set_yscale('log')
ax[1].set_xlabel('x');ax[1].ylabel['k']

im2 = ax[2].contourf(x,scales*dx,np.abs(coef),64)
plt.colorbar(im2,ax=ax[2])
#ax[2].set_yscale('log')
ax[2].set_xlabel('x');ax[2].ylabel['scale']

plt.show()
