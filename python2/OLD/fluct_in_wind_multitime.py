#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
sys.path.append('/home/finelli/Documents/templates')
from histABC_wrapped import hist2d_wrap as h2dw

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta cumulated over different times.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'fluct_in_wind_multitimes'
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

calc = fibo('calc')
calc.meta=run.meta

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#plot type
mode = 'ABCacc'
logscale_flg = True

#B0
B0_as_time_avg = True

#mask
msk_flg = False # True -> 'cutout'; False -> 'CS1'
if msk_flg:
    mask_name = 'cutout'
    mask = np.ones((nx,ny),dtype=bool)
    for ix in range(nx):
        for iy in range(cut1,cut2+1):
            mask[ix,iy] = False
else:
    mask_name = 'CS1'
    mask = np.zeros((nx,ny),dtype=bool)
    for ix in range(nx):
        for iy in range(int(12.5/run.meta['ddd'][0])):
            mask[ix,iy] = True

#see Hellinger et al. 2006
#    Astfalk and Jenko 2016
#    Lazar et al. 2015
params = {}
params['H2006'] = {}
params['A2016'] = {}
params['L2015'] = {}

params['H2006']['PC'] = {}
params['H2006']['PC']['10m3'] = {}
params['H2006']['PC']['10m3']['bM'] = [ 0.43,0.42,-0.0004]

params['L2015']['EC'] = {}
params['L2015']['EC']['e05'] = {}
params['L2015']['EC']['e05']['10m2'] = {}
params['L2015']['EC']['e05']['10m2']['k2'] = {}
params['L2015']['EC']['e05']['10m2']['k2']['T'] = [0.54,0.21,0.14,1.28]
params['L2015']['EC']['e05']['10m2']['k2']['C'] = [0.26,-0.01,1.18,0.56]
params['L2015']['EC']['e05']['10m2']['k6'] = {}
params['L2015']['EC']['e05']['10m2']['k6']['T'] = [0.43,0.17,0.02,2.04]
params['L2015']['EC']['e05']['10m2']['k6']['C'] = [0.19,-0.09,1.11,0.63]
params['L2015']['EC']['e05']['10m2']['bM'] = {}
params['L2015']['EC']['e05']['10m2']['bM']['T'] = [0.40,0.10,0.05,1.26]
params['L2015']['EC']['e05']['10m2']['bM']['C'] = [0.17,-0.11,1.18,0.65]
params['L2015']['EC']['e05']['10m3'] = {}
params['L2015']['EC']['e05']['10m3']['k2'] = {}
params['L2015']['EC']['e05']['10m3']['k2']['T'] = [0.10,0.30,0.09,1.22]
params['L2015']['EC']['e05']['10m3']['k2']['C'] = [0.01,-0.46,19.1,0.97]
params['L2015']['EC']['e05']['10m3']['k6'] = {}
params['L2015']['EC']['e05']['10m3']['k6']['T'] = [0.08,0.38,0.003,2.54]
params['L2015']['EC']['e05']['10m3']['k6']['C'] = [0.005,-0.52,14.6,1.07]
params['L2015']['EC']['e05']['10m3']['bM'] = {}
params['L2015']['EC']['e05']['10m3']['bM']['T'] = [0.07,0.31,0.02,1.23]
params['L2015']['EC']['e05']['10m3']['bM']['C'] = [0.003,-0.67,21.3,1.23]
params['L2015']['EC']['e01'] = {}
params['L2015']['EC']['e01']['10m2'] = {}
params['L2015']['EC']['e01']['10m2']['k2'] = {}
params['L2015']['EC']['e01']['10m2']['k2']['T'] = [1.74,0.05,0.04,1.29]
params['L2015']['EC']['e01']['10m2']['k2']['C'] = [1.35,-0.04,0.30,0.60]
params['L2015']['EC']['e01']['10m2']['k6'] = {}
params['L2015']['EC']['e01']['10m2']['k6']['T'] = [1.54,0.06,0.003,1.86]
params['L2015']['EC']['e01']['10m2']['k6']['C'] = [1.27,-0.03,0.16,0.62]
params['L2015']['EC']['e01']['10m2']['bM'] = {}
params['L2015']['EC']['e01']['10m2']['bM']['T'] = [1.47,0.05,0.002,2.03]
params['L2015']['EC']['e01']['10m2']['bM']['C'] = [1.23,-0.03,0.14,0.63]
params['L2015']['EC']['e01']['10m3'] = {}
params['L2015']['EC']['e01']['10m3']['k2'] = {}
params['L2015']['EC']['e01']['10m3']['k2']['T'] = [0.21,0.12,0.04,1.28]
params['L2015']['EC']['e01']['10m3']['k2']['C'] = [0.11,-0.07,0.84,0.60]
params['L2015']['EC']['e01']['10m3']['k6'] = {}
params['L2015']['EC']['e01']['10m3']['k6']['T'] = [0.18,0.14,0.001,2.21]
params['L2015']['EC']['e01']['10m3']['k6']['C'] = [0.11,-0.06,0.47,0.63]
params['L2015']['EC']['e01']['10m3']['bM'] = {}
params['L2015']['EC']['e01']['10m3']['bM']['T'] = [0.17,0.15,0.0001,2.83]
params['L2015']['EC']['e01']['10m3']['bM']['C'] = [0.11,-0.05,0.40,0.64]

params['H2006']['MR'] = {}
params['H2006']['MR']['10m3'] = {}
params['H2006']['MR']['10m3']['bM'] = [ 0.77,0.76,-0.0160]

params['H2006']['PF'] = {}
params['H2006']['PF']['10m3'] = {}
params['H2006']['PF']['10m3']['bM'] = [-0.47,0.53, 0.5900]

params['H2006']['OF'] = {}
params['H2006']['OF']['10m3'] = {}
params['H2006']['OF']['10m3']['bM'] = [-1.40,1.00,-0.1100]

params['A2016']['PF'] = {}
params['A2016']['PF']['10m3'] = {}
params['A2016']['PF']['10m3']['bM']  = [-0.487,0.537, 0.560]
params['A2016']['PF']['10m3']['k12'] = [-0.438,0.475, 0.503]
params['A2016']['PF']['10m3']['k8']  = [-0.429,0.486, 0.423]
params['A2016']['PF']['10m3']['k6']  = [-0.417,0.498, 0.350]
params['A2016']['PF']['10m3']['k4']  = [-0.387,0.518, 0.226]
params['A2016']['PF']['10m3']['k2']  = [-0.274,0.536, 0.042]
params['A2016']['PF']['10m2'] = {}
params['A2016']['PF']['10m2']['bM']  = [-0.701,0.623, 0.599]
params['A2016']['PF']['10m2']['k12'] = [-0.656,0.596, 0.567]
params['A2016']['PF']['10m2']['k8']  = [-0.623,0.579, 0.569]
params['A2016']['PF']['10m2']['k6']  = [-0.625,0.585, 0.501]
params['A2016']['PF']['10m2']['k4']  = [-0.625,0.593, 0.379]
params['A2016']['PF']['10m2']['k2']  = [-0.632,0.589, 0.139]
params['A2016']['PF']['10m1'] = {}
params['A2016']['PF']['10m1']['bM']  = [-0.872,0.495, 1.233]
params['A2016']['PF']['10m1']['k12'] = [-0.899,0.502, 1.213]
params['A2016']['PF']['10m1']['k8']  = [-0.937,0.509, 1.097]
params['A2016']['PF']['10m1']['k6']  = [-0.947,0.505, 1.088]
params['A2016']['PF']['10m1']['k4']  = [-0.977,0.496, 1.068]
params['A2016']['PF']['10m1']['k2']  = [-1.230,0.464, 1.206]

params['A2016']['OF'] = {}
params['A2016']['OF']['10m3'] = {}
params['A2016']['OF']['10m3']['bM']  = [-1.371,0.996,-0.083]
params['A2016']['OF']['10m3']['k12'] = [-1.444,0.995,-0.070]
params['A2016']['OF']['10m3']['k8']  = [-1.484,0.994,-0.061]
params['A2016']['OF']['10m3']['k6']  = [-1.525,0.993,-0.052]
params['A2016']['OF']['10m3']['k4']  = [-1.613,0.990,-0.026]
params['A2016']['OF']['10m2'] = {}
params['A2016']['OF']['10m2']['bM']  = [-1.371,0.980,-0.049]
params['A2016']['OF']['10m2']['k12'] = [-1.440,0.979,-0.034]
params['A2016']['OF']['10m2']['k8']  = [-1.477,0.978,-0.024]
params['A2016']['OF']['10m2']['k6']  = [-1.514,0.976,-0.012]
params['A2016']['OF']['10m2']['k4']  = [-1.594,0.973, 0.017]

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

#see Lazar et al. 2015
@njit
def th_func_2(bparl,a,b,c,d):
    return 1. + (a/bparl**b)*(1.+c/bparl**d)

#-------------------------------------------------------
#plot function
#-------------
def fluct_plot_func_masked( ax, xdata, ydata, mask, field, field_name, species_vars,
                            xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                            legend_flag=False, params=params, run_label=run_label,
                            mode=mode,logscale_flg=logscale_flg,vmin=None,vmax=None,
                            h_old=None, c_pass=0, n_pass=1 ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    mask         -> numpy.ndarray(nx,ny) - island mask
    field        -> numpy.ndarray(nx,ny) - field
    field_name   -> string - name of the field
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] or [array,array] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    params       -> [optional, set by default] dict -
                    dict containing all thresholds parameters
    run_label    -> [optional, set by default] string - run label
    mode         -> [optional, set by default] string - 'ABC' or 'ABCmax' or 'standard'
    logscale_flg -> [optional, set by default] boolean - if colormap is logscaled
    vmin         -> float or None - minimum value in the colorbar
    vmax         -> float or none - maximum value in the colorbar
    h_old        -> [optional] None or np.array - default=None - 
                    where histograms are cumulated
    c_pass       -> [optional] int - default=0 - passage index
    n_pass       -> [optional] int - default=1 - number of cumulating passages

    OUTPUTS:
    if c_pass < n_pass-1:
        h            -> np.array - cumulated 2D histogram
    if c_pass == n_pass-1:
        handles      -> [lines.Line2D, ...] - list of handles for the legend
    if ERROR:
        -1           -> int - ERROR exit
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    if xdata_range == None:
        xdata_range = [np.min(xdata),np.max(xdata)]
    if ydata_range == None:
        ydata_range = [np.min(ydata),np.max(ydata)]

    xdata_masked = xdata[mask]
    ydata_masked = ydata[mask]
    field_masked = field[mask]
    h,xbin,ybin = h2dw(np.log10(xdata_masked),np.log10(ydata_masked),field_masked,nbins_xy=n_bins,
                       x_range=list(np.log10(xdata_range)),y_range=list(np.log10(ydata_range)),
                       mode=mode)
    del field_masked,xdata_masked,ydata_masked
    if type(h_old) != type(None):
        if h_old.shape != h.shape:
            print('\nh_old.shape=%s is different from h.shape=%s\n'%(str(h_old.shape),str(h.shape)))
            return -1
        h = h + h_old
    if c_pass == n_pass-1:
        if vmin == None:
            vmin = np.min(h)
        if vmax == None:
            vmax = np.max(h)
        tmp_xbin = np.power(10.,xbin)
        tmp_ybin = np.power(10.,ybin)
        X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
        h = np.divide(h[n_bins[0]:,:],h[:n_bins[0],:])
        if logscale_flg:
            im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())#vmin=vmin,vmax=vmax))
        else:
            im = ax.pcolormesh(X,Y,h.T,vmin=vmin,vmax=vmax)
        del X,Y,h

        xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),len(xbin)-1)
        del xbin,ybin,tmp_xbin,tmp_ybin

        if species_tag == 'p':
            p = params['H2006']['PC']['10m3']['bM'] # ProtonCyclotron - bi-Maxwellian
            lC, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label="PCI")

        p = params['H2006']['MR']['10m3']['bM'] # Mirror - bi-Maxwellian
        lM, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label="MI")

        if species_tag == 'e':
            p = params['L2015']['EC']['e05']['10m2']['bM']['T'] # ElectronCyclotron - bi-Maxwellian
            lC, = ax.plot(xvals,th_func_2(xvals,p[0],p[1],p[2],p[3]),'-k',label="ECI")

        p = params['A2016']['PF']['10m2']['bM'] # ParallelFirehose - bi-Maxwellian
        lP, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label="PFHI")

        p = params['A2016']['OF']['10m2']['bM'] # ObliqueFirehose - bi-Maxwellian
        lO, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label="OFHI")

        p = params['A2016']['OF']['10m2']['k4'] # ObliqueFirehose - bi-kappa
        lOk, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label="OFHI $\kappa=4$")

        lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',label='$\\beta_{\parallel}^{-1}$') # CGL expansion
        
        ax.set_xlabel('$\\beta_{\parallel,%s}$'%(species_tag))
        ax.set_ylabel('$p_{\perp,%s}/p_{\parallel,%s}$'%(species_tag,species_tag))
        ax.set_xlim(xdata_range[0],xdata_range[1])
        ax.set_ylim(ydata_range[0],ydata_range[1])
        if legend_flag:
            ax.legend()
        title_str = run_label+' - '+species_name+' - '+field_name
        if t != None:
            title_str += ' - $t = %s$'%(t)
        ax.set_title(title_str)
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.colorbar(im,ax=ax)

        return [lC,lM,lP,lO,lOk,lb]
    elif c_pass < n_pass-1:
        return h
    else:
        print('\nc_pass=%d is greater than n_pass-1=%d\n'%(c_pass,n_pass-1))
        return -1

def first_step():
    hp = None
    he = None
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
    return fig,ax,hp,he

def second_step( ax,field,field_name,mask,p_vars,e_vars,w_ele,n_bins,
                 hp,he,c_pass,n_pass,bparlp,anisp,bparle,anise,
                 betap_lim=None,anisp_lim=None,betae_lim=None,anise_lim=None,
                 vminp=None,vmaxp=None,vmine=None,vmaxe=None ):
    hp = fluct_plot_func_masked(ax[0],bparlp,anisp,mask,field,field_name,p_vars,
                                xdata_range=betap_lim,ydata_range=anisp_lim,n_bins=[n_bins,n_bins],
                                vmin=vminp,vmax=vmaxp,h_old=hp,c_pass=c_pass,n_pass=n_pass)
    if type(hp) == np.int:
        if hp == -1:
            print('\nSomething went wrong in windlike_plot_func (protons)\n')
#
    if w_ele:
        he = fluct_plot_func_masked(ax[1],bparle,anise,mask,field,field_name,e_vars,
                                    xdata_range=betae_lim,ydata_range=anise_lim,n_bins=[n_bins,n_bins],
                                    vmin=vmine,vmax=vmaxe,h_old=he,c_pass=c_pass,n_pass=n_pass)
        if type(he) == np.int:
            if he == -1:
                print('\nSomething went wrong in windlike_plot_func (electrons)\n')
    return hp,he

def third_step( fig,ax,w_ele,hp,n_bins,field_label,
                logscale_flg=logscale_flg,opath=opath,out_dir=out_dir,time_avg_flg=B0_as_time_avg,
                run_name=run_name,ind1=ind1,ind2=ind2,mask_name=mask_name,mode=mode ):
    if not w_ele:
        he = hp[:]
        he[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [hp[0],#[lC,lM,lP,lO,lOk,lb]
                   he[0],
                   hp[1],
                   hp[2],
                   hp[3],
                   hp[4],
                   hp[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
    #
    fig.tight_layout()
    title = opath+'/'+out_dir+'/'+'fluct_in_wind_multitimes_'+mask_name+'_'+mode
    if logscale_flg:
        title += '_logscale'
    title += '_'+run_name+'_'+field_label
    if time_avg_flg:
        title += '_B0timeavg'
    title += '_'+str(ind1)+'-'+str(ind2)+'_b'+str(n_bins)
    fig.savefig(title+'.png')
    plt.close(fig)
    return

#-------------------------
#B0
if B0_as_time_avg:
    #as time average
    #---> loop over times <---
    cnt = 0.

    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        _,B = run.get_EB(ind)
        if ind == ind1:
            Beq = B
        else:
            Beq += B
        cnt += 1.

    Beq /= cnt
else:
    #as initial field
    if code_name == 'HVM':
        Bx0 = 1.
        Bz0 = 0.25
    elif code_name == 'iPIC':
        Bx0 = 0.01
        Bz0 = 0.25*0.01
    A_1 = - Bx0
    A_2 = - A_1

    _,Y = np.meshgrid(x,y)
    Y = Y.T
    Bxeq = ( - A_1*np.tanh( (Y - y1)/L1 )
             - A_2*np.tanh( (Y - y2)/L2 )
             - np.sign(A_1)*(A_1 - A_2)*0.5 )
    del Y

    Beq = np.array([Bxeq,np.zeros((nx,ny),dtype=type(B[0,0,0,0])),
                   np.full((nx,ny),Bz0,dtype=type(B[0,0,0,0]))]).reshape(3,nx,ny,1)
    del Bxeq

Beq2 = Beq[0]*Beq[0] + Beq[1]*Beq[1] + Beq[2]*Beq[2]
#---> loop over times <---
n_bins = 25*int(round(np.sqrt(ind2-ind1+1)))
        #50
ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
n_pass = len(ind_range)

plt.close('all')

field_names = {'curv_b':'$|\hat{b}\cdot\\nabla\hat{b}|\;[d_i^{-1}]$',
               'fluct_B':'$\mathrm{d}B/B_{\mathrm{eq}}\;[d_i^{-1}]$',
               'fluct_Bperp':'$\mathrm{d}B_{\perp}/B_{\mathrm{eq}}\;[d_i^{-1}]$',
               'anis_dB':'$\mathrm{d}B_{\parallel}/\mathrm{d}B_{\perp}\;[d_i^{-1}]$'}
fg_dict = {}
ax_dict = {}
hp_dict = {}
he_dict = {}
keys = list(field_names.keys())
for k in keys:
    fg_dict[k],ax_dict[k],hp_dict[k],he_dict[k] = first_step()

fields = {}
v = {'min':{'p':{},'e':{}},
     'max':{'p':{},'e':{}}}
for k in keys:
    v['min']['p'][k] = None
    v['max']['p'][k] = None
    v['min']['e'][k] = None
    v['max']['e'][k] = None

print " ",
for c_pass,ind in enumerate(ind_range):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            n_p,_ = run.get_Ion(ind)
            pparle,pperpe = run.get_Te(ind)
            pparle *= n_p
            pperpe *= n_p
            del n_p
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe
    else:
        anise = None
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')
        del pparle
    else:
        bparle = None
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #fluctuations
    dB = B - Beq
#
    b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    for i in range(3):
        b[i] = B[i]/np.sqrt(B2)
    del B2,B
    dBparl = np.abs(dB[0]*b[0] + dB[1]*b[1] + dB[2]*b[2])
    dB2 = dB[0]*dB[0] + dB[1]*dB[1] + dB[2]*dB[2]
    dBperp = np.sqrt(dB2 - dBparl*dBparl)
#
    dxbx = calc.calc_gradx(b[0])
    dybx = calc.calc_grady(b[0])
    dxby = calc.calc_gradx(b[1])
    dyby = calc.calc_grady(b[1])
    dxbz = calc.calc_gradx(b[2])
    dybz = calc.calc_grady(b[2])
    b_Nb = np.array([b[0]*dxbx+b[1]*dybx, b[0]*dxby + b[1]*dyby, b[0]*dxbz + b[1]*dybz])
    del b,dxbx,dybx,dxby,dyby,dxbz,dybz
    fields['curv_b'] = np.sqrt(b_Nb[0]**2 + b_Nb[1]**2 + b_Nb[2]**2)###
    del b_Nb
#
    fields['fluct_B'] = np.divide(np.sqrt(dB2),np.sqrt(Beq2))###
    fields['fluct_Bperp'] = np.divide(dBperp,np.sqrt(Beq2))###
    fields['anis_dB'] = np.divide(dBparl,dBperp)###
    del dB2,dBperp,dBparl
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    for k in keys:
        hp_dict[k],he_dict[k] = second_step(ax_dict[k],fields[k],field_names[k],
                                            mask,p_vars,e_vars,w_ele,n_bins,
                                            hp_dict[k],he_dict[k],c_pass,n_pass,
                                            bparlp,anisp,bparle,anise,
                                            betap_lim=betap_lim,anisp_lim=anisp_lim,
                                            betae_lim=betae_lim,anise_lim=anise_lim,
                                            vminp=v['min']['p'][k],vmaxp=v['max']['p'][k],
                                            vmine=v['min']['e'][k],vmaxe=v['max']['e'][k])
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

for k in keys:
    third_step(fg_dict[k],ax_dict[k],w_ele,hp_dict[k],n_bins,k)
