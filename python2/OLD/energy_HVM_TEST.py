#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot directional kinetic energy per species\n')


ipath = '/work1/califano/HVM2D/LF_2d_DH'
spath = '00'
run = from_HVM(ipath)
run.get_meta(spath)
calc = fibo('calc')
calc.meta = run.meta
run.get_EB(300.,fibo_obj=calc)
run.get_Ion(300.,fibo_obj=calc)
run.get_Press(300.,fibo_obj=calc)
run.get_Te(300.,fibo_obj=calc)
run.calc_all('%08.3f'%300.,fibo_obj=calc,list_terms=['mom_2'])



#------------------------------------------------------
#Init
#----
input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run8.dat']
master_in = 0

F = {'E':{},'M':{}}
K = {'p':{},'e':{},'parlp':{},'perpp':{},'parle':{},'perpe':{}}
I = {'p':{},'e':{},'parlp':{},'perpp':{},'parle':{},'perpe':{}}
t = {}
E0 = {}
Etot = {}

run_name_vec = []
run_labels = {}
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind1,ind2,ind_step = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    code_name = run.meta['code_name']
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    m_p = 1.
    m_e = m_p/run.meta['mime']
    if code_name == 'iPIC':
        #run_label = 'iPIC'
        run_label = 'iPIC%s'%(run_name.split('run')[1].split('_')[0])
        qom_e = run.meta['msQOM'][0]
    elif code_name == 'HVM':
        if w_ele:
            run_label = 'HVLF'
        else:
            run_label = 'HVM'
    else:
        run_label = 'unknown'
    run_labels[run_name] = run_label
    if ii == master_in:
        opath = run.meta['opath']
        out_dir = 'energy_budget_fullbox'
        ml.create_path(opath+'/'+out_dir)
        calc = fibo('calc')
#
    for k in F.keys():
        F[k][run_name] = []
    for k in K.keys():
        K[k][run_name] = []
    for k in I.keys():
        I[k][run_name] = []
    t[run_name] = []
    Etot[run_name] = []
# 
    #---> loop over times <---
    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        #get data
        #magnetic fields and co.
        E,B = run.get_EB(ind)
        E2 = E[0]*E[0] + E[1]*E[1] + E[2]*E[2]
        B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
        Bn = np.sqrt(B2)
        b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
        for i in range(3):
            b[i] = np.divide(B[i],Bn)
        del Bn,E
#
        #densities and currents
        n_p,u_p = run.get_Ion(ind)
        if code_name == 'HVM':
            calc.meta=run.meta
            cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
            J = np.array([cx,cy,cz]) # J = rot(B)
            del cx,cy,cz
            n_e = n_p.copy() # n_e = n_p = n (no charge separation)
            u_e = np.empty(u_p.shape,dtype=type(u_p[0,0,0,0])) # u_e = u_p - J/n
            u_e[0] = u_p[0] - np.divide(J[0],n_p)
            u_e[1] = u_p[1] - np.divide(J[1],n_p)
            u_e[2] = u_p[2] - np.divide(J[2],n_p)
            del J
        elif code_name == 'iPIC':
            n_e,u_e = run.get_Ion(ind,qom=qom_e)
        u_p2 = u_p[0]*u_p[0] + u_p[1]*u_p[1] + u_p[2]*u_p[2]
        u_e2 = u_e[0]*u_e[0] + u_e[1]*u_e[1] + u_e[2]*u_e[2]
        u_parlp2 = (u_p[0]*b[0] + u_p[1]*b[1] + u_p[2]*b[2])**2
        u_perpp2 = u_p2 - u_parlp2
        u_parle2 = (u_e[0]*b[0] + u_e[1]*b[1] + u_e[2]*b[2])**2
        u_perpe2 = u_e2 - u_parle2
        del u_p,u_e,b
#
        #Pressures
        Pp = run.get_Press(ind)
        pparlp,pperpp = ml.proj_tens_on_vec(Pp,B) # pparlp = Pp:bb
#                                                 # pperpp = Pp:(id - bb)/2
        Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3. # Pisop = Tr(Pp)/3
        del Pp
        if not w_ele:
            Pisoe = n_e*run.meta['beta']*0.5*run.meta['teti'] # Pisoe = n_e * T_e,0
            pparle = Pisoe # ppalre = Pisoe
            pperpe = pparle # pperpe = Pisoe
        else:
            if code_name == 'HVM':
                tparle,tperpe = run.get_Te(ind)
                pparle = tparle*n_e # pparle = n_e*tparle
                pperpe = tperpe*n_e # pperpe = n_e*tperpe
                del tparle,tperpe
                Pisoe = (pparle + 2.*pperpe)/3. # Pisoe  = (pparle + 2*pperpe)/3
            elif code_name == 'iPIC':
                Pe = run.get_Press(ind,qom=qom_e)
                pparle,pperpe = ml.proj_tens_on_vec(Pe,B) # pparle = Pe:bb
                                                          # pperpe = Pe:(id - bb)/2
                Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.  # Pisoe = Tr(Pe)/3
                del Pe
        del B
#
        #energies
        if code_name == 'iPIC':
            Fmult = 1./(4.*np.pi)
        elif code_name == 'HVM':
            Fmult = 1.
        else:
            print('\nERROR: code name unknown!')
            sys.exit()
        F['E'][run_name].append(np.mean(0.5*E2*Fmult))
        F['M'][run_name].append(np.mean(0.5*B2*Fmult))
        K['p'][run_name].append(np.mean( 0.5*m_p*n_p*
                                         u_p2) )
        K['e'][run_name].append(np.mean( 0.5*m_e*n_e*
                                         u_e2) )
        K['parlp'][run_name].append(np.mean( 0.5*m_p*n_p*
                                             u_parlp2) )
        K['perpp'][run_name].append(np.mean( 0.5*m_p*n_p*
                                             u_perpp2) )
        K['parle'][run_name].append(np.mean( 0.5*m_e*n_e*
                                             u_parle2) )
        K['perpe'][run_name].append(np.mean( 0.5*m_e*n_e*
                                             u_perpe2) )
        I['p'][run_name].append(np.mean(1.5*Pisop))
        I['e'][run_name].append(np.mean(1.5*Pisoe))
        I['parlp'][run_name].append(np.mean(0.5*pparlp))
        I['perpp'][run_name].append(np.mean(1.0*pperpp))
        I['parle'][run_name].append(np.mean(0.5*pparle))
        I['perpe'][run_name].append(np.mean(1.0*pperpe))
        Etot[run_name].append( F['M'][run_name][-1] + F['E'][run_name][-1] +
                               K['p'][run_name][-1] + K['e'][run_name][-1] +
                               I['p'][run_name][-1] + I['e'][run_name][-1] )
        t[run_name].append(times[ind])
    #---> loop over time <---
        print "\r",
        print run_name," t = ",times[ind],m_p,m_e,
        gc.collect()
        sys.stdout.flush()
    #------------------------
#
    #initial budget
    E0[run_name] = ( F['M'][run_name][0] + F['E'][run_name][0] +
                     K['p'][run_name][0] + K['e'][run_name][0] +
                     I['p'][run_name][0] + I['e'][run_name][0] )

#
    #make inito ndarray
    F['E'][run_name]     = np.array(F['E'][run_name])
    F['M'][run_name]     = np.array(F['M'][run_name])
    K['p'][run_name]     = np.array(K['p'][run_name])
    K['e'][run_name]     = np.array(K['e'][run_name])
    K['parlp'][run_name] = np.array(K['parlp'][run_name])
    K['perpp'][run_name] = np.array(K['perpp'][run_name])
    K['parle'][run_name] = np.array(K['parle'][run_name])
    K['perpe'][run_name] = np.array(K['perpe'][run_name])
    I['p'][run_name]     = np.array(I['p'][run_name])
    I['e'][run_name]     = np.array(I['e'][run_name])
    I['parlp'][run_name] = np.array(I['parlp'][run_name])
    I['perpp'][run_name] = np.array(I['perpp'][run_name])
    I['parle'][run_name] = np.array(I['parle'][run_name])
    I['perpe'][run_name] = np.array(I['perpe'][run_name])
    Etot[run_name]       = np.array(Etot[run_name])
    t[run_name]          = np.array(t[run_name])

#----------------
#PLOT TIME!!!!!!!
#----------------
#functions
def get_ax(ax,i,j):
    if type(ax) != np.ndarray:
        return ax
    if len(ax.shape) == 2:
        return ax[i,j]
    else:
        return ax[i+j]

#PLOT F,K,I
#inputs
nrow = 3
ncol = 2
sharex = True # True, False, 'col'
sharey = False # True, False, 'row'
title = 'Energy budget distribution'
xlabels = [['dummy',                   'dummy'                   ],
           ['dummy',                   'dummy'                   ],
           ['$t\quad [\Omega_c^{-1}]$','$t\quad [\Omega_c^{-1}]$']]
ylabels = [['$\Delta E_{m.f.}/E_0$','$\Delta E_{e.f.}/E_0$'   ],
           ['$\Delta K_p/E_0$',     '$\Delta K_e/E_0$'        ],
           ['$\Delta I_p/E_0$',     '$\Delta I_e/E_0$'        ]]

#axes
plt.close('all')
fig = plt.figure(1,figsize=(12,12))
ax = fig.subplots(nrow,ncol,sharex=sharex,sharey=sharey)

#legend
ax_ = get_ax(ax,0,0)
lines = {}
for run_name in run_name_vec:
    lin, = ax_.plot([],[],linestyle='-')
    lines[run_labels[run_name]] = lin

fig.legend(lines.values(),lines.keys(),
        ncol=3,mode="expand",borderaxespad=0.,framealpha=0.,
        bbox_to_anchor=(0.125,0.9,0.775,0.9),loc='lower left')

#plots
ax_ = get_ax(ax,0,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(F['M'][run_name] - F['M'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,0,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(F['E'][run_name] - F['E'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,1,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(K['p'][run_name] - K['p'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,1,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(K['e'][run_name] - K['e'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,2,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(I['p'][run_name] - I['p'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,2,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(I['e'][run_name] - I['e'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

#ticks and labels
if sharex != False:
    top_ext = 0.05
else:
    top_ext = 0.

fig.subplots_adjust(hspace=.0,wspace=.0,top=0.85+top_ext)#,right=0.9,left=0.125,bottom=0.1)
for (i,j) in [(i,j) for i in range(nrow) for j in range(ncol)]:
    ax_ = get_ax(ax,i,j)
    ax_.tick_params(bottom=True,top=True,left=True,right=True,
            direction='in',labelbottom=False,labeltop=False,
            labelleft=False,labelright=False)
    if j == 0:
        ax_.tick_params(labelleft=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label1.set_visible(False)
    if i == nrow-1:
        ax_.tick_params(labelbottom=True)
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label1.set_visible(False)
    if (j == ncol-1) and (sharey == False):
        ax_.tick_params(labelright=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.yaxis.set_label_position("right")
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label2.set_visible(False)
    if (i == 0) and (sharex == False):
        ax_.tick_params(labeltop=True)
        ax_.xaxis.set_label_position("top")
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label2.set_visible(False)

fig.suptitle(title)

#draw
fname = opath+'/'+out_dir+'/'+'energy_budget'
for lab in run_labels.values():
    fname = fname+'_%s'%(lab)

plt.savefig(fname+'.png')
#plt.show(1)
plt.close(1)

#PLOT Kparl,Kperp,Iparl,Iperp
#inputs
nrow = 4
ncol = 2
figsize = (12,16)
sharex = True # True, False, 'col'
sharey = False # True, False, 'row'
#labels = run_labels.values()
#linestyles = ['-','-','-']
title = 'Energy distribution in prallel/perpendicular directions'
xlabels = [['dummy',                   'dummy'                   ],
           ['dummy',                   'dummy'                   ],
           ['dummy',                   'dummy'                   ],
           ['$t\quad [\Omega_c^{-1}]$','$t\quad [\Omega_c^{-1}]$']]
ylabels = [['$\Delta K_{\parallel,p}/E_0$','$\Delta K_{\parallel,e}/E_0$'],
           ['$\Delta K_{\perp,p}/E_0$',    '$\Delta K_{\perp,e}/E_0$'    ],
           ['$\Delta I_{\parallel,p}/E_0$','$\Delta I_{\parallel,e}/E_0$'],
           ['$\Delta I_{\perp,p}/E_0$',    '$\Delta I_{\perp,e}/E_0$'    ]]

#axes
plt.close('all')
fig = plt.figure(1,figsize=figsize)
ax = fig.subplots(nrow,ncol,sharex=sharex,sharey=sharey)

#legend
ax_ = get_ax(ax,0,0)
lines = {}
for run_name in run_name_vec:
    lin, = ax_.plot([],[],linestyle='-')
    lines[run_labels[run_name]] = lin

fig.legend(lines.values(),lines.keys(),
        ncol=3,mode="expand",borderaxespad=0.,framealpha=0.,
        bbox_to_anchor=(0.125,0.9,0.775,0.9),loc='lower left')

#plots
ax_ = get_ax(ax,0,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(K['parlp'][run_name] - K['parlp'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,0,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(K['parle'][run_name] - K['parle'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,1,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(K['perpp'][run_name] - K['perpp'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,1,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(K['perpe'][run_name] - K['perpe'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,2,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(I['parlp'][run_name] - I['parlp'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,2,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(I['parle'][run_name] - I['parle'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,3,0)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(I['perpp'][run_name] - I['perpp'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

ax_ = get_ax(ax,3,1)
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(I['perpe'][run_name] - I['perpe'][run_name][0],E0[run_name])
    ax_.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax_.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax_.set_ylim(np.min(ymin_all),np.max(ymax_all))

#ticks and labels
if sharex != False:
    top_ext = 0.05
else:
    top_ext = 0.

fig.subplots_adjust(hspace=.0,wspace=.0,top=0.85+top_ext)
for (i,j) in [(i,j) for i in range(nrow) for j in range(ncol)]:
    ax_ = get_ax(ax,i,j)
    ax_.tick_params(bottom=True,top=True,left=True,right=True,
            direction='in',labelbottom=False,labeltop=False,
            labelleft=False,labelright=False)
    if j == 0:
        ax_.tick_params(labelleft=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label1.set_visible(False)
    if i == nrow-1:
        ax_.tick_params(labelbottom=True)
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label1.set_visible(False)
    if (j == ncol-1) and (sharey == False):
        ax_.tick_params(labelright=True)
        ax_.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter("%.1e"))
        ax_.yaxis.set_label_position("right")
        ax_.set_ylabel(ylabels[i][j])
        if i != 0:
            yticks = ax_.yaxis.get_major_ticks()
            yticks[-1].label2.set_visible(False)
    if (i == 0) and (sharex == False):
        ax_.tick_params(labeltop=True)
        ax_.xaxis.set_label_position("top")
        ax_.set_xlabel(xlabels[i][j])
        if j != ncol-1:
            xticks = ax_.xaxis.get_major_ticks()
            xticks[-1].label2.set_visible(False)

fig.suptitle(title)

#draw
fname = opath+'/'+out_dir+'/'+'energy_parlperp'
for lab in run_labels.values():
    fname = fname+'_%s'%(lab)

plt.savefig(fname+'.png')
#plt.show(1)
plt.close(1)

#PLOT E_tot
#init
plt.close()
fig,ax = plt.subplots(1,1,figsize=(12,10))

#legend
lines = {}
for run_name in run_name_vec:
    lin, = ax.plot([],[],linestyle='-')
    lines[run_labels[run_name]] = lin

ax.legend(lines.values(),lines.keys(),
        ncol=3,framealpha=0.,loc='best')

#plot
xmin_all = []; xmax_all = []; ymin_all = []; ymax_all = []
for run_name,lab in zip(run_labels.keys(),run_labels.values()):#run_name_vec,labels):
    XDATA = t[run_name]
    YDATA = np.divide(Etot[run_name] - Etot[run_name][0],E0[run_name])
    ax.plot(XDATA,YDATA,color=lines[lab].get_color(),
             linestyle=lines[lab].get_linestyle())
    xmin_all.append(np.min(XDATA))
    xmax_all.append(np.max(XDATA))
    ymin_all.append(np.min(YDATA))
    ymax_all.append(np.max(YDATA))

ax.set_xlim(np.min(xmin_all),np.max(xmax_all))
ax.set_ylim(np.min(ymin_all),np.max(ymax_all))

#labels
ax.set_xlabel('$t\quad [\Omega_c^{-1}]$')
ax.set_ylabel('$\Delta E_{tot}/E_0$')

fig.suptitle('Percentual variations of the total energy')

#draw
fname = opath+'/'+out_dir+'/'+'energy_tot'
for lab in run_labels.values():
    fname = fname+'_%s'%(lab)

plt.savefig(fname+'.png')
#plt.show()
plt.close()
