import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.signal as scisi

import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

ipath = '/work1/califano/HVM2D/LF_2d_DH'
run = from_HVM(ipath)
run.get_meta()

nx = run.meta['nx']
dx = run.meta['dx']
x = run.meta['x']
y = run.meta['y']


ind = 295.
_,B = run.get_EB(ind)
Psi = wl.psi_2d(B[0:2,:,:,0],run.meta)

y0 = 85
yr = 40
ymax = 170
data = B[0,:,y0-yr:y0+yr+1,0].copy()
data = np.pad(data,((nx//2,nx//2),(0,0)),mode='wrap')
for i in range(2*yr+1):
    data[:,i] = data[:,i] - np.mean(data[:,i])

fs = 1./dx
nw = int(2**round(np.log2(float(nx)/64.)))
wname = 'hann'

k_,x_,Zxx = scisi.stft(data,fs=fs,window=wname,nperseg=nw,detrend='linear',axis=0,boundary='even',padded=False)
nx_ = len(x_)

mZxx2 = np.mean(np.abs(Zxx)**2.,axis=1)

plt.close()
fig,ax = plt.subplots(2,1,figsize=(12,8))

im1 = ax[0].contourf(x,y[0:ymax+1],B[0,:,0:ymax+1,0].T,64)
ax[0].contour(x,y[0:ymax+1],Psi[:,0:ymax+1].T,8,cmap='inferno')
plt.colorbar(im1,ax=ax[0])
ax[0].axhline(y=y[y0-yr],linestyle='--',color='k')
ax[0].axhline(y=y[y0+yr],linestyle='--',color='k')

im2 = ax[1].contourf(x_-x[nx//2],k_[1:],mZxx2[1:,:],64,locator=mpl.ticker.LogLocator())
plt.colorbar(im2,ax=ax[1])
ax[1].set_xlim(x[0],x[-1])
ax[1].set_yscale('log')

plt.show()
