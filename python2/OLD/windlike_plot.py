#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta for one time.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

run_name = run.meta['run_name']
out_dir = 'windlike_plot_NEW'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']
if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
if ans:
    betap_lim = input('betap limits ([x0,x1]): ')
    anisp_lim = input('anisp limits ([y0,y1]): ')
    betae_lim = input('betae limits ([x0,x1]): ')
    anise_lim = input('anise limits ([y0,y1]): ')
else:
    betap_lim = None
    anisp_lim = None
    betae_lim = None
    anise_lim = None

wl.Can_I()

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = False
smooth_flag_post = True
gfs = 2

#see Hellinger et al. 2006
#    Astfalk and Jenko 2016
params = {}
params['H2006'] = {}
params['A2016'] = {}
params['L2015'] = {}

params['H2006']['PC'] = {}
params['H2006']['PC']['10m3'] = {}
params['H2006']['PC']['10m3']['bM'] = [ 0.43,0.42,-0.0004]

params['L2015']['EC'] = {}
params['L2015']['EC']['e05'] = {}
params['L2015']['EC']['e05']['10m2'] = {}
params['L2015']['EC']['e05']['10m2']['k2'] = {}
params['L2015']['EC']['e05']['10m2']['k2']['T'] = [0.54,0.21,0.14,1.28]
params['L2015']['EC']['e05']['10m2']['k2']['C'] = [0.26,-0.01,1.18,0.56]
params['L2015']['EC']['e05']['10m2']['k6'] = {}
params['L2015']['EC']['e05']['10m2']['k6']['T'] = [0.43,0.17,0.02,2.04]
params['L2015']['EC']['e05']['10m2']['k6']['C'] = [0.19,-0.09,1.11,0.63]
params['L2015']['EC']['e05']['10m2']['bM'] = {}
params['L2015']['EC']['e05']['10m2']['bM']['T'] = [0.40,0.10,0.05,1.26]
params['L2015']['EC']['e05']['10m2']['bM']['C'] = [0.17,-0.11,1.18,0.65]
params['L2015']['EC']['e05']['10m3'] = {}
params['L2015']['EC']['e05']['10m3']['k2'] = {}
params['L2015']['EC']['e05']['10m3']['k2']['T'] = [0.10,0.30,0.09,1.22]
params['L2015']['EC']['e05']['10m3']['k2']['C'] = [0.01,-0.46,19.1,0.97]
params['L2015']['EC']['e05']['10m3']['k6'] = {}
params['L2015']['EC']['e05']['10m3']['k6']['T'] = [0.08,0.38,0.003,2.54]
params['L2015']['EC']['e05']['10m3']['k6']['C'] = [0.005,-0.52,14.6,1.07]
params['L2015']['EC']['e05']['10m3']['bM'] = {}
params['L2015']['EC']['e05']['10m3']['bM']['T'] = [0.07,0.31,0.02,1.23]
params['L2015']['EC']['e05']['10m3']['bM']['C'] = [0.003,-0.67,21.3,1.23]
params['L2015']['EC']['e01'] = {}
params['L2015']['EC']['e01']['10m2'] = {}
params['L2015']['EC']['e01']['10m2']['k2'] = {}
params['L2015']['EC']['e01']['10m2']['k2']['T'] = [1.74,0.05,0.04,1.29]
params['L2015']['EC']['e01']['10m2']['k2']['C'] = [1.35,-0.04,0.30,0.60]
params['L2015']['EC']['e01']['10m2']['k6'] = {}
params['L2015']['EC']['e01']['10m2']['k6']['T'] = [1.54,0.06,0.003,1.86]
params['L2015']['EC']['e01']['10m2']['k6']['C'] = [1.27,-0.03,0.16,0.62]
params['L2015']['EC']['e01']['10m2']['bM'] = {}
params['L2015']['EC']['e01']['10m2']['bM']['T'] = [1.47,0.05,0.002,2.03]
params['L2015']['EC']['e01']['10m2']['bM']['C'] = [1.23,-0.03,0.14,0.63]
params['L2015']['EC']['e01']['10m3'] = {}
params['L2015']['EC']['e01']['10m3']['k2'] = {}
params['L2015']['EC']['e01']['10m3']['k2']['T'] = [0.21,0.12,0.04,1.28]
params['L2015']['EC']['e01']['10m3']['k2']['C'] = [0.11,-0.07,0.84,0.60]
params['L2015']['EC']['e01']['10m3']['k6'] = {}
params['L2015']['EC']['e01']['10m3']['k6']['T'] = [0.18,0.14,0.001,2.21]
params['L2015']['EC']['e01']['10m3']['k6']['C'] = [0.11,-0.06,0.47,0.63]
params['L2015']['EC']['e01']['10m3']['bM'] = {}
params['L2015']['EC']['e01']['10m3']['bM']['T'] = [0.17,0.15,0.0001,2.83]
params['L2015']['EC']['e01']['10m3']['bM']['C'] = [0.11,-0.05,0.40,0.64]

params['H2006']['MR'] = {}
params['H2006']['MR']['10m3'] = {}
params['H2006']['MR']['10m3']['bM'] = [ 0.77,0.76,-0.0160]

params['H2006']['PF'] = {}
params['H2006']['PF']['10m3'] = {}
params['H2006']['PF']['10m3']['bM'] = [-0.47,0.53, 0.5900]

params['H2006']['OF'] = {}
params['H2006']['OF']['10m3'] = {}
params['H2006']['OF']['10m3']['bM'] = [-1.40,1.00,-0.1100]

params['A2016']['PF'] = {}
params['A2016']['PF']['10m3'] = {}
params['A2016']['PF']['10m3']['bM']  = [-0.487,0.537, 0.560]
params['A2016']['PF']['10m3']['k12'] = [-0.438,0.475, 0.503]
params['A2016']['PF']['10m3']['k8']  = [-0.429,0.486, 0.423]
params['A2016']['PF']['10m3']['k6']  = [-0.417,0.498, 0.350]
params['A2016']['PF']['10m3']['k4']  = [-0.387,0.518, 0.226]
params['A2016']['PF']['10m3']['k2']  = [-0.274,0.536, 0.042]
params['A2016']['PF']['10m2'] = {}
params['A2016']['PF']['10m2']['bM']  = [-0.701,0.623, 0.599]
params['A2016']['PF']['10m2']['k12'] = [-0.656,0.596, 0.567]
params['A2016']['PF']['10m2']['k8']  = [-0.623,0.579, 0.569]
params['A2016']['PF']['10m2']['k6']  = [-0.625,0.585, 0.501]
params['A2016']['PF']['10m2']['k4']  = [-0.625,0.593, 0.379]
params['A2016']['PF']['10m2']['k2']  = [-0.632,0.589, 0.139]
params['A2016']['PF']['10m1'] = {}
params['A2016']['PF']['10m1']['bM']  = [-0.872,0.495, 1.233]
params['A2016']['PF']['10m1']['k12'] = [-0.899,0.502, 1.213]
params['A2016']['PF']['10m1']['k8']  = [-0.937,0.509, 1.097]
params['A2016']['PF']['10m1']['k6']  = [-0.947,0.505, 1.088]
params['A2016']['PF']['10m1']['k4']  = [-0.977,0.496, 1.068]
params['A2016']['PF']['10m1']['k2']  = [-1.230,0.464, 1.206]

params['A2016']['OF'] = {}
params['A2016']['OF']['10m3'] = {}
params['A2016']['OF']['10m3']['bM']  = [-1.371,0.996,-0.083]
params['A2016']['OF']['10m3']['k12'] = [-1.444,0.995,-0.070]
params['A2016']['OF']['10m3']['k8']  = [-1.484,0.994,-0.061]
params['A2016']['OF']['10m3']['k6']  = [-1.525,0.993,-0.052]
params['A2016']['OF']['10m3']['k4']  = [-1.613,0.990,-0.026]
params['A2016']['OF']['10m2'] = {}
params['A2016']['OF']['10m2']['bM']  = [-1.371,0.980,-0.049]
params['A2016']['OF']['10m2']['k12'] = [-1.440,0.979,-0.034]
params['A2016']['OF']['10m2']['k8']  = [-1.477,0.978,-0.024]
params['A2016']['OF']['10m2']['k6']  = [-1.514,0.976,-0.012]
params['A2016']['OF']['10m2']['k4']  = [-1.594,0.973, 0.017]

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

@njit
def th_func_2(bparl,a,b,c,d):
    return 1. + (a/bparl**b)*(1.+c/bparl**d)

#-------------------------------------------------------
#plot function
#-------------
def windlike_plot_func(ax,xdata_uncut,ydata_uncut,species_name,species_tag,b0_mult,xr=None,yr=None,t=None,params=params,run_label=run_label):
    xdata = np.concatenate((xdata_uncut[:,:cut1,:],xdata_uncut[:,cut2+1:,:]),axis=1)
    ydata = np.concatenate((ydata_uncut[:,:cut1,:],ydata_uncut[:,cut2+1:,:]),axis=1)
    h,xbin,ybin=np.histogram2d(np.log(np.array(xdata.flat)),np.log(np.array((ydata+1.).flat)),100)
    X,Y = np.meshgrid(np.exp(xbin),np.exp(ybin))
    im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())

    if xr == None:
        xr = [np.min(np.exp(xbin)),np.max(np.exp(xbin))]
    if yr == None:
        yr = [np.min(np.exp(ybin)),np.max(np.exp(ybin))]
    xvals = np.logspace(np.log10(xr[0]),np.log10(xr[1]),100)

    p = params['H2006']['PC']['10m3']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label='PC10m3bM')

    p = params['H2006']['MR']['10m3']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label='MR10m3bM')

    #p = params['A2016']['PF']['10m3']['bM']
    #ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),p[0],p[1],p[2]*b0_mult),'-r',label='PF10m3bM')

    #p = params['A2016']['PF']['10m3']['k4']
    #ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),p[0],p[1],p[2]*b0_mult),'--r',label='PF10m3k4')

    #p = params['A2016']['OF']['10m3']['bM']
    #ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),p[0],p[1],p[2]*b0_mult),'-c',label='OF10m3bM')

    #p = params['A2016']['OF']['10m3']['k4']
    #ax.plot(np.exp(xbin[:-1]),th_func(np.exp(xbin[:-1]),p[0],p[1],p[2]*b0_mult),'--c',label='OF10m3k4')

    p = params['A2016']['PF']['10m2']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label='PF10m2bM')

    p = params['A2016']['PF']['10m2']['k4']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--m',label='PF10m2k4')

    p = params['A2016']['OF']['10m2']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label='OF10m2bM')
    
    p = params['A2016']['OF']['10m2']['k4']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label='OF10m2k4')
    
    ax.set_ylim(np.exp(ybin[0]),np.exp(ybin[-1]));ax.legend()
    ax.set_xlabel('$\\beta_{\parallel,%s}$'%(species_tag))
    ax.set_ylabel('$P_{\perp,%s}/P_{\parallel,%s}$'%(species_tag,species_tag))
    if xr != None:
        ax.set_xlim(xr[0],xr[1])
    if yr != None:
        ax.set_ylim(yr[0],yr[1])
    if t != None:
        ax.set_title(run_label+' - '+species_name+' anisotropy - $t = %s$'%(t))
    else:
        ax.set_title(run_label+' - '+species_name+' anisotropy')
    ax.set_xscale('log');ax.set_yscale('log');plt.colorbar(im,ax=ax)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')
    Pp = run.get_Press(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3):
            for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[gfs,gfs],mode='wrap')
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp) - 1.
    if code_name == 'iPIC' and smooth_flag_post:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfs,gfs],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            if code_name == 'iPIC' and smooth_flag:
                for i in range(3):
                    for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[gfs,gfs],mode='wrap')
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        if code_name == 'iPIC' and smooth_flag_post:
            anise[:,:,0] = gf(anise[:,:,0],[gfs,gfs],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag_post:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfs,gfs],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag_post:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfs,gfs],mode='wrap')
        del pparle
    del B2
    del B
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    plt.close()
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig,ax = plt.subplots(1,nplot,figsize=(10*nplot,8))
#
    if w_ele:
        ax0 = ax[0]
    else:
        ax0 = ax
    windlike_plot_func(ax0,bparlp,anisp,'Protons CS1','p',1.,xr=betap_lim,yr=anisp_lim,t=time[ind])
#
    if w_ele:
        windlike_plot_func(ax[1],bparle,anise,'Electrons CS1','e',teti,xr=betae_lim,yr=anise_lim,t=time[ind])
#
    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'anis_windlike_'+run_name+'_'+str(ind)+'.png')
    plt.close()
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
