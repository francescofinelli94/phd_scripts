#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)


#------------------------------------------------------
#Intent
#------
print('\nWill check enropies\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
out_dir = 'FirstMaxwellEq'+'/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

#params
g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#declare arrays
t_vec = []

m_diff = []
s_diff = []

#------------------
smooth_flag = False
smooth_flag_post = True
gfs = 2

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    E,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and (smooth_flag or smooth_flag_post):
        Psi = gf(Psi,[gfs,gfs],mode='wrap')

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
        J = np.array([cx,cy,cz]) # J = rot(B)
        del cx,cy,cz

    #densities
    n_p,_ = run.get_Ion(ind)
    if code_name == 'iPIC' and smooth_flag:
        n_p[:,:,0] = gf(n_p[:,:,0],[gfs,gfs],mode='wrap')
    if code_name == 'HVM':
        n_e = n_p.copy() # n_e = n_p = n (no charge separation)
    elif code_name == 'iPIC':
        n_e,_ = run.get_Ion(ind,qom=qom_e)
        if smooth_flag:
            n_e[:,:,0] = gf(n_e[:,:,0],[gfs,gfs],mode='wrap')

    #diff = nabla.E - a*(n_p - n_e) # PIC ? a = 4*pi : a = 1
    if code_name == 'iPIC':
        mult = 4.*np.pi
    elif code_name == 'HVM':
        mult = 1.
    else:
        print 'ERROR: code_name='+code_name+' is unknown!'
    diff_no_abs = calc.calc_divr(E[0],E[1],E[2]) - mult*(n_p - n_e)
    diff = np.abs(diff_no_abs)

    if code_name == 'iPIC' and smooth_flag_post:
        diff_no_abs[:,:,0] = gf(diff_no_abs[:,:,0],[gfs,gfs],mode='wrap')
        diff[:,:,0] = gf(diff[:,:,0],[gfs,gfs],mode='wrap')

    #plot 2D
    plt.close()
    fig,ax = plt.subplots(2,1,figsize=(10*1,4*2))

    im0 = ax[0].contourf(x[ixmin:ixmax],y[iymin:iymax],diff[ixmin:ixmax,iymin:iymax,0].T,32)
    plt.colorbar(im0,ax=ax[0])
    ax[0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
    ax[0].set_title(r'$|\nabla\cdot E-4\pi(n_p-n_e)|$ - '
                   +run_name.replace('_',' ')+' - t='+str(time[ind]))

    h_,b_,im1 = ax[1].hist(np.array(diff_no_abs[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    ax[1].set_ylim(yL,yH)
    ax[1].set_xlim(xL,xH)
    ax[1].set_xlabel('value')
    ax[1].set_ylabel('counts')

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'FirstMaxwellEq_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #save means and s.d.
    m_diff.append(np.nanmean(diff_no_abs))
    s_diff.append(np.nanmean((diff_no_abs-m_diff[-1])**2))

    #save times
    t_vec.append(time[ind])

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
del diff

#from list to numpy.array
t_vec = np.array(t_vec)

m_diff = np.array(m_diff)
s_diff = np.array(s_diff)

#plot
plt.close('all')
fig,ax = plt.subplots(1,1,figsize=(8,8))

ax.plot(t_vec,m_diff,'-k',label='mean')
ax.plot(t_vec,m_diff+s_diff,'--k',label='mean $\pm$ s.d.')
ax.plot(t_vec,m_diff-s_diff,'--k')
ax.legend()
ax.set_xlabel('$t\quad [\Omega_c^{-1}]$')
ax.set_ylabel(r'$\nabla\cdot E-4\pi(n_p-n_e)$')
ax.set_title(run_name.replace('_',' '))

plt.tight_layout()
#plt.show()
plt.savefig(opath+'/'+out_dir+'/'+'m_FirstMaxwellEq_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
