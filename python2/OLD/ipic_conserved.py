"""
Script to read and plot conserved quantities from iPic3D simulations.

Author: Federico Lavorenti

Modified by: Francesco Finelli - November 9 2021
"""

import numpy as np
import matplotlib.pyplot as plt


# species: 0 = electrons background
#	   1 = electrons current sheet 1
#	   2 = electrons current sheet 2
#	   3 = protons background
#	   4 = protons current sheet 1
#	   5 = protons current sheet 2

# path to data
simu_path = '/work2/finelli/DoubleHarris/run11/data0'
nrun=1

Ncycle = []
TotEnergy = []
TotMomentum = []
EleEnergy = []
MagEnergy = []
KinEnergy = []
KinEnergy0 = []
KinEnergy1 = []
KinEnergy2 = []
KinEnergy3 = []
KinEnergy4 = []
KinEnergy5 = []
BulEnergy0 = []
BulEnergy1 = []
BulEnergy2 = []
BulEnergy3 = []
BulEnergy4 = []
BulEnergy5 = []

for irun in range(0,nrun):

    if (irun==0): 
        sr=1
    else:
        sr=0

    file_path = simu_path + '/ConservedQuantities.txt'
    (Ncycle_1, TotEnergy_1, TotMomentum_1,
     EleEnergy_1, MagEnergy_1, KinEnergy_1,
     KinEnergy0_1, KinEnergy1_1, KinEnergy2_1,
     KinEnergy3_1, KinEnergy4_1, KinEnergy5_1,
     BulEnergy0_1, BulEnergy1_1, BulEnergy2_1,
     BulEnergy3_1, BulEnergy4_1, BulEnergy5_1) = np.loadtxt(file_path,
                                                            unpack='True',
                                                            skiprows=sr)

    Ncycle = np.concatenate((Ncycle,Ncycle_1))
    TotEnergy = np.concatenate((TotEnergy,TotEnergy_1))
    TotMomentum = np.concatenate((TotMomentum,TotMomentum_1))
    EleEnergy = np.concatenate((EleEnergy,EleEnergy_1))
    MagEnergy = np.concatenate((MagEnergy,MagEnergy_1))
    KinEnergy = np.concatenate((KinEnergy,KinEnergy_1))
    KinEnergy0 = np.concatenate((KinEnergy0,KinEnergy0_1))
    KinEnergy1 = np.concatenate((KinEnergy1,KinEnergy1_1))
    KinEnergy2 = np.concatenate((KinEnergy2,KinEnergy2_1))
    KinEnergy3 = np.concatenate((KinEnergy3,KinEnergy3_1))
    KinEnergy4 = np.concatenate((KinEnergy4,KinEnergy4_1))
    KinEnergy5 = np.concatenate((KinEnergy5,KinEnergy5_1))
    BulEnergy0 = np.concatenate((BulEnergy0,BulEnergy0_1))
    BulEnergy1 = np.concatenate((BulEnergy1,BulEnergy1_1))
    BulEnergy2 = np.concatenate((BulEnergy2,BulEnergy2_1))
    BulEnergy3 = np.concatenate((BulEnergy3,BulEnergy3_1))
    BulEnergy4 = np.concatenate((BulEnergy4,BulEnergy4_1))
    BulEnergy5 = np.concatenate((BulEnergy5,BulEnergy5_1))

    print(irun)
    print(Ncycle)

# plot evolution conserved quantities
plt.subplot(211)
plt.title(simu_path,fontsize=13)
plt.plot(Ncycle,TotEnergy/TotEnergy[0],label='Total E/E[0]')
plt.plot(Ncycle,TotMomentum/TotMomentum[0],label='Total Mom/Mom[0]')
plt.legend(loc=0,fontsize=14)
plt.yticks(fontsize=14)

plt.subplot(212)
plt.plot(Ncycle,EleEnergy/EleEnergy[0],label=r'$E^2/E^2(0)$')
plt.plot(Ncycle,MagEnergy/MagEnergy[0],label=r'$B^2/B^2(0)$')
plt.plot(Ncycle,KinEnergy/KinEnergy[0],label=r'$P/P(0)$')
plt.plot(Ncycle,KinEnergy0/KinEnergy0[0],label=r'$P_{e,bkg}/P_{e,bkg}(0)$')
plt.plot(Ncycle,KinEnergy1/KinEnergy1[0],label=r'$P_{e,cs1}/P_{e,cs1}(0)$')
plt.plot(Ncycle,KinEnergy2/KinEnergy2[0],label=r'$P_{e,cs2}/P_{e,cs2}(0)$')
plt.plot(Ncycle,KinEnergy3/KinEnergy3[0],label=r'$P_{i,bkg}/P_{i,bkg}(0)$')
plt.plot(Ncycle,KinEnergy4/KinEnergy4[0],label=r'$P_{i,cs1}/P_{1,cs1}(0)$')
plt.plot(Ncycle,KinEnergy5/KinEnergy5[0],label=r'$P_{i,cs2}/P_{i,cs2}(0)$')
plt.plot(Ncycle,BulEnergy0/BulEnergy0[0],label=r'$K_{e,bkg}/K_{e,bkg}(0)$')
plt.plot(Ncycle,BulEnergy1/BulEnergy1[0],label=r'$K_{e,cs1}/K_{e,cs1}(0)$')
plt.plot(Ncycle,BulEnergy2/BulEnergy2[0],label=r'$K_{e,cs2}/K_{e,cs2}(0)$')
plt.plot(Ncycle,BulEnergy3/BulEnergy3[0],label=r'$K_{i,bkg}/K_{i,bkg}(0)$')
plt.plot(Ncycle,BulEnergy4/BulEnergy4[0],label=r'$K_{i,cs1}/K_{1,cs1}(0)$')
plt.plot(Ncycle,BulEnergy5/BulEnergy5[0],label=r'$K_{i,cs2}/K_{i,cs2}(0)$')
plt.legend(loc=0,fontsize=14)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.xlabel('Ncycle',fontsize=14)

plt.show()
