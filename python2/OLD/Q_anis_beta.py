#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
import pandas as pd
import seaborn as sns
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta for one time.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'Q_anis_beta'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']
if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#ans = False
#ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
#if ans:
#    betap_lim = input('betap limits ([x0,x1]): ')
#    anisp_lim = input('anisp limits ([y0,y1]): ')
#    betae_lim = input('betae limits ([x0,x1]): ')
#    anise_lim = input('anise limits ([y0,y1]): ')
#else:
#    betap_lim = None
#    anisp_lim = None
#    betae_lim = None
#    anise_lim = None

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = False
smooth_flag_post = False
gfs = 2

def pairgrid_heatmap(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    plt.hist2d(x, y, cmap=cmap, cmin=1, **kws)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag_post:
        Psi = gf(Psi,[gfs,gfs],mode='wrap')
    Pp = run.get_Press(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3):
            for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[gfs,gfs],mode='wrap')
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    anisp = np.divide(pperpp,pparlp) - 1.
    if code_name == 'iPIC' and smooth_flag_post:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfs,gfs],mode='wrap')
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            if code_name == 'iPIC' and smooth_flag:
                for i in range(3):
                    for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[gfs,gfs],mode='wrap')
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        if code_name == 'iPIC' and smooth_flag_post:
            anise[:,:,0] = gf(anise[:,:,0],[gfs,gfs],mode='wrap')
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag_post:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfs,gfs],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag_post:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfs,gfs],mode='wrap')
    del B2
    del B
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #nongyrotropy
    sqrtQp = np.sqrt(np.divide(Pp[0,1]**2+Pp[0,2]**2+Pp[1,2]**2,pperpp**2+2.*pperpp*pparlp))
    del Pp,pparlp,pperpp
    if code_name == 'iPIC':
        sqrtQe = np.sqrt(np.divide(Pe[0,1]**2+Pe[0,2]**2+Pe[1,2]**2,pperpe**2+2.*pperpe*pparle))
        del Pe
    if w_ele:
        del pparle,pperpe
#
    #------------------------------------------------------
    #plot 2d histograms protons
    #--------------------------
    anisp_cut = np.concatenate((anisp[:,:cut1,:],anisp[:,cut2+1:,:]),axis=1)
    del anisp
    bparlp_cut = np.concatenate((bparlp[:,:cut1,:],bparlp[:,cut2+1:,:]),axis=1)
    del bparlp
    sqrtQp_cut = np.concatenate((sqrtQp[:,:cut1,:],sqrtQp[:,cut2+1:,:]),axis=1)
    del sqrtQp
#
    df = pd.DataFrame({
        'LogAp':np.log10(np.array((anisp_cut+1.).flat)),
        'LogBp':np.log10(np.array(bparlp_cut.flat)),
        'LogQp':np.log10(np.array(sqrtQp_cut.flat))
        })
    del anisp_cut,bparlp_cut,sqrtQp_cut
#
    plt.close()
#
    grid = sns.PairGrid(df,size=5)
    grid = grid.map_lower(plt.scatter)
    grid = grid.map_diag(plt.hist,bins=100)
    grid = grid.map_upper(pairgrid_heatmap,bins=100,norm=LogNorm())
#
    plt.suptitle('A vs. beta vs. Q - protons - %s - t=%f'%(run_label,time[ind]))
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'Q_anis_beta_protons_'+run_name+'_'+str(ind)+'.png')
    plt.close()
    del df
#
    #------------------------------------------------------
    #plot 2d histograms electrons
    #----------------------------
    if code_name == 'iPIC':
        anise_cut = np.concatenate((anise[:,:cut1,:],anise[:,cut2+1:,:]),axis=1)
        del anise
        bparle_cut = np.concatenate((bparle[:,:cut1,:],bparle[:,cut2+1:,:]),axis=1)
        del bparle
        sqrtQe_cut = np.concatenate((sqrtQe[:,:cut1,:],sqrtQe[:,cut2+1:,:]),axis=1)
        del sqrtQe
    #
        df = pd.DataFrame({
            'LogAe':np.log10(np.array((anise_cut+1.).flat)),
            'LogBe':np.log10(np.array(bparle_cut.flat)),
            'LogQe':np.log10(np.array(sqrtQe_cut.flat))
            })
        del anise_cut,bparle_cut,sqrtQe_cut
    #
        plt.close()
    #
        grid = sns.PairGrid(df,size=5)
        grid = grid.map_lower(plt.scatter)
        grid = grid.map_diag(plt.hist,bins=100)
        grid = grid.map_upper(pairgrid_heatmap,bins=100,norm=LogNorm())
    #
        plt.suptitle('A vs. beta vs. Q - electrons - %s - t=%f'%(run_label,time[ind]))
        #plt.show()
        plt.savefig(opath+'/'+out_dir+'/'+'Q_anis_beta_electrons_'+run_name+'_'+str(ind)+'.png')
        plt.close()
        del df
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
