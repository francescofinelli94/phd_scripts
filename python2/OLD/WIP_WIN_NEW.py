import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.signal as scisi

import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

def psd_1d(field_,dt):
    field = field_ - np.mean(field_)
    N = len(field)
    xt      = np.fft.rfft(field)/float(N)  # fourier transform of x
    freq    = np.fft.rfftfreq(N,d=dt)  # frequencies involved
    dfreq   = 1.0/(float(N)*dt)        # frequency spacing
    # compute the spectrum
    spec = np.zeros(N//2+1)
    Ef   = np.zeros(N//2+1)
    spec[0]  = (np.real(xt[0])**2+np.imag(xt[0])**2)
    spec[1:] = 2.0*(np.real(xt[1:])**2+np.imag(xt[1:])**2)
    if N % 2 == 0 :    #correct the last term in case N is even
        spec[-1] /= 2.0
    #Compute the power spectral density
    Ef[0]    =2.0*float(N)/dfreq*spec[0]
    Ef[1:-1] =float(N)/dfreq*spec[1:-1]
    if N % 2 == 0:     #last term in case N is even
        Ef[-1]   = 2.0*float(N)/dfreq*spec[-1]
    else:             #last term in case N is odd
        Ef[-1]   = float(N)/dfreq*spec[-1]
    return freq,Ef

ipath = '/work1/califano/HVM2D/LF_2d_DH'
run = from_HVM(ipath)
run.get_meta()

nx = run.meta['nx']
ny = run.meta['ny']
dx = run.meta['dx']
dy = run.meta['dy']
x = run.meta['x']
y = run.meta['y']

ymax = 170
y0 = 85

t = 295.
_,B = run.get_EB(t)
Psi = wl.psi_2d(B[0:2,:,:,0],run.meta)

plt.close()
fig,ax = plt.subplots(1,1,figsize=(12,4))
ax.contour(Psi[:,0:ymax+1].T,32)
plt.show()
plt.close

x1 = ml.secure_input('x1 = ',10,True)
x2 = ml.secure_input('x2 = ',10,True)
y1 = ml.secure_input('y1 = ',10,True)
y2 = ml.secure_input('y2 = ',10,True)

wname = input('window: ')#'boxcar', 'hann', 'flattop'

if x1 > x2:
    sh = x2 + 1 
    B = np.roll(B,-sh,axis=1)
    Psi = np.roll(Psi,-sh,axis=0)
    x1 = x1 - sh
    x2 = nx - 1
if y1 > y2:
    sh = y2 + 1 
    B = np.roll(B,-sh,axis=2)
    Psi = np.roll(Psi,-sh,axis=1)
    y1 = y1 - sh
    y2 = ny - 1

nwx = x2 - x1 + 1
nwy = y2 - y1 + 1
winx = scisi.windows.get_window(wname,nwx)
winy = scisi.windows.get_window(wname,nwy)

plt.close()
fig,ax = plt.subplots(3,2,figsize=(24,12))

ax[0,1].plot(winx,label='winx')
ax[0,1].plot(winy,label='winy')
ax[0,1].legend()
ax[0,1].set_xlabel('indices');ax[0,1].set_ylabel('window');ax[0,1].set_title(str(wname))

im0 = ax[0,0].contourf(B[0,:,0:ymax+1,0].T,64)
plt.colorbar(im0,ax=ax[0,0])
ax[0,0].contour(Psi[:,0:ymax+1].T,8,cmap='inferno')
ax[0,0].axhline(y=y1,linestyle='--',color='k')
ax[0,0].axhline(y=y2,linestyle='--',color='k')
ax[0,0].axvline(x=x1,linestyle='--',color='k')
ax[0,0].axvline(x=x2,linestyle='--',color='k')
ax[0,0].set_xlabel('x indices');ax[0,0].set_ylabel('y indices');ax[0,0].set_title('Bx t='+str(t))

im1 = ax[1,0].contourf(B[1,:,0:ymax+1,0].T,64)
plt.colorbar(im1,ax=ax[1,0])
ax[1,0].contour(Psi[:,0:ymax+1].T,8,cmap='inferno')
ax[1,0].axhline(y=y1,linestyle='--',color='k')
ax[1,0].axhline(y=y2,linestyle='--',color='k')
ax[1,0].axvline(x=x1,linestyle='--',color='k')
ax[1,0].axvline(x=x2,linestyle='--',color='k')
ax[1,0].set_xlabel('x indices');ax[1,0].set_ylabel('y indices');ax[1,0].set_title('By')

im2 = ax[2,0].contourf(B[2,:,0:ymax+1,0].T,64)
plt.colorbar(im2,ax=ax[2,0])
ax[2,0].contour(Psi[:,0:ymax+1].T,8,cmap='inferno')
ax[2,0].axhline(y=y1,linestyle='--',color='k')
ax[2,0].axhline(y=y2,linestyle='--',color='k')
ax[2,0].axvline(x=x1,linestyle='--',color='k')
ax[2,0].axvline(x=x2,linestyle='--',color='k')
ax[2,0].set_xlabel('x indices');ax[2,0].set_ylabel('y indices');ax[2,0].set_title('Bz')

data = B[:,x1:x2+1,y1:y2+1,0].copy()
PSDx = []
PSDy = []
for i in range(3):
    for iy in range(nwy):
        kx_,tmp = psd_1d(data[i,:,iy]*winx,dx)
        if PSDx == []:
            PSDx = np.zeros((3,len(kx_)),dtype=np.float64)
        PSDx[i] += tmp
    for ix in range(nwx):
        ky_,tmp = psd_1d(data[i,ix,:]*winy,dy)
        if PSDy == []:
            PSDy = np.zeros((3,len(ky_)),dtype=np.float64)
        PSDy[i] += tmp

PSDx = PSDx/float(nwy)
PSDy = PSDy/float(nwx)
mx = kx_*(x[-1] + dx)/(2.*np.pi)
my = ky_*(y[-1] + dy)/(2.*np.pi)

ax[1,1].plot(mx[1:],PSDx[0,1:],'--',label='Bx')
ax[1,1].plot(mx[1:],PSDx[1,1:],'--',label='By')
ax[1,1].plot(mx[1:],PSDx[2,1:],'--',label='Bz')
ax[1,1].plot(mx[1:],PSDx[0,1:]+PSDx[1,1:]+PSDx[2,1:],label='B')
ax[1,1].legend();ax[1,1].set_xscale('log');ax[1,1].set_yscale('log')
ax[1,1].set_xlabel('mx');ax[1,1].set_ylabel('PSD');ax[1,1].set_title('x transform')

ax[2,1].plot(my[1:],PSDy[0,1:],'--',label='Bx')
ax[2,1].plot(my[1:],PSDy[1,1:],'--',label='By')
ax[2,1].plot(my[1:],PSDy[2,1:],'--',label='Bz')
ax[2,1].plot(my[1:],PSDy[0,1:]+PSDy[1,1:]+PSDy[2,1:],label='B')
ax[2,1].legend();ax[2,1].set_xscale('log');ax[2,1].set_yscale('log')
ax[2,1].set_xlabel('my');ax[2,1].set_ylabel('PSD');ax[2,1].set_title('y transform')

plt.tight_layout()
plt.show()
plt.close()
