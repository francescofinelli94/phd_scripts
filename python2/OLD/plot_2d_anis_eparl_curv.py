#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of anis, eparl, and curv.\n')

#------------------------------------------------------
#Init
#----
input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']

plt_show_flg = True
zoom_flg = True
rms_flg = False

anis = {'p':{},'e':{}}
eparl = {}
Psi = {}
curv = {}
t = {}
x = {}
y = {}

run_name_vec = []
run_labels = {}
ind_dict = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind,_,_ = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    ind_dict[run_name] = ind
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    nx,ny,_ = run.meta['nnn']
    x_ = run.meta['x']
    y_ = run.meta['y']
    code_name = run.meta['code_name']
#
    calc = fibo('calc')
    calc.meta = run.meta
#
    if code_name == 'HVM':
        smooth_flag = False
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
        B0 = 1.
        n0 = 1.
        V0 = 1.
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
        gfsp = 2
        gfse = 2
        B0 = 0.01
        n0 = 1./(4.*np.pi)
        V0 = 0.01
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
#
    #get data
    #magnetic fields and co.
    E,B = run.get_EB(ind)
#
    Psi_ = wl.psi_2d(B[:-1,...,0],run.meta)
    if smooth_flag:
        Psi_ = gf(Psi_,[gfsp,gfsp],mode='wrap')
    Psi[run_name] = Psi_[ixmin:ixmax,iymin:iymax]/B0
    del Psi_
#
    eparl_ = np.divide(np.sum(np.multiply(E,B),axis=0),np.sqrt(np.sum(np.multiply(B,B),axis=0)))[...,0]
    del E
    if smooth_flag:
        eparl_ = gf(eparl_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        eparl_rms = np.sqrt(np.mean(eparl_[ixmin:ixmax,iymin:iymax]*eparl_[ixmin:ixmax,iymin:iymax]))
        eparl[run_name] = eparl_[ixmin:ixmax,iymin:iymax]/eparl_rms
    else:
        eparl_rms = np.sqrt(np.mean(eparl_[ixmin:ixmax,iymin:iymax]*eparl_[ixmin:ixmax,iymin:iymax]))
        eparl[run_name] = eparl_[ixmin:ixmax,iymin:iymax]/eparl_rms
    del eparl_
#
    #anisotropy
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp,pparlp
    anisp = anisp[...,0]
    if rms_flg:
        anisp_rms = np.sqrt(np.mean(anisp[ixmin:ixmax,iymin:iymax]*anisp[ixmin:ixmax,iymin:iymax]))
        anis['p'][run_name] = anisp[ixmin:ixmax,iymin:iymax]/anisp_rms
    else:
        anis['p'][run_name] = anisp[ixmin:ixmax,iymin:iymax]
    del anisp
#
    if w_ele:
        if code_name == 'HVM':
            n_p,_ = run.get_Ion(ind)
            pparle,pperpe = run.get_Te(ind)
            pparle *= n_p
            pperpe *= n_p
            del n_p
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe,pparle
    else:
        anise = np.ones((nx,ny,1),dtype=np.float64)
    anise = anise[...,0]
    if rms_flg:
        anise_rms = np.sqrt(np.mean(anise[ixmin:ixmax,iymin:iymax]*anise[ixmin:ixmax,iymin:iymax]))
        anis['e'][run_name] = anise[ixmin:ixmax,iymin:iymax]/anise_rms
    else:
        anis['e'][run_name] = anise[ixmin:ixmax,iymin:iymax]
    del anise
#
    b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    Bn = np.sqrt(B[0]*B[0] + B[1]*B[1] + B[2]*B[2])
    for i in range(3):
        b[i] = np.divide(B[i],Bn)
    del B,Bn
    dxbx = calc.calc_gradx(b[0])
    dybx = calc.calc_grady(b[0])
    dxby = calc.calc_gradx(b[1])
    dyby = calc.calc_grady(b[1])
    dxbz = calc.calc_gradx(b[2])
    dybz = calc.calc_grady(b[2])
    b_Nb = np.array([b[0]*dxbx+b[1]*dybx, b[0]*dxby + b[1]*dyby, b[0]*dxbz + b[1]*dybz])
    del b,dxbx,dybx,dxby,dyby,dxbz,dybz
    curv_ = np.sqrt(b_Nb[0]**2 + b_Nb[1]**2 + b_Nb[2]**2)[...,0]
    del b_Nb
    if smooth_flag:
        curv_ = gf(curv_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        curv_rms = np.sqrt(np.mean(curv_[ixmin:ixmax,iymin:iymax]*curv_[ixmin:ixmax,iymin:iymax]))
        curv[run_name] = curv_[ixmin:ixmax,iymin:iymax]/curv_rms
    else:
        curv[run_name] = curv_[ixmin:ixmax,iymin:iymax]
    del curv_
#
    t[run_name] =  times[ind]
    del times
#
    x[run_name] = x_[ixmin:ixmax]
    y[run_name] = y_[iymin:iymax]
    del x_,y_
#
    gc.collect()

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
fig,ax = plt.subplots(4,3,figsize=(16,14),sharex=True,sharey=True)
fig.subplots_adjust(hspace=.06,wspace=.03,top=.95,bottom=.1,left=.055,right=1.075)#.5#1.07

vmin = ml.min_dict(anis['p'])
vmax = ml.max_dict(anis['p'])
vminanisp = 10.**(-max(np.log10(vmax),-np.log10(vmin)))
vmaxanisp = 10.**max(np.log10(vmax),-np.log10(vmin))
vmin = ml.min_dict(anis['e'])
vmax = ml.max_dict(anis['e'])
vminanise = 10.**(-max(np.log10(vmax),-np.log10(vmin)))
vmaxanise = 10.**max(np.log10(vmax),-np.log10(vmin))
vmin = ml.min_dict(eparl)
vmax = ml.max_dict(eparl)
vmineparl = -max(vmax,-vmin)
vmaxeparl = max(vmax,-vmin)
vmincurv = ml.min_dict(curv)
vmaxcurv = ml.max_dict(curv)
for j,run_name in enumerate(run_name_vec):
    i = 0
    im1 = ax[i,j].pcolormesh(x[run_name],y[run_name],anis['p'][run_name].T,shading='gouraud',
                             cmap='seismic',
                             norm=mpl.colors.LogNorm(vmin=vminanisp,vmax=vmaxanisp))
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
    ax[i,j].set_title(run_labels[run_name])
#
    i = 1
    im2 = ax[i,j].pcolormesh(x[run_name],y[run_name],anis['e'][run_name].T,shading='gouraud',
                             cmap='seismic',
                             norm=mpl.colors.LogNorm(vmin=vminanise,vmax=vmaxanise))
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
#
    i = 2
    im3 = ax[i,j].pcolormesh(x[run_name],y[run_name],eparl[run_name].T,shading='gouraud',
                             cmap='seismic',
                             vmin=vmineparl,vmax=vmaxeparl)
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
#
    i = 3
    im4 = ax[i,j].pcolormesh(x[run_name],y[run_name],curv[run_name].T,shading='gouraud',
                             cmap='Reds',
                             norm=mpl.colors.LogNorm(vmin=vmincurv,vmax=vmaxcurv))
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    ax[i,j].set_xlabel('$x\quad [d_i]$')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')

cb1 = plt.colorbar(im1,ax=ax[0,:],pad=.007)
if rms_flg:
    label_ = '$A_{\mathrm{p}} / \overline{A_{\mathrm{p}}}$'
else:
    label_ = '$p_{\perp,\mathrm{p}}/p_{\parallel,\mathrm{p}}$'
cb1.set_label(label_,rotation=270,labelpad=25)

cb2 = plt.colorbar(im2,ax=ax[1,:],pad=.007)
if rms_flg:
    label_ = '$A_{\mathrm{e}} / \overline{A_{\mathrm{e}}}$'
else:
    label_ = '$p_{\perp,\mathrm{e}}/p_{\parallel,\mathrm{e}}$'
cb2.set_label(label_,rotation=270,labelpad=20)#20)

cb3 = plt.colorbar(im3,ax=ax[2,:],pad=.007)
if rms_flg:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
else:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
cb3.set_label(label_,rotation=270,labelpad=45)#40)

cb4 = plt.colorbar(im4,ax=ax[3,:],pad=.007)
if rms_flg:
    label_ = '$\kappa / \overline{\kappa}$'
else:
    label_ = '$|\hat{b}\cdot\\nabla\hat{b}|$'
cb4.set_label(label_,rotation=270,labelpad=45)#40)

if plt_show_flg:
    plt.show()

out_dir = 'plot_2d_anis_eparl_curv'
ml.create_path(opath+'/'+out_dir)
out_dir += '/comp'
fig_name = 'Jz_Bz_Veplane'
for run_name in run_name_vec:
    out_dir += '__' + run_name
    fig_name += '__' + run_labels[run_name] + '_ind%d'%ind_dict[run_name]

if zoom_flg:
    fig_name += '__zoom'
if rms_flg:
    fig_name += '__rms'
fig_name += '.png'
ml.create_path(opath+'/'+out_dir)
fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
