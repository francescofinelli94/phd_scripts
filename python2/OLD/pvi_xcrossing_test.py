import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import glob
import work_lib as wl
import scipy.ndimage.interpolation as ndm
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)


ipath='/work1/califano/HVM2D/HVM_large'
pvi_path = '/home/sisti/python_routines/VIRTUAL_SATELLITE'
run = from_HVM(ipath)
run.get_meta('01')
run_name = 'HVM_large'
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']

calc = fibo('calc')
calc.meta=run.meta

#t_ = ml.secure_input('Choose time (float): ',1.,True) #247. #494.
t_ = 247. #494.
ind = ml.index_of_closest(t_,run.meta['time'])

#----------------------------------------------------------------------------------------------------

#PRIMA PARTE: ricostruzione traiettoria, le coordinate finali xco e yco sono in unita' [di]

#input_angle  = ml.secure_input('Choose angle (integer): ',10,True) #5 #8 #13 #75 #80 #85
input_angle = 38 #5 #8 #13 #75 #80 #85
input_folder = run.meta['time2seg'][ind]
pvi_path = pvi_path + '/new_pvi_%s/angle%.d'%(input_folder,input_angle)

pi2 = 2.0*np.pi
theta_s = float(input_angle)*pi2/360.0

files = glob.glob(pvi_path+'/B_1Dcut_pvi_*_center.dat')
ll = []
for f in files: ll.append(int(f.split('_')[-2].split('.')[0]))
files = [tmp for _,tmp in sorted(zip(ll,files))]
ll = sorted(ll)

lstart = ll[0]
for il,l,fname in zip(range(len(ll)),ll,files):
    f = open(fname, 'r')
    NN = len(f.readlines())
    f.seek(0)
    if (l == lstart):
        sco = np.zeros((NN),dtype=np.float64)
        xco = np.zeros((NN),dtype=np.float64)
        yco = np.zeros((NN),dtype=np.float64)
        PVI = np.full((NN,len(ll)),0.,dtype=np.float64)
    for i in range(NN):
        tr = f.readline().split()
        if (l == lstart):
            sco[i] = float(tr[0])
            xco[i] = float(tr[1])
            yco[i] = float(tr[2])
        PVI[i,il] = float(tr[3])
    f.close()

sco = sco*xl/pi2
xco = xco*xl/pi2
yco = yco*yl/pi2

ds = sco[1] - sco[0]

#----------------------------------------------------------------------------------------------------

#SECONDA PARTE: interpolazione

#calcolare dati
_,B = run.get_EB(ind)

cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
J = np.array([cx,cy,cz])
del cx,cy,cz

Jn = np.sqrt(J[0]**2 + J[1]**2 + J[2]**2)

#interpolazione vera e propria
new_coordsx = xco/dx
new_coordsy = yco/dy

Bx_an = ndm.map_coordinates(B[0,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
By_an = ndm.map_coordinates(B[1,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
Bz_an = ndm.map_coordinates(B[2,:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')
Jn_an = ndm.map_coordinates( Jn[:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')

#----------------------------------------------------------------------------------------------------
Xpoints = np.array([[196.86086777865728, 289.9223689103862 ,  43.360523604234125],
                    [ 47.04207749515967, 193.69064081702697, 213.01879874438603 ]])

plt.close()
fig, ax = plt.subplots(1,1,figsize=(8,8))

ax.contourf(x,y,Jn[...,0].T,32)
ax.plot()

plt.tight_layout()
plt.show()
plt.close()
