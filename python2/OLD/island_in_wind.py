#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.optimize import curve_fit
#import shift_and_flood as saf
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
#mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill follow island in windlike plot.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

run_name = run.meta['run_name']
out_dir = 'island_in_wind/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']
x = run.meta['x']
y = run.meta['y']

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
if ans:
    betap_lim = input('betap limits ([x0,x1]): ')
    anisp_lim = input('anisp limits ([y0,y1]): ')
    betae_lim = input('betae limits ([x0,x1]): ')
    anise_lim = input('anise limits ([y0,y1]): ')
else:
    betap_lim = None
    anisp_lim = None
    betae_lim = None
    anise_lim = None

wl.Can_I()

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfs = 2

#threshold parameter (closer to 1 this, the smaller the the island)
th_param = 0.90

#see Hellinger et al. 2006
#    Astfalk and Jenko 2016
params = {}
params['H2006'] = {}
params['A2016'] = {}

params['H2006']['PC'] = {}
params['H2006']['PC']['10m3'] = {}
params['H2006']['PC']['10m3']['bM'] = [ 0.43,0.42,-0.0004]

params['H2006']['MR'] = {}
params['H2006']['MR']['10m3'] = {}
params['H2006']['MR']['10m3']['bM'] = [ 0.77,0.76,-0.0160]

params['H2006']['PF'] = {}
params['H2006']['PF']['10m3'] = {}
params['H2006']['PF']['10m3']['bM'] = [-0.47,0.53, 0.5900]

params['H2006']['OF'] = {}
params['H2006']['OF']['10m3'] = {}
params['H2006']['OF']['10m3']['bM'] = [-1.40,1.00,-0.1100]

params['A2016']['PF'] = {}
params['A2016']['PF']['10m3'] = {}
params['A2016']['PF']['10m3']['bM']  = [-0.487,0.537, 0.560]
params['A2016']['PF']['10m3']['k12'] = [-0.438,0.475, 0.503]
params['A2016']['PF']['10m3']['k8']  = [-0.429,0.486, 0.423]
params['A2016']['PF']['10m3']['k6']  = [-0.417,0.498, 0.350]
params['A2016']['PF']['10m3']['k4']  = [-0.387,0.518, 0.226]
params['A2016']['PF']['10m3']['k2']  = [-0.274,0.536, 0.042]
params['A2016']['PF']['10m2'] = {}
params['A2016']['PF']['10m2']['bM']  = [-0.701,0.623, 0.599]
params['A2016']['PF']['10m2']['k12'] = [-0.656,0.596, 0.567]
params['A2016']['PF']['10m2']['k8']  = [-0.623,0.579, 0.569]
params['A2016']['PF']['10m2']['k6']  = [-0.625,0.585, 0.501]
params['A2016']['PF']['10m2']['k4']  = [-0.625,0.593, 0.379]
params['A2016']['PF']['10m2']['k2']  = [-0.632,0.589, 0.139]
params['A2016']['PF']['10m1'] = {}
params['A2016']['PF']['10m1']['bM']  = [-0.872,0.495, 1.233]
params['A2016']['PF']['10m1']['k12'] = [-0.899,0.502, 1.213]
params['A2016']['PF']['10m1']['k8']  = [-0.937,0.509, 1.097]
params['A2016']['PF']['10m1']['k6']  = [-0.947,0.505, 1.088]
params['A2016']['PF']['10m1']['k4']  = [-0.977,0.496, 1.068]
params['A2016']['PF']['10m1']['k2']  = [-1.230,0.464, 1.206]

params['A2016']['OF'] = {}
params['A2016']['OF']['10m3'] = {}
params['A2016']['OF']['10m3']['bM']  = [-1.371,0.996,-0.083]
params['A2016']['OF']['10m3']['k12'] = [-1.444,0.995,-0.070]
params['A2016']['OF']['10m3']['k8']  = [-1.484,0.994,-0.061]
params['A2016']['OF']['10m3']['k6']  = [-1.525,0.993,-0.052]
params['A2016']['OF']['10m3']['k4']  = [-1.613,0.990,-0.026]
params['A2016']['OF']['10m2'] = {}
params['A2016']['OF']['10m2']['bM']  = [-1.371,0.980,-0.049]
params['A2016']['OF']['10m2']['k12'] = [-1.440,0.979,-0.034]
params['A2016']['OF']['10m2']['k8']  = [-1.477,0.978,-0.024]
params['A2016']['OF']['10m2']['k6']  = [-1.514,0.976,-0.012]
params['A2016']['OF']['10m2']['k4']  = [-1.594,0.973, 0.017]

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

#-------------------------------------------------------
#plot function
#-------------
def windlike_plot_func_masked( ax,xdata_uncut,ydata_uncut,mask_uncut,
                               species_name,species_tag,b0_mult,
                               xr=None,yr=None,t=None,params=params ):
    xdata = np.concatenate((xdata_uncut[:,:cut1,:],xdata_uncut[:,cut2+1:,:]),axis=1)
    ydata = np.concatenate((ydata_uncut[:,:cut1,:],ydata_uncut[:,cut2+1:,:]),axis=1)
    mask  = np.concatenate(( mask_uncut[:,:cut1,:], mask_uncut[:,cut2+1:,:]),axis=1)

    h,xbin,ybin=np.histogram2d(np.log(np.array(xdata.flat)),np.log(np.array(ydata.flat)+1.),80)
    X,Y = np.meshgrid(np.exp(xbin),np.exp(ybin))
    im = ax.pcolormesh(X,Y,h.T,norm=mpl.colors.LogNorm())

    ax.plot(xdata[mask],ydata[mask]+1.,color='mediumvioletred',marker='.',linestyle='')

    if xr == None:
        xr = [np.min(np.exp(xbin)),np.max(np.exp(xbin))]
    if yr == None:
        yr = [np.min(np.exp(ybin)),np.max(np.exp(ybin))]
    xvals = np.logspace(np.log10(xr[0]),np.log10(xr[1]),100)

    p = params['H2006']['PC']['10m3']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label='PC10m3bM')

    p = params['H2006']['MR']['10m3']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label='MR10m3bM')

    #p = params['A2016']['PF']['10m3']['bM']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-r',label='PF10m3bM')

    #p = params['A2016']['PF']['10m3']['k4']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--r',label='PF10m3k4')

    #p = params['A2016']['OF']['10m3']['bM']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-c',label='OF10m3bM')

    #p = params['A2016']['OF']['10m3']['k4']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--c',label='OF10m3k4')

    p = params['A2016']['PF']['10m2']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label='PF10m2bM')

    p = params['A2016']['PF']['10m2']['k4']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--m',label='PF10m2k4')

    p = params['A2016']['OF']['10m2']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label='OF10m2bM')
    
    p = params['A2016']['OF']['10m2']['k4']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label='OF10m2k4')

    ax.plot(xvals,np.divide(1.,xvals),'-.k',label='1/b_parl')

    xg = np.mean(xdata[mask])
    yg = np.mean(ydata[mask]) + 1.
    ax.plot([xg],[yg],linestyle='',marker='o',markersize=10,color='lime',label='mean')

    h_,xb_,yb_ = np.histogram2d(np.log(xdata[mask]),np.log(ydata[mask]+1.),40)
    #X_,Y_ = np.meshgrid(np.exp(xb_),np.exp(yb_))
    #im = ax.pcolormesh(X_,Y_,h_.T,norm=mpl.colors.LogNorm())
    ind = np.unravel_index(np.argmax(h_,axis=None),h_.shape)
    xmd = np.exp(0.5*(xb_[ind[0]] + xb_[ind[0]+1]))
    ymd = np.exp(0.5*(yb_[ind[1]] + yb_[ind[1]+1]))
    ax.plot([xmd],[ymd],linestyle='',marker='o',markersize=10,color='darkgoldenrod',label='mode')

    ax.legend()
    ax.set_xlabel('Parallel '+species_name+' beta')
    ax.set_ylabel('P_perp_'+species_tag+'/P_parl_'+species_tag)
    ax.set_xlim(xr[0],xr[1]);ax.set_ylim(yr[0],yr[1])
    if t != None:
        ax.set_title(species_name+' anisotropy t = '+str(t))
    else:
        ax.set_title(species_name+' anisotropy')
    ax.set_xscale('log');ax.set_yscale('log');plt.colorbar(im,ax=ax)

    return [xg,yg,xmd,ymd]

def to_fit(x,a=1.,b=-1.):
    return a*(x**b)

def windlike_g_plot_func( ax,t_vec,g_pos,
                          species_name,species_tag,b0_mult,
                          xr=None,yr=None,t=None,params=params,itrange=None ):

    im = ax.scatter(g_pos[:,0],g_pos[:,1],c=t_vec,cmap='gist_rainbow')
    cbar = plt.colorbar(im,ax=ax)
    cbar.set_label('time',rotation=270)

    if xr == None:
        xr = [np.min(g_pos[:,0]),np.max(g_pos[:,0])]
    if yr == None:
        yr = [np.min(g_pos[:,1]),np.max(g_pos[:,1])]
    xvals = np.logspace(np.log10(xr[0]),np.log10(xr[1]),100)

    p = params['H2006']['PC']['10m3']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label='PC10m3bM')

    p = params['H2006']['MR']['10m3']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label='MR10m3bM')

    #p = params['A2016']['PF']['10m3']['bM']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-r',label='PF10m3bM')

    #p = params['A2016']['PF']['10m3']['k4']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--r',label='PF10m3k4')

    #p = params['A2016']['OF']['10m3']['bM']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-c',label='OF10m3bM')

    #p = params['A2016']['OF']['10m3']['k4']
    #ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--c',label='OF10m3k4')

    p = params['A2016']['PF']['10m2']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label='PF10m2bM')

    p = params['A2016']['PF']['10m2']['k4']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--m',label='PF10m2k4')

    p = params['A2016']['OF']['10m2']['bM']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label='OF10m2bM')
    
    p = params['A2016']['OF']['10m2']['k4']
    ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label='OF10m2k4')

    ax.plot(xvals,np.divide(1.,xvals),'-.k',label='1/b_parl')

    if itrange == None:
        itmin = 0
        itmax = len(g_pos[:,0])
    else:
        itmin = itrange[0]
        itmax = itrange[1]
    a0 = g_pos[itmax,0]*g_pos[itmax,1]
    b0 = - 1.
    par,_ = curve_fit(to_fit,g_pos[itmin:itmax,0],g_pos[itmin:itmax,1],p0=[a0,b0])
    ax.plot(xvals,to_fit(xvals,*par),':k',label='exp.='+str(par[1]))

    ax.plot(xvals,np.divide(a0,xvals),'-.k',)

    ax.legend()
    ax.set_xlabel('Parallel '+species_name+' beta')
    ax.set_ylabel('P_perp_'+species_tag+'/P_parl_'+species_tag)
    ax.set_xlim(xr[0],xr[1]);ax.set_ylim(yr[0],yr[1])
    if t != None:
        ax.set_title(species_name+' anisotropy t = '+str(t))
    else:
        ax.set_title(species_name+' anisotropy')
    ax.set_xscale('log');ax.set_yscale('log')

#---> loop over times <---
pro_g = []
ele_g = []
all_t = []
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
#
    Pp = run.get_Press(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3):
            for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[gfs,gfs],mode='wrap')
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp) - 1.
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            if code_name == 'iPIC' and smooth_flag:
                for i in range(3):
                    for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[gfs,gfs],mode='wrap')
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        del pparle
    del B2
    del B
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    mask = (Psi > np.max(Psi)*th_param).reshape(nx,ny,nz)
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    plt.close()
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig,ax = plt.subplots(1,nplot+1,figsize=(10*(nplot+1),8))
#
    if w_ele:
        ax0 = ax[0]
        ax2 = ax[2]
    else:
        ax0 = ax[0]
        ax2 = ax[1]
    tmp = windlike_plot_func_masked( ax0,bparlp,anisp,mask,
                                     'Protons CS1','p',1.,xr=betap_lim,yr=anisp_lim,t=time[ind] )
    pro_g.append(tmp)
#
    ax2.contourf(x,y,mask[...,0].T,1);ax2.contour(x,y,Psi.T,32)
    ax2.set_xlabel('x');ax2.set_ylabel('y');ax2.set_title('this is the island')
#
    if w_ele:
        tmp = windlike_plot_func_masked( ax[1],bparle,anise,mask,
                                         'Electrons CS1','e',teti,xr=betae_lim,yr=anise_lim,t=time[ind])
        ele_g.append(tmp)
#
    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'island_in_wind_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    all_t.append(time[ind])
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

#plot island center of gravity
pro_g = np.array(pro_g)
ele_g = np.array(ele_g)
all_t = np.array(all_t)

plt.close()
if w_ele:
    nplot = 2
else:
    nplot = 1
fig,ax = plt.subplots(1,nplot,figsize=(10*nplot,8))

if w_ele:
    ax0 = ax[0]
else:
    ax0 = ax
windlike_g_plot_func( ax0,all_t,pro_g[:,:2],'Protons CS1','p',1.,
                      xr=betap_lim,yr=anisp_lim,t=time[ind],itrange=[80,100] )

if w_ele:
    windlike_g_plot_func( ax[1],all_t,ele_g[:,:2],'Electrons CS1','e',teti,
                          xr=betae_lim,yr=anise_lim,t=time[ind],itrange=[60,80] )

plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'island_in_wind_g_dyn_'+run_name+'.png')
plt.close()

#plot island mode
plt.close()
if w_ele:
    nplot = 2
else:
    nplot = 1
fig,ax = plt.subplots(1,nplot,figsize=(10*nplot,8))

if w_ele:
    ax0 = ax[0]
else:
    ax0 = ax
windlike_g_plot_func( ax0,all_t,pro_g[:,2:],'Protons CS1','p',1.,
                      xr=betap_lim,yr=anisp_lim,t=time[ind],itrange=[80,100] )

if w_ele:
    windlike_g_plot_func( ax[1],all_t,ele_g[:,2:],'Electrons CS1','e',teti,
                          xr=betae_lim,yr=anise_lim,t=time[ind],itrange=[80,100] )

plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'island_in_wind_md_dyn_'+run_name+'.png')
plt.close()
