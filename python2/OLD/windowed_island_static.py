#-------------------------------------------------------
#imports
#----------------
import sys
import gc
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.signal as scisi

import work_lib as wl
import shift_and_flood as saf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#-------------------------------------------------------
#functions
#----------------
def psd_1d(field_,dt):
    field = field_ - np.mean(field_)
    N = len(field)
    xt      = np.fft.rfft(field)/float(N)  # fourier transform of x
    freq    = np.fft.rfftfreq(N,d=dt)  # frequencies involved
    dfreq   = 1.0/(float(N)*dt)        # frequency spacing
    # compute the spectrum
    spec = np.zeros(N//2+1)
    Ef   = np.zeros(N//2+1)
    spec[0]  = (np.real(xt[0])**2+np.imag(xt[0])**2)
    spec[1:] = 2.0*(np.real(xt[1:])**2+np.imag(xt[1:])**2)
    if N % 2 == 0 :    #correct the last term in case N is even
        spec[-1] /= 2.0
    #Compute the power spectral density
    Ef[0]    =2.0*float(N)/dfreq*spec[0]
    Ef[1:-1] =float(N)/dfreq*spec[1:-1]
    if N % 2 == 0:     #last term in case N is even
        Ef[-1]   = 2.0*float(N)/dfreq*spec[-1]
    else:             #last term in case N is odd
        Ef[-1]   = float(N)/dfreq*spec[-1]
    return freq,Ef

#-------------------------------------------------------
#init
#----------------
run,tr,_=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'windowed_island_static'
opath = run.meta['opath']
code_name = run.meta['code_name']
nx,ny,_ = run.meta['nnn']
dx = run.meta['dx']
dy = run.meta['dy']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])
y0 = int(y1/run.meta['dy'])
yr = int(min(abs(y1-y2),run.meta['yl']-abs(y1-y2))/2./run.meta['dy'])
perm_sh_y = y0 - yr

if run_name == 'HVM_2d_DH':
    nwx = 512
    nwy = 128
    th_param = 0.98
elif run_name == 'LF_2d_DH':
    nwx = 512
    nwy = 128
    th_param = 0.98
elif run_name == 'DH_run3_data0':
    nwx = 512
    nwy = 128
    th_param = 0.98
else:
    print('ERROR: wrong run_name')

#window
    #'boxcar', 'hamming', 'hann', 'blackman', 'flattop', 'nuttall', 'blackmanharris'
    # (+) <-  FREQ. RES.  <- (-)
    # (-) ->  DYN. RANGE  -> (+)
print('Suggested windows:')
print('boxcar, hamming, hann, blackman, flattop, nuttall, blackmanharris')
print(' (+) <-  FREQ. RES.  <- (-)')
print(' (-) ->  DYN. RANGE  -> (+)')
wname = ml.secure_input('chosen window: ','',True)

#subpath
out_dir = ( out_dir + '/' + run_name + '/' + wname + '_' +
            str(nwx) + '_' + str(nwy)+ '_' + str(ind1)+ '_' + str(ind2) )
ml.create_path(opath+'/'+out_dir)
wl.Can_I()

#-------------------------------------------------------
#main loop
#----------------
island_width   = []
island_length  = []
island_surface = []
island_center  = []

PSDx_t = []
mx_t   = []
PSDy_t = []
my_t   = []
time_t = []

B_flag = False
B_sh = 0
#---> loop over time <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#------------------------
    #get the data
        #ind = 80 ,120 is the goal
    _,B = run.get_EB(ind)
    if perm_sh_y < 0:
        B = np.roll(B,-perm_sh_y,axis=2)
        y0 = y0 - perm_sh_y
        B_sh = perm_sh_y
        perm_sh_y = 0
        B_flag = True
    if B_flag:
        B = np.roll(B,-B_sh,axis=2)
    Psi = wl.psi_2d(B[0:2,:,:,0],run.meta)

    #find the island
    reg,nreg = saf.my_flood(-Psi,-np.max(Psi)*th_param,periodicity=[True,True])

    v0 = 0
    cnt = 0
    for n in range(nreg):
        c = np.sum(reg == n+1)
        if c > cnt:
            cnt = c
            v0 = n + 1

    island_surface.append(cnt)

    reg = reg == v0
    for ix in range(nx):
        if np.sum(reg[ix,:]) > 0:
            x1 = ix
            break
    for ix in range(nx):
        if ix < x1:
            continue
        if np.sum(reg[ix,:]) == 0:
            x2 = ix - 1 
            break
    if x1 == 0:
        try:
            for ix in range(nx):
                if ix <= x2:
                    continue
                if np.sum(reg[ix,:]) > 0:
                    x1 = ix
                    break
        except:
            x2 = nx -1

    for iy in range(ny):
        if np.sum(reg[:,iy]) > 0:
            y1 = iy
            break
    for iy in range(ny):
        if iy < y1:
            continue
        if np.sum(reg[:,iy]) == 0:
            y2 = iy - 1
            break
    if y1 == 0:
        try:
            for iy in range(ny):
                if iy <= y2:
                    continue
                if np.sum(reg[:,iy]) > 0:
                    y1 = iy
                    break
        except:
            y2 = ny - 1

    if x1 > x2:
        sh = x2 + 1
        B = np.roll(B,-sh,axis=1)
        Psi = np.roll(Psi,-sh,axis=0)
        x1 = x1 - sh
        x2 = nx - 1
    if y1 > y2:
        sh = y2 + 1
        B = np.roll(B,-sh,axis=2)
        Psi = np.roll(Psi,-sh,axis=1)
        y1 = y1 - sh
        y2 = ny - 1

    island_length.append(max(x2-x1+1,y2-y1+1))
    island_width.append(min(x2-x1+1,y2-y1+1))

    xg = 0
    yg = 0
    cnt = 0
    for ix in range(x2-x1+1):
        for iy in range(y2-y1+1):
            if reg[ix+x1,iy+y1]:
                cnt += 1
                xg += ix + x1
                yg += iy + y1
    xg = int(float(xg)/float(cnt))
    yg = int(float(yg)/float(cnt))
    island_center.append([xg,yg])

    x1 = (xg - (nwx-1)//2 + nx)%nx
    x2 = (xg + (nwx)//2)%nx
    y1 = (yg - (nwy-1)//2 + ny)%ny
    y2 = (yg + (nwy)//2)%ny

    if x1 > x2:
        sh = x2 + 1
        B = np.roll(B,-sh,axis=1)
        Psi = np.roll(Psi,-sh,axis=0)
        x1 = x1 - sh
        x2 = nx - 1
    if y1 > y2:
        sh = y2 + 1
        B = np.roll(B,-sh,axis=2)
        Psi = np.roll(Psi,-sh,axis=1)
        y1 = y1 - sh
        y2 = ny - 1

    if nwx != x2 - x1 + 1:
        print '\nAAAAAAAAAAAAAAAAAAAAAAH\n'
    if nwy != y2 - y1 + 1:
        print '\nAAAAAAAAAAAAAAAAAAAAAAH\n'
    winx = scisi.windows.get_window(wname,nwx)
    winy = scisi.windows.get_window(wname,nwy)

    #WFFT -> PSD
    data = B[:,x1:x2+1,y1:y2+1,0].copy()
    PSDx = []
    PSDy = []
    for i in range(3):
        for iy in range(nwy):
            kx_,tmp = psd_1d(data[i,:,iy]*winx,dx)
            if PSDx == []:
                PSDx = np.zeros((3,len(kx_)),dtype=np.float64)
            PSDx[i] += tmp
        for ix in range(nwx):
            ky_,tmp = psd_1d(data[i,ix,:]*winy,dy)
            if PSDy == []:
                PSDy = np.zeros((3,len(ky_)),dtype=np.float64)
            PSDy[i] += tmp

    PSDx = PSDx/float(nwy)
    PSDy = PSDy/float(nwx)
    mx = kx_/kx_[1]*float(nx)/float(nwx)
    my = ky_/ky_[1]*float(ny)/float(nwy)

    #3x2 plot
    plt.close()
    fig,ax = plt.subplots(3,2,figsize=(24,12))

    ax[0,1].plot(winx,label='winx')
    ax[0,1].plot(winy,label='winy')
    ax[0,1].legend()
    ax[0,1].set_xlabel('indices');ax[0,1].set_ylabel('window');ax[0,1].set_title(str(wname))

    im0 = ax[0,0].contourf(B[0,:,y0-yr:y0+yr+1,0].T,64)
    plt.colorbar(im0,ax=ax[0,0])
    ax[0,0].contour(Psi[:,y0-yr:y0+yr+1].T,8,cmap='inferno')
    ax[0,0].axhline(y=y1,linestyle='--',color='k')
    ax[0,0].axhline(y=y2,linestyle='--',color='k')
    ax[0,0].axvline(x=x1,linestyle='--',color='k')
    ax[0,0].axvline(x=x2,linestyle='--',color='k')
    ax[0,0].set_xlabel('x indices');ax[0,0].set_ylabel('y indices');ax[0,0].set_title('Bx t='+str(time[ind]))

    im1 = ax[1,0].contourf(B[1,:,y0-yr:y0+yr+1,0].T,64)
    plt.colorbar(im1,ax=ax[1,0])
    ax[1,0].contour(Psi[:,y0-yr:y0+yr+1].T,8,cmap='inferno')
    ax[1,0].axhline(y=y1,linestyle='--',color='k')
    ax[1,0].axhline(y=y2,linestyle='--',color='k')
    ax[1,0].axvline(x=x1,linestyle='--',color='k')
    ax[1,0].axvline(x=x2,linestyle='--',color='k')
    ax[1,0].set_xlabel('x indices');ax[1,0].set_ylabel('y indices');ax[1,0].set_title('By')

    im2 = ax[2,0].contourf(B[2,:,y0-yr:y0+yr+1,0].T,64)
    plt.colorbar(im2,ax=ax[2,0])
    ax[2,0].contour(Psi[:,y0-yr:y0+yr+1].T,8,cmap='inferno')
    ax[2,0].axhline(y=y1,linestyle='--',color='k')
    ax[2,0].axhline(y=y2,linestyle='--',color='k')
    ax[2,0].axvline(x=x1,linestyle='--',color='k')
    ax[2,0].axvline(x=x2,linestyle='--',color='k')
    ax[2,0].set_xlabel('x indices');ax[2,0].set_ylabel('y indices');ax[2,0].set_title('Bz')

    ax[1,1].plot(mx[1:],PSDx[0,1:],'--',label='Bx')
    ax[1,1].plot(mx[1:],PSDx[1,1:],'--',label='By')
    ax[1,1].plot(mx[1:],PSDx[2,1:],'--',label='Bz')
    ax[1,1].plot(mx[1:],PSDx[0,1:]+PSDx[1,1:]+PSDx[2,1:],label='B')
    ax[1,1].legend();ax[1,1].set_xscale('log');ax[1,1].set_yscale('log')
    ax[1,1].set_xlabel('mx');ax[1,1].set_ylabel('PSD');ax[1,1].set_title('x transform')

    ax[2,1].plot(my[1:],PSDy[0,1:],'--',label='Bx')
    ax[2,1].plot(my[1:],PSDy[1,1:],'--',label='By')
    ax[2,1].plot(my[1:],PSDy[2,1:],'--',label='Bz')
    ax[2,1].plot(my[1:],PSDy[0,1:]+PSDy[1,1:]+PSDy[2,1:],label='B')
    ax[2,1].legend();ax[2,1].set_xscale('log');ax[2,1].set_yscale('log')
    ax[2,1].set_xlabel('my');ax[2,1].set_ylabel('PSD');ax[2,1].set_title('y transform')

    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/'+'windowed_island_'+wname+'_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #save spectra at this time
    PSDx_t.append(PSDx)
    mx_t.append(mx)
    PSDy_t.append(PSDy)
    my_t.append(my)
    time_t.append(time[ind])
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
time_t = np.array(time_t,dtype=np.float64)
nt = time_t.shape[0]

#plot island growth
#"""
island_width   = np.array(island_width,dtype=np.float64)*dy
island_length  = np.array(island_length,dtype=np.float64)*dx
island_surface = np.array(island_surface,dtype=np.float64)*dx*dy

plt.close()
plt.plot(time_t,(island_width-island_width[0])/island_width[0],label='D_width/width_0')
plt.plot(time_t,(island_length-island_length[0])/island_length[0],label='D_length/length_0')
plt.plot(time_t,(island_surface-island_surface[0])/island_surface[0],label='D_surface/surface_0')
plt.xlabel('t');plt.ylabel('relative variation of feauture');plt.title('relative variations of the island')
plt.legend()
plt.savefig(opath+'/'+out_dir+'/../'+'island_growth_'+run_name+'_'+str(ind1)+'to'+str(ind2)+'.png')
plt.close()
#"""

#uniforming PSDs
mx = mx_t[0]
my = my_t[0]
nmx = len(mx)
nmy = len(my)
PSDx = np.empty((nt,3,nmx),dtype=np.float64)
PSDy = np.empty((nt,3,nmy),dtype=np.float64)
for i in range(nt):
    PSDx[i] = PSDx_t[i]
    PSDy[i] = PSDy_t[i]

flag = True
cnt = 0
while flag:
    plt.close()
    for i in range(nmx//2):
        if mx[i] < 1+cnt*10:
            continue
        if mx[i] > (1+cnt)*10:
            break
        plt.plot(time_t,PSDx[:,0,i]+PSDx[:,1,i]+PSDx[:,2,i],label=str(int(round(mx[i]))))
    plt.legend();plt.yscale('log')
    plt.xlabel('t');plt.ylabel('PSD');plt.title('mx='+str(int(round(mx[1+cnt*10])))+' -> '+str(int(round(mx[(1+cnt)*10])))+' - '+wname)
    plt.savefig(opath+'/'+out_dir+'/'+'windowed_island_'+wname+'_'+run_name+'_modes_x_'+str(1+cnt*10)+'to'+str((1+cnt)*10)+'.png')
    plt.close()
    cnt +=1
    if 1+cnt*10 >= nmx//2:
        flag = False

flag = True
cnt = 0
while flag:
    plt.close()
    for i in range(nmy//2):
        if my[i] < 1+cnt*10:
            continue
        if my[i] > (1+cnt)*10:
            break
        plt.plot(time_t,PSDy[:,0,i]+PSDy[:,1,i]+PSDy[:,2,i],label=str(int(round(my[i]))))
    plt.legend();plt.yscale('log')
    plt.xlabel('t');plt.ylabel('PSD');plt.title('my='+str(int(round(my[1+cnt*10])))+' -> '+str(int(round(my[(1+cnt)*10])))+' - '+wname)
    plt.savefig(opath+'/'+out_dir+'/'+'windowed_island_'+wname+'_'+run_name+'_modes_y_'+str(1+cnt*10)+'to'+str((1+cnt)*10)+'.png')
    plt.close()
    cnt +=1
    if 1+cnt*10 >= nmy//2:
        flag = False

