#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot directional kinetic energy per species\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
out_dir = 'directional_Kenergy/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']
if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

#params
g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

idtens = np.zeros((3,3,nx,ny,nz),dtype=np.float64)
for i in range(3):
    idtens[i,i] = 1.

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
        J = np.array([cx,cy,cz]) # J = rot(B)
        del cx,cy,cz

    #densities and currents
    Kx = {}
    Ky = {}
    Kz = {}

    n_p,u_p = run.get_Ion(ind)
    Kx['p'] = 0.5*n_p*u_p[0]**2
    Ky['p'] = 0.5*n_p*u_p[1]**2
    Kz['p'] = 0.5*n_p*u_p[2]**2
    if code_name == 'HVM':
        n_e = n_p.copy() # n_e = n_p = n (no charge separation)
        u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0],n_p)
        u_e[1] = u_p[1] - np.divide(J[1],n_p)
        u_e[2] = u_p[2] - np.divide(J[2],n_p)
        del J
        m_e = 1./run.meta['mime']
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
        m_e = 1./abs(qom_e)
    Kx['e'] = 0.5*n_e*u_e[0]**2*m_e
    Ky['e'] = 0.5*n_e*u_e[1]**2*m_e
    Kz['e'] = 0.5*n_e*u_e[2]**2*m_e
    del n_p,u_p,n_e,u_e
    
    if code_name == 'iPIC':
        stag = run.meta['stag']
        sQOM = run.meta['sQOM']
        m_s = {}
        for i,s in enumerate(stag):
            n_s,u_s = run.get_Spec(ind,s)
            m_s[s] = 1./abs(sQOM[i])
            Kx[s] = 0.5*n_s*u_s[0]**2*m_s[s]
            Ky[s] = 0.5*n_s*u_s[1]**2*m_s[s]
            Kz[s] = 0.5*n_s*u_s[2]**2*m_s[s]
        del n_s,u_s

    #plots
    pkeys = ['p']
    ekeys = ['e']
    if code_name == 'iPIC':
        for i,s in enumerate(stag):
            if sQOM[i] < 0.:
                ekeys.append(s)
            else:
                pkeys.append(s)

    bmin = {'x':[],'y':[],'z':[]}
    bmax = {'x':[],'y':[],'z':[]}
    for k in pkeys:
        bmin['x'].append(np.min(Kx[k]))
        bmax['x'].append(np.max(Kx[k]))
        bmin['y'].append(np.min(Ky[k]))
        bmax['y'].append(np.max(Ky[k]))
        bmin['z'].append(np.min(Kz[k]))
        bmax['z'].append(np.max(Kz[k]))
    bxp = np.linspace(min(bmin['x']),max(bmax['x']),250+1)
    byp = np.linspace(min(bmin['y']),max(bmax['y']),250+1)
    bzp = np.linspace(min(bmin['z']),max(bmax['z']),250+1)

    bmin = {'x':[],'y':[],'z':[]}
    bmax = {'x':[],'y':[],'z':[]}
    for k in ekeys:
        bmin['x'].append(np.min(Kx[k]))
        bmax['x'].append(np.max(Kx[k]))
        bmin['y'].append(np.min(Ky[k]))
        bmax['y'].append(np.max(Ky[k]))
        bmin['z'].append(np.min(Kz[k]))
        bmax['z'].append(np.max(Kz[k]))
    bxe = np.linspace(min(bmin['x']),max(bmax['x']),250+1)
    bye = np.linspace(min(bmin['y']),max(bmax['y']),250+1)
    bze = np.linspace(min(bmin['z']),max(bmax['z']),250+1)

    plt.close()
    ncol = 2
    nrow = 3
    fig,ax = plt.subplots(nrow,ncol,figsize=(10*ncol,4*nrow))

    ax_ = ax[0,0]#x,p
    for k in pkeys:
        if k == '5': continue
        _,_,_ = ax_.hist(np.array(Kx[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxp,histtype='step',log=True,label='$K_{x,%s}$'%(k))
    ax_.set_xlabel('$K_x$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()
    ax_.set_title('%s - directional kinetic energy - $t=%f$'%(run_label,time[ind]))

    ax_ = ax[0,1]#x,e
    for k in ekeys:
        if k == '2': continue
        _,_,_ = ax_.hist(np.array(Kx[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxe,histtype='step',log=True,label='$K_{x,%s}$'%(k))
    ax_.set_xlabel('$K_x$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax[1,0]#y,p
    for k in pkeys:
        if k == '5': continue
        _,_,_ = ax_.hist(np.array(Ky[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=byp,histtype='step',log=True,label='$K_{y,%s}$'%(k))
    ax_.set_xlabel('$K_y$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax[1,1]#y,e
    for k in ekeys:
        if k == '2': continue
        _,_,_ = ax_.hist(np.array(Ky[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bye,histtype='step',log=True,label='$K_{y,%s}$'%(k))
    ax_.set_xlabel('$K_y$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax[2,0]#z,p
    for k in pkeys:
        if k == '5': continue
        _,_,_ = ax_.hist(np.array(Kz[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bzp,histtype='step',log=True,label='$K_{z,%s}$'%(k))
    ax_.set_xlabel('$K_z$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax[2,1]#z,e
    for k in ekeys:
        if k == '2': continue
        _,_,_ = ax_.hist(np.array(Kz[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bze,histtype='step',log=True,label='$K_{z,%s}$'%(k))
    ax_.set_xlabel('$K_z$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'dir_Kenergy_'+run_name+'_'+str(ind)+'.png')
    plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
