#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
#mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot something\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
out_dir = '2d_plots/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']

out_dir = out_dir+'_gausspost2'#+'_Xzoom'#+'_gauss2'#+'_Xzoom_gauss2'
ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

#params
g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

idtens = np.zeros((3,3,nx,ny,nz),dtype=np.float64)
for i in range(3):
    idtens[i,i] = 1.

#------------------
def my_FFT_smooth(field,keep_percentage,gauss=True):
    if not gauss:
        nx,ny = field.shape
    #
        mx_cut = int(round(float(nx//2+1)*keep_percentage[0]))
        my_cut = int(round(float(ny//2+1)*keep_percentage[1]))
    #
        f = np.empty((nx,ny//2+1),dtype=np.complex128)
        f = np.fft.rfft2(field)
        f[mx_cut:nx//2+1,:] = 0.
        if nx%2 == 0:
            f[nx//2+1:nx//2+1+nx//2-mx_cut,:] = 0.
        else:
            f[nx//2+1:nx//2+1+nx//2-mx_cut+1,:] = 0.
        f[:,my_cut:] = 0.
        field_ = np.empty((nx,2*(ny//2)),np.double)
        field_ = np.fft.irfft2(f)
    else:
        field_ = gf(field,[2,2],mode='wrap')
#
    return field_

kp = [3./5.,3./5.]

smooth_flag = False #True
smooth_flag_post = True

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    E,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = my_FFT_smooth(B[i,:,:,0],kp)

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
        J = np.array([cx,cy,cz]) # J = rot(B)
        del cx,cy,cz

    Bn = np.sqrt(B[0]**2+B[1]**2+B[2]**2) # Bn = |B|
#   b = np.empty((3,nx,ny,nz),dtype=np.float64) # b = B/|B|
#   for i in range(3):
#       b[i] = np.divide(B[i],Bn)
#   bb = np.empty((3,3,nx,ny,nz),dtype=np.float64) # bb = b (x) b
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       bb[i,j] = b[i]*b[j]
#   id_bb = np.empty((3,3,nx,ny,nz),dtype=np.float64) # id_bb = id - bb
#   id_bb = idtens - bb

    #densities and currents
    n_p,u_p = run.get_Ion(ind)
    if code_name == 'iPIC' and smooth_flag:
        n_p[:,:,0] = my_FFT_smooth(n_p[:,:,0],kp)
        for i in range(3): u_p[i,:,:,0] = my_FFT_smooth(u_p[i,:,:,0],kp)
    if code_name == 'HVM':
        n_e = n_p.copy() # n_e = n_p = n (no charge separation)
        u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0],n_p)
        u_e[1] = u_p[1] - np.divide(J[1],n_p)
        u_e[2] = u_p[2] - np.divide(J[2],n_p)
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
        if smooth_flag:
            n_e[:,:,0] = my_FFT_smooth(n_e[:,:,0],kp)
            for i in range(3): u_e[i,:,:,0] = my_FFT_smooth(u_e[i,:,:,0],kp)
        J = n_p*u_p - n_e*u_e

    #Pressures
#   Pp = run.get_Press(ind)
#   if code_name == 'iPIC' and smooth_flag:
#       for i in range(3): 
#           for j in range(3): Pp[i,j,:,:,0] = my_FFT_smooth(Pp[i,j,:,:,0],kp)
#   pparlp,pperpp = ml.proj_tens_on_vec(Pp,B) # pparlp = Pp:bb
#                                             # pperpp = Pp:(id - bb)/2
#   Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3. # Pisop = Tr(Pp)/3

#   if not w_ele:
#       Pisoe = n_e*run.meta['beta']*0.5*run.meta['teti'] # Pisoe = n_e * T_e,0
#       Pe = np.zeros((3,3,nx,ny,nz),dtype=np.float64) # Pe = Pisoe*id
#       for i in range(3):
#           Pe[i,i] = Pisoe.copy()
#       pparle = Pisoe # ppalre = Pisoe
#       pperpe = pparle # pperpe = Pisoe
#   else:
#       if code_name == 'HVM':
#           tparle,tperpe = run.get_Te(ind)
#           pparle = tparle*n_e # pparle = n_e*tparle
#           pperpe = tperpe*n_e # pperpe = n_e*tperpe
#           Pisoe = (pparle + 2.*pperpe)/3. # Pisoe  = (pparle + 2*pperpe)/3
#           Pe = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pe = pparle*bb + pperpe*(id - bb)
#           for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#               Pe[i,j] = pparle*bb[i,j] + pperpe*id_bb[i,j]
#       elif code_name == 'iPIC':
#           Pe = run.get_Press(ind,qom=qom_e)
#           if smooth_flag:
#               for i in range(3):
#                   for j in range(3): Pe[i,j,:,:,0] = my_FFT_smooth(Pe[i,j,:,:,0],kp)
#           pparle,pperpe = ml.proj_tens_on_vec(Pe,B) # pparle = Pe:bb
#                                                     # pperpe = Pe:(id - bb)/2
#           Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.  # Pisoe = Tr(Pe)/3

#   Pnp = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pnp = Pp - [pparlp*bb + pperpp*(id - bb)]
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       Pnp[i,j] = Pp[i,j] - (pparlp*bb[i,j] + pperpp*id_bb[i,j])
#   if code_name == 'iPIC':
#       Pne = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pne = Pe - [pparle*bb + pperpe*(id - bb)]
#       for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#           Pne[i,j] = Pe[i,j] - (pparle*bb[i,j] + pperpe*id_bb[i,j])

#   #Heat fluxes
#   if code_name == 'HVM':
#       sparlp,sperpp = run.get_Q(ind) # sparlp = Qp:bb
#                                      # sperpp = Qp:(id - bb)/2
#       qp = 0.5*sparlp + sperpp # qp = Qp:id/2 = sparlp/2 + sperpp
#       Sp = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # Goswami 2005, eq. 7, S = S(sparl,sperp)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           Sp[i,j,k] = ( 0.5*(sperpp[i]*id_bb[j,k] + sperpp[j]*id_bb[i,k] + sperpp[k]*id_bb[i,j])
#                            + sparlp[i]*bb[j,k]    + sparlp[j]*bb[i,k]    + sparlp[k]*bb[i,j] )
#           for l in range(3):
#               Sp[i,j,k] += ( 0.5*(sperpp[l]*bb[l,i]*id_bb[j,k]
#                                 + sperpp[l]*bb[l,j]*id_bb[i,k]
#                                 + sperpp[l]*bb[l,k]*id_bb[i,j])
#                        - (2./3.)*(sparlp[l]*bb[l,i]*bb[j,k]
#                                 + sparlp[l]*bb[l,j]*bb[i,k]
#                                 + sparlp[l]*bb[l,k]*bb[i,j]) )
#   elif code_name == 'iPIC':
#       qp = run.get_Q(ind) # qp = Qp:id/2
#       if smooth_flag:
#           for i in range(3): qp[i,:,:,0] = my_FFT_smooth(qp[i,:,:,0],kp)
#       Sp = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # S'_ijk = 
#                                                        # = 2/5*(q_i*id_jk + q_j*id_ik + q_k*id_ij)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           Sp[i,j,k] = (2./5.)*(qp[i]*idtens[j,k] + qp[j]*idtens[i,k] + qp[k]*idtens[i,j])
#   if w_ele:
#       if code_name == 'HVM':
#           qparle,qperpe = run.get_Qe(ind) # qparle = Qe:.bbb
#                                           # qperpe = Qe:.(id - bb)b/2
#           sparle = np.empty((3,nx,ny,nz),dtype=np.float64) # sparle = qparle*b
#           sperpe = np.empty((3,nx,ny,nz),dtype=np.float64) # sperpe = qperpe*b
#           for i in range(3):
#               sparle[i] = qparle*b[i]
#               sperpe[i] = qperpe*b[i]
#           qe = 0.5*sparle + sperpe # qe = sparle/2 + sperpe
#           Se = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # Goswami 2005, eq. 7, S = S(sparl,sperp)
#           for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#               Se[i,j,k] = ( 0.5*(sperpe[i]*id_bb[j,k] + sperpe[j]*id_bb[i,k] + sperpe[k]*id_bb[i,j])
#                                + sparle[i]*bb[j,k]    + sparle[j]*bb[i,k]    + sparle[k]*bb[i,j] )
#               for l in range(3):
#                   Se[i,j,k] += ( 0.5*(sperpe[l]*bb[l,i]*id_bb[j,k]
#                                     + sperpe[l]*bb[l,j]*id_bb[i,k]
#                                     + sperpe[l]*bb[l,k]*id_bb[i,j])
#                            - (2./3.)*(sparle[l]*bb[l,i]*bb[j,k]
#                                     + sparle[l]*bb[l,j]*bb[i,k]
#                                     + sparle[l]*bb[l,k]*bb[i,j]) )
#       elif code_name == 'iPIC':
#           qe = run.get_Q(ind,qom=qom_e) # qe = Qe:id/2
#           if smooth_flag:
#               for i in range(3): qe[i,:,:,0] = my_FFT_smooth(qe[i,:,:,0],kp)
#           Se = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # S'_ijk =
#                                                            # = 2/5*(q_i*id_jk + q_j*id_ik + q_k*id_ij)
#           for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#               Se[i,j,k] = (2./5.)*(qe[i]*idtens[j,k] + qe[j]*idtens[i,k] + qe[k]*idtens[i,j])
#   else:
#       #qe = np.empty((3,nx,ny,nz),dtype=np.float64) # qe = 3/2*n*T_e,0*u_e
#       #for i in range(3):
#       #    qe[i] = (3./2.)*n_e*run.meta['beta']*0.5*run.meta['teti']*u_e[i]
#       qe = np.zeros((3,nx,ny,nz),dtype=np.float64) # qe = 0
#       Se = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # S'_ijk =
#                                                        # = 2/5*(q_i*id_jk + q_j*id_ik + q_k*id_ij)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           Se[i,j,k] = (2./5.)*(qe[i]*idtens[j,k] + qe[j]*idtens[i,k] + qe[k]*idtens[i,j])

#   #get entropies
#   Sfp = (3./2.)*np.log(np.divide(Pisop,n_p**g_adiab)) # Sfp = 3/2*ln( Pisop/n_p^(5/3) )
#   Sfe = (3./2.)*np.log(np.divide(Pisoe,n_e**g_adiab)) # Sfe = 3/2*ln( Pisoe/n_e^(5/3) )

#   Sgp = (3./2.)*np.log(np.divide((pparlp**(1./3.))*(pperpp**(2./3.)),n_p**g_adiab)) #
#                   # Sgp = (3/2)*ln( ( pparlp^(1/3) * pperpp^(2/3) )/( n_p^(5/3) ) )
#   if w_ele:
#       Sge = (3./2.)*np.log(np.divide((pparle**(1./3.))*(pperpe**(2./3.)),n_e**g_adiab)) #
#                       # Sge = (3/2)*ln( ( pparle^(1/3) * pperpe^(2/3) )/( n_e^(5/3) ) )

    #in-plane alfven mach number
#   if code_name == 'HVM':
#       v_A = np.divide(Bn,np.sqrt(n_p))
#   elif code_name == 'iPIC':
#       v_A = np.divide(Bn,np.sqrt(4.*np.pi*n_p))
#   else:
#       print('\nERROR: code_name unknown\n')
#   MAxy_p = np.divide(np.sqrt(u_p[0]**2+u_p[1]**2),v_A)
#   MAxy_e = np.divide(np.sqrt(u_e[0]**2+u_e[1]**2),v_A)
#   del v_A

#   #in-plane electron velocity
#   u_e_xy = np.sqrt(u_e[0]**2+u_e[1]**2)

    #charge separation
    ch_sep = 1. - np.divide(n_e,n_p)
    ch_sep_uf = ch_sep.copy()
    if code_name == 'iPIC' and smooth_flag_post:
        ch_sep[:,:,0] = my_FFT_smooth(ch_sep[:,:,0],kp)

    #---------------------------------------------
    #plots
    # -> |u_p_xy|/v_A, |u_e_xy|/v_A
    #plt.close()
    #if w_ele:
    #    nplot = 2
    #else:
    #    nplot = 1
    #fig,ax = plt.subplots(nplot,1,figsize=(15,5*nplot))

    #if w_ele:
    #    ax0 = ax[0]
    #else:
    #    ax0 = ax
    #im0 = ax0.contourf( x[ixmin:ixmax],y[iymin:iymax],MAxy_p[ixmin:ixmax,iymin:iymax,0].T,32 )
    #plt.colorbar(im0,ax=ax0)
    #ax0.contour( x[ixmin:ixmax],y[iymin:iymax],
    #             Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno' )
    #ax0.set_xlabel('x');ax0.set_ylabel('y')
    #ax0.set_title('in-plane protons alfven mach number - t='+str(time[ind]))

    #if w_ele:
    #    im1 = ax[1].contourf( x[ixmin:ixmax],y[iymin:iymax],MAxy_e[ixmin:ixmax,iymin:iymax,0].T,32 )
    #    plt.colorbar(im1,ax=ax[1])
    #    ax[1].contour( x[ixmin:ixmax],y[iymin:iymax],
    #                   Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno' )
    #    ax[1].set_xlabel('x');ax[1].set_ylabel('y')
    #    ax[1].set_title('in-plane electrons alfven mach number - t='+str(time[ind]))

    #plt.tight_layout()
    #plt.savefig(opath+'/'+out_dir+'/'+'inplane_MA_'+run_name+'_'+str(ind)+'.png')
    #plt.close()

    # -> Psi,Jz - Psi,Bz - Psi,vexy
#   plt.close()
#   fig,ax = plt.subplots(3,1,figsize=(15,5*3))

#   if run_name.split('_')[0] == 'DH':
#       norm = 4.*np.pi*100.
#   else:
#       norm = 1.
#   im0 = ax[0].contourf(x[ixmin:ixmax],y[iymin:iymax],norm*J[2,ixmin:ixmax,iymin:iymax,0].T,32)
#   plt.colorbar(im0,ax=ax[0])
#   ax[0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='inferno')
#   ax[0].set_xticks([]);ax[0].set_ylabel('y')
#   ax[0].set_title('$J_z$ - t='+str(time[ind])+'$\Omega_C^{-1}$')

#   if run_name.split('_')[0] == 'DH':
#       norm = 100.
#   else:
#       norm = 1.
#   im1 = ax[1].contourf(x[ixmin:ixmax],y[iymin:iymax],norm*B[2,ixmin:ixmax,iymin:iymax,0].T,32)
#   plt.colorbar(im1,ax=ax[1])
#   ax[1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='inferno')
#   ax[1].set_xticks([]);ax[1].set_ylabel('y')
#   ax[1].set_title('$B_z$ - t='+str(time[ind])+'$\Omega_C^{-1}$')

#   if run_name.split('_')[0] == 'DH':
#       norm = 100.
#   else:
#       norm = 1.
#   im2 = ax[2].contourf(x[ixmin:ixmax],y[iymin:iymax],norm*u_e_xy[ixmin:ixmax,iymin:iymax,0].T,32)
#   plt.colorbar(im2,ax=ax[2])
#   ax[2].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='pink')
#   ax[2].set_xlabel('x');ax[2].set_ylabel('y')
#   ax[2].set_title('In-plane electron velocity - t='+str(time[ind])+'$\Omega_C^{-1}$')

#   plt.tight_layout()
#   #plt.show()
#   plt.savefig(opath+'/'+out_dir+'/'+'Jz_Bz_uexy_'+run_name+'_'+str(ind)+'.png')
#   plt.close()

    # -> 1-n_e/n_p (PIC)
    if code_name == 'iPIC':
        plt.close()
        fig,ax = plt.subplots(1,1,figsize=(15,5))

        im0 = ax.contourf(x[ixmin:ixmax],y[iymin:iymax],ch_sep[ixmin:ixmax,iymin:iymax,0].T,32)
        plt.colorbar(im0,ax=ax)
        ax.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='inferno')
        #ax.set_xticks([])
        ax.set_ylabel('y')
        ax.set_title('$1-n_e/n_p$ - t='+str(time[ind])+'$\Omega_C^{-1}$')

        plt.tight_layout()
        #plt.show()
        plt.savefig(opath+'/'+out_dir+'/'+'charge_sep_'+run_name+'_'+str(ind)+'.png')
        plt.close()

    # -> 1-n_e/n_p pdf (PIC)
    if code_name == 'iPIC':
        plt.close()
        fig,ax = plt.subplots(1*2,1,figsize=(15,5*2))

        h0,b0,im0 = ax[0].hist(np.array(ch_sep.flat),bins=500,density=False,histtype='step',log=True)
        yL0 = int(float(np.max(h0))/1000.)
        yH0 = np.max(h0)
        xL0 = b0[np.argmax(h0>yL0)]
        xH0 = b0[len(h0)-1-np.argmax(h0[::-1]>yL0)]
        ax[0].set_ylim(yL0,yH0)
        ax[0].set_xlim(xL0,xH0)
        ax[0].set_xlabel('$1-n_e/n_p$');ax[0].set_ylabel('counts (log)')
        ax[0].set_title('charge separation - t='+str(time[ind])+'$\Omega_C^{-1}$')

        h0,b0,im2 = ax[1].hist(np.array(ch_sep.flat),bins=500,density=False,histtype='step',log=True)
        yL0 = int(float(np.max(h0))/1000.)
        yH0 = np.max(h0)
        xL0 = b0[np.argmax(h0>yL0)]
        xH0 = b0[len(h0)-1-np.argmax(h0[::-1]>yL0)]
        h1,b1,im3 = ax[1].hist(np.array(ch_sep_uf.flat),bins=500,density=False,histtype='step',log=True)
        yL1 = int(float(np.max(h1))/1000.)
        yH1 = np.max(h1)
        xL1 = b1[np.argmax(h1>yL1)]
        xH1 = b1[len(h1)-1-np.argmax(h1[::-1]>yL1)]
        ax[1].set_ylim(min(yL0,yL1),max(yH0,yH1))
        ax[1].set_xlim(min(xL0,xL1),max(xH0,xH1))
        ax[1].set_xlabel('$1-n_e/n_p$');ax[1].set_ylabel('counts (log)')
        ax[1].set_title('charge separation - t='+str(time[ind])+'$\Omega_C^{-1}$')

        plt.tight_layout()
        #plt.show()
        plt.savefig(opath+'/'+out_dir+'/'+'charge_sep_'+run_name+'_'+str(ind)+'.png')
        plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
