#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
import gc
import os
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter as gf
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of Jz, Bz, and Ve in plane.\n')

#------------------------------------------------------
#Init
#----
input_files = ['HVM_2d_DH.dat','LF_2d_DH.dat','DH_run11_data0.dat']

plt_show_flg = False
zoom_flg = True
rms_flg = False

Bz = {}
Jz = {}
Psi = {}
Veplane = {}
t = {}
x = {}
y = {}

run_name_vec = []
run_labels = {}
ind_dict = {}
master_flag = True
for ii,i_f in zip(range(len(input_files)),input_files):
    run,tr,xr=wl.Init(i_f)
    ind,_,_ = tr
    ixmin,ixmax,iymin,iymax = xr
    run_name = run.meta['run_name']
    run_name_vec.append(run_name)
    ind_dict[run_name] = ind
    w_ele = run.meta['w_ele']
    times = run.meta['time']*run.meta['tmult']
    x_ = run.meta['x']
    y_ = run.meta['y']
    code_name = run.meta['code_name']
#
    if code_name == 'HVM':
        smooth_flag = False
        calc = fibo('calc')
        calc.meta = run.meta
        if w_ele:
            run_labels[run_name] = 'HVLF'
        else:
            run_labels[run_name] = 'HVM'
        B0 = 1.
        n0 = 1.
        V0 = 1.
    elif code_name == 'iPIC':
        run_labels[run_name] = 'iPIC'
        smooth_flag = True
        qom_e = run.meta['msQOM'][0]
        gfsp = 2
        gfse = 2
        B0 = 0.01
        n0 = 1./(4.*np.pi)
        V0 = 0.01
    else:
        print('ERROR: unknown code_name %s'%(code_name))
        sys.exit(-1)
#
    if master_flag:
        master_flag = False
        opath = run.meta['opath']
#
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
#
    Psi_ = wl.psi_2d(B[:-1,...,0],run.meta)
    if smooth_flag:
        Psi_ = gf(Psi_,[gfsp,gfsp],mode='wrap')
    Psi[run_name] = Psi_[ixmin:ixmax,iymin:iymax]/B0
    del Psi_
#
    Bz_ = B[2,:,:,0] - run.meta['Bz0']
    if smooth_flag:
        Bz_ = gf(Bz_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        Bz_rms = np.sqrt( np.mean( Bz_[ixmin:ixmax,iymin:iymax]*Bz_[ixmin:ixmax,iymin:iymax] ) )###
        Bz[run_name] = Bz_[ixmin:ixmax,iymin:iymax]/Bz_rms###
    else:
        Bz[run_name] = Bz_[ixmin:ixmax,iymin:iymax]/B0###
    del Bz_
#
    n_p,u_p = run.get_Ion(ind)
    if code_name == 'HVM':
        Jx_,Jy_,Jz_ = calc.calc_curl(B[0],B[1],B[2])
        u_ex_ = u_p[0] - np.divide(Jx_,n_p)
        u_ey_ = u_p[1] - np.divide(Jy_,n_p)
        u_eplane = np.sqrt(u_ex_**2+u_ey_**2)
        del u_ex_,u_ey_,Jx_,Jy_
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
        Jz_ = n_p*u_p[2] - n_e*u_e[2]
        u_eplane = np.sqrt(u_e[0]**2+u_e[1]**2)
        del n_e,u_e
    del n_p,u_p,B
#
    Jz_ = Jz_[...,0]
    if smooth_flag:
        Jz_ = gf(Jz_,[gfsp,gfsp],mode='wrap')
    if rms_flg:
        Jz_rms = np.sqrt( np.mean( Jz_[ixmin:ixmax,iymin:iymax]*Jz_[ixmin:ixmax,iymin:iymax] ) )###
        Jz[run_name] = Jz_[ixmin:ixmax,iymin:iymax]/Jz_rms###
    else:
        Jz[run_name] = Jz_[ixmin:ixmax,iymin:iymax]/(n0*V0)###
    del Jz_
#
    u_eplane = u_eplane[...,0]
    if smooth_flag:
        u_eplane = gf(u_eplane,[gfse,gfse],mode='wrap')
    if rms_flg:
        u_eplane_rms = np.sqrt( np.mean( 
                       u_eplane[ixmin:ixmax,iymin:iymax]*u_eplane[ixmin:ixmax,iymin:iymax] ) )###
        Veplane[run_name] = u_eplane[ixmin:ixmax,iymin:iymax]/u_eplane_rms###
    else:
        Veplane[run_name] = u_eplane[ixmin:ixmax,iymin:iymax]/V0###
    del u_eplane
#
    t[run_name] =  times[ind]
    del times
#
    x[run_name] = x_[ixmin:ixmax]
    y[run_name] = y_[iymin:iymax]
    del x_,y_
#
    gc.collect()

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
fig,ax = plt.subplots(3,3,figsize=(16,10),sharex=True,sharey=True)
fig.subplots_adjust(hspace=.06,wspace=.03,top=.95,bottom=.1,left=.055,right=1.075)#.5#1.07

vminJz = ml.min_dict(Jz)
vmaxJz = ml.max_dict(Jz)
vmin = ml.min_dict(Bz)
vmax = ml.max_dict(Bz)
vminBz = -max(vmax,-vmin)
vmaxBz = max(vmax,-vmin)
vminVeplane = ml.min_dict(Veplane)
vmaxVeplane = ml.max_dict(Veplane)
for j,run_name in enumerate(run_name_vec):
    i = 0
    im1 = ax[i,j].pcolormesh(x[run_name],y[run_name],Jz[run_name].T,shading='gouraud',
                             vmin=vminJz,vmax=vmaxJz,cmap='Reds_r')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[i,j].set_title(run_labels[run_name])
#
    i = 1
    im2 = ax[i,j].pcolormesh(x[run_name],y[run_name],Bz[run_name].T,shading='gouraud',
                           vmin=vminBz,vmax=vmaxBz,cmap='PuOr')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
#
    i = 2
    im3 = ax[i,j].pcolormesh(x[run_name],y[run_name],Veplane[run_name].T,shading='gouraud',
                          vmin=vminVeplane,vmax=vmaxVeplane,cmap='Greens')
    ax[i,j].contour(x[run_name],y[run_name],Psi[run_name].T,8,colors='black')
    ax[i,j].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')

cb1 = plt.colorbar(im1,ax=ax[0,:],pad=.007)
if rms_flg:
    label_ = '$J_z / \overline{J_z}$'
else:
    label_ = '$J_z / ( e n_0 v_A )$'
cb1.set_label(label_,rotation=270,labelpad=25)

cb2 = plt.colorbar(im2,ax=ax[1,:],pad=.007)
if rms_flg:
    label_ = '$\Delta B_z / \overline{B_z}$'
else:
    label_ = '$\Delta B_z / B_0$'
cb2.set_label(label_,rotation=270,labelpad=20)#20)

cb3 = plt.colorbar(im3,ax=ax[2,:],pad=.007)
if rms_flg:
    label_ = '$u_{\mathrm{e}}^{(\\text{plane})} / \overline{u_e^{(\\text{plane})}}$'
else:
    label_ = '$u_{\mathrm{e}}^{(\\text{plane})} / v_A$'
cb3.set_label(label_,rotation=270,labelpad=45)#40)

if plt_show_flg:
    plt.show()

out_dir = 'plot_2d_Jz_Bz_Veplane'
ml.create_path(opath+'/'+out_dir)
out_dir += '/comp'
fig_name = 'Jz_Bz_Veplane'
for run_name in run_name_vec:
    out_dir += '__' + run_name
    fig_name += '__' + run_labels[run_name] + '_ind%d'%ind_dict[run_name]

if zoom_flg:
    fig_name += '__zoom'
if rms_flg:
    fig_name += '__rms'
fig_name += '.png'
ml.create_path(opath+'/'+out_dir)
fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
