#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
sys.path.append('/home/finelli/Documents/templates')
from histABC_wrapped import hist2d_wrap as h2dw

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nCompute fluctuations around the equilibrium and plot them in windlike plot.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'fluct_in_wind_v3/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

calc = fibo('calc')
calc.meta=run.meta

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#------------
mode = 'ABC'
logscale_flg = True

#see Hellinger et al. 2006
#    Astfalk and Jenko 2016
#    Lazar et al. 2015
params = {}
params['H2006'] = {}
params['A2016'] = {}
params['L2015'] = {}

params['H2006']['PC'] = {}
params['H2006']['PC']['10m3'] = {}
params['H2006']['PC']['10m3']['bM'] = [ 0.43,0.42,-0.0004]

params['L2015']['EC'] = {}
params['L2015']['EC']['e05'] = {}
params['L2015']['EC']['e05']['10m2'] = {}
params['L2015']['EC']['e05']['10m2']['k2'] = {}
params['L2015']['EC']['e05']['10m2']['k2']['T'] = [0.54,0.21,0.14,1.28]
params['L2015']['EC']['e05']['10m2']['k2']['C'] = [0.26,-0.01,1.18,0.56]
params['L2015']['EC']['e05']['10m2']['k6'] = {}
params['L2015']['EC']['e05']['10m2']['k6']['T'] = [0.43,0.17,0.02,2.04]
params['L2015']['EC']['e05']['10m2']['k6']['C'] = [0.19,-0.09,1.11,0.63]
params['L2015']['EC']['e05']['10m2']['bM'] = {}
params['L2015']['EC']['e05']['10m2']['bM']['T'] = [0.40,0.10,0.05,1.26]
params['L2015']['EC']['e05']['10m2']['bM']['C'] = [0.17,-0.11,1.18,0.65]
params['L2015']['EC']['e05']['10m3'] = {}
params['L2015']['EC']['e05']['10m3']['k2'] = {}
params['L2015']['EC']['e05']['10m3']['k2']['T'] = [0.10,0.30,0.09,1.22]
params['L2015']['EC']['e05']['10m3']['k2']['C'] = [0.01,-0.46,19.1,0.97]
params['L2015']['EC']['e05']['10m3']['k6'] = {}
params['L2015']['EC']['e05']['10m3']['k6']['T'] = [0.08,0.38,0.003,2.54]
params['L2015']['EC']['e05']['10m3']['k6']['C'] = [0.005,-0.52,14.6,1.07]
params['L2015']['EC']['e05']['10m3']['bM'] = {}
params['L2015']['EC']['e05']['10m3']['bM']['T'] = [0.07,0.31,0.02,1.23]
params['L2015']['EC']['e05']['10m3']['bM']['C'] = [0.003,-0.67,21.3,1.23]
params['L2015']['EC']['e01'] = {}
params['L2015']['EC']['e01']['10m2'] = {}
params['L2015']['EC']['e01']['10m2']['k2'] = {}
params['L2015']['EC']['e01']['10m2']['k2']['T'] = [1.74,0.05,0.04,1.29]
params['L2015']['EC']['e01']['10m2']['k2']['C'] = [1.35,-0.04,0.30,0.60]
params['L2015']['EC']['e01']['10m2']['k6'] = {}
params['L2015']['EC']['e01']['10m2']['k6']['T'] = [1.54,0.06,0.003,1.86]
params['L2015']['EC']['e01']['10m2']['k6']['C'] = [1.27,-0.03,0.16,0.62]
params['L2015']['EC']['e01']['10m2']['bM'] = {}
params['L2015']['EC']['e01']['10m2']['bM']['T'] = [1.47,0.05,0.002,2.03]
params['L2015']['EC']['e01']['10m2']['bM']['C'] = [1.23,-0.03,0.14,0.63]
params['L2015']['EC']['e01']['10m3'] = {}
params['L2015']['EC']['e01']['10m3']['k2'] = {}
params['L2015']['EC']['e01']['10m3']['k2']['T'] = [0.21,0.12,0.04,1.28]
params['L2015']['EC']['e01']['10m3']['k2']['C'] = [0.11,-0.07,0.84,0.60]
params['L2015']['EC']['e01']['10m3']['k6'] = {}
params['L2015']['EC']['e01']['10m3']['k6']['T'] = [0.18,0.14,0.001,2.21]
params['L2015']['EC']['e01']['10m3']['k6']['C'] = [0.11,-0.06,0.47,0.63]
params['L2015']['EC']['e01']['10m3']['bM'] = {}
params['L2015']['EC']['e01']['10m3']['bM']['T'] = [0.17,0.15,0.0001,2.83]
params['L2015']['EC']['e01']['10m3']['bM']['C'] = [0.11,-0.05,0.40,0.64]

params['H2006']['MR'] = {}
params['H2006']['MR']['10m3'] = {}
params['H2006']['MR']['10m3']['bM'] = [ 0.77,0.76,-0.0160]

params['H2006']['PF'] = {}
params['H2006']['PF']['10m3'] = {}
params['H2006']['PF']['10m3']['bM'] = [-0.47,0.53, 0.5900]

params['H2006']['OF'] = {}
params['H2006']['OF']['10m3'] = {}
params['H2006']['OF']['10m3']['bM'] = [-1.40,1.00,-0.1100]

params['A2016']['PF'] = {}
params['A2016']['PF']['10m3'] = {}
params['A2016']['PF']['10m3']['bM']  = [-0.487,0.537, 0.560]
params['A2016']['PF']['10m3']['k12'] = [-0.438,0.475, 0.503]
params['A2016']['PF']['10m3']['k8']  = [-0.429,0.486, 0.423]
params['A2016']['PF']['10m3']['k6']  = [-0.417,0.498, 0.350]
params['A2016']['PF']['10m3']['k4']  = [-0.387,0.518, 0.226]
params['A2016']['PF']['10m3']['k2']  = [-0.274,0.536, 0.042]
params['A2016']['PF']['10m2'] = {}
params['A2016']['PF']['10m2']['bM']  = [-0.701,0.623, 0.599]
params['A2016']['PF']['10m2']['k12'] = [-0.656,0.596, 0.567]
params['A2016']['PF']['10m2']['k8']  = [-0.623,0.579, 0.569]
params['A2016']['PF']['10m2']['k6']  = [-0.625,0.585, 0.501]
params['A2016']['PF']['10m2']['k4']  = [-0.625,0.593, 0.379]
params['A2016']['PF']['10m2']['k2']  = [-0.632,0.589, 0.139]
params['A2016']['PF']['10m1'] = {}
params['A2016']['PF']['10m1']['bM']  = [-0.872,0.495, 1.233]
params['A2016']['PF']['10m1']['k12'] = [-0.899,0.502, 1.213]
params['A2016']['PF']['10m1']['k8']  = [-0.937,0.509, 1.097]
params['A2016']['PF']['10m1']['k6']  = [-0.947,0.505, 1.088]
params['A2016']['PF']['10m1']['k4']  = [-0.977,0.496, 1.068]
params['A2016']['PF']['10m1']['k2']  = [-1.230,0.464, 1.206]

params['A2016']['OF'] = {}
params['A2016']['OF']['10m3'] = {}
params['A2016']['OF']['10m3']['bM']  = [-1.371,0.996,-0.083]
params['A2016']['OF']['10m3']['k12'] = [-1.444,0.995,-0.070]
params['A2016']['OF']['10m3']['k8']  = [-1.484,0.994,-0.061]
params['A2016']['OF']['10m3']['k6']  = [-1.525,0.993,-0.052]
params['A2016']['OF']['10m3']['k4']  = [-1.613,0.990,-0.026]
params['A2016']['OF']['10m2'] = {}
params['A2016']['OF']['10m2']['bM']  = [-1.371,0.980,-0.049]
params['A2016']['OF']['10m2']['k12'] = [-1.440,0.979,-0.034]
params['A2016']['OF']['10m2']['k8']  = [-1.477,0.978,-0.024]
params['A2016']['OF']['10m2']['k6']  = [-1.514,0.976,-0.012]
params['A2016']['OF']['10m2']['k4']  = [-1.594,0.973, 0.017]

#-------------------------------------------------------
#threshold function
#------------------
#see Hellinger et al. 2006
@njit
def th_func(bparl,a,b,b0):
    return 1. + a/(bparl-b0)**b

#see Lazar et al. 2015
@njit
def th_func_2(bparl,a,b,c,d):
    return 1. + (a/bparl**b)*(1.+c/bparl**d)

#-------------------------------------------------------
#plot function
#-------------
def fluct_plot_func_masked( ax, xdata, ydata, mask, field, field_name, species_vars,
                               xdata_range=None, ydata_range=None, t=None, 
                               legend_flag=False, params=params, run_label=run_label,
                               mode=mode,logscale_flg=logscale_flg ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray(nx,ny) - first variable
    ydata        -> numpy.ndarray(nx,ny) - second variable
    mask         -> numpy.ndarray(nx,ny) - island mask
    field        -> numpy.ndarray(nx,ny) - field
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    params       -> [optional, set by default] dict -
                    dict containing all thresholds parameters
    run_label    -> [optional, set by default] string - run label

    OUTPUTS:
    isl_avg      -> [float,float] - average anisotropy-beta value for the island
    handles      -> [lines.Line2D, ...] - list of handles for the legend
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    if xdata_range == None:
        xdata_range = [np.min(xdata),np.max(xdata)]
    if ydata_range == None:
        ydata_range = [np.min(ydata),np.max(ydata)]
    xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),256)

    xdata_masked = xdata[mask]
    ydata_masked = ydata[mask]
    field_masked = field[mask]
    h,xbin,ybin = h2dw(np.log10(xdata_masked),np.log10(ydata_masked),field_masked,mode=mode)###
    Xb,Yb = np.meshgrid(np.power(10.,xbin),np.power(10.,ybin))###
    if logscale_flg:
        im = ax.pcolormesh(Xb,Yb,h.T,norm=mpl.colors.LogNorm())###
    else:
        im = ax.pcolormesh(Xb,Yb,h.T)###
    del field_masked,xdata_masked,ydata_masked

    if species_tag == 'p':
        p = params['H2006']['PC']['10m3']['bM'] # ProtonCyclotron - bi-Maxwellian
        lC, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-b',label="PCI")

    p = params['H2006']['MR']['10m3']['bM'] # Mirror - bi-Maxwellian
    lM, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-g',label="MI")

    if species_tag == 'e':
        p = params['L2015']['EC']['e05']['10m2']['bM']['T'] # ElectronCyclotron - bi-Maxwellian
        lC, = ax.plot(xvals,th_func_2(xvals,p[0],p[1],p[2],p[3]),'-k',label="ECI")

    p = params['A2016']['PF']['10m2']['bM'] # ParallelFirehose - bi-Maxwellian
    lP, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-m',label="PFHI")

    p = params['A2016']['OF']['10m2']['bM'] # ObliqueFirehose - bi-Maxwellian
    lO, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'-y',label="OFHI")

    p = params['A2016']['OF']['10m2']['k4'] # ObliqueFirehose - bi-kappa
    lOk, = ax.plot(xvals,th_func(xvals,p[0],p[1],p[2]*b0_mult),'--y',label="OFHI $\kappa=4$")

    lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',label='$\\beta_{\parallel}^{-1}$') # CGL expansion

    ax.set_xlabel('$\\beta_{\parallel,%s}$'%(species_tag))
    ax.set_ylabel('$p_{\perp,%s}/p_{\parallel,%s}$'%(species_tag,species_tag))
    ax.set_xlim(xdata_range[0],xdata_range[1])
    ax.set_ylim(ydata_range[0],ydata_range[1])
    if legend_flag:
        ax.legend()
    title_str = run_label+' - '+species_name+' - '+field_name
    if t != None:
        title_str += ' - $t = %s$'%(t)
    ax.set_title(title_str)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.colorbar(im,ax=ax)

    return [lC,lM,lP,lO,lOk,lb]

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp
#
    n_p,_ = run.get_Ion(ind)
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            pparle *= n_p
            pperpe *= n_p
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')
        del pparle
    del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #initial fields
    if code_name == 'HVM':
        Bx0 = 1.
        Bz0 = 0.25
        n_ = 1.
    elif code_name == 'iPIC':
        Bx0 = 0.01
        Bz0 = 0.25*0.01
        n_ = 0.25/np.pi
    beta = 1.
    A_1 = - Bx0
    A_2 = - A_1
    Ti = 0.5*beta
    n1 = A_1*A_1/(2.*Ti*(1.+teti))/Bx0/Bx0
    nb0 = 1.
#
    _,Y = np.meshgrid(x,y)
    Y = Y.T
    Bxeq = ( - A_1*np.tanh( (Y - y1)/L1 )
             - A_2*np.tanh( (Y - y2)/L2 )
             - np.sign(A_1)*(A_1 - A_2)*0.5 )
    neq = ( ( n1/( np.cosh( (Y - y1)/L1 )**2 ) + nb0 )*n_ ).reshape(nx,ny,1)
    del Y
#
    Beq = np.array([Bxeq,np.zeros((nx,ny),dtype=type(B[0,0,0,0])),
                   np.full((nx,ny),Bz0,dtype=type(B[0,0,0,0]))]).reshape(3,nx,ny,1)
    del Bxeq
#
    #fluctuations
    if w_ele:
        if code_name == 'HVM':
            n_e = n_p
        elif code_name == 'iPIC':
            n_e,_ = run.get_Ion(ind,qom=qom_e)
    dB = B - Beq
    dn_p = n_p - neq
    if w_ele:
        if code_name == 'HVM':
            dn_e = dn_p
        elif code_name == 'iPIC':
            dn_e = n_e - neq
#
    b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    for i in range(3):
        b[i] = B[i]/np.sqrt(B2)
    del B2,B
    dBparl = np.abs(dB[0]*b[0] + dB[1]*b[1] + dB[2]*b[2])
    dB2 = dB[0]*dB[0] + dB[1]*dB[1] + dB[2]*dB[2]
    dBperp = np.sqrt(dB2 - dBparl*dBparl)
#
    dxbx = calc.calc_gradx(b[0])
    dybx = calc.calc_grady(b[0])
    dxby = calc.calc_gradx(b[1])
    dyby = calc.calc_grady(b[1])
    dxbz = calc.calc_gradx(b[2])
    dybz = calc.calc_grady(b[2])
    b_Nb = np.array([b[0]*dxbx+b[1]*dybx, b[0]*dxby + b[1]*dyby, b[0]*dxbz + b[1]*dybz])
    del b,dxbx,dybx,dxby,dyby,dxbz,dybz
    curv_b = np.sqrt(b_Nb[0]**2 + b_Nb[1]**2 + b_Nb[2]**2)
    del b_Nb
#
    Beq2 = Beq[0]*Beq[0] + Beq[1]*Beq[1] + Beq[2]*Beq[2]
    fluct_B = np.divide(np.sqrt(dB2),np.sqrt(Beq2))
    del Beq2,Beq
    fluct_n_p = np.abs(np.divide(dn_p,neq))
    if w_ele:
        fluct_n_e = np.abs(np.divide(dn_e,neq))
        del dn_e
    del dn_p,neq
    anis_dB = np.divide(dBparl,dBperp)
    compr_B = np.divide(dBparl*dBparl,dB2)
    del dB2,dBperp,dBparl
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
#
    msk_flg = False # True -> 'cutout'; False -> 'CS1'
    if msk_flg:
        mask_name = 'cutout'
        mask = np.ones((nx,ny),dtype=bool)
        for ix in range(nx):
            for iy in range(cut1,cut2+1):
                mask[ix,iy] = False
    else:
        mask_name = 'CS1'
        mask = np.zeros((nx,ny),dtype=bool)
        for ix in range(nx):
            for iy in range(int(12.5/run.meta['ddd'][0])):
                mask[ix,iy] = True
    #plt.close()
    #X,Y = np.meshgrid(x,y)
    #im = plt.pcolormesh(X,Y,mask.astype(np.int).T)
    #plt.colorbar(im)
    #plt.xlabel('x')
    #plt.ylabel('y')
    #plt.title('mask')
    #plt.show()
    #plt.close()
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    print('\nStart plotting...')
    # -> curv_b
    field_label = 'curv_b'
#
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    handles_p = fluct_plot_func_masked( ax[0],bparlp[...,0],anisp[...,0],mask,curv_b[...,0],
                                           '$|\hat{b}\cdot\\nabla\hat{b}|$',
                                           p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                                           legend_flag=False,params=params,run_label=run_label )
#
    if w_ele:
        handles_e = fluct_plot_func_masked( ax[1],bparle[...,0],anise[...,0],mask,curv_b[...,0],
                                               '$|\hat{b}\cdot\\nabla\hat{b}|$',
                                               e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                               legend_flag=False,params=params,run_label=run_label )
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4],
                   handles_p[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    if logscale_flg:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_logscale_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    else:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    plt.close()
    print('\nplotted '+field_label)
#
    # -> fluct_B
    field_label = 'fluct_B'
#
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    handles_p = fluct_plot_func_masked( ax[0],bparlp[...,0],anisp[...,0],mask,fluct_B[...,0],
                                           '$\mathrm{d}B/B_{\mathrm{eq}}$',
                                           p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                                           legend_flag=False,params=params,run_label=run_label )
#
    if w_ele:
        handles_e = fluct_plot_func_masked( ax[1],bparle[...,0],anise[...,0],mask,fluct_B[...,0],
                                               '$\mathrm{d}B/B_{\mathrm{eq}}$',
                                               e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                               legend_flag=False,params=params,run_label=run_label )
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4],
                   handles_p[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    if logscale_flg:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_logscale_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    else:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    plt.close()
    print('\nplotted '+field_label)
#
    # -> fluct_n
    field_label = 'fluct_n'
#
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    handles_p = fluct_plot_func_masked( ax[0],bparlp[...,0],anisp[...,0],mask,fluct_n_p[...,0],
                                           '$|\mathrm{d}n_{\mathrm{p}}/n_{\mathrm{eq}}|$',
                                           p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                                           legend_flag=False,params=params,run_label=run_label )
#
    if w_ele:
        handles_e = fluct_plot_func_masked( ax[1],bparle[...,0],anise[...,0],mask,fluct_n_e[...,0],
                                               '$|\mathrm{d}n_{\mathrm{e}}/n_{\mathrm{eq}}|$',
                                               e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                               legend_flag=False,params=params,run_label=run_label )
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4],
                   handles_p[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    if logscale_flg:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_logscale_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    else:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    plt.close()
    print('\nplotted '+field_label)
#
    # -> anis_dB
    field_label = 'anis_dB'
#
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    handles_p = fluct_plot_func_masked( ax[0],bparlp[...,0],anisp[...,0],mask,anis_dB[...,0],
                                           '$\mathrm{d}B_{\parallel}/\mathrm{d}B_{\perp}$',
                                           p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                                           legend_flag=False,params=params,run_label=run_label )
#
    if w_ele:
        handles_e = fluct_plot_func_masked( ax[1],bparle[...,0],anise[...,0],mask,anis_dB[...,0],
                                               '$\mathrm{d}B_{\parallel}/\mathrm{d}B_{\perp}$',
                                               e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                               legend_flag=False,params=params,run_label=run_label )
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4],
                   handles_p[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    if logscale_flg:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_logscale_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    else:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    plt.close()
    print('\nplotted '+field_label)
#
    # -> compr_B
    field_label = 'compr_B'
#
    plt.close()
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
#
    handles_p = fluct_plot_func_masked( ax[0],bparlp[...,0],anisp[...,0],mask,compr_B[...,0],
                                           '$\mathrm{d}B_{\parallel}^2/\mathrm{d}B^2$',
                                           p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                                           legend_flag=False,params=params,run_label=run_label )
#
    if w_ele:
        handles_e = fluct_plot_func_masked( ax[1],bparle[...,0],anise[...,0],mask,compr_B[...,0],
                                               '$\mathrm{d}B_{\parallel}^2/\mathrm{d}B^2$',
                                               e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                               legend_flag=False,params=params,run_label=run_label )
    else:
        handles_e = handles_p[:]
        handles_e[0], =  ax[1].plot([],[],color='black',label='ECI')
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        handles = [handles_p[0],#[lC,lM,lP,lO,lOk,lb]
                   handles_e[0],
                   handles_p[1],
                   handles_p[2],
                   handles_p[3],
                   handles_p[4],
                   handles_p[5]]
        ax[1].legend(handles=handles,loc='center',borderaxespad=0)
#
    plt.tight_layout()
    if logscale_flg:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_logscale_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    else:
        plt.savefig(opath+'/'+out_dir+'/'+'fluct_in_wind_'+mask_name+'_'+mode+'_'+run_name+'_'+field_label+'_'+str(ind)+'.png')
    plt.close()
    print('\nplotted '+field_label)
#
    print('\n... plotting complete!')
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
