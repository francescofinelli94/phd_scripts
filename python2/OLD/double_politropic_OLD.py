#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.optimize import curve_fit
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nDouble-polytropic study.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

run_name = run.meta['run_name']
out_dir = 'double_polytropic/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']
x = run.meta['x']
y = run.meta['y']

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

wl.Can_I()

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#threshold parameter (closer to 1 this, the smaller the the island)
th_param = 0.87#0.98

#-------------------------------------------------------
#functions
#---------
def fittin_around_2d(pparl_,pperp_,n_,Bn_,r=1,test=False):
    nx,ny,_ = n_.shape
    Xperp = np.pad(np.log(Bn_),r,mode='wrap')
    Yperp = np.pad(np.log(np.divide(pperp_*Bn_,n_)),r,mode='wrap')
    Xparl = np.pad(np.log(np.divide(n_,Bn_)),r,mode='wrap')
    Yparl = np.pad(np.log(np.divide(pparl_,Bn_)),r,mode='wrap')
#
    gparl = np.empty((nx,ny),dtype=np.float64)
    gperp = np.empty((nx,ny),dtype=np.float64)
    cparl = np.empty((nx,ny),dtype=np.float64)
    cperp = np.empty((nx,ny),dtype=np.float64)
    rparl = np.empty((nx,ny),dtype=np.float64)
    rperp = np.empty((nx,ny),dtype=np.float64)
#
    for (ix,iy) in [(ix,iy) for ix in range(nx) for iy in range(ny)]:
        x_ = Xperp[ix:ix+1+2*r,iy:iy+1+2*r].flat[:]
        y_ = Yperp[ix:ix+1+2*r,iy:iy+1+2*r].flat[:]
        (m,q),res,_,_,_ = np.polyfit(x_,y_,1,full=True)
        gperp[ix,iy] = m
        cperp[ix,iy] = q
        rperp[ix,iy] = res
#
        x_ = Xparl[ix:ix+1+2*r,iy:iy+1+2*r].flat[:]
        y_ = Yparl[ix:ix+1+2*r,iy:iy+1+2*r].flat[:]
        (m,q),res,_,_,_ = np.polyfit(x_,y_,1,full=True)
        gparl[ix,iy] = m
        cparl[ix,iy] = q
        rparl[ix,iy] = res
#
    if test:
        return gparl,gperp,cparl,cperp,rparl,rperp
    else:
        return rparl,rperp

#-------------------------------------------------------
#da loop
#-------
#data structures
Bn_isl = []
n_isl = []
#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #getting data and computing stuff
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    Bn = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
#
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
#
    n_p,_ = run.get_Ion(ind)
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n_e = n_p.copy()
            pparle = pparle*n_e
            pperpe = pperpe*n_e
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
            n_e,_ = run.get_Ion(ind,qom=qom_e)
        else:
            print('\nWhat code is it?\n')
#
    mask = (Psi > np.max(Psi)*th_param).reshape(nx,ny,nz)
#
    Bn_isl.append(np.mean(Bn[mask]))
    n_isl.append(np.mean(n_p[mask]))
#
    #fittin around
    gparlp,gperpp,cparlp,cperpp,rparlp,rperpp = fittin_around_2d(pparlp,pperpp,n_p,Bn,test=True)
#
    #plottin around
    plt.close()
    fig,ax = plt.subplots(2,3,figsize=(36,24))
#
    im0_0 = ax[0,0].contourf(x,y,gparlp.T,levels=np.linspace(0.5,3.5,32+1),vmin=0.5,vmax=3.5)
    plt.colorbar(im0_0,ax=ax[0,0])
    ax[0,0].set_title('$\gamma_{\parallel}$ protons - t='+str(time[ind]))
#
    im0_1 = ax[0,1].contourf(x,y,cparlp.T,32)
    plt.colorbar(im0_1,ax=ax[0,1])
    ax[0,1].set_title('$C_{\parallel}$ protons')
#
    im0_2 = ax[0,2].contourf(x,y,np.log(rparlp).T,32)
    plt.colorbar(im0_2,ax=ax[0,2])
    ax[0,2].set_title('$Log(R_{\parallel})$ protons')
#
    im1_0 = ax[1,0].contourf(x,y,gperpp.T,levels=np.linspace(0.5,2.5,32+1),vmin=0.5,vmax=2.5)
    plt.colorbar(im1_0,ax=ax[1,0])
    ax[1,0].set_title('$\gamma_{\perp}$ protons')
#
    im1_1 = ax[1,1].contourf(x,y,cperpp.T,32)
    plt.colorbar(im1_1,ax=ax[1,1])
    ax[1,1].set_title('$C_{\perp}$ protons')
#
    im1_2 = ax[1,2].contourf(x,y,np.log(rperpp).T,32)
    plt.colorbar(im1_2,ax=ax[1,2])
    ax[1,2].set_title('$Log(R_{\perp})$ protons')
#
    #plt.tight_layout()
    plt.show()
    plt.close()
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
Bn_isl = np.array(Bn_isl)
n_isl = np.array(n_isl)

#B-n in time plot
plt.close()
fig,ax = plt.subplots(1,1,figsize=(8,10))

im = ax.scatter(n_isl,Bn_isl,c=time,cmap='gist_rainbow')
cb = plt.colorbar(im,ax=ax);cb.set_label('time')
ax.set_xlabel('n');ax.set_ylabel('$|B|$')
ax.set_title('Average n and $|B|$ values in islands - '+run_name)

plt.tight_layout()
#plt.savefig(opath+'/'+out_dir+'/n_B_isl_avg_'+run_name+'.png')
plt.close()
