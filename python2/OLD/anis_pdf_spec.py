#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill check enropies\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

calc = fibo('calc')
calc.meta=run.meta

#params
g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

idtens = np.zeros((3,3,nx,ny,nz),dtype=np.float64)
for i in range(3):
    idtens[i,i] = 1.

#declare arrays
####t_vec = []

####m_Sfp = []
####m_adv_Sfp = []
####m_gPiD_Sfp = []
####m_nPiD_Sfp = []
####m_vhf_Sfp = []

####m_Sfe = []
####m_adv_Sfe = []
####m_gPiD_Sfe = []
####m_nPiD_Sfe = []
####m_vhf_Sfe = []

####m_Sgp = []
####m_adv_Sgp = []
####m_nPiD_Sgp = []
####m_vhf_Sgp = []
####m_ng_Sgp = []

####if w_ele:
####    m_Sge = []
####    m_adv_Sge = []
####    m_nPiD_Sge = []
####    m_vhf_Sge = []
####    m_ng_Sge = []

#------------------
smooth_flag = False
smooth_flag_post = False
gfs = 2

if smooth_flag:
    out_dir = 'anis_pdf'+'/'+run_name+'_gauss_'+str(gfs)
elif smooth_flag_post:
    out_dir = 'anis_pdf'+'/'+run_name+'_gausspost_'+str(gfs)
else:
    out_dir = 'anis_pdf'+'/'+run_name

ml.create_path(opath+'/'+out_dir)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')

#   Psi = wl.psi_2d(B[:-1,...,0],run.meta)
#   if code_name == 'iPIC' and (smooth_flag or smooth_flag_post):
#       Psi = gf(Psi,[gfs,gfs],mode='wrap')

#   if code_name == 'HVM':
#       cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
#       J = np.array([cx,cy,cz]) # J = rot(B)
#       del cx,cy,cz

#   Bn = np.sqrt(B[0]**2+B[1]**2+B[2]**2) # Bn = |B|
#   b = np.empty((3,nx,ny,nz),dtype=np.float64) # b = B/|B|
#   for i in range(3):
#       b[i] = np.divide(B[i],Bn)
#   bb = np.empty((3,3,nx,ny,nz),dtype=np.float64) # bb = b (x) b
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       bb[i,j] = b[i]*b[j]
#   id_bb = np.empty((3,3,nx,ny,nz),dtype=np.float64) # id_bb = id - bb
#   id_bb = idtens - bb

#   #densities and currents
#   n_p,u_p = run.get_Ion(ind)
#   if code_name == 'iPIC' and smooth_flag:
#       n_p[:,:,0] = gf(n_p[:,:,0],[gfs,gfs],mode='wrap')
#       for i in range(3): u_p[i,:,:,0] = gf(u_p[i,:,:,0],[gfs,gfs],mode='wrap')
#   if code_name == 'HVM':
#       n_e = n_p.copy() # n_e = n_p = n (no charge separation)
#       u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
#       u_e[0] = u_p[0] - np.divide(J[0],n_p)
#       u_e[1] = u_p[1] - np.divide(J[1],n_p)
#       u_e[2] = u_p[2] - np.divide(J[2],n_p)
#   elif code_name == 'iPIC':
#       n_e,u_e = run.get_Ion(ind,qom=qom_e)
#       if smooth_flag:
#           n_e[:,:,0] = gf(n_e[:,:,0],[gfs,gfs],mode='wrap')
#           for i in range(3): u_e[i,:,:,0] = gf(u_e[i,:,:,0],[gfs,gfs],mode='wrap')
#       J = n_p*u_p - n_e*u_e

    #Pressures
#   Pp = run.get_Press(ind)
#   if code_name == 'iPIC' and smooth_flag:
#       for i in range(3): 
#           for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[gfs,gfs],mode='wrap')
#   pparlp,pperpp = ml.proj_tens_on_vec(Pp,B) # pparlp = Pp:bb
#                                             # pperpp = Pp:(id - bb)/2
#   Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3. # Pisop = Tr(Pp)/3

#   if not w_ele:
#       Pisoe = n_e*run.meta['beta']*0.5*run.meta['teti'] # Pisoe = n_e * T_e,0
#       Pe = np.zeros((3,3,nx,ny,nz),dtype=np.float64) # Pe = Pisoe*id
#       for i in range(3):
#           Pe[i,i] = Pisoe.copy()
#       pparle = Pisoe # ppalre = Pisoe
#       pperpe = pparle # pperpe = Pisoe
#   else:
#       if code_name == 'HVM':
#           tparle,tperpe = run.get_Te(ind)
#           pparle = tparle*n_e # pparle = n_e*tparle
#           pperpe = tperpe*n_e # pperpe = n_e*tperpe
#           Pisoe = (pparle + 2.*pperpe)/3. # Pisoe  = (pparle + 2*pperpe)/3
#           Pe = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pe = pparle*bb + pperpe*(id - bb)
#           for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#               Pe[i,j] = pparle*bb[i,j] + pperpe*id_bb[i,j]
#       elif code_name == 'iPIC':
#           Pe = run.get_Press(ind,qom=qom_e)
#           if smooth_flag:
#               for i in range(3):
#                   for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[gfs,gfs],mode='wrap')
#           pparle,pperpe = ml.proj_tens_on_vec(Pe,B) # pparle = Pe:bb
                                                      # pperpe = Pe:(id - bb)/2
#           Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.  # Pisoe = Tr(Pe)/3

    anis = {}
    if code_name == 'iPIC':
        for st in run.meta['stag']:
            P = run.get_Pspec(ind,st)
            pparls,pperps = ml.proj_tens_on_vec(P,B)
            pparls[pparls<=0.] = 1.e-30
            pperps[pperps<=0.] = 1.e-30
            anis[st] = np.divide(pperps,pparls)
            anis[st][anis[st]<0.001] = 1.
            anis[st][anis[st]>4] = 1.
            print st,np.min(pparls),np.min(pperps),np.max(pparls),np.max(pperps)
            print st,np.min(anis[st]),np.max(anis[st])
        P = run.get_Press(ind,qom=qom_e)
        pparls,pperps = ml.proj_tens_on_vec(P,B)
        anis['e'] = np.divide(pperps,pparls)
        P = run.get_Press(ind)
        pparls,pperps = ml.proj_tens_on_vec(P,B)
        anis['p'] = np.divide(pperps,pparls)
        del P,pparls,pperps
        stag = run.meta['stag']
    elif code_name == 'HVM':
        P = run.get_Press(ind)
        pparls,pperps = ml.proj_tens_on_vec(P,B)
        anis['p'] = np.divide(pperps,pparls)
        del P,pparls,pperps
        if w_ele:
            tparle,tperpe = run.get_Te(ind)
            anis['e'] = np.divide(tperpe,tparle)
            del tparle,tperpe
    else:
        print('code name unknown')

    plt.close()
    if w_ele:
        ncol = 2
    else:
        ncol = 1
    nrow = 1
    fig,ax = plt.subplots(nrow,ncol,figsize=(10*ncol,4*nrow))

    if w_ele:
        ax_ = ax[0]
    else:
        ax_ = ax
    h_,b_,im00 = ax_.hist(np.array(anis['p'][ixmin:ixmax,iymin:iymax,0].flat),
                          bins=200,histtype='step',log=True,label='all')
    ax_.set_title('anis p - '+run_name.replace('_',' ')+' - t='+str(time[ind]))
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    if code_name == 'iPIC':
        h_,b_,im00 = ax_.hist(np.array(anis[stag[3]][ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True,label='bkg')
        yH = max(np.max(h_),yH)
        xL = min(b_[np.argmax(h_>yL)],xL)
        xH = max(b_[len(h_)-1-np.argmax(h_[::-1]>yL)],xH)

        h_,b_,im00 = ax_.hist(np.array(anis[stag[4]][ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True,label='hs1')
        yH = max(np.max(h_),yH)
        xL = min(b_[np.argmax(h_>yL)],xL)
        xH = max(b_[len(h_)-1-np.argmax(h_[::-1]>yL)],xH)

        h_,b_,im00 = ax_.hist(np.array(anis[stag[5]][ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True,label='hs2')
        yH = max(np.max(h_),yH)
        xL = min(b_[np.argmax(h_>yL)],xL)
        xH = max(b_[len(h_)-1-np.argmax(h_[::-1]>yL)],xH)
    ax_.set_ylim(yL,yH)
    ax_.set_xlim(xL,xH)
    ax_.legend()

    if w_ele:
        ax_ = ax[1]
        h_,b_,im01 = ax_.hist(np.array(anis['e'][ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True,label='all')
        ax_.set_title('anis e - '+run_name.replace('_',' ')+' - t='+str(time[ind]))
        yL = 10#int(float(np.max(h_))/500.)
        yH = np.max(h_)
        xL = b_[np.argmax(h_>yL)]
        xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
        if code_name == 'iPIC':
            h_,b_,im01 = ax_.hist(np.array(anis[stag[0]][ixmin:ixmax,iymin:iymax,0].flat),
                                  bins=200,histtype='step',log=True,label='bkg')
            yH = max(np.max(h_),yH)
            xL = min(b_[np.argmax(h_>yL)],xL)
            xH = max(b_[len(h_)-1-np.argmax(h_[::-1]>yL)],xH)

            h_,b_,im01 = ax_.hist(np.array(anis[stag[1]][ixmin:ixmax,iymin:iymax,0].flat),
                                  bins=200,histtype='step',log=True,label='hs1')
            yH = max(np.max(h_),yH)
            xL = min(b_[np.argmax(h_>yL)],xL)
            xH = max(b_[len(h_)-1-np.argmax(h_[::-1]>yL)],xH)

            h_,b_,im01 = ax_.hist(np.array(anis[stag[2]][ixmin:ixmax,iymin:iymax,0].flat),
                                  bins=200,histtype='step',log=True,label='hs2')
            yH = max(np.max(h_),yH)
            xL = min(b_[np.argmax(h_>yL)],xL)
            xH = max(b_[len(h_)-1-np.argmax(h_[::-1]>yL)],xH)
        ax_.set_ylim(yL,yH)
        ax_.set_xlim(xL,xH)
        ax_.legend()

    plt.tight_layout()
    plt.show()
    #plt.savefig(opath+'/'+out_dir+'/'+'anis_PDF_'+run_name+'_'+str(ind)+'.png')
    plt.close()

#   Pnp = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pnp = Pp - [pparlp*bb + pperpp*(id - bb)]
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       Pnp[i,j] = Pp[i,j] - (pparlp*bb[i,j] + pperpp*id_bb[i,j])
#   if code_name == 'iPIC':
#       Pne = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pne = Pe - [pparle*bb + pperpe*(id - bb)]
#       for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#           Pne[i,j] = Pe[i,j] - (pparle*bb[i,j] + pperpe*id_bb[i,j])

#   #Heat fluxes
#   if code_name == 'HVM':
#       sparlp,sperpp = run.get_Q(ind) # sparlp = Qp:bb
#                                      # sperpp = Qp:(id - bb)/2
#       qp = 0.5*sparlp + sperpp # qp = Qp:id/2 = sparlp/2 + sperpp
#       Sp = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # Goswami 2005, eq. 7, S = S(sparl,sperp)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           Sp[i,j,k] = ( 0.5*(sperpp[i]*id_bb[j,k] + sperpp[j]*id_bb[i,k] + sperpp[k]*id_bb[i,j])
#                            + sparlp[i]*bb[j,k]    + sparlp[j]*bb[i,k]    + sparlp[k]*bb[i,j] )
#           for l in range(3):
#               Sp[i,j,k] += ( 0.5*(sperpp[l]*bb[l,i]*id_bb[j,k]
#                                 + sperpp[l]*bb[l,j]*id_bb[i,k]
#                                 + sperpp[l]*bb[l,k]*id_bb[i,j])
#                        - (2./3.)*(sparlp[l]*bb[l,i]*bb[j,k]
#                                 + sparlp[l]*bb[l,j]*bb[i,k]
#                                 + sparlp[l]*bb[l,k]*bb[i,j]) )
#   elif code_name == 'iPIC':
#       qp = run.get_Q(ind) # qp = Qp:id/2
#       if smooth_flag:
#           for i in range(3): qp[i,:,:,0] = gf(qp[i,:,:,0],[gfs,gfs],mode='wrap')
#       Sp = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # S'_ijk = 
#                                                        # = 2/5*(q_i*id_jk + q_j*id_ik + q_k*id_ij)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           Sp[i,j,k] = (2./5.)*(qp[i]*idtens[j,k] + qp[j]*idtens[i,k] + qp[k]*idtens[i,j])
#   if w_ele:
#       if code_name == 'HVM':
#           qparle,qperpe = run.get_Qe(ind) # qparle = Qe:.bbb
#                                           # qperpe = Qe:.(id - bb)b/2
#           sparle = np.empty((3,nx,ny,nz),dtype=np.float64) # sparle = qparle*b
#           sperpe = np.empty((3,nx,ny,nz),dtype=np.float64) # sperpe = qperpe*b
#           for i in range(3):
#               sparle[i] = qparle*b[i]
#               sperpe[i] = qperpe*b[i]
#           qe = 0.5*sparle + sperpe # qe = sparle/2 + sperpe
#           Se = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # Goswami 2005, eq. 7, S = S(sparl,sperp)
#           for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#               Se[i,j,k] = ( 0.5*(sperpe[i]*id_bb[j,k] + sperpe[j]*id_bb[i,k] + sperpe[k]*id_bb[i,j])
#                                + sparle[i]*bb[j,k]    + sparle[j]*bb[i,k]    + sparle[k]*bb[i,j] )
#               for l in range(3):
#                   Se[i,j,k] += ( 0.5*(sperpe[l]*bb[l,i]*id_bb[j,k]
#                                     + sperpe[l]*bb[l,j]*id_bb[i,k]
#                                     + sperpe[l]*bb[l,k]*id_bb[i,j])
#                            - (2./3.)*(sparle[l]*bb[l,i]*bb[j,k]
#                                     + sparle[l]*bb[l,j]*bb[i,k]
#                                     + sparle[l]*bb[l,k]*bb[i,j]) )
#       elif code_name == 'iPIC':
#           qe = run.get_Q(ind,qom=qom_e) # qe = Qe:id/2
#           if smooth_flag:
#               for i in range(3): qe[i,:,:,0] = gf(qe[i,:,:,0],[gfs,gfs],mode='wrap')
#           Se = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # S'_ijk =
#                                                            # = 2/5*(q_i*id_jk + q_j*id_ik + q_k*id_ij)
#           for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#               Se[i,j,k] = (2./5.)*(qe[i]*idtens[j,k] + qe[j]*idtens[i,k] + qe[k]*idtens[i,j])
#   else:
#       #qe = np.empty((3,nx,ny,nz),dtype=np.float64) # qe = 3/2*n*T_e,0*u_e
#       #for i in range(3):
#       #    qe[i] = (3./2.)*n_e*run.meta['beta']*0.5*run.meta['teti']*u_e[i]
#       qe = np.zeros((3,nx,ny,nz),dtype=np.float64) # qe = 0
#       Se = np.empty((3,3,3,nx,ny,nz),dtype=np.float64) # S'_ijk =
#                                                        # = 2/5*(q_i*id_jk + q_j*id_ik + q_k*id_ij)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           Se[i,j,k] = (2./5.)*(qe[i]*idtens[j,k] + qe[j]*idtens[i,k] + qe[k]*idtens[i,j])

#   #Hall electric field (rotor of, normalized over |B|)
#   tmp1 = np.empty((3,nx,ny,nz),dtype=np.float64) # tmp1 = nabla.Pe
#   for i in range(3):
#       tmp1[i] = calc.calc_divr(Pe[0,i],Pe[1,i],Pe[2,i])
#   tmp2 = np.empty((3,nx,ny,nz),dtype=np.float64) # tmp2 = J x B
#   tmp2[0] = J[1]*B[2] - J[2]*B[1]
#   tmp2[1] = J[2]*B[0] - J[0]*B[2]
#   tmp2[2] = J[0]*B[1] - J[1]*B[0]
#   del J
#   del B
#   E_H = np.empty((3,nx,ny,nz),dtype=np.float64) # E_H = (J x B - nabla.Pe)/n_p
#   for i in range(3):
#       E_H[i] = np.divide(tmp2[i]-tmp1[i],n_p)
#   del tmp1
#   del tmp2
#   cx,cy,cz = calc.calc_curl(E_H[0],E_H[1],E_H[2])
#   del E_H
#   rotE_H_Bn = np.array([cx,cy,cz]) # rotE_H_Bn = rot(E_H)/|B|
#   del cx,cy,cz
#   for i in range(3):
#       rotE_H_Bn[i] = np.divide(rotE_H_Bn[i],Bn)
#   del Bn

#   #get entropies
#   Sfp = (3./2.)*np.log(np.divide(Pisop,n_p**g_adiab)) # Sfp = 3/2*ln( Pisop/n_p^(5/3) )
#   Sfe = (3./2.)*np.log(np.divide(Pisoe,n_e**g_adiab)) # Sfe = 3/2*ln( Pisoe/n_e^(5/3) )

#   Sgp = (3./2.)*np.log(np.divide((pparlp**(1./3.))*(pperpp**(2./3.)),n_p**g_adiab)) #
#                   # Sgp = (3/2)*ln( ( pparlp^(1/3) * pperpp^(2/3) )/( n_p^(5/3) ) )
#   del n_p
#   if w_ele:
#       Sge = (3./2.)*np.log(np.divide((pparle**(1./3.))*(pperpe**(2./3.)),n_e**g_adiab)) #
#                       # Sge = (3/2)*ln( ( pparle^(1/3) * pperpe^(2/3) )/( n_e^(5/3) ) )
#   del n_e

#   if code_name == 'iPIC' and smooth_flag_post:
#       Sfp[:,:,0] = gf(Sfp[:,:,0],[gfs,gfs],mode='wrap')
#       Sfe[:,:,0] = gf(Sfe[:,:,0],[gfs,gfs],mode='wrap')
#       Sgp[:,:,0] = gf(Sgp[:,:,0],[gfs,gfs],mode='wrap')
#       if w_ele:
#           Sge[:,:,0] = gf(Sge[:,:,0],[gfs,gfs],mode='wrap')

#   #entropies sources
#   #-> advection
#   #-> -> isotropic
#   tmp = np.empty((3,nx,ny,nz),dtype=np.float64)
#   tmp[0] = calc.calc_gradx(Sfp)
#   tmp[1] = calc.calc_grady(Sfp)
#   tmp[2] = calc.calc_gradz(Sfp)
#   adv_Sfp = -(u_p[0]*tmp[0] + u_p[1]*tmp[1] + u_p[2]*tmp[2]) # adv_Sfp = -u_p.grad(Sfp)
#   tmp[0] = calc.calc_gradx(Sfe)
#   tmp[1] = calc.calc_grady(Sfe)
#   tmp[2] = calc.calc_gradz(Sfe)
#   adv_Sfe = -(u_e[0]*tmp[0] + u_e[1]*tmp[1] + u_e[2]*tmp[2]) # adv_Sfe = -u_e.grad(Sfe)

#   if code_name == 'iPIC' and smooth_flag_post:
#       adv_Sfp[:,:,0] = gf(adv_Sfp[:,:,0],[gfs,gfs],mode='wrap')
#       adv_Sfe[:,:,0] = gf(adv_Sfe[:,:,0],[gfs,gfs],mode='wrap')

#   #-> -> anisotropic
#   tmp[0] = calc.calc_gradx(Sgp)
#   tmp[1] = calc.calc_grady(Sgp)
#   tmp[2] = calc.calc_gradz(Sgp)
#   adv_Sgp = -(u_p[0]*tmp[0] + u_p[1]*tmp[1] + u_p[2]*tmp[2]) # adv_Sgp = -u_p.grad(Sgp)
#   if w_ele:
#       tmp[0] = calc.calc_gradx(Sge)
#       tmp[1] = calc.calc_grady(Sge)
#       tmp[2] = calc.calc_gradz(Sge)
#       adv_Sge = -(u_e[0]*tmp[0] + u_e[1]*tmp[1] + u_e[2]*tmp[2]) # adv_Sge = -u_e.grad(Sge)
#   del tmp

#   if code_name == 'iPIC' and smooth_flag_post:
#       adv_Sgp[:,:,0] = gf(adv_Sgp[:,:,0],[gfs,gfs],mode='wrap')
#       if w_ele:
#           adv_Sge[:,:,0] = gf(adv_Sge[:,:,0],[gfs,gfs],mode='wrap')

#   #-> pressure-strain
#   #-> -> isotropic
#   grad_u_p = np.empty((3,3,nx,ny,nz),dtype=np.float64) # grad(u_p)
#   for i in range(3):
#       grad_u_p[0,i] = calc.calc_gradx(u_p[i])
#       grad_u_p[1,i] = calc.calc_grady(u_p[i])
#       grad_u_p[2,i] = calc.calc_gradz(u_p[i])

#   grad_u_e = np.empty((3,3,nx,ny,nz),dtype=np.float64) # grad(u_e)
#   for i in range(3):
#       grad_u_e[0,i] = calc.calc_gradx(u_e[i])
#       grad_u_e[1,i] = calc.calc_grady(u_e[i])
#       grad_u_e[2,i] = calc.calc_gradz(u_e[i])

#   tmp = calc.calc_divr(u_p[0],u_p[1],u_p[2])
#   strainp = np.empty((3,3,nx,ny,nz),dtype=np.float64) # strainp = 2*sym(grad(u_p)) - (2/3)*div(u_p)*id
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       strainp[i,j] = grad_u_p[i,j] + grad_u_p[j,i] - (2./3.)*tmp*idtens[i,j]
#   tmp = calc.calc_divr(u_e[0],u_e[1],u_e[2])
#   straine = np.empty((3,3,nx,ny,nz),dtype=np.float64) # straine = 2*sym(grad(u_e)) - (2/3)*div(u_e)*id
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       straine[i,j] = grad_u_e[i,j] + grad_u_e[j,i] - (2./3.)*tmp*idtens[i,j]
#   del tmp

#   gPiD_Sfp = np.zeros((nx,ny,nz),dtype=np.float64) # gPiD_Sfp = -(pparlp-pperpp)/(2*Pisop)*bb:strainp
#   gPiD_Sfe = np.zeros((nx,ny,nz),dtype=np.float64) # gPiD_Sfe = -(pparle-pperpe)/(2*Pisoe)*bb:straine
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       gPiD_Sfp += bb[i,j]*strainp[j,i]
#       gPiD_Sfe += bb[i,j]*straine[j,i]
#   gPiD_Sfp = gPiD_Sfp*(-0.5*np.divide(pparlp-pperpp,Pisop))
#   gPiD_Sfe = gPiD_Sfe*(-0.5*np.divide(pparle-pperpe,Pisoe))

#   nPiD_Sfp = np.zeros((nx,ny,nz),dtype=np.float64) # nPiD_Sfp = -(Pnp:strainp)/(2*Pisop)
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       nPiD_Sfp += Pnp[i,j]*strainp[j,i]
#   nPiD_Sfp = -0.5*np.divide(nPiD_Sfp,Pisop)

#   nPiD_Sfe = np.zeros((nx,ny,nz),dtype=np.float64) # nPiD_Sfe = -(Pne:straine)/(2*Pisoe)
#   if code_name == 'iPIC':
#       for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#           nPiD_Sfe += Pne[i,j]*straine[j,i]
#       nPiD_Sfe = -0.5*np.divide(nPiD_Sfe,Pisoe)

#   if code_name == 'iPIC' and smooth_flag_post:
#       gPiD_Sfp[:,:,0] = gf(gPiD_Sfp[:,:,0],[gfs,gfs],mode='wrap')
#       gPiD_Sfe[:,:,0] = gf(gPiD_Sfe[:,:,0],[gfs,gfs],mode='wrap')
#       nPiD_Sfp[:,:,0] = gf(nPiD_Sfp[:,:,0],[gfs,gfs],mode='wrap')
#       nPiD_Sfe[:,:,0] = gf(nPiD_Sfe[:,:,0],[gfs,gfs],mode='wrap')

#   #-> -> anisotropic
#   nPiD_Sgp = np.divide(Pisop*nPiD_Sfp,pperpp) # nPiD_Sgp = -(Pnp:strainp)/(2*pperpp)
#   nPiD_Sge = np.divide(Pisoe*nPiD_Sfe,pperpe) # nPiD_Sge = -(Pne:straine)/(2*pperpe)

#   if code_name == 'iPIC' and smooth_flag_post:
#       nPiD_Sgp[:,:,0] = gf(nPiD_Sgp[:,:,0],[gfs,gfs],mode='wrap')
#       nPiD_Sge[:,:,0] = gf(nPiD_Sge[:,:,0],[gfs,gfs],mode='wrap')

#   #-> heat flux
#   #-> -> isotropic
#   tmp = calc.calc_divr(qp[0],qp[1],qp[2])
#   vhf_Sfp = -np.divide(tmp,Pisop) # vhf_Sfp = -div(qp)/Pisop
#   tmp = calc.calc_divr(qe[0],qe[1],qe[2])
#   vhf_Sfe = -np.divide(tmp,Pisoe) # vhf_Sfe = -div(qe)/Pisoe
#   del tmp

#   if code_name == 'iPIC' and smooth_flag_post:
#       vhf_Sfp[:,:,0] = gf(vhf_Sfp[:,:,0],[gfs,gfs],mode='wrap')
#       vhf_Sfe[:,:,0] = gf(vhf_Sfe[:,:,0],[gfs,gfs],mode='wrap')

#   #-> -> anisotropic
#   vhf_Sgp = np.divide(vhf_Sfp*Pisop,pperpp) # vhf_Sgp = -div(qp)/pperpp
#   vhf_Sge = np.divide(vhf_Sfe*Pisoe,pperpe) # vhf_Sge = -div(qe)/pperpe

#   if code_name == 'iPIC' and smooth_flag_post:
#       vhf_Sgp[:,:,0] = gf(vhf_Sgp[:,:,0],[gfs,gfs],mode='wrap')
#       vhf_Sge[:,:,0] = gf(vhf_Sge[:,:,0],[gfs,gfs],mode='wrap')

#   #-> non-gyrotropy
#   #-> -> anisotropic
#   tmp1 = np.zeros((nx,ny,nz),dtype=np.float64) # tmp1 = bb:(Pnp.grad(u_p))
#   for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#       tmp1 += bb[k,j]*Pnp[j,i]*grad_u_p[i,k]

#   tmp2 = np.zeros((nx,ny,nz),dtype=np.float64) # tmp2 = 2*b.Pnp.(b.grad(u_p)) - 2*b.Pnp.rotE_H_Bn
#                                                #      = 2*b.Pnp.(b.grad(u_p) - rotE_H_Bn)
#   for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#       tmp2 += 2.*b[j]*Pnp[j,k]*b[i]*grad_u_p[i,k]
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       tmp2 += -2.*b[j]*Pnp[j,i]*rotE_H_Bn[i]

#   tmp3 = np.zeros((nx,ny,nz),dtype=np.float64) # tmp3 = bb:(div(Q)) = b_i*b_j*der_k*S_ijk
#   for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#       tmp = calc.calc_gradx(Sp[i,j,0])
#       tmp3 += bb[i,j]*tmp
#       tmp = calc.calc_grady(Sp[i,j,1])
#       tmp3 += bb[i,j]*tmp
#       tmp = calc.calc_gradz(Sp[i,j,2])
#       tmp3 += bb[i,j]*tmp
#   del tmp

#   ng_Sgp = -(np.divide(1.,pparlp) - np.divide(1.,pperpp))*(tmp1 - 0.5*tmp2 + 0.5*tmp3*0.)
#          # ng_Sgp = -(1/pparlp - 1/pperpp)*[bb:(Pnp.grad(u_p)) - (d(bb)/dt):Pnp/2 + bb:(div(S))/2]
#          #                                        tmp1                 -tmp2/2         +tmp3/2
#   del tmp1,tmp2,tmp3

#   if code_name == 'iPIC':
#       tmp1 = np.zeros((nx,ny,nz),dtype=np.float64)  # tmp1 = bb:(Pne.grad(u_e))
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           tmp1 += bb[k,j]*Pne[j,i]*grad_u_e[i,k]

#       tmp2 = np.zeros((nx,ny,nz),dtype=np.float64) # tmp2 = 2*b.Pne.(b.grad(u_p)) - 2*b.Pne.rotE_H_Bn
#                                                    #      = 2*b.Pne.(b.grad(u_p) - rotE_H_Bn)
#       for (i,j,k) in [(i,j,k) for i in range(3) for j in range(3) for k in range(3)]:
#           tmp2 += 2.*b[j]*Pne[j,k]*b[i]*grad_u_p[i,k]
#       for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#           tmp2 += -2.*b[j]*Pne[j,i]*rotE_H_Bn[i]

#       tmp3 = np.zeros((nx,ny,nz),dtype=np.float64) # tmp3 = bb:(div(Q)) = b_i*b_j*der_k*S_ijk
#       for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#           tmp = calc.calc_gradx(Se[i,j,0])
#           tmp3 += bb[i,j]*tmp
#           tmp = calc.calc_grady(Se[i,j,1])
#           tmp3 += bb[i,j]*tmp
#           tmp = calc.calc_gradz(Se[i,j,2])
#           tmp3 += bb[i,j]*tmp
#       del tmp

#       ng_Sge = -(np.divide(1.,pparle) - np.divide(1.,pperpe))*(tmp1 - 0.5*tmp2 + 0.5*tmp3*0.)#!!!!!!!!
#              # ng_Sge = -(1/pparle - 1/pperpe)*[bb:(Pne.grad(u_e)) - (d(bb)/dt):Pne/2 + bb:(div(S))/2]
#              #                                        tmp1                 -tmp2/2          +tmp3/2
#       del tmp1,tmp2,tmp3
#   elif code_name == 'HVM':
#       if w_ele:
#           tmp3 = np.zeros((nx,ny,nz),dtype=np.float64) # tmp3 = bb:(div(Q)) = b_i*b_j*der_k*S_ijk
#           for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
#               tmp = calc.calc_gradx(Se[i,j,0])
#               tmp3 += bb[i,j]*tmp
#               tmp = calc.calc_grady(Se[i,j,1])
#               tmp3 += bb[i,j]*tmp
#               tmp = calc.calc_gradz(Se[i,j,2])
#               tmp3 += bb[i,j]*tmp
#           del tmp

#           ng_Sge = -(np.divide(1.,pparle) - np.divide(1.,pperpe))*(0.5*tmp3*0.)#!!!!!!!!!!!!!!
#                  # ng_Sge = -(1/pparle - 1/pperpe)*[bb:(div(S))/2]
#                  #                                     tmp3/2
#           del tmp3
#       else:
#           ng_Sge = np.zeros((nx,ny,nz),dtype=np.float64)

#   del grad_u_p
#   del grad_u_e

#   if code_name == 'iPIC' and smooth_flag_post:
#       ng_Sgp[:,:,0] = gf(ng_Sgp[:,:,0],[gfs,gfs],mode='wrap')
#       ng_Sge[:,:,0] = gf(ng_Sge[:,:,0],[gfs,gfs],mode='wrap')

#   #save means
#   m_Sfp.append(np.nanmean(Sfp,dtype=np.float64))
#   m_adv_Sfp.append(np.nanmean(adv_Sfp,dtype=np.float64))
#   m_gPiD_Sfp.append(np.nanmean(gPiD_Sfp,dtype=np.float64))
#   m_nPiD_Sfp.append(np.nanmean(nPiD_Sfp,dtype=np.float64))
#   m_vhf_Sfp.append(np.nanmean(vhf_Sfp,dtype=np.float64))

#   m_Sfe.append(np.nanmean(Sfe,dtype=np.float64))
#   m_adv_Sfe.append(np.nanmean(adv_Sfe,dtype=np.float64))
#   m_gPiD_Sfe.append(np.nanmean(gPiD_Sfe,dtype=np.float64))
#   m_nPiD_Sfe.append(np.nanmean(nPiD_Sfe,dtype=np.float64))
#   m_vhf_Sfe.append(np.nanmean(vhf_Sfe,dtype=np.float64))

#   m_Sgp.append(np.nanmean(Sgp,dtype=np.float64))
#   m_adv_Sgp.append(np.nanmean(adv_Sgp,dtype=np.float64))
#   m_nPiD_Sgp.append(np.nanmean(nPiD_Sgp,dtype=np.float64))
#   m_vhf_Sgp.append(np.nanmean(vhf_Sgp,dtype=np.float64))
#   m_ng_Sgp.append(np.nanmean(ng_Sgp,dtype=np.float64))

#   if w_ele:
#       m_Sge.append(np.nanmean(Sge,dtype=np.float64))
#       m_adv_Sge.append(np.nanmean(adv_Sge,dtype=np.float64))
#       m_nPiD_Sge.append(np.nanmean(nPiD_Sge,dtype=np.float64))
#       m_vhf_Sge.append(np.nanmean(vhf_Sge,dtype=np.float64))
#       m_ng_Sge.append(np.nanmean(ng_Sge,dtype=np.float64))

    #save times
#    t_vec.append(time[ind])

#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
####del Sfp
####del adv_Sfp
####del gPiD_Sfp
####del nPiD_Sfp
####del vhf_Sfp

####del Sfe
####del adv_Sfe
####del gPiD_Sfe
####del nPiD_Sfe
####del vhf_Sfe

####del Sgp
####del adv_Sgp
####del nPiD_Sgp
####del vhf_Sgp
####del ng_Sgp

####if w_ele:
####    del Sge
####    del adv_Sge
####    del nPiD_Sge
####    del vhf_Sge
####    del ng_Sge

#####from list to numpy.array
####t_vec = np.array(t_vec)

####m_Sfp = np.array(m_Sfp)
####m_adv_Sfp = np.array(m_adv_Sfp)
####m_gPiD_Sfp = np.array(m_gPiD_Sfp)
####m_nPiD_Sfp = np.array(m_nPiD_Sfp)
####m_vhf_Sfp = np.array(m_vhf_Sfp)

####m_Sfe = np.array(m_Sfe)
####m_adv_Sfe = np.array(m_adv_Sfe)
####m_gPiD_Sfe = np.array(m_gPiD_Sfe)
####m_nPiD_Sfe = np.array(m_nPiD_Sfe)
####m_vhf_Sfe = np.array(m_vhf_Sfe)

####m_Sgp = np.array(m_Sgp)
####m_adv_Sgp = np.array(m_adv_Sgp)
####m_nPiD_Sgp = np.array(m_nPiD_Sgp)
####m_vhf_Sgp = np.array(m_vhf_Sgp)
####m_ng_Sgp = np.array(m_ng_Sgp)

####if w_ele:
####    m_Sge = np.array(m_Sge)
####    m_adv_Sge = np.array(m_adv_Sge)
####    m_nPiD_Sge = np.array(m_nPiD_Sge)
####    m_vhf_Sge = np.array(m_vhf_Sge)
####    m_ng_Sge = np.array(m_ng_Sge)

#####rate of change
####r_m_Sfp = np.gradient(m_Sfp,t_vec/run.meta['tmult'])
####r_m_Sfe = np.gradient(m_Sfe,t_vec/run.meta['tmult'])
####r_m_Sgp = np.gradient(m_Sgp,t_vec/run.meta['tmult'])
####if w_ele:
####    r_m_Sge = np.gradient(m_Sge,t_vec/run.meta['tmult'])

#####deviation from equation balance
####diff_Sfp = r_m_Sfp - m_adv_Sfp - m_gPiD_Sfp - m_nPiD_Sfp - m_vhf_Sfp
####diff_Sfe = r_m_Sfe - m_adv_Sfe - m_gPiD_Sfe - m_nPiD_Sfe - m_vhf_Sfe
####diff_Sgp = r_m_Sgp - m_adv_Sgp - m_nPiD_Sgp - m_vhf_Sgp - m_ng_Sgp
####if w_ele:
####    diff_Sge = r_m_Sge - m_adv_Sge - m_nPiD_Sge - m_vhf_Sge - m_ng_Sge

#####write on file
####f = open(opath+'/'+out_dir+'/'+'entr_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.txt','w')
####for i in range(len(t_vec)):
####    if w_ele:
####        f.write(str(t_vec[i])+' '+str(m_Sfp[i])+' '+str(r_m_Sfp[i])+' '+str(m_adv_Sfp[i])+' '+
####                str(m_gPiD_Sfp[i])+' '+str(m_nPiD_Sfp[i])+' '+str(m_vhf_Sfp[i])+' '+
####                str(diff_Sfp[i])+' '+str(m_Sfe[i])+' '+str(r_m_Sfe[i])+' '+str(m_adv_Sfe[i])+' '+
####                str(m_gPiD_Sfe[i])+' '+str(m_nPiD_Sfe[i])+' '+str(m_vhf_Sfe[i])+' '+
####                str(diff_Sfe[i])+' '+str(m_Sgp[i])+' '+str(r_m_Sgp[i])+' '+str(m_adv_Sgp[i])+' '+
####                str(m_nPiD_Sgp[i])+' '+str(m_vhf_Sgp[i])+' '+str(m_ng_Sgp[i])+' '+
####                str(diff_Sgp[i])+' '+str(m_Sge[i])+' '+str(r_m_Sge[i])+' '+str(m_adv_Sge[i])+' '+
####                str(m_nPiD_Sge[i])+' '+str(m_vhf_Sge[i])+' '+str(m_ng_Sge[i])+' '+str(diff_Sge[i])+'\n')
####    else:
####        f.write(str(t_vec[i])+' '+str(m_Sfp[i])+' '+str(r_m_Sfp[i])+' '+str(m_adv_Sfp[i])+' '+
####                str(m_gPiD_Sfp[i])+' '+str(m_nPiD_Sfp[i])+' '+str(m_vhf_Sfp[i])+' '+
####                str(diff_Sfp[i])+' '+str(m_Sfe[i])+' '+str(r_m_Sfe[i])+' '+str(m_adv_Sfe[i])+' '+
####                str(m_gPiD_Sfe[i])+' '+str(m_nPiD_Sfe[i])+' '+str(m_vhf_Sfe[i])+' '+
####                str(diff_Sfe[i])+' '+str(m_Sgp[i])+' '+str(r_m_Sgp[i])+' '+str(m_adv_Sgp[i])+' '+
####                str(m_nPiD_Sgp[i])+' '+str(m_vhf_Sgp[i])+' '+str(m_ng_Sgp[i])+' '+
####                str(diff_Sgp[i])+'\n')
####f.close()

#####plot
####plt.close('all')
####fig,ax = plt.subplots(2,2,figsize=(4*2,4*2))

####ax[0,0].plot(t_vec,r_m_Sfp,label='rate')
####ax[0,0].plot(t_vec,m_adv_Sfp,label='adv')
####ax[0,0].plot(t_vec,m_gPiD_Sfp,label='gPiD')
####ax[0,0].plot(t_vec,m_nPiD_Sfp,label='nPiD')
####ax[0,0].plot(t_vec,m_vhf_Sfp,label='vhf')
####ax[0,0].plot(t_vec,diff_Sfp,label='diff')
####ax[0,0].set_xlabel('t')
####ax[0,0].set_title('Isotropic protons entropy')
####ax[0,0].legend()

####ax[0,1].plot(t_vec,r_m_Sfe,label='rate')
####ax[0,1].plot(t_vec,m_adv_Sfe,label='adv')
####ax[0,1].plot(t_vec,m_gPiD_Sfe,label='gPiD')
####ax[0,1].plot(t_vec,m_nPiD_Sfe,label='nPiD')
####ax[0,1].plot(t_vec,m_vhf_Sfe,label='vhf')
####ax[0,1].plot(t_vec,diff_Sfe,label='diff')
####ax[0,1].set_xlabel('t')
####ax[0,1].set_title('Isotropic electrons entropy')
####ax[0,1].legend()

####ax[1,0].plot(t_vec,r_m_Sgp,label='rate')
####ax[1,0].plot(t_vec,m_adv_Sgp,label='adv')
####ax[1,0].plot(t_vec,m_nPiD_Sgp,label='nPiD')
####ax[1,0].plot(t_vec,m_vhf_Sgp,label='vhf')
####ax[1,0].plot(t_vec,m_ng_Sgp,label='ng')
####ax[1,0].plot(t_vec,diff_Sgp,label='diff')
####ax[1,0].set_xlabel('t')
####ax[1,0].set_title('anisotropic protons entropy')
####ax[1,0].legend()

####if w_ele:
####    ax[1,1].plot(t_vec,r_m_Sge,label='rate')
####    ax[1,1].plot(t_vec,m_adv_Sge,label='adv')
####    ax[1,1].plot(t_vec,m_nPiD_Sge,label='nPiD')
####    ax[1,1].plot(t_vec,m_vhf_Sge,label='vhf')
####    ax[1,1].plot(t_vec,m_ng_Sge,label='ng')
####    ax[1,1].plot(t_vec,diff_Sge,label='diff')
####    ax[1,1].set_xlabel('t')
####    ax[1,1].set_title('anisotropic electrons entropy')
####    ax[1,1].legend()

####plt.tight_layout()
####plt.savefig(opath+'/'+out_dir+'/'+'entr_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
####plt.close()
