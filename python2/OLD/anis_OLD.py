#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
sys.path.insert(0,'/work1/califano/VISUAL/PYTHON')
import fibo_beta as fb
import mylib as ml
import fourier as fl

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
ipath = l1[0]
opath = l2[0]

run = fb.from_HVM(ipath)
try:
    run.get_meta('00')
except:
    ipp = ml.secure_input('Insert the name of the directory containing input_parameters: ','')
    run.get_meta(ipp)

run.segs = ml.dict_keys_sort(run.segs)
time,time2seg,time2exit = ml.time_manage(run.segs)
print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)

x,y,z = ml.create_xyz(run.meta['nnn'],run.meta['lll'])

w_ele = run.meta['model'] > 1

print('\nMetadata loaded\n')

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]

anisp_min = np.zeros(act_nt)
anisp_max = np.zeros(act_nt)
if w_ele:
    anise_min = np.zeros(act_nt)
    anise_max = np.zeros(act_nt)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing anisotropy
    #-------------------------------------
    E,B = run.get_EB(time2seg[ind],time2exit[ind])
    B[0] = B[0] + run.meta['Bx0']
    B[1] = B[1] + run.meta['By0']
    B[2] = B[2] + run.meta['Bz0']
    del E

    P = run.get_Press(time2seg[ind],time2exit[ind])

    pparlp,pperpp = ml.proj_tens_on_vec(P,B,True)
    del P

    if w_ele:
        tmp = run.get_Te(time2seg[ind],time2exit[ind])
        tparle = tmp[0]
        tperpe = tmp[1]
        del tmp

    rho,tmp = run.get_Ion(time2seg[ind],time2exit[ind])
    del tmp
    if w_ele:
        pparle = tparle*rho
        pperpe = tperpe*rho
        gc.collect()

    anisp = pperpp/pparlp -1.
    if w_ele:
        anise = pperpe/pparle -1.

    #----------------------------------------------------
    #plotting anis2d
    #---------------
    ml.create_path(opath+'/anis2d')
    plt.close()
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig, ax = plt.subplots(nrows=nplot,ncols=1,figsize=(10,4*nplot))
    if w_ele:
        ax0 = ax[0]
    else:
        ax0 = ax
    im = ax0.contourf(x,y,anisp[:,:,0].T)
    plt.colorbar(im,ax=ax0)
    ax0.set_title('Proton anisotropy - t='+str(time[ind]))
    ax0.set_xlabel('x [d_i]')
    ax0.set_ylabel('y [d_i]')
    ax0.set_xlim(x[ixmin],x[ixmax])
    ax0.set_ylim(y[iymin],y[iymax])
    if w_ele:
        im = ax[1].contourf(x,y,anise[:,:,0].T)
        plt.colorbar(im,ax=ax[1])
        ax[1].set_title('Electorn anisotropy')
        ax[1].set_xlabel('x [d_i]')
        ax[1].set_ylabel('y [d_i]')
        ax[1].set_xlim(x[ixmin],x[ixmax])
        ax[1].set_ylim(y[iymin],y[iymax])
    plt.tight_layout()
    plt.savefig(opath+'/anis2d/'+'anis2d_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #-----------------------------------------------------
    #get min and max of anisotropy
    #-----------------------------
    anisp_min[act_cnt] = np.min(anisp)
    anisp_max[act_cnt] = np.max(anisp)
    if w_ele:
        anise_min[act_cnt] = np.min(anise)
        anise_max[act_cnt] = np.max(anise)

#---> loop over time <---
    act_cnt = act_cnt + 1
    print "\r",
    print "t = ",time[ind],
    sys.stdout.flush()
#------------------------

#------------------------------------------------------
#max and min anisotropy vs. time
#-------------------------------
ml.create_path(opath+'/anis_minmax')
plt.close()
fig, ax = plt.subplots(nrows=1,ncols=1,figsize=(10,8))
im = ax.plot(act_time,anisp_max,'b',label='protons')
im = ax.plot(act_time,anisp_min,'b')
if w_ele:
    im = ax.plot(act_time,anise_max,'r',label='electrons')
    im = ax.plot(act_time,anise_min,'r')
ax.set_title('Max and min anisotropy - t = '+str(time[ind1])+' -> '+str(time[ind2]))
ax.set_xlabel('t [ion cyclotron times]')
ax.set_ylabel('pperp/pparl - 1 ')
plt.tight_layout()
plt.legend()
plt.savefig(opath+'/anis_minmax/'+'anis_minmax_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
