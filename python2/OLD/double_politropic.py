#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.optimize import curve_fit
from scipy.ndimage import gaussian_filter as gf
from scipy.signal import argrelextrema
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nDouble-polytropic study.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,ixmax = xr

run_name = run.meta['run_name']
out_dir = 'double_polytropic/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']
x = run.meta['x']
y = run.meta['y']

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

wl.Can_I()

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#threshold parameter (closer to 1 this, the smaller the the island)
th_param = 0.98#0.87

#-------------------------------------------------------
#functions
#---------
@njit('f8[:](f8[:],f8[:],f8[:],f8)')
def Cparl(p_,n_,B_,g_):
    return np.divide(p_*B_**(g_-1.),n_**g_)

@njit('f8[:](f8[:],f8[:],f8[:],f8)')
def Cperp(p_,n_,B_,g_):
    return np.divide(p_,n_*B_**(g_-1.))

def C_std(pparl_,pperp_,n_,B_,gparl_range=[0.5,4.0],gperp_range=[0.5,2.5],dg=0.005):
    gparl_ = np.arange(gparl_range[0],gparl_range[1]+dg,dg)
    Cparl_std_ = np.empty(gparl_.shape,dtype=np.float64)
    for i in range(len(gparl_)):
        Cparl_std_[i] = np.std(Cparl(pparl_,n_,B_,gparl_[i]))
#
    gperp_ = np.arange(gperp_range[0],gperp_range[1]+dg,dg)
    Cperp_std_ = np.empty(gperp_.shape,dtype=np.float64)
    for i in range(len(gperp_)):
        Cperp_std_[i] = np.std(Cperp(pperp_,n_,B_,gperp_[i]))
#
    return gparl_,Cparl_std_,gperp_,Cperp_std_



#-------------------------------------------------------
#da loop
#-------
#data structures
Bn_isl = []
n_isl = []

gparlp_isl = []
gperpp_isl = []
gparle_isl = []
gperpe_isl = []
stdparlp_isl = []
stdperpp_isl = []
stdparle_isl = []
stdperpe_isl = []

t_loop = []
t_parlp = []
t_perpp = []
t_parle = []
t_perpe = []
#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #getting data and computing stuff
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC':
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[5,5],mode='wrap')
    Bn = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
#
    Pp = run.get_Press(ind)
    if code_name == 'iPIC':
        for i in range(3):
            for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[5,5],mode='wrap')
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
#
    n_p,_ = run.get_Ion(ind)
    if code_name == 'iPIC': n_p[:,:,0] = gf(n_p[:,:,0],[5,5],mode='wrap')
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n_e = n_p.copy()
            pparle = pparle*n_e
            pperpe = pperpe*n_e
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            if code_name == 'iPIC':
                for i in range(3):
                    for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[5,5],mode='wrap')
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
            n_e,_ = run.get_Ion(ind,qom=qom_e)
            if code_name == 'iPIC': n_e[:,:,0] = gf(n_e[:,:,0],[5,5],mode='wrap')
        else:
            print('\nWhat code is it?\n')
#
    mask = (Psi > np.max(Psi)*th_param).reshape(nx,ny,nz)
#
    #B-n in island
    Bn_isl.append(np.mean(Bn[mask]))
    n_isl.append(np.mean(n_p[mask]))
#
    #adiabatic invariants standard deviation
    gparlp,Cparlp_std,gperpp,Cperpp_std = C_std(pparlp[mask],pperpp[mask],n_p[mask],Bn[mask])
    i = argrelextrema(Cparlp_std,np.less)
    if len(i[0]) > 0:
        gparlp_isl.append(gparlp[i[0][0]])
        stdparlp_isl.append(Cparlp_std[i[0][0]])
        t_parlp.append(time[ind])
    i = argrelextrema(Cperpp_std,np.less)
    if len(i[0]) > 0:
        gperpp_isl.append(gperpp[i[0][0]])
        stdperpp_isl.append(Cperpp_std[i[0][0]])
        t_perpp.append(time[ind])
    if w_ele:
        gparle,Cparle_std,gperpe,Cperpe_std = C_std(pparle[mask],pperpe[mask],n_e[mask],Bn[mask])
        i = argrelextrema(Cparle_std,np.less)
        if len(i[0]) > 0:
            gparle_isl.append(gparle[i[0][0]])
            stdparle_isl.append(Cparle_std[i[0][0]])
            t_parle.append(time[ind])
        i = argrelextrema(Cperpe_std,np.less)
        if len(i[0]) > 0:
            gperpe_isl.append(gperpe[i[0][0]])
            stdperpe_isl.append(Cperpe_std[i[0][0]])
            t_perpe.append(time[ind])
#
    #plot C_std
    plt.close()
    fig,ax = plt.subplots(1,1,figsize=(8,8))
#
    ax.plot(gparlp,np.log10(Cparlp_std),'r',label='$\gamma_{\parallel,p}=$'+str(gparlp_isl[-1]))
    ax.axvline(x=gparlp_isl[-1],ls=':',c='k')
    ax.axhline(y=np.log10(stdparlp_isl[-1]),ls=':',c='k')
    ax.plot(gperpp,np.log10(Cperpp_std),'--r',label='$\gamma_{\perp,p}=$'+str(gperpp_isl[-1]))
    ax.axvline(x=gperpp_isl[-1],ls=':',c='k')
    ax.axhline(y=np.log10(stdperpp_isl[-1]),ls=':',c='k')
    if w_ele:
        ax.plot(gparle,np.log10(Cparle_std),'b',label='$\gamma_{\parallel,e}=$'+str(gparle_isl[-1]))
        ax.axvline(x=gparle_isl[-1],ls=':',c='k')
        ax.axhline(y=np.log10(stdparle_isl[-1]),ls=':',c='k')
        ax.plot(gperpe,np.log10(Cperpe_std),'--b',label='$\gamma_{\perp,e}=$'+str(gperpe_isl[-1]))
        ax.axvline(x=gperpe_isl[-1],ls=':',c='k')
        ax.axhline(y=np.log10(stdperpe_isl[-1]),ls=':',c='k')
    ax.set_xlabel('$\gamma$');ax.set_ylabel('$std(C)$')
    ax.set_title(run_name+' - t='+str(time[ind]))
    ax.legend()
#
    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/gamma_via_Cstd_'+run_name+'_'+str(ind)+'.png')
    plt.close()
#
    t_loop.append(time[ind])
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
t_loop = np.array(t_loop)
t_parlp = np.array(t_parlp)
t_perpp = np.array(t_perpp)
if w_ele:
    t_parle = np.array(t_parle)
    t_perpe = np.array(t_perpe)

Bn_isl = np.array(Bn_isl)
n_isl = np.array(n_isl)

gparlp_isl = np.array(gparlp_isl)
gperpp_isl = np.array(gperpp_isl)
stdparlp_isl = np.array(stdparlp_isl)
stdperpp_isl = np.array(stdperpp_isl)
if w_ele:
    gparle_isl = np.array(gparle_isl)
    gperpe_isl = np.array(gperpe_isl)
    stdparle_isl = np.array(stdparle_isl)
    stdperpe_isl = np.array(stdperpe_isl)

#B-n in time plot
plt.close()
fig,ax = plt.subplots(1,1,figsize=(8,10))

im = ax.scatter(n_isl,Bn_isl,c=t_loop,cmap='gist_rainbow')
cb = plt.colorbar(im,ax=ax);cb.set_label('time')
ax.set_xlabel('n');ax.set_ylabel('$|B|$')
ax.set_title('Average n and $|B|$ values in islands - '+run_name)
ax.set_xlim(np.min(n_isl),np.max(n_isl));ax.set_ylim(np.min(Bn_isl),np.max(Bn_isl))
ax.legend()

plt.tight_layout()
#plt.show()
plt.savefig(opath+'/'+out_dir+'/n_B_isl_avg_'+run_name+'.png')
plt.close()

#plot gamma over time
plt.close()
fig,ax = plt.subplots(1,1,figsize=(8,8))

ax.errorbar(t_parlp,gparlp_isl,yerr=stdparlp_isl,fmt='r',label='$\gamma_{\parallel,p}$')
ax.errorbar(t_perpp,gperpp_isl,yerr=stdperpp_isl,fmt='--r',label='$\gamma_{\perp,p}$')
if w_ele:
    ax.errorbar(t_parle,gparle_isl,yerr=stdparle_isl,fmt='b',label='$\gamma_{\parallel,e}$')
    ax.errorbar(t_perpe,gperpe_isl,yerr=stdperpe_isl,fmt='--b',label='$\gamma_{\perp,e}$')
ax.set_xlabel('$t [\Omega_{c,p}^{-1}]$');ax.set_ylabel('$\gamma \pm std(C)$')
ax.set_title(run_name+' - double polytropic')

plt.tight_layout()
#plt.show()
plt.savefig(opath+'/'+out_dir+'/double_polytropic_'+run_name+'.png')
plt.close()
