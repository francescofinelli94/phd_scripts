#-------------------------------------------------------
#imports
#----------------
import sys
import gc
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.signal as scisi

import work_lib as wl
import shift_and_flood as saf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#-------------------------------------------------------
#init
#----------------
run,tr,_=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'windowed_island_static'
opath = run.meta['opath']
code_name = run.meta['code_name']
nx,ny,_ = run.meta['nnn']
dx = run.meta['dx']
dy = run.meta['dy']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

#-------------------------------------------------------
#inputs
#----------------
#cuts
L1 = 0.85 #HARDCODED
L2 = 1.7 #HARDCODED
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
exclusion_width = 5. #HARDCODED
cut1 = int((y2-exclusion_width)/run.meta['dy'])
cut2 = int((y2+exclusion_width)/run.meta['dy'])
y0 = int(y1/run.meta['dy'])
yr = int(min(abs(y1-y2),run.meta['yl']-abs(y1-y2))/2./run.meta['dy'])
perm_sh_y = y0 - yr

#number of points to fit for "instantaneous" growth rate
p2f = 10 #HARDCODED

#threshold parameter: threshold = min(field)*th_param ; min(field) < 0
th_param = ml.secure_input('Choose threshold parameter. Recommended: 0.98 : ',1.,True)

#window
print('Choose window dimensions. Recommended: 512x128 or 256x128.')
nwx = ml.secure_input('x dimension: ',10,True)
nwy = ml.secure_input('y dimension: ',10,True)

    #'boxcar', 'hamming', 'hann', 'blackman', 'flattop', 'nuttall', 'blackmanharris'
    # (+) <-  FREQ. RES.  <- (-)
    # (-) ->  DYN. RANGE  -> (+)
print('Suggested windows:')
print('boxcar, hamming, hann, blackman, flattop, nuttall, blackmanharris')
print(' (+) <-  FREQ. RES.  <- (-)')
print(' (-) ->  DYN. RANGE  -> (+)')
wname = ml.secure_input('chosen window: ','',True)

#time interval overwrite
print('TIME INTERVAL OVERWRITE!')
print('Max time range: '+str(np.min(time))+' -> '+str(np.max(time)))
t_start = ml.secure_input('insert starting time: ',1.,True)
t_stop  = ml.secure_input('insert stopping time: ',1.,True)
ind1 = ml.index_of_closest(t_start,time)
ind2 = ml.index_of_closest(t_stop,time)

#subpath
out_dir = ( out_dir + '/' + run_name + '/' + wname + '_' +
            str(nwx) + '_' + str(nwy)+ '_' + str(ind1)+ '_' + str(ind2) )
ml.create_path(opath+'/'+out_dir)

#it's ok?
wl.Can_I()

#-------------------------------------------------------
#main loop
#----------------
B_flag = False
B_sh = 0
#---> loop over time <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#------------------------
    #get the data
    _,B = run.get_EB(ind)
    if perm_sh_y < 0:
        B = np.roll(B,-perm_sh_y,axis=2)
        y0 = y0 - perm_sh_y
        B_sh = perm_sh_y
        perm_sh_y = 0
        B_flag = True
    if B_flag:
        B = np.roll(B,-B_sh,axis=2)
    Psi = wl.psi_2d(B[0:2,:,:,0],run.meta)

    #find the island
    reg,nreg = saf.my_flood(-Psi,-np.max(Psi)*th_param,periodicity=[True,True])

    v0 = 0
    cnt = 0
    for n in range(nreg):
        c = np.sum(reg == n+1)
        if c > cnt:
            cnt = c
            v0 = n + 1

    reg = reg == v0
    for ix in range(nx):
        if np.sum(reg[ix,:]) > 0:
            x1 = ix
            break
    for ix in range(nx):
        if ix < x1:
            continue
        if np.sum(reg[ix,:]) == 0:
            x2 = ix - 1 
            break
    if x1 == 0:
        try:
            for ix in range(nx):
                if ix <= x2:
                    continue
                if np.sum(reg[ix,:]) > 0:
                    x1 = ix
                    break
        except:
            x2 = nx -1

    for iy in range(ny):
        if np.sum(reg[:,iy]) > 0:
            y1 = iy
            break
    for iy in range(ny):
        if iy < y1:
            continue
        if np.sum(reg[:,iy]) == 0:
            y2 = iy - 1
            break
    if y1 == 0:
        try:
            for iy in range(ny):
                if iy <= y2:
                    continue
                if np.sum(reg[:,iy]) > 0:
                    y1 = iy
                    break
        except:
            y2 = ny - 1

    if x1 > x2:
        sh = x2 + 1
        B = np.roll(B,-sh,axis=1)
        Psi = np.roll(Psi,-sh,axis=0)
        x1 = x1 - sh
        x2 = nx - 1
    if y1 > y2:
        sh = y2 + 1
        B = np.roll(B,-sh,axis=2)
        Psi = np.roll(Psi,-sh,axis=1)
        y1 = y1 - sh
        y2 = ny - 1

    xg = 0
    yg = 0
    cnt = 0
    for ix in range(x2-x1+1):
        for iy in range(y2-y1+1):
            if reg[ix+x1,iy+y1]:
                cnt += 1
                xg += ix + x1
                yg += iy + y1
    xg = int(float(xg)/float(cnt))
    yg = int(float(yg)/float(cnt))

    x1 = (xg - (nwx-1)//2 + nx)%nx
    x2 = (xg + (nwx)//2)%nx
    y1 = (yg - (nwy-1)//2 + ny)%ny
    y2 = (yg + (nwy)//2)%ny

    if x1 > x2:
        sh = x2 + 1
        B = np.roll(B,-sh,axis=1)
        Psi = np.roll(Psi,-sh,axis=0)
        x1 = x1 - sh
        x2 = nx - 1
    if y1 > y2:
        sh = y2 + 1
        B = np.roll(B,-sh,axis=2)
        Psi = np.roll(Psi,-sh,axis=1)
        y1 = y1 - sh
        y2 = ny - 1

    winx = scisi.windows.get_window(wname,nwx)
    winy = scisi.windows.get_window(wname,nwy)
    win0 = scisi.windows.get_window(wname,cut1)

    #0: full box
    #---> wfft
    data = np.concatenate((B[:,:cut1,0],B[:,cut2+1:,0]),axis=1)
    MAx = []
    MAy = []
    for i in range(3):
        for iy in range(ny+cut1-cut2-1):
            kx_,tmp = mode_ampl_1d(data[i,:,iy],dx)
            if MAx == []:
                MAx = np.zeros((3,len(kx_)),dtype=np.float64)
            MAx[i] += tmp
        for ix in range(nx):
            ky_,tmp = mode_ampl_1d(data[i,ix,:cut1]*win0,dy)
            if MAy == []:
                MAy = np.zeros((3,len(ky_)),dtype=np.float64)
            MAy[i] += tmp

    MAx = MAx/float(ny+cut1-cut2-1)
    MAy = MAy/float(nx)
    mx = kx_/kx_[1]
    my = ky_/ky_[1]*float(ny)/float(ny+cut1-cut2-1)

    #---> save modes at this time
    MAx_0.append(MAx)
    MAy_0.append(MAy)
    if mx0 == []:
        mx0 = mx
    if my0 == []:
        my0 = my

    #1: primary sub-box
    #---> wfft
    data = B[:,x1:x2+1,y1:y2+1,0].copy()
    MAx = []
    MAy = []
    for i in range(3):
        for iy in range(nwy):
            kx_,tmp = mode_ampl_1d(data[i,:,iy]*winx,dx)
            if MAx == []:
                MAx = np.zeros((3,len(kx_)),dtype=np.float64)
            MAx[i] += tmp
        for ix in range(nwx):
            ky_,tmp = mode_ampl_1d(data[i,ix,:]*winy,dy)
            if MAy == []:
                MAy = np.zeros((3,len(ky_)),dtype=np.float64)
            MAy[i] += tmp

    MAx = MAx/float(nwy)
    MAy = MAy/float(nwx)
    mx = kx_/kx_[1]*float(nx)/float(nwx)
    my = ky_/ky_[1]*float(ny)/float(nwy)

    #---> save modes at this time
    MAx_1.append(MAx)
    MAy_1.append(MAy)
    if mx1 == []:
        mx1 = mx
    if my1 == []:
        my1 = my

    #2: secondary sub-box
    #---> it's shift time!
    B = np.roll(B,nx//2,axis=1)

    #---> wfft
    data = B[:,x1:x2+1,y1:y2+1,0].copy()
    MAx = []
    MAy = []
    for i in range(3):
        for iy in range(nwy):
            kx_,tmp = mode_ampl_1d(data[i,:,iy]*winx,dx)
            if MAx == []:
                MAx = np.zeros((3,len(kx_)),dtype=np.float64)
            MAx[i] += tmp
        for ix in range(nwx):
            ky_,tmp = mode_ampl_1d(data[i,ix,:]*winy,dy)
            if MAy == []:
                MAy = np.zeros((3,len(ky_)),dtype=np.float64)
            MAy[i] += tmp

    MAx = MAx/float(nwy)
    MAy = MAy/float(nwx)
    mx = kx_/kx_[1]*float(nx)/float(nwx)
    my = ky_/ky_[1]*float(ny)/float(nwy)

    #---> save modes at this time
    MAx_2.append(MAx)
    MAy_2.append(MAy)
    if mx2 == []:
        mx2 = mx
    if my2 == []:
        my2 = my

    #save current time
    t_vec.append(time[ind])

"""
    #3x2 plot
    plt.close()
    fig,ax = plt.subplots(3,2,figsize=(24,12))

    ax[0,1].plot(winx,label='winx')
    ax[0,1].plot(winy,label='winy')
    ax[0,1].legend()
    ax[0,1].set_xlabel('indices');ax[0,1].set_ylabel('window');ax[0,1].set_title(str(wname))

    im0 = ax[0,0].contourf(B[0,:,y0-yr:y0+yr+1,0].T,64)
    plt.colorbar(im0,ax=ax[0,0])
    ax[0,0].contour(Psi[:,y0-yr:y0+yr+1].T,8,cmap='inferno')
    ax[0,0].axhline(y=y1,linestyle='--',color='k')
    ax[0,0].axhline(y=y2,linestyle='--',color='k')
    ax[0,0].axvline(x=x1,linestyle='--',color='k')
    ax[0,0].axvline(x=x2,linestyle='--',color='k')
    ax[0,0].set_xlabel('x indices');ax[0,0].set_ylabel('y indices');ax[0,0].set_title('Bx t='+str(time[ind]))

    im1 = ax[1,0].contourf(B[1,:,y0-yr:y0+yr+1,0].T,64)
    plt.colorbar(im1,ax=ax[1,0])
    ax[1,0].contour(Psi[:,y0-yr:y0+yr+1].T,8,cmap='inferno')
    ax[1,0].axhline(y=y1,linestyle='--',color='k')
    ax[1,0].axhline(y=y2,linestyle='--',color='k')
    ax[1,0].axvline(x=x1,linestyle='--',color='k')
    ax[1,0].axvline(x=x2,linestyle='--',color='k')
    ax[1,0].set_xlabel('x indices');ax[1,0].set_ylabel('y indices');ax[1,0].set_title('By')

    im2 = ax[2,0].contourf(B[2,:,y0-yr:y0+yr+1,0].T,64)
    plt.colorbar(im2,ax=ax[2,0])
    ax[2,0].contour(Psi[:,y0-yr:y0+yr+1].T,8,cmap='inferno')
    ax[2,0].axhline(y=y1,linestyle='--',color='k')
    ax[2,0].axhline(y=y2,linestyle='--',color='k')
    ax[2,0].axvline(x=x1,linestyle='--',color='k')
    ax[2,0].axvline(x=x2,linestyle='--',color='k')
    ax[2,0].set_xlabel('x indices');ax[2,0].set_ylabel('y indices');ax[2,0].set_title('Bz')

    ax[1,1].plot(mx[1:],PSDx[0,1:],'--',label='Bx')
    ax[1,1].plot(mx[1:],PSDx[1,1:],'--',label='By')
    ax[1,1].plot(mx[1:],PSDx[2,1:],'--',label='Bz')
    ax[1,1].plot(mx[1:],PSDx[0,1:]+PSDx[1,1:]+PSDx[2,1:],label='B')
    ax[1,1].legend();ax[1,1].set_xscale('log');ax[1,1].set_yscale('log')
    ax[1,1].set_xlabel('mx');ax[1,1].set_ylabel('PSD');ax[1,1].set_title('x transform')

    ax[2,1].plot(my[1:],PSDy[0,1:],'--',label='Bx')
    ax[2,1].plot(my[1:],PSDy[1,1:],'--',label='By')
    ax[2,1].plot(my[1:],PSDy[2,1:],'--',label='Bz')
    ax[2,1].plot(my[1:],PSDy[0,1:]+PSDy[1,1:]+PSDy[2,1:],label='B')
    ax[2,1].legend();ax[2,1].set_xscale('log');ax[2,1].set_yscale('log')
    ax[2,1].set_xlabel('my');ax[2,1].set_ylabel('PSD');ax[2,1].set_title('y transform')

    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/'+'windowed_island_'+wname+'_'+run_name+'_'+str(ind)+'.png')
    plt.close()
"""
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
