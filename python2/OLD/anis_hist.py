#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nWill plot ion and electron anisotropy distribution.\n')

#------------------------------------------------------
#read input file
#---------------
try:
    input_file = open(sys.argv[1],'r')
except:
    ml.error_msg('can not open the input_file.')
lines = input_file.readlines()
l0  = (lines[ 0]).split()
l1  = (lines[ 1]).split()
l2  = (lines[ 2]).split()
l3  = (lines[ 3]).split()
l4  = (lines[ 4]).split()
l5  = (lines[ 5]).split()

#------------------------------------------------------
#loading metadata
#----------------
run_name = l0[0]
code_name = l0[1]
ipath = l1[0]
opath = l2[0]

if code_name == 'HVM':
    run = from_HVM(ipath)
elif code_name == 'iPIC':
    run = from_iPIC(ipath)
else:
    print('\nWhat code is this?\n')
try:
    run.get_meta()
except:
    run.get_meta(l5[0])

w_ele = (run.meta['model'] > 1) or (run.meta['model'] < 0)

time = run.meta['time']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']

calc = fibo('calculator')
calc.meta = run.meta

print('\nMetadata loaded\n')

print('Segments: ')
print(run.segs.keys())
print('Times: ')
print(time)

#-------------------------------------------------------
#Asking about time and space intervals
#--------------------------
tbyval = bool(l3[0])
if tbyval:
    t1 = float(l3[1])
    ind1 = ml.index_of_closest(t1,time)
    t2 = float(l3[2])
    ind2 = ml.index_of_closest(t2,time)
else:
    ind1 = int(l3[1])
    ind2 = int(l3[2])
print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
ind_step = int(l3[3])

xbyval = bool(l4[0])
if xbyval:
    xmin  = float(l4[1])
    ixmin = ml.index_of_closest(xmin,x)
    xmax  = float(l4[2])
    ixmax = ml.index_of_closest(xmax,x)
    ymin  = float(l4[3])
    iymin = ml.index_of_closest(ymin,y)
    ymax  = float(l4[4])
    iymax = ml.index_of_closest(ymax,y)
else:
    ixmin = float(l4[1])
    ixmax = float(l4[2])
    iymin = float(l4[3])
    iymax = float(l4[4])
print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

#-------------------------------------------------------
#Ask permission to continue
#--------------------------
ans = ''
while (ans != 'y') and (ans != 'n'):
    ans = ml.secure_input('Continue? (y/n) ','',False)
if ans == 'n':
    sys.exit(-1)

#--------------------------------------------------------
#arrays declaration
#------------------
act_nt = (ind2-ind1)//ind_step+1 #actual numer of time exits considered
act_cnt = 0 #global index
act_time = time[ind1:ind2+ind_step:ind_step]

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    E,B = run.get_EB(ind)
    del E
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    #pparlp = (Pp[0,0]*B[0]*B[0] + Pp[1,1]*B[1]*B[1] + Pp[2,2]*B[2]*B[2] 
    #        + 2.*(Pp[0,1]*B[0]*B[1] + Pp[0,2]*B[0]*B[2] + Pp[1,2]*B[1]*B[2]))
    #pparlp = np.divide(pparlp,B[0]*B[0]+B[1]*B[1]+B[2]*B[2])
    #pperpp = 0.5*(Pp[0,0] + Pp[1,1] + Pp[2,2] - pparlp)
    del Pp

    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,tmp = run.get_Ion(ind)
            del tmp
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')

    anisp = np.divide(pperpp,pparlp) - 1.
    if w_ele:
        anise = np.divide(pperpe,pparle) - 1.

    #----------------------------------------------------
    #plotting anis 1D hist
    #-----------------------------
    ml.create_path(opath+'/anis_hist1d')
    plt.close()
    if w_ele:
        nplot = 2
    else:
        nplot = 1
    fig,ax = plt.subplots(1,nplot,figsize=(4*nplot,4))

    if w_ele:
        axs = ax[0]
    else:
        axs = ax
    h,b,im = axs.hist(anisp[:,:,0].flat,bins=1000,histtype='step',label='box')
    h,b,im = axs.hist(anisp[:,0:170,0].flat,bins=1000,histtype='step',label='CS1')
    h,b,im = axs.hist(anisp[:,170:512,0].flat,bins=1000,histtype='step',label='CS2')
    axs.set_title('Proton anisotropy - t = '+str(time[ind]))

    if w_ele:
        h,b,im = ax[1].hist(anise[:,:,0].flat,bins=1000,histtype='step',label='box')
        h,b,im = ax[1].hist(anise[:,0:170,0].flat,bins=1000,histtype='step',label='CS1')
        h,b,im = ax[1].hist(anise[:,170:512,0].flat,bins=1000,histtype='step',label='CS2')
        ax[1].set_title('Electron anisotropy')

    if w_ele:
        axiter = ax.flat
    else:
        axiter = [ax]
    for axs in axiter:
        axs.set_yscale('log')
        axs.set_xlabel('Pperp/Pparl-1')
        axs.set_ylabel('hist')
        axs.legend()

    plt.tight_layout()
    plt.savefig(opath+'/anis_hist1d/'+'anis_hist1d_'+run_name+'_'+str(ind)+'.png')
    plt.close()

#---> loop over time <---
    act_cnt = act_cnt + 1
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
