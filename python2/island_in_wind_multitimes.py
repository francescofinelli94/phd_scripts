#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
import work_lib as wl
from scipy.ndimage import gaussian_filter as gf
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
import thresholds as th

#latex fonts
font = 28#17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#Hawley colormap
bit_rgb = np.linspace(0,1,256)
colors = [(0,0,127), (0,3,255), (0,255,255), (128,128,128), (255,255,0),(255,0,0),(135,0,0)]
positions = [0.0,0.166667,0.333333,0.5,0.666667,0.833333,1.]
for iii in range(len(colors)):
    colors[iii] = (bit_rgb[colors[iii][0]],
                   bit_rgb[colors[iii][1]],
                   bit_rgb[colors[iii][2]])

cdict = {'red':[], 'green':[], 'blue':[]}
for pos, color in zip(positions, colors):
    cdict['red'].append((pos, color[0], color[0]))
    cdict['green'].append((pos, color[1], color[1]))
    cdict['blue'].append((pos, color[2], color[2]))

Hawley_cmap = mpl.colors.LinearSegmentedColormap('Hawley',cdict,256)

#my cmap
bit_rgb = np.linspace(0,1,256)
colors = [(0,255,0),(0,0,0)]
positions = [0.0,1.]
for iii in range(len(colors)):
    colors[iii] = (bit_rgb[colors[iii][0]],
                   bit_rgb[colors[iii][1]],
                   bit_rgb[colors[iii][2]])

cdict = {'red':[], 'green':[], 'blue':[]}
for pos, color in zip(positions, colors):
    cdict['red'].append((pos, color[0], color[0]))
    cdict['green'].append((pos, color[1], color[1]))
    cdict['blue'].append((pos, color[2], color[2]))

my_cmap = mpl.colors.LinearSegmentedColormap('mycmap',cdict,256)

#------------------------------------------------------
#Intent
#------
print('\nWill plot pdf of anisotrpy vs parallel beta cumulated over different times.\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'island_in_wind_multitimes_DEF'
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,_ = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']
teti = run.meta['teti']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#threshold parameter (closer to 1 this, the smaller the the island)
th_param = 0.90

#mask
mask_name = 'MT%03d'%(round(th_param*100.))

#species variables
p_vars = {'species_name':'Protons',
          'species_tag':'p',
          'b0_mult':1.}
e_vars = {'species_name':'Electrons',
          'species_tag':'e',
          'b0_mult':teti}
#-------------------------------------------------------
#plot function
#-------------
def windlike_plot_func( ax, xdata, ydata, species_vars,
                        xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                        legend_flag=False, run_label=run_label,
                        h_old=None, c_pass=0, n_pass=1 ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] or [array,array] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    run_label    -> [optional, set by default] string - run label
    h_old        -> [optional] None or np.array - default=None - 
                    where histograms are cumulated
    c_pass       -> [optional] int - default=0 - passage index
    n_pass       -> [optional] int - default=1 - number of cumulating passages

    OUTPUTS:
    if c_pass < n_pass-1:
        h            -> np.array - cumulated 2D histogram
    if c_pass == n_pass-1:
        handles      -> [lines.Line2D, ...] - list of handles for the legend
    if ERROR:
        -1           -> int - ERROR exit
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']
    b0_mult = species_vars['b0_mult']

    h,xbin,ybin=np.histogram2d(np.log10(xdata).flatten(),np.log10(ydata).flatten(),n_bins)
    if type(h_old) != type(None):
        if h_old.shape != h.shape:
            print('\nh_old.shape=%s is different from h.shape=%s\n'%(str(h_old.shape),str(h.shape)))
            return -1
        h = h + h_old
    if c_pass == n_pass-1:
        tmp_xbin = np.power(10.,xbin)
        tmp_ybin = np.power(10.,ybin)
        X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
        im = ax.pcolormesh(X,Y,h.T,cmap=Hawley_cmap,norm=mpl.colors.LogNorm())
        del X,Y,h

        if xdata_range == None:
            xdata_range = [np.min(tmp_xbin),np.max(tmp_xbin)]
        if ydata_range == None:
            ydata_range = [np.min(tmp_ybin),np.max(tmp_ybin)]
        xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),len(xbin)-1)
        del xbin,ybin,tmp_xbin,tmp_ybin

        if species_tag == 'p':
            p,f = th.params['L2014']['pC']['1.0e-2Op']['bM']
            lC, = ax.plot(xvals,th.func[f](xvals,*p),'darkorange',lw=2,label="Cyclotron (CI)")

            p,f = th.params['M2012']['pM']['1.0e-2Op']['bM']
            lM, = ax.plot(xvals,th.func[f](xvals,*p),'red',lw=2,label="Mirror (MI)")

            p,f = th.params['A2016']['pPF']['1.0e-2Op']['bM']
            lP, = ax.plot(xvals,th.func[f](xvals,*p),'forestgreen',lw=2,label="parallel Firehose (PFHI)")

            p,f = th.params['A2016']['pOF']['1.0e-2Op']['bM']
            lO, = ax.plot(xvals,th.func[f](xvals,*p),'darkviolet',lw=2,label="oblique Firehose (OFHI)")

        if species_tag == 'e':
            p,f = th.params['L2013']['eC']['1.0e-3Oe']['bM']
            lC, = ax.plot(xvals,th.func[f](xvals,*p),'darkorange',lw=2,label="Cyclotron (CI)")

            p,f = th.params['G2006']['eM']['1.0e-3Oe']['bM']
            lM, = ax.plot(xvals,th.func[f](xvals,*p),'red',lw=2,label="Mirror (MI)")

            p,f = th.params['G2003']['ePF']['1.0e-2Op']['bM']
            lP, = ax.plot(xvals,th.func[f](xvals,*p),'forestgreen',lw=2,label="parallel Firehose (PFHI)")

            p,f = th.params['H2014']['eOF']['1.0e-3Oe']['bM']
            lO, = ax.plot(xvals,th.func[f](xvals,*p),'darkviolet',lw=2,label="oblique Firehose (OFHI)")

        lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',lw=2,label='$\\beta_{\parallel}^{-1}$')
        
        ax.set_xlabel('$\\beta_{\parallel,\mathrm{%s}}$'%(species_tag))
        ax.set_ylabel('$p_{\perp,\mathrm{%s}}/p_{\parallel,\mathrm{%s}}$'%(species_tag,species_tag))
        ax.set_xlim(xdata_range[0],xdata_range[1])
        ax.set_ylim(ydata_range[0],ydata_range[1])
        if legend_flag:
            ax.legend()
        title_str = run_label+' - '+species_name
        if t != None:
            title_str += ' - $t = %s$'%(t)
        ax.set_title(title_str)
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.colorbar(im,ax=ax)

        return [lC,lM,lP,lO,lb]
    elif c_pass < n_pass-1:
        return h
    else:
        print('\nc_pass=%d is greater than n_pass-1=%d\n'%(c_pass,n_pass-1))
        return -1

#---> loop over times <---
hp = None
he = None
n_bins = 50*int(round(np.sqrt(ind2-ind1+1)))
xbinp = np.linspace(np.log10(betap_lim[0]),np.log10(betap_lim[1]),n_bins+1) # WIP
ybinp = np.linspace(np.log10(anisp_lim[0]),np.log10(anisp_lim[1]),n_bins+1) # WIP
xbine = np.linspace(np.log10(betae_lim[0]),np.log10(betae_lim[1]),n_bins+1) # WIP
ybine = np.linspace(np.log10(anise_lim[0]),np.log10(anise_lim[1]),n_bins+1) # WIP

plt.close()
fig,ax = plt.subplots(1,2,figsize=(10*2,8))

ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
n_pass = len(ind_range)
c_pass = 0

t1_ = input('\nExternal first time (float or None): ')
t2_ = input('External last time (float or None): ')
print('')
if t1_ == None:
    ind1_ = ind1
else:
    ind1_ = ml.index_of_closest(float(t1_),time)
if t2_ == None:
    ind2_ = ind2
else:
    ind2_ = ml.index_of_closest(float(t2_),time)
print('t(%d)=%f ooooo t(%d)=%f xxxxx t(%d)=%f ooooo t(%d)=%f\n'%(ind1_,time[ind1_],ind1,time[ind1],ind2,time[ind2],ind2_,time[ind2_]))

G = {'p':[],'e':[]}
print " ",
for ind in np.arange(ind1_,ind2_+ind_step,ind_step,dtype=np.int):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and smooth_flag:
        Psi = gf(Psi,[gfsp,gfsp],mode='wrap')
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n,_ = run.get_Ion(ind)
            pparle *= n
            pperpe *= n
            del n
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise[:,:,0] = gf(anise[:,:,0],[gfse,gfse],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    del B
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp[:,:,0] = gf(bparlp[:,:,0],[gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle[:,:,0] = gf(bparle[:,:,0],[gfse,gfse],mode='wrap')
        del pparle
    del B2,pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #mask
    mask = (Psi > np.max(Psi)*th_param).reshape(nx,ny)
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    bp_cut = bparlp[mask]
    ap_cut =  anisp[mask]
    if w_ele:
        be_cut = bparle[mask]
        ae_cut =  anise[mask]
    Glabel = 0
#
    if ind in ind_range:
        hp = windlike_plot_func(ax[0],bp_cut,ap_cut,p_vars,xdata_range=betap_lim,ydata_range=anisp_lim,
                                n_bins=[xbinp,ybinp],h_old=hp,c_pass=c_pass,n_pass=n_pass)
        if type(hp) == np.int:
            if hp == -1:
                print('\nSomething went wrong in windlike_plot_func (protons)\n')
#
        if w_ele:
            he = windlike_plot_func(ax[1],be_cut,ae_cut,e_vars,xdata_range=betae_lim,ydata_range=anise_lim,
                                    n_bins=[xbine,ybine],h_old=he,c_pass=c_pass,n_pass=n_pass)
            if type(he) == np.int:
                if he == -1:
                    print('\nSomething went wrong in windlike_plot_func (electrons)\n')
#
        c_pass += 1
        Glabel = 1
#
    G['p'].append([np.mean(bp_cut),np.mean(ap_cut),Glabel])
    if w_ele:
        G['e'].append([np.mean(be_cut),np.mean(ae_cut),Glabel])
#
    del bp_cut,ap_cut
    if w_ele:
        del be_cut,ae_cut
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

Gp = np.array(G['p'])
ax[0].scatter(Gp[:,0],Gp[:,1],c=Gp[:,2],cmap=my_cmap)

if  w_ele:
    Ge = np.array(G['e'])
    ax[1].scatter(Ge[:,0],Ge[:,1],c=Ge[:,2],cmap=my_cmap)
else:
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].spines['bottom'].set_visible(False)
    ax[1].spines['left'].set_visible(False)
    ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
    ax[1].legend(handles=hp,loc='center',borderaxespad=0)
#
plt.tight_layout()
plt.savefig(opath+'/'+out_dir+'/'+'island_in_wind_multitimes_'+mask_name+'_'+run_name+'_'+
            str(ind1)+'-'+str(ind2)+'_b'+str(n_bins)+'.png')
plt.close()
