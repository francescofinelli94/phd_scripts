import sys
import glob
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec as SubSpec
import scipy.ndimage.interpolation as ndm
from scipy.integrate import simps
#import pywt
#import scaleogram as scg 
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from HVM_loader import *

font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#----------------------------------------------------------------------------------------------------
#HARDCODED!!!

sim_path='/work1/califano/HVM2D/HVM_3072'
meta_subpath = '01'

pvi_path = '/home/sisti/python_routines/VIRTUAL_SATELLITE'

t_dict = {}
t_dict['28'] = 247.
t_dict['60'] = 494.

p = 2.0  # number of steps inside each cell
SSx0 = 0.01   # initial positions of the virtual satellite inside the simulation box
SSy0 = 0.01

regions_path = '/home/sisti/python_routines/regions_verified_reconnection_tTIME.txt'

opath = '/home/finelli/Downloads'

#----------------------------------------------------------------------------------------------------
#Metadata time

run = from_HVM(sim_path)
run.get_meta(meta_subpath)
run_name = sim_path.split('/')[-1]
x = run.meta['x']
y = run.meta['y']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
xl,yl,_ = run.meta['lll']

calc = fibo('calc')
calc.meta = run.meta

out_dir = 'PVI-scalograms_v4' 

#----------------------------------------------------------------------------------------------------
#PRIMA PARTE: ricostruzione traiettoria, le coordinate finali xco e yco sono in unita' [di]

# -> what time is it?
segments = sorted([int(tmp.split('_')[-1]) for tmp in glob.glob(pvi_path+'/new_pvi_*')])
if len(segments) == 0:
    sys.exit("ERROR: no direcory new_pvi_* found in:\n"+pvi_path)

print('\nsegments:')
print(segments)
seg = ml.secure_input('Choose a segment: ',10,True)
#                                    WAIT FOR INPUT
if seg not in segments:
    sys.exit("ERROR: choosen segment not found.")

seg = str(seg)
if seg in t_dict.keys():
    t_ = t_dict[seg]
    print("\nt="+str(t_))
else:
    t_ = ml.secure_input('Choose time (float): ',1.,True)
#                                    WAIT FOR INPUT

ind = ml.index_of_closest(t_,run.meta['time'])

# -> angle?
angles = sorted([int(tmp.split('angle')[-1]) 
    for tmp in glob.glob(pvi_path+('/new_pvi_%s/angle*')%(seg)) 
    if len(tmp.split('angle')[-1].split('_'))==1])
if len(angles) == 0:
    sys.exit("ERROR: no direcory angle* found in:\n"+pvi_path+('/new_pvi_%s')%(seg))

print('\nangles:')
print(angles)
ang = ml.secure_input('Choose an angle: ',10,True)
#                                    WAIT FOR INPUT
if ang not in angles:
    sys.exit("ERROR: choosen angle not found.")

# -> load/construct trajectory
pvi_path = pvi_path + '/new_pvi_%s/angle%d'%(seg,ang)

pi2 = 2.0*np.pi
theta_s = float(ang)*pi2/360.0

files = glob.glob(pvi_path+'/B_1Dcut_pvi_*.dat')
ll = [] #PVI scale
cc = [] #is centered?
for f in files:
    tmp = f.split('pvi_')[-1].split('.')[0]
    ll.append(int(tmp.split('_')[0]))
    cc.append(tmp.split('_')[-1] == 'center')


files = [tmp for _,tmp in sorted(zip(ll,files))]
cc = [tmp for _,tmp in sorted(zip(ll,cc))]
ll = sorted(ll)

lstart = ll[0]
f = open(files[0], 'r')
lines = f.readlines()
f.close()
ns = len(lines)
sco = np.zeros((ns),dtype=np.float64)
xco = np.zeros((ns),dtype=np.float64)
yco = np.zeros((ns),dtype=np.float64)
PVI = np.full((ns,len(ll)),-1.,dtype=np.float64)
num_col = len(lines[0].split())
if num_col == 2:
    DELTAs  = pi2/float(nx)/p
    DELTAsx = DELTAs*np.cos(theta_s)
    DELTAsy = DELTAs*np.sin(theta_s)
    SSx = SSx0
    SSy = SSy0
    SSS = np.sqrt(SSx**2+SSy**2)
    for i in range(ns):
        SSx += DELTAsx
        SSy += DELTAsy
        SSS += DELTAs
        if SSx >= pi2: SSx -= pi2
        if SSy >= pi2: SSy -= pi2
        sco[i]=SSS
        xco[i]=SSx
        yco[i]=SSy
elif num_col == 4:
    for i in range(ns):
        tr = lines[i].split()
        sco[i] = float(tr[0])
        xco[i] = float(tr[1])
        yco[i] = float(tr[2])
    DELTAs  = sco[1] - sco[0]
    DELTAsx = xco[1] - xco[0]
    DELTAsy = yco[1] - yco[0]
else:
    sys.exit("ERROR: number of columns is not 2 nor 4, but "+str(num_col)+".")

sco = sco*xl/pi2
xco = xco*xl/pi2
yco = yco*yl/pi2
DELTAs  = DELTAs*xl/pi2
DELTAsx = DELTAsx*xl/pi2 
DELTAsy = DELTAsy*yl/pi2 
ds = sco[1] - sco[0]

# -> read PVIs
for il,l,c,fname in zip(range(len(ll)),ll,cc,files):
    f = open(fname, 'r')
    lines = f.readlines()
    f.close()
    for i in range(len(lines)):
        PVI[i,il] = float(lines[i].split()[-1])
    if not c:
        PVI[:,il] = np.roll(PVI[:,il],l//2)

# -> PVIs scaled
PVI_sc = np.empty(PVI.shape,dtype=np.float64)
for il in range(len(ll)):
    PVI_sc[:,il] = np.divide(PVI[:,il],np.max(PVI[:,il]))

# -> integrated PVI
intPVI = np.full((ns),-1.,dtype=np.float64)
for i in range(ns):
    if i < ll[-1]//2 or i >= ns-1-ll[-1]//2:
        continue
    intPVI[i] = simps(PVI[i,:],ll)

intPVI_sc = np.divide(intPVI,np.max(intPVI))
del intPVI

#----------------------------------------------------------------------------------------------------
#SECONDA PARTE: interpolazione

# -> calcolare dati
_,B = run.get_EB(ind)

cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
del B
J = np.array([cx,cy,cz])
del cx,cy,cz
Jn = np.sqrt(J[0]**2 + J[1]**2 + J[2]**2)
del J

# -> interpolazione vera e propria
new_coordsx = xco/dx
new_coordsy = yco/dy

Jn_an = ndm.map_coordinates( Jn[:,:,0].T,np.vstack((new_coordsx,new_coordsy)),mode='wrap')

# -> J scaled
Jn_an_sc = np.divide(Jn_an,np.max(Jn_an))
del Jn_an

# -> where is reconnection?
#tmp = regions_path.split('TIME') 
#regions_path = tmp[0] + str(int(t_)) + tmp[1]
#regions = np.loadtxt(regions_path)
#regions = (regions > -1.).astype(np.int)
#regions_an = ndm.map_coordinates(regions[:,:].T,np.vstack((new_coordsx,new_coordsy)),
#                                 mode='wrap',order=1,prefilter=False)
#del regions
#rec_ind = np.where(regions_an[:] > 0.5)[0]

#----------------------------------------------------------------------------------------------------
#PLOT GENERAL

#def rainbow_text(x, y, strings, colors, orientation='horizontal',
#                 ax=None, **kwargs):
#    """
#    Take a list of *strings* and *colors* and place them next to each
#    other, with text strings[i] being shown in colors[i].
#
#    Parameters
#    ----------
#    x, y : float
#        Text position in data coordinates.
#    strings : list of str
#        The strings to draw.
#    colors : list of color
#        The colors to use.
#    orientation : {'horizontal', 'vertical'}
#    ax : Axes, optional
#        The Axes to draw into. If None, the current axes will be used.
#    **kwargs
#        All other keyword arguments are passed to plt.text(), so you can
#        set the font size, family, etc.
#    """
#    if ax is None:
#        ax = plt.gca()
#    t = ax.transData
#    canvas = ax.figure.canvas
#
#    assert orientation in ['horizontal', 'vertical']
#    if orientation == 'vertical':
#        kwargs.update(rotation=90, verticalalignment='bottom')
#
#    for s, c in zip(strings, colors):
#        text = ax.text(x, y, s, color=c, transform=t, **kwargs)
#
#        # Need to draw to update the text position.
#        text.draw(canvas.get_renderer())
#        ex = text.get_window_extent()
#        if orientation == 'horizontal':
#            t = mpl.transforms.offset_copy(
#                text.get_transform(), x=ex.width, units='dots')
#        else:
#            t = mpl.transforms.offset_copy(
#                text.get_transform(), y=ex.height, units='dots')

gs_flg = ml.secure_input('Make plots?\n\tYes -> 0\n\tNo -> 1\n: ',1,True)

#sys.stderr.write("THIS IS A TEST AND gs_flg = %d\n"%(gs_flg))

plot_flag = True
while plot_flag:
#    print('\tfull plot start...')
    sys.stdout.flush()
#
    i0 = 0
    i1 = len(sco) - 1
    #print("\ntrajectory range: %f -> %f"%(sco[0],sco[-1]))
    #sco0 = ml.secure_input('start from: ',1.,True)
    #sco1 = ml.secure_input('arrive at: ',1.,True)
    #i0 = np.argmin(np.abs(sco-sco0))
    #i1 = np.argmin(np.abs(sco-sco1))
#
    ans = ml.secure_input('Thresholds:\n\tmanual -> 0\n\tstat. -> 1\n: ',1,True)
    if ans == 0:
        thJn  = ml.secure_input('|J| threshold (0 -> 1): ',1.,True)
        thPVI = ml.secure_input('PVI threshold (0 -> 1): ',1.,True)
#
        th_sup = ' - J%d P%d'%(int(thJn*100),int(thPVI*100))
        th_file = 'J%dP%d_NOCWT'%(int(thJn*100),int(thPVI*100))
    elif ans == 1:
        sigJn = ml.secure_input('Number of st. dev. for |J| threshold (>0.): ',1.,True)
        J2 = Jn_an_sc**2
        thJn = np.sqrt( np.mean(J2) + sigJn*np.std(J2) )
#        del J2
        sigPVI = ml.secure_input('Number of st. dev. for PVI threshold (>0.): ',1.,True)
        thPVI = np.mean(intPVI_sc) + sigPVI*np.std(intPVI_sc)
#
        with open('file.txt','a') as f:
            f.write('%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'%(t_,float(ang),float(Jn_an_sc.size),sigJn,np.sum(J2),np.sum(J2**2),sigPVI,np.sum(intPVI_sc),np.sum(intPVI_sc**2)))
#
#        th_sup = ' - J%ds P%ds'%(int(sigJn*100),int(sigPVI*100))
#        th_file = 'J%dsP%ds_NOCWT'%(int(sigJn*100),int(sigPVI*100))
    else:
        print('ans value %d not recognized.'%ans)
        sys.exit(-1)
#
#    if gs_flg == 1:
#        th_descriptor = th_file
#        ml.create_path(opath+'/'+out_dir+'/'+th_descriptor)
#        filewp = (opath+'/'+out_dir+'/'+th_descriptor+'/pvi_'+run_name+'_'+
#                    th_descriptor+'_%d_%d_s%sa%d'%(i0,i1,seg,ang)+'.GRIDSEARCH')
#        os.system('touch %s'%(filewp))
#        ans = ml.secure_input('\nDummy question? (Y/n): ','',True)
#        break
##
#    sys.stderr.write("INIZIO IL FULL PLOT\n")
#    sav_flg = True
#    #sav_flg = ml.secure_input('show (0) or save (1,...): ',1,True)
##
#    W1 = 4 #length first box
#    H1 = 4 #height first box
#    W2 = 3 #length second box
#    H2 = 1 #height second box
#
#    w1 = 0.15 #width spacing grids
#
#    l1 = 0.05 #left margin
#    r1 = 0.98 #right margin
#    b1 = 0.08 #bottom margin
#    t1 = 0.95 #top margin
#
#    m1 = 1.6 #multiplier
##
#    plt.close()
#    fig = plt.figure(figsize=(m1*(W1+W2+H2)*(1.+w1*0.5)/(r1-l1),m1*H1/(t1-b1)),
#                     constrained_layout=False)
#    gs = GridSpec(1,2,figure=fig,width_ratios=[W1,W2+H2],
#                  wspace=w1,left=l1,right=r1,bottom=b1,top=t1)
#    gs1 = SubSpec(2,2,gs[1],width_ratios=[W2,H2],height_ratios=[H2,H1-H2],
#                  wspace=0.,hspace=0.)
##
#    # -> 2D: J (heatmap), trajectory (darkgray), rec (red), J/Jmax>thJn (green), 
#    #        (int. PVI)/(int. PVI)max>thPVI (blue)
#    ax0 = fig.add_subplot(gs[0])
#    ax0.contourf(y,x,Jn[:,:,0],63)
#    ax0.scatter(xco[i0:i1],yco[i0:i1],s=1,c='darkgray')
#    ax0.scatter(xco[rec_ind[:]],yco[rec_ind[:]],s=110,c='red')
#    ax0.scatter(xco[Jn_an_sc>thJn],yco[Jn_an_sc>thJn],s=65,c='green')
#    ax0.scatter(xco[intPVI_sc>thPVI],yco[intPVI_sc>thPVI],s=20,c='blue')
#    ax0.set_ylabel('x [$d_i$]')
#    ax0.set_xlabel('y [$d_i$]')
#    ax0.set_title('$t=%s$ - $\\alpha=%d^{\circ}$'%(t_,ang))
#    ax0.set_ylim(x[0],x[-1])
#    ax0.set_xlim(y[0],y[-1])
#    ax0.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=True,labeltop=False,
#                    labelleft=True,labelright=False)
##
#    # -> 1D: PVI scale int. (blue), J (green), rec (red)
#    ax1 = fig.add_subplot(gs1[0,0])
#    for s in sco[rec_ind[:]]:
#        ax1.axvspan(s-ds*0.5,s+ds*0.50,alpha=1.,color='red',lw=3)
#    ax1.plot(sco,Jn_an_sc,color='green')
#    ax1.plot(sco,intPVI_sc,color='blue')
#    ax1.axhline(y=thJn,linestyle='--',color='green')
#    ax1.axhline(y=thPVI,linestyle='--',color='blue')
#    ax1.set_xlim(sco[i0],sco[i1])
#    ax1.set_ylim(0.,1.)
#    ax1.set_title('PVI-scalogram and its marginals')
#    rainbow_text(sco[i0]-0.14*(sco[i1]-sco[i0]),0.01,['$|J|$',',','l','$\int_\ell\mathcal{I}$ (norm.)'],
#                 ['green','black','white','blue'],orientation='vertical',
#                 ax=ax1,size=font)
#    ax1.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=False,labeltop=False,
#                    labelleft=True,labelright=False)
##
#    # -> 2D: PVI scalogram
#    ax2 = fig.add_subplot(gs1[1,0])
#    ax2.contourf(sco[i0:i1],ll,PVI[i0:i1,:].T,63)
#    ax2.set_xlim(sco[i0],sco[i1])
#    ax2.set_ylim(ll[0],ll[-1])
#    ax2.set_xlabel('s [$d_i$]')
#    ax2.set_ylabel('$\ell$ [$ds='+str(round(ds,4))+'\;d_i$]')
#    ax2.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=True,labeltop=False,
#                    labelleft=True,labelright=False)
#    xticks = ax2.xaxis.get_major_ticks()
#    xticks[-1].label1.set_visible(False)
#    xticks[-2].label1.set_visible(False)
#    yticks = ax2.yaxis.get_major_ticks()
#    yticks[-1].label1.set_visible(False)
##
#    # -> 1D: PVI space int. (cyan)
#    ax3 = fig.add_subplot(gs1[1,1])
#    tmp = np.empty((len(ll)),dtype=np.float64)
#    for il in range(len(ll)):
#        tmp[il] = simps(PVI[i0:i1+1,il],sco[i0:i1+1])
#    tmp = np.divide(tmp,np.max(tmp))
#    ax3.plot(tmp,ll,'c')
#    ax3.set_ylim(ll[0],ll[-1])
#    ax3.set_xlabel('$\int_s\mathcal{I}$ (norm.)')
#    ax3.tick_params(bottom=True,top=True,left=True,right=True,
#                direction='in',labelbottom=True,labeltop=False,
#                labelleft=False,labelright=False)
##
#    if sav_flg == 0:
#        fig.show()
#    else:
#        th_descriptor = th_file
#        ml.create_path(opath+'/'+out_dir+'/'+th_descriptor)
#        fig.savefig(opath+'/'+out_dir+'/'+th_descriptor+'/pvi_'+run_name+'_'+
#                th_descriptor+'_%d_%d_s%sa%d'%(i0,i1,seg,ang)+'.png')
##
    ans = ml.secure_input('\nPlot again? (Y/n): ','',True)
    if ans == 'n': plot_flag = False
#    sys.stderr.write("FATTO IL FULL PLOT\n")
#
#if gs_flg == 0:
#    plt.close()
#
#print('\t... full plot end')
sys.stdout.flush()

#----------------------------------------------------------------------------------------------------
##FINDING SITES
#
#lm = ll[-1]//2
#lp = 1 + ll[-1]//2
#
#flg = {}
#mlt = {}
#flg['r'] = regions_an > 0.5
#flg['J'] = Jn_an_sc > thJn
#flg['P'] = intPVI_sc > thPVI
#mlt['r'] = 1
#mlt['J'] = 10
#mlt['P'] = 100
#
#sco_seg = {}
#sco_seg['r'] = np.zeros((ns),dtype=np.int)
#sco_seg['J'] = np.zeros((ns),dtype=np.int)
#sco_seg['P'] = np.zeros((ns),dtype=np.int)
#
#for i in range(ns):
#    for th in ['r','J','P']:#,'C']:
#        if flg[th][i]:
#            ii = i
#            while flg[th][ii] and (ii < ns):
#                flg[th][ii] = False
#                ii += 1
#            ii = (i + (ii - 1))//2
#            sco_seg[th][max(ii-lm,lm):min(ii+lp,ns-lp+1)] = 1
#
#del flg
#
#sco_segs = ( sco_seg['r']*mlt['r'] + sco_seg['J']*mlt['J'] + 
#        sco_seg['P']*mlt['P'] )
#del sco_seg
#segs = []
#ii = -1
#for i in range(ns):
#    if i <= ii:
#        continue
#    if sco_segs[i] != 0:
#        ii = i
#        while (sco_segs[ii] != 0) and (ii < ns):
#            ii += 1
#        ii -= 1
#        maxval = np.max(sco_segs[i:ii+1])
#        jj = 0
#        cnt = 0
#        for j_ in range(ii+1-i):
#            if sco_segs[j_+i] == maxval:
#                cnt += 1
#                jj += j_ + i
#        jj = jj//cnt
#        segs.append([max(jj-lm,lm),min(jj+lp,ns-lp+1),maxval])
#
#del sco_segs
##----------------------------------------------------------------------------------------------------
##PLOT SITES
#dnx = 500 # approx. 50di added, 25 per side
#hdnx = dnx//2
#dnx = hdnx*2 #avoid problem w/ odd numbers
#x_ax = np.empty(nx+dnx,dtype=np.float64)
#x_ax[hdnx:hdnx+nx] = x
#x_ax[0:hdnx] = (np.arange(hdnx,dtype=np.float) - float(hdnx))*dx
#x_ax[hdnx+nx:dnx+nx] = (np.arange(hdnx,dtype=np.float) + float(nx))*dx
#
#dny = 500 # approx. 50di added, 25 per side
#hdny = dny//2
#dny = hdny*2 #avoid problem w/ odd numbers
#y_ax = np.empty(nx+dnx,dtype=np.float64)
#y_ax[hdny:hdny+ny] = y
#y_ax[0:hdny] = (np.arange(hdny,dtype=np.float) - float(hdny))*dy
#y_ax[hdny+ny:dny+ny] = (np.arange(hdny,dtype=np.float) + float(ny))*dy
#
#Jn = np.pad(Jn[...,0],((hdnx,hdnx),(hdny,hdny)),'wrap')
#
#s_cnt = 1
#s_len = len(segs)
#for s in segs:
#    print('\tsite %d over %d start...'%(s_cnt,s_len))
#    sys.stdout.flush()
##
#    i0 = s[0]
#    i1 = s[1] - 1
##
#    th_flg = [(s[2]%(n*10))//n for n in [1,10,100,1000]]
##
#    if gs_flg == 1:
#        th_descriptor = th_file
#        #ml.create_path(opath+'/'+out_dir+'/'+th_descriptor)
#        filewp = (opath+'/'+out_dir+'/'+th_descriptor+'/pvi_scalogram_'+run_name+'_'+
#                  th_descriptor+'tf%d_%d_%d_s%sa%d'%(s[2],i0,i1,seg,ang)+'.GRIDSEARCH')
#        os.system('touch %s'%(filewp))
#        print('\t...site %d over %d end'%(s_cnt,s_len))
#        sys.stdout.flush()
#        s_cnt += 1
#        continue
##
#    sys.stderr.write("INIZIO IL PLOT\n")
#    phi = (1.+5.**0.5)*0.5
#    conj_phi = phi - 1.
#
#    W1 = 2 #length first box
#    H1 = W1*phi #height first box
#    H2 = W1*conj_phi**1 #height second box
#    H3 = H2*conj_phi #height third box
#
#    l1 = 0.11 #left margin
#    r1 = 0.90 #right margin
#    b1 = 0.08 #bottom margin
#    t1 = 0.92 #top margin
#
#    m1 = 1.6 #multiplier
##
#    plt.close()
#    fig = plt.figure(figsize=(m1*(W1+H2+H3)/(r1-l1),m1*(H1+H2+H3)/(t1-b1)),
#                     constrained_layout=False)
#    gs = GridSpec(3,3,figure=fig,width_ratios=[W1,H2,H3],height_ratios=[H3,H2,H1],
#                  wspace=0.,hspace=0.,left=l1,right=r1,bottom=b1,top=t1)
##
#    # -> 1D: scale int. PVI (blue), J (green), rec (red)
#    ax0 = fig.add_subplot(gs[1,0])
#    for s_ in sco[rec_ind[:]]:
#        if (s_ < sco[i0]) or (s_ > sco[i1]):
#            continue
#        ax0.axvspan(s_-ds*0.5,s_+ds*0.50,alpha=1.,color='red',lw=3)
#    ax0.plot(sco[i0:i1+1],Jn_an_sc[i0:i1+1],color='green')
#    ax0.plot(sco[i0:i1+1],intPVI_sc[i0:i1+1],color='blue')
#    ax0.axhline(y=thJn,linestyle='--',color='green')
#    ax0.axhline(y=thPVI,linestyle='--',color='blue')
#    ax0.set_xlim(sco[i0],sco[i1])
#    ax0.set_ylim(0.,1.)
#    ax0.set_title('PVI-scalogram')
#    rainbow_text(sco[i0]-0.235*(sco[i1]-sco[i0]),0.09,['$|J|$',',','l','$\int_\ell\mathcal{I}$ (norm.)'],
#                 ['green','black','white','blue'],orientation='vertical',
#                 ax=ax0,size=font)
#    ax0.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=False,labeltop=False,
#                    labelleft=True,labelright=False)
##
#    # -> 2D: J (heat), trajectory (darkgray), site (red square)
#    ax1 = fig.add_subplot(gs[0:2,1:3])
#    ic = (i0 + i1)//2
#    xc = xco[ic]
#    yc = yco[ic]
#    mrg = 17. #max 25.
#    x0 = xc - mrg
#    x1 = xc + mrg
#    y0 = yc - mrg
#    y1 = yc + mrg
#    ix0 = np.argmin(np.abs(x_ax-y0))
#    ix1 = np.argmin(np.abs(x_ax-y1))
#    iy0 = np.argmin(np.abs(y_ax-x0))
#    iy1 = np.argmin(np.abs(y_ax-x1))
#    ax1.contourf(y_ax[iy0:iy1+1],x_ax[ix0:ix1+1],Jn[ix0:ix1+1,iy0:iy1+1],63)
#    ax1.scatter([xc+DELTAsx*(i_-(i1+1-i0)//2) for i_ in range(i1+1-i0)],
#                [yc+DELTAsy*(i_-(i1+1-i0)//2) for i_ in range(i1+1-i0)],s=1,c='darkgray')
#    ax1.scatter([xc],[yc],s=5,c='red')
#    ax1.xaxis.set_label_position("top")
#    ax1.set_xlabel('y [$d_i$]')
#    ax1.yaxis.set_label_position("right")
#    ax1.set_ylabel('x [$d_i$]')
#    ax1.set_ylim(x_ax[ix0],x_ax[ix1])
#    ax1.set_xlim(y_ax[iy0],y_ax[iy1])
#    ax1.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=False,labeltop=True,
#                    labelleft=False,labelright=True)
##
#    # -> 2D: PVI scalogram
#    ax2 = fig.add_subplot(gs[2,0])
#    ax2.contourf(sco[i0:i1+1],ll,PVI[i0:i1+1,:].T,63)
#    ax2.set_xlabel('s [$d_i$]')
#    ax2.set_ylabel('scales [$ds='+str(round(ds,4))+'\;d_i$]')
#    ax2.set_xlim(sco[i0],sco[i1])
#    ax2.set_ylim(ll[0],ll[-1])
#    ax2.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=True,labeltop=False,
#                    labelleft=True,labelright=False)
#    xticks = ax2.xaxis.get_major_ticks()
#    xticks[-1].label1.set_visible(False)
#    yticks = ax2.yaxis.get_major_ticks()
#    yticks[-1].label1.set_visible(False)
##
#    # -> 1D: space int. PVI
#    ax3 = fig.add_subplot(gs[2,1])
#    tmp = np.empty((len(ll)),dtype=np.float64)
#    for il in range(len(ll)):
#        tmp[il] = simps(PVI[i0:i1+1,il],sco[i0:i1+1])
#    tmp = np.divide(tmp,np.max(tmp))
#    ax3.plot(tmp,ll,'c')
#    ax3.set_ylim(ll[0],ll[-1])
#    ax3.set_xlabel('$\int_s\mathcal{I}$ (norm.)')
#    ax3.tick_params(bottom=True,top=True,left=True,right=True,
#                    direction='in',labelbottom=True,labeltop=False,
#                    labelleft=False,labelright=False)
##
#    ax4 = fig.add_subplot(gs[0,0])
#    ax4.axis('off')
#    ax4.text(0.5,0.5,'$t=%s$ - $\\alpha=%d^{\circ}$'%(t_,ang),ha='center',va='center')
##
#    th_descriptor = th_file
#    #ml.create_path(opath+'/'+out_dir+'/'+th_descriptor)
#    fig.savefig(opath+'/'+out_dir+'/'+th_descriptor+'/pvi_scalogram_'+run_name+'_'+
#                th_descriptor+'tf%d_%d_%d_s%sa%d'%(s[2],i0,i1,seg,ang)+'.png')
#    #fig.show()
#    #tmp = input('whatever ')
#    #break
##
#    print('\t...site %d over %d end'%(s_cnt,s_len))
#    sys.stdout.flush()
#    s_cnt += 1
#    sys.stderr.write("FIMISCO IL PLOT\n")
#
#if gs_flg == 0:
#    plt.close()
