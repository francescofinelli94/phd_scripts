import h5py as h5
import numpy as np
import numba
from scipy.optimize import curve_fit
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import matplotlib.pyplot as plt
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
from iPIC_loader import *

ipath  = '/work1/fpucci/DoubleHarris/run3/data0'
fname = 'DH-Partcl_200000.h5'
s = 0
nbins = 1000
xmin = 45.#500./1024.*np.pi*2.*12.
xmax = 55.#850./1024.*np.pi*2.*12.
ymin = 1.#50./512.*np.pi*2.*6.
ymax = 11.#120./512.*np.pi*2.*6.

run = from_iPIC(ipath)
run.get_meta()
xl,yl,_ = run.meta['lll']
nx,ny,_ = run.meta['nnn']
dx,dy,_ = run.meta['ddd']
over_dx = 1./dx
over_dy = 1./dy

t_ = float(fname.split('_')[-1].split('.')[0])*run.meta['dt']
_,B = run.get_EB(t_)
Bn = np.sqrt(B[0]**2+B[1]**2+B[2]**2)
b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
for i in range(3):
    b[i] = np.divide(B[i],Bn)

b = b.reshape(3,nx,ny)

f = h5.File(ipath+'/'+fname,'r')
f = f['Step#0']
ns = len(f.keys())//7
nn = np.empty(ns,dtype=np.int)
for i in range(ns):
    nn[i] = f['q_'+str(i)].shape[0]

s_str = str(s)
qom_s = run.meta['sQOM'][0]

#--------------------------------------------

@numba.njit('f8[:](f8[:],f8[:,:],f8[:,:],f8[:],i4,f8,f8[:],f8[:],f8,f8[:,:,:])',parallel=True)
def fill_hist_vparl(vparl_,xy,uvw,q,nb,max_,dxy,rxy,qom,b_):
    N = q.shape[0]
#
    db = (2.*max_)/float(nb)
    over_db = 1./db
#
    over_dx = 1./dxy[0]
    over_dy = 1./dxy[1]
    over_qom = 1./qom
#
    for ip in range(N):
        x_ = xy[0,ip]
        y_ = xy[1,ip]
        if ((x_ < rxy[0]) or (x_ > rxy[1]) or (y_ < rxy[2]) or (y_ > rxy[3])):
            continue
        v_dot_b = np.dot(uvw[:,ip],b_[:,int(x_*over_dx),int(y_*over_dy)])
        vparl_[int((v_dot_b+max_)*over_db)] += q[ip]*over_qom
        if not ip//100000:
            print(float(ip)/float(N)*100.)
#
    return vparl_

@numba.jit('f8(f8[:])',parallel=True)
def my_max(vec):
    return np.max(np.abs(vec))

umax = my_max(f['u_'+s_str][:])
vmax = my_max(f['v_'+s_str][:])
wmax = my_max(f['w_'+s_str][:])
vel_max = np.sqrt(umax**2+vmax**2+wmax**2)
dbins = (2.*vel_max)/float(nbins)
bins = np.linspace(-vel_max+dbins*0.5,vel_max+dbins*0.5,nbins+1)
vparl = np.zeros(bins.shape,dtype=np.float64)

chunk_size = 50000000
chunks = [chunk_size for i in range(nn[0]//chunk_size)]
if nn[0]%chunk_size != 0:
    chunks.append(nn[0]%chunk_size)

start = 0
for chunk in chunks:
    stop = start + chunk
    vparl = fill_hist_vparl( vparl,
            np.array([f['x_'+s_str][start:stop],f['y_'+s_str][start:stop]]),
            np.array([f['u_'+s_str][start:stop],f['v_'+s_str][start:stop],f['w_'+s_str][start:stop]]),
            f['q_'+s_str][start:stop],
            nbins,vel_max,np.array([dx,dy]),np.array([xmin,xmax,ymin,ymax]),qom_s,b )
    start = stop
    print('chunk done')

print('chunks done!')

def to_fit(x,A=1.,m=0.,s=1.):
    return A*np.exp(-0.5*((x-m)/s)**2)

popt,pcov = curve_fit(lambda x,m,s: to_fit(x,A=np.max(vparl),m=m,s=s),bins,vparl,p0=[0.,0.05])
plt.plot(bins,vparl);plt.plot(bins,to_fit(bins,np.max(vparl),*popt));plt.show()

#----------------------------------------------

@numba.jit('f8(f8[:])',parallel=True)
def my_max(vec):
    return np.max(np.abs(vec))

umax = my_max(f['u_'+s_str][:])
vmax = my_max(f['v_'+s_str][:])
wmax = my_max(f['w_'+s_str][:])
db_u  = (2.*umax)/float(nbins)
db_v  = (2.*vmax)/float(nbins)
db_w  = (2.*wmax)/float(nbins)
b_u = np.linspace(-umax+db_u*0.5,umax+db_u*0.5,nbins+1)
b_v = np.linspace(-vmax+db_v*0.5,vmax+db_v*0.5,nbins+1)
b_w = np.linspace(-wmax+db_w*0.5,wmax+db_w*0.5,nbins+1)
u = np.zeros(b_u.shape,dtype=np.float64)
v = np.zeros(b_v.shape,dtype=np.float64)
w = np.zeros(b_w.shape,dtype=np.float64)

@numba.njit('f8[:,:](f8[:],f8[:],f8[:],f8[:,:],f8[:,:],f8[:],i4,f8[:],f8[:],f8)')
def fill_hist_uvw(u_,v_,w_,xy,uvw,q,nb,max_,rxy,qom):
    N = q.shape[0]
#
    dbu = (2.*max_[0])/float(nb)
    over_dbu = 1./dbu
    dbv = (2.*max_[1])/float(nb)
    over_dbv = 1./dbv
    dbw = (2.*max_[2])/float(nb)
    over_dbw = 1./dbw
#
    over_qom = 1./qom
#
    for ip in range(N):
        x_ = xy[0,ip]
        y_ = xy[1,ip]
        if ((x_ < rxy[0]) or (x_ > rxy[1]) or (y_ < rxy[2]) or (y_ > rxy[3])):
            continue
        u_[int((uvw[0,ip]+max_[0])*over_dbu)] += q[ip]*over_qom
        v_[int((uvw[1,ip]+max_[1])*over_dbv)] += q[ip]*over_qom
        w_[int((uvw[2,ip]+max_[2])*over_dbw)] += q[ip]*over_qom
        if not ip//100000:
            print(float(ip)/float(N)*100.)
#
    out = np.empty((3,len(u_)),dtype=np.float64)
    out[0] = u_
    out[1] = v_
    out[2] = w_
    return out

chunk_size = 50000000
chunks = [chunk_size for i in range(nn[0]//chunk_size)]
if nn[0]%chunk_size != 0:
    chunks.append(nn[0]%chunk_size)

start = 0
for chunk in chunks:
    stop = start + chunk
    u,v,w = fill_hist_uvw( u,v,w,
            np.array([f['x_'+s_str][start:stop],f['y_'+s_str][start:stop]]),
            np.array([f['u_'+s_str][start:stop],f['v_'+s_str][start:stop],f['w_'+s_str][start:stop]]),
            f['q_'+s_str][start:stop],
            nbins,np.array([umax,vmax,wmax]),np.array([xmin,xmax,ymin,ymax]),qom_s )
    start = stop
    print('chunk done')

print('chunks done!')

m_u = np.average(b_u,weights=u)
s_u = np.sqrt(np.average((b_u-m_u)**2,weights=u))
norm_u =  np.sum(u)*db_u/np.sqrt(2.*np.pi*s_u**2)
plt.plot(b_u,u);plt.plot(b_u,to_fit(b_u,norm_u,m_u,s_u));plt.show()

m_v = np.average(b_v,weights=v)
s_v = np.sqrt(np.average((b_v-m_v)**2,weights=v))
norm_v =  np.sum(v)*db_v/np.sqrt(2.*np.pi*s_v**2)
plt.plot(b_v,v);plt.plot(b_v,to_fit(b_v,norm_v,m_v,s_v));plt.show()

m_w = np.average(b_w,weights=w)
s_w = np.sqrt(np.average((b_w-m_w)**2,weights=w))
norm_w =  np.sum(w)*db_w/np.sqrt(2.*np.pi*s_w**2)
plt.plot(b_w,w);plt.plot(b_w,to_fit(b_w,norm_w,m_w,s_w));plt.show()

#def to_fit(x,A=1.,m=0.,s=1.):
#    return A*np.exp(-0.5*((x-m)/s)**2)
#
#popt,pcov = curve_fit(lambda x,m,s: to_fit(x,A=np.max(u),m=m,s=s),b_u,u,p0=[0.,0.05])
#plt.plot(b_u,u);plt.plot(b_u,to_fit(b_u,np.max(u),*popt));plt.show()
#
#popt,pcov = curve_fit(lambda x,m,s: to_fit(x,A=np.max(v),m=m,s=s),b_v,v,p0=[0.,0.05])
#plt.plot(b_v,v);plt.plot(b_v,to_fit(b_v,np.max(v),*popt));plt.show()
#
#popt,pcov = curve_fit(lambda x,m,s: to_fit(x,A=np.max(w),m=m,s=s),b_w,w,p0=[0.,0.05])
#plt.plot(b_w,w);plt.plot(b_w,to_fit(b_w,np.max(w),*popt));plt.show()

#----------------------------------------------

@numba.jit('f8(f8[:])',parallel=True)
def my_max(vec):
    return np.max(np.abs(vec))

umax = my_max(f['u_'+s_str][:])
vmax = my_max(f['v_'+s_str][:])
wmax = my_max(f['w_'+s_str][:])
db_u  = (2.*umax)/float(nbins)
db_v  = (2.*vmax)/float(nbins)
db_w  = (2.*wmax)/float(nbins)
b_u = np.linspace(-umax+db_u*0.5,umax+db_u*0.5,nbins+1)
b_v = np.linspace(-vmax+db_v*0.5,vmax+db_v*0.5,nbins+1)
b_w = np.linspace(-wmax+db_w*0.5,wmax+db_w*0.5,nbins+1)
u = np.zeros(b_u.shape,dtype=np.float64)
v = np.zeros(b_v.shape,dtype=np.float64)
w = np.zeros(b_w.shape,dtype=np.float64)

nmax = np.sqrt(umax**2+vmax**2+wmax**2)
db_parl = (2.*nmax)/float(nbins)
b_parl = np.linspace(-nmax+db_parl*0.5,nmax+db_parl*0.5,nbins+1)
parl =  np.zeros(b_parl.shape,dtype=np.float64)
db_perp = (nmax)/float(nbins)
b_perp = np.linspace(db_perp*0.5,nmax+db_perp*0.5,nbins+1)
perp =  np.zeros(b_perp.shape,dtype=np.float64)

@numba.njit('f8[:,:](f8[:],f8[:],f8[:],f8[:],f8[:],f8[:,:,:],f8[:,:],f8[:,:],f8[:],f8[:],i4,f8[:],f8[:],f8)')
def fill_hist_all(u_,v_,w_,parl_,perp_,b_,xy,uvw,q,dxy,nb,max_,rxy,qom):
    N = q.shape[0]
#
    dbu = (2.*max_[0])/float(nb)
    over_dbu = 1./dbu
    dbv = (2.*max_[1])/float(nb)
    over_dbv = 1./dbv
    dbw = (2.*max_[2])/float(nb)
    over_dbw = 1./dbw
    dbparl = (2.*max_[3])/float(nb)
    over_dbparl = 1./dbparl
    dbperp = (max_[3])/float(nb)
    over_dbperp = 1./dbperp
#
    over_qom = 1./qom
    over_dx = 1./dxy[0]
    over_dy = 1./dxy[1]
#
    for ip in range(N):
        x_ = xy[0,ip]
        y_ = xy[1,ip]
        if ((x_ < rxy[0]) or (x_ > rxy[1]) or (y_ < rxy[2]) or (y_ > rxy[3])):
            continue
        utmp,vtmp,wtmp = uvw[0,ip],uvw[1,ip],uvw[2,ip]
        parltmp = ( utmp*b_[0,int(x_*over_dx),int(y_*over_dy)] +
                    vtmp*b_[1,int(x_*over_dx),int(y_*over_dy)] +
                    wtmp*b_[2,int(x_*over_dx),int(y_*over_dy)] )
        perptmp = np.sqrt(utmp**2+vtmp**2+wtmp**2 - parltmp**2)
        u_[int((utmp+max_[0])*over_dbu)] += q[ip]*over_qom
        v_[int((vtmp+max_[1])*over_dbv)] += q[ip]*over_qom
        w_[int((wtmp+max_[2])*over_dbw)] += q[ip]*over_qom
        parl_[int((parltmp+max_[3])*over_dbparl)] += q[ip]*over_qom
        perp_[int(perptmp*over_dbperp)] += q[ip]*over_qom
        if not ip//100000:
            print(float(ip)/float(N)*100.)
#
    out = np.empty((5,len(u_)),dtype=np.float64)
    out[0] = u_
    out[1] = v_
    out[2] = w_
    out[3] = parl_
    out[4] = perp_
    return out

chunk_size = 50000000
chunks = [chunk_size for i in range(nn[0]//chunk_size)]
if nn[0]%chunk_size != 0:
    chunks.append(nn[0]%chunk_size)

start = 0
for chunk in chunks:
    stop = start + chunk
    u,v,w,parl,perp = fill_hist_all( u,v,w,parl,perp,b,
            np.array([f['x_'+s_str][start:stop],f['y_'+s_str][start:stop]]),
            np.array([f['u_'+s_str][start:stop],f['v_'+s_str][start:stop],f['w_'+s_str][start:stop]]),
            f['q_'+s_str][start:stop],np.array([dx,dy]),
            nbins,np.array([umax,vmax,wmax,nmax]),np.array([xmin,xmax,ymin,ymax]),qom_s )
    start = stop
    print('chunk done')

print('chunks done!')

m_u = np.average(b_u,weights=u)
s_u = np.sqrt(np.average((b_u-m_u)**2,weights=u))
norm_u =  np.sum(u)*db_u/np.sqrt(2.*np.pi*s_u**2)
plt.plot(b_u,u);plt.plot(b_u,to_fit(b_u,norm_u,m_u,s_u));plt.show()

m_v = np.average(b_v,weights=v)
s_v = np.sqrt(np.average((b_v-m_v)**2,weights=v))
norm_v =  np.sum(v)*db_v/np.sqrt(2.*np.pi*s_v**2)
plt.plot(b_v,v);plt.plot(b_v,to_fit(b_v,norm_v,m_v,s_v));plt.show()

m_w = np.average(b_w,weights=w)
s_w = np.sqrt(np.average((b_w-m_w)**2,weights=w))
norm_w =  np.sum(w)*db_w/np.sqrt(2.*np.pi*s_w**2)
plt.plot(b_w,w);plt.plot(b_w,to_fit(b_w,norm_w,m_w,s_w));plt.show()

m_parl = np.average(b_parl,weights=parl)
s_parl = np.sqrt(np.average((b_parl-m_parl)**2,weights=parl))
norm_parl =  np.sum(parl)*db_parl/np.sqrt(2.*np.pi*s_parl**2)
plt.plot(b_parl,parl);plt.plot(b_parl,to_fit(b_parl,norm_parl,m_parl,s_parl));plt.show()

m_perp = b_perp[np.argmax(perp)]
norm_perp =  np.sum(perp)*db_perp
MB_perp = b_perp/(m_perp**2)*np.exp(-0.5*(b_perp/m_perp)**2)
plt.plot(b_perp,perp);plt.plot(b_perp,norm_perp*MB_perp);plt.show()

#---------
f.close()
