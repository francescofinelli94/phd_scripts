"""
Functions to perform shifts with/without wrapping
and to find connected structur in scalar fields by flooding

20/1/2020 - F. Finelli
"""

import numpy as np
from scipy.ndimage import measurements as mlabel

#FUNCTIONS

#---------------------------------------------------------------------------------------

def my_shift(data,sh,ax,wr=False,pd=0):
    """
    It shifts an n-dimensional array along a given direction and 
    of a given amount of spaces. During the shift, it can wrap around 
    or pad the array with a given constant.

    INPUTS:
    data -> n-dimensional array to be shifted
    sh -> integer, shifting amount (with sign)
    ax -> integer between 0 and n-1, direction along which shifting will happen
    wr -> boolean, if True will wrap the array, else will pad with a constant
    pd -> scalar, constant to pad with (if wr == False)

    OUTPUTS:
    n-dimensional array with data.shape as shape, but shifted sh times along
    the ax axis
    """
    if sh == 0:
        return data
    dim = len(data.shape)
    t = []
    for n in range(dim):
        if n == ax:
            if sh > 0:
                t.append((sh,0))
            else:
                t.append((0,-sh))
        else:
            t.append((0,0))
    t = tuple(t)
    if wr:
        tmp = np.pad(data,t,mode='wrap')
    else:
        tmp = np.pad(data,t,mode='constant',constant_values=pd)
    if sh > 0:
        return np.rollaxis(np.rollaxis(tmp,ax,0)[:-sh],0,ax+1)
    else:
        return np.rollaxis(np.rollaxis(tmp,ax,0)[-sh:],0,ax+1)

#---------------------------------------------------------------------------------------

def my_multi_shift(data,sh_vec,ax_vec,wr_vec,pd=0):
    """
    It shifts an n-dimensional array along a given set of directios
    and of a given amount of spaces (direction-wise specified). 
    During the shift, it can wrap around or pad the array with a given constant.

    INPUTS:
    data -> n-dimensional array to be shifted
    sh_vec -> list of integers, shifting amount (with sign) along various directions
    ax_vec -> list of integers between 0 and n-1, directions along which shifting will happen
    wr_vec -> list of boolean, if True for a given direction, will wrap the array along said 
          direction, else will pad with a constant
    pd -> scalar, constant to pad with (if wr == False)

    OUTPUTS:
    n-dimensional array with data.shape as shape, but shifted sh_vec[i] times along
    the ax_vec[i] axis
    """
    len_sh_vec = len(sh_vec)
    if len_sh_vec != len(ax_vec):
        print('ERROR: sh_vec and ax_vec have different shape.')
        return None
    if len_sh_vec != len(wr_vec):
        print('ERROR: sh_vec and wr_vec have different shape.')
        return None
    if np.sum(np.abs(sh_vec)) == 0:
        return data
    for i in range(len_sh_vec):
        if sh_vec[i] == 0:
            continue
        data = my_shift(data,sh_vec[i],ax_vec[i],wr=wr_vec[i],pd=pd)
    return data

#---------------------------------------------------------------------------------------

def my_flood(field,th0,periodicity=[],structure=[]):
    """
    Will find the basins one obtains if tries to flood the field up to a given threshold

    INPUTS:
    field -> n-dimensional array to be flooded to find connected structure
    th0 -> scalar, threshold value (i.e., water level)
    periodicity -> list of n booleans, True means periodicity on that direction
    structure -> 3^n array of integers/booleans, specifying if a connection exist between
                 a point and a neighbour. Must be symmetric respect to all diagonals.

    OUTPUTS:
    reg -> n-dimensional array of integers, 0 means no structure, each structure is labeled
           counting from 1
    nreg -> number of regions
    """
    if type(field) != type(np.empty(1)):
        field = np.array(field)
    ndim = len(field.shape)
    if periodicity == []:
        periodicity = np.full(ndim,False)
    else:
        periodicity = np.array(periodicity).astype(bool)
    if structure == []:
        structure = np.ones(tuple(np.full(ndim,3)),dtype=np.int)
    else:
        structure = np.array(structure).astype(int)
    if len(structure.shape) != ndim:
        print('ERROR: structure has wrong shape.')
        return None
    if len(periodicity) != ndim:
        print('ERROR: periodicity has wrong length.')
        return None
    aaxx = np.arange(ndim)
#
    mask = field < th0
    reg,nreg = mlabel.label(mask,structure=structure)
    structure = structure.astype(bool)
#
    for n in range(ndim):
        if periodicity[n]:
            tmp = np.rollaxis(reg,n,0)
            b0 = tmp[0]
            b1 = tmp[-1]
            del tmp
            struct_slice = np.rollaxis(structure,n,0)[0]
            period_slice = periodicity[aaxx!=n]
#
            b_sh = list(b1.shape)
            b0l = b0.astype(bool)
            b1l = b1.astype(bool)
            b2l = np.zeros(b1l.shape,dtype=bool)
            b1_new = np.zeros(b1l.shape,dtype=np.int)
#
            cs = np.rollaxis(np.indices(struct_slice.shape),0,ndim).reshape(3**(ndim-1),ndim-1)
            for c in cs:
                b2l += struct_slice[tuple(c)]*my_multi_shift(b1l,1-c,aaxx[:-1],period_slice,pd=False)
                b1_tmp = int(struct_slice[tuple(c)])*my_multi_shift(b1,1-c,aaxx[:-1],period_slice,pd=0)
                mask = b1_tmp > b1_new
                b1_new = b1_new*np.logical_not(mask).astype(int) + b1_tmp*mask.astype(int)
            b1l = np.logical_and(b0l,b2l)
            b1 = b1_new
            del b1_tmp,b1_new
#
            cc = np.rollaxis(np.indices(b1l.shape),0,ndim).reshape(np.prod(b1l.shape),ndim-1)
            for c in cc:
                tc = tuple(c)
                if not b1l[tc]:
                    continue
                v0 = b0[tc]
                v1 = b1[tc]
                if v0 == v1:
                    continue
                mask = np.logical_or(reg==v0,reg==v1)
                reg = reg*np.logical_not(mask).astype(int) + mask.astype(int)*min(v0,v1)
                b0[b0==max(v0,v1)] = min(v0,v1)
                b1[b1==max(v0,v1)] = min(v0,v1)
                nreg -= 1
#
    vals = list(set(reg.reshape(np.prod(reg.shape))))[1:]
    for n in range(nreg):
        if n == 0:
            continue
        mask = reg==vals[n]
        reg = reg*np.logical_not(mask).astype(int) + mask.astype(int)*(n+1)
    return reg,nreg

#---------------------------------------------------------------------------------------

"""
#TEST
import matplotlib.pyplot as plt
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *
import work_lib as wl

ipath = '/work1/califano/HVM2D/LF_2d_DH'
run = from_HVM(ipath)
run.get_meta()

_,B = run.get_EB(300.)
Psi = wl.psi_2d(B[:-1,...,0],run.meta)

field = -Psi
periodicity = [True,True]
th0 = np.mean(-Psi)
structure = [[0,1,0],
             [1,1,1],
             [0,1,0]]

reg,nreg=my_flood(np.roll(field,(100,-70),axis=(0,1)),np.min(field)*0.9,periodicity=periodicity,structure=list(structure))

plt.imshow(reg.T);plt.show()
"""
