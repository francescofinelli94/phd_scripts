#-----------------------------------------------------
#plot flags
#----------
plt_show_flg = False
plt_save_flg = True

#-----------------------------------------------------
#importing stuff
#---------------
#system utilities
import sys
import gc

#computation
import numpy as np
from numba import njit
#from scipy.ndimage import to_fitsian_filter as gf
from scipy.optimize import curve_fit

#plotting
import matplotlib as mpl
if not plt_show_flg:
    mpl.use('Agg')
import matplotlib.pyplot as plt

#personal - I/O + utils
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#personal - ABC hist + thresholds
sys.path.append('/home/finelli/Documents/templates')
from histABC_wrapped import hist2d_wrap as h2dw
import thresholds as th

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWIP (something about increments/fft of field and anis-beta space)\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
opath = run.meta['opath']
code_name = run.meta['code_name']

w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
xl,yl,zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

if code_name == 'iPIC':
    run_label = 'iPIC'
elif code_name == 'HVM':
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

out_dir = 'WIP6_3'
ml.create_path(opath+'/'+out_dir)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

ans = False
if ans:
    ans = ml.secure_input('Set plot boundaries (True/False): ',True,True)
    if ans:
        betap_lim = input('betap limits ([x0,x1]): ')
        anisp_lim = input('anisp limits ([y0,y1]): ')
        betae_lim = input('betae limits ([x0,x1]): ')
        anise_lim = input('anise limits ([y0,y1]): ')
    else:
        betap_lim = None
        anisp_lim = None
        betae_lim = None
        anise_lim = None
else:
    betap_lim = [.3,4000.]
    anisp_lim = [.4,2.]
    betae_lim = [.06,800.]
    anise_lim = [.2,3.]

#-------------------------------------------------------
#hardcoded inputs
#----------------
#cuts
L1 = 0.85
L2 = 1.7
y1 = run.meta['yl']/(2.0*(1.0+L2/L1))
y2 = run.meta['yl'] - y1*L2/L1
cut1 = int((y2-5.)/run.meta['dy'])
cut2 = int((y2+5.)/run.meta['dy'])

#iPIC smoothing
smooth_flag = True
gfsp = 2
gfse = 2

#plot type
mode = 'ABCmoments'
logscale_flg = False

#B0
B0_as_time_avg = True

#mask
msk_flg = False # True -> 'cutout'; False -> 'CS1'
if msk_flg:
    mask_name = 'cutout'
    mask = np.ones((nx,ny,nz),dtype=bool)
    for ix in range(nx):
        for iy in range(cut1,cut2+1):
            for iz in range(nz):
                mask[ix,iy,iz] = False
else:
    mask_name = 'CS1'
    mask = np.zeros((nx,ny,nz),dtype=bool)
    for ix in range(nx):
        for iy in range(int(12.5/run.meta['ddd'][0])):
            for iz in range(nz):
                mask[ix,iy,iz] = True

#-------------------------------------------------------
#Functions
#---------
#def v1v2_ang(v1,v2):
#    return np.arctan2( np.linalg.norm(np.cross(v1,v2,axis=0),axis=0),
#                       np.sum(np.multiply(v1,v2),axis=0) )

def to_fit(x,a,m,s):
    return a*np.exp(-(x-m)**2/(2.*s**2))

#@njit('float64(float64[:])')
def gamma2(x):
    """
    excess of kurtosis: < ( (x - mean) / sigma )^4 > - 3
    where: mean = < x > ; sigma = sqrt( < (x - mean)^2 > )
    """
    if len(x) == 0:
        return np.nan
    return (( np.divide((x-x.mean()),x.std()) )**4).mean()-3.

@njit('float64[:,:](float64[:,:,:])')
def gamma2_2dmap(acc_map):
    """
    excess of kurtosis: < ( (x - mean) / sigma )^4 > - 3
    where: mean = < x > ; sigma = sqrt( < (x - mean)^2 > )
    for a 2d map w/ accumulator up to x^4
    """
    acc_map = np.divide(acc_map,acc_map[0])
    out_map = ( acc_map[4] - 4.*acc_map[3]*acc_map[1] + 
                6.*acc_map[2]*acc_map[1]**2 - 3.*acc_map[1]**4 )
    out_map = np.divide(out_map,(acc_map[2] - acc_map[1]**2)**2)
    out_map = out_map - 3.
    out_map = np.multiply(out_map,acc_map[0])
    return out_map

#-------------------------------------------------------
#Filtering functions
#-------------------
@njit('float64(float64,float64,float64)')
def blackman(x,xL,xH):
    if xL < x < xH:
        t = (x - xL)/(xH - xL)
        return 0.42 - .5*np.cos(2.*np.pi*t) + .08*np.cos(4.*np.pi*t)
    else:
        return 0.

@njit('float64[:,:,:](float64[:,:,:],float64[:],float64[:],float64[:],float64,float64)')
def blackman_filter(f,kx,ky,kz,kL,kH):
    nmx = len(kx)
    nmy = len(ky)
    nmz = len(kz)
    ff = np.zeros((nmx,nmy,nmz),dtype=np.float64)#complex128)#
    for i in range(nmx):
        for j in range(nmy):
            for k in range(nmz):
                ff[i,j,k] = f[i,j,k]*blackman(np.sqrt(kx[i]**2+ky[j]**2+kz[k]**2),kL,kH)
    return ff

#-------------------------------------------------------
#plot functions
#-------------
def fluct_plot_func_masked( ax, xdata, ydata, mask, field, field_name, species_vars,
                            xdata_range=None, ydata_range=None, t=None, n_bins=[100,100],
                            legend_flag=False, run_label=run_label,
                            mode=mode,logscale_flg=logscale_flg,vmin=None,vmax=None,
                            h_old=None, c_pass=0, n_pass=1 ):
    """
    INPUTS:
    ax           -> pyplot.axis - target axis
    xdata        -> numpy.ndarray - first variable
    ydata        -> numpy.ndarray - second variable
    mask         -> numpy.ndarray(nx,ny) - island mask
    field        -> numpy.ndarray(nx,ny) - field
    field_name   -> string - name of the field
    species_vars -> dict {species_name:'Abc', species_tag:'a', b0_mult=3.14} -
                    species-realted variables
    xdata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the first variable
    ydata_range  -> [optional] list or array [2.73, 3.14] -
                    plot range for the second variable
    t            -> [optional] float - if set, the time to be shown in the title
    n_bins       -> [optional] [int,int] or [array,array] - default=[100,100] - 
                    number of bins per variable
    legend_flag  -> [optional] bool - default=False -
                    if True, legend is shown
    run_label    -> [optional, set by default] string - run label
    mode         -> [optional, set by default] string - 'ABC' or 'ABCmax' or 'standard'
    logscale_flg -> [optional, set by default] boolean - if colormap is logscaled
    vmin         -> float or None - minimum value in the colorbar
    vmax         -> float or none - maximum value in the colorbar
    h_old        -> [optional] None or np.array - default=None - 
                    where histograms are cumulated
    c_pass       -> [optional] int - default=0 - passage index
    n_pass       -> [optional] int - default=1 - number of cumulating passages

    OUTPUTS:
    if c_pass < n_pass-1:
        h            -> np.array - cumulated 2D histogram
    if c_pass == n_pass-1:
        handles      -> [lines.Line2D, ...] - list of handles for the legend
    if ERROR:
        -1           -> int - ERROR exit
    """

    species_name = species_vars['species_name']
    species_tag = species_vars['species_tag']

    if xdata_range == None:
        xdata_range = [np.min(xdata),np.max(xdata)]
    if ydata_range == None:
        ydata_range = [np.min(ydata),np.max(ydata)]

    xdata_masked = xdata[mask]
    ydata_masked = ydata[mask]
    field_masked = field[mask]
    h,xbin,ybin = h2dw(np.log10(xdata_masked),np.log10(ydata_masked),field_masked,n=4,nbins_xy=n_bins,
                       x_range=list(np.log10(xdata_range)),y_range=list(np.log10(ydata_range)),
                       mode=mode)
    del field_masked,xdata_masked,ydata_masked
    if type(h_old) != type(None):
        if h_old.shape != h.shape:
            print('\nh_old.shape=%s is different from h.shape=%s\n'%(str(h_old.shape),str(h.shape)))
            return -1
        h = h + h_old
    if c_pass == n_pass-1:
        tmp_xbin = np.power(10.,xbin)
        tmp_ybin = np.power(10.,ybin)
        X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
        f = open(opath+'/'+out_dir+'/acc_%s_%s_b%d.txt'%(species_tag,field_name,tmp_xbin.size-1),'w')
        for i in range(h.shape[1]):
            for j in range(h.shape[2]):
                    for v in h[:,i,j]:
                            f.write('%f '%v)
                    f.write('\n')
        f.close()
        with np.errstate(divide='ignore', invalid='ignore'):
        #    h = np.divide(h[n_bins[0]:,:],h[:n_bins[0],:])
            h = gamma2_2dmap(h)
            h[h==np.inf] = np.nan
            h[h==-np.inf] = np.nan
        #h[np.isnan(h)] = 0.
        if logscale_flg:
            h[np.isnan(h)] = 0.
            if vmin == None:
                vmin = np.nanmin(h[h>0.])
            if vmax == None:
                vmax = np.nanmax(h[h>0.])
            im = ax.pcolormesh(X,Y,h.T,cmap='viridis',norm=mpl.colors.LogNorm(vmin=vmin,vmax=vmax))
        else:
            if vmin == None:
                vmin = np.nanmin(h)
            if vmax == None:
                vmax = np.nanmax(h)
            print(vmin,vmax,species_name,field_name)
            #h[h==0.] = np.nan
            #im = ax.pcolormesh(X,Y,h.T,cmap='viridis',vmin=vmin,vmax=vmax)
            im = ax.pcolormesh(X,Y,h.T,edgecolors='black',cmap='viridis',vmin=vmin,vmax=vmax)
        del X,Y,h

        xvals = np.logspace(np.log10(xdata_range[0]),np.log10(xdata_range[1]),len(xbin)-1)
        del xbin,ybin,tmp_xbin,tmp_ybin

        if species_tag == 'p':
            p,f = th.params['L2014']['pC']['1.0e-2Op']['bM']
            lC, = ax.plot(xvals,th.func[f](xvals,*p),'darkorange',lw=2,label="Cyclotron (CI)")

            p,f = th.params['M2012']['pM']['1.0e-2Op']['bM']
            lM, = ax.plot(xvals,th.func[f](xvals,*p),'red',lw=2,label="Mirror (MI)")

            p,f = th.params['A2016']['pPF']['1.0e-2Op']['bM']
            lP, = ax.plot(xvals,th.func[f](xvals,*p),'forestgreen',lw=2,
                          label="parallel Firehose (PFHI)")

            p,f = th.params['A2016']['pOF']['1.0e-2Op']['bM']
            lO, = ax.plot(xvals,th.func[f](xvals,*p),'darkviolet',lw=2,
                          label="oblique Firehose (OFHI)")

        if species_tag == 'e':
            p,f = th.params['L2013']['eC']['1.0e-2Oe']['bM']
            lC, = ax.plot(xvals,th.func[f](xvals,*p),'darkorange',lw=2,label="Cyclotron (CI)")

            p,f = th.params['G2006']['eM']['1.0e-3Oe']['bM']
            lM, = ax.plot(xvals,th.func[f](xvals,*p),'red',lw=2,label="Mirror (MI)")

            p,f = th.params['G2003']['ePF']['1.0e-2Op']['bM']
            lP, = ax.plot(xvals,th.func[f](xvals,*p),'forestgreen',lw=2,
                          label="parallel Firehose (PFHI)")

            p,f = th.params['H2014']['eOF']['1.0e-3Oe']['bM']
            lO, = ax.plot(xvals,th.func[f](xvals,*p),'darkviolet',lw=2,
                          label="oblique Firehose (OFHI)")

        lb, = ax.plot(xvals,np.divide(1.,xvals),'-.k',lw=2,label='$\\beta_{\parallel}^{-1}$')

        ax.set_xlabel('$\\beta_{\parallel,\mathrm{%s}}$'%(species_tag))
        ax.set_ylabel('$p_{\perp,\mathrm{%s}}/p_{\parallel,\mathrm{%s}}$'%(species_tag,species_tag))
        ax.set_xlim(xdata_range[0],xdata_range[1])
        ax.set_ylim(ydata_range[0],ydata_range[1])
        if legend_flag:
            ax.legend()
        title_str = run_label+' - '+species_name+' - '+field_name
        if t != None:
            title_str += ' - $t = %s$'%(t)
        ax.set_title(title_str)
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.colorbar(im,ax=ax)

        return [lC,lM,lP,lO,lb]
    elif c_pass < n_pass-1:
        return h
    else:
        print('\nc_pass=%d is greater than n_pass-1=%d\n'%(c_pass,n_pass-1))
        return -1

def first_step():
    hp = None
    he = None
    fig,ax = plt.subplots(1,2,figsize=(10*2,8))
    return fig,ax,hp,he

def second_step( ax,field,field_name,mask,p_vars,e_vars,w_ele,n_bins,
                 hp,he,c_pass,n_pass,bparlp,anisp,bparle,anise,
                 betap_lim=None,anisp_lim=None,betae_lim=None,anise_lim=None,
                 vminp=None,vmaxp=None,vmine=None,vmaxe=None ):
    hp = fluct_plot_func_masked(ax[0],bparlp,anisp,mask,field,field_name,p_vars,
                                xdata_range=betap_lim,ydata_range=anisp_lim,n_bins=[n_bins,n_bins],
                                vmin=vminp,vmax=vmaxp,h_old=hp,c_pass=c_pass,n_pass=n_pass)
    if type(hp) == np.int:
        if hp == -1:
            print('\nSomething went wrong in windlike_plot_func (protons)\n')
#
    if w_ele:
        he = fluct_plot_func_masked(ax[1],bparle,anise,mask,field,field_name,e_vars,
                                    xdata_range=betae_lim,ydata_range=anise_lim,n_bins=[n_bins,n_bins],
                                    vmin=vmine,vmax=vmaxe,h_old=he,c_pass=c_pass,n_pass=n_pass)
        if type(he) == np.int:
            if he == -1:
                print('\nSomething went wrong in windlike_plot_func (electrons)\n')
    return hp,he

def third_step( fig,ax,w_ele,hp,n_bins,field_label,
                logscale_flg=logscale_flg,opath=opath,out_dir=out_dir,time_avg_flg=B0_as_time_avg,
                run_name=run_name,ind1=ind1,ind2=ind2,mask_name=mask_name,mode=mode ):
    if not w_ele:
        ax[1].spines['top'].set_visible(False)
        ax[1].spines['right'].set_visible(False)
        ax[1].spines['bottom'].set_visible(False)
        ax[1].spines['left'].set_visible(False)
        ax[1].tick_params(bottom=False,left=False,labelbottom=False,labelleft=False)
        ax[1].legend(handles=hp,loc='center',borderaxespad=0)
    #
    fig.tight_layout()
    title = opath+'/'+out_dir+'/'+'fluct_in_wind_multitimes_'+mask_name+'_'+mode
    if logscale_flg:
        title += '_logscale'
    title += '_'+run_name+'_'+field_label
    if time_avg_flg:
        title += '_B0timeavg'
    title += '_'+str(ind1)+'-'+str(ind2)+'_b'+str(n_bins)
    fig.savefig(title+'.png')
    plt.close(fig)
    return

#-------------------------
#B0
if B0_as_time_avg:
    #as time average
    #---> loop over times <---
    cnt = 0.

    print " ",
    for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
    #-------------------------
        _,B = run.get_EB(ind)
        if ind == ind1:
            Beq = B
        else:
            Beq += B
        cnt += 1.

    Beq /= cnt
else:
    #as initial field
    if code_name == 'HVM':
        Bx0 = 1.
        Bz0 = 0.25
    elif code_name == 'iPIC':
        Bx0 = 0.01
        Bz0 = 0.25*0.01
    A_1 = - Bx0
    A_2 = - A_1

    _,Y = np.meshgrid(x,y)
    Y = Y.T
    Bxeq = ( - A_1*np.tanh( (Y - y1)/L1 )
             - A_2*np.tanh( (Y - y2)/L2 )
             - np.sign(A_1)*(A_1 - A_2)*0.5 )
    del Y

    Beq = np.array([Bxeq,np.zeros((nx,ny),dtype=type(B[0,0,0,0])),
                   np.full((nx,ny),Bz0,dtype=type(B[0,0,0,0]))]).reshape(3,nx,ny,1)
    del Bxeq

Beq2 = Beq[0]*Beq[0] + Beq[1]*Beq[1] + Beq[2]*Beq[2]

#---> loop over times <---
n_bins = 10#5#25*int(round(np.sqrt(ind2-ind1+1)))

#####################################
delta = {'p':[1./((np.log10(betap_lim[1]) - np.log10(betap_lim[0])) / n_bins),
              1./((np.log10(anisp_lim[1]) - np.log10(anisp_lim[0])) / n_bins)],
         'e':[1./((np.log10(betae_lim[1]) - np.log10(betae_lim[0])) / n_bins),
              1./((np.log10(anise_lim[1]) - np.log10(anise_lim[0])) / n_bins)]}

grid = {   'dBparl':{'p':[],'e':[]},
        'fdidBparl':{'p':[],'e':[]},
        'fridBparl':{'p':[],'e':[]}}
for k in grid.keys():
    for _ in range(n_bins):
        grid[k]['p'].append([])
        grid[k]['e'].append([])
        for _ in range(n_bins):
            grid[k]['p'][-1].append([])
            grid[k]['e'][-1].append([])
#####################################

ind_range = np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int)
n_pass = len(ind_range)

plt.close('all')

#field_names = {'dBparl':'$\mathrm{d}B_{\parallel}$',
#               'fdidBparl':'$\mathrm{d}B^{d_{\mathrm{p}}}_{\parallel}$',
#               'fridBparl':'$\mathrm{d}B^{\\rho_{\mathrm{p}}}_{\parallel}$'}
field_names = {'dBparl'   :'dBparl'   ,
               'fdidBparl':'fdidBparl',
               'fridBparl':'fridBparl'}

fg_dict = {}
ax_dict = {}
hp_dict = {}
he_dict = {}
keys = list(field_names.keys())
for k in keys:
    fg_dict[k],ax_dict[k],hp_dict[k],he_dict[k] = first_step()

fields = {}
v = {'min':{'p':{},'e':{}},
     'max':{'p':{},'e':{}}}
for k in keys:
    v['min']['p'][k] = None
    v['max']['p'][k] = None
    v['min']['e'][k] = None
    v['max']['e'][k] = None

print " ",
for c_pass,ind in enumerate(ind_range):
#-------------------------
    #--------------------------------------------------------
    #getting data and computing stuff
    #-------------------------------------
    #anisotropy
    out_dict = wl.compute_anis_and_betaparl(run,ind,r_B=True,r_pparlp=True,r_pperpp=True,r_n_p=True)
    anisp  = out_dict.pop('anisp' )
    anise  = out_dict.pop('anise' )
    bparlp = out_dict.pop('bparlp')
    bparle = out_dict.pop('bparle')
    B      = out_dict.pop('B'     )
    pparlp = out_dict.pop('pparlp')
    pperpp = out_dict.pop('pperpp')
    n_p    = out_dict.pop('n_p'   )
    del out_dict
    Pisop = (pparlp+2.*pperpp)/3.
    del pparlp,pperpp
    #if code_name == 'iPIC' and smooth_flag:
    #    anisp[:,:,0] = gf(anisp[:,:,0],[gfsp,gfsp],mode='wrap')
    #    ETC.
#
    #fluctuations
    dB = B - Beq
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    del B
    #thBdB = v1v2_ang(dB,Beq)
    fields['dBparl'] = np.divide( dB[0]*Beq[0] + dB[1]*Beq[1] + dB[2]*Beq[2],
                        np.sqrt(Beq[0]*Beq[0] + Beq[1]*Beq[1] + Beq[2]*Beq[2]) )
    del dB
#
    #intersting scales
    inv_di = np.sqrt(n_p)
    inv_ri = np.sqrt(B2/Pisop)*inv_di
    if code_name == 'iPIC':
        inv_ri /= 2*np.sqrt(np.pi)
    del B2,n_p,Pisop
    m_inv_di = np.mean(inv_di)
    s_inv_di = np.std(inv_di)
    m_inv_ri = np.mean(inv_ri)
    s_inv_ri = np.std(inv_ri)
    del inv_di,inv_ri
    num_s_di = 1.
    num_s_ri = 1.
    kL_inv_di = m_inv_di - num_s_di*s_inv_di
    kH_inv_di = m_inv_di + num_s_di*s_inv_di
    kL_inv_ri = m_inv_ri - num_s_ri*s_inv_ri
    kH_inv_ri = m_inv_ri + num_s_ri*s_inv_ri
#
    #filtering
    if nz == 1:
        FTdBparl = np.fft.rfft2(fields['dBparl'][...,0]).reshape(nx,ny//2+1,1)
    else:
        FTdBparl = np.fft.rfftn(fields['dBparl'])
    phFTdBparl = np.angle(FTdBparl)
    amFTdBparl = np.abs(FTdBparl)
    del FTdBparl
#
    kx = np.fft.fftfreq(nx,d=x[1]-x[0])*2.*np.pi
    if nz == 1:
        ky = np.fft.rfftfreq(ny,d=y[1]-y[0])*2.*np.pi
        kz = np.linspace(0.,0.,1)
    else:
        ky = np.fft.fftfreq(ny,d=y[1]-y[0])*2.*np.pi
        kz = np.fft.rfftfreq(nz,d=z[1]-z[0])*2.*np.pi
#
    fdiFTdBparl = blackman_filter(amFTdBparl,kx,ky,kz,kL_inv_di,kH_inv_di)*np.exp(1.j*phFTdBparl)
    friFTdBparl = blackman_filter(amFTdBparl,kx,ky,kz,kL_inv_ri,kH_inv_ri)*np.exp(1.j*phFTdBparl)
    del amFTdBparl,phFTdBparl
#
    if nz == 1:
        fields['fdidBparl'] = np.fft.irfft2(fdiFTdBparl[...,0]).reshape(nx,ny,1)
        fields['fridBparl'] = np.fft.irfft2(friFTdBparl[...,0]).reshape(nx,ny,1)
    else:
        fields['fdidBparl'] = np.fft.irfftn(fdiFTdBparl)
        fields['fridBparl'] = np.fft.irfftn(friFTdBparl)
    del fdiFTdBparl,friFTdBparl
#
    ########################################
    for k in grid.keys():
        tracks = np.array([np.log10(bparlp[mask].flatten()),np.log10(anisp[mask].flatten()),fields[k][mask].flatten()],dtype=np.float64)
        for t in range(tracks.shape[1]):
            i = (tracks[0,t] - np.log10(betap_lim[0])) * delta['p'][0]
            j = (tracks[1,t] - np.log10(anisp_lim[0])) * delta['p'][1]
            if 0 <= i < n_bins and 0 <= j < n_bins:
                grid[k]['p'][int(i)][int(j)].append(tracks[2,t])

        tracks = np.array([np.log10(bparle[mask].flatten()),np.log10(anise[mask].flatten()),fields[k][mask].flatten()],dtype=np.float64)
        for t in range(tracks.shape[1]):
            i = (tracks[0,t] - np.log10(betae_lim[0])) * delta['e'][0]
            j = (tracks[1,t] - np.log10(anise_lim[0])) * delta['e'][1]
            if 0 <= i < n_bins and 0 <= j < n_bins:
                grid[k]['e'][int(i)][int(j)].append(tracks[2,t])

    ########################################
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p'}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e'}
#
    #------------------------------------------------------
    #plot 2d histogram
    #-----------------
    for k in keys:
        hp_dict[k],he_dict[k] = second_step(ax_dict[k],fields[k],field_names[k],
                                            mask,p_vars,e_vars,w_ele,n_bins,
                                            hp_dict[k],he_dict[k],c_pass,n_pass,
                                            bparlp,anisp,bparle,anise,
                                            betap_lim=betap_lim,anisp_lim=anisp_lim,
                                            betae_lim=betae_lim,anise_lim=anise_lim,
                                            vminp=v['min']['p'][k],vmaxp=v['max']['p'][k],
                                            vmine=v['min']['e'][k],vmaxe=v['max']['e'][k])
#
#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------

for k in keys:
    third_step(fg_dict[k],ax_dict[k],w_ele,hp_dict[k],n_bins,k)

########################################
for k in grid.keys():
    f = open(opath+'/'+out_dir+'/grid_p_%s_b%d.txt'%(k,n_bins),'w')
    for i in range(n_bins):
        for j in range(n_bins):
                for v in grid[k]['p'][i][j]:
                        f.write('%f '%v)
                f.write('\n')
    f.close()
    f = open(opath+'/'+out_dir+'/grid_e_%s_b%d.txt'%(k,n_bins),'w')
    for i in range(n_bins):
        for j in range(n_bins):
                for v in grid[k]['e'][i][j]:
                        f.write('%f '%v)
                f.write('\n')
    f.close()

    n_ele = {'p':np.zeros((n_bins,n_bins),dtype=np.int),
             'e':np.zeros((n_bins,n_bins),dtype=np.int)}
    g2 = {'p':np.zeros((n_bins,n_bins),dtype=np.float64),
          'e':np.zeros((n_bins,n_bins),dtype=np.float64)}
    for i in range(n_bins):
        for j in range(n_bins):
            valp = np.array(grid[k]['p'][i][j])
            n_ele['p'][i,j] = valp.size
            with np.errstate(divide='ignore', invalid='ignore'):
                g2['p'][i,j] = gamma2(valp)
            vale = np.array(grid[k]['e'][i][j])
            n_ele['e'][i,j] = vale.size
            with np.errstate(divide='ignore', invalid='ignore'):
                g2['e'][i,j] = gamma2(vale)
    #
            plt.close()
            fig,(ax0,ax1) = plt.subplots(1,2,figsize=(14,6))
            if n_ele['p'][i,j] == 0:
                ax0.set_title('p %s %d %d NODATA'%(k,i,j))
            else:
                s = valp.std()
                if s == 0.:
                    ax0.set_title('p %s %d %d SIGMA=0'%(k,i,j))
                else:
                    m = valp.mean()
                    db = .2*s
                    bm = m - 5.5*s
                    bM = m + 5.5*s
                    bp = np.linspace(bm-db*.5,bM+db*.5,int(round((bM-bm+db)/db+1)))
                    hp,_ = np.histogram(valp,bins=bp,density=True)
                    ax0.bar(bp[:-1],hp,width=bp[1]-bp[0],align='edge')
                    bc = (bp[:-1]+bp[1:])*.5
                    maxh = np.max(hp)
                    popt,_ = curve_fit(to_fit,bc,hp,p0=[maxh,m,s])
                    ax0.plot(bc,to_fit(bc,*popt),'--r')
                    ax0.set_title('p %s %d %d %.4f %d'%(k,i,j,g2['p'][i,j],n_ele['p'][i][j]))
                    ax0.set_xlim(bp[0],bp[-1])
                    ax0.set_ylim(np.min(hp[hp>0.]),maxh)
                    ax0.set_yscale('log')
            if n_ele['e'][i,j] == 0:
                ax1.set_title('e %s %d %d NODATA'%(k,i,j))
            else:
                s = vale.std()
                if s == 0.:
                    ax1.set_title('e %s %d %d SIGMA=0'%(k,i,j))
                else:
                    m = vale.mean()
                    db = .2*s
                    bm = m - 5.5*s
                    bM = m + 5.5*s
                    be = np.linspace(bm-db*.5,bM+db*.5,int(round((bM-bm+db)/db+1)))
                    he,_ = np.histogram(vale,bins=be,density=True)
                    ax1.bar(be[:-1],he,width=be[1]-be[0],align='edge')
                    bc = (be[:-1]+be[1:])*.5
                    maxh = np.max(he)
                    popt,_ = curve_fit(to_fit,bc,he,p0=[maxh,m,s])
                    ax1.plot(bc,to_fit(bc,*popt),'--r')
                    ax1.set_title('e %s %d %d %.4f %d'%(k,i,j,g2['e'][i,j],n_ele['e'][i][j]))
                    ax1.set_xlim(be[0],be[-1])
                    ax1.set_ylim(np.min(he[he>0.]),maxh)
                    ax1.set_yscale('log')
            ax0.set_xlabel('k')
            ax1.set_xlabel('k')
            plt.tight_layout()
            fig.savefig(opath+'/'+out_dir+'/%s_PDF_%d_%d_b%d.png'%(k,i,j,n_bins))

    bpbins = np.linspace(np.log10(betap_lim[0]),np.log10(betap_lim[1]),n_bins+1)
    apbins = np.linspace(np.log10(anisp_lim[0]),np.log10(anisp_lim[1]),n_bins+1)
    bebins = np.linspace(np.log10(betae_lim[0]),np.log10(betae_lim[1]),n_bins+1)
    aebins = np.linspace(np.log10(anise_lim[0]),np.log10(anise_lim[1]),n_bins+1)

    plt.close()
    fig,((ax0,ax1),(ax2,ax3)) = plt.subplots(2,2,figsize=(14,14))

    tmp_xbin = np.power(10.,bpbins)
    tmp_ybin = np.power(10.,apbins)
    X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
    h = g2['p']
    vmin = np.nanmin(h)
    vmax = np.nanmax(h)
    print(vmin,vmax)
    im0 = ax0.pcolormesh(X,Y,h.T,edgecolors='black',cmap='viridis',vmin=vmin,vmax=vmax,shading='flat')
    for i in range(h.shape[0]):
        for j in range(h.shape[1]):
            ax0.text((tmp_xbin[i]*tmp_xbin[i+1])**.5,
                     (tmp_ybin[j]*tmp_ybin[j+1])**.5,
                     '%d,%d'%(i,j),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color='red')
    ax0.set_xlabel('betap')
    ax0.set_ylabel('anisp')
    ax0.set_title('p g2 %s'%k)
    plt.colorbar(im0,ax=ax0)
    ax0.set_xscale('log')
    ax0.set_yscale('log')

    tmp_xbin = np.power(10.,bebins)
    tmp_ybin = np.power(10.,aebins)
    X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
    h = g2['e']
    vmin = np.nanmin(h)
    vmax = np.nanmax(h)
    print(vmin,vmax)
    im1 = ax1.pcolormesh(X,Y,h.T,edgecolors='black',cmap='viridis',vmin=vmin,vmax=vmax,shading='flat')
    for i in range(h.shape[0]):
        for j in range(h.shape[1]):
            ax1.text((tmp_xbin[i]*tmp_xbin[i+1])**.5,
                     (tmp_ybin[j]*tmp_ybin[j+1])**.5,
                     '%d,%d'%(i,j),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color='red')
    ax1.set_xlabel('betae')
    ax1.set_ylabel('anise')
    ax1.set_title('e g2 %s'%k)
    plt.colorbar(im1,ax=ax1)
    ax1.set_xscale('log')
    ax1.set_yscale('log')

    tmp_xbin = np.power(10.,bpbins)
    tmp_ybin = np.power(10.,apbins)
    X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
    h = n_ele['p']
    vmin = np.nanmin(h[h>0.])
    vmax = np.nanmax(h[h>0.])
    im2 = ax2.pcolormesh(X,Y,h.T,edgecolors='black',cmap='viridis',norm=mpl.colors.LogNorm(vmin=vmin,vmax=vmax))
    for i in range(h.shape[0]):
        for j in range(h.shape[1]):
            ax2.text((tmp_xbin[i]*tmp_xbin[i+1])**.5,
                     (tmp_ybin[j]*tmp_ybin[j+1])**.5,
                     '%d,%d'%(i,j),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color='red')
    ax2.set_xlabel('betap')
    ax2.set_ylabel('anisp')
    ax2.set_title('p n %s'%k)
    plt.colorbar(im2,ax=ax2)
    ax2.set_xscale('log')
    ax2.set_yscale('log')

    tmp_xbin = np.power(10.,bebins)
    tmp_ybin = np.power(10.,aebins)
    X,Y = np.meshgrid(tmp_xbin,tmp_ybin)
    h = n_ele['e']
    vmin = np.nanmin(h[h>0.])
    vmax = np.nanmax(h[h>0.])
    im3 = ax3.pcolormesh(X,Y,h.T,edgecolors='black',cmap='viridis',norm=mpl.colors.LogNorm(vmin=vmin,vmax=vmax))
    for i in range(h.shape[0]):
        for j in range(h.shape[1]):
            ax3.text((tmp_xbin[i]*tmp_xbin[i+1])**.5,
                     (tmp_ybin[j]*tmp_ybin[j+1])**.5,
                     '%d,%d'%(i,j),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color='red')
    ax3.set_xlabel('betae')
    ax3.set_ylabel('anise')
    ax3.set_title('e n %s'%k)
    plt.colorbar(im3,ax=ax3)
    ax3.set_xscale('log')
    ax3.set_yscale('log')

    plt.tight_layout()
    fig.savefig(opath+'/'+out_dir+'/%s_g2_n_control_b%d.png'%(k,n_bins))
########################################
