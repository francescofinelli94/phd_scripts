#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
#mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot something\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
out_dir = '2d_plots_By_Bz/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']

#out_dir = out_dir+'_gausspost2'#+'_Xzoom'#+'_gauss2'#+'_Xzoom_gauss2'
ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

#params
g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

idtens = np.zeros((3,3,nx,ny,nz),dtype=np.float64)
for i in range(3):
    idtens[i,i] = 1.

#------------------
def my_FFT_smooth(field,keep_percentage,gauss=True):
    if not gauss:
        nx,ny = field.shape
    #
        mx_cut = int(round(float(nx//2+1)*keep_percentage[0]))
        my_cut = int(round(float(ny//2+1)*keep_percentage[1]))
    #
        f = np.empty((nx,ny//2+1),dtype=np.complex128)
        f = np.fft.rfft2(field)
        f[mx_cut:nx//2+1,:] = 0.
        if nx%2 == 0:
            f[nx//2+1:nx//2+1+nx//2-mx_cut,:] = 0.
        else:
            f[nx//2+1:nx//2+1+nx//2-mx_cut+1,:] = 0.
        f[:,my_cut:] = 0.
        field_ = np.empty((nx,2*(ny//2)),np.double)
        field_ = np.fft.irfft2(f)
    else:
        field_ = gf(field,[2,2],mode='wrap')
#
    return field_

kp = [3./5.,3./5.]

smooth_flag = False #True
smooth_flag_post = False

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = my_FFT_smooth(B[i,:,:,0],kp)

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)

    # -> By,Bz
    plt.close()
    fig,(ax0,ax1) = plt.subplots(2,1,figsize=(15,10))

    im0 = ax0.contourf(x[ixmin:ixmax],y[iymin:iymax],B[1,ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im0,ax=ax0)
    ax0.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='inferno')
    ax0.set_ylabel('y')
    ax0.set_title('$B_y$ - t='+str(time[ind])+'$\Omega_C^{-1}$')

    im1 = ax1.contourf(x[ixmin:ixmax],y[iymin:iymax],B[2,ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im1,ax=ax1)
    ax1.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,16,cmap='inferno')
    ax1.set_xlabel('x')
    ax1.set_ylabel('y')
    ax1.set_title('$B_z$')

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'By_Bx_'+run_name+'_'+str(ind)+'.png')
    plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
