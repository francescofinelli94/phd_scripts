close,/all

;load routines
resolve_routine,'singular2d',/is_function

;define input path
ipath = '/home/finelli/Downloads/Psi_field'
print,'input path set to: '
print,ipath
ans = ''
ans = 'n' ;DBG
while (ans ne 'y') and (ans ne 'n') do begin
    print,'change input path? (y/n)'
    read,ans
endwhile
if ans eq 'y' then begin
    print,'choose new input path:'
    read,ipath
endif
run_name = ''
print,'insert run name:'
read,run_name ;'DH_run3_data0' ;'LF_2d_DH' ;'HVM_2d_DH' ;'DH_run5_data0'

;create output folder
out_dir = ipath+'/'+run_name+'/tracked_points'
res = file_test(out_dir, /directory)
if res then res = 0 else res = 1
if res then file_mkdir,out_dir

;append?
ll_append = 1
print,'start new rec_data files or append?'
print,'    0 -> new file'
print,'    1 -> append'
read,ll_append

;---------------
;DEAL WITH THESE ;HARDCODED!!!!!!

if run_name eq 'LF_2d_DH' then begin
    gs_sigma = [2,2]
    control_range = [50,50]
    max_dist = [50,50]
endif
if run_name eq 'HVM_2d_DH' then begin
    gs_sigma = [0,0]
    control_range = [50,50]
    max_dist = [50,50]
endif
if run_name eq 'DH_run3_data0' then begin
    gs_sigma = [5,5]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'DH_run5_data0' then begin
    gs_sigma = [5,5]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'DH_run7_data0' then begin
    gs_sigma = [2,2]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'DH_run8' then begin
    gs_sigma = [2,2]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'DH_run9' then begin
    gs_sigma = [2,2]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'DH_run10' then begin
    gs_sigma = [2,2]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'DH_run11_data0' then begin
    gs_sigma = [2,2]
    control_range = [20,20]
    max_dist = [100,100]
endif
if run_name eq 'HVM_large' then begin
    gs_sigma = [5,5]
    control_range = [20,20]
    max_dist = [50,50]
endif

;---------------

;init reading variables
nx = 0L
ny = 0L
nz = 0L
t  = 0.0d0
a0 = 0.0e0

;get times
dat_file = file_search(ipath+'/'+run_name+'/Psi_*.dat',/test_regular)
nt = n_elements(dat_file)
time = dblarr(nt)
for it = 0, nt-1 do begin
    openr,10,dat_file[it]
    readf,10,nx,ny,nz,t
    time[it] = t
    close,10
endfor
dat_file = dat_file[sort(time)]
time = time[sort(time)]

;space range ;HARDCODED!!!!!!!!
xmin = 0L
xmax = nx - 1L
ymin = 85L - 15L
ymax = 85L + 15L
xyper = 0
xper  = 1
yper  = 0
aper  = 0

;time range ;HARDCODED!!!!
first = 0
last = nt-1

;init data structures and variables
xll = list(list(),list(),list())
oll = list(list(),list(),list())

;----------------
;MAIN LOOP
for it = first, last do begin
;----------------
;get Psi
openr,10,dat_file[it]
readf,10,nx,ny,nz,t
Psi = dblarr(nx,ny)
for iy = 0, ny-1 do begin
    for ix = 0, nx-1 do begin
    	readf,10,a0
    	Psi[ix,iy] = a0
    endfor
endfor
close,10

;find singular points
tmp_Psi = Psi
if xyper then $
SngPnt = singular2d(tmp_Psi,[xmin,xmax,ymin,ymax],/xperiodic,/yperiodic,gs_sigma=gs_sigma,dc=control_range,/indices_gl,/clean)
if xper then $
SngPnt = singular2d(tmp_Psi,[xmin,xmax,ymin,ymax],/xperiodic,gs_sigma=gs_sigma,dc=control_range,/indices_gl,/clean)
if yper then $
SngPnt = singular2d(tmp_Psi,[xmin,xmax,ymin,ymax],/yperiodic,gs_sigma=gs_sigma,dc=control_range,/indices_gl,/clean)
if aper then $
SngPnt = singular2d(tmp_Psi,[xmin,xmax,ymin,ymax],gs_sigma=gs_sigma,dc=control_range,/indices_gl,/clean)

npts = n_elements(SngPnt.sad)
if npts gt 0 then xpoints = [[SngPnt.sad[*].ix],[SngPnt.sad[*].iy]] else xpoints = []
npts = n_elements(SngPnt.min)
if npts gt 0 then opoints = [[SngPnt.min[*].ix],[SngPnt.min[*].iy]] else opoints = []
npts = n_elements(SngPnt.max)
if npts gt 0 then opoints = [opoints,[[SngPnt.max[*].ix],[SngPnt.max[*].iy]]]

;tracking
if it eq first then begin ;first initialization
    if xpoints ne !NULL then xnum = n_elements(xpoints(*,0)) else xnum = 0
    for i = 0, xnum-1 do begin
        xll(0).add, []   ;time
        xll(1).add, []   ;coordinates of X-point
        xll(2).add, []   ;Psi(X)
    endfor

    if opoints ne !NULL then onum = n_elements(opoints(*,0)) else onum = 0
    for i = 0, onum-1 do begin
        oll(0).add, []   ;time
        oll(1).add, []   ;coordinates of O-point
        oll(2).add, []   ;Psi(O)
    endfor

    x_update_flags = []
    old_x = []
    for i = 0, xnum-1 do begin
        x_update_flags = [x_update_flags,1]
        old_x = [old_x,[[xpoints(i,0)],[xpoints(i,1)]]]

        xll(0,i) = [xll(0,i),time(it)]
        xll(1,i) = [xll(1,i),[[xpoints(i,0)],[xpoints(i,1)]]]
        xll(2,i) = [xll(2,i),Psi(xpoints(i,0),xpoints(i,1))]
    endfor

    o_update_flags = []
    old_o = []
    for i = 0, onum-1 do begin
        o_update_flags = [o_update_flags,1]
        old_o = [old_o,[[opoints(i,0)],[opoints(i,1)]]]

        oll(0,i) = [oll(0,i),time(it)]
        oll(1,i) = [oll(1,i),[[opoints(i,0)],[opoints(i,1)]]]
        oll(2,i) = [oll(2,i),Psi(opoints(i,0),opoints(i,1))]
    endfor
endif else begin
    ;X-points tracking
    if xpoints ne !NULL then begin
        tmp_xnum = n_elements(xpoints(*,0))
        x_flags = make_array(tmp_xnum,/integer,value=0)
        for i = 0, xnum-1 do begin
            if x_update_flags(i) eq 0 then continue

            jj = 0
            distij = []
            distijj = []
            for j = 0, tmp_xnum-1 do begin
                distij = [min([abs(old_x(i,0)-xpoints(j,0)),nx-abs(old_x(i,0)-xpoints(j,0))]),$
                          min([abs(old_x(i,1)-xpoints(j,1)),ny-abs(old_x(i,1)-xpoints(j,1))])]
                distijj = [min([abs(old_x(i,0)-xpoints(jj,0)),nx-abs(old_x(i,0)-xpoints(jj,0))]),$
                           min([abs(old_x(i,1)-xpoints(jj,1)),ny-abs(old_x(i,1)-xpoints(jj,1))])]
                if (distij(0)^2+distij(1)^2) lt (distijj(0)^2+distijj(1)^2) then jj = j
            endfor
            distijj = [min([abs(old_x(i,0)-xpoints(jj,0)),nx-abs(old_x(i,0)-xpoints(jj,0))]),$
                       min([abs(old_x(i,1)-xpoints(jj,1)),ny-abs(old_x(i,1)-xpoints(jj,1))])]
            if (distijj(0) gt max_dist(0)) or (distijj(1) gt max_dist(1)) then x_update_flags(i) = 0

            if x_update_flags(i) eq 0 then continue

            x_flags(jj) = 1

            old_x(i,*) = xpoints(jj,*)

            xll(0,i) = [xll(0,i),time(it)]
            xll(1,i) = [xll(1,i),[[xpoints(jj,0)],[xpoints(jj,1)]]]
            xll(2,i) = [xll(2,i),Psi(xpoints(jj,0),xpoints(jj,1))]
        endfor
    endif else begin
        if x_update_flags ne !NULL then x_update_flags = x_update_flags*0
    endelse

    ;O-points tracking
    if opoints ne !NULL then begin
        tmp_onum = n_elements(opoints(*,0))
        o_flags = make_array(tmp_onum,/integer,value=0)
        for i = 0, onum-1 do begin
            if o_update_flags(i) eq 0 then continue

            jj = 0
            distij = []
            distijj = []
            for j = 0, tmp_onum-1 do begin
                distij = [min([abs(old_o(i,0)-opoints(j,0)),nx-abs(old_o(i,0)-opoints(j,0))]),$
                          min([abs(old_o(i,1)-opoints(j,1)),ny-abs(old_o(i,1)-opoints(j,1))])]
                distijj = [min([abs(old_o(i,0)-opoints(jj,0)),nx-abs(old_o(i,0)-opoints(jj,0))]),$
                           min([abs(old_o(i,1)-opoints(jj,1)),ny-abs(old_o(i,1)-opoints(jj,1))])]
                if (distij(0)^2+distij(1)^2) lt (distijj(0)^2+distijj(1)^2) then jj = j
            endfor
            distijj = [min([abs(old_o(i,0)-opoints(jj,0)),nx-abs(old_o(i,0)-opoints(jj,0))]),$
                       min([abs(old_o(i,1)-opoints(jj,1)),ny-abs(old_o(i,1)-opoints(jj,1))])]
            if (distijj(0) gt max_dist(0)) or (distijj(1) gt max_dist(1)) then o_update_flags(i) = 0

            if o_update_flags(i) eq 0 then continue

            o_flags(jj) = 1

            old_o(i,*) = opoints(jj,*)

            oll(0,i) = [oll(0,i),time(it)]
            oll(1,i) = [oll(1,i),[[opoints(jj,0)],[opoints(jj,1)]]]
            oll(2,i) = [oll(2,i),Psi(opoints(jj,0),opoints(jj,1))]
        endfor
    endif else begin
        if o_update_flags ne !NULL then o_update_flags = o_update_flags*0
    endelse

    ;new X-points addition
    if xpoints ne !NULL then begin
        for j = 0, tmp_xnum-1 do begin
            if x_flags(j) eq 1 then continue

            xll(0).add, []
            xll(1).add, []
            xll(2).add, []

            x_update_flags = [x_update_flags,1]
            old_x = [old_x,[[xpoints(j,0)],[xpoints(j,1)]]]

            xll(0,xnum) = [xll(0,xnum),time(it)]
            xll(1,xnum) = [xll(1,xnum),[[xpoints(j,0)],[xpoints(j,1)]]]
            xll(2,xnum) = [xll(2,xnum),Psi(xpoints(j,0),xpoints(j,1))]

            xnum = xnum + 1
        endfor
    endif

    ;new O-points addition
    if opoints ne !NULL then begin
        for j = 0, tmp_onum-1 do begin
            if o_flags(j) eq 1 then continue

            oll(0).add, []
            oll(1).add, []
            oll(2).add, []

            o_update_flags = [o_update_flags,1]
            old_o = [old_o,[[opoints(j,0)],[opoints(j,1)]]]

            oll(0,onum) = [oll(0,onum),time(it)]
            oll(1,onum) = [oll(1,onum),[[opoints(j,0)],[opoints(j,1)]]]
            oll(2,onum) = [oll(2,onum),Psi(opoints(j,0),opoints(j,1))]

            onum = onum + 1
        endfor
    endif
endelse
;----------------
;END MAIN LOOP
print,'it = '+string(it)+' done'
endfor
;----------------

;   [x/o]ll(0) -> time
;   [x/o]ll(1) -> coordinates of [X/O]-point
;   [x/o]ll(2) -> Psi([X/O])

;look for time extremes
t0 = oll(0,0,0)
t1 = t0
for i = 0, xnum-1 do begin
    if t1 lt max(xll(0,i)) then t1 = max(xll(0,i))
endfor
for i = 0, onum-1 do begin
    if t1 lt max(oll(0,i)) then t1 = max(oll(0,i))
endfor

;plot tracks
print,'starting plot'

fig = plot([0],[0],xrange=[xmin,xmax],yrange=[t0,t1],symbol='none',xtitle='x',ytitle='t',title='tracked X-points and O-points')
;for i = 0, xnum-1 do begin
;    if (xll(0,i,0) eq t0) and (xll(0,i,-1) eq t1) then continue
;    fig = plot(xll(1,i,*,0),xll(0,i),linestyle='-','-k',/overplot)
;endfor
;for i = 0, onum-1 do begin
;    if (oll(0,i,0) eq t0) and (oll(0,i,-1) eq t1) then continue
;    fig = plot(oll(1,i,*,0),oll(0,i),linestyle='-.','-r',/overplot)
;endfor
for i = 0, xnum-1 do begin
    if (xll(0,i,0) eq t0) and (xll(0,i,-1) eq t1) then $
    fig = plot(xll(1,i,*,0),xll(0,i),linestyle='-','-g',/overplot)
endfor
for i = 0, onum-1 do begin
    if (oll(0,i,0) eq t0) and (oll(0,i,-1) eq t1) then $
    fig = plot(oll(1,i,*,0),oll(0,i),linestyle='-.','-g',/overplot)
endfor
fig.save,out_dir+'/tracked_points_'+run_name+'.png'
fig.close

print,'plot done'

;output on files
print,'writing file'

;x points
for i = 0, xnum-1 do begin
    if (xll(0,i,0) ne t0) or (xll(0,i,-1) ne t1) then continue
    if ll_append eq 0 then begin
        openw,10,out_dir+'/'+'xpoint_from_'+strtrim(xll(1,i,0,0),2)+'.dat'
    endif else begin
        openu,10,out_dir+'/'+'xpoint_from_'+strtrim(xll(1,i,0,0),2)+'.dat',/append
    endelse
    tnum = n_elements(xll(0,i))
    for it = 0, tnum-1 do begin
        printf,10,format='(1F,2I,1E)',xll(0,i,it),xll(1,i,it,0),xll(1,i,it,1),xll(2,i,it)
    endfor
    close,10
endfor

;o points
for i = 0, onum-1 do begin
    if (oll(0,i,0) ne t0) or (oll(0,i,-1) ne t1) then continue
    if ll_append eq 0 then begin
        openw,10,out_dir+'/'+'opoint_from_'+strtrim(oll(1,i,0,0),2)+'_'+run_name+'.dat'
    endif else begin
        openu,10,out_dir+'/'+'opoint_from_'+strtrim(oll(1,i,0,0),2)+'_'+run_name+'.dat',/append
    endelse
    tnum = n_elements(oll(0,i))
    for it = 0, tnum-1 do begin
        printf,10,format='(1F,2I,1E)',oll(0,i,it),oll(1,i,it,0),oll(1,i,it,1),oll(2,i,it)
    endfor
    close,10
endfor

print,'writing done'

;-------------
;END
end
;-------------
