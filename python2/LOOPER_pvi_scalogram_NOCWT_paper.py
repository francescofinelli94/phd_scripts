import sys
import glob
import os
sys.path.insert(0,'/work2/finelli/Documents_SAV/my_branch')
import mylib as ml

#----------------------------------------------------------------------------------------------------
#HARDCODED!!!
pvi_path = '/work2/finelli/pvi_series'

input_rm = True
log_rm = True
err_log_rm = False

#----------------------------------------------------------------------------------------------------
def dir_name(n,a,b):
    if n == 0:
        return 'J%dP%d_NOCWT'%(int(a*100),int(b*100))
    elif n == 1:
        return 'J%dsP%ds_NOCWT'%(int(a*100),int(b*100))
    else:
        print('Unknown n=%d value!'%n)
        return None

gs_flg = ml.secure_input('Make plots?\n\tYes -> 0\n\tNo -> 1\n: ',1,True)

ans = ml.secure_input('Thresholds:\n\tmanual -> 0\n\tstat. -> 1\n: ',1,True)
if ans == 0:
    thJ = ml.secure_input('|J| threshold (0 -> 1): ',1.,True)
    thP = ml.secure_input('PVI threshold (0 -> 1): ',1.,True)
elif ans == 1:
    thJ = ml.secure_input('Number of st. dev. for J^2 threshold (>0.): ',1.,True)
    thP = ml.secure_input('Number of st. dev. for PVI threshold (>0.): ',1.,True)
else:
    print('ans value %d not recognized.'%ans)
    sys.exit(-1)

os.system('echo -----------------RUN START---------------- >> err_log_pvi_scalogram_%s.dat'%(dir_name(ans,thJ,thP)))

# -> what time is it?
segments = sorted([int(tmp.split('_')[-1]) for tmp in glob.glob(pvi_path+'/new_pvi_*')])
if len(segments) == 0:
    sys.exit("ERROR: no direcory new_pvi_* found in:\n"+pvi_path)
print('\nsegments:')
print(segments)

for seg in segments:
    angles = sorted([int(tmp.split('angle')[-1]) 
        for tmp in glob.glob(pvi_path+('/new_pvi_%d/angle*')%(seg)) 
        if len(tmp.split('angle')[-1].split('_'))==1])
    if len(angles) == 0:
        sys.exit("ERROR: no direcory angle* found in:\n"+pvi_path+('/new_pvi_%d')%(seg))
    print('\nangles:')
    print(angles)

    for ang in angles:
        print('\n---> SEG %d ANG %d START!!! <---\n'%(seg,ang))
        if (seg == 28) and (ang == 38):
            continue
        os.system('echo ---- seg %d ang %d ---- >> err_log_pvi_scalogram_%s.dat'%(seg,ang,dir_name(ans,thJ,thP)))
        fname = 'fast_input_pvi_scalogram_%d_%d_%s.dat'%(seg,ang,dir_name(ans,thJ,thP))
        logname = 'log_pvi_scalogram_%d_%d_%s.dat'%(seg,ang,dir_name(ans,thJ,thP))
        f = open(fname,'w')
        f.write('%d\n%d\n%d\n%d\n%f\n%f\n\'n\'\n '%(seg,ang,gs_flg,ans,thJ,thP))
        f.close()
        os.system('python pvi_scalogram_NOCWT_paper.py < %s > %s 2>> err_log_pvi_scalogram_%s.dat'%(fname,logname,dir_name(ans,thJ,thP)))
        print('\n---> SEG %d ANG %d DONE!!! <---\n'%(seg,ang))

if input_rm:
    os.system('rm -f fast_input_pvi_scalogram_*_%s.dat'%(dir_name(ans,thJ,thP)))
if log_rm:
    os.system('rm -f log_pvi_scalogram_*_%s.dat'%(dir_name(ans,thJ,thP)))
if err_log_rm:
    os.system('rm -f err_log_pvi_scalogram_%s.dat'%(dir_name(ans,thJ,thP)))

