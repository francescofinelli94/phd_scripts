#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
import gc

import numpy as np
from matplotlib import use as mpl_use
mpl_use('Agg')
import matplotlib.pyplot as plt
from scipy.integrate import ode as si_ode
from scipy.interpolate import RegularGridInterpolator
from scipy.ndimage import map_coordinates as ndm_map_coordinates
from scipy.integrate import simps as si_simpson

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nPerforms 3D line integration of Eparl\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------
run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
# ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'line_integration'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------
def apply_threshold(field, th, dx=dz, dy=dy, dz=dx):
    pos_x, pos_y, pos_z = np.where(field > th)
    pos_x, pos_y, pos_z = ( pos_x.astype(np.float64)*dx,
                            pos_y.astype(np.float64)*dy,
                            pos_z.astype(np.float64)*dz )
    return pos_x, pos_y, pos_z

def generate_stream_seeds(pos_x, pos_y, pos_z, n_seed):
    n_seed = min(pos_x.size, n_seed)
    inds = np.arange(0, pos_x.size, pos_x.size//n_seed, dtype=int)
    return np.array([pos_x[inds], pos_y[inds], pos_z[inds]])

def construct_field_lines( interpolated_function, seeds, dt, xc, yc,
                           max_dist, max_length, max_cnt=10000, xl=xl, yl=yl, zl=zl):
    max_dist = max_dist**2
    x_list = []
    y_list = []
    z_list = []
    s_list = []
    for ns in range(seeds.shape[1]):
        # calculate field lines starting from the seed
        # integrate in both directions
        x = [seeds[0, ns]]
        y = [seeds[1, ns]]
        z = [seeds[2, ns]]
        s = [0.]
        for d in [-1, 1]:
            # set the direction
            dt *= -1.
            r = si_ode(f_norm_interpolated)
            r.set_integrator('vode')
            r.set_f_params(xl, yl, zl, interpolated_function)
            # initial position of the magnetic field line
            r.set_initial_value(seeds[:, ns], 0)
            cnt = 0
            while r.successful():
                r.integrate(r.t+dt)
                x.append(r.y[0])
                y.append(r.y[1])
                z.append(r.y[2])
                ds = np.sqrt( (x[-1] - x[-2])**2 + (y[-1] - y[-2])**2 + (z[-1] - z[-2])**2 ) 
                s.append(s[-1] + ds*float(d))
                # check if field line left a given volume
                cnt += 1
                if ( ((r.y[0] - xc)**2 + (r.y[1] - yc)*2 > max_dist) or
                     (cnt > max_cnt) or (abs(s[-1]) > max_length) ):
                    break
            x = x[::d]
            y = y[::d]
            z = z[::d]
            s = s[::d]
        x_list.append(np.array(x))
        y_list.append(np.array(y))
        z_list.append(np.array(z))
        s_list.append(np.array(s))
    return x_list, y_list, z_list, s_list

def f_norm_interpolated(t, p, xl, yl, zl, interpolated_function):
    x, y, z = p[0]%xl, p[1]%yl, p[2]%zl
    return interpolated_function([x, y, z])[0] 

def remove_imterpolation_artifacts(a, min_jmp=5, max_jmp=200, str_jmp=10):
    da = np.diff(a)
    dda = np.diff(da)
    ix_jmp = np.where(np.abs(dda - np.mean(dda)) > np.std(dda))[0][1::2]
    a_ = a.copy()
    flg_continue = False
    err_list = []
    ix_jmp_cp = ix_jmp.copy()
    ix_jmp = list(ix_jmp)
    for jmp in range(min_jmp, max_jmp+str_jmp, str_jmp):
        tbr = [False for ix in ix_jmp]
        for i in range(len(ix_jmp)):
            if i == len(ix_jmp) - 1: break
            if flg_continue:
                flg_continue = False
                continue
            ix = ix_jmp[i]
            ix_nxt = ix_jmp[i+1]
            if ix_nxt - ix > jmp: continue
            err_list.append([ix+1, ix_nxt])
            tbr[i] = True
            tbr[i+1] = True
            flg_continue = True
        for i in range(len(tbr)-1,-1,-1):
            if tbr[i]:
                del ix_jmp[i]
    for lst in err_list:
        ix, ix_nxt = lst
        j = a[ix] - a[ix-1]
        a_[ ix : ix_nxt+1 ] -= j
    return a_

def cut_field_lines(Eparl_lines, s_lines):
    cut_lines = []
    for i in range(len(Eparl_lines)):
        is0 = np.argmin(np.abs(s_lines[i]))
        pos = np.zeros((Eparl_lines[i].size,), dtype=bool)
        nel = np.zeros((Eparl_lines[i].size,), dtype=bool)
        ner = np.zeros((Eparl_lines[i].size,), dtype=bool)
        pos[is0] = True
        for j in range(is0+1,pos.size):
            if Eparl_lines[i][j] < 0.: break
            pos[j] = True
        for j in range(is0-1,-1,-1):
            if Eparl_lines[i][j] < 0.: break
            pos[j] = True
        for j in range(is0+1,pos.size):
            if pos[j]: continue
            if Eparl_lines[i][j] >= 0.: break
            ner[j] = True
        for j in range(is0-1,-1,-1):
            if pos[j]: continue
            if Eparl_lines[i][j] >= 0.: break
            nel[j] = True
        i0 = np.argmin(Eparl_lines[i]*nel.astype(float))
        i1 = np.argmin(Eparl_lines[i]*ner.astype(float))
        pos = np.zeros((Eparl_lines[i].size,), dtype=bool)
        pos[i0 : i1+1] = True
        cut_lines.append(pos)
    return cut_lines

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #------------------------------------------------------
    # Get data
    #----------
    #-------
    t0 = t_time()
    #-------
    # -> magnetic fields and co.
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm

    del Bnorm

    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
        J = np.array([cx, cy, cz]) # J = rot(B)
        del cx, cy, cz

    # -> densities and currents
    if code_name == 'HVM':
        n_p, u_p = run.get_Ion(ind)
        u_e = np.empty((3, nx, ny, nz), dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0], n_p)
        u_e[1] = u_p[1] - np.divide(J[1], n_p)
        u_e[2] = u_p[2] - np.divide(J[2], n_p)
        del n_p, J, u_p
    elif code_name == 'iPIC':
        # _, u_p = run.get_Ion(ind)
        # n_e, u_e = run.get_Ion(ind,qom=qom_e)
        # J = n_p*u_p - n_e*u_e
        _, u_e = run.get_Ion(ind,qom=qom_e)

    # -> demag
    e_demag = np.empty(E.shape, dtype=np.float64)
    e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
    e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
    e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
    e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)
    del e_demag

    # p_demag = np.empty(E.shape,dtype=np.float64)
    # p_demag[0] = E[0] + u_p[1]*B[2] - u_p[2]*B[1]
    # p_demag[1] = E[1] + u_p[2]*B[0] - u_p[0]*B[2]
    # p_demag[2] = E[2] + u_p[0]*B[1] - u_p[1]*B[0]
    # p_demag_n = np.sqrt(p_demag[0]**2 + p_demag[1]**2 + p_demag[2]**2)
    # del p_demag

    del B

    #-------
    print('\nData loading and computing: %fs'%(t_time() - t0,))
    #-------
    #------------------------------------------------------
    # apply threshold and generate seeds
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    n_seeds = 200
    pos_x, pos_y, pos_z = apply_threshold(e_demag_n, np.max(e_demag_n)*.75)
    seeds = generate_stream_seeds(pos_x, pos_y, pos_z, n_seeds)

    #------------------------------------------------------
    # generate field lines
    #----------------------
    dt = max(dx, dy, dz)*.1
    max_dist = 50.
    max_length = 100.
    interpolated_function = RegularGridInterpolator( (x, y, z), b, method='linear',
                                                     bounds_error=False, fill_value=None )
    x_lines, y_lines, z_lines, s_lines = construct_field_lines( interpolated_function, seeds, dt,
                                                                np.mean(pos_x), np.mean(pos_y),
                                                                max_dist, max_length )

    #-------
    print('Generating field lines: %fs'%(t_time() - t0,))
    #-------
    #------------------------------------------------------
    # interpolte parallel electric field
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    Eparl_lines = []
    for i in range(seeds.shape[1]):
        coords_x = (x_lines[i]/dx) % nx
        coords_y = (y_lines[i]/dy) % ny
        coords_z = (z_lines[i]/dz) % nz
        Eparl_lines.append( ndm_map_coordinates(
                            Eparl, np.vstack((coords_x, coords_y, coords_z)), mode='wrap'
                            ) )
        Eparl_lines[-1] = remove_imterpolation_artifacts(Eparl_lines[-1])

    #-------
    print('Initerpolating Eparl: %fs'%(t_time() - t0,))
    #-------
    #------------------------------------------------------
    # interpolte parallel electric field
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    cut_lines = cut_field_lines(Eparl_lines, s_lines)
    #-------
    print('Cutting Eparl: %fs'%(t_time() - t0,))
    #-------
    #------------------------------------------------------
    # integrate Eparl on lines
    #--------------------------
    #-------
    t0 = t_time()
    #-------
    R_lines = []
    for i in range(seeds.shape[1]):
        R_lines.append( si_simpson(Eparl_lines[i][cut_lines[i]], x=s_lines[i][cut_lines[i]]) )
    #-------
    print('Integrating Eparl: %fs'%(t_time() - t0,))
    #-------
    #------------------------------------------------------
    # write on file
    #-------------------------
    with open(opath+'/'+out_dir+'/R_lines_%s_%d.dat'%(run_name, ind), 'w') as f:
        for r in R_lines:
            f.write('%f\n'%(r,))

    #------------------------------------------------------
    # plot Eparl interpolated
    #-------------------------
    if 1:
        plt.close('all')
        for i in range(seeds.shape[1]):
            plt.plot(s_lines[i][cut_lines[i]], Eparl_lines[i][cut_lines[i]], label='%f'%(R_lines[i],))
        plt.xlabel('s')
        plt.ylabel('Eparl')
        plt.legend()
        plt.savefig(opath+'/'+out_dir+'/Eparl_lines_%s_%d.png'%(run_name, ind))
        plt.close()

    #------------------------------------------------------
    # 2d contourf plots
    #-------------------
    if 1:
        plt.close('all')
        # fig, (ax0, ax1) = plt.subplots(2, 1)
        fig, ax1 = plt.subplots(1, 1)

        #im0 = ax0.contourf(x, y, p_demag_n[...,nz//2].T, 63)
        #plt.colorbar(im0, ax=ax0)
        #ax0.set_title('|E + u_p x B|')
        #ax0.set_xlabel('x')
        #ax0.set_ylabel('y')
        #ax0.set_xlim(x[0], x[-1])
        #ax0.set_ylim(y[0], y[-1])

        im1 = ax1.contourf(x, y, e_demag_n[...,nz//2].T, 63)
        plt.colorbar(im1, ax=ax1)
        ax1.set_title('|E + u_e x B|')
        ax1.set_xlabel('x')
        ax1.set_ylabel('y')
        ax1.set_xlim(x[0], x[-1])
        ax1.set_ylim(y[0], y[-1])

        plt.tight_layout()
        plt.savefig(opath+'/'+out_dir+'/e_demag_halfz_%s_%d.png'%(run_name, ind))
        plt.close()

    #------------------------------------------------------
    # 2d contourf plots in 3d box
    #-----------------------------
    if 1:
        plt.close('all')
        fig = plt.figure()
        ax = fig.gca(projection='3d')

        Fmin = np.min(e_demag_n)
        Fmax = np.max(e_demag_n)
        z0 = (e_demag_n[...,0].T - Fmin)/(Fmax - Fmin)*.1
        # z1 = z[nz//2] + (e_demag_n[...,0].T - Fmin)/(Fmax - Fmin)*.1

        levels = np.linspace(.0, .1, 63)
        X, Y = np.meshgrid(x, y)
        n_points = 200

        # ax.contourf(X, Y, z0, levels=levels)
        # ax.contourf(X, Y, z1, levels=z[nz//2]+levels)

        n_points = min(pos_x.size, n_points)
        every = pos_x.size//n_points
        if 1:
            ax.scatter(pos_x[::every], pos_y[::every], pos_z[::every], c='r')

        ax.scatter(seeds[0], seeds[1], seeds[2], c='g')

        for i in range(seeds.shape[1]):
            ax.plot(x_lines[i][cut_lines[i]], y_lines[i][cut_lines[i]], z_lines[i][cut_lines[i]])

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        ax.set_xlim3d(x[0], x[-1])
        ax.set_ylim3d(y[0], y[-1])
        ax.set_zlim3d(z[0], z[-1])

        plt.savefig(opath+'/'+out_dir+'/B_lines_plot_3d_%s_%d.png'%(run_name, ind))
        plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",time[ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
