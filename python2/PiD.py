#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)


#------------------------------------------------------
#Intent
#------
print('\nWill check enropies\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
x = run.meta['x']
y = run.meta['y']
time = run.meta['time']*run.meta['tmult']

calc = fibo('calc')
calc.meta=run.meta

#params
g_iso = 1.
g_adiab = 5./3.
g_parl = 3.
g_perp = 2.

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

idtens = np.zeros((3,3,nx,ny,nz),dtype=np.float64)
for i in range(3):
    idtens[i,i] = 1.

#declare arrays
t_vec = []

m_gPiD_Sfp = []
m_nPiD_Sfp = []

m_gPiD_Sfe = []
m_nPiD_Sfe = []

m_nPiD_Sgp = []

if w_ele:
    m_nPiD_Sge = []

#------------------
smooth_flag = False
smooth_flag_post = True
gfs = 2
gfsp = 5

if smooth_flag:
    out_dir = 'PiD'+'/'+run_name+'_gauss_e'+str(gfs)+'p'+str(gfsp)
elif smooth_flag_post:
    out_dir = 'PiD'+'/'+run_name+'_gausspost_e'+str(gfs)+'p'+str(gfsp)
else:
    out_dir = 'PiD'+'/'+run_name

ml.create_path(opath+'/'+out_dir)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): B[i,:,:,0] = gf(B[i,:,:,0],[gfs,gfs],mode='wrap')

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)
    if code_name == 'iPIC' and (smooth_flag or smooth_flag_post):
        Psi = gf(Psi,[gfs,gfs],mode='wrap')

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
        J = np.array([cx,cy,cz]) # J = rot(B)
        del cx,cy,cz

    Bn = np.sqrt(B[0]**2+B[1]**2+B[2]**2) # Bn = |B|
    b = np.empty((3,nx,ny,nz),dtype=np.float64) # b = B/|B|
    for i in range(3):
        b[i] = np.divide(B[i],Bn)
    bb = np.empty((3,3,nx,ny,nz),dtype=np.float64) # bb = b (x) b
    for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
        bb[i,j] = b[i]*b[j]
    id_bb = np.empty((3,3,nx,ny,nz),dtype=np.float64) # id_bb = id - bb
    id_bb = idtens - bb

    #densities and currents
    n_p,u_p = run.get_Ion(ind)
    if code_name == 'iPIC' and smooth_flag:
        n_p[:,:,0] = gf(n_p[:,:,0],[gfs,gfs],mode='wrap')
        for i in range(3): u_p[i,:,:,0] = gf(u_p[i,:,:,0],[gfs,gfs],mode='wrap')
    if code_name == 'HVM':
        n_e = n_p.copy() # n_e = n_p = n (no charge separation)
        u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0],n_p)
        u_e[1] = u_p[1] - np.divide(J[1],n_p)
        u_e[2] = u_p[2] - np.divide(J[2],n_p)
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
        if smooth_flag:
            n_e[:,:,0] = gf(n_e[:,:,0],[gfs,gfs],mode='wrap')
            for i in range(3): u_e[i,:,:,0] = gf(u_e[i,:,:,0],[gfs,gfs],mode='wrap')
        J = n_p*u_p - n_e*u_e

    #Pressures
    Pp = run.get_Press(ind)
    if code_name == 'iPIC' and smooth_flag:
        for i in range(3): 
            for j in range(3): Pp[i,j,:,:,0] = gf(Pp[i,j,:,:,0],[gfs,gfs],mode='wrap')
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B) # pparlp = Pp:bb
                                              # pperpp = Pp:(id - bb)/2
    Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3. # Pisop = Tr(Pp)/3

    if not w_ele:
        Pisoe = n_e*run.meta['beta']*0.5*run.meta['teti'] # Pisoe = n_e * T_e,0
        Pe = np.zeros((3,3,nx,ny,nz),dtype=np.float64) # Pe = Pisoe*id
        for i in range(3):
            Pe[i,i] = Pisoe.copy()
        pparle = Pisoe # ppalre = Pisoe
        pperpe = pparle # pperpe = Pisoe
    else:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            pparle = tparle*n_e # pparle = n_e*tparle
            pperpe = tperpe*n_e # pperpe = n_e*tperpe
            Pisoe = (pparle + 2.*pperpe)/3. # Pisoe  = (pparle + 2*pperpe)/3
            Pe = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pe = pparle*bb + pperpe*(id - bb)
            for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
                Pe[i,j] = pparle*bb[i,j] + pperpe*id_bb[i,j]
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            if smooth_flag:
                for i in range(3):
                    for j in range(3): Pe[i,j,:,:,0] = gf(Pe[i,j,:,:,0],[gfs,gfs],mode='wrap')
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B) # pparle = Pe:bb
                                                      # pperpe = Pe:(id - bb)/2
            Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.  # Pisoe = Tr(Pe)/3

    Pnp = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pnp = Pp - [pparlp*bb + pperpp*(id - bb)]
    for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
        Pnp[i,j] = Pp[i,j] - (pparlp*bb[i,j] + pperpp*id_bb[i,j])
    if code_name == 'iPIC':
        Pne = np.empty((3,3,nx,ny,nz),dtype=np.float64) # Pne = Pe - [pparle*bb + pperpe*(id - bb)]
        for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
            Pne[i,j] = Pe[i,j] - (pparle*bb[i,j] + pperpe*id_bb[i,j])


    #-> pressure-strain
    #-> -> isotropic
    grad_u_p = np.empty((3,3,nx,ny,nz),dtype=np.float64) # grad(u_p)
    for i in range(3):
        grad_u_p[0,i] = calc.calc_gradx(u_p[i])
        grad_u_p[1,i] = calc.calc_grady(u_p[i])
        grad_u_p[2,i] = calc.calc_gradz(u_p[i])

    grad_u_e = np.empty((3,3,nx,ny,nz),dtype=np.float64) # grad(u_e)
    for i in range(3):
        grad_u_e[0,i] = calc.calc_gradx(u_e[i])
        grad_u_e[1,i] = calc.calc_grady(u_e[i])
        grad_u_e[2,i] = calc.calc_gradz(u_e[i])

    tmp = calc.calc_divr(u_p[0],u_p[1],u_p[2])
    strainp = np.empty((3,3,nx,ny,nz),dtype=np.float64) # strainp = 2*sym(grad(u_p)) - (2/3)*div(u_p)*id
    for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
        strainp[i,j] = grad_u_p[i,j] + grad_u_p[j,i] - (2./3.)*tmp*idtens[i,j]
    tmp = calc.calc_divr(u_e[0],u_e[1],u_e[2])
    straine = np.empty((3,3,nx,ny,nz),dtype=np.float64) # straine = 2*sym(grad(u_e)) - (2/3)*div(u_e)*id
    for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
        straine[i,j] = grad_u_e[i,j] + grad_u_e[j,i] - (2./3.)*tmp*idtens[i,j]
    del tmp

    gPiD_Sfp = np.zeros((nx,ny,nz),dtype=np.float64) # gPiD_Sfp = -(pparlp-pperpp)/(2*Pisop)*bb:strainp
    gPiD_Sfe = np.zeros((nx,ny,nz),dtype=np.float64) # gPiD_Sfe = -(pparle-pperpe)/(2*Pisoe)*bb:straine
    for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
        gPiD_Sfp += bb[i,j]*strainp[j,i]
        gPiD_Sfe += bb[i,j]*straine[j,i]
    gPiD_Sfp = gPiD_Sfp*(-0.5*np.divide(pparlp-pperpp,Pisop))
    gPiD_Sfe = gPiD_Sfe*(-0.5*np.divide(pparle-pperpe,Pisoe))

    nPiD_Sfp = np.zeros((nx,ny,nz),dtype=np.float64) # nPiD_Sfp = -(Pnp:strainp)/(2*Pisop)
    for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
        nPiD_Sfp += Pnp[i,j]*strainp[j,i]
    nPiD_Sfp = -0.5*np.divide(nPiD_Sfp,Pisop)

    nPiD_Sfe = np.zeros((nx,ny,nz),dtype=np.float64) # nPiD_Sfe = -(Pne:straine)/(2*Pisoe)
    if code_name == 'iPIC':
        for (i,j) in [(i,j) for i in range(3) for j in range(3)]:
            nPiD_Sfe += Pne[i,j]*straine[j,i]
        nPiD_Sfe = -0.5*np.divide(nPiD_Sfe,Pisoe)

    if code_name == 'iPIC' and smooth_flag_post:
        gPiD_Sfp[:,:,0] = gf(gPiD_Sfp[:,:,0],[gfsp,gfsp],mode='wrap')
        gPiD_Sfe[:,:,0] = gf(gPiD_Sfe[:,:,0],[gfs,gfs],mode='wrap')
        nPiD_Sfp[:,:,0] = gf(nPiD_Sfp[:,:,0],[gfsp,gfsp],mode='wrap')
        nPiD_Sfe[:,:,0] = gf(nPiD_Sfe[:,:,0],[gfs,gfs],mode='wrap')

    if run_name.split('_')[0] == 'DH':
        gPiD_Sfp = gPiD_Sfp*100.
        gPiD_Sfe = gPiD_Sfe*100.
        nPiD_Sfp = nPiD_Sfp*100.
        nPiD_Sfe = nPiD_Sfe*100.

    #-> -> anisotropic
    nPiD_Sgp = np.divide(Pisop*nPiD_Sfp,pperpp) # nPiD_Sgp = -(Pnp:strainp)/(2*pperpp)
    nPiD_Sge = np.divide(Pisoe*nPiD_Sfe,pperpe) # nPiD_Sge = -(Pne:straine)/(2*pperpe)

    if code_name == 'iPIC' and smooth_flag_post:
        nPiD_Sgp[:,:,0] = gf(nPiD_Sgp[:,:,0],[gfsp,gfsp],mode='wrap')
        nPiD_Sge[:,:,0] = gf(nPiD_Sge[:,:,0],[gfs,gfs],mode='wrap')

    #plot 2D
    plt.close()
    fig,ax = plt.subplots(3,2,figsize=(10*2,4*3))

    im00 = ax[0,0].contourf(x[ixmin:ixmax],y[iymin:iymax],gPiD_Sfp[ixmin:ixmax,iymin:iymax,0].T,32)
    plt.colorbar(im00,ax=ax[0,0])
    ax[0,0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
    ax[0,0].set_title('gPiD Sfp - '+run_name.replace('_',' ')+' - t='+str(time[ind]))

    im01 = ax[0,1].contourf(x[ixmin:ixmax],y[iymin:iymax],gPiD_Sfe[ixmin:ixmax,iymin:iymax,0].T,32)
    plt.colorbar(im01,ax=ax[0,1])
    ax[0,1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
    ax[0,1].set_title('gPiD Sfe')

    im10 = ax[1,0].contourf(x[ixmin:ixmax],y[iymin:iymax],nPiD_Sfp[ixmin:ixmax,iymin:iymax,0].T,32)
    plt.colorbar(im10,ax=ax[1,0])
    ax[1,0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
    ax[1,0].set_title('nPiD Sfp')

    im11 = ax[1,1].contourf(x[ixmin:ixmax],y[iymin:iymax],nPiD_Sfe[ixmin:ixmax,iymin:iymax,0].T,32)
    plt.colorbar(im11,ax=ax[1,1])
    ax[1,1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
    ax[1,1].set_title('nPiD Sfe')

    im20 = ax[2,0].contourf(x[ixmin:ixmax],y[iymin:iymax],nPiD_Sgp[ixmin:ixmax,iymin:iymax,0].T,32)
    plt.colorbar(im20,ax=ax[2,0])
    ax[2,0].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
    ax[2,0].set_title('nPiD Sgp')

    if w_ele:
        im21 = ax[2,1].contourf(x[ixmin:ixmax],y[iymin:iymax],nPiD_Sge[ixmin:ixmax,iymin:iymax,0].T,32)
        plt.colorbar(im21,ax=ax[2,1])
        ax[2,1].contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,8,cmap='inferno')
        ax[2,1].set_title('nPiD Sge')

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'2d_PiD_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #plot PDFs
    plt.close()
    fig,ax = plt.subplots(3,2,figsize=(10*2,4*3))

    h_,b_,im00 = ax[0,0].hist(np.array(gPiD_Sfp[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
    ax[0,0].set_title('gPiD Sfp - '+run_name.replace('_',' ')+' - t='+str(time[ind]))
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    ax[0,0].set_ylim(yL,yH)
    ax[0,0].set_xlim(xL,xH)

    h_,b_,im01 = ax[0,1].hist(np.array(gPiD_Sfe[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
    ax[0,1].set_title('gPiD Sfe')
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    ax[0,1].set_ylim(yL,yH)
    ax[0,1].set_xlim(xL,xH)

    h_,b_,im10 = ax[1,0].hist(np.array(nPiD_Sfp[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
    ax[1,0].set_title('nPiD Sfp')
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    ax[1,0].set_ylim(yL,yH)
    ax[1,0].set_xlim(xL,xH)

    h_,b_,im11 = ax[1,1].hist(np.array(nPiD_Sfe[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
    ax[1,1].set_title('nPiD Sfe')
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    ax[1,1].set_ylim(yL,yH)
    ax[1,1].set_xlim(xL,xH)

    h_,b_,im20 = ax[2,0].hist(np.array(nPiD_Sgp[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
    ax[2,0].set_title('nPiD Sgp')
    yL = 10#int(float(np.max(h_))/500.)
    yH = np.max(h_)
    xL = b_[np.argmax(h_>yL)]
    xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
    ax[2,0].set_ylim(yL,yH)
    ax[2,0].set_xlim(xL,xH)

    if w_ele:
        h_,b_,im21 = ax[2,1].hist(np.array(nPiD_Sge[ixmin:ixmax,iymin:iymax,0].flat),
                              bins=200,histtype='step',log=True)
        ax[2,1].set_title('nPiD Sge')
        yL = 10#int(float(np.max(h_))/500.)
        yH = np.max(h_)
        xL = b_[np.argmax(h_>yL)]
        xH = b_[len(h_)-1-np.argmax(h_[::-1]>yL)]
        ax[2,1].set_ylim(yL,yH)
        ax[2,1].set_xlim(xL,xH)

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'PDF_PiD_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #save means
    m_gPiD_Sfp.append(np.nanmean(gPiD_Sfp,dtype=np.float64))
    m_nPiD_Sfp.append(np.nanmean(nPiD_Sfp,dtype=np.float64))

    m_gPiD_Sfe.append(np.nanmean(gPiD_Sfe,dtype=np.float64))
    m_nPiD_Sfe.append(np.nanmean(nPiD_Sfe,dtype=np.float64))

    m_nPiD_Sgp.append(np.nanmean(nPiD_Sgp,dtype=np.float64))

    if w_ele:
        m_nPiD_Sge.append(np.nanmean(nPiD_Sge,dtype=np.float64))

    #save times
    t_vec.append(time[ind])

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
del gPiD_Sfp
del nPiD_Sfp

del gPiD_Sfe
del nPiD_Sfe

del nPiD_Sgp

if w_ele:
    del nPiD_Sge

#from list to numpy.array
t_vec = np.array(t_vec)

m_gPiD_Sfp = np.array(m_gPiD_Sfp)
m_nPiD_Sfp = np.array(m_nPiD_Sfp)

m_gPiD_Sfe = np.array(m_gPiD_Sfe)
m_nPiD_Sfe = np.array(m_nPiD_Sfe)

m_nPiD_Sgp = np.array(m_nPiD_Sgp)

if w_ele:
    m_nPiD_Sge = np.array(m_nPiD_Sge)

#plot
plt.close('all')
fig,ax = plt.subplots(1,1,figsize=(4*2,4*2))

ax.plot(t_vec,m_gPiD_Sfp,'-r',label='gPiD fp')
ax.plot(t_vec,m_nPiD_Sfp,'--r',label='nPiD fp')
ax.plot(t_vec,m_gPiD_Sfe,'-b',label='gPiD fe')
ax.plot(t_vec,m_nPiD_Sfe,'--b',label='nPiD fe')
ax.plot(t_vec,m_nPiD_Sgp,'--g',label='nPiD gp')
if w_ele:
    ax.plot(t_vec,m_nPiD_Sge,'--k',label='nPiD ge')
ax.legend()
ax.set_xlabel('$t\quad [\Omega_c^{-1}]$')
ax.set_ylabel('PiD')
ax.set_title('pressure-strain - '+run_name.replace('_',' '))

plt.tight_layout()
#plt.show()
plt.savefig(opath+'/'+out_dir+'/'+'m_PiD_'+run_name+'_'+str(ind1)+'_'+str(ind2)+'.png')
plt.close()
