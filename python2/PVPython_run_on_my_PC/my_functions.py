#imports
import re
#from paraview.simple import *

# data dicnioray key generation
def d_key(v='NG',t='NG'):
    """
    generate a string to be used as a key in data dictionary

    INPUTS:
    v -> str, default='NG', var_name
    t -> str/int/float, default='NG', time

    OUTPUT:
    _ -> str, '<var_name>_<time>', var_name is given with %s, time is given with %s OR %d OR %08.3f
    
    ERRORS:
    'UT' -> given as var_name or time if wrong type is given
    """
    if type(v) != str:
        v = 'UT'
    if type(t) == str:
        return '%s_%s'%(v,t)
    elif type(t) == int:
        return '%s_%d'%(v,t)
    elif type(t) == float:
        return '%s_%08.3f'%(v,t)
    else:
        return '%s_UT'%(v)

# elimitates special char from strings
def sub_func(in_str):
    """
    removes '[()!@#$.,*^]' from strings

    INPUTS:
    in_str -> str

    OUTPUTS:
    _ -> str

    ERRORS:
    'NOT_A_STRING' -> returned if wrong type is given
    """
    if type(in_str)!=str:
        return 'NOT_A_STRING'
    return re.sub('[()!@#$.,*^]','',in_str)

# dictionary key generation, time only
def t_key(t='NG'):
    """
    generate a string to be used as a key in data dictionary

    INPUTS:
    t -> str/int/float, default='NG', time

    OUTPUT:
    _ -> str, '<time>', time is given with %s OR %d OR %08.3f
    
    ERRORS:
    'UT' -> given as time if wrong type is given
    """
    if type(t) == str:
        return '%s'%(t)
    elif type(t) == int:
        return '%d'%(t)
    elif type(t) == float:
        return '%08.3f'%(t)
    else:
        return 'UT'

# create and set Display
def set_Display(Attribute,renderView,v,t,Representation='Outline',ColorByProperty='Magnitude'):
    # show data in view
    D = Show(Attribute,renderView,'UniformGridRepresentation')

    # variable name
    var_name = sub_func(d_key(v,t))

    # trace defaults for the display properties.
    D.Representation = Representation
    D.ColorArrayName = [None, '']
    D.OSPRayScaleArray = var_name
    D.OSPRayScaleFunction = 'PiecewiseFunction'
    D.SelectOrientationVectors = var_name
    D.ScaleFactor = 5.0085962877195005
    D.SelectScaleArray = var_name
    D.GlyphType = 'Arrow'
    D.GlyphTableIndexArray = var_name
    D.GaussianRadius = 0.250429814385975
    D.SetScaleArray = ['POINTS', var_name]
    D.ScaleTransferFunction = 'PiecewiseFunction'
    D.OpacityArray = ['POINTS', var_name]
    D.OpacityTransferFunction = 'PiecewiseFunction'
    D.DataAxesGrid = 'GridAxesRepresentation'
    D.PolarAxes = 'PolarAxesRepresentation'
    D.ScalarOpacityUnitDistance = 0.3417875960252012
    D.SliceFunction = 'Plane'
    D.Slice = 139

    # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
    D.ScaleTransferFunction.Points = [-0.006755399983376265, 0.0, 0.5, 0.0, 0.007502499967813492, 1.0, 0.5, 0.0]

    # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
    D.OpacityTransferFunction.Points = [-0.006755399983376265, 0.0, 0.5, 0.0, 0.007502499967813492, 1.0, 0.5, 0.0]

    # init the 'Plane' selected for 'SliceFunction'
    D.SliceFunction.Origin = [25.0429814385975, 12.1175717083875, 25.0429814385975]

    # reset view to fit data
    renderView.ResetCamera()

    # update the view to ensure updated data information
    renderView.Update()

    # get color transfer function/color map for 'ue'
    ueLUT = GetColorTransferFunction(var_name)

    # get opacity transfer function/opacity map for 'ue'
    uePWF = GetOpacityTransferFunction(var_name)

    # set scalar coloring
    ColorBy(D, ('POINTS', var_name, ColorByProperty))

    # rescale color and/or opacity maps used to exactly fit the current data range
    D.RescaleTransferFunctionToDataRange(True, False)

    # Update a scalar bar component title.
    UpdateScalarBarsComponentTitle(ueLUT, D)

    # show color bar/color legend
    D.SetScalarBarVisibility(renderView, True)

    return D

#create and destroy sphere to waste time
def Sphere_cycle():
    # create a new 'Sphere'
    sphere1 = Sphere()

    # show data in view
    sphere1Display = Show(sphere1, renderView1, 'GeometryRepresentation')

    # trace defaults for the display properties.
    sphere1Display.Representation = 'Surface'
    sphere1Display.ColorArrayName = [None, '']
    sphere1Display.OSPRayScaleArray = 'Normals'
    sphere1Display.OSPRayScaleFunction = 'PiecewiseFunction'
    sphere1Display.SelectOrientationVectors = 'None'
    sphere1Display.ScaleFactor = 0.1
    sphere1Display.SelectScaleArray = 'None'
    sphere1Display.GlyphType = 'Arrow'
    sphere1Display.GlyphTableIndexArray = 'None'
    sphere1Display.GaussianRadius = 0.005
    sphere1Display.SetScaleArray = ['POINTS', 'Normals']
    sphere1Display.ScaleTransferFunction = 'PiecewiseFunction'
    sphere1Display.OpacityArray = ['POINTS', 'Normals']
    sphere1Display.OpacityTransferFunction = 'PiecewiseFunction'
    sphere1Display.DataAxesGrid = 'GridAxesRepresentation'
    sphere1Display.PolarAxes = 'PolarAxesRepresentation'

    # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
    sphere1Display.ScaleTransferFunction.Points = [-0.9965844750404358, 0.0, 0.5, 0.0, 0.9965844750404358, 1.0, 0.5, 0.0]

    # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
    sphere1Display.OpacityTransferFunction.Points = [-0.9965844750404358, 0.0, 0.5, 0.0, 0.9965844750404358, 1.0, 0.5, 0.0]

    # reset view to fit data
    renderView1.ResetCamera()

    # get the material library
    materialLibrary1 = GetMaterialLibrary()

    # update the view to ensure updated data information
    renderView1.Update()

    # set scalar coloring
    ColorBy(sphere1Display, ('POINTS', 'Normals', 'Magnitude'))

    # rescale color and/or opacity maps used to include current data range
    sphere1Display.RescaleTransferFunctionToDataRange(True, False)

    # show color bar/color legend
    sphere1Display.SetScalarBarVisibility(renderView1, False)

    # get color transfer function/color map for 'Normals'
    normalsLUT = GetColorTransferFunction('Normals')

    # get opacity transfer function/opacity map for 'Normals'
    normalsPWF = GetOpacityTransferFunction('Normals')

    time.sleep(1)

    # destroy sphere1
    Delete(sphere1)
    del sphere1
