# script for paraview version 5.8.1
#
# To ensure correct image size when batch processing, please 
# use `renderView*.ViewSize = [*,*]`

#### general imports
import os
import sys
import time

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

#ssh coord
workpath = '/home/frafin/Documents/LF_3d_DH'
server_workpath = '/home/finelli/Documents'
server_script = '2vtk_timeloop.py'

uname = 'finelli'
hname = 'plasmi1.df.unipi.it'

fname_c2c = 'control2client.txt'
fname_c2s = 'control2server.txt'

fname_cbuffer = 'client_buffer.txt'
fname_sbuffer = 'server_buffer.txt'

STOP_MSG = 'STOP'
WAITING_TIME = 0 #seconds

CLIENT_TAG = 'client'
SERVER_TAG = 'server'

dots = ['   ','.  ','.. ','...']

#start server
cmd = "gnome-terminal -- sh -c \"ssh -t %s@%s 'python %s/%s;bash'\""%(uname,hname,
                                                                 server_workpath,server_script)
os.system(cmd)

#make control file
f = open('%s/%s'%(workpath,fname_c2c),'w')
f.write(SERVER_TAG)
f.close()

# set environment
vtk_common_path = '/work1/finelli/LF_3d_DH/vtk/calc_'
screenshot_path = '/home/frafin/Documents/LF_3d_DH/screenshots'
functions_path = '/home/frafin/Documents/LF_3d_DH/my_functions.py'

if not os.path.exists(screenshot_path):
    os.makedirs(screenshot_path)

# read functions file
exec(open(functions_path).read())

# select variables
var_list = ['ui','n','J']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1529, 548]

# current camera placement for renderView1
renderView1.CameraPosition = [153.73197385666495, -30.61331037614026, 75.33890386388265]
renderView1.CameraFocalPoint = [25.04298210144042, 12.117571830749508, 25.042982101440412]
renderView1.CameraViewUp = [-0.0021878606654061605, 0.759322755840431, 0.6507105083895618]
renderView1.CameraParallelScale = 37.431770625225646

# get layout
layout1 = GetLayout()

#main loop
run_flag = True
while run_flag:
    #wait for get control back
    wait_flag = True
    ##cnt = 0
    while wait_flag:
        os.system('scp -q %s@%s:%s/%s %s/%s'%(uname,hname,server_workpath,fname_c2s,workpath,fname_c2c))
        f = open('%s/%s'%(workpath,fname_c2c),'r')
        tag = f.readline()
        f.close()
        if tag == CLIENT_TAG:
            wait_flag = False
        else:
            ##print('waiting for control%s'%(dots[cnt%4]),end='\r')
            ##sys.stdout.flush()
            ##cnt += 1
            ##time.sleep(WAITING_TIME)
            Sphere_cycle()
    print('')

    #read file content
    os.system('scp -q %s@%s:%s/%s %s/%s'%(uname,hname,server_workpath,fname_sbuffer,
                                                      workpath,fname_cbuffer))
    f = open('%s/%s'%(workpath,fname_cbuffer))
    l = f.readline()
    f.close()
    if l == STOP_MSG:
        run_flag = False
        continue
    exec(l)
    print('time_list=',time_list)

    # crete data dict.
    data = {}

    for v,t in [(v,t) for v in var_list for t in time_list]:
        data[d_key(v,t)] = {}

    # TIME LOOP
    for t in time_list:
        # create 'Legacy VTK Readers', i.e. load vtk data
        for v in var_list:
            d_key_tmp = d_key(v,t)
            d_key_sub = sub_func(d_key_tmp)
            data[d_key_tmp]['LegacyVTKReader'] = LegacyVTKReader(FileNames=[vtk_common_path+
                                                                            d_key_sub+'.vtk'])

        # create an 'Append Attributes' for each time step
        time_step = AppendAttributes(Input=[data[d_key(v,t)]['LegacyVTKReader'] for v in var_list])

        # create a new 'Calculator' for each time
        d_key_tmp = d_key('ue',t)
        data[d_key_tmp] = {}
        data[d_key_tmp]['Calculator'] = Calculator(Input=time_step[t_key(t)])
        data[d_key_tmp]['Calculator'].ResultArrayName = sub_func(d_key_tmp)
        data[d_key_tmp]['Calculator'].Function = ( sub_func(d_key('ui',t)) + ' - ' +
                                                   sub_func(d_key('J',t)) + ' / ' + 
                                                   sub_func(d_key('n',t)) )

        # display something
        ueZDisplay = set_Display(data[d_key('ue',t)]['Calculator'],renderView1,'ue',t,
                                 Representation='Surface',ColorByProperty='Z')

        # save screenshot
        SaveScreenshot(screenshot_path+'/'+sub_func(d_key('ue',t))+'_'+'Z'+'.png', 
                                                renderView1, ImageResolution=[1529, 548])
        
        # hide data in view
        Hide(data[d_key('ue',t)]['Calculator'], renderView1)

        # destroy calculator
        Delete(data[d_key('ue',t)]['Calculator'])
        del data[d_key('ue',t)]['Calculator']

        # destroy appendAttributes
        Delete(time_step)
        del time_step

        # destroy legacyVTKReader
        for v in var_list:
            Delete(data[d_key(v,t)]['LegacyVTKReader'])
            del data[d_key(v,t)]['LegacyVTKReader']
    
    #give control to server
    f = open('%s/%s'%(workpath,fname_c2c),'w')
    f.write(SERVER_TAG)
    f.close()
    os.system('scp -q %s/%s %s@%s:%s/%s'%(workpath,fname_c2c,uname,hname,server_workpath,fname_c2s))

print('\nDone!')
