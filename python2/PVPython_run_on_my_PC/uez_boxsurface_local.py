# script for paraview version 5.8.1
#
# To ensure correct image size when batch processing, please 
# use `renderView*.ViewSize = [*,*]`

#### general imports
# other imports here

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# set environment
vtk_common_path = '/work1/finelli/LF_3d_DH/vtk/calc_'
screenshot_path = '/home/frafin/Documents/LF_3d_DH/screenshots'
functions_path = '/home/frafin/Documents/LF_3d_DH/my_functions.py'

# read functions file
exec(open(functions_path).read())

# select variables and times
var_list = ['ui','n','J']
time_list = [0.00,2.25,4.50,6.75]

# crete data dict.
data = {}

for v,t in [(v,t) for v in var_list for t in time_list]:
    data[d_key(v,t)] = {}

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1529, 548]

# current camera placement for renderView1
renderView1.CameraPosition = [153.73197385666495, -30.61331037614026, 75.33890386388265]
renderView1.CameraFocalPoint = [25.04298210144042, 12.117571830749508, 25.042982101440412]
renderView1.CameraViewUp = [-0.0021878606654061605, 0.759322755840431, 0.6507105083895618]
renderView1.CameraParallelScale = 37.431770625225646

# get layout
layout1 = GetLayout()

# TIME LOOP
for t in time_list:
    # create 'Legacy VTK Readers', i.e. load vtk data
    for v in var_list:
        d_key_tmp = d_key(v,t)
        d_key_sub = sub_func(d_key_tmp)
        data[d_key_tmp]['LegacyVTKReader'] = LegacyVTKReader(FileNames=[vtk_common_path+d_key_sub+'.vtk'])

    # create an 'Append Attributes' for each time step
    time_step = AppendAttributes(Input=[data[d_key(v,t)]['LegacyVTKReader'] for v in var_list])

    # create a new 'Calculator' for each time
    d_key_tmp = d_key('ue',t)
    data[d_key_tmp] = {}
    data[d_key_tmp]['Calculator'] = Calculator(Input=time_step[t_key(t)])
    data[d_key_tmp]['Calculator'].ResultArrayName = sub_func(d_key_tmp)
    data[d_key_tmp]['Calculator'].Function = ( sub_func(d_key('ui',t)) + ' - ' +
                                               sub_func(d_key('J',t)) + ' / ' + 
                                               sub_func(d_key('n',t)) )

    # display something
    ueZDisplay = set_Display(data[d_key('ue',t)]['Calculator'],renderView1,'ue',t,Representation='Surface',ColorByProperty='Z')

    # save screenshot
    SaveScreenshot(screenshot_path+sub_func(d_key('ue',t))+'_'+'Z'+'.png', 
                                            renderView1, ImageResolution=[1529, 548])
    
    # hide data in view
    Hide(data[d_key('ue',t)]['Calculator'], renderView1)

    # destroy calculator
    Delete(data[d_key('ue',t)]['Calculator'])
    del data[d_key('ue',t)]['Calculator']

    # destroy appendAttributes
    Delete(time_step)
    del time_step

    # destroy legacyVTKReader
    for v in var_list:
        Delete(data[d_key(v,t)]['LegacyVTKReader'])
        del data[d_key(v,t)]['LegacyVTKReader']

