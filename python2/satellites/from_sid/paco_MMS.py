"""
code for python3

given some time interval, 
extracts MMS data with pyspedas

date:    07.may.21
author:  sid 
"""
#--------
# imports
import sys
# import time 
# import datetime
# import pickle
import os 
import copy
import shutil

import numpy as np
import scipy.constants
# import matplotlib.pyplot as plt
import astropy.time as apt
# import astropy.units as apu

# sys.path.append('/home/fadanelli/CEFLIB/PYTHON') 
# sys.path.append('/home/fadanelli/fibo_MMS')
sys.path.append('/home/fadanelli/SCL')
# sys.path.append('/home/fadanelli')
# sys.path.append('/home/fadanelli/python_environment/lib_locale/CEFLIB2/PYTHON') 
# sys.path.append('/home/fadanelli/python_environment/lib_locale/CEFLIB3/PYTHON')
import SCL as scl

#---------------
# init constants
cst_e = scipy.constants.e * 1e19 #1.6021766208              #elementary charge, in Coulomb * [10**-19]
cst_c = scipy.constants.c * 1e-8 #2.997924580               #speed of light in vacuum, in m/s * [10**8]
cst_me = scipy.constants.m_e * 1e31 #9.10938356             #electron mass, in kg * [10**-31]
cst_mi = scipy.constants.m_p * 1e31                         #ion mass, in kg * [10**-31]
cst_ep0 = scipy.constants.epsilon_0 * 1e12 #8.85418781762   #electric permittivity of vacuum, in [10**-12]
cst_mu0 = scipy.constants.mu_0 * 1e7  #12.566370            #magnetic permittivity of vacuum, in m^2 T^2 N^-2 * [10**-7]

#--------------------
# initial information
address = '/home/finelli/satellites/'
event = '16-oct-15'

ref_frame = 'gse' 
res_level = 'brst'
prd_level = 'l2'
var_list = ['r', 'B', 'ue', 'ui', 'ne', 'ni', 'Pe', 'Pi','Qi','Qe']
sat_list = ['1', '2', '3', '4']

# to interpolate: reference variable and reference satellite
ref_var, ref_sat = 'ue', 'SC1'  # ???

# to perform running average: half width and step of the moving window 
# (0,1 means no averaging)
ave_kwargs = {'hawi':0,'step':1}

# here you pass from the list of variables to load to a dictionary of to-load
exec(open(address+'translator_pys.py').read())

# get the beginning and end of times of every time segment you are considering
timeints = open(address+event+'.txt').read().split('\n')

for timeint_num in range(len(timeints)): 
  timeints[timeint_num] = timeints[timeint_num].split('/')
  timeints[timeint_num] = apt.Time(timeints[timeint_num]) #, format='isot', scale='utc') 

#----------
for timeint_num in range(0,len(timeints)):  # i have chosen this syntax rather
                                            # than enumerate to make re-runs easier
    loader = scl.from_spedas(timeints[timeint_num],sat_list=sat_list,var_dict=translator)

    #----------
    # here you load all in a dictionary called raw
    raw = {}
    args_load = {'data_rate':res_level, 'time_clip':True}

    loader.load_mec(var_list,raw,args_load.copy())
    loader.load_fgm(var_list,raw,args_load.copy())
    loader.load_scm(var_list,raw,args_load.copy())
    loader.load_edp(var_list,raw,args_load.copy())
    loader.load_dsp(var_list,raw,args_load.copy())
    loader.load_fpi(var_list,raw,args_load.copy())

    # all the previous lines can be also written as
    #exec("loader.load_"+ins+"(tar_vars=tar_files, **load_request)")


    #-------------
    # henceforth I will need only scalar pressure, hence .. 

    for sat in sat_list: 
        raw['Pi_SC'+sat].data['|Pi|_SC'+sat] = (raw['Pi_SC'+sat].data['Pi_SC'+sat][:,0,0] + raw['Pi_SC'+sat].data['Pi_SC'+sat][:,1,1] + raw['Pi_SC'+sat].data['Pi_SC'+sat][:,2,2])/3.
        raw['Pe_SC'+sat].data['|Pe|_SC'+sat] = (raw['Pe_SC'+sat].data['Pe_SC'+sat][:,0,0] + raw['Pe_SC'+sat].data['Pe_SC'+sat][:,1,1] + raw['Pe_SC'+sat].data['Pe_SC'+sat][:,2,2])/3.
        
        #raw['Pi_SC'+sat].data['|Pi|_SC'+sat] = np.reshape( raw['Pi_SC'+sat].data['|Pi|_SC'+sat] , (len(raw['Pi_SC'+sat].data['|Pi|_SC'+sat]),) )
        #raw['Pe_SC'+sat].data['|Pe|_SC'+sat] = np.reshape( raw['Pe_SC'+sat].data['|Pe|_SC'+sat] , (len(raw['Pe_SC'+sat].data['|Pe|_SC'+sat]),) )
        
        del(raw['Pi_SC'+sat].data['Pi_SC'+sat])
        del(raw['Pe_SC'+sat].data['Pe_SC'+sat])

        del(raw['Pi_SC'+sat].meta['fields']['Pi_SC'+sat])
        del(raw['Pe_SC'+sat].meta['fields']['Pe_SC'+sat])

    # by the way, now that you have all your stuff inside raw you can delete some useless data files
    shutil.rmtree(os.getcwd()+'/pydata') 

    #-------------
    # here you interpolate over one epoch only

    ref_name = ref_var+'_'+ref_sat

    int_toto = scl.toto('int')
    int_toto.meta['epoch'] = raw[ref_name].meta['epoch']
    int_toto.meta['fields'] = {}

    for tar_name in raw.keys() :
        for var in raw[tar_name].data.keys() :
            int_toto.data[var] = raw[tar_name].make_t_int(var, int_toto.meta['epoch'])
        int_toto.meta['fields'].update(raw[tar_name].meta['fields'])

    del(raw)

    # you micht check for nan entries here (it is easier to do now that all data are in int_toto)

    #-------------
    # here you might go for time averages
    ave_toto = scl.toto(timeints[timeint_num])
    ave_toto.meta['fields'] = int_toto.meta['fields']

    for var in int_toto.data.keys() : 
        ave_toto.data[var] = int_toto.make_t_ave(var,**ave_kwargs)
        ave_toto.data[var] = ave_toto.set_shape(var,'np-ten')

    ave_toto.meta['epoch'] = int_toto.make_t_ave_epoch(**ave_kwargs)

    del(int_toto)


"""
  #-------------
  # here you calculate stuff ... as in this example 
  absc_size = ave_toto.meta['epoch'].shape[0]
  
  # calculate SC conf quantities and the gradient of B
  inv_SC, sep_SC = ave_toto.calc_conf('r',True,True)
  
  
  ave_toto.data['sep_SCa'] = np.sqrt( np.average( np.sum( np.square(sep_SC) ,axis=2), axis=0) ) 
  ave_toto.data['gB_SCa'] = ave_toto.calc_grad(['B_SC'+sat for sat in sat_list],inv_SC)
  
  del(inv_SC)
  del(sep_SC)

  # calculate the 4-spacecraft averages of some other quantities 
  tar_vars=['B', 'ne', 'ni', '|Pe|', '|Pi|']
  
  for nom in tar_vars: 
    ave_toto.data[nom+'_SCa'] = np.zeros(np.shape(ave_toto.data[nom+'_SC1'])) 
    for sat in sat_list:
      ave_toto.data[nom+'_SCa'] += np.multiply(0.25, ave_toto.data[nom+'_SC'+sat])
    #int_toto._varnames = np.append(int_toto._varnames,np.array([nom+'_SCa']))
  
  
  # delete single-SC values for quantities you have SC-averaged
  for s in range(1,5):
    for nom in tar_vars: 
      del(ave_toto.data[nom+'_SC'+str(s)])
    #for nom in tar_vars[:3]: 
    #  del(ave_toto.meta['fields'][nom+'_SC'+str(s)])

  # the square norm of B_SCa 
  ave_toto.data['||B||_SCa'] = np.sum( np.square(ave_toto.data['B_SCa']), axis=1)

  # from B gradient calculate J, MCA eigenvalues
  ave_toto.data['J_SCa'] = np.zeros([absc_size,3], dtype=float)
  ave_toto.data['MCA_lengths'] = np.zeros([absc_size,3], dtype=float)
  ave_toto.data['MCA_min_var'] = np.zeros([absc_size,3], dtype=float)

  vecs = np.zeros([3,3], dtype=float)
  tens = np.zeros([3,3], dtype=float)

  to_del = [] # list of rows to be deleted 'cause eigh did not work

  for k in range(absc_size):
    
    grad = ave_toto.get_data('gB_SCa')[k,:,:]
    
    ave_toto.data['J_SCa'][k,0] = grad[2,1] - grad[1,2]
    ave_toto.data['J_SCa'][k,0] = grad[0,2] - grad[2,0]
    ave_toto.data['J_SCa'][k,0] = grad[1,0] - grad[0,1]
    
    tens[:,:]  = np.dot(grad,np.transpose(grad)) 
    tens[:,:] /= ave_toto.data['||B||_SCa'][k]
    
    try: 
      ave_toto.data['MCA_lengths'][k,:], vecs = np.linalg.eigh(tens[:,:])
      ave_toto.data['MCA_min_var'][k,:] = vecs[:,0]
    
    except: 
      to_del.insert(0,k) #note that rows to be deleted are listed from max to min
  
  if len(to_del) is not 0:  
    
    absc_size -= len(to_del)
    
    for k in range(len(to_del)): 
      kk = to_del[k] #this way it is possible to delete all the cort rows!
      
      for var in ave_toto.data.keys(): 
        np.delete(ave_toto.data[var],kk,axis=0)
      np.delete(ave_toto.meta['epoch'],kk)
      

  del(ave_toto.data['gB_SCa'])
  for sat in sat_list : 
    del(ave_toto.data['r_SC'+sat])

  ave_toto.data['J_SCa'] *= 1e1 / cst_mu0 
  ave_toto.data['MCA_lengths'][ave_toto.data['MCA_lengths'] == np.nan] = 0.000000000000000000000001
  ave_toto.data['MCA_lengths'][ave_toto.data['MCA_lengths'] == 0.0]    = 0.000000000000000000000001
  ave_toto.data['MCA_lengths'] = np.sqrt( np.iprocal( ave_toto.data['MCA_lengths'] ))

  # then MCA shape factors: Planarity and Elongation 
  ave_toto.data['ShaPE'] = np.zeros([absc_size,2])
  ave_toto.data['ShaPE'][:,0] = 1. - (ave_toto.data['MCA_lengths'][:,2]/ave_toto.data['MCA_lengths'][:,1])  #Planarity
  ave_toto.data['ShaPE'][:,1] = 1. - (ave_toto.data['MCA_lengths'][:,1]/ave_toto.data['MCA_lengths'][:,0])  #Elongation
  
  
  # find ion+ele skin depth
  ave_toto.data['skin_ele_SCa'] = np.sqrt(1e-1 * cst_me * cst_ep0 /  np.reshape(ave_toto.data['ne_SCa'],(absc_size)) )
  ave_toto.data['skin_ele_SCa'] = ave_toto.data['skin_ele_SCa'] * cst_c / cst_e 
  
  ave_toto.data['skin_ion_SCa'] = np.sqrt(1e-1 * cst_mi * cst_ep0 /  np.reshape(ave_toto.data['ni_SCa'],(absc_size)) )
  ave_toto.data['skin_ion_SCa'] = ave_toto.data['skin_ion_SCa'] * cst_c / cst_e 

  # find beta_ion and beta_ele
  ave_toto.data['beta_ion_SCa'] = 2 * 1e2 * cst_mu0 * ave_toto.data['|Pi|_SCa'] / ave_toto.data['||B||_SCa']
  ave_toto.data['beta_ele_SCa'] = 2 * 1e2 * cst_mu0 * ave_toto.data['|Pe|_SCa'] / ave_toto.data['||B||_SCa']
  
  # error calculations: maximum detectable variation length
  ave_toto.data['RES_length'] =  ave_toto.data['sep_SCa'] * np.sqrt( ave_toto.data['||B||_SCa'] ) / ERR_B

  selector_min = (ave_toto.data['RES_length'] > ave_toto.data['MCA_lengths'][:,0])
  selector_med = (ave_toto.data['RES_length'] > ave_toto.data['MCA_lengths'][:,1])
  selector_max = (ave_toto.data['RES_length'] > ave_toto.data['MCA_lengths'][:,2])
  
  absc_max = np.sum(selector_max)
  absc_med = np.sum(selector_med)
  absc_min = np.sum(selector_min)
  
  max_perc = '%.2f' %(absc_max/float(absc_size))
  med_perc = '%.2f' %(absc_med/float(absc_size))
  min_perc = '%.2f' %(absc_min/float(absc_size))
  
  
  del(ave_toto.data['|Pe|_SCa'])
  del(ave_toto.data['|Pi|_SCa'])
  del(ave_toto.data['ne_SCa'])
  del(ave_toto.data['ni_SCa'])
  del(ave_toto.data['||B||_SCa'])
  
  

  
  # let's now update all stuff into the database
  with open(address+'/data_files/'+event+'_interval-'+str(timeint_num).zfill(4)+'.npsave', 'wb') as f: pickle.dump(ave_toto, f) 
  f.close()
  
  del(ave_toto)
 


"""
