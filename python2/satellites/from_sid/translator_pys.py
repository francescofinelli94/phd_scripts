translator = {
    'r'  : 'mec_r_'+ref_frame ,
    'B'  : 'fgm_b_'+ref_frame ,
    'E'  : 'edp_dce_'+ref_frame ,
    'ue' : 'des_bulkv_'+ref_frame ,
    'ui' : 'dis_bulkv_'+ref_frame ,
    'ne' : 'des_numberdensity' ,
    'ni' : 'dis_numberdensity' ,
    'Pe' : 'des_prestensor_'+ref_frame , 
    'Pi' : 'dis_prestensor_'+ref_frame ,
    'Qi' : 'dis_heatq_'+ref_frame ,
    'Qe' : 'des_heatq_'+ref_frame
}
