"""
Let's try loading some magnetic field data from MMS
"""
# imports
from matplotlib import use as mpl_use
mpl_use('Agg')
import matplotlib.pyplot as plt  # because I want
from pyspedas.mms import mms_load_fgm  # load data from the FGM onboard  of MMS
from pyspedas import tnames  # to see what we get
from pyspedas import time_string, time_double  # change time representation
from pytplot import get_data  # get data on arrays


# hardcoded inputs
t_start = '2015-12-8/11:20:10'
t_end = '2015-12-8/11:21:00'
probe = 1  # which of the MMS satellite
data_rate = 'srvy'  # sampling rate
level = 'l2'  # should be "masaged" 
coord = 'gsm'  # coordinate system

# load data
varnames = ['mms%d_fgm_b_%s_%s_%s'%(probe, coord, data_rate, level)]
fgm_vars = mms_load_fgm( trange=[t_start, t_end],
                         probe=probe, data_rate=data_rate, level=level,
                         # varnames=varnames  # this raises warnings and errors...
                       )

# get data
t, B = get_data(varnames[0])
B = B[:,:4].T  # I do not need the magnitude and prefer B = [Bx, By, Bz]
print(time_string(t[0]))
print(time_string(t[1]))

# plot the magnetic field
for i in range(3):
    plt.plot(t,B[i],label='B%d'%i)

plt.legend()
plt.savefig('B.png')
