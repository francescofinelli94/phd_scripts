data = {}
for it in [0, 1]:
    data[it] = {}
    data[it]['trange'] = 'trange %d'%it
    for probe in [1, 2]:
        data[it]['mms%d'%probe] = {}
        for field in ['b_gse', 'b_gsm']:
            data[it]['mms%d'%probe][field] = {}
            data[it]['mms%d'%probe][field]['epoch'] = 'epoch %d %d %s'%(it, probe, field)
            data[it]['mms%d'%probe][field]['field'] = 'field %d %d %s'%(it, probe, field)

def show_data_tree( data, name='data structure', head='|-->',
                    spacer=' ', head_len=None ):
    """
    A functon which plot the structure of a given
    dictionary in a quite readable way.

    INPUTS:
    data     -> dict or dict-like, a dictionaty
    name     -> str or None, optional, name to be shown
                in the beginning, default is 'data structure'
    head     -> string, optional, a set of charachter preposed to
                the keys, defaultis '|-->'
    spacer   -> str, optional, characher used to make levels
                indentations, default is ' '
    head_len -> None or int, optional, the lenght of head,
                default is None
    """
    # check header situation
    if head_len is None:
        head_len = len(head)
    pre_head = head[0]
    for _ in range(head_len - 1):
        pre_head = pre_head + spacer
    # printing stuff
    if name is not None:
        print(name)
    for k0 in data:
        print('%s%s'%(head,k0))
        if isinstance(data[k0], dict):
            show_data_tree( data[k0], name=None,
                            head='%s%s'%(pre_head, head),
                            head_len=head_len )
            print(head[:-(head_len - 1)])

show_data_tree(data)
