"""
Let's try loading some magnetic field data from MMS
"""
from os import getcwd as os_getcwd
from os.path import join as os_path_join
from shutil import rmtree as su_rmtree

import astropy.time as apt  # unix time conversion
from pyspedas.mms import mms_load_fgm  # to download MMS.FGM data
# from pyspedas import tnames  # to see what we get
from pytplot import get_data as ptp_get_data  # to get data
from pytplot import del_data as ptp_del_data  # delete tplot data
from matplotlib import use as mpl_use
mpl_use('Agg')
import matplotlib.pyplot as plt

# functions
# - just a little function to se the structure of data dict
def show_data_tree( data, name='data structure', head='|-->',
                    spacer=' ', head_len=None ):
    """
    A functon which plot the structure of a given
    dictionary in a quite readable way.

    INPUTS:
    data     -> dict or dict-like, a dictionaty
    name     -> str or None, optional, name to be shown
                in the beginning, default is 'data structure'
    head     -> string, optional, a set of charachter preposed to
                the keys, defaultis '|-->'
    spacer   -> str, optional, characher used to make levels
                indentations, default is ' '
    head_len -> None or int, optional, the lenght of head,
                default is None
    """
    # check header situation
    if head_len is None:
        head_len = len(head)
    pre_head = head[0]
    for _ in range(head_len - 1):
        pre_head = pre_head + spacer
    # printing stuff
    if name is not None:
        print(name)
    for k0 in data:
        print('%s%s'%(head,k0))
        if isinstance(data[k0], dict):
            show_data_tree( data[k0], name=None,
                            head='%s%s'%(pre_head, head),
                            head_len=head_len )
            print(head[:-(head_len - 1)])

# inputs
# - time ranges
tranges = [ '2015-10-16T13:06:58.000Z/2015-10-16T13:07:03.000Z',
            '2015-12-8T11:20:10.000Z/2015-12-8T11:21:00.000Z' ]
# - which probe(s)
probes = [1,2]
# - rate of data
data_rate = 'brst'
# - always True, I guess...
time_clip = True
# - which fields
fields = ['b_gse','b_gsm']

# init data dict
data = {}

# process time ranges
for it,_ in enumerate(tranges):
    tranges[it] = tranges[it].split('/')
    tranges[it] =  apt.Time(tranges[it])
    if ( tranges[it][1] - tranges[it][0] ).to_value('sec') < 120:  # impose at least 2 mins
        tranges[it][0] -= 1./1440.
        tranges[it][1] += 1./1440.
    data[it] = {}
    data[it]['trange'] = tranges[it]
    tranges[it] = list(tranges[it].fits)

# time loop
for it,trange in enumerate(tranges):
    # - download data
    _ = mms_load_fgm(probe=probes, trange=trange, data_rate=data_rate, time_clip=time_clip)
    # - retrieve data
    for probe in probes:
        data[it]['mms%d'%probe] = {}
        for field in fields:
            data[it]['mms%d'%probe][field] = {}
            epochdata = ptp_get_data('mms%d_fgm_%s_%s_l2'%(probe, field, data_rate))
            data[it]['mms%d'%probe][field]['epoch'] = (
                                        apt.Time(epochdata[0], format='unix')
                                                      ).to_datetime()
            data[it]['mms%d'%probe][field]['field'] = epochdata[1][:,:-1].T
    del epochdata

# free memory
ptp_del_data()
su_rmtree(os_path_join(os_getcwd(), 'pydata'))

# at this point in data we have:
show_data_tree(data)

# make some plots
for it,_ in enumerate(tranges):
    fig, ax = plt.subplots(2, 1, figsize=(12,12))
    for probe,ls in zip(probes,['-', '--']):
        for n,field in enumerate(fields):
            for i,c in zip(range(3),['r', 'g', 'b']):
                ax[n].plot( data[it]['mms%d'%probe][field]['epoch'],
                            data[it]['mms%d'%probe][field]['field'][i],
                            label='mms%d B%d'%(probe,i), ls=ls, c=c )
            ax[n].set_title('trange%d %s'%(it, field))
            ax[n].legend()
    fig.savefig(os_path_join(os_getcwd(), 'trange%d.png'%(it,)))
    plt.close()
