#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
#mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nWill plot something\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'psi_plot_2d'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']

ml.create_path(opath+'/'+out_dir)

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
    Psi = wl.psi_2d(B[:-1,...,0],run.meta)

    # -> Psi contourplot
    plt.close()
    fig,ax = plt.subplots(1,1,figsize=(15,7))

    im0 = ax.contour(x[ixmin:ixmax],y[iymin:iymax],Psi[ixmin:ixmax,iymin:iymax].T,64,cmap='inferno')
    plt.colorbar(im0,ax=ax)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('$\Psi$ - t='+str(time[ind])+'$\Omega_c^{-1}$')

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'Psi_%s_%d_%d_%d_%d_%d.png'%(run_name,ind,ixmin,ixmax,iymin,iymax))
    plt.close()

#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
