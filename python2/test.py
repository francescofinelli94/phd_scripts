import numpy as np
import matplotlib.pyplot as plt

#

n_bins = 10

grid = {}
f = open('grid_p_dBparl.txt','r')
grid['p'] = []
for i in range(n_bins):
    grid['p'].append([])
    for j in range(n_bins):
            grid['p'][-1].append(map(float,f.readline().split()))

f.close()
f = open('grid_e_dBparl.txt','r')
grid['e'] = []
for i in range(n_bins):
    grid['e'].append([])
    for j in range(n_bins):
            grid['e'][-1].append(map(float,f.readline().split()))

f.close()

#

acc = {}
acc['p'] = np.zeros((5,n_bins,n_bins),dtype=np.float64)
acc['e'] = np.zeros((5,n_bins,n_bins),dtype=np.float64)
f = open('acc_p_dBparl.txt','r')
for i in range(n_bins):
    for j in range(n_bins):
            acc['p'][:,i,j] = np.array(map(float,f.readline().split()))

f.close()
f = open('acc_e_dBparl.txt','r')
for i in range(n_bins):
    for j in range(n_bins):
            acc['e'][:,i,j] = np.array(map(float,f.readline().split()))

f.close()

#

new_acc = {}
new_acc['p'] = np.zeros((5,n_bins,n_bins),dtype=np.float64)
new_acc['e'] = np.zeros((5,n_bins,n_bins),dtype=np.float64)
for i in range(n_bins):
    for j in range(n_bins):
            vals = np.array(grid['p'][i][j])
            new_acc['p'][0,i,j] = float(vals.size)
            for k in range(4):
                    new_acc['p'][k+1,i,j] = np.sum(vals**(k+1))
            vals = np.array(grid['e'][i][j])
            new_acc['e'][0,i,j] = float(vals.size)
            for k in range(4):
                    new_acc['e'][k+1,i,j] = np.sum(vals**(k+1))

#

def gamma2(x):
    """
    excess of kurtosis: < ( (x - mean) / sigma )^4 > - 3
    where: mean = < x > ; sigma = sqrt( < (x - mean)^2 > )
    """
    if len(x) == 0:
        return np.nan
    return (( np.divide((x-x.mean()),x.std()) )**4).mean()-3.

#

def gamma2_2dmap(acc_map):
    """
    excess of kurtosis: < ( (x - mean) / sigma )^4 > - 3
    where: mean = < x > ; sigma = sqrt( < (x - mean)^2 > )
    for a 2d map w/ accumulator up to x^4
    """
    n_ele = acc_map[0]
    acc_map = np.divide(acc_map,acc_map[0])
    out_map = ( acc_map[4] - 4.*acc_map[3]*acc_map[1] +
                6.*acc_map[2]*acc_map[1]**2 - 3.*acc_map[1]**4 )
    out_map = np.divide(out_map,(acc_map[2] - acc_map[1]**2)**2)
    out_map = out_map - 3.
    out_map = np.multiply(out_map,acc_map[0])
    return out_map,n_ele

#

n_ele = {'p':np.zeros((n_bins,n_bins),dtype=np.int),
         'e':np.zeros((n_bins,n_bins),dtype=np.int)}
g2 = {'p':np.zeros((n_bins,n_bins),dtype=np.float64),
      'e':np.zeros((n_bins,n_bins),dtype=np.float64)}
for i in range(n_bins):
    for j in range(n_bins):
        valp = np.array(grid['p'][i][j])
        n_ele['p'][i,j] = valp.size
        with np.errstate(divide='ignore', invalid='ignore'):
            g2['p'][i,j] = gamma2(valp)
        vale = np.array(grid['e'][i][j])
        n_ele['e'][i,j] = vale.size
        with np.errstate(divide='ignore', invalid='ignore'):
            g2['e'][i,j] = gamma2(vale)

g2_acc = {}
n_ele_acc = {}
g2_acc['p'],n_ele_acc['p'] = gamma2_2dmap(acc['p'])
g2_acc['e'],n_ele_acc['e'] = gamma2_2dmap(acc['e'])

g2_new_acc = {}
n_ele_new_acc = {}
g2_new_acc['p'],n_ele_new_acc['p'] = gamma2_2dmap(new_acc['p'])
g2_new_acc['e'],n_ele_new_acc['e'] = gamma2_2dmap(new_acc['e'])

#

plt.close()
fig,(ax0,ax1,ax2) = plt.subplots(3,1,figsize=(6,18))

ax0.plot(np.abs(n_ele['p']-n_ele_acc['p']),'--b',label='n_ele diff p')
ax0.plot(np.abs(g2['p']-g2_acc['p']),'-b',label='g2 diff p')
ax0.plot(np.abs(n_ele['e']-n_ele_acc['e']),'--r',label='n_ele diff e')
ax0.plot(np.abs(g2['e']-g2_acc['e']),'-r',label='g2 diff e')
ax0.legend()
ax0.set_title('* - acc')

ax1.plot(np.abs(n_ele['p']-n_ele_new_acc['p']),'--b',label='n_ele diff p')
ax1.plot(np.abs(g2['p']-g2_new_acc['p']),'-b',label='g2 diff p')
ax1.plot(np.abs(n_ele['e']-n_ele_new_acc['e']),'--r',label='n_ele diff e')
ax1.plot(np.abs(g2['e']-g2_new_acc['e']),'-r',label='g2 diff e')
ax1.legend()
ax1.set_title('* - new_acc')

ax2.plot(np.abs(n_ele_acc['p']-n_ele_new_acc['p']),'--b',label='n_ele diff p')
ax2.plot(np.abs(g2_acc['p']-g2_new_acc['p']),'-b',label='g2 diff p')
ax2.plot(np.abs(n_ele_acc['e']-n_ele_new_acc['e']),'--r',label='n_ele diff e')
ax2.plot(np.abs(g2_acc['e']-g2_new_acc['e']),'-r',label='g2 diff e')
ax2.legend()
ax2.set_title('acc - new_acc')

plt.tight_layout()
plt.show()
