#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
from scipy.ndimage import gaussian_filter as gf
from numba import njit, jit
import work_lib as wl
sys.path.insert(0,'/home/finelli/Documents/my_branch')
import mylib as ml
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#latex fonts
font = 17
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlot directional kinetic energy per species\n')

#------------------------------------------------------
#Init
#------
run,tr,xr=wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr

wl.Can_I()

run_name = run.meta['run_name']
out_dir = 'directional_Kenergy_v3/'+run_name
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx,ny,nz = run.meta['nnn']
time = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']
if code_name == 'iPIC':
    run_label = 'iPIC'
    vA0 = 0.01
    n0 = 1./(4.*np.pi)
    stag = run.meta['stag']
    sQOM = run.meta['sQOM']
elif code_name == 'HVM':
    vA0 = 1.
    n0 = 1.
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
else:
    run_label = 'unknown'

ml.create_path(opath+'/'+out_dir)

calc = fibo('calc')
calc.meta=run.meta

#params
norm = 1./(n0*vA0**2)

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

idtens = np.zeros((3,3,nx,ny,nz),dtype=np.float64)
for i in range(3):
    idtens[i,i] = 1.

#---> loop over times <---
print " ",
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=np.int):
#-------------------------
    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)

    Psi = wl.psi_2d(B[:-1,...,0],run.meta)

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0],B[1],B[2])
        J = np.array([cx,cy,cz]) # J = rot(B)
        del cx,cy,cz

    #densities and currents
    Kx = {}
    Ky = {}
    Kz = {}

    n_p,u_p = run.get_Ion(ind)
    Kx['p'] = n_p*u_p[0]**2*norm
    Ky['p'] = n_p*u_p[1]**2*norm
    Kz['p'] = n_p*u_p[2]**2*norm
    if code_name == 'HVM':
        n_e = n_p.copy() # n_e = n_p = n (no charge separation)
        u_e = np.empty((3,nx,ny,nz),dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0],n_p)
        u_e[1] = u_p[1] - np.divide(J[1],n_p)
        u_e[2] = u_p[2] - np.divide(J[2],n_p)
        del J
    elif code_name == 'iPIC':
        n_e,u_e = run.get_Ion(ind,qom=qom_e)
    Kx['e'] = n_e*u_e[0]**2*norm
    Ky['e'] = n_e*u_e[1]**2*norm
    Kz['e'] = n_e*u_e[2]**2*norm
    del n_p,u_p,n_e,u_e
    
    if code_name == 'iPIC':
        for i,s in enumerate(stag):
            n_s,u_s = run.get_Spec(ind,s)
            Kx[s] = n_s*u_s[0]**2*norm
            Ky[s] = n_s*u_s[1]**2*norm
            Kz[s] = n_s*u_s[2]**2*norm
        del n_s,u_s

    #plots
    pkeys = ['p']
    ekeys = ['e']
    if code_name == 'iPIC':
        for i,s in enumerate(stag):
            if sQOM[i] < 0.:
                ekeys.append(s)
            else:
                pkeys.append(s)

    bmin = {'x':[],'y':[],'z':[]}
    bmax = {'x':[],'y':[],'z':[]}
    for k in pkeys:
        bmin['x'].append(np.min(Kx[k]))
        bmax['x'].append(np.max(Kx[k]))
        bmin['y'].append(np.min(Ky[k]))
        bmax['y'].append(np.max(Ky[k]))
        bmin['z'].append(np.min(Kz[k]))
        bmax['z'].append(np.max(Kz[k]))
    bxp = np.linspace(min(bmin['x']),max(bmax['x']),250+1)
    byp = np.linspace(min(bmin['y']),max(bmax['y']),250+1)
    bzp = np.linspace(min(bmin['z']),max(bmax['z']),250+1)

    bmin = {'x':[],'y':[],'z':[]}
    bmax = {'x':[],'y':[],'z':[]}
    for k in ekeys:
        bmin['x'].append(np.min(Kx[k]))
        bmax['x'].append(np.max(Kx[k]))
        bmin['y'].append(np.min(Ky[k]))
        bmax['y'].append(np.max(Ky[k]))
        bmin['z'].append(np.min(Kz[k]))
        bmax['z'].append(np.max(Kz[k]))
    bxe = np.linspace(min(bmin['x']),max(bmax['x']),250+1)
    bye = np.linspace(min(bmin['y']),max(bmax['y']),250+1)
    bze = np.linspace(min(bmin['z']),max(bmax['z']),250+1)

    #K_x
    plt.close()
    ncol = 2
    if code_name == 'iPIC':
        nrow = 3
    elif code_name == 'HVM':
        nrow = 2
    fig,ax = plt.subplots(nrow,ncol,figsize=(10*ncol,4*nrow))

    ax_ = ax[0,0]#x,p
    for k in pkeys:
        if k == '5': continue
        _,_,_ = ax_.hist(np.array(Kx[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxp,histtype='step',log=True,label='$K_{x,%s}$'%(k))
    ax_.set_xlabel('$K_x/K_0$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()
    ax_.set_title('%s - x-directional kinetic energy - $t=%f$'%(run_label,time[ind]))

    ax_ = ax[0,1]#x,e
    for k in ekeys:
        if k == '2': continue
        _,_,_ = ax_.hist(np.array(Kx[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxe,histtype='step',log=True,label='$K_{x,%s}$'%(k))
    ax_.set_xlabel('$K_x/K_0$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax [1,0]#x,p,b
    if code_name == 'iPIC':
        k = '3'
    elif code_name == 'HVM':
        k = 'p'
    im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kx[k][ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im,ax=ax_)
    ax_.set_xlim(x[ixmin],x[ixmax])
    ax_.set_ylim(y[iymin],y[iymax])
    ax_.set_xlabel('x')
    ax_.set_ylabel('y')
    ax_.set_title('$K_{x,%s}/K_0$'%(k))

    ax_ = ax [1,1]#x,e,b
    if code_name == 'iPIC':
        k = '0'
    elif code_name == 'HVM':
        k = 'e'
    im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kx[k][ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im,ax=ax_)
    ax_.set_xlim(x[ixmin],x[ixmax])
    ax_.set_ylim(y[iymin],y[iymax])
    ax_.set_xlabel('x')
    ax_.set_ylabel('y')
    ax_.set_title('$K_{x,%s}/K_0$'%(k))

    if code_name == 'iPIC':
        ax_ = ax [2,0]#x,p,1
        k = '4'
        im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kx[k][ixmin:ixmax,iymin:iymax,0].T,63)
        plt.colorbar(im,ax=ax_)
        ax_.set_xlim(x[ixmin],x[ixmax])
        ax_.set_ylim(y[iymin],y[iymax])
        ax_.set_xlabel('x')
        ax_.set_ylabel('y')
        ax_.set_title('$K_{x,%s}/K_0$'%(k))

        ax_ = ax [2,1]#x,e,1
        k = '1'
        im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kx[k][ixmin:ixmax,iymin:iymax,0].T,63)
        plt.colorbar(im,ax=ax_)
        ax_.set_xlim(x[ixmin],x[ixmax])
        ax_.set_ylim(y[iymin],y[iymax])
        ax_.set_xlabel('x')
        ax_.set_ylabel('y')
        ax_.set_title('$K_{x,%s}/K_0$'%(k))

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'Kx_energy_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #K_y
    plt.close()
    ncol = 2
    if code_name == 'iPIC':
        nrow = 3
    elif code_name == 'HVM':
        nrow = 2
    fig,ax = plt.subplots(nrow,ncol,figsize=(10*ncol,4*nrow))

    ax_ = ax[0,0]#y,p
    for k in pkeys:
        if k == '5': continue
        _,_,_ = ax_.hist(np.array(Ky[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxp,histtype='step',log=True,label='$K_{y,%s}$'%(k))
    ax_.set_xlabel('$K_y/K_0$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()
    ax_.set_title('%s - y-directional kinetic energy - $t=%f$'%(run_label,time[ind]))

    ax_ = ax[0,1]#y,e
    for k in ekeys:
        if k == '2': continue
        _,_,_ = ax_.hist(np.array(Ky[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxe,histtype='step',log=True,label='$K_{y,%s}$'%(k))
    ax_.set_xlabel('$K_y/K_0$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax [1,0]#y,p,b
    if code_name == 'iPIC':
        k = '3'
    elif code_name == 'HVM':
        k = 'p'
    im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Ky[k][ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im,ax=ax_)
    ax_.set_xlim(x[ixmin],x[ixmax])
    ax_.set_ylim(y[iymin],y[iymax])
    ax_.set_xlabel('x')
    ax_.set_ylabel('y')
    ax_.set_title('$K_{y,%s}/K_0$'%(k))

    ax_ = ax [1,1]#y,e,b
    if code_name == 'iPIC':
        k = '0'
    elif code_name == 'HVM':
        k = 'e'
    im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Ky[k][ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im,ax=ax_)
    ax_.set_xlim(x[ixmin],x[ixmax])
    ax_.set_ylim(y[iymin],y[iymax])
    ax_.set_xlabel('x')
    ax_.set_ylabel('y')
    ax_.set_title('$K_{y,%s}/K_0$'%(k))

    if code_name == 'iPIC':
        ax_ = ax [2,0]#y,p,1
        k = '4'
        im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Ky[k][ixmin:ixmax,iymin:iymax,0].T,63)
        plt.colorbar(im,ax=ax_)
        ax_.set_xlim(x[ixmin],x[ixmax])
        ax_.set_ylim(y[iymin],y[iymax])
        ax_.set_xlabel('x')
        ax_.set_ylabel('y')
        ax_.set_title('$K_{y,%s}/K_0$'%(k))

        ax_ = ax [2,1]#x,e,1
        k = '1'
        im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Ky[k][ixmin:ixmax,iymin:iymax,0].T,63)
        plt.colorbar(im,ax=ax_)
        ax_.set_xlim(x[ixmin],x[ixmax])
        ax_.set_ylim(y[iymin],y[iymax])
        ax_.set_xlabel('x')
        ax_.set_ylabel('y')
        ax_.set_title('$K_{y,%s}/K_0$'%(k))

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'Ky_energy_'+run_name+'_'+str(ind)+'.png')
    plt.close()

    #K_z
    plt.close()
    ncol = 2
    if code_name == 'iPIC':
        nrow = 3
    elif code_name == 'HVM':
        nrow = 2
    fig,ax = plt.subplots(nrow,ncol,figsize=(10*ncol,4*nrow))

    ax_ = ax[0,0]#z,p
    for k in pkeys:
        if k == '5': continue
        _,_,_ = ax_.hist(np.array(Kz[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxp,histtype='step',log=True,label='$K_{z,%s}$'%(k))
    ax_.set_xlabel('$K_z/K_0$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()
    ax_.set_title('%s - z-directional kinetic energy - $t=%f$'%(run_label,time[ind]))

    ax_ = ax[0,1]#z,e
    for k in ekeys:
        if k == '2': continue
        _,_,_ = ax_.hist(np.array(Kz[k][ixmin:ixmax,iymin:iymax,0].flat),
                density=1,bins=bxe,histtype='step',log=True,label='$K_{z,%s}$'%(k))
    ax_.set_xlabel('$K_z/K_0$')
    ax_.set_ylabel('discrete pdf')
    ax_.legend()

    ax_ = ax [1,0]#z,p,b
    if code_name == 'iPIC':
        k = '3'
    elif code_name == 'HVM':
        k = 'p'
    im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kz[k][ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im,ax=ax_)
    ax_.set_xlim(x[ixmin],x[ixmax])
    ax_.set_ylim(y[iymin],y[iymax])
    ax_.set_xlabel('x')
    ax_.set_ylabel('y')
    ax_.set_title('$K_{z,%s}/K_0$'%(k))

    ax_ = ax [1,1]#z,e,b
    if code_name == 'iPIC':
        k = '0'
    elif code_name == 'HVM':
        k = 'e'
    im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kz[k][ixmin:ixmax,iymin:iymax,0].T,63)
    plt.colorbar(im,ax=ax_)
    ax_.set_xlim(x[ixmin],x[ixmax])
    ax_.set_ylim(y[iymin],y[iymax])
    ax_.set_xlabel('x')
    ax_.set_ylabel('y')
    ax_.set_title('$K_{z,%s}/K_0$'%(k))

    if code_name == 'iPIC':
        ax_ = ax [2,0]#z,p,1
        k = '4'
        im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kz[k][ixmin:ixmax,iymin:iymax,0].T,63)
        plt.colorbar(im,ax=ax_)
        ax_.set_xlim(x[ixmin],x[ixmax])
        ax_.set_ylim(y[iymin],y[iymax])
        ax_.set_xlabel('x')
        ax_.set_ylabel('y')
        ax_.set_title('$K_{z,%s}/K_0$'%(k))

        ax_ = ax [2,1]#z,e,1
        k = '1'
        im = ax_.contourf(x[ixmin:ixmax],y[iymin:iymax],Kz[k][ixmin:ixmax,iymin:iymax,0].T,63)
        plt.colorbar(im,ax=ax_)
        ax_.set_xlim(x[ixmin],x[ixmax])
        ax_.set_ylim(y[iymin],y[iymax])
        ax_.set_xlabel('x')
        ax_.set_ylabel('y')
        ax_.set_title('$K_{z,%s}/K_0$'%(k))

    plt.tight_layout()
    #plt.show()
    plt.savefig(opath+'/'+out_dir+'/'+'Kz_energy_'+run_name+'_'+str(ind)+'.png')
    plt.close()



#---> loop over time <---
    print "\r",
    print "t = ",run.meta['time'][ind],
    gc.collect()
    sys.stdout.flush()
#------------------------
