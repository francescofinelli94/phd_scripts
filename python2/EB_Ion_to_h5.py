#libs
#import matplotlib as mpl
#mpl.use('Agg')
#import matplotlib.pyplot as plt
from numpy import ndarray as np_ndarray
#from numpy import sqrt as np_sqrt
from h5py import File as h5_File
from os.path import join as os_path_join
import sys
sys.path.insert(0,'/home/finelli/Documents/my_branch')
from iPIC_loader import *
from HVM_loader import *
#from fibo_beta import *

#inputs
code_name = 'HVM'
run_path = '/work1/califano/HVM2D/HVM_epsilon'#'/work1/califano/HVM2D/HVM_3072'
run_name = 'HVM_epsilon'#'HVM_3072'
t_target = float(sys.argv[1])#247.4
out_dir = '/home/finelli/Downloads'
meta_subdir = '00'

#set loader
if code_name == 'HVM':
    run = from_HVM(run_path)
elif code_name == 'iPIC':
    run = from_iPIC(run_path)
else:
    print('\nWhat code is this?\n')
    sys.exit()

#get meta
try:
    run.get_meta(meta_subdir)
except Exception:
    print('\nMetadata not found')
    sys.exit()

run.meta['run_name'] = run_name
run.meta['code_name'] = code_name

#calc = fibo('calc')
#calc.meta = run.meta

#get time
time = run.meta['time']
ind_t = np.argmin(np.abs(time - t_target))
t = time[ind_t]
print('t =',t)
run.meta['t'] = t
run.meta['ind_t'] = ind_t

#get fields
E,B = run.get_EB(ind_t)
n,u = run.get_Ion(ind_t)

#Jx,Jy,Jz = calc.calc_curl(B[0],B[1],B[2])
#Jn = np_sqrt(Jx**2 + Jy**2 + Jz**2)

#plt.contourf(Jn[...,0].T,63)
#plt.colorbar()
#plt.title('Jn %s %d'%(run_name,ind_t))
#plt.savefig('Jn_%s_%d.png'%(run_name,ind_t))
#sys.exit()

#write hdf5 file
h5_file = 'EB_Ion_%s_%d.h5'%(run_name,ind_t)
h5wfile = h5_File(os_path_join(out_dir,h5_file),'w')
h5wfile.create_dataset('B',data=B,dtype='f8')
h5wfile.create_dataset('E',data=E,dtype='f8')
h5wfile.create_dataset('n',data=n,dtype='f8')
h5wfile.create_dataset('u',data=u,dtype='f8')
for k in run.meta:
    if type(run.meta[k]) is dict:
        for kk in run.meta[k]:
            h5wfile.attrs[k+'_'+kk] = run.meta[k][kk]
    else:
        h5wfile.attrs[k] = run.meta[k]
h5wfile.flush()
h5wfile.close()

#done
print('Done!')
