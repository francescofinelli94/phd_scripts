import os
#import sys
import time

import numpy as np

#import work_lib as wl
#sys.path.insert(0,'/work2/finelli/Documents_SAV/my_branch')
#from iPIC_loader import *
#from HVM_loader import *

#run,tr,xr=wl.Init(sys.argv[1])

intervals = [ [  0.,  30.],
              [ 40.,  75.],
              [100., 140.],
              [150., 210.],
              [225., 237.],
              [240., 260.],
              [260., 295.],
              [300., 310.],
              [312., 319.] ]

for t0, t1 in intervals:
    with open('inp_tmp_%f_%f.dat'%(t0, t1), 'w') as f:
        f.write('LF_3d_DH HVM\n')
        f.write('/work2/finelli/LF_3d_DH\n')
        f.write('/work2/finelli/Outputs\n')
        f.write('True %f %f 1\n'%(t0, t1))
        f.write('True 0. 76. 0. 12.5\n')
        f.write('00\n')
    print('\nStarting t0 = %f - t1 = %f\n'%(t0, t1))
    #os.system('screen -dmS wl%f%f python2 windlike_plot_multitimes_3d.py inp_tmp_%f_%f.dat'%(
    #          t0, t1, t0, t1 ))
    os.system('screen -dmS fl%f%f python2 fluct_in_wind_multitimes_3d.py inp_tmp_%f_%f.dat'%(
              t0, t1, t0, t1 ))
    time.sleep(1)
    os.system('rm -f inp_tmp_%f_%f.dat'%(t0, t1))
