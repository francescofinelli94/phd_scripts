import sys
import os

path1 = sys.argv[1]
path2 = sys.argv[2]

print(f'{path1 = }\n{path2 = }')

def get_names_sizes(path):
    os.system('ls -l %s > tmp.dat'%(path,))

    with open('tmp.dat', 'r') as f:
        f.readline()
        lines = f.readlines()

    names = []
    sizes = []
    for line in lines:
        line = line.split()
        names.append( line[8] )
        sizes.append( int(line[4]) )

    os.system('rm -f tmp.dat')

    return names, sizes

names1, sizes1 = get_names_sizes(path1)
names2, sizes2 = get_names_sizes(path2)

for n1, s1, n2, s2 in zip(names1, sizes1, names2, sizes2):
    if s1 != s2:
        print(n1, s1, n2, s2)
