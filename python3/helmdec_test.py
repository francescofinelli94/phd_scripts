import numpy as np
import matplotlib.pyplot as plt

def helmholtz_decomposition_3D(field, ddd=(1., 1., 1.)):
    # box dimensions
    nx, ny, nz = field[0].shape
    dx, dy, dz = ddd
    x = np.linspace(0., float(nx-1)*dx, nx)
    y = np.linspace(0., float(ny-1)*dy, ny)
    z = np.linspace(0., float(nz-1)*dz, nz)

    # divergence of field
    div_field = ( np.gradient(field[0], x, axis=0, edge_order=2) +
                  np.gradient(field[1], y, axis=1, edge_order=2) +
                  np.gradient(field[2], z, axis=2, edge_order=2) )

    # FFT of div field
    div_field_ft = np.fft.rfftn(div_field)

    # create FFT mesh grid
    mx = np.fft.fftfreq(nx, 1)*nx
    my = np.fft.fftfreq(ny, 1)*ny
    mz = np.fft.rfftfreq(nz, 1)*nz
    m, n, p = np.meshgrid(mx, my, mz, indexing='ij')

    # init FFT of potential
    phi_ft = np.zeros(div_field_ft.shape, dtype=complex)

    # FFT of the potential calculated assuming the zero
    # frequency component is zero
    phi_ft[1:, :, :] = div_field_ft[1:, :, :]/(2*( (np.cos(2*np.pi*m[1:, :, :]/nx)-1)/dx**2 +
                                                   (np.cos(2*np.pi*n[1:, :, :]/ny)-1)/dy**2 +
                                                   (np.cos(2*np.pi*p[1:, :, :]/nz)-1)/dz**2))
    phi_ft[0, 1:, :] = div_field_ft[0, 1:, :]/(2*( (np.cos(2*np.pi*m[0, 1:, :]/nx)-1)/dx**2 +
                                                   (np.cos(2*np.pi*n[0, 1:, :]/ny)-1)/dy**2 +
                                                   (np.cos(2*np.pi*p[0, 1:, :]/nz)-1)/dz**2 ))
    phi_ft[0, 0, 1:] = div_field_ft[0, 0, 1:]/(2*( (np.cos(2*np.pi*m[0, 0, 1:]/nx)-1)/dx**2 +
                                                   (np.cos(2*np.pi*n[0, 0, 1:]/ny)-1)/dy**2 +
                                                   (np.cos(2*np.pi*p[0, 0, 1:]/nz)-1)/dz**2 ))

    # potential init
    phi = np.zeros((nx, ny, nz), dtype=float)

    # potential computed
    phi = np.fft.irfftn(phi_ft, s=(nx, ny, nz))

    # irrotational and solenoidal components
    field_irr = np.array([ np.gradient(phi, x, axis=0, edge_order=2),
                           np.gradient(phi, y, axis=1, edge_order=2),
                           np.gradient(phi, z, axis=2, edge_order=2) ])
    field_sol = field - field_irr

    return (field_irr, field_sol)

nx, ny, nz = 128, 128, 128
xl, yl, zl = 2.*np.pi*2., 2.*np.pi*2., 2.*np.pi*2.
dx, dy, dz = xl/float(nx), yl/float(ny), zl/float(nz)
x = np.linspace(0., xl-dx, nx)
y = np.linspace(0., yl-dy, ny)
z = np.linspace(0., zl-dz, nz)
X, Y, Z = np.meshgrid(x, y, z, indexing='ij')
phi = np.zeros((nx, ny, nz), dtype=float)
A = np.zeros((3, nx, ny, nz), dtype=float)
nmx = 2
nmy = 2
nmz = 2
for m, n, p in [(m, n, p) for m in range(0, nmx+1)
                          for n in range(0, nmy+1)
                          for p in range(0, nmz+1)]:
    if (m, n, p) == (0, 0, 0): continue
    a = np.random.random()
    fx, fy, fz = np.random.random(3)*2.*np.pi
    phi += a*np.cos(X*m*2.*np.pi/xl + fx)*np.cos(Y*n*2.*np.pi/yl + fy)*np.cos(Z*p*2.*np.pi/zl + fz)
    for c in range(3):
        a = np.random.random()
        fx, fy, fz = np.random.random(3)*2.*np.pi
        A[c] += a*(np.cos(X*m*2.*np.pi/xl + fx)*
                   np.cos(Y*n*2.*np.pi/yl + fy)*
                   np.cos(Z*p*2.*np.pi/zl + fz))
    print(m, n, p)

field_irr = - np.array([ np.gradient(phi, x, axis=0, edge_order=2),
                         np.gradient(phi, y, axis=1, edge_order=2),
                         np.gradient(phi, z, axis=2, edge_order=2) ])
A = np.array([np.gradient(A[2], y, axis=1, edge_order=2) -
              np.gradient(A[1], z, axis=2, edge_order=2),
              np.gradient(A[0], z, axis=2, edge_order=2) -
              np.gradient(A[2], x, axis=0, edge_order=2),
              np.gradient(A[1], x, axis=0, edge_order=2) -
              np.gradient(A[0], y, axis=1, edge_order=2)])
A = A - np.mean(A)
field_sol = np.array([np.gradient(A[2], y, axis=1, edge_order=2) -
                      np.gradient(A[1], z, axis=2, edge_order=2),
                      np.gradient(A[0], z, axis=2, edge_order=2) -
                      np.gradient(A[2], x, axis=0, edge_order=2),
                      np.gradient(A[1], x, axis=0, edge_order=2) -
                      np.gradient(A[0], y, axis=1, edge_order=2)])
field = field_irr + field_sol
field_irr_, field_sol_ = helmholtz_decomposition_3D(field, ddd=(dx, dy, dz))
rms_err_irr = np.sqrt(np.mean(((field_irr - field_irr_)**2)))
rms_err_sol = np.sqrt(np.mean((field_sol - field_sol_)**2))
rms_irr = np.sqrt(np.mean(((field_irr)**2)))
rms_sol = np.sqrt(np.mean((field_sol)**2))
print(f"{rms_err_irr/rms_irr = }")
print(f"{rms_err_sol/rms_sol = }")
max_err_irr = np.max(np.abs(((field_irr - field_irr_))))
max_err_sol = np.max(np.abs((field_sol - field_sol_)))
print(f"{max_err_irr/rms_irr = }")
print(f"{max_err_sol/rms_sol = }")

field_irr = np.sqrt(field_irr[0]**2 + field_irr[1]**2 + field_irr[2]**2)
field_irr_ = np.sqrt(field_irr_[0]**2 + field_irr_[1]**2 + field_irr_[2]**2)
field_sol = np.sqrt(field_sol[0]**2 + field_sol[1]**2 + field_sol[2]**2)
field_sol_ = np.sqrt(field_sol_[0]**2 + field_sol_[1]**2 + field_sol_[2]**2)

plt.subplot(231)
vmax = max(np.max((field_irr - field_irr_)[..., nz//2]), -np.min((field_irr - field_irr_)[..., nz//2]))
vmin = - vmax
plt.contourf((field_irr - field_irr_)[..., nz//2], vmin=vmin, vmax=vmax, cmap='seismic')
plt.colorbar()

plt.subplot(232)
vmax = max(np.max((field_irr - field_irr_)[:, ny//2]), -np.min((field_irr - field_irr_)[:, ny//2]))
vmin = - vmax
plt.contourf((field_irr - field_irr_)[:, ny//2], vmin=vmin, vmax=vmax, cmap='seismic')
plt.colorbar()

plt.subplot(233)
vmax = max(np.max((field_irr - field_irr_)[nx//2]), -np.min((field_irr - field_irr_)[nx//2]))
vmin = - vmax
plt.contourf((field_irr - field_irr_)[nx//2], vmin=vmin, vmax=vmax, cmap='seismic')
plt.colorbar()

plt.subplot(234)
vmax = max(np.max((field_sol - field_sol_)[..., nz//2]), -np.min((field_sol - field_sol_)[..., nz//2]))
vmin = - vmax
plt.contourf((field_sol - field_sol_)[..., nz//2], vmin=vmin, vmax=vmax, cmap='seismic')
plt.colorbar()

plt.subplot(235)
vmax = max(np.max((field_sol - field_sol_)[:, ny//2]), -np.min((field_sol - field_sol_)[:, ny//2]))
vmin = - vmax
plt.contourf((field_sol - field_sol_)[:, ny//2], vmin=vmin, vmax=vmax, cmap='seismic')
plt.colorbar()

plt.subplot(236)
vmax = max(np.max((field_sol - field_sol_)[nx//2]), -np.min((field_sol - field_sol_)[nx//2]))
vmin = - vmax
plt.contourf((field_sol - field_sol_)[nx//2], vmin=vmin, vmax=vmax, cmap='seismic')
plt.colorbar()

plt.show()
