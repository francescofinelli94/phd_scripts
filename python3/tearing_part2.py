# coding: utf-8
import sys

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

xl = 8.*2.*np.pi
L1 = 0.85
mime = 64.
dp = 1.
de = dp/np.sqrt(mime)
betap = 1.
rhop = np.sqrt(betap/2.)*dp
teti = 0.25
rhoe = np.sqrt(teti*betap/2.)*de

csv_path = "/home/frafin/Downloads/tearing/tearing_0_182.csv"  # sys.argv[1]
df = pd.read_csv(csv_path, header=0, index_col=0)
    
nmodes = len(df.columns) - 1 #5 #10 #20
color = plt.cm.viridis(np.linspace(0, 1, nmodes))

for m in range(1,nmodes+1):
    plt.plot(df["t"][1:], df[f"mod{m}"][1:], c=color[m-1])
plt.yscale('log')
plt.xlabel("t")
plt.ylabel("<|FFT[By](kx)|^2>")
plt.show()
plt.close()

t_ranges = [[22., 48.],
            [50., 74.],
            [100., 120.]]

all_gr = []
flg_first = True
for t0, t1 in t_ranges:
    all_gr.append([])
    it0 = np.argmin(np.abs(df["t"].values - t0))
    it1 = np.argmin(np.abs(df["t"].values - t1))
    for m in range(1,nmodes+1):
        if flg_first:
            plt.plot(df["t"][1:], df[f"mod{m}"][1:], c=color[m-1], label=f"mod{m}")
        else:
            plt.plot(df["t"][1:], df[f"mod{m}"][1:], c=color[m-1])
        coef = np.polyfit(df["t"][it0:it1+1], np.log(df[f"mod{m}"][it0:it1+1]), 1)
        all_gr[-1].append(coef[0])
        poly1d_fn = np.poly1d(coef)
        plt.plot(df["t"][it0:it1+1], np.exp(poly1d_fn(df["t"][it0:it1+1])), '*', c=color[m-1])
    flg_first = False
plt.yscale('log')
plt.xlabel("t")
plt.ylabel("<|FFT[By](kx)|^2>")
plt.legend()
plt.show()
plt.close()

for itr, gr in enumerate(all_gr):
    t0, t1 = t_ranges[itr]
    plt.plot(np.arange(1,nmodes+1), gr, label=f"{t0}-{t1}")
plt.axvline(x=xl/(L1*2.*np.pi), c='k', ls='--', label='kx L1 = 1')
plt.axvline(x=xl/(L1*2.*np.pi)*2., c='k', ls=':', label='kx L1 = 2')
plt.axvline(x=xl/(dp*2.*np.pi), c='r', ls='--', label='kx dp = 1')
plt.axvline(x=xl/(rhop*2.*np.pi), c='r', ls=':', label='kx rhop = 1')
plt.axvline(x=xl/(de*2.*np.pi), c='b', ls='--', label='kx de = 1')
#plt.axvline(x=xl/(rhoe*2.*np.pi), c='b', ls=':', label='kx rhoe = 1')
plt.xlabel("mx")
plt.ylabel("gamma")
plt.title("By x-modes growth rate")
plt.legend()
plt.show()
plt.close()
