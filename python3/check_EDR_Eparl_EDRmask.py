"""
This scripts plots the top view of the region selected as EDR
and also the z-averaged Eparl.
"""

# imports
import sys
import gc
from os.path import join

import numpy as np
from matplotlib import use as mpl_use
mpl_use('Agg')
import matplotlib.pyplot as plt
from scipy.ndimage import binary_dilation

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *

#latex font
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

# init
run,tr,xr = wl.Init(sys.argv[1])
ind1,ind2,ind_step = tr
run_name = run.meta['run_name']
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']
if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

out_dir = 'check_EDR_Eparl_EDRmask'
ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

n0 = 1.
Bx0 = 0.718722
vA = Bx0/np.sqrt(n0)

# -> mask
num_EDR = 2  # 1, 2
if num_EDR == 1:
    x0, x1, y0, y1 = 27.50, 32.50,  3.50,  4.50  # EDR1
elif num_EDR == 2:
    x0, x1, y0, y1 =  8.00, 13.00, 15.75, 16.75  # EDR2
else:
    print(f"{num_EDR = } is not valid. Valid options are 1 and 2")
    sys.exit()
mask = np.zeros((nx, ny, nz), dtype=bool)
ix0 = np.argmin(np.abs(x-x0))
ix1 = np.argmin(np.abs(x-x1))
iy0 = np.argmin(np.abs(y-y0))
iy1 = np.argmin(np.abs(y-y1))
mask[ix0:ix1+1, iy0:iy1+1, :] = True

# time loop
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
    # -> Eparl
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm
    del Bnorm
    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]
    del b

    # -> z-avg/sum
    Eparl_ = np.mean(Eparl, axis=-1)/(Bx0*vA)
    mask_ = np.sum(mask, axis=-1)
    del Eparl

    # -> plot
    plt.close('all')
    fig, ax = plt.subplots(2, 1, figsize=(16,12), sharex=True, sharey=True)
    
    vminmask = 0.
    vmaxmask = 1.
    vmin = np.min(Eparl_)
    vmax = np.max(Eparl_)
    vminEparl = -max(vmax,-vmin)
    vmaxEparl = max(vmax,-vmin)

    im0 = ax[0].pcolormesh(x, y, mask_.T>0., shading='gouraud',
                           vmin=vminmask, vmax=vmaxmask, cmap='pink_r')
    ax[0].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[0].set_title('mask $z$-view')
    plt.colorbar(im0, ax=ax[0])

    im1 = ax[1].pcolormesh(x, y, Eparl_.T, shading='gouraud',
                           vmin=vminEparl, vmax=vmaxEparl, cmap='seismic')
    ax[1].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
    ax[1].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[1].set_title('$E_{\parallel}/B_0/v_A$ $z$-avg')
    plt.colorbar(im1, ax=ax[1])

    fig.suptitle('$t=%.2f\;[\Omega_{\mathrm{p}}^{-1}]$'%(time[ind],))
    
    fig_name = "mask_Eparl_%s_EDR%d_ind%d.png"%(run_name, num_EDR, ind)
    fig.savefig(join(opath, out_dir, fig_name))
    plt.close()

# end time loop
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
