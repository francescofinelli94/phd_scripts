# coding: utf-8
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

path1 = "/home/frafin/Downloads/Eparl_EDR_EDRmask_BIS/Eparl_EDR2_adim_uns_log_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
path2 = "/home/frafin/Downloads/rotEparl_EDR_EDRmask/rotEparl_EDR2_adim_uns_log_rotEparl_EDR1_EDR2_LF_3d_DH_0_182.csv"

df1 = pd.read_csv(path1, header=0, index_col=0)
df2 = pd.read_csv(path2, header=0, index_col=0)

plt.subplot(211)
#plt.plot(df1["avg1"]+3.*df1["std1"], df2["avg1"]+3.*df2["std1"], 'k:')
plt.scatter(df1["avg1"]+3.*df1["std1"], df2["avg1"]+3.*df2["std1"], c=df1["t"], cmap="tab10")
plt.xlabel("Eparl")
plt.ylabel("rotEparl")
plt.xscale("log")
plt.yscale("log")
plt.colorbar(label="t")
coef = np.polyfit(np.log(df1["avg1"]+3.*df1["std1"])[1:], np.log(df2["avg1"]+3.*df2["std1"])[1:], 1)
poly1d_fn = np.poly1d(coef)
plt.plot(df1["avg1"]+3.*df1["std1"], np.exp(poly1d_fn(np.log(df1["avg1"]+3.*df1["std1"]))), "r--")
plt.subplot(212)
m, q = coef
devx = np.log(df1["avg1"]+3.*df1["std1"]) - (np.log(df2["avg1"]+3.*df2["std1"])/m - q/m)
devy = np.log(df2["avg1"]+3.*df2["std1"]) - (np.log(df1["avg1"]+3.*df1["std1"])*m + q)
dev = devx*devy/np.sqrt(devx**2+devy**2)*np.sign(devx)
plt.plot(df1["t"], dev)
plt.xlabel("t")
plt.ylabel("Deviation from fit")
plt.axhline(y=0., c="k", ls="--")
plt.show()
