from glob import glob
from os.path import join
from sys import argv

if len(argv) < 2:
    WPATH = "."
else:
    WPATH = argv[1]
mult = [1, 60, 60*60, 60*60*24]  # s, m, h, d
NPROC_PER_NODE = 68

segs = sorted(glob(join(WPATH, "[0-9][0-9]")))

total_cost = 0
for seg in segs:
    with open(join(seg, "start"), "r") as sfile:
        sline = sfile.readline()[:-1]
    with open(join(seg, "end"), "r") as efile:
        eline = efile.readline()[:-1]
    stime = 0
    for i, v in enumerate(list(map(int, sline.split(" ")[-2].strip(",").split(".")))):
        stime += v*mult[2-i]
    etime = 0
    for i, v in enumerate(list(map(int, eline.split(" ")[-2].strip(",").split(".")))):
        etime += v*mult[2-i]
    seconds = (etime - stime) % mult[-1]
    with open(join(seg, "job.out"), "r") as jfile:
        _ = jfile.readline()
        _ = jfile.readline()
        jline = jfile.readline()
    nodes = int(jline.split(" ")[2])
    total_cost += seconds*nodes*NPROC_PER_NODE

mhcpu = round(float(total_cost)/float(mult[2])/1000000., 3)
print(f"Cost = {mhcpu} Mh CPU")
