"""
z-averaged plots from 3D sim.
plots: anisotropy, curvature, Pdrift
compare the two current sheets
"""

#-----------------------------------------------------
#importing stuff
#---------------
import sys
import gc
import os
import glob

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of anisotropy, curvature, and Pdrift.\n')

#------------------------------------------------------
#Init
#----
input_file = sys.argv[1]

plt_show_flg = False
plt_save_flg = True
zoom_flg = True
rms_flg = False

PDplane_flg = False

run,tr,xr=wl.Init(input_file)
ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr
run_name = run.meta['run_name']
w_ele = run.meta['w_ele']
times = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
nx = run.meta['nx']
ny = run.meta['ny']
nz = run.meta['nz']
dx = run.meta['dx']
dy = run.meta['dy']
dz = run.meta['dz']
code_name = run.meta['code_name']
#
if code_name == 'HVM':
    calc = fibo_calc()
    calc.meta = run.meta
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
    B0 = 0.718722  # 1.
    n0 = 1.
    V0 = 0.718722  # 1.
elif code_name == 'iPIC':
    run_label = 'iPIC'
    qom_e = run.meta['msQOM'][0]
    B0 = 0.01
    n0 = 1./(4.*np.pi)
    V0 = 0.01
else:
    print('ERROR: unknown code_name %s'%(code_name))
    sys.exit(-1)
#
opath = run.meta['opath']

#---> loop over times <---
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    anis_dict = {'p':{}, 'e':{}}
    curv_dict = {}
    Pdrift_dict = {}
    Psi_dict = {}
    x_dict = {}
    y_dict = {}

    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
    #
    #anisotropy
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    del pparlp,pperpp
    #
    if w_ele:
        if code_name == 'HVM':
            n_p,_ = run.get_Ion(ind)
            pparle,pperpe = run.get_Te(ind)
            pparle *= n_p
            pperpe *= n_p
            del n_p
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
    else:
        anise = np.ones((nx,ny,1),dtype=np.float64)
        n_e,_ = run.get_Ion(ind)
        pperpe = n_e*run.meta['beta']*0.5*run.meta['teti']
        pparle = pperpe
        del n_e
    #
    b = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    Bn = np.sqrt(B2)
    for i in range(3):
        b[i] = np.divide(B[i],Bn)
    del Bn
    dxbx = calc.calc_gradx(b[0])
    dybx = calc.calc_grady(b[0])
    dzbx = calc.calc_gradz(b[0])
    dxby = calc.calc_gradx(b[1])
    dyby = calc.calc_grady(b[1])
    dzby = calc.calc_gradz(b[1])
    dxbz = calc.calc_gradx(b[2])
    dybz = calc.calc_grady(b[2])
    dzbz = calc.calc_gradz(b[2])
    b_Nb = np.array([b[0]*dxbx + b[1]*dybx + b[2]*dzbx,
                     b[0]*dxby + b[1]*dyby + b[2]*dzby,
                     b[0]*dxbz + b[1]*dybz + b[2]*dzbz])
    del b,dxbx,dybx,dzbx,dxby,dyby,dzby,dxbz,dybz,dzbz
    curv = np.sqrt(b_Nb[0]**2 + b_Nb[1]**2 + b_Nb[2]**2)
    #
    gradpperpe = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    gradpperpe[0] = calc.calc_gradx(pperpe)
    gradpperpe[1] = calc.calc_grady(pperpe)
    gradpperpe[2] = calc.calc_gradz(pperpe)
    PrD = np.array( [ (B[1]*gradpperpe[2]-B[2]*gradpperpe[1])/B2,
                      (B[2]*gradpperpe[0]-B[0]*gradpperpe[2])/B2,
                      (B[0]*gradpperpe[1]-B[1]*gradpperpe[0])/B2 ] )
    del gradpperpe
    CrD = np.array( [ (pparle-pperpe)*(B[1]*b_Nb[2]-B[2]*b_Nb[1])/B2,
                      (pparle-pperpe)*(B[2]*b_Nb[0]-B[0]*b_Nb[2])/B2,
                      (pparle-pperpe)*(B[0]*b_Nb[1]-B[1]*b_Nb[0])/B2 ] )
    del b_Nb,pparle,pperpe,B2
    PD = PrD + CrD
    del PrD,CrD
    if PDplane_flg:
        Pdrift = np.sqrt(PD[0]**2+PD[1]**2)
    else:
        Pdrift = np.sqrt(PD[0]**2+PD[1]**2+PD[2]**2)
    del PD
    #
    t =  times[ind]
    #
    cs_list = ['First', 'Second']
    #
    B_ = np.mean(B, axis=-1)
    anisp_ = np.mean(anisp, axis=-1)
    anise_ = np.mean(anise, axis=-1)
    curv_ = np.mean(curv, axis=-1)
    Pdrift_ = np.mean(Pdrift, axis=-1)
    #
    Psi_ = wl.psi_2d(B_[:-1],run.meta)
    #
    x_ = x.copy()
    y_ = y.copy()
    for cs in cs_list:
        if cs == 'Second':
            Dx = 20.
            Dy = -11.
            Dix = round(Dx/dx)
            Diy = round(Dy/dy)
            anisp_ = np.roll(anisp_, (Dix, Diy), axis=(0,1))
            anise_ = np.roll(anise_, (Dix, Diy), axis=(0,1))
            curv_ = np.roll(curv_, (Dix, Diy), axis=(0,1))
            Pdrift_ = np.roll(Pdrift_, (Dix, Diy), axis=(0,1))
            Psi_ = np.roll(Psi_, (Dix, Diy), axis=(0,1))
            x_ += -Dix*dx
            y_ += -Diy*dy
        #
        x_dict[cs] = x_[ixmin:ixmax].copy()
        y_dict[cs] = y_[iymin:iymax].copy()
        #
        Psi_dict[cs] = Psi_[ixmin:ixmax,iymin:iymax]/B0
        #
        if rms_flg:
            anisp_norm = np.sqrt(np.mean(
                         anisp_[ixmin:ixmax,iymin:iymax]*anisp_[ixmin:ixmax,iymin:iymax]))
        else:
            anisp_norm = 1.
        anis_dict['p'][cs] = anisp_[ixmin:ixmax,iymin:iymax]/anisp_norm
        #
        if rms_flg:
            anise_norm = np.sqrt(np.mean(
                         anise_[ixmin:ixmax,iymin:iymax]*anise_[ixmin:ixmax,iymin:iymax]))
        else:
            anise_norm = 1.
        anis_dict['e'][cs] = anise_[ixmin:ixmax,iymin:iymax]/anise_norm
        #
        if rms_flg:
            curv_norm = np.sqrt(np.mean(
                        curv_[ixmin:ixmax,iymin:iymax]*curv_[ixmin:ixmax,iymin:iymax]))
        else:
            curv_norm = 1.
        curv_dict[cs] = curv_[ixmin:ixmax,iymin:iymax]/curv_norm
        #
        if rms_flg:
            Pdrift_norm = np.sqrt(np.mean(
                          Pdrift_[ixmin:ixmax,iymin:iymax]*Pdrift_[ixmin:ixmax,iymin:iymax] ))
        else:
            Pdrift_norm = n0*V0
        Pdrift_dict[cs] = Pdrift_[ixmin:ixmax,iymin:iymax]/Pdrift_norm
    #
    del B_, Psi_, anisp_, anise_, curv_, Pdrift_, x_, y_
    gc.collect()

    #----------------
    #PLOT TIME!!!!!!!
    #----------------
    plt.close('all')
    fig,ax = plt.subplots(4,len(cs_list),figsize=(18,14))#,sharex=True,sharey=True)
    fig.subplots_adjust(hspace=.06,wspace=.09,top=.92,bottom=.1,left=.055,right=0.975)

    vmin = ml.min_dict(anis_dict['p'])
    vmax = ml.max_dict(anis_dict['p'])
    vminanisp = 10.**(-max(np.log10(vmax),-np.log10(vmin)))
    vmaxanisp = 10.**max(np.log10(vmax),-np.log10(vmin))

    #vmin = ml.min_dict(anis_dict['e'])
    mins = []
    for k in anis_dict['e']:
        mins.append(np.min(anis_dict['e'][k][anis_dict['e'][k]>0.]))
    vmin = min(mins)
    vmax = ml.max_dict(anis_dict['e'])
    vminanise = 10.**(-max(np.log10(vmax),-np.log10(vmin)))
    vmaxanise = 10.**max(np.log10(vmax),-np.log10(vmin))

    vmincurv = ml.min_dict(curv_dict)
    vmaxcurv = ml.max_dict(curv_dict)

    vminPdrift = ml.min_dict(Pdrift_dict)
    vmaxPdrift = ml.max_dict(Pdrift_dict)

    for j, cs in enumerate(cs_list):
        i = 0
        im0 = ax[i,j].pcolormesh(x_dict[cs], y_dict[cs], anis_dict['p'][cs].T, shading='gouraud',
                                 cmap='PiYG',
                                 norm=mpl.colors.LogNorm(vmin=vminanisp,vmax=vmaxanisp))
        ax[i,j].contour(x_dict[cs], y_dict[cs], Psi_dict[cs].T, 8, colors='black')
        if j == 0:
            ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
        ax[i,j].set_title('%s CS - $z$ avg.'%(cs,), pad=10.)
        ax[i,j].set_xticklabels([])
    #
        i = 1
        im1 = ax[i,j].pcolormesh(x_dict[cs], y_dict[cs], anis_dict['e'][cs].T, shading='gouraud',
                                 cmap='PRGn',
                                 norm=mpl.colors.LogNorm(vmin=vminanise,vmax=vmaxanise))
        ax[i,j].contour(x_dict[cs], y_dict[cs], Psi_dict[cs].T, 8, colors='black')
        if j == 0:
            ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
        ax[i,j].set_xticklabels([])
    #
        i = 2
        im2 = ax[i,j].pcolormesh(x_dict[cs], y_dict[cs], curv_dict[cs].T, shading='gouraud',
                                 cmap='cubehelix',
                                 norm=mpl.colors.LogNorm(vmin=vmincurv,vmax=vmaxcurv))
        ax[i,j].contour(x_dict[cs], y_dict[cs], Psi_dict[cs].T, 8, colors='black')
        if j == 0:
            ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
        ax[i,j].set_xticklabels([])
    #
        i = 3
        im3 = ax[i,j].pcolormesh(x_dict[cs], y_dict[cs], Pdrift_dict[cs].T, shading='gouraud',
                              vmin=vminPdrift, vmax=vmaxPdrift, cmap='magma_r')
        ax[i,j].contour(x_dict[cs], y_dict[cs], Psi_dict[cs].T, 8, colors='white')
        ax[i,j].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
        if j == 0:
            ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')

    cb0 = plt.colorbar(im0, ax=ax[0,:], pad=.007)
    if rms_flg:
        label_ = '$A_{\mathrm{p}} / \overline{A_{\mathrm{p}}}$'
    else:
        label_ = '$p_{\perp,\mathrm{p}}/p_{\parallel,\mathrm{p}}$'
    cb0.set_label(label_, rotation=270, labelpad=35)

    cb1 = plt.colorbar(im1, ax=ax[1,:], pad=.007)
    if rms_flg:
        label_ = '$A_{\mathrm{e}} / \overline{A_{\mathrm{e}}}$'
    else:
        label_ = '$p_{\perp,\mathrm{e}}/p_{\parallel,\mathrm{e}}$'
    cb1.set_label(label_, rotation=270, labelpad=35)

    cb2 = plt.colorbar(im2, ax=ax[2,:], pad=.007)
    if rms_flg:
        label_ = '$\kappa / \overline{\kappa}$'
    else:
        label_ = '$|\hat{\\boldsymbol{b}}\cdot\\boldsymbol{\\nabla}\hat{\\boldsymbol{b}}|d_{\mathrm{p}}$'
    cb2.set_label(label_, rotation=270, labelpad=35)

    cb3 = plt.colorbar(im3, ax=ax[3,:], pad=.007)
    if PDplane_flg:
        if rms_flg:
            label_ = '$J_{P,\mathrm{e}}^{(\\text{plane})}/\overline{J_{\Pi,\mathrm{e}}}^{(\\text{plane})}$'
        else:
            label_ = '$J_{P,\mathrm{e}}^{(\\text{plane})} / ( e n_0 v_A )$'
    else:
        if rms_flg:
            label_ = '$J_{P,\mathrm{e}} / \overline{J_{\Pi,\mathrm{e}}}$'
        else:
            label_ = '$J_{P,\mathrm{e}} / ( e n_0 v_A )$'
    cb3.set_label(label_, rotation=270, labelpad=45)

    fig.suptitle('$t=%.2f\;[\Omega_{\mathrm{p}}^{-1}]$'%(t,))

    if plt_show_flg:
        plt.show()
    if plt_save_flg:
        out_dir = 'plot_3d_anis_curv_Pdrift_both_cs_v2'
        ml.create_path(opath+'/'+out_dir)
        out_dir += '/comp'
        fig_name = 'anis_'
        fig_name += 'curv_'
        if PDplane_flg:
            fig_name += 'PDplane'
        else:
            fig_name += 'Pdrift'
        fig_name += '__%s'%(run_name,)
        for cs in cs_list:
            out_dir += '__' + cs
            fig_name += '__' + cs + '_ind%d'%ind

        if zoom_flg:
            fig_name += '__zoom'
        if rms_flg:
            fig_name += '__rms'
        fig_name += '.png'
        ml.create_path(opath+'/'+out_dir)
        fig.savefig(opath+'/'+out_dir+'/'+fig_name)
    plt.close()

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", times[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------
