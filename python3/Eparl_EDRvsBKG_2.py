#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
import gc

import numpy as np
from matplotlib import use as mpl_use
from matplotlib import get_backend as mpl_get_backend
mpl_use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm as mplcol_LogNorm
from scipy.ndimage import binary_dilation

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
import mylib as ml
from mod_calc import *
from mod_from import *

#------------------------------------------------------
#Intent
#------
print('\nPlots statistics about Eparl\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------
if len(sys.argv) != 5:
    print('\n4 inputs are required:\n\t- .dat file\n\t- flg_abs\n\t- flg_uns\n\t- flg_log\n')
    sys.exit()

run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'Eparl_EDRvsBKG_2'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

n0 = 1.
Bx0 = 0.718722

vA = Bx0/np.sqrt(n0)

flg_2pl = True  # -> EDR and BKG plots on different PNGs
#flg_2pl = False  # -> EDR and BKG plots on the same PNG

flg_abs = False  # -> Eparl is taken with its sign
#flg_abs = True  # -> Eparl is taken without its sign
#flg_abs = sys.argv[2].lower() in ['true', '1']

#flg_uns = False  # -> Eparl average is taken with its sign
flg_uns = True  # -> Eparl average is taken without its sign
#flg_uns = sys.argv[3].lower() in ['true', '1']

#flg_log = False  # -> linear scale on y-axis
flg_log = True  # -> logarithmic scale on y-axis
#flg_log = sys.argv[4].lower() in ['true', '1']

print(f'{flg_abs = }\n{flg_uns = }\n{flg_log = }')
#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------

#---> loop over times <---
t_all = []
Eparl_EDR_min = []
Eparl_EDR_max = []
Eparl_EDR_std = []
Eparl_EDR_avg = []
Eparl_EDR_h_all = []
Eparl_EDR_b_all = []
Eparl_BKG_min = []
Eparl_BKG_max = []
Eparl_BKG_std = []
Eparl_BKG_avg = []
Eparl_BKG_h_all = []
Eparl_BKG_b_all = []
n_bins = 100

print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    #------------------------------------------------------
    # Get data
    #----------
    #-------
    t0 = t_time()
    #-------
    # -> magnetic fields and co.
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm

    del Bnorm

    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
        J = np.array([cx, cy, cz]) # J = rot(B)
        del cx, cy, cz

    # -> densities and currents
    if code_name == 'HVM':
        n_p, u_p = run.get_Ion(ind)
        u_e = np.empty((3, nx, ny, nz), dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0], n_p)
        u_e[1] = u_p[1] - np.divide(J[1], n_p)
        u_e[2] = u_p[2] - np.divide(J[2], n_p)
        del n_p, J, u_p
    elif code_name == 'iPIC':
        _, u_e = run.get_Ion(ind,qom=qom_e)

    # -> demag
    e_demag = np.empty(E.shape, dtype=np.float64)
    e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
    e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
    e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
    e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)
    del e_demag, B, E, u_e
    #-------
    print('\nData loading and computing: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # selecting regions
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    th = np.max(e_demag_n)*.75

    mask_EDR = (e_demag_n>th)
    #mask_EDR = binary_dilation(mask_EDR, iterations=4)
    mask_BKG = (e_demag_n>th)
    mask_BKG = np.logical_not(binary_dilation(mask_BKG, iterations=10))

    if flg_abs:
        Eparl_EDR = np.abs(Eparl[mask_EDR]/(Bx0*vA))
        Eparl_BKG = np.abs(Eparl[mask_BKG]/(Bx0*vA))
    else:
        Eparl_EDR = Eparl[mask_EDR]/(Bx0*vA)
        Eparl_BKG = Eparl[mask_BKG]/(Bx0*vA)
    #-------
    print('Selecting regions: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # generatying histogram
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    t_all.append(time[ind])
    Eparl_EDR_min.append(np.min(Eparl_EDR))
    Eparl_EDR_max.append(np.max(Eparl_EDR))
    Eparl_EDR_std.append(np.std(Eparl_EDR))
    Eparl_EDR_avg.append(np.mean(Eparl_EDR))
    Eparl_EDR_b_all.append(np.linspace(Eparl_EDR_min[-1], Eparl_EDR_max[-1], n_bins+1))
    h, _ = np.histogram(Eparl_EDR, bins=Eparl_EDR_b_all[-1], density=True)
    Eparl_EDR_h_all.append(h)
    Eparl_BKG_min.append(np.min(Eparl_BKG))
    Eparl_BKG_max.append(np.max(Eparl_BKG))
    Eparl_BKG_std.append(np.std(Eparl_BKG))
    Eparl_BKG_avg.append(np.mean(Eparl_BKG))
    Eparl_BKG_b_all.append(np.linspace(Eparl_BKG_min[-1], Eparl_BKG_max[-1], n_bins+1))
    h, _ = np.histogram(Eparl_BKG, bins=Eparl_BKG_b_all[-1], density=True)
    Eparl_BKG_h_all.append(h)
    #-------
    print('Generating histogram: %fs'%(t_time() - t0,))
    #-------

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")

t_all = np.array(t_all)
Eparl_EDR_min = np.array(Eparl_EDR_min)
Eparl_EDR_max = np.array(Eparl_EDR_max)
Eparl_EDR_std = np.array(Eparl_EDR_std)
Eparl_EDR_avg = np.array(Eparl_EDR_avg)
Eparl_BKG_min = np.array(Eparl_BKG_min)
Eparl_BKG_max = np.array(Eparl_BKG_max)
Eparl_BKG_std = np.array(Eparl_BKG_std)
Eparl_BKG_avg = np.array(Eparl_BKG_avg)
#------------------------

#------------------------------------------------------
# plotting
#-------------------------
#-------
t0 = t_time()
#-------
# -> initi time bins
left_dt = t_all.copy()
left_dt[1:] = t_all[1:] - t_all[:-1]
left_dt[0] = 0.
right_dt = t_all.copy()
right_dt[:-1] = t_all[1:] - t_all[:-1]
right_dt[-1] = 0.

# -> init figure(s)
plt.close('all')
if flg_2pl:
    fig0, ax0 = plt.subplots(1, 1, figsize=(16, 10))
    fig1, ax1 = plt.subplots(1, 1, figsize=(16, 10))
else:
    fig1, (ax0, ax1) = plt.subplots(2, 1, figsize=(16, 10))

# -> hist plot  (EDR and BKG)
for i, (t, EDR_b, EDR_h, EDR_a, BKG_b, BKG_h, BKG_a) in enumerate(zip(
            t_all, Eparl_EDR_b_all, Eparl_EDR_h_all, Eparl_EDR_avg,
            Eparl_BKG_b_all, Eparl_BKG_h_all, Eparl_BKG_avg )):
    if flg_uns:
        cmap_EDR = 'PuBuGn' if (EDR_a >= 0.) else 'YlOrRd'
        cmap_BKG = 'PuBuGn' if (BKG_a >= 0.) else 'YlOrRd'
        d_EDR = 1 if (EDR_a >= 0.) else -1
        d_BKG = 1 if (BKG_a >= 0.) else -1
    else:
        cmap_EDR = 'PuBuGn'
        cmap_BKG = 'PuBuGn'
        d_EDR = 1
        d_BKG = 1
    T, B = [t-left_dt[i]*.5, t+right_dt[i]*.5], d_EDR*EDR_b[::d_EDR]
    ax0.pcolormesh( T, B, EDR_h[::d_EDR].reshape(1,EDR_h.size).T,
                    cmap=cmap_EDR, shading='flat', norm=mplcol_LogNorm() )
    B = d_BKG*BKG_b[::d_BKG]
    ax1.pcolormesh( T, B, BKG_h[::d_BKG].reshape(1,BKG_h.size).T,
                    cmap=cmap_BKG, shading='flat', norm=mplcol_LogNorm() )

# -> deal with avg sign
if flg_uns:
    Eparl_EDR_max[Eparl_EDR_avg<0.], Eparl_EDR_min[Eparl_EDR_avg<0.] = (
            -Eparl_EDR_min[Eparl_EDR_avg<0.], -Eparl_EDR_max[Eparl_EDR_avg<0.] )
    Eparl_BKG_max[Eparl_BKG_avg<0.], Eparl_BKG_min[Eparl_BKG_avg<0.] = (
            -Eparl_BKG_min[Eparl_BKG_avg<0.], -Eparl_BKG_max[Eparl_BKG_avg<0.] )
    Eparl_EDR_avg = np.abs(Eparl_EDR_avg)
    Eparl_BKG_avg = np.abs(Eparl_BKG_avg)

# -> stat curves plot for EDR
ax0.plot(t_all, Eparl_EDR_avg, 'k-', label=('|avg|' if flg_uns else 'avg'))
ax0.plot(t_all, Eparl_EDR_avg+Eparl_EDR_std, 'k--', label='avg%sstd'%('+' if flg_log else '+-',))
ax0.plot(t_all, Eparl_EDR_avg+2.*Eparl_EDR_std, 'k-.', label='avg%s2std'%('+' if flg_log else '+-',))
ax0.plot(t_all, Eparl_EDR_avg+3.*Eparl_EDR_std, 'k:', label='avg%s3std'%('+' if flg_log else '+-',))
ax0.plot(t_all, Eparl_EDR_max, 'r-', label=('max' if flg_log else 'min, max'))
if not flg_log:
    ax0.plot(t_all, Eparl_EDR_avg-Eparl_EDR_std, 'k--')
    ax0.plot(t_all, Eparl_EDR_avg-2.*Eparl_EDR_std, 'k-.')
    ax0.plot(t_all, Eparl_EDR_avg-3.*Eparl_EDR_std, 'k:')
    ax0.plot(t_all, Eparl_EDR_min, 'r-')

# -> EDR labels, lims, and legend
ax0.set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
tmp = 'E_{\parallel}'
if flg_abs:
    tmp = '|%s|'%(tmp,)
tmp = '$%s/B_0/v_A$'%(tmp,)
if flg_uns:
    tmp = '%s (unsigned average)'%(tmp,)
ax0.set_ylabel('%s - EDR'%(tmp,))
ax0.set_xlim(t_all[0], t_all[-1])
if flg_log:
    ax0.set_ylim(max(np.min(Eparl_EDR_avg[Eparl_EDR_avg>0.]), 1.e-5), np.max(Eparl_EDR_max))
    ax0.set_yscale('log')
else:
    ax0.set_ylim(np.min(Eparl_EDR_min), np.max(Eparl_EDR_max))
ax0.legend()

# -> save/show EDR fig (for separate figs)
if flg_2pl:
    fig0.tight_layout()
    if mpl_get_backend() == 'agg':
        fname = '/Eparl_EDR_adim_'
        if flg_abs:
            fname = fname + 'abs_'
        if flg_uns:
            fname = fname + 'uns_'
        if flg_log:
            fname = fname + 'log_'
        fig0.savefig(opath+'/'+out_dir+fname+'%s_%d_%d.png'%(run_name, ind1, ind2))
    else:
        fig0.show()
    plt.close(fig0)

# -> stat curves plot for BKG
ax1.plot(t_all, Eparl_BKG_avg, 'k-', label=('|avg|' if flg_uns else 'avg'))
ax1.plot(t_all, Eparl_BKG_avg+Eparl_BKG_std, 'k--', label='avg%sstd'%('+' if flg_log else '+-',))
ax1.plot(t_all, Eparl_BKG_avg+2.*Eparl_BKG_std, 'k-.', label='avg%s2std'%('+' if flg_log else '+-',))
ax1.plot(t_all, Eparl_BKG_avg+3.*Eparl_BKG_std, 'k:', label='avg%s3std'%('+' if flg_log else '+-',))
ax1.plot(t_all, Eparl_BKG_max, 'r-', label=('max' if flg_log else 'min, max'))
if not flg_log:
    ax1.plot(t_all, Eparl_BKG_avg-Eparl_BKG_std, 'k--')
    ax1.plot(t_all, Eparl_BKG_avg-2.*Eparl_BKG_std, 'k-.')
    ax1.plot(t_all, Eparl_BKG_avg-3.*Eparl_BKG_std, 'k:')
    ax1.plot(t_all, Eparl_BKG_min, 'r-')

# -> BKG labels, lims, and legend
ax1.set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
tmp = 'E_{\parallel}'
if flg_abs:
    tmp = '|%s|'%(tmp,)
tmp = '$%s/B_0/v_A$'%(tmp,)
if flg_uns:
    tmp = '%s (unsigned average)'%(tmp,)
ax1.set_ylabel('%s - BKG'%(tmp,))
ax1.set_xlim(t_all[0], t_all[-1])
if flg_log:
    ax1.set_ylim(max(np.min(Eparl_BKG_avg[Eparl_BKG_avg>0.]), 1.e-5), np.max(Eparl_BKG_max))
    ax1.set_yscale('log')
else:
    ax1.set_ylim(np.min(Eparl_BKG_min), np.max(Eparl_BKG_max))
ax1.legend()

# -> save/show BKG fig (or both)
fig1.tight_layout()
if mpl_get_backend() == 'agg':
    if flg_2pl:
        fname = '/Eparl_BKG_adim_'
    else:
        fname = '/Eparl_EDRvsBKG_adim_'
    if flg_abs:
        fname = fname + 'abs_'
    if flg_uns:
        fname = fname + 'uns_'
    if flg_log:
        fname = fname + 'log_'
    fig1.savefig(opath+'/'+out_dir+fname+'%s_%d_%d.png'%(run_name, ind1, ind2))
else:
    fig1.show()
plt.close(fig1)
#-------
print('Plotting: %fs'%(t_time() - t0,))
#-------
