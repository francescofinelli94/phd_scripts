# coding: utf-8
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

path1 = "/home/frafin/Downloads/Eparl_EDR_EDRmask_BIS/Eparl_EDR2_adim_uns_log_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
path2 = "/home/frafin/Downloads/line_integrated_Eparl_EDR_EDRmask/line_integrated_Eparl_EDR2_adim_uns_log_line_integrated_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"

df1 = pd.read_csv(path1, header=0, index_col=0)
df2 = pd.read_csv(path2, header=0, index_col=0)

plt.plot(df1["t"][1:], df1["avg1"][1:], 'b-', label='avg Eparl')
plt.plot(df1["t"][1:], df1["avg1"][1:]+1.*df1["std1"][1:], 'b--', label='+1std Eparl')
plt.plot(df1["t"][1:], df1["avg1"][1:]+2.*df1["std1"][1:], 'b-.', label='+2std Eparl')
plt.plot(df1["t"][1:], df1["avg1"][1:]+3.*df1["std1"][1:], 'b:', label='+3std Eparl')
plt.plot(df1["t"][1:], df1["max1"][1:], 'b*', label='max Eparl')
plt.plot(df2["t"][1:], df2["avg1"][1:], 'r-', label='avg intEparl')
plt.plot(df2["t"][1:], df2["avg1"][1:]+1.*df2["std1"][1:], 'r--', label='+1std intEparl')
plt.plot(df2["t"][1:], df2["avg1"][1:]+2.*df2["std1"][1:], 'r-.', label='+2std intEparl')
plt.plot(df2["t"][1:], df2["avg1"][1:]+3.*df2["std1"][1:], 'r:', label='+3std intEparl')
plt.plot(df2["t"][1:], df2["max1"][1:], 'r*', label='max intEparl')
plt.xlabel("t")
plt.ylabel("Eparl")
plt.yscale("log")
plt.legend()
plt.show()
