# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt

nx, ny, nz = 128, 64, 256
lll = [2., 1., 4.]
x = np.linspace(0., lll[0]*2.*np.pi, nx+1)[:-1]
y = np.linspace(0., lll[1]*2.*np.pi, ny+1)[:-1]
z = np.linspace(0., lll[2]*2.*np.pi, nz+1)[:-1]
XYZ = np.meshgrid(x, y, z, indexing='ij')
    
field = np.zeros(XYZ[0].shape, dtype=float)
for n in range(5+1):
    if n==0: continue
    for d in range(3):
        a = float(n)#(1. + np.random.random())
        f = np.random.random()*2.*np.pi
        field += a*np.cos(float(n)*XYZ[d]/lll[d] + f)

def old(field, ix0, ix1, iy0, iy1, nz=nz):
    inv_norm = 1./float((ix1+1-ix0)*(iy1+1-iy0))#*nz)
    fftn = np.zeros((nz//2+1,), dtype=np.float64)
    for ix in range(ix0, ix1+1):
        for iy in range(iy0, iy1+1):
            fftn += np.abs(np.fft.rfft(field[ix,iy,:]-np.mean(field[ix,iy,:]))/float(nz))**2
    fftn[1:] *= 4.
    fftn *= inv_norm  # x- and y-average, z-normalization
    psn = fftn
    return psn, np.arange(0., float(nz//2+1))/lll[2]
    
def power_spectrum(field, direction, range1=None, range2=None):
    rvrt = False
    if field.shape[0] in [1, 2, 3]:
        nnn = list(field.shape[1:])
        nd = nnn[direction]
        ps_shape = (3, nd//2+1)
        nc = field.shape[0]
    else:
        nnn = list(field.shape)
        nd = nnn[direction]
        ps_shape = (1, nd//2+1)
        field = field.reshape(1, *nnn)
        rvrt = True
        nc = 1
    del nnn[direction]
    n1, n2 = nnn
    if range1==None:
        i10 = 0
        i11 = n1 - 1
    else:
        i10, i11 = range1
    if range2==None:
        i20 = 0
        i21 = n2 - 1
    else:
        i20, i21 = range2
    ps_field = np.zeros(ps_shape, dtype=np.float64)
    field = np.rollaxis(field, direction+1, 4)
    for i1 in range(i10, i11+1):
        for i2 in range(i20, i21+1):
            for ic in range(nc):
                ps_field += np.abs(np.fft.rfft(field[ic,i1,i2]-np.mean(field[ic,i1,i2]))*(1./float(nd)))**2
    ps_field[:,1:] *= 4.
    ps_field *= 1./float((i11+1-i10)*(i21+1-i20))
    ps_field = np.sum(ps_field, axis=0)
    field = np.rollaxis(field, 3, direction+1)
    if rvrt:
        field = field.reshape(*list(field.shape[1:]))
    return ps_field, np.arange(0., float(nd//2+1))

ps0, k = old(field, 0, nx-1, 0, ny//2-1)
plt.plot(k[1:]*lll[2], ps0[1:], label='old')
ps1, m = power_spectrum(field, 2, [0, nx-1], [0, ny//2-1])
plt.plot(m[1:], ps1[1:], '*', label='ps')
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.show()

