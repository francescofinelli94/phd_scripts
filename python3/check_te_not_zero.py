"""
check if tparle or tperpe have non-positive points
"""

#-----------------------------------------------------
#importing stuff
#---------------
import sys
import gc
import os
import glob

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\ncheck if tparle or tperpe have non-positive points\n')

#------------------------------------------------------
#Init
#----
input_file = sys.argv[1]

plt_show_flg = False
plt_save_flg = True
zoom_flg = True
rms_flg = False

PDplane_flg = False

run,tr,xr=wl.Init(input_file)
ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr
run_name = run.meta['run_name']
w_ele = run.meta['w_ele']
times = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
nx = run.meta['nx']
ny = run.meta['ny']
nz = run.meta['nz']
dx = run.meta['dx']
dy = run.meta['dy']
dz = run.meta['dz']
code_name = run.meta['code_name']
#
if code_name == 'HVM':
    calc = fibo_calc()
    calc.meta = run.meta
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
    B0 = 0.718722  # 1.
    n0 = 1.
    V0 = 0.718722  # 1.
elif code_name == 'iPIC':
    run_label = 'iPIC'
    qom_e = run.meta['msQOM'][0]
    B0 = 0.01
    n0 = 1./(4.*np.pi)
    V0 = 0.01
else:
    print('ERROR: unknown code_name %s'%(code_name))
    sys.exit(-1)
#
opath = run.meta['opath']

#---> loop over times <---
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    #anisotropy
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
        elif code_name == 'iPIC':
            _,B = run.get_EB(ind)
            n_p,_ = run.get_Ion(ind,qom=qom_e)
            Pe = run.get_Press(ind,qom=qom_e)
            tparle,tperpe = ml.proj_tens_on_vec(Pe,B,True)
            tparle = np.divide(tparle,n_p)
            tperpe = np.divide(tperpe,n_p)
            del Pe, n_p, B
        else:
            print('\nWhat code is it?\n')
    else:
        n_e,_ = run.get_Ion(ind)
        tperpe = np.ones(n_e.shape,dtype=float)*run.meta['beta']*0.5*run.meta['teti']
        tparle = tperpe
        del n_e
    #
    t = times[ind]
    num_tparle_zero = np.sum(tparle==0.)
    num_tparle_negative = np.sum(tparle<0.)
    min_tparle = np.min(tparle)
    num_tperpe_zero = np.sum(tperpe==0.)
    num_tperpe_negative = np.sum(tperpe<0.)
    min_tperpe = np.min(tperpe)
    print(f"{ind=}\t{t=}\n{num_tparle_zero=}\t{num_tparle_negative=}\t{min_tparle=}\n{num_tperpe_zero=}\t{num_tperpe_negative=}\t{min_tperpe=}\n")
    
#---> loop over time <---
    #print("\r", end=" ")
    #print("t = ", times[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------
