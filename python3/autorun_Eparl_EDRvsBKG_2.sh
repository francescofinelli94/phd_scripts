#!/bin/bash

DDD=$(date)
SPACER='#----- '$DDD' -----#'

for i in {0..1}
	do for j in {0..1}
		do for k in {0..1}
			do echo $SPACER >> $i$j$k.log
                        echo $SPACER >> $i$j$k.err
			CMD1='python3 Eparl_EDRvsBKG_2.py LF_3d_DH.dat $0 $1 $2'
			CMD2=' >> $0$1$2.log 2>> $0$1$2.err'
			screen -dmS $i$j$k bash -c "$CMD1$CMD2" $i $j $k
		done
	done
done
