#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
import gc
from os.path import join as os_join

import numpy as np
#from matplotlib import use as mpl_use
#from matplotlib import get_backend as mpl_get_backend
#mpl_use('Agg')
import matplotlib.pyplot as plt

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *

#------------------------------------------------------
#Intent
#------
print('\nPower spectra of J, n, E, and B\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------

run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'JnBE_fft'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
mime = run.meta['mime']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

n0 = 1.
Bx0 = 0.718722

vA = Bx0/np.sqrt(n0)

#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------

# -> k
kz = np.arange(0., float(nz//2+1))*2.*np.pi/zl

# -> x,y region
x0, x1, y0, y1 = 27.34, 32.48,  3.79,  4.30  # EDR1
#x0, x1, y0, y1 =  7.35, 12.20, 15.99, 16.68  # EDR2
ix0 = np.argmin(np.abs(x-x0))
ix1 = np.argmin(np.abs(x-x1))
iy0 = np.argmin(np.abs(y-y0))
iy1 = np.argmin(np.abs(y-y1))

# -> norm
inv_norm = 1./float((ix1+1-ix0)*(iy1+1-iy0)*nz)

#---> loop over times <---
plt.close('all')
fig, ax = plt.subplots(2, 2, sharex=True)

my_times = [75., 115., 155.]
indices = []
for t in my_times:
    indices.append(ml.index_of_closest(t,time))

print(" ", end=" ")
#for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
for ind in indices:
#-------------------------
    #------------------------------------------------------
    # Get data
    #----------
    #-------
    t0 = t_time()
    #-------
    # -> magnetic fields and co.
    E, B = run.get_EB(ind)

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
        J = np.array([cx, cy, cz]) # J = rot(B)
        del cx, cy, cz

    # -> densities and currents
    if code_name == 'HVM':
        n_p, _ = run.get_Ion(ind)
    elif code_name == 'iPIC':
        n_p, u_p = run.get_Ion(ind)
        n_e, u_e = run.get_Ion(ind,qom=qom_e)
        J = n_p*u_p - n_e*u_e
        del u_p, n_e, u_e
    
    #-------
    print('\nData loading and computing: %fs'%(t_time() - t0,))
    #-------

    #-------
    # FFT
    #-------
    # -> J
    fftJ = np.zeros((3, nz//2+1), dtype=np.float64)
    for ix in range(ix0, ix1+1):
        for iy in range(iy0, iy1+1):
            for ic in range(3):
                fftJ[ic] += np.abs(np.fft.rfft(J[ic,ix,iy,:]-np.mean(J[ic,ix,iy,:])))**2
    fftJ[:,1:] *= 2.
    fftJ *= inv_norm  # x- and y-average, z-normalization
    psJ = np.sum(fftJ, axis=0)
    ax[0,0].plot(kz[1:], psJ[1:], label='t=%.2f'%(time[ind],))

    # -> n
    fftn = np.zeros((nz//2+1,), dtype=np.float64)
    for ix in range(ix0, ix1+1):
        for iy in range(iy0, iy1+1):
            fftn += np.abs(np.fft.rfft(n_p[ix,iy,:]-np.mean(n_p[ix,iy,:])))**2
    fftn[1:] *= 2.             
    fftn *= inv_norm  # x- and y-average, z-normalization
    psn = fftn
    ax[0,1].plot(kz[1:], psn[1:], label='t=%.2f'%(time[ind],))
    
    # -> B
    fftB = np.zeros((3, nz//2+1), dtype=np.float64)
    for ix in range(ix0, ix1+1):
        for iy in range(iy0, iy1+1):
            for ic in range(3):
                fftB[ic] += np.abs(np.fft.rfft(B[ic,ix,iy,:]-np.mean(B[ic,ix,iy,:])))**2
    fftB[:,1:] *= 2.             
    fftB *= inv_norm  # x- and y-average, z-normalization
    psB = np.sum(fftB, axis=0)
    ax[1,0].plot(kz[1:], psB[1:], label='t=%.2f'%(time[ind],))
    
    # -> E
    fftE = np.zeros((3, nz//2+1), dtype=np.float64)
    for ix in range(ix0, ix1+1):
        for iy in range(iy0, iy1+1):
            for ic in range(3):
                fftE[ic] += np.abs(np.fft.rfft(E[ic,ix,iy,:]-np.mean(E[ic,ix,iy,:])))**2
    fftE[:,1:] *= 2.             
    fftE *= inv_norm  # x- and y-average, z-normalization
    psE = np.sum(fftE, axis=0)
    ax[1,1].plot(kz[1:], psE[1:], label='t=%.2f'%(time[ind],))

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------

ax[0,0].axvline(x=1., c='k', ls='--', label='$k_z d_{\mathrm{p}}=1$')
ax[0,0].axvline(x=1.*np.sqrt(mime), c='r', ls='--', label='$k_z d_{\mathrm{e}}=1$')
ax[0,1].axvline(x=1., c='k', ls='--', label='$k_z d_{\mathrm{p}}=1$')
ax[0,1].axvline(x=1.*np.sqrt(mime), c='r', ls='--', label='$k_z d_{\mathrm{e}}=1$')
ax[1,0].axvline(x=1., c='k', ls='--', label='$k_z d_{\mathrm{p}}=1$')
ax[1,0].axvline(x=1.*np.sqrt(mime), c='r', ls='--', label='$k_z d_{\mathrm{e}}=1$')
ax[1,1].axvline(x=1., c='k', ls='--', label='$k_z d_{\mathrm{p}}=1$')
ax[1,1].axvline(x=1.*np.sqrt(mime), c='r', ls='--', label='$k_z d_{\mathrm{e}}=1$')
ax[0,0].legend()
ax[0,1].legend()
ax[1,0].legend()
ax[1,1].legend()
ax[0,0].set_title('J power spectrum')
ax[0,1].set_title('n power spectrum')
ax[1,0].set_title('B power spectrum')
ax[1,1].set_title('E power spectrum')
ax[1,0].set_xlabel('$k_z d_{\mathrm{p}}$')
ax[1,1].set_xlabel('$k_z d_{\mathrm{p}}$')
plt.tight_layout()
plt.show()
