"""
z-averaged plots from 3D sim.
plots: DBz, E_parl, Jz and Je_perp
compare both current sheets
"""

#-----------------------------------------------------
#importing stuff
#---------------
import sys
import gc
import os
import glob

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *
import thresholds as th

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of DBz, E_parl, Jz and Je_perp.\n')

#------------------------------------------------------
#Init
#----
input_file = sys.argv[1]

plt_show_flg = False
plt_save_flg = True

run,tr,xr=wl.Init(input_file)
ind1,ind2,ind_step = tr
ixmin,ixmax,iymin,iymax = xr
run_name = run.meta['run_name']
w_ele = run.meta['w_ele']
teti = run.meta['teti']
times = run.meta['time']*run.meta['tmult']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
nx = run.meta['nx']
ny = run.meta['ny']
nz = run.meta['nz']
dx = run.meta['dx']
dy = run.meta['dy']
dz = run.meta['dz']
code_name = run.meta['code_name']
#
if code_name == 'HVM':
    calc = fibo_calc()
    calc.meta = run.meta
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
    B0 = 0.718722  # 1.
    n0 = 1.
    V0 = 0.718722  # 1.
elif code_name == 'iPIC':
    run_label = 'iPIC'
    qom_e = run.meta['msQOM'][0]
    B0 = 0.01
    n0 = 1./(4.*np.pi)
    V0 = 0.01
else:
    print('ERROR: unknown code_name %s'%(code_name))
    sys.exit(-1)
#
opath = run.meta['opath']

#-------------------------
# Functions
#-------------------------
def make_custom_cmap(main_color_name='blue'):
    main_color = mpl.colors.to_rgb(main_color_name)
    white = mpl.colors.to_rgb('white')
    black = mpl.colors.to_rgb('black')
    cdict = {'red':   (#(0., white[0], white[0]),
                       (0., main_color[0], main_color[0]),
                       (1., black[0], black[0])),
             'green': (#(0., white[1], white[1]),
                       (0., main_color[1], main_color[1]),
                       (1., black[1], black[1])),
             'blue':  (#(0., white[2], white[2]),
                       (0., main_color[2], main_color[2]),
                       (1., black[2], black[2])),
             'alpha': ((0., 0., 0.),
                       (0.01, 1., 1.),
                       (1., 1., 1.))
    }
    return mpl.colors.LinearSegmentedColormap('CustomCmap_%s'%(main_color_name), cdict)

#---> loop over times <---
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    oot = {'p':{'CI':  None,
                #'MI':  None,
                #'PFHI':None,
                'OFHI':None},
           'e':{'CI':  None,
                #'MI':  None,
                #'PFHI':None,
                'OFHI':None}}
    ccm = {'p':{'CI':  None,
                #'MI':  None,
                #'PFHI':None,
                'OFHI':None},
           'e':{'CI':  None,
                #'MI':  None,
                #'PFHI':None,
                'OFHI':None}}

    #get data
    #magnetic fields and co.
    _,B = run.get_EB(ind)
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp)
    if code_name == 'iPIC' and smooth_flag:
        anisp = gf(anisp,[gfsp,gfsp,gsfp],mode='wrap')
    del pperpp
#
    if w_ele:
        if code_name == 'HVM':
            pparle,pperpe = run.get_Te(ind)
            n,_ = run.get_Ion(ind)
            pparle *= n
            pperpe *= n
            del n
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle)
        if code_name == 'iPIC' and smooth_flag:
            anise = gf(anise,[gfse,gfse,gsfe],mode='wrap')
        del pperpe
#
    #beta parl
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    bparlp = 2.*np.divide(pparlp,B2)
    if code_name == 'iPIC' and smooth_flag:
        bparlp = gf(bparlp,[gfsp,gfsp,gfsp],mode='wrap')
    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if code_name == 'iPIC' and smooth_flag:
            bparle = gf(bparle,[gfse,gfse,gsfe],mode='wrap')
        del pparle
    del B2,pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp
        if w_ele:
            bparle = 4.*np.pi*bparle
    elif code_name != 'HVM':
        print('\nWhat code is it?\n')
#
    #species variables
    p_vars = {'species_name':'Protons',
              'species_tag':'p',
              'b0_mult':1.}
    e_vars = {'species_name':'Electrons',
              'species_tag':'e',
              'b0_mult':teti}
    #
    p,f = th.params['L2014']['pC']['1.0e-2Op']['bM']
    oot['p']['CI'] = np.mean(anisp > th.func[f](bparlp, *p), axis=-1)
    ccm['p']['CI'] = make_custom_cmap('darkorange')

    p,f = th.params['M2012']['pM']['1.0e-2Op']['bM']
    oot['p']['MI'] = np.mean(anisp > th.func[f](bparlp, *p), axis=-1)
    ccm['p']['MI'] = make_custom_cmap('red')

    p,f = th.params['A2016']['pPF']['1.0e-2Op']['bM']
    oot['p']['PFHI'] = np.mean(anisp < th.func[f](bparlp, *p), axis=-1)
    ccm['p']['PFHI'] = make_custom_cmap('forestgreen')

    p,f = th.params['A2016']['pOF']['1.0e-2Op']['bM']
    oot['p']['OFHI'] = np.mean(anisp < th.func[f](bparlp, *p), axis=-1)
    ccm['p']['OFHI'] = make_custom_cmap('darkviolet')

    p,f = th.params['L2013']['eC']['1.0e-3Oe']['bM']
    oot['e']['CI'] = np.mean(anise > th.func[f](bparle, *p), axis=-1)
    ccm['e']['CI'] = make_custom_cmap('darkorange')

    p,f = th.params['G2006']['eM']['1.0e-3Oe']['bM']
    oot['e']['MI'] = np.mean(anise > th.func[f](bparle, *p), axis=-1)
    ccm['e']['MI'] = make_custom_cmap('red')

    p,f = th.params['G2003']['ePF']['1.0e-2Op']['bM']
    oot['e']['PFHI'] = np.mean(anise < th.func[f](bparle, *p), axis=-1)
    ccm['e']['PFHI'] = make_custom_cmap('forestgreen')

    p,f = th.params['H2014']['eOF']['1.0e-3Oe']['bM']
    oot['e']['OFHI'] = np.mean(anise < th.func[f](bparle, *p), axis=-1)
    ccm['e']['OFHI'] = make_custom_cmap('darkviolet')
    #
    t =  times[ind]
    #
    B_ = np.mean(B, axis=-1)
    Psi = wl.psi_2d(B_[:-1],run.meta)
    del B_
    #
    gc.collect()

    #----------------
    #PLOT TIME!!!!!!!
    #----------------
    species_names = {'p':'Protons', 'e':'Electrons'}
    plt.close('all')
    fig,ax = plt.subplots(2,2,figsize=(18,14),sharex=True,sharey=True)
    fig.subplots_adjust(hspace=.06,wspace=.03,top=.92,bottom=.1,left=.055,right=.95) 

    for j, cs in enumerate(['p', 'e']):
        i = 0
        im0 = ax[i,j].pcolormesh(x, y, oot[cs]['CI'].T, shading='gouraud', cmap=ccm[cs]['CI'])
        im0 = ax[i,j].pcolormesh(x, y, oot[cs]['PFHI'].T, shading='gouraud', cmap=ccm[cs]['PFHI'])
        ax[i,j].contour(x, y, Psi.T, 8, colors='black')
        if j == 0:
            ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
        ax[i,j].set_title('%s - $z$ avg.'%(species_names[cs],), pad=10.)
    #
        i = 1
        im1 = ax[i,j].pcolormesh(x, y, oot[cs]['MI'].T, shading='gouraud', cmap=ccm[cs]['MI'])
        im1 = ax[i,j].pcolormesh(x, y, oot[cs]['OFHI'].T, shading='gouraud', cmap=ccm[cs]['OFHI'])
        ax[i,j].contour(x, y, Psi.T, 8, colors='black')
        if j == 0:
            ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
        ax[i,j].set_xlabel('$x\quad [d_{\mathrm{p}}]$')

    fig.suptitle('$t=%.2f\;[\Omega_{\mathrm{p}}^{-1}]$'%(t,))

    if plt_show_flg:
        plt.show()
    if plt_save_flg:
        out_dir = 'plot_3d_out_of_thresholds_both_cs_BIS'
        ml.create_path(opath+'/'+out_dir)
        out_dir += '/comp'
        fig_name = 'out_of_thresholds_'
        fig_name += '__%s'%(run_name,)
        for cs in ['p', 'e']:
            out_dir += '__' + cs
            fig_name += '__' + cs + '_ind%d'%ind
        fig_name += '.png'
        ml.create_path(opath+'/'+out_dir)
        fig.savefig(opath+'/'+out_dir+'/'+fig_name)
    plt.close()

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", times[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------
