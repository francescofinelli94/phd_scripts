# coding: utf-8
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
path1 = "/home/frafin/Downloads/Eparl_EDR_EDRmask_BIS/Eparl_EDR2_adim_uns_log_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
path2 = "/home/frafin/Downloads/line_integrated_Eparl_EDR_EDRmask/line_integrated_Eparl_EDR2_adim_uns_log_line_integrated_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
df1 = pd.read_csv(path1, header=0, index_col=0)
df2 = pd.read_csv(path2, header=0, index_col=0)
it0 = np.argmin(np.abs(df1["t"]-160.))
it1 = np.argmin(np.abs(df1["t"]-220.))
m1 = np.mean(df1["max1"][it0:it1+1])
m2 = np.mean(df2["max1"][it0:it1+1])
plt.plot(df1["t"][1:], df1["max1"][1:], 'b-', label='max Eparl')
plt.plot(df2["t"][1:], df2["max1"][1:], 'r-', label='max intEparl')
plt.plot(df1["t"][it0:it1+1], df1["t"][it0:it1+1]*0.+m1, 'b--', label=f"{m1 = }")
plt.plot(df2["t"][it0:it1+1], df2["t"][it0:it1+1]*0.+m2, 'r--', label=f"{m2 = }")
plt.xlabel("t")
plt.ylabel("Eparl")
plt.yscale("log")
plt.legend()
plt.show()
