# import
import sys
import gc

import numpy as np
from matplotlib import use as mpl_use
mpl_use('Agg')
import matplotlib.pyplot as plt
import pandas as pd

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

# init
run,tr,xr = wl.Init(sys.argv[1])
ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'tearing_wide'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
mime = run.meta['mime']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

n0 = 1.
Bx0 = 0.718722
vA = Bx0/np.sqrt(n0)
L1 = 0.85
L2 = 1.7
y1_ = yl/(2.*(1.+L2/L1))
y2_ = yl - y1_*L2/L1

# select region
#x0, x1, y0, y1 = 27.34, 32.48,  3.79,  4.30
x0, x1, y0, y1 = 27.34, 32.48, y1_-L1, y1_+L1
ix0 = np.argmin(np.abs(x-x0))
ix1 = np.argmin(np.abs(x-x1))
iy0 = np.argmin(np.abs(y-y0))
iy1 = np.argmin(np.abs(y-y1))

# save modes init
m0 = 1
m1 = nx//3
modes_dict = {}
modes_dict["t"] = []
for m in range(m0, m1+1):
    modes_dict[f"mod{m}"] = []

# power spectrum
def power_spectrum(field, direction, range1=None, range2=None):
    rvrt = False
    if field.shape[0] in [1, 2, 3]:
        nnn = list(field.shape[1:])
        nd = nnn[direction]
        ps_shape = (3, nd//2+1)
        nc = field.shape[0]
    else:
        nnn = list(field.shape)
        nd = nnn[direction]
        ps_shape = (1, nd//2+1)
        field = field.reshape(1, *nnn)
        rvrt = True
        nc = 1
    del nnn[direction]
    n1, n2 = nnn
    if range1==None:
        i10 = 0
        i11 = n1 - 1
    else:
        i10, i11 = range1
    if range2==None:
        i20 = 0
        i21 = n2 - 1
    else:
        i20, i21 = range2
    ps_field = np.zeros(ps_shape, dtype=np.float64)
    field = np.rollaxis(field, direction+1, 4)
    for i1 in range(i10, i11+1):
        for i2 in range(i20, i21+1):
            for ic in range(nc):
                ps_field += np.abs(np.fft.rfft(field[ic,i1,i2]-np.mean(field[ic,i1,i2]))*(1./float(nd)))**2
    ps_field[:,1:] *= 4.
    ps_field *= 1./float((i11+1-i10)*(i21+1-i20))
    ps_field = np.sum(ps_field, axis=0)
    field = np.rollaxis(field, 3, direction+1)
    if rvrt:
        field = field.reshape(*list(field.shape[1:]))
    return ps_field, np.arange(0., float(nd//2+1))

#---> loop over times <---
plt.close('all')
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    # get fields
    _, B = run.get_EB(ind)
    By = B[1]
    del B

    # compute spectrum
    psBy, mx = power_spectrum(By, 0, [iy0, iy1], [0, nz-1])

    # save modes
    modes_dict["t"].append(time[ind])
    for m in range(m0, m1+1):
        modes_dict[f"mod{m}"].append(psBy[m])

    # plot
    fig = plt.figure(figsize=(18, 8))
    plt.subplot(121)
    plt.plot(mx[1:], psBy[1:])
    plt.axvline(x=zl/(L1*2.*np.pi), c='k', ls='--', label='kx L1 ~ 1')
    plt.xlabel("mx")
    plt.ylabel("By power spectrum")
    plt.yscale('log')
    plt.xscale('log')
    plt.title(f"t={time[ind]}")
    plt.legend()

    plt.subplot(122)
    vmax = np.max(np.abs(By))
    vmin = - vmax
    plt.pcolormesh(x, y, By[..., nz//2].T, shading='gouraud', cmap='seismic', vmin=vmin, vmax=vmax)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("By z=Lz/2")
    plt.colorbar()

    plt.tight_layout()
    plt.savefig(opath+'/'+out_dir+'/tearing_%d.png'%(ind))
    plt.close()

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------

# save modes
df = pd.DataFrame(modes_dict)
df.to_csv(opath+'/'+out_dir+'/tearing_%d_%d.csv'%(ind1, ind2))
