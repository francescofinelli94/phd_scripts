# coding: utf-8

#---------------------------------------------------------------------------------------
#-(1)-----21.12.18----FDP:S,F-----------------------------------------------------------
#-(2)-----30.12.18----FDP:S,J,R---------------------------------------------------------
#-(3)-----02.04.19----FDP:S,P,L---------------------------------------------------------
#-(4)-----04.04.19----FDP:S,J,L---------------------------------------------------------
#---------------------------------------------------------------------------------------
#-(alpha)-19.07.19----FDP:S,J,L---------------------------------------------------------
#-(beta0)-12.11.19----FDP:S,F-----------------------------------------------------------
#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------

import collections 
import numpy as np
import os

#---------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------
#this should read Multi-FLuid codes outputs 
#for now it reads the 2fluid_3d outputs only, but might be expanded to get also those from 3_fluid etc.
class from_MFL (object):

  def __init__(self, 
      address,
      prefix,
      nproc):
    """
    ------------------------------------------------------------------------------------
      creates the object to retrieve data from Multi-FLuid codes
    ------------------------------------------------------------------------------------
    address      [address] where your data are (folder with segs inside)
    prefix       [str] name of the simulation run you are using
    nproc        [int] number of processors in the run you are using
    ------------------------------------------------------------------------------------
    """

    self.address = address
    self.prefix = prefix
    self.nproc = nproc
    self.segs = {}
    self.meta = {}

  #------------------------------------------------------------
  def help(self):
    print('For further help, please shout:')
    print('!!!SIIIIIIIIIIIID!!!')

  #------------------------------------------------------------
  def get_meta(self,
      extra_address = '',
      silent = True):
    """
    ------------------------------------------------------------------------------------
      fills the metadata list 
    ------------------------------------------------------------------------------------
    extra_address = ''     [address] to reach any subfolder where your meta-data are
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    
    #get mesh infos from input_parameters (I take the input_parameters from subfolder 01)
    infos = open(os.path.join(self.address,extra_address,self.prefix+'_cpu.dat'),'r')
    
    nx, ny, nz, nwx, nwy, nwz = map(float, infos.readline().split())
    self.meta['nx'] = int(nx / nwx)
    self.meta['ny'] = int(ny / nwy)
    self.meta['nz'] = int(nz / nwz)

    self.meta['xl'], self.meta['yl'], self.meta['zl'] = map(float, infos.readline().split())
    self.meta['dx'] = self.meta['xl'] / self.meta['nx'] #(nx_original - 1)
    self.meta['dy'] = self.meta['yl'] / self.meta['ny']
    self.meta['dz'] = self.meta['zl'] / self.meta['nz']

    self.meta['nnn'] = (self.meta['nx'], self.meta['ny'], self.meta['nz'])
    self.meta['lll'] = (self.meta['xl'], self.meta['yl'], self.meta['zl'])
    self.meta['ddd'] = (self.meta['dx'], self.meta['dy'], self.meta['dz']) 

    self.meta['dt'] = 'BOH WHO KNOWS'
    self.meta['ts'] = self.meta['dt']      #this is just for jeremy :)
    
    infos.close()
    self.meta['space_dim'] = str(np.count_nonzero(np.array(self.meta['nnn'])))+'D'

    #get time segment infos from all subdirectories: 
    segments = sorted([d for d in os.listdir(self.address) if os.path.isdir(os.path.join(self.address,d))])

    for seg in segments:
      infot = open(os.path.join(self.address,seg,self.prefix+'_cpu.dat'),'r')
      nexits = len(infot.readlines())
      self.segs[seg] = []
      infot.seek(0)
      for time_exit_num in range(nexits):
        time_exit = '%.3f' %float(infot.readline().split()[2])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        self.segs[seg].append(time_exit)
      infot.close()
    
    if not silent: print(sorted(self.segs))

    #add informations on species (sorry, some of these are hard-coded - change them in future!)
    self.meta['teti'] = 1. #HORRIBLE! PLEASE CHANGE THIS !! 

    self.meta['nss'] = 2     #number of species

    species  = []
    species.append('ion     ')
    species.append('electron')

    charges = np.zeros([self.meta['nss']])
    charges[0] = 1.
    charges[1] = -1.

    masses = np.zeros([self.meta['nss']])
    masses[0] = 1.
    masses[1] = 0. #1./self.meta['mime']

    self.meta['species']  = species
    self.meta['charges']  = { kk:charges[k] for k,kk in enumerate(species)}
    self.meta['masses']   = { kk:masses[k] for k,kk in enumerate(species)}

    if not silent : 
      print('MFL_'+self.meta['space_dim']+'> cell number               :  ', self.meta['nnn'])
      print('MFL_'+self.meta['space_dim']+'> domain size               :  ', self.meta['lll'])
      print('MFL_'+self.meta['space_dim']+'> mesh spacing              :  ', self.meta['ddd'])
      print('MFL_'+self.meta['space_dim']+'> time step                 :  ', self.meta['dt'])
      print('MFL_'+self.meta['space_dim']+'> species                   :  ', self.meta['species'])
      for i in range(self.meta['nss']):
        print('          '+species[i]+' charge                :  ', self.meta['charges'][species[i]])
        print('          '+species[i]+' mass                  :  ', self.meta['masses'][species[i]])
      print('MFL_'+self.meta['space_dim']+'> teti                      :  ', self.meta['teti'])

  #------------------------------------------------------------
  def get_B(self, 
      seg,
      exit_num,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the B field at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    exit_num               [int] number of time exit (0,1,...)
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nx,ny,nz = self.meta['nnn']
    Bx = np.empty([nx,ny,nz])
    By = np.empty([nx,ny,nz])
    Bz = np.empty([nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_EB.da'), 'r')
    
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        #if time_exit != self.segs[seg][exit_num] : print('=====WRONG=EXIT=====')
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('jumping time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)

        for il in range(nx*ny*nz) : 
          rf.readline()

      #fill data vectors
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      #if time_exit != self.segs[seg][exit_num] : print('=====WRONG=EXIT=====')
      iyi, iyf, izi, izf = map(int, rf.readline().split())
      if not silent: print('reading time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)

      for iz in range(-1,izf-izi):
        for iy in range(-1,iyf-iyi):
          for ix in range(nx):
            EB_line = rf.readline().split()
            Bx[ix,iy,iz] = float(EB_line[3])
            By[ix,iy,iz] = float(EB_line[4])
            Bz[ix,iy,iz] = float(EB_line[5])

      rf.close()

    if (fibo_obj != None) :
      fibo_obj.data['B_x_'+time_exit] = Bx
      fibo_obj.data['B_y_'+time_exit] = By
      fibo_obj.data['B_z_'+time_exit] = Bz
    else: return np.array([Bx, By, Bz])

    if not silent: print('done with the reading!')

  #------------------------------------------------------------
  def get_E(self,
      seg,
      exit_num,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the B field at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    exit_num               [int] number of time exit (0,1,...)
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns valuesd
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    
    #create data vectors
    nx,ny,nz = self.meta['nnn']
    Ex = np.empty([nx,ny,nz])
    Ey = np.empty([nx,ny,nz])
    Ez = np.empty([nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_EB.da'), 'r')
    
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('jumping time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)
        for il in range(nx*ny*nz) : 
          rf.readline()

      #fill data vectors
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      iyi, iyf, izi, izf = map(int, rf.readline().split())
      if not silent: print('reading time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)
      for iz in range(-1,izf-izi):
        for iy in range(-1,iyf-iyi):
          for ix in range(nx):
            EB_line = rf.readline().split()
            Ex[ix,iy,iz] = float(EB_line[0])
            Ey[ix,iy,iz] = float(EB_line[1])
            Ez[ix,iy,iz] = float(EB_line[2])

      rf.close()

    if (fibo_obj != None) :
      fibo_obj.data['E_x_'+time_exit] = Ex
      fibo_obj.data['E_y_'+time_exit] = Ey
      fibo_obj.data['E_z_'+time_exit] = Ez
    else: return np.array([Ex, Ey, Ez])

    if not silent: print('done with the reading!')

  #------------------------------------------------------------
  def get_Ui(self,
      seg,
      exit_num,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the B field at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    exit_num               [int] number of time exit (0,1,...)
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """

    #create data vectors
    nx,ny,nz = self.meta['nnn']
    Uix = np.empty([nx,ny,nz])
    Uiy = np.empty([nx,ny,nz])
    Uiz = np.empty([nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_U.da'), 'r')
    
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('jumping time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)
        for il in range(nx*ny*nz) : 
          rf.readline()

      #fill data vectors
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      iyi, iyf, izi, izf = map(int, rf.readline().split())
      if not silent: print('reading time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)
      for iz in range(-1,izf-izi):
        for iy in range(-1,iyf-iyi):
          for ix in range(nx):
            U_line = rf.readline().split()
            Uix[ix,iy,iz] = float(U_line[3])
            Uiy[ix,iy,iz] = float(U_line[4])
            Uiz[ix,iy,iz] = float(U_line[5])

      rf.close()

    if (fibo_obj != None) :
      fibo_obj.data['ui_x_'+time_exit] = Uix
      fibo_obj.data['ui_y_'+time_exit] = Uiy
      fibo_obj.data['ui_z_'+time_exit] = Uiz
    else: return np.array([Uix, Uiy, Uiz])

    if not silent: print('done with the reading!')

  #------------------------------------------------------------
  def get_Ue(self,
      seg,
      exit_num,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the B field at the nth exit of specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    exit_num               [int] number of time exit (0,1,...)
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    
    #create data vectors
    nx,ny,nz = self.meta['nnn']
    Uex = np.empty([nx,ny,nz])
    Uey = np.empty([nx,ny,nz])
    Uez = np.empty([nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_U.da'), 'r')
    
      #jump to the correct line in the file
      for l in range(exit_num):
        time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
        time_exit = time_exit.zfill(8)        # ..and three decimal digits
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('jumping time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)
        for il in range(nx*ny*nz) : 
          rf.readline()

      #fill data vectors
      time_exit = '%.3f' %float(rf.readline().split()[0])  #writes time exit with four integer .. 
      time_exit = time_exit.zfill(8)        # ..and three decimal digits
      iyi, iyf, izi, izf = map(int, rf.readline().split())
      if not silent: print('reading time:' , time_exit, 'procs', ip, iyi, iyf, izi, izf)
      for iz in range(-1,izf-izi):
        for iy in range(-1,iyf-iyi):
          for ix in range(nx):
            U_line = rf.readline().split()
            Uex[ix,iy,iz] = float(U_line[0])
            Uey[ix,iy,iz] = float(U_line[1])
            Uez[ix,iy,iz] = float(U_line[2])

      rf.close()

    if (fibo_obj != None) :
      fibo_obj.data['ue_x_'+time_exit] = Uex
      fibo_obj.data['ue_y_'+time_exit] = Uey
      fibo_obj.data['ue_z_'+time_exit] = Uez
    else: return np.array([Uex, Uey, Uez])

    if not silent: print('done with the reading!')

  #------------------------------------------------------------
  def get_seg_U(self,
      seg,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the ue,ui vector fields in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    
    nexits = len(self.segs[seg])
    nx,ny,nz = self.meta['nnn']

    #create data vectors
    Uex = np.empty([nexits,nx,ny,nz])
    Uey = np.empty([nexits,nx,ny,nz])
    Uez = np.empty([nexits,nx,ny,nz])
    Uix = np.empty([nexits,nx,ny,nz])
    Uiy = np.empty([nexits,nx,ny,nz])
    Uiz = np.empty([nexits,nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_U.da'), 'r')
    
      #read all data in the file
      for l in range(nexits):
        t = float(rf.readline().split()[0])
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('reading time:' , t, 'procs', ip, iyi, iyf, izi, izf)
        for iz in range(-1,izf-izi):
          for iy in range(-1,iyf-iyi):
            for ix in range(nx):
              U_line = rf.readline().split()
              Uex[l,ix,iy,iz] = float(U_line[0])
              Uey[l,ix,iy,iz] = float(U_line[1])
              Uez[l,ix,iy,iz] = float(U_line[2])
              Uix[l,ix,iy,iz] = float(U_line[3])
              Uiy[l,ix,iy,iz] = float(U_line[4])
              Uiz[l,ix,iy,iz] = float(U_line[5])

      rf.close()

    if (fibo_obj != None) :
      for l in range(nexits):
        time_exit = self.segs[seg][l]
        fibo_obj.data['ue_x_'+time_exit] = Uex[l,:,:,:]
        fibo_obj.data['ue_y_'+time_exit] = Uey[l,:,:,:]
        fibo_obj.data['ue_z_'+time_exit] = Uez[l,:,:,:]
        fibo_obj.data['ui_x_'+time_exit] = Uix[l,:,:,:]
        fibo_obj.data['ui_y_'+time_exit] = Uiy[l,:,:,:]
        fibo_obj.data['ui_z_'+time_exit] = Uiz[l,:,:,:]
    else: return np.array([Uex, Uey, Uez]), np.array([Uix, Uiy, Uiz])

    if not silent: print('done with the reading!')



  #------------------------------------------------------------
  def get_seg_EB(self,
      seg,        #segment considered (str)
      fibo_obj = None,  #fibo object you are considering - if None, EB values will just be returned 
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the E,B vector fields in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """
    
    nexits = len(self.segs[seg])
    nx,ny,nz = self.meta['nnn']

    #create data vectors
    Ex = np.empty([nexits,nx,ny,nz])
    Ey = np.empty([nexits,nx,ny,nz])
    Ez = np.empty([nexits,nx,ny,nz])
    Bx = np.empty([nexits,nx,ny,nz])
    By = np.empty([nexits,nx,ny,nz])
    Bz = np.empty([nexits,nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_EB.da'), 'r')
    
      #read all data in the file
      for l in range(nexits):
        t = float(rf.readline().split()[0])
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('reading time:' , t, 'procs', ip, iyi, iyf, izi, izf)
        for iz in range(-1,izf-izi):
          for iy in range(-1,iyf-iyi):
            for ix in range(nx):
              EB_line = rf.readline().split()
              Ex[l,ix,iy,iz] = float(EB_line[0])
              Ey[l,ix,iy,iz] = float(EB_line[1])
              Ez[l,ix,iy,iz] = float(EB_line[2])
              Bx[l,ix,iy,iz] = float(EB_line[3])
              By[l,ix,iy,iz] = float(EB_line[4])
              Bz[l,ix,iy,iz] = float(EB_line[5])

      rf.close()

    if (fibo_obj != None) :
      for l in range(nexits):
        time_exit = self.segs[seg][l]
        fibo_obj.data['E_x_'+time_exit] = Ex[l,:,:,:]
        fibo_obj.data['E_y_'+time_exit] = Ey[l,:,:,:]
        fibo_obj.data['E_z_'+time_exit] = Ez[l,:,:,:]
        fibo_obj.data['B_x_'+time_exit] = Bx[l,:,:,:]
        fibo_obj.data['B_y_'+time_exit] = By[l,:,:,:]
        fibo_obj.data['B_z_'+time_exit] = Bz[l,:,:,:]
    else: return np.array([Ex, Ey, Ez]), np.array([Bx, By, Bz])

    if not silent: print('done with the reading!')

  #------------------------------------------------------------
  def get_seg_DPJ(self,
      seg,
      fibo_obj = None,
      silent = True): 
    """
    ------------------------------------------------------------------------------------
      gets the n,Pe,Pi,Trac scalar fields in the specified data segment
    ------------------------------------------------------------------------------------
    seg                    [str] segment name
    fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
    silent = True          [bool] don't you want to see all infos printed on shell?
    ------------------------------------------------------------------------------------
    """    

    nexits = len(self.segs[seg])
    nx,ny,nz = self.meta['nnn']

    #create data vectors
    Den  = np.empty([nexits,nx,ny,nz])
    Pe   = np.empty([nexits,nx,ny,nz])
    Pi   = np.empty([nexits,nx,ny,nz])
    Trac = np.empty([nexits,nx,ny,nz])

    for ip in range (self.nproc):

      rf = open(os.path.join(self.address,seg,self.prefix+'_'+str(ip).zfill(4)+'_DPJ.da'), 'r')
    
      #read all data in the file
      for l in range(nexits):
        t = float(rf.readline().split()[0])
        iyi, iyf, izi, izf = map(int, rf.readline().split())
        if not silent: print('reading time:' , t, 'procs', ip, iyi, iyf, izi, izf)
        for iz in range(-1,izf-izi):
          for iy in range(-1,iyf-iyi):
            for ix in range(nx):
              DPJ_line = rf.readline().split()
              Den[l,ix,iy,iz] = float(DPJ_line[0])
              Pe[l,ix,iy,iz] = float(DPJ_line[1])
              Pi[l,ix,iy,iz] = float(DPJ_line[2])
              Trac[l,ix,iy,iz] = float(DPJ_line[3])

      rf.close()

    if (fibo_obj != None) :
      for l in range(nexits):
        time_exit = self.segs[seg][l]
        fibo_obj.data['Den_'+time_exit] = Den[l,:,:,:]
        fibo_obj.data['Pe_'+time_exit] = Pe[l,:,:,:]
        fibo_obj.data['Pi_'+time_exit] = Pi[l,:,:,:]
        fibo_obj.data['Trac_'+time_exit] = Trac[l,:,:,:]
    else: return Den, Pe, Pi, Trac

    if not silent: print('done with the reading!')

