# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 17:25:35 2017

@author: fpucci
"""
import numpy as np
import time

def compute_1D_psd(x, N, dt, parc=False): 
 """ This function compute the power spectral density E(fn) of a
    field x. E = int E(fn) dfn, where E is the total energy.
    E(fn) has the dimension of energy divided by frequency.
   
    Inputs
    x  - numpy array representing the function whose P(fn) is computed
    N  - number of elements in x
    dt - if x(t) is a time series, dt is the sample rate

    Outputs
    freq - frequencies = 0, 1.0/(N*dt), 2.0/(N*dt), ..., N/(2.0*N*dt) (if N is even) or (N-1)/(2.0*N*dt) (if N is odd)      
    spec - squared module of Fourier coefficients xt (dimension N//2+1): 
           |xt(fn)|^2     for n = 0
           2.0*|xt(fn)|^2 for n >= 1  (if N is odd this contains the largest frequency (N-1)/(2.0*N*dt) )
           |xt(fn)|^2     for n = N/2 (if N is even, there is no fN/2 if N is odd)
    ef   - power spectral density                   
 """   
  # Compute the transform and the frequencies
 xt      = np.fft.rfft(x)/float(N)  # fourier transform of x
 freq    = np.fft.rfftfreq(N,d=dt)  # frequencies involved
 dfreq   = 1.0/(float(N)*dt)        # frequency spacing 
  # compute the spectrum 
 spec = np.zeros(N//2+1) 
 Ef   = np.zeros(N//2+1) 
 spec[0]  = (np.real(xt[0])**2+np.imag(xt[0])**2)  
 spec[1:] = 2.0*(np.real(xt[1:])**2+np.imag(xt[1:])**2) 
 if N % 2 == 0 :    #correct the last term in case N is even     
   spec[-1] /= 2.0
  #Compute the power spectral density
 Ef[0]    =2.0*float(N)/dfreq*spec[0]
 Ef[1:-1] =float(N)/dfreq*spec[1:-1]
 if N % 2 == 0:     #last term in case N is even
  Ef[-1]   = 2.0*float(N)/dfreq*spec[-1]
 else:             #last term in case N is odd
  Ef[-1]   = float(N)/dfreq*spec[-1]
     
 if parc:
   # Verify Parceval
  parc_real = np.sum(x**2)/float(N)
  parc_four = np.sum(spec)
  print "Parceval Theorem (the first two values have to be the same)"
  print "Average energy in real space      : {:.16E} ".format(parc_real)
  print "Sum of the element of the spectrum: {:.16E} ".format(parc_four)
  print "Mean value squared                : {:.16E} ".format(spec[0]) 
  print "Mean value of squared fluctuations: {:.16E} ".format(np.sum(spec[1:])) 
  print "Total energy                      : {:.16E} ".format(np.sum(x**2))  
  print "Total energy from int Ef df       : {:.16E} ".format(((Ef[0]+Ef[-1])/2.0+np.sum(Ef[1:-1]))/(float(N)*dt))
  
 return freq, spec, Ef


def compute_2D_spectrum(v,nx,ny,parce=False):
    v_t = np.fft.rfftn(v)/float(nx*ny)    
    spec = np.empty((nx,ny//2+1))
    spec[:,0]  = v_t[:,0].real**2 + v_t[:,0].imag**2
    spec[:,1:-1]  = 2.0*(v_t[:,1:-1].real**2 + v_t[:,1:-1].imag**2)
    spec[:,-1] = v_t[:,-1].real**2 + v_t[:,-1].imag**2 
    if parce: 
      print "Parceval"
      print "real, fourier", np.sum(v**2)/float(nx*ny), np.sum(spec)
    return spec

def compute_1D_iso_from_2D_spectrum(v,nx,ny,dx,dy,parce=False):
    #compute spectrum
    begin = time.time()
    v_t = np.fft.rfftn(v)/float(nx*ny)    
    spec = np.empty((nx,ny//2+1))
    spec[:,0]  = v_t[:,0].real**2 + v_t[:,0].imag**2
    spec[:,1:-1]  = 2.0*(v_t[:,1:-1].real**2 + v_t[:,1:-1].imag**2)
    spec[:,-1] = v_t[:,-1].real**2 + v_t[:,-1].imag**2  
    if parce:
      print "Parceval"
      print "real, fourier", np.sum(v**2)/float(nx*ny), np.sum(spec)
    #compute 1D isotropic spectrum
    kx = np.fft.fftfreq(nx,d=dx/(2.0*np.pi))
    ky = np.fft.rfftfreq(ny,d=dy/(2.0*np.pi))
    dkx = kx[1]
    dky = ky[1]
    kxmax = max(kx)
    kymax = max(ky)
    dk = max(dkx,dky) 
    kmax = min(kxmax,kymax)
    k = np.arange(0.0, kmax + dk/2.0 , dk)
    isospec = np.zeros(len(k))
    for i in range(len(kx)):
     for j in range(len(ky)):
       kappa = np.sqrt( kx[i]**2 + ky[j]**2 )
#       if int(kappa/dk) < len(k)-1: 
       if int(kappa/dk) < len(k): 
        isospec[int(kappa/dk)] += spec[i,j]
#       else:
#        isospec[-1] += spec[i,j]    
    print "Number of points nx, ny", nx,ny    
    print "Elapsed time for computing 1D iso spectrum", time.time()-begin
    
    return  k, isospec

def compute_1D_reduced_from_2D_spectrum(field,n,d,axis_to_reduce=1):
    begin = time.time()
    if axis_to_reduce==1:
        print 'Computing spectra along kx averaged along ky'
        av_spec = np.empty((field.shape[0]//2+1))
        for i in range(field.shape[axis_to_reduce]):
            k, spec, e = compute_1D_psd(field[:,i],n,d)
            av_spec += spec
        k = 2.0*np.pi*k
        av_spec /= float(field.shape[axis_to_reduce])
    print 'Elapsed time to compute reduced spectum', time.time()-begin        
    return k, av_spec    

def compute_1D_reduced_spectrum(field,nx,ny,nz,dx,dy,dz,axis='0',checktime=False):

    if checktime:
        begin = time.time()

    #print 'axis=', axis
    if axis=='0':
        print 'Computing kx-spectrum averaged over the domain on y and z'
        av_spec = np.zeros((field.shape[0]//2+1))
        for iy in range(ny):
           for iz in range(nz):
               k, spec, e = compute_1D_psd(field[:,iy,iz],nx,dx)
               av_spec += spec
        k = 2.0*np.pi*k
        av_spec /= float(ny*nz)
        #print 'esco axis=0'
        
    if axis=='1':
        print 'Computing ky-spectrum averaged over the domain on x and z'
        av_spec = np.zeros((field.shape[1]//2+1))
        for ix in range(nx):
           for iz in range(nz):
               k, spec, e = compute_1D_psd(field[ix,:,iz],ny,dy)
               av_spec += spec
        k = 2.0*np.pi*k
        av_spec /= float(nx*nz)
        #print 'esco axis=1'

    if axis=='2':
        print 'Computing kz-spectrum averaged over the domain on x and y'
        av_spec = np.zeros((field.shape[2]//2+1))
        for ix in range(nx):
           for iy in range(ny):
               k, spec, e = compute_1D_psd(field[ix,iy,:],nz,dz)
               av_spec += spec
        k = 2.0*np.pi*k
        av_spec /= float(nx*ny)
        #print 'esco axis=2'

    if axis=='all':
        
        print 'Computing kx-spectrum averaged over the domain on y and z'
        av_spec_0 = np.zeros((field.shape[0]//2+1))
        for iy in range(ny):
          for iz in range(nz):
            k0, spec, e = compute_1D_psd(field[:,iy,iz],nx,dx)
            av_spec_0 += spec
        k0 = 2.0*np.pi*k0       
        av_spec_0 /= float(ny*nz)

        print 'Computing ky-spectrum averaged over the domain on x and z'
        av_spec_1 = np.zeros((field.shape[1]//2+1))
        for ix in range(nx):
          for iz in range(nz):
            k1, spec, e = compute_1D_psd(field[ix,:,iz],ny,dy)
            av_spec_1 += spec
        k1 = 2.0*np.pi*k1
        av_spec_1 /= float(nx*nz)

        print 'Computing kz-spectrum averaged over the domain on x and y'
        av_spec_2 = np.zeros((field.shape[2]//2+1))
        for ix in range(nx):
          for iy in range(ny):
            k2, spec, e = compute_1D_psd(field[ix,iy,:],nz,dz)
            av_spec_2 += spec
        k2 = 2.0*np.pi*k2
        av_spec_2 /= float(nx*ny)
        #print 'esco axis=all'

    if checktime:
        print 'Elapsed time to compute reduced spectum', time.time()-begin        

    if axis=='all':
        return [k0,k1,k2],[av_spec_0,av_spec_1,av_spec_2]     
    else:
        return k,av_spec    


def compute_2D_transform(v,nx,ny,dx,dy):
    v_t = np.fft.rfftn(v)    
    kx = np.fft.fftshift(np.fft.fftfreq(nx,d=dx/(2.0*np.pi)))
    ky = np.fft.rfftfreq(ny,d=dy/(2.0*np.pi))
    for j in range(len(ky)):
        v_t[:,j] = np.fft.fftshift(v_t[:,j])
    dkx = kx[1]-kx[0]
    dky = ky[1]-ky[0]    
    return dkx, dky, kx, ky, v_t

def fourier_filter2D(v,nx,ny,dx,dy,kmax):
    #compute spectrum
    begin = time.time()
    v_t = np.fft.rfftn(v)    
    kx = np.fft.fftfreq(nx,d=dx/(2.0*np.pi))
    ky = np.fft.rfftfreq(ny,d=dy/(2.0*np.pi))
    kxx, kyy = np.meshgrid(kx, ky, indexing='ij')
    #print 'kxx.max', np.max(kxx)
    #print 'kxx.min', np.min(kxx)
    #print 'kyy.max', np.max(kyy)
    #print 'kyy.min', np.min(kyy)
    k = np.sqrt(kxx**2.0+kyy**2.0)
    #print 'k.max', np.max(k)
    #print 'k.min', np.min(k)
    v_t[np.where(k>kmax)] = 1e-12
    fil_v = np.fft.irfftn(v_t)
    print "Number of points nx, ny", nx,ny    
    print "Elapsed time for 2D fourier filtering", time.time()-begin
    return fil_v
    
def fourier_filter3D(v,nx,ny,nz,dx,dy,dz,kmax):
    #compute spectrum
    begin = time.time()
    v_t = np.fft.rfftn(v)    
    kx = np.fft.fftfreq(nx,d=dx/(2.0*np.pi))
    ky = np.fft.fftfreq(ny,d=dy/(2.0*np.pi))
    kz = np.fft.rfftfreq(nz,d=dz/(2.0*np.pi))
    kxx, kyy, kzz = np.meshgrid(kx, ky, kz, indexing='ij')
    #print 'kxx.max', np.max(kxx)
    #print 'kxx.min', np.min(kxx)
    #print 'kyy.max', np.max(kyy)
    #print 'kyy.min', np.min(kyy)
    k = np.sqrt(kxx**2.0+kyy**2.0+kzz**2.0)
    #print 'k.max', np.max(k)
    #print 'k.min', np.min(k)
    v_t[np.where(k>kmax)] = 1e-12
    fil_v = np.fft.irfftn(v_t)
    print "Number of points nx, ny, nz", nx,ny,nz    
    print "Elapsed time for 3D fourier filtering", time.time()-begin
    return fil_v


def compute_detrended_f(f,dt,del_last=True):
     """This function take a discrete signal f
       and make it periodic with zero mean.
       The last value (equal to the first one) is eliminated.

       Inputs     
             f  - 1D function to be detrended. f has N points
             dt - sampling function
       Outputs
             f_d - detrended f
     """
     N  = f.size
     x_trend = np.arange(0, float(N)*dt, dt)
     m_trend = (f[-1]-f[0])/(x_trend[-1]-x_trend[0])  
     q_trend = (f[0]*x_trend[-1]-f[-1]*x_trend[0])/(x_trend[-1]-x_trend[0])
     y_trend = m_trend*x_trend+q_trend

     f_d = f - y_trend
     if del_last:
      f_d = f_d[:-1] #eliminate last point

     return f_d

def compute_1D_iso_from_3D_spectrum(v,nx,ny,nz,dx,dy,dz,parce=False):
    #compute spectrum
    begin = time.time()
    v_t = np.fft.rfftn(v)/float(nx*ny*nz)    
    spec = np.empty((nx,ny,nz//2+1))
    spec[:,:,0]  = v_t[:,:,0].real**2 + v_t[:,:,0].imag**2
    spec[:,:,1:-1]  = 2.0*(v_t[:,:,1:-1].real**2 + v_t[:,:,1:-1].imag**2)
    spec[:,:,-1] = v_t[:,:,-1].real**2 + v_t[:,:,-1].imag**2  
    if parce:
      print "Parceval"
      print "real, fourier", np.sum(v**2)/float(nx*ny*nz), np.sum(spec)
    #compute 1D isotropic spectrum
    kx = np.fft.fftfreq(nx,d=dx/(2.0*np.pi))
    ky = np.fft.fftfreq(ny,d=dy/(2.0*np.pi))
    kz = np.fft.rfftfreq(nz,d=dz/(2.0*np.pi))
    dkx = kx[1]
    dky = ky[1]
    dkz = kz[1]
    kxmax = max(kx)
    kymax = max(ky)
    kzmax = max(kz)
    dk = max(dkx,dky,dkz) 
    kmax = min(kxmax,kymax,kzmax)
    k = np.arange(0.0, kmax + dk/2.0 , dk)
    isospec = np.zeros(len(k))
    for i in range(len(kx)):
      for j in range(len(ky)):
        for l in range(len(kz)):
          kappa = np.sqrt( kx[i]**2 + ky[j]**2 + kz[l]**2)
#         if int(kappa/dk) < len(k)-1: 
          if int(kappa/dk) < len(k): 
            isospec[int(kappa/dk)] += spec[i,j,l]
#         else:
#           isospec[-1] += spec[i,j]    
    print "Number of points nx, ny, nz", nx,ny,nz  
    print "Elapsed time for computing 1D iso spectrum", time.time()-begin
    
    return  k, isospec

#def compute_1D_iso_from_2D(k1, k2, dk1, dk2, kmax1, kmax2, spec):
#   dk = max(dk1,dk2) 
#   kmax = min(kmax1,kmax2)
#   k = np.arange(0.0, kmax + dk/2.0 , dk)
#   isospec = np.zeros(len(k))
#   for i in range(len(k1)):
#     for j in range(len(k2)):
#       kappa = np.sqrt( k1[i]**2 + k2[j]**2 )
#       if int(kappa/dk) < len(k)-1: 
#        isospec[int(kappa/dk)] += spec[i,j]
#       else:
#        isospec[-1] += spec[i,j] 
#   return  k, isospec
