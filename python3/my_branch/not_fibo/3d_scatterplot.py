import numpy as np
from pylab import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

step = 1
th = -11
mono = False
quadrant = False

if mono:
    mult = 0
    add = 1
else:
    mult = 1
    add = 0

plt.close()
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(111,projection='3d')

xs=[]
yz=[]
zs=[]
ys=[]
vs=[]
for ix in range(nx//step):
    for iy in range(ny//step):
        for iz in range(nz//step):
            val = np.log(fBx_[ix*step,iy*step,iz*step]**2+fBy_[ix*step,iy*step,iz*step]**2+fBz_[ix*step,iy*step,iz*step]**2)
            if quadrant:
                if (kx_[ix*step] < 0.) or (ky_[iy*step] < 0.) or (kz_[iz*step] < 0.):
                    continue
            if val > th :
                xs.append(kx_[ix*step])
                ys.append(ky_[iy*step])
                zs.append(kz_[iz*step])
                vs.append(val)

#colors = cm.hsv((vs-min(vs))/(max(vs)-min(vs))*mult+add)
#colmap = cm.ScalarMappable(cmap=cm.hsv)
#colmap.set_array(vs)

#yg = ax.scatter(xs, ys, zs, c=colors, marker='o')
#cb = fig.colorbar(colmap)

cm = plt.cm.get_cmap('RdYlBu')
sc = ax.scatter(xs, ys, zs, c=vs, vmin=min(vs), vmax=max(vs), cmap=cm)
plt.colorbar(sc)

if quadrant:
    ax.set_xlim(0.,max(kx_))
    ax.set_ylim(0.,max(ky_))
    ax.set_zlim(0.,max(kz_))
else:
    ax.set_xlim(min(kx_),max(kx_))
    ax.set_ylim(min(ky_),max(ky_))
    ax.set_zlim(min(kz_),max(kz_))

ax.set_xlabel('kx')
ax.set_ylabel('ky')
ax.set_zlabel('kz')

plt.show()
plt.close()
