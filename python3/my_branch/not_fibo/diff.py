10c10
< #---------------------------------------------------------------------------------------
---
> #-(beta)---------------------------------------------------------------------------------
43,44c43,45
<     self.meta = {}   #will contain meta_data - must be copied from the loaders
< 
---
>     self.meta = {}  #will contain meta_data - must be copied from the loaders
>     self.stat = {}  #will contain statistics ..
>     self.pnts = {}  #will contain lists of points 
112a114,151
>   #-----routines-for-basic-statistics--------------------------
>   #------------------------------------------------------------
>   def calc_minimal_stats(self,  
>       tar_var_list = None, 
>       range_x = None,
>       range_y = None,
>       range_z = None,
>       ave_label = 'ave',
>       var_label = 'var'):
>     """
>     ----------------------------------------------------------------------------
>       performs averages and variances
>     ----------------------------------------------------------------------------
>     tar_var_list = None  [list of fibo_vars OR list of (nx,ny,nz) arrays] 
>     range_x = None       [int,int] x coordinates of the first and last point 
>     range_y = None       [int,int] y coordinates of the first and last point 
>     range_z = None       [int,int] z coordinates of the first and last point
>     ave_label = 'ave'    [str] name of dictionary with averages
>     var_label = 'var'    [str] name of dictionary with variances
>     ----------------------------------------------------------------------------
>     averages             [list of stat_vars]
>     variances            [list of stat_vars]
>     ----------------------------------------------------------------------------
>     """
> 
>     if ave_label not in self.stat.keys() : self.stat[ave_label] = {} 
>     if var_label not in self.stat.keys() : self.stat[var_label] = {}
>     
>     if tar_var_list is None : tar_var_list = self.data.keys()
>     if range_x == None : range_x = [0,self.meta['nx']]
>     if range_y == None : range_y = [0,self.meta['ny']]
>     if range_z == None : range_z = [0,self.meta['nz']]
>     
>     for var in tar_var_list : 
>       self.stat[ave_label][var] = np.average(self.get_data(var)[range_x[0]:range_x[1],range_y[0]:range_y[1],range_z[0]:range_z[1]])
>       self.stat[var_label][var] = np.var(self.get_data(var)[range_x[0]:range_x[1],range_y[0]:range_y[1],range_z[0]:range_z[1]])
> 
> 
224,226c263,265
<       range_x,   
<       range_y,   
<       range_z):
---
>       ranges_x,   
>       ranges_y,   
>       ranges_z):
232,235c271,274
<     grid_pts  [int,int] lengths (in points) of the plane you ask to extract
<     range_x    [int,int,int] x coordinates of the first and two last points 
<     range_y    [int,int,int] y coordinates of the first and two last points 
<     range_z    [int,int,int] z coordinates of the first and two last points
---
>     grid_pts   [int,int] lengths (in points) of the plane you ask to extract
>     ranges_x    [int,int,int] x coordinates of the first and two last points 
>     ranges_y    [int,int,int] y coordinates of the first and two last points 
>     ranges_z    [int,int,int] z coordinates of the first and two last points
241,243c280,282
<     sec_x,sec_xx = np.meshgrid(np.linspace(range_x[0],range_x[1],grid_pts[0],endpoint=False),np.linspace(range_x[0],range_x[2],grid_pts[1],endpoint=False))
<     sec_y,sec_yy = np.meshgrid(np.linspace(range_y[0],range_y[1],grid_pts[0],endpoint=False),np.linspace(range_y[0],range_y[2],grid_pts[1],endpoint=False))
<     sec_z,sec_zz = np.meshgrid(np.linspace(range_z[0],range_z[1],grid_pts[0],endpoint=False),np.linspace(range_z[0],range_z[2],grid_pts[1],endpoint=False))
---
>     sec_x,sec_xx = np.meshgrid(np.linspace(ranges_x[0],ranges_x[1],grid_pts[0],endpoint=False),np.linspace(ranges_x[0],ranges_x[2],grid_pts[1],endpoint=False))
>     sec_y,sec_yy = np.meshgrid(np.linspace(ranges_y[0],ranges_y[1],grid_pts[0],endpoint=False),np.linspace(ranges_y[0],ranges_y[2],grid_pts[1],endpoint=False))
>     sec_z,sec_zz = np.meshgrid(np.linspace(ranges_z[0],ranges_z[1],grid_pts[0],endpoint=False),np.linspace(ranges_z[0],ranges_z[2],grid_pts[1],endpoint=False))
271c310
<     vals    [array(3)]
---
>     vals     [array(3)]
698,700c737
<     xgr_var_x         [fibo_var]
<     xgr_var_y         [fibo_var]
<     xgr_var_z         [fibo_var]
---
>     xgr_var           [fibo_var]
748,750c785
<     ygr_var_x         [fibo_var]
<     ygr_var_y         [fibo_var]
<     ygr_var_z         [fibo_var]
---
>     ygr_var           [fibo_var]
794c829
<     tar_var             [str OR np.ndarray] target variable of the procedure
---
>     tar_var            [str OR np.ndarray] target variable of the procedure
798,800c833
<     zgr_var_x          [fibo_var]
<     zgr_var_y          [fibo_var]
<     zgr_var_z          [fibo_var]
---
>     zgr_var            [fibo_var]
1502c1535
<     range_y = None      [None OR int,int] y range
---
>     range_y = None     [None OR int,int] y range
2022c2055
<     infos.readline()
---
>     self.meta['model'] = int(infos.readline().split()[2]) #not totally sure: in case substitute this line with an innocent infos.readline()
2115c2148
<     infos.readline()
---
>     self.meta['model'] = int(infos.readline().split()[2]) #not totally sure: in case substitute this line with an innocent infos.readline()
2212c2245
<     infos.readline()
---
>     self.meta['model'] = int(infos.readline().split()[2])
2473c2506
<       fibo_obj.data['ni_'+time_exit] = ni
---
>       fibo_obj.data['n_'+time_exit] = ni
2664a2698
> 
2675a2710,2814
>   def get_Te(self,   
>       seg,
>       exit_num,
>       fibo_obj = None,
>       silent = True): 
>     """
>     ------------------------------------------------------------------------------------
>       gets the Te_per,Te_par fields at the nth exit of the data segment
>     ------------------------------------------------------------------------------------
>     seg                    [str] segment name
>     exit_num               [int] number of time exit (0,1,...)
>     fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
>     silent = True          [bool] don't you want to see all infos printed on shell?
>     ------------------------------------------------------------------------------------
>     """
> 
>     #create data vectors
>     Te_par = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>     Te_per = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
> 
>     #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 2*8*nx*ny*nz data, 4 empty) for each time exit
>     rf = open(os.path.join(self.address,seg,'Te.bin'), 'r')
>     rfr = rf.read()
>     offset = 0
>     #jump to the correct line in the file
>     for l in range(exit_num):
>       time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
>       time_exit = time_exit.zfill(8)        # ..and three decimal digits
>       if not silent: print('jumping time:' , time_exit)
>       offset += 32 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4
> 
>     #fill data vectors
>     time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
>     time_exit = time_exit.zfill(8)        # ..and three decimal digits
>     if not silent: print('reading time:' , time_exit)
>     offset += 32
> 
>     flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
>     arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
>     Te_par = np.transpose(arr[0,:,:,:],(2,1,0)) + 0.5 * self.meta['beta'] * self.meta['teti']
>     Te_per = np.transpose(arr[1,:,:,:],(2,1,0)) + 0.5 * self.meta['beta'] * self.meta['teti'] * self.meta['alpha_e']
> 
>     rf.close()
> 
>     if (fibo_obj != None) :
>       #time_exit = self.segs[seg][exit_num]
>       fibo_obj.data['Te_par_'+time_exit] = Te_par[:,:,:]
>       fibo_obj.data['Te_per_'+time_exit] = Te_per[:,:,:]
>     else: return np.array([Te_par, Te_per])
>     if not silent: print('done with reading Te_par and Te_per!')
> 
>   #------------------------------------------------------------
>   def get_Qe(self,   
>       seg,
>       exit_num,
>       fibo_obj = None,
>       silent = True): 
>     """
>     ------------------------------------------------------------------------------------
>       gets the qe_per,qe_par fields at the nth exit of the data segment
>     ------------------------------------------------------------------------------------
>     seg                    [str] segment name
>     exit_num               [int] number of time exit (0,1,...)
>     fibo_obj = None        [None OR fibo_obj] fibo to fill - if None, returns values
>     silent = True          [bool] don't you want to see all infos printed on shell?
>     ------------------------------------------------------------------------------------
>     """
> 
>     #create data vectors
>     qe_par = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>     qe_per = np.empty([self.meta['nx'],self.meta['ny'],self.meta['nz']])
> 
>     #NB binary file structure: (4 empty, 8 time_exit, 4+4+4 nx ny nz, 8 empty, 2*8*nx*ny*nz data, 4 empty) for each time exit
>     rf = open(os.path.join(self.address,seg,'Qe.bin'), 'r')
>     rfr = rf.read()
>     offset = 0
>     #jump to the correct line in the file
>     for l in range(exit_num):
>       time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
>       time_exit = time_exit.zfill(8)        # ..and three decimal digits
>       if not silent: print('jumping time:' , time_exit)
>       offset += 32 + 2*8*self.meta['nx']*self.meta['ny']*self.meta['nz'] +4
> 
>     #fill data vectors
>     time_exit = '%.3f' %float(struct.unpack('d',rfr[offset+4:offset+12])[0])  #writes time exit with four integer .. 
>     time_exit = time_exit.zfill(8)        # ..and three decimal digits
>     if not silent: print('reading time:' , time_exit)
>     offset += 32
> 
>     flat_arr = struct.unpack(str(self.meta['nx']*self.meta['ny']*self.meta['nz']*2)+'d',rfr[offset:offset+2*8*self.meta['nx']*self.meta['ny']*self.meta['nz']])
>     arr = np.reshape(flat_arr,(2,self.meta['nz'],self.meta['ny'],self.meta['nx']))
>     qe_par = np.transpose(arr[0,:,:,:],(2,1,0))
>     qe_per = np.transpose(arr[1,:,:,:],(2,1,0))
> 
>     rf.close()
> 
>     if (fibo_obj != None) :
>       #time_exit = self.segs[seg][exit_num]
>       fibo_obj.data['qe_par_'+time_exit] = qe_par[:,:,:]
>       fibo_obj.data['qe_per_'+time_exit] = qe_per[:,:,:]
>     else: return np.array([qe_par, qe_per])
>     if not silent: print('done with reading qe_par and qe_per!')
> 
> 
>   #------------------------------------------------------------
2828c2967
<         fibo_obj.data['ni_'+time_exit] = ni[l,:,:,:] 
---
>         fibo_obj.data['n_'+time_exit] = ni[l,:,:,:] 
2912,2914c3051,3053
<   def get_seg_Q(self, #achtung that silvio's Q is actually more than the heat flux - so I will need to post-process it
<       seg,        #segment considered (str)
<       fibo_obj = None,  #fibo object you are considering - if None, EB values will just be returned 
---
>   def get_seg_Q(self,  #achtung that silvio's Q is actually more than the heat flux - so I will need to post-process it
>       seg,      
>       fibo_obj = None,
3012,3013d3150
< 
<   #----NOTE-TO-SELF:--MOVE-ALL-THIS-INSIDE-PHYBO-???---SPLIT-INTO-DIFFERENT-FUNCTIONS-???----------
3017a3155
>       list_terms = ['mom_1','mom_2','mom_3'],
3021c3159
<       gives all quantities at some time, needs E,B,ni,ui,Pi and sQi OR Qi_par,Qi_per
---
>       calcs all from E,B,ni,ui,Pi,sQi OR Qi_par,Qi_per ((Te_par,Te_per,qe_par,qe_per)) 
3024a3163
>     list_terms = ['mom_1','mom_2','mom_3'] choose how much you want ... 
3031,3033d3169
<     #create current density
<     fibo_obj.data['J_x_'+time_exit], fibo_obj.data['J_y_'+time_exit], fibo_obj.data['J_z_'+time_exit] =  fibo_obj.calc_curl('B_x_'+time_exit,'B_y_'+time_exit,'B_z_'+time_exit)
< 
3035c3171,3172
<     fibo_obj.data['n_'+time_exit] = fibo_obj.data.pop('ni_'+time_exit) 
---
>     #if 'ni_'+time_exit in fibo_obj.data.keys() :
>     #  fibo_obj.data['n_'+time_exit] = fibo_obj.data.pop('ni_'+time_exit) 
3037,3066c3174,3182
<     #create ue and u
<     fibo_obj.data['ue_x_'+time_exit] = fibo_obj.data['ui_x_'+time_exit] - np.divide(fibo_obj.data['J_x_'+time_exit],fibo_obj.data['n_'+time_exit])
<     fibo_obj.data['ue_y_'+time_exit] = fibo_obj.data['ui_y_'+time_exit] - np.divide(fibo_obj.data['J_y_'+time_exit],fibo_obj.data['n_'+time_exit])
<     fibo_obj.data['ue_z_'+time_exit] = fibo_obj.data['ui_z_'+time_exit] - np.divide(fibo_obj.data['J_z_'+time_exit],fibo_obj.data['n_'+time_exit])
< 
<     fibo_obj.data['u_x_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_x_'+time_exit] + fibo_obj.data['ue_x_'+time_exit]) / (1. + self.meta['mime'])
<     fibo_obj.data['u_y_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_y_'+time_exit] + fibo_obj.data['ue_y_'+time_exit]) / (1. + self.meta['mime'])
<     fibo_obj.data['u_z_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_z_'+time_exit] + fibo_obj.data['ue_z_'+time_exit]) / (1. + self.meta['mime'])
< 
<     #process Pi, create Pe and P
<     fibo_obj.data['Pe_'+time_exit]  = 0.5 * self.meta['beta'] * self.meta['teti'] * fibo_obj.data['n_'+time_exit]
< 
<     fibo_obj.data['P_xx_'+time_exit]  = fibo_obj.data['Pi_xx_'+time_exit] + fibo_obj.data['Pe_'+time_exit]  
<     fibo_obj.data['P_xx_'+time_exit] += np.square(fibo_obj.data['ui_x_'+time_exit] - fibo_obj.data['u_x_'+time_exit]) 
<     fibo_obj.data['P_xx_'+time_exit] += np.square(fibo_obj.data['ue_x_'+time_exit] - fibo_obj.data['u_x_'+time_exit]) / self.meta['mime']
<     fibo_obj.data['P_yy_'+time_exit]  = fibo_obj.data['Pi_yy_'+time_exit] + fibo_obj.data['Pe_'+time_exit]
<     fibo_obj.data['P_yy_'+time_exit] += np.square(fibo_obj.data['ui_y_'+time_exit] - fibo_obj.data['u_y_'+time_exit]) 
<     fibo_obj.data['P_yy_'+time_exit] += np.square(fibo_obj.data['ue_y_'+time_exit] - fibo_obj.data['u_y_'+time_exit]) / self.meta['mime']
<     fibo_obj.data['P_zz_'+time_exit]  = fibo_obj.data['Pi_zz_'+time_exit] + fibo_obj.data['Pe_'+time_exit]
<     fibo_obj.data['P_zz_'+time_exit] += np.square(fibo_obj.data['ui_z_'+time_exit] - fibo_obj.data['u_z_'+time_exit]) 
<     fibo_obj.data['P_zz_'+time_exit] += np.square(fibo_obj.data['ue_z_'+time_exit] - fibo_obj.data['u_z_'+time_exit]) / self.meta['mime']
<     fibo_obj.data['P_xy_'+time_exit]  = + fibo_obj.data['Pi_xy_'+time_exit]
<     fibo_obj.data['P_xy_'+time_exit] += np.multiply(fibo_obj.data['ui_x_'+time_exit] - fibo_obj.data['u_x_'+time_exit],fibo_obj.data['ui_y_'+time_exit] - fibo_obj.data['u_y_'+time_exit])
<     fibo_obj.data['P_xy_'+time_exit] += np.multiply(fibo_obj.data['ue_x_'+time_exit] - fibo_obj.data['u_x_'+time_exit],fibo_obj.data['ue_y_'+time_exit] - fibo_obj.data['u_y_'+time_exit]) / self.meta['mime']
<     fibo_obj.data['P_xz_'+time_exit]  = + fibo_obj.data['Pi_xz_'+time_exit]
<     fibo_obj.data['P_xz_'+time_exit] += np.multiply(fibo_obj.data['ui_x_'+time_exit] - fibo_obj.data['u_x_'+time_exit],fibo_obj.data['ui_z_'+time_exit] - fibo_obj.data['u_z_'+time_exit])
<     fibo_obj.data['P_xz_'+time_exit] += np.multiply(fibo_obj.data['ue_x_'+time_exit] - fibo_obj.data['u_x_'+time_exit],fibo_obj.data['ue_z_'+time_exit] - fibo_obj.data['u_z_'+time_exit]) / self.meta['mime']
<     fibo_obj.data['P_yz_'+time_exit]  = + fibo_obj.data['Pi_yz_'+time_exit]
<     fibo_obj.data['P_yz_'+time_exit] += np.multiply(fibo_obj.data['ui_y_'+time_exit] - fibo_obj.data['u_y_'+time_exit],fibo_obj.data['ui_z_'+time_exit] - fibo_obj.data['u_z_'+time_exit])
<     fibo_obj.data['P_yz_'+time_exit] += np.multiply(fibo_obj.data['ue_y_'+time_exit] - fibo_obj.data['u_y_'+time_exit],fibo_obj.data['ue_z_'+time_exit] - fibo_obj.data['u_z_'+time_exit]) / self.meta['mime']
---
>     #create current density and magnetic field curvature
>     iB = np.sqrt(np.reciprocal(fibo_obj.calc_scalr('B_x_'+time_exit,'B_x_'+time_exit,'B_y_'+time_exit,'B_y_'+time_exit,'B_z_'+time_exit,'B_z_'+time_exit)))
>     bx, by, bz = iB*fibo_obj.data['B_x_'+time_exit], iB*fibo_obj.data['B_y_'+time_exit], iB*fibo_obj.data['B_z_'+time_exit]
> 
>     fibo_obj.data['J_x_'+time_exit], fibo_obj.data['J_y_'+time_exit], fibo_obj.data['J_z_'+time_exit] =  fibo_obj.calc_curl('B_x_'+time_exit,'B_y_'+time_exit,'B_z_'+time_exit)
> 
>     fibo_obj.data['C_x_'+time_exit] = bx * fibo_obj.calc_gradx(bx) + by * fibo_obj.calc_grady(bx) + bz * fibo_obj.calc_gradz(bx)
>     fibo_obj.data['C_y_'+time_exit] = bx * fibo_obj.calc_gradx(by) + by * fibo_obj.calc_grady(by) + bz * fibo_obj.calc_gradz(by)
>     fibo_obj.data['C_z_'+time_exit] = bx * fibo_obj.calc_gradx(bz) + by * fibo_obj.calc_grady(bz) + bz * fibo_obj.calc_gradz(bz)
3079,3107d3194
<     #local kinetic energy density
<     fibo_obj.data['Ki_'+time_exit]  = np.square(fibo_obj.data['ui_x_'+time_exit])
<     fibo_obj.data['Ki_'+time_exit] += np.square(fibo_obj.data['ui_y_'+time_exit])
<     fibo_obj.data['Ki_'+time_exit] += np.square(fibo_obj.data['ui_z_'+time_exit])
<     fibo_obj.data['Ki_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['Ki_'+time_exit])/2.
< 
<     fibo_obj.data['Ke_'+time_exit]  = np.square(fibo_obj.data['ue_x_'+time_exit])
<     fibo_obj.data['Ke_'+time_exit] += np.square(fibo_obj.data['ue_y_'+time_exit])
<     fibo_obj.data['Ke_'+time_exit] += np.square(fibo_obj.data['ue_z_'+time_exit])
<     fibo_obj.data['Ke_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['Ke_'+time_exit])/(2.*self.meta['mime'])
< 
<     fibo_obj.data['K_'+time_exit]  = np.square(fibo_obj.data['u_x_'+time_exit])
<     fibo_obj.data['K_'+time_exit] += np.square(fibo_obj.data['u_y_'+time_exit])
<     fibo_obj.data['K_'+time_exit] += np.square(fibo_obj.data['u_z_'+time_exit])
<     fibo_obj.data['K_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['K_'+time_exit])*(self.meta['mime']+1)/(2.*self.meta['mime'])
< 
<     #local internal energy density
<     fibo_obj.data['Ui_'+time_exit] = (fibo_obj.data['Pi_xx_'+time_exit] + fibo_obj.data['Pi_yy_'+time_exit] + fibo_obj.data['Pi_zz_'+time_exit]) /2.
<     fibo_obj.data['Ue_'+time_exit] =  fibo_obj.data['Pe_'+time_exit] *3./2.
<     fibo_obj.data['U_'+time_exit]  = (fibo_obj.data['P_xx_'+time_exit] + fibo_obj.data['P_yy_'+time_exit] + fibo_obj.data['P_zz_'+time_exit]) /2.
< 
<     #calculate heat fluxes
<     if 'sQi_x_'+time_exit in fibo_obj.data.keys() :
<       fibo_obj.data['sQe_x_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_x_'+time_exit])
<       fibo_obj.data['sQe_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_x_'+time_exit])
<       fibo_obj.data['sQe_y_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_y_'+time_exit])
<       fibo_obj.data['sQe_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_y_'+time_exit])
<       fibo_obj.data['sQe_z_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_z_'+time_exit])
<       fibo_obj.data['sQe_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_z_'+time_exit])  
3108a3196,3374
>     if np.any(['mom_1' in list_terms,'mom_2' in list_terms,'mom_3' in list_terms]) :
>       #create ue and u
>       fibo_obj.data['ue_x_'+time_exit] = fibo_obj.data['ui_x_'+time_exit] - np.divide(fibo_obj.data['J_x_'+time_exit],fibo_obj.data['n_'+time_exit])
>       fibo_obj.data['ue_y_'+time_exit] = fibo_obj.data['ui_y_'+time_exit] - np.divide(fibo_obj.data['J_y_'+time_exit],fibo_obj.data['n_'+time_exit])
>       fibo_obj.data['ue_z_'+time_exit] = fibo_obj.data['ui_z_'+time_exit] - np.divide(fibo_obj.data['J_z_'+time_exit],fibo_obj.data['n_'+time_exit])
>   
>       fibo_obj.data['u_x_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_x_'+time_exit] + fibo_obj.data['ue_x_'+time_exit]) / (1. + self.meta['mime'])
>       fibo_obj.data['u_y_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_y_'+time_exit] + fibo_obj.data['ue_y_'+time_exit]) / (1. + self.meta['mime'])
>       fibo_obj.data['u_z_'+time_exit] = (self.meta['mime'] * fibo_obj.data['ui_z_'+time_exit] + fibo_obj.data['ue_z_'+time_exit]) / (1. + self.meta['mime'])
>   
>       #local kinetic energy density
>       fibo_obj.data['Ki_'+time_exit]  = np.square(fibo_obj.data['ui_x_'+time_exit])
>       fibo_obj.data['Ki_'+time_exit] += np.square(fibo_obj.data['ui_y_'+time_exit])
>       fibo_obj.data['Ki_'+time_exit] += np.square(fibo_obj.data['ui_z_'+time_exit])
>       fibo_obj.data['Ki_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['Ki_'+time_exit])/2.
>   
>       fibo_obj.data['Ke_'+time_exit]  = np.square(fibo_obj.data['ue_x_'+time_exit])
>       fibo_obj.data['Ke_'+time_exit] += np.square(fibo_obj.data['ue_y_'+time_exit])
>       fibo_obj.data['Ke_'+time_exit] += np.square(fibo_obj.data['ue_z_'+time_exit])
>       fibo_obj.data['Ke_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['Ke_'+time_exit])/(2.*self.meta['mime'])
>   
>       fibo_obj.data['K_'+time_exit]  = np.square(fibo_obj.data['u_x_'+time_exit])
>       fibo_obj.data['K_'+time_exit] += np.square(fibo_obj.data['u_y_'+time_exit])
>       fibo_obj.data['K_'+time_exit] += np.square(fibo_obj.data['u_z_'+time_exit])
>       fibo_obj.data['K_'+time_exit]  = np.multiply(fibo_obj.data['n_'+time_exit],fibo_obj.data['K_'+time_exit])*(self.meta['mime']+1)/(2.*self.meta['mime'])
> 
>       if not silent: print('done with calculating mom_1!')
> 
>     if np.any(['mom_2' in list_terms,'mom_3' in list_terms]) :
>       #process Pi, create Pe and P
>       fibo_obj.data['Pe_xx_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Pe_yy_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Pe_zz_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Pe_xy_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Pe_xz_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Pe_yz_'+time_exit]  = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>   
>       if 'Te_par_'+time_exit in fibo_obj.data.keys() :
>         myfac = iB*iB*(fibo_obj.data['Te_par_'+time_exit] - fibo_obj.data['Te_per_'+time_exit]) 
>   
>         fibo_obj.data['Pe_xx_'+time_exit] += fibo_obj.data['B_x_'+time_exit]*fibo_obj.data['B_x_'+time_exit]*myfac + fibo_obj.data['Te_per_'+time_exit]
>         fibo_obj.data['Pe_xx_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>         fibo_obj.data['Pe_yy_'+time_exit] += fibo_obj.data['B_y_'+time_exit]*fibo_obj.data['B_y_'+time_exit]*myfac + fibo_obj.data['Te_per_'+time_exit]
>         fibo_obj.data['Pe_yy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>         fibo_obj.data['Pe_zz_'+time_exit] += fibo_obj.data['B_z_'+time_exit]*fibo_obj.data['B_z_'+time_exit]*myfac + fibo_obj.data['Te_per_'+time_exit]
>         fibo_obj.data['Pe_zz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>         fibo_obj.data['Pe_xy_'+time_exit] += fibo_obj.data['B_x_'+time_exit]*fibo_obj.data['B_y_'+time_exit]*myfac
>         fibo_obj.data['Pe_xy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>         fibo_obj.data['Pe_xz_'+time_exit] += fibo_obj.data['B_x_'+time_exit]*fibo_obj.data['B_z_'+time_exit]*myfac
>         fibo_obj.data['Pe_xz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>         fibo_obj.data['Pe_yz_'+time_exit] += fibo_obj.data['B_y_'+time_exit]*fibo_obj.data['B_z_'+time_exit]*myfac
>         fibo_obj.data['Pe_yz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>   
>       else : 
>         Pe = 0.5 * self.meta['beta'] * self.meta['teti'] * fibo_obj.data['n_'+time_exit] #- 0.5d0*beta*TeTi 
>   
>         fibo_obj.data['Pe_xx_'+time_exit] += Pe
>         fibo_obj.data['Pe_yy_'+time_exit] += Pe
>         fibo_obj.data['Pe_zz_'+time_exit] += Pe
>   
>       fibo_obj.data['P_xx_'+time_exit]  = + np.square(fibo_obj.data['ui_x_'+time_exit]) 
>       fibo_obj.data['P_xx_'+time_exit] += + np.square(fibo_obj.data['ue_x_'+time_exit]) / self.meta['mime'] 
>       fibo_obj.data['P_xx_'+time_exit] -= + np.square(fibo_obj.data['u_x_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime'] 
>       fibo_obj.data['P_yy_'+time_exit]  = + np.square(fibo_obj.data['ui_y_'+time_exit]) 
>       fibo_obj.data['P_yy_'+time_exit] += + np.square(fibo_obj.data['ue_y_'+time_exit]) / self.meta['mime']
>       fibo_obj.data['P_yy_'+time_exit] -= + np.square(fibo_obj.data['u_y_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
>       fibo_obj.data['P_zz_'+time_exit]  = + np.square(fibo_obj.data['ui_z_'+time_exit]) 
>       fibo_obj.data['P_zz_'+time_exit] += + np.square(fibo_obj.data['ue_z_'+time_exit]) / self.meta['mime']
>       fibo_obj.data['P_zz_'+time_exit] -= + np.square(fibo_obj.data['u_z_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
>       fibo_obj.data['P_xy_'+time_exit]  = + np.multiply(fibo_obj.data['ui_x_'+time_exit] , fibo_obj.data['ui_y_'+time_exit])
>       fibo_obj.data['P_xy_'+time_exit] += + np.multiply(fibo_obj.data['ue_x_'+time_exit] , fibo_obj.data['ue_y_'+time_exit]) / self.meta['mime']
>       fibo_obj.data['P_xy_'+time_exit] -= + np.multiply(fibo_obj.data['u_x_'+time_exit] ,  fibo_obj.data['u_y_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
>       fibo_obj.data['P_xz_'+time_exit]  = + np.multiply(fibo_obj.data['ui_x_'+time_exit] , fibo_obj.data['ui_z_'+time_exit])
>       fibo_obj.data['P_xz_'+time_exit] += + np.multiply(fibo_obj.data['ue_x_'+time_exit] , fibo_obj.data['ue_z_'+time_exit]) / self.meta['mime']
>       fibo_obj.data['P_xz_'+time_exit] -= + np.multiply(fibo_obj.data['u_x_'+time_exit] ,  fibo_obj.data['u_z_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
>       fibo_obj.data['P_yz_'+time_exit]  = + np.multiply(fibo_obj.data['ui_y_'+time_exit] , fibo_obj.data['ui_z_'+time_exit])
>       fibo_obj.data['P_yz_'+time_exit] += + np.multiply(fibo_obj.data['ue_y_'+time_exit] , fibo_obj.data['ue_z_'+time_exit]) / self.meta['mime']
>       fibo_obj.data['P_yz_'+time_exit] -= + np.multiply(fibo_obj.data['u_y_'+time_exit] ,  fibo_obj.data['u_z_'+time_exit]) * (1. + self.meta['mime']) / self.meta['mime']
>   
>       fibo_obj.data['P_xx_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>       fibo_obj.data['P_yy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>       fibo_obj.data['P_zz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>       fibo_obj.data['P_xy_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>       fibo_obj.data['P_xz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>       fibo_obj.data['P_yz_'+time_exit] *= fibo_obj.data['n_'+time_exit]
>   
>       fibo_obj.data['P_xx_'+time_exit] += fibo_obj.data['Pi_xx_'+time_exit] + fibo_obj.data['Pe_xx_'+time_exit]  
>       fibo_obj.data['P_yy_'+time_exit] += fibo_obj.data['Pi_yy_'+time_exit] + fibo_obj.data['Pe_yy_'+time_exit]
>       fibo_obj.data['P_zz_'+time_exit] += fibo_obj.data['Pi_zz_'+time_exit] + fibo_obj.data['Pe_zz_'+time_exit]
>       fibo_obj.data['P_xy_'+time_exit] += fibo_obj.data['Pi_xy_'+time_exit] + fibo_obj.data['Pe_xy_'+time_exit] 
>       fibo_obj.data['P_xz_'+time_exit] += fibo_obj.data['Pi_xz_'+time_exit] + fibo_obj.data['Pe_xz_'+time_exit] 
>       fibo_obj.data['P_yz_'+time_exit] += fibo_obj.data['Pi_yz_'+time_exit] + fibo_obj.data['Pe_yz_'+time_exit] 
>   
>       #local internal energy density
>       fibo_obj.data['Ui_'+time_exit] = (fibo_obj.data['Pi_xx_'+time_exit] + fibo_obj.data['Pi_yy_'+time_exit] + fibo_obj.data['Pi_zz_'+time_exit]) /2.
>       fibo_obj.data['Ue_'+time_exit] = (fibo_obj.data['Pe_xx_'+time_exit] + fibo_obj.data['Pe_yy_'+time_exit] + fibo_obj.data['Pe_zz_'+time_exit]) /2.
>       fibo_obj.data['U_'+time_exit]  = (fibo_obj.data['P_xx_'+time_exit] + fibo_obj.data['P_yy_'+time_exit] + fibo_obj.data['P_zz_'+time_exit]) /2.
> 
>       if not silent: print('done with calculating mom_2!')
> 
>     if ('mom_3' in list_terms) :
>       #calculate heat fluxes
>       fibo_obj.data['Qe_x_'+time_exit] = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Qe_y_'+time_exit] = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>       fibo_obj.data['Qe_z_'+time_exit] = np.zeros([self.meta['nx'],self.meta['ny'],self.meta['nz']])
>   
>       if 'qe_par_'+time_exit in fibo_obj.data.keys() :
>         s1 = fibo_obj.data['Te_par_'+time_exit] + 2 * fibo_obj.data['Te_per_'+time_exit]
>         s2 = 2 * fibo_obj.data['Te_par_'+time_exit] * (1 - fibo_obj.data['Te_par_'+time_exit] / fibo_obj.data['Te_per_'+time_exit])
>         myfac = iB * fibo_obj.data['n_'+time_exit] * fibo_obj.data['Te_per_'+time_exit]
>   
>         addx = myfac * (fibo_obj.calc_gradx(s1) - fibo_obj.data['C_x_'+time_exit] * s2)
>         addy = myfac * (fibo_obj.calc_grady(s1) - fibo_obj.data['C_y_'+time_exit] * s2)
>         addz = myfac * (fibo_obj.calc_gradz(s1) - fibo_obj.data['C_z_'+time_exit] * s2)
>   
>         fibo_obj.data['Qe_x_'+time_exit], fibo_obj.data['Qe_y_'+time_exit], fibo_obj.data['Qe_z_'+time_exit] = fibo_obj.calc_cross(addx,bx,addy,by,addz,bz)
>   
>         myfac = 2 * fibo_obj.data['qe_per_'+time_exit] + fibo_obj.data['qe_par_'+time_exit]
>   
>         fibo_obj.data['Qe_x_'+time_exit] += bx * myfac
>         fibo_obj.data['Qe_y_'+time_exit] += by * myfac
>         fibo_obj.data['Qe_z_'+time_exit] += bz * myfac
>   
>       #fibo_obj.data['sQe_x_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_x_'+time_exit])
>       #fibo_obj.data['sQe_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_x_'+time_exit])
>       #fibo_obj.data['sQe_y_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_y_'+time_exit])
>       #fibo_obj.data['sQe_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_y_'+time_exit])
>       #fibo_obj.data['sQe_z_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_z_'+time_exit])
>       #fibo_obj.data['sQe_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_z_'+time_exit])  
>       fibo_obj.data['sQe_x_'+time_exit]  = + fibo_obj.data['Qe_x_'+time_exit] 
>       fibo_obj.data['sQe_x_'+time_exit] +=   2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_x_'+time_exit])
>       fibo_obj.data['sQe_x_'+time_exit] +=   2 * fibo_obj.calc_scalr('Pe_xx_'+time_exit,'ue_x_'+time_exit,'Pe_xy_'+time_exit,'ue_y_'+time_exit,'Pe_xz_'+time_exit,'ue_z_'+time_exit)
>       fibo_obj.data['sQe_y_'+time_exit]  = + fibo_obj.data['Qe_y_'+time_exit] 
>       fibo_obj.data['sQe_y_'+time_exit] +=   2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_y_'+time_exit])
>       fibo_obj.data['sQe_y_'+time_exit] +=   2 * fibo_obj.calc_scalr('Pe_xy_'+time_exit,'ue_x_'+time_exit,'Pe_yy_'+time_exit,'ue_y_'+time_exit,'Pe_yz_'+time_exit,'ue_z_'+time_exit)
>       fibo_obj.data['sQe_z_'+time_exit]  = + fibo_obj.data['Qe_z_'+time_exit] 
>       fibo_obj.data['sQe_z_'+time_exit] +=   2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_z_'+time_exit])
>       fibo_obj.data['sQe_z_'+time_exit] +=   2 * fibo_obj.calc_scalr('Pe_xz_'+time_exit,'ue_x_'+time_exit,'Pe_yz_'+time_exit,'ue_y_'+time_exit,'Pe_zz_'+time_exit,'ue_z_'+time_exit)
>   
>       if 'sQi_x_'+time_exit in fibo_obj.data.keys() : #from sQi to Qi
>         #fibo_obj.data['sQ_x_'+time_exit] = fibo_obj.data['sQi_x_'+time_exit] + fibo_obj.data['sQe_x_'+time_exit]
>         #fibo_obj.data['sQ_y_'+time_exit] = fibo_obj.data['sQi_y_'+time_exit] + fibo_obj.data['sQe_y_'+time_exit]
>         #fibo_obj.data['sQ_z_'+time_exit] = fibo_obj.data['sQi_z_'+time_exit] + fibo_obj.data['sQe_z_'+time_exit]
>   
>         fibo_obj.data['Qi_x_'+time_exit]  = + fibo_obj.data['sQi_x_'+time_exit] 
>         fibo_obj.data['Qi_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_x_'+time_exit])
>         fibo_obj.data['Qi_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xx_'+time_exit,'ui_x_'+time_exit,'Pi_xy_'+time_exit,'ui_y_'+time_exit,'Pi_xz_'+time_exit,'ui_z_'+time_exit)
>         fibo_obj.data['Qi_y_'+time_exit]  = + fibo_obj.data['sQi_y_'+time_exit] 
>         fibo_obj.data['Qi_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
>         fibo_obj.data['Qi_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xy_'+time_exit,'ui_x_'+time_exit,'Pi_yy_'+time_exit,'ui_y_'+time_exit,'Pi_yz_'+time_exit,'ui_z_'+time_exit)
>         fibo_obj.data['Qi_z_'+time_exit]  = + fibo_obj.data['sQi_z_'+time_exit]
>         fibo_obj.data['Qi_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
>         fibo_obj.data['Qi_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xz_'+time_exit,'ui_x_'+time_exit,'Pi_yz_'+time_exit,'ui_y_'+time_exit,'Pi_zz_'+time_exit,'ui_z_'+time_exit)
>   
>         #fibo_obj.data['Q_x_'+time_exit]  = + fibo_obj.data['sQ_x_'+time_exit] 
>         #fibo_obj.data['Q_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_x_'+time_exit])
>         #fibo_obj.data['Q_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xx_'+time_exit,'u_x_'+time_exit,'P_xy_'+time_exit,'u_y_'+time_exit,'P_xz_'+time_exit,'u_z_'+time_exit)
>         #fibo_obj.data['Q_y_'+time_exit]  = + fibo_obj.data['sQ_y_'+time_exit] 
>         #fibo_obj.data['Q_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_y_'+time_exit])
>         #fibo_obj.data['Q_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xy_'+time_exit,'u_x_'+time_exit,'P_yy_'+time_exit,'u_y_'+time_exit,'P_yz_'+time_exit,'u_z_'+time_exit)
>         #fibo_obj.data['Q_z_'+time_exit]  = + fibo_obj.data['sQ_z_'+time_exit] 
>         #fibo_obj.data['Q_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_z_'+time_exit])
>         #fibo_obj.data['Q_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xz_'+time_exit,'u_x_'+time_exit,'P_yz_'+time_exit,'u_y_'+time_exit,'P_zz_'+time_exit,'u_z_'+time_exit)
>   
>       else : #reconstruct Qi, then from Qi to sQi
>         fibo_obj.data['Qi_x_'+time_exit] = fibo_obj.data['Qi_par_x_'+time_exit] + 2.*fibo_obj.data['Qi_per_x_'+time_exit]
>         fibo_obj.data['Qi_y_'+time_exit] = fibo_obj.data['Qi_par_y_'+time_exit] + 2.*fibo_obj.data['Qi_per_y_'+time_exit]
>         fibo_obj.data['Qi_z_'+time_exit] = fibo_obj.data['Qi_par_z_'+time_exit] + 2.*fibo_obj.data['Qi_per_z_'+time_exit]
>   
>         fibo_obj.data['sQi_x_'+time_exit]  = + fibo_obj.data['Qi_x_'+time_exit] 
>         fibo_obj.data['sQi_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_x_'+time_exit])
>         fibo_obj.data['sQi_x_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xx_'+time_exit,'ui_x_'+time_exit,'Pi_xy_'+time_exit,'ui_y_'+time_exit,'Pi_xz_'+time_exit,'ui_z_'+time_exit)
>         fibo_obj.data['sQi_y_'+time_exit]  = + fibo_obj.data['Qi_y_'+time_exit] 
>         fibo_obj.data['sQi_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
>         fibo_obj.data['sQi_y_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xy_'+time_exit,'ui_x_'+time_exit,'Pi_yy_'+time_exit,'ui_y_'+time_exit,'Pi_yz_'+time_exit,'ui_z_'+time_exit)
>         fibo_obj.data['sQi_z_'+time_exit]  = + fibo_obj.data['Qi_z_'+time_exit]
>         fibo_obj.data['sQi_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_z_'+time_exit])
>         fibo_obj.data['sQi_z_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xz_'+time_exit,'ui_x_'+time_exit,'Pi_yz_'+time_exit,'ui_y_'+time_exit,'Pi_zz_'+time_exit,'ui_z_'+time_exit)
>   
3112,3122c3378
< 
<       fibo_obj.data['Qi_x_'+time_exit]  = + fibo_obj.data['sQi_x_'+time_exit] 
<       fibo_obj.data['Qi_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_x_'+time_exit])
<       fibo_obj.data['Qi_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xx_'+time_exit,'ui_x_'+time_exit,'Pi_xy_'+time_exit,'ui_y_'+time_exit,'Pi_xz_'+time_exit,'ui_z_'+time_exit)
<       fibo_obj.data['Qi_y_'+time_exit]  = + fibo_obj.data['sQi_y_'+time_exit] 
<       fibo_obj.data['Qi_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
<       fibo_obj.data['Qi_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xy_'+time_exit,'ui_x_'+time_exit,'Pi_yy_'+time_exit,'ui_y_'+time_exit,'Pi_yz_'+time_exit,'ui_z_'+time_exit)
<       fibo_obj.data['Qi_z_'+time_exit]  = + fibo_obj.data['sQi_z_'+time_exit]
<       fibo_obj.data['Qi_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
<       fibo_obj.data['Qi_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('Pi_xz_'+time_exit,'ui_x_'+time_exit,'Pi_yz_'+time_exit,'ui_y_'+time_exit,'Pi_zz_'+time_exit,'ui_z_'+time_exit)
< 
---
>   
3133,3153c3389
<     else :
<       fibo_obj.data['Qi_x_'+time_exit] = fibo_obj.data['Qi_par_x_'+time_exit] + 2.*fibo_obj.data['Qi_per_x_'+time_exit]
<       fibo_obj.data['Qi_y_'+time_exit] = fibo_obj.data['Qi_par_y_'+time_exit] + 2.*fibo_obj.data['Qi_per_y_'+time_exit]
<       fibo_obj.data['Qi_z_'+time_exit] = fibo_obj.data['Qi_par_z_'+time_exit] + 2.*fibo_obj.data['Qi_per_z_'+time_exit]
< 
<       fibo_obj.data['sQe_x_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_x_'+time_exit])
<       fibo_obj.data['sQe_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_x_'+time_exit])
<       fibo_obj.data['sQe_y_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_y_'+time_exit])
<       fibo_obj.data['sQe_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_y_'+time_exit])
<       fibo_obj.data['sQe_z_'+time_exit]  = 2 * np.multiply(fibo_obj.data['Ue_'+time_exit] + fibo_obj.data['Ke_'+time_exit], fibo_obj.data['ue_z_'+time_exit])
<       fibo_obj.data['sQe_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Pe_'+time_exit],fibo_obj.data['ue_z_'+time_exit])  
< 
<       fibo_obj.data['sQi_x_'+time_exit]  = + fibo_obj.data['Qi_x_'+time_exit] 
<       fibo_obj.data['sQi_x_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_x_'+time_exit])
<       fibo_obj.data['sQi_x_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xx_'+time_exit,'ui_x_'+time_exit,'Pi_xy_'+time_exit,'ui_y_'+time_exit,'Pi_xz_'+time_exit,'ui_z_'+time_exit)
<       fibo_obj.data['sQi_y_'+time_exit]  = + fibo_obj.data['Qi_y_'+time_exit] 
<       fibo_obj.data['sQi_y_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_y_'+time_exit])
<       fibo_obj.data['sQi_y_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xy_'+time_exit,'ui_x_'+time_exit,'Pi_yy_'+time_exit,'ui_y_'+time_exit,'Pi_yz_'+time_exit,'ui_z_'+time_exit)
<       fibo_obj.data['sQi_z_'+time_exit]  = + fibo_obj.data['Qi_z_'+time_exit]
<       fibo_obj.data['sQi_z_'+time_exit] += 2 * np.multiply(fibo_obj.data['Ui_'+time_exit] + fibo_obj.data['Ki_'+time_exit], fibo_obj.data['ui_z_'+time_exit])
<       fibo_obj.data['sQi_z_'+time_exit] += 2 * fibo_obj.calc_scalr('Pi_xz_'+time_exit,'ui_x_'+time_exit,'Pi_yz_'+time_exit,'ui_y_'+time_exit,'Pi_zz_'+time_exit,'ui_z_'+time_exit)
---
>       if not silent: print('done with calculating mom_3!')
3155,3157c3391
<       fibo_obj.data['sQ_x_'+time_exit] = fibo_obj.data['sQi_x_'+time_exit] + fibo_obj.data['sQe_x_'+time_exit]
<       fibo_obj.data['sQ_y_'+time_exit] = fibo_obj.data['sQi_y_'+time_exit] + fibo_obj.data['sQe_y_'+time_exit]
<       fibo_obj.data['sQ_z_'+time_exit] = fibo_obj.data['sQi_z_'+time_exit] + fibo_obj.data['sQe_z_'+time_exit]
---
>     if not silent: print('done with calculating all!')
3159,3167c3393,3406
<       fibo_obj.data['Q_x_'+time_exit]  = + fibo_obj.data['sQ_x_'+time_exit] 
<       fibo_obj.data['Q_x_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_x_'+time_exit])
<       fibo_obj.data['Q_x_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xx_'+time_exit,'u_x_'+time_exit,'P_xy_'+time_exit,'u_y_'+time_exit,'P_xz_'+time_exit,'u_z_'+time_exit)
<       fibo_obj.data['Q_y_'+time_exit]  = + fibo_obj.data['sQ_y_'+time_exit] 
<       fibo_obj.data['Q_y_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_y_'+time_exit])
<       fibo_obj.data['Q_y_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xy_'+time_exit,'u_x_'+time_exit,'P_yy_'+time_exit,'u_y_'+time_exit,'P_yz_'+time_exit,'u_z_'+time_exit)
<       fibo_obj.data['Q_z_'+time_exit]  = + fibo_obj.data['sQ_z_'+time_exit] 
<       fibo_obj.data['Q_z_'+time_exit] -= 2 * np.multiply(fibo_obj.data['U_'+time_exit] + fibo_obj.data['K_'+time_exit], fibo_obj.data['u_z_'+time_exit])
<       fibo_obj.data['Q_z_'+time_exit] -= 2 * fibo_obj.calc_scalr('P_xz_'+time_exit,'u_x_'+time_exit,'P_yz_'+time_exit,'u_y_'+time_exit,'P_zz_'+time_exit,'u_z_'+time_exit)
---
>   #------------------------------------------------------------
>   def clean_old_vars(self,
>       time_exit,
>       fibo_obj = None,
>       silent = True): 
>     """
>     ------------------------------------------------------------------------------------
>       deletes sQi,sQe,sQ ((Te_par, Te_per, qe_par, qe_per)) 
>     ------------------------------------------------------------------------------------
>     time_exit              [str] time exit (usually format '0000.000')
>     fibo_obj = None        [fibo_obj] fibo to fill - if None, you will be insulted
>     silent = True          [bool] don't you want to see all infos printed on shell?
>     ------------------------------------------------------------------------------------
>     """
3169c3408,3499
<     if not silent: print('done with calculating all!')
---
>     if fibo_obj == None : print('FDP: creer un object fibo SVP')
> 
>     if 'sQi_x_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['sQi_x_'+time_exit]
>       del fibo_obj.data['sQi_y_'+time_exit]
>       del fibo_obj.data['sQi_z_'+time_exit]
>     if 'sQe_x_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['sQe_x_'+time_exit]
>       del fibo_obj.data['sQe_y_'+time_exit]
>       del fibo_obj.data['sQe_z_'+time_exit]
>     if 'sQ_x_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['sQ_x_'+time_exit]
>       del fibo_obj.data['sQ_y_'+time_exit]
>       del fibo_obj.data['sQ_z_'+time_exit]
>     
>     if 'Te_par_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['Te_par_'+time_exit]
>       del fibo_obj.data['Te_per_'+time_exit]
>     if 'qe_par_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['qe_par_'+time_exit]
>       del fibo_obj.data['qe_per_'+time_exit]
> 
>     if not silent: print('done with cleaning sQi,sQe,sQ ((Te_par, Te_per, qe_par, qe_per)) ')
> 
>   #------------------------------------------------------------
>   def clean_new_vars(self,
>       time_exit,
>       fibo_obj = None,
>       silent = True): 
>     """
>     ------------------------------------------------------------------------------------
>       deletes ue,Pe,Qe,sQe, u,P,Q,sQ, enE,enB, Ki,Ke,K,Ui,Ue,U 
>     ------------------------------------------------------------------------------------
>     time_exit              [str] time exit (usually format '0000.000')
>     fibo_obj = None        [fibo_obj] fibo to fill - if None, you will be insulted
>     silent = True          [bool] don't you want to see all infos printed on shell?
>     ------------------------------------------------------------------------------------
>     """
> 
>     if fibo_obj == None : print('FDP: creer un object fibo SVP')
> 
>     if 'ue_x_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['ue_x_'+time_exit]
>       del fibo_obj.data['ue_y_'+time_exit]
>       del fibo_obj.data['ue_z_'+time_exit]
>     if 'Pe_xx_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['Pe_xx_'+time_exit]
>       del fibo_obj.data['Pe_yy_'+time_exit]
>       del fibo_obj.data['Pe_zz_'+time_exit]
>       del fibo_obj.data['Pe_xy_'+time_exit]
>       del fibo_obj.data['Pe_xz_'+time_exit]
>       del fibo_obj.data['Pe_yz_'+time_exit]
>     if 'Qe_x_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['Qe_x_'+time_exit]
>       del fibo_obj.data['Qe_y_'+time_exit]
>       del fibo_obj.data['Qe_z_'+time_exit]
>     if 'sQe_x_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['sQe_x_'+time_exit]
>       del fibo_obj.data['sQe_y_'+time_exit]
>       del fibo_obj.data['sQe_z_'+time_exit]
>   
>     if 'u_x_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['u_x_'+time_exit]
>       del fibo_obj.data['u_y_'+time_exit]
>       del fibo_obj.data['u_z_'+time_exit]
>     if 'P_xx_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['P_xx_'+time_exit]
>       del fibo_obj.data['P_yy_'+time_exit]
>       del fibo_obj.data['P_zz_'+time_exit]
>       del fibo_obj.data['P_xy_'+time_exit]
>       del fibo_obj.data['P_xz_'+time_exit]
>       del fibo_obj.data['P_yz_'+time_exit]
>     if 'Q_x_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['Q_x_'+time_exit]
>       del fibo_obj.data['Q_y_'+time_exit]
>       del fibo_obj.data['Q_z_'+time_exit]
>     if 'sQ_x_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['sQ_x_'+time_exit]
>       del fibo_obj.data['sQ_y_'+time_exit]
>       del fibo_obj.data['sQ_z_'+time_exit]
>   
>   
>     del fibo_obj.data['enE_'+time_exit]
>     del fibo_obj.data['enB_'+time_exit]
>     if 'K_'+time_exit in fibo_obj.data.keys() :  
>       del fibo_obj.data['Ki_'+time_exit]
>       del fibo_obj.data['Ke_'+time_exit]
>       del fibo_obj.data['K_'+time_exit]
>     if 'U_'+time_exit in fibo_obj.data.keys() :
>       del fibo_obj.data['Ui_'+time_exit]
>       del fibo_obj.data['Ue_'+time_exit]
>       del fibo_obj.data['U_'+time_exit]
3170a3501
>     if not silent: print('done with cleaning ue,Pe,Qe,sQe, u,P,Q,sQ, enE,enB, Ki,Ke,K,Ui,Ue,U ')
3860,3862c4191
<     #if not silent: print('done with the reading!')
< 
< 
---
>     #if not silent: print('done with the reading!')
\ No newline at end of file
