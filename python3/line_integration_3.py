#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
import gc

import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import use as mpl_use
from matplotlib import get_backend as mpl_get_backend
#mpl_use('Agg')
import matplotlib.pyplot as plt
from scipy.integrate import ode as si_ode
from scipy.interpolate import RegularGridInterpolator
from scipy.ndimage import map_coordinates as ndm_map_coordinates
from scipy.integrate import simps as si_simpson

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
import mylib as ml
from mod_calc import *
from mod_from import *

#------------------------------------------------------
#Intent
#------
print('\nPerforms 3D line integration of Eparl\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------
run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'line_integration_3'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------
def apply_threshold(field, th, dx=dz, dy=dy, dz=dx):
    pos_x, pos_y, pos_z = np.where(field > th)
    pos_x, pos_y, pos_z = ( pos_x.astype(np.float64)*dx,
                            pos_y.astype(np.float64)*dy,
                            pos_z.astype(np.float64)*dz )
    return pos_x, pos_y, pos_z

def generate_stream_seeds(pos_x, pos_y, pos_z, n_seed):
    n_seed = min(pos_x.size, n_seed)
    inds = np.arange(0, pos_x.size, pos_x.size//n_seed, dtype=int)
    return np.array([pos_x[inds], pos_y[inds], pos_z[inds]])

def construct_field_lines( interpolated_function, seeds, dt,
                           max_dist=1000., max_cnt=100000, xl=xl, yl=yl, zl=zl):
    x_lines = []
    y_lines = []
    z_lines = []
    s_lines = []
    for ns in range(seeds.shape[1]):
        # calculate field lines starting from the seed
        # integrate in both directions
        x_line = [seeds[0, ns]]
        y_line = [seeds[1, ns]]
        z_line = [seeds[2, ns]]
        s_line = [0.]
        for d in [-1, 1]:
            # set the direction
            dt *= -1.
            r = si_ode(f_norm_interpolated)
            r.set_integrator('vode')
            r.set_f_params(xl, yl, zl, interpolated_function)
            # initial position of the magnetic field line
            r.set_initial_value(seeds[:, ns], 0)
            cnt = 0
            while r.successful():
                r.integrate(r.t+dt)
                x_line.append(r.y[0])
                y_line.append(r.y[1])
                z_line.append(r.y[2])
                s_line.append(r.t)
                # check if field line reached a given condition
                if abs(r.t) > max_dist: break
                # -> check if too many integration steps were done
                cnt += 1
                if cnt > max_cnt:
                    print( '\nWARNING: mx_cnt=%d '%(max_cnt,) +
                           'rached in directoin %d '%(d,) +
                           'from seed (%f, %f, %f)!\n'%(seeds[0, ns], seeds[1, ns], seeds[2, ns]) )
                    break
            x_line = x_line[::d]
            y_line = y_line[::d]
            z_line = z_line[::d]
            s_line = s_line[::d]
        x_lines.append(np.array(x_line))
        y_lines.append(np.array(y_line))
        z_lines.append(np.array(z_line))
        s_lines.append(np.array(s_line))
    return x_lines, y_lines, z_lines, s_lines

def f_norm_interpolated(t, p, xl, yl, zl, interpolated_function):
    x, y, z = p[0]%xl, p[1]%yl, p[2]%zl
    return interpolated_function([x, y, z])[0] 

#---> loop over times <---
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    #------------------------------------------------------
    # Get data
    #----------
    #-------
    t0 = t_time()
    #-------
    # -> magnetic fields and co.
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm

    del Bnorm

    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
        J = np.array([cx, cy, cz]) # J = rot(B)
        del cx, cy, cz

    # -> densities and currents
    if code_name == 'HVM':
        n_p, u_p = run.get_Ion(ind)
        u_e = np.empty((3, nx, ny, nz), dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0], n_p)
        u_e[1] = u_p[1] - np.divide(J[1], n_p)
        u_e[2] = u_p[2] - np.divide(J[2], n_p)
        del n_p, J, u_p
    elif code_name == 'iPIC':
        _, u_e = run.get_Ion(ind,qom=qom_e)

    # -> demag
    e_demag = np.empty(E.shape, dtype=np.float64)
    e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
    e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
    e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
    e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)
    del e_demag, B, E
    #-------
    print('\nData loading and computing: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # apply threshold and generate seeds
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    n_seeds = 5
    pos_x, pos_y, pos_z = apply_threshold(e_demag_n, np.max(e_demag_n)*.75)
    seeds = generate_stream_seeds(pos_x, pos_y, pos_z, n_seeds)

    #------------------------------------------------------
    # generate field lines
    #----------------------
    dt = min(dx, dy, dz)*.5
    interpolated_function = RegularGridInterpolator( (x, y, z), b, method='linear',
                                                     bounds_error=False, fill_value=None )
    x_lines, y_lines, z_lines, s_lines = construct_field_lines( interpolated_function, seeds, dt,
                                                                max_dist=5. )
    #-------
    print('Generating field lines: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # interpolte parallel electric field
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    Eparl_lines = []
    for i in range(seeds.shape[1]):
        coords_x = (x_lines[i]/dx) % nx
        coords_y = (y_lines[i]/dy) % ny
        coords_z = (z_lines[i]/dz) % nz
        Eparl_lines.append( ndm_map_coordinates(
                            Eparl, np.vstack((coords_x, coords_y, coords_z)),
                            mode='grid-wrap', prefilter=False
                            ) )
    #-------
    print('Initerpolating Eparl: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # interpolte e demag
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    De_lines = []
    for i in range(seeds.shape[1]):
        coords_x = (x_lines[i]/dx) % nx
        coords_y = (y_lines[i]/dy) % ny
        coords_z = (z_lines[i]/dz) % nz
        De_lines.append( ndm_map_coordinates(
                            e_demag_n, np.vstack((coords_x, coords_y, coords_z)),
                            mode='grid-wrap', prefilter=False
                            ) )
    #-------
    print('Initerpolating |De|: %fs'%(t_time() - t0,))
    #-------
    #------------------------------------------------------
    # integrating Eparl
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    R_lines = []
    for i in range(seeds.shape[1]):
        R_lines.append( si_simpson(Eparl_lines[i], x=s_lines[i]) )
    #-------
    print('Integrating Eparl: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # write on file
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    if 1:
        for i in range(seeds.shape[1]):
            with open(opath+'/'+out_dir+'/line_%d_%s_%d.dat'%(i, run_name, ind), 'w') as f:
                f.write('%f\t%f\n'%(time[ind], R_lines[i]))
                for j in range(s_lines[i].size):
                    f.write('%f\t%f\t%f\t%f\t%f\t%f\n'%( x_lines[i][j], y_lines[i][j],
                                                         z_lines[i][j],
                                                         s_lines[i][j], Eparl_lines[i][j],
                                                         De_lines[i][j]) )
    #-------
    print('Writing on file: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # plot Eparl and e demag interpolated
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    if 1:
        plt.close('all')
        fig, (ax0, ax1) = plt.subplots(2, 1, sharex=True)
        for i in range(seeds.shape[1]):
            ax0.plot(s_lines[i], Eparl_lines[i])
        ax1.set_xlabel('s')
        ax0.set_ylabel('Eparl')
        for i in range(seeds.shape[1]):
            ax1.plot(s_lines[i], De_lines[i])
        ax1.set_ylabel('De')
        plt.tight_layout()
        if mpl_get_backend() == 'agg':
            plt.savefig(opath+'/'+out_dir+'/Eparl_lines_%s_%d.png'%(run_name, ind))
        else:
            plt.show()
        plt.close()

    #------------------------------------------------------
    # plot average Eparl
    #-------------------------
    if 1:
        plt.close('all')
        fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(16, 10), sharex=True)
        val_max = -1.
        val_min = 100.
        s_min = 1.
        s_max = -1.
        for i in range(seeds.shape[1]):
            is0 = np.argmin(np.abs(s_lines[i]))
            incr_avg = np.empty(s_lines[i].shape, dtype=float)
            for j in range(s_lines[i].size):
                if j < is0:
                    incr_avg[j] = np.mean(Eparl_lines[i][j:is0+1])
                elif j > is0:
                    incr_avg[j] = np.mean(Eparl_lines[i][is0:j+1])
                else:
                    incr_avg[j] = Eparl_lines[i][is0]
            val_min = min(val_min, np.min(np.abs(incr_avg)))
            val_max = max(val_max, np.max(np.abs(incr_avg)))
            s_min = min(s_min, s_lines[i][0])
            s_max = max(s_max, s_lines[i][-1])
            ax0.plot(s_lines[i], np.abs(incr_avg)/(0.718722**2))
            ax1.plot(s_lines[i], np.abs(incr_avg)/(0.718722**2))
        ax0.set_ylabel('|<Eparl>_{0,s}|/B/vA')
        ax1.set_ylabel('|<Eparl>_{0,s}|/B/vA (logscale)')
        ax1.set_xlabel('s [d_i]')
        ax0.set_xlim(s_min, s_max)
        ax0.set_ylim(val_min/(0.718722**2), val_max/(0.718722**2))
        ax1.set_ylim(1.e-4, val_max/(0.718722**2))
        ax1.set_yscale('log')
        plt.tight_layout()
        if mpl_get_backend() == 'agg':
            plt.savefig(opath+'/'+out_dir+'/Eparl_incr_avg_%s_%d.png'%(run_name, ind))
        else:
            plt.show()
        plt.close()

    #------------------------------------------------------
    # plot in 3d box
    #-----------------------------
    if 1:
        plt.close('all')
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        n_points = 200
        n_points = min(pos_x.size, n_points)
        every = pos_x.size//n_points
        if 1:
            ax.scatter(pos_x[::every], pos_y[::every], pos_z[::every], c='r')

        ax.scatter(seeds[0], seeds[1], seeds[2], c='g')

        for i in range(seeds.shape[1]):
            ax.plot(x_lines[i], y_lines[i], z_lines[i], 'o', ms=.5)

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_xlim3d(x[0], x[-1])
        ax.set_ylim3d(y[0], y[-1])
        ax.set_zlim3d(z[0], z[-1])
        if mpl_get_backend() == 'agg':
            plt.savefig(opath+'/'+out_dir+'/B_lines_plot_3d_%s_%d.png'%(run_name, ind))
        else:
            plt.show()
        plt.close()
    #-------
    print('Plotting: %fs'%(t_time() - t0,))
    #-------

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------
