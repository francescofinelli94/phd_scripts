# coding: utf-8
from pandas import read_csv
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

#latex font
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#csv_path = "/home/frafin/Downloads/Eparl_EDR_EDRmask_BIS/Eparl_EDR2_adim_uns_log_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
csv_path = "/home/frafin/Downloads/line_integrated_Eparl_EDR_EDRmask/line_integrated_Eparl_EDR2_adim_uns_log_line_integrated_Eparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
df = read_csv(csv_path, header=0, index_col=0)

t_all = df['t'].values
Eparl_EDR_max = df['max1'].values
Eparl_EDR_min = df['min1'].values
Eparl_EDR_std = df['std1'].values
Eparl_EDR_avg = df['avg1'].values

plt.close('all')
fig0, ax0 = plt.subplots(1, 1, figsize=(16, 10))

Eparl_EDR_max[Eparl_EDR_avg<0.], Eparl_EDR_min[Eparl_EDR_avg<0.] = (
        -Eparl_EDR_min[Eparl_EDR_avg<0.], -Eparl_EDR_max[Eparl_EDR_avg<0.] )
Eparl_EDR_avg = np.abs(Eparl_EDR_avg)

ax0.plot(t_all, Eparl_EDR_avg, 'k-', label='avg')
ax0.plot(t_all, Eparl_EDR_avg+Eparl_EDR_std, 'k--', label='avg+std')
ax0.plot(t_all, Eparl_EDR_avg+2.*Eparl_EDR_std, 'k-.', label='avg+2std')
ax0.plot(t_all, Eparl_EDR_avg+3.*Eparl_EDR_std, 'k:', label='avg+3std')
ax0.plot(t_all, Eparl_EDR_max, 'r-', label='max')

t0 = 2.5
t1 = 45.
it0 = np.argmin(np.abs(t_all - t0))
it1 = np.argmin(np.abs(t_all - t1))
coef = np.polyfit(t_all[it0:it1+1], np.log(Eparl_EDR_avg)[it0:it1+1], 1)
poly1d_fn = np.poly1d(coef)
ax0.plot(t_all[it0:it1+1], np.exp(poly1d_fn(t_all[it0:it1+1])), 'g-',
         label='$\gamma = %.3f \Omega_{\mathrm{p}}$'%(coef[0]))
coef = np.polyfit(t_all[it0:it1+1], np.log(Eparl_EDR_avg+Eparl_EDR_std)[it0:it1+1], 1)
poly1d_fn = np.poly1d(coef)
ax0.plot(t_all[it0:it1+1], np.exp(poly1d_fn(t_all[it0:it1+1])), 'g--',
         label='$\gamma = %.3f \Omega_{\mathrm{p}}$'%(coef[0]))
coef = np.polyfit(t_all[it0:it1+1], np.log(Eparl_EDR_avg+2.*Eparl_EDR_std)[it0:it1+1], 1)
poly1d_fn = np.poly1d(coef)
ax0.plot(t_all[it0:it1+1], np.exp(poly1d_fn(t_all[it0:it1+1])), 'g-.',
         label='$\gamma = %.3f \Omega_{\mathrm{p}}$'%(coef[0]))
coef = np.polyfit(t_all[it0:it1+1], np.log(Eparl_EDR_avg+3.*Eparl_EDR_std)[it0:it1+1], 1)
poly1d_fn = np.poly1d(coef)
ax0.plot(t_all[it0:it1+1], np.exp(poly1d_fn(t_all[it0:it1+1])), 'g:',
         label='$\gamma = %.3f \Omega_{\mathrm{p}}$'%(coef[0]))
coef = np.polyfit(t_all[it0:it1+1], np.log(Eparl_EDR_max)[it0:it1+1], 1)
poly1d_fn = np.poly1d(coef)
ax0.plot(t_all[it0:it1+1], np.exp(poly1d_fn(t_all[it0:it1+1])), 'r-',
         label='$\gamma = %.3f \Omega_{\mathrm{p}}$'%(coef[0]))

ax0.set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
ax0.set_ylabel('$E_{\parallel}/B_0/v_A$ - EDR1')
ax0.set_xlim(t_all[0], t_all[-1])
ax0.set_ylim(max(np.min(Eparl_EDR_avg[Eparl_EDR_avg>0.]), 1.e-5), np.max(Eparl_EDR_max))
ax0.set_yscale('log')
ax0.legend(ncol=2)
fig0.tight_layout()
plt.show()
plt.close(fig0)
