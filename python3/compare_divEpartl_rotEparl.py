# coding: utf-8
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

path1 = "/home/frafin/Downloads/divEparl_EDR_EDRmask/divEparl_EDR2_adim_uns_log_divEparl_EDR1_EDR2_LF_3d_DH_0_182.csv"
path2 = "/home/frafin/Downloads/rotEparl_EDR_EDRmask/rotEparl_EDR2_adim_uns_log_rotEparl_EDR1_EDR2_LF_3d_DH_0_182.csv"

df1 = pd.read_csv(path1, header=0, index_col=0)
df2 = pd.read_csv(path2, header=0, index_col=0)

plt.plot(df1["t"], (df1["avg1"]+3.*df1["std1"])/(df2["avg1"]+3.*df2["std1"]))
plt.xlabel("t")
plt.ylabel("divEparl / rotEparl")
plt.show()
