# coding: utf-8
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

#latex font
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

path1 = "/work2/finelli/Outputs/Eparlirrnorm_EDR_EDRmask/Eparlirrnorm_EDR1_EDR2_LF_3d_DH_0_182.csv"
path2 = "/work2/finelli/Outputs/Eparlsolnorm_EDR_EDRmask/Eparlsolnorm_EDR1_EDR2_LF_3d_DH_0_182.csv"

df1 = pd.read_csv(path1, header=0, index_col=0)
t1 = df1["t"]
m1 = df1["avg1"]
s1 = df1["std1"]
rms1 = np.sqrt(m1*m1 + s1*s1)
df2 = pd.read_csv(path2, header=0, index_col=0)
t2 = df2["t"]
m2 = df2["avg1"]
s2 = df2["std1"]
rms2 = np.sqrt(m2*m2 + s2*s2)

plt.plot(t1, m1/m2, label="averages ratio")
plt.plot(t1, rms1/rms2, label="RMSs ratio")
plt.xlabel("$t \;[\Omega_{\mathrm{p}}^{-1}]$")
plt.ylabel("$E_{\parallel}^{(irr)}/E_{\parallel}^{(sol)}$ - EDR1")
plt.legend()
plt.show()
