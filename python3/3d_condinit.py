import sys

import numpy as np
import matplotlib.pyplot as plt

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
import mylib as ml
from mod_from import *

if len(sys.argv) != 4:
    print('provide 3 arguments: L1, L2, Bx0')
    sys.exit()

run,tr,xr = wl.Init('LF_3d_DH.dat')

nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']

ind = 0
_, B_ = run.get_EB(ind)
n_, _ = run.get_Ion(ind)

def B(y, L1, L2, Bx0, yl=yl):
    y1 = yl/(2.*(1.+L2/L1))
    y2 = yl - y1*L2/L1
    A_1 = - Bx0
    A_2 = - A_1
    return - A_1*np.tanh((y-y1)/L1) - A_2*np.tanh((y-y2)/L2) - np.sign(A_1)*(A_1 - A_2)/2.

def n(y, L1, L2, Bx0, yl=yl):
    y1 = yl/(2.*(1.+L2/L1))
    y2 = yl - y1*L2/L1
    A_1 = - Bx0
    A_2 = - A_1
    Ti = .5
    TeTi = .25
    n1 = A_1*A_1/(2.*Ti*(1.+TeTi))
    n2 = A_2*A_2/(2.*Ti*(1.+TeTi))
    nb0 = 1.
    return n1/np.cosh((y-y1)/L1)**2 + n2/np.cosh((y-y2)/L2)**2 + nb0

def n1(y, L1, L2, Bx0, yl=yl):
    y1 = yl/(2.*(1.+L2/L1))
    A_1 = - Bx0
    Ti = .5
    TeTi = .25
    n1 = A_1*A_1/(2.*Ti*(1.+TeTi))
    return n1/np.cosh((y-y1)/L1)**2

def n2(y, L1, L2, Bx0, yl=yl):
    y1 = yl/(2.*(1.+L2/L1))
    y2 = yl - y1*L2/L1
    A_1 = - Bx0
    A_2 = - A_1
    Ti = .5
    TeTi = .25
    n2 = A_2*A_2/(2.*Ti*(1.+TeTi))
    return n2/np.cosh((y-y2)/L2)**2

plt.close('all')
plt.plot(y, B_[0, 0, :, 0], label='data')
plt.plot(y, B(y, float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])), label='profile')
plt.legend()
plt.show()

plt.close('all')
plt.plot(y, n_[0, :, 0], label='data')
plt.plot(y, n(y, float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])), label='profile')
y1 = yl/(2.*(1.+float(sys.argv[2])/float(sys.argv[1])))
y2 = yl - y1*float(sys.argv[2])/float(sys.argv[1])
plt.axvline(x=y2)
plt.axvline(x=y2+5)
plt.axvline(x=y2-5)
plt.legend()
plt.show()

plt.close('all')
plt.plot(y, n1(y, float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])), label='CS1')
plt.plot(y, n2(y, float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])), label='CS2')
y1 = yl/(2.*(1.+float(sys.argv[2])/float(sys.argv[1])))
y2 = yl - y1*float(sys.argv[2])/float(sys.argv[1])
plt.axvline(x=y1,c='r',label='y1')
plt.axvline(x=y2,c='g',label='y2')
plt.legend()
plt.show()
