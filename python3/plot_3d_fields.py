"""
slice and z-averaged plots from 2D sim.
plots: DBz, E_parl, Jz and Je_perp
"""

#-----------------------------------------------------
#importing stuff
#---------------
import sys
import gc
import os
import glob

import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt

import work_lib as wl
#sys.path.insert(0,'/work2/finelli/phd_scripts/python3/my_branch')
import mylib as ml
#from fibo_beta import *
from mod_calc import *
#from iPIC_loader import *
#from HVM_loader import *
from mod_from import *

#latex fonts
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\n2d plots of DBz, E_parl, Jz and Je_perp.\n')

#------------------------------------------------------
#Init
#----
input_file = sys.argv[1]
z_slices_rel_pos = [.5, -1.]

plt_show_flg = True
plt_save_flg = False
zoom_flg = True
rms_flg = False

Jperpe_flg = False

run,tr,xr=wl.Init(input_file)
ind,_,_ = tr
ixmin,ixmax,iymin,iymax = xr
run_name = run.meta['run_name']
w_ele = run.meta['w_ele']
times = run.meta['time']*run.meta['tmult']
x_ = run.meta['x']
y_ = run.meta['y']
z_ = run.meta['z']
nx = run.meta['nx']
ny = run.meta['ny']
nz = run.meta['nz']
code_name = run.meta['code_name']
#
if code_name == 'HVM':
    calc = fibo_calc()
    calc.meta = run.meta
    if w_ele:
        run_label = 'HVLF'
    else:
        run_label = 'HVM'
    B0 = 0.718722  # 1.
    n0 = 1.
    V0 = 0.718722  # 1.
elif code_name == 'iPIC':
    run_label = 'iPIC'
    qom_e = run.meta['msQOM'][0]
    B0 = 0.01
    n0 = 1./(4.*np.pi)
    V0 = 0.01
else:
    print('ERROR: unknown code_name %s'%(code_name))
    sys.exit(-1)
#
opath = run.meta['opath']

DBz_dict = {}
Eparl_dict = {}
Jz_dict = {}
Jperpe_dict = {}
Psi_dict = {}

#get data
#magnetic fields and co.
E,B = run.get_EB(ind)
DBz = B[2] - run.meta['Bz0']
Eparl = np.divide(np.sum(np.multiply(E,B),axis=0),
                   np.sqrt(np.sum(np.multiply(B,B),axis=0)))
del E
#
n_p,u_p = run.get_Ion(ind)
if code_name == 'HVM':
    Jx,Jy,Jz = calc.calc_curl(B[0],B[1],B[2])
    Je = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    Je[0] = n_p*u_p[0] - Jx
    Je[1] = n_p*u_p[1] - Jy
    Je[2] = n_p*u_p[2] - Jz
    del Jx,Jy
elif code_name == 'iPIC':
    n_e,u_e = run.get_Ion(ind,qom=qom_e)
    Jz = n_p*u_p[2] - n_e*u_e[2]
    Je = np.empty(B.shape,dtype=type(B[0,0,0,0]))
    Je[0] = - n_e*u_e[0]
    Je[1] = - n_e*u_e[1]
    Je[2] = - n_e*u_e[2]
    del n_e,u_e
del n_p,u_p
#
if Jperpe_flg:
    Jparle = (Je[0]*B[0] + Je[1]*B[1] + Je[2]*B[2])/np.sqrt(B[0]*B[0] + B[1]*B[1] + B[2]*B[2])
    Jperpe = np.sqrt((Je[0]*Je[0] + Je[1]*Je[1] + Je[2]*Je[2]) - Jparle*Jparle)
    del Jparle
else:
    Jperpe = np.sqrt(Je[0]**2+Je[1]**2)
del Je
#
t =  times[ind]
del times
#
x = x_[ixmin:ixmax]
y = y_[iymin:iymax]
del x_,y_
#
iz_dict = {}
for rel_z in z_slices_rel_pos:
    if rel_z >= 0.:
        iz = int(nz*rel_z)
        z_str = '$z=%.2f\;[d_{\mathrm{p}}]$'%(round(z_[iz],2))
        iz_dict[z_str] = iz
#
        B_ = B[..., iz]
        DBz_ = DBz[..., iz]
        Eparl_ = Eparl[..., iz]
        Jz_ = Jz[..., iz]
        Jperpe_ = Jperpe[..., iz]
    else:
        z_str = '$z$ avg.'
        iz_dict[z_str] = 'avg'
#
        B_ = np.mean(B, axis=-1)
        DBz_ = np.mean(DBz, axis=-1)
        Eparl_ = np.mean(Eparl, axis=-1)
        Jz_ = np.mean(Jz, axis=-1)
        Jperpe_ = np.mean(Jperpe, axis=-1)
#
    Psi_ = wl.psi_2d(B_[:-1],run.meta)
    Psi_dict[z_str] = Psi_[ixmin:ixmax,iymin:iymax]/B0
    del B_, Psi_
#
    if rms_flg:
        DBz_rms = np.sqrt(np.mean(DBz_[ixmin:ixmax,iymin:iymax]*DBz_[ixmin:ixmax,iymin:iymax]))
        DBz_dict[z_str] = DBz_[ixmin:ixmax,iymin:iymax]/DBz_rms
    else:
        DBz_dict[z_str] = DBz_[ixmin:ixmax,iymin:iymax]/B0
    del DBz_
#
    Eparl_rms = np.sqrt(np.mean(Eparl_[ixmin:ixmax,iymin:iymax]*Eparl_[ixmin:ixmax,iymin:iymax]))
    Eparl_dict[z_str] = Eparl_[ixmin:ixmax,iymin:iymax]/Eparl_rms
    del Eparl_
#
    if rms_flg:
        Jz_rms = np.sqrt(np.mean(Jz_[ixmin:ixmax,iymin:iymax]*Jz_[ixmin:ixmax,iymin:iymax]))
        Jz_dict[z_str] = Jz_[ixmin:ixmax,iymin:iymax]/Jz_rms
    else:
        Jz_dict[z_str] = Jz_[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del Jz_
#
    if rms_flg:
        Jperpe_rms = np.sqrt(np.mean( 
                     Jperpe_[ixmin:ixmax,iymin:iymax]*Jperpe_[ixmin:ixmax,iymin:iymax]))
        Jperpe_dict[z_str] = Jperpe_[ixmin:ixmax,iymin:iymax]/Jperpe_rms
    else:
        Jperpe_dict[z_str] = Jperpe_[ixmin:ixmax,iymin:iymax]/(n0*V0)
    del Jperpe_
#
    gc.collect()

#----------------
#PLOT TIME!!!!!!!
#----------------
plt.close('all')
fig,ax = plt.subplots(4,len(iz_dict),figsize=(18,14),sharex=True,sharey=True)
fig.subplots_adjust(hspace=.06,wspace=.03,top=.95,bottom=.1,left=.055,right=1.075)

vmin = ml.min_dict(DBz_dict)
vmax = ml.max_dict(DBz_dict)
vminDBz = -max(vmax,-vmin)
vmaxDBz = max(vmax,-vmin)

vmin = ml.min_dict(Eparl_dict)
vmax = ml.max_dict(Eparl_dict)
vminEparl = -max(vmax,-vmin)
vmaxEparl = max(vmax,-vmin)

vminJz = ml.min_dict(Jz_dict)
vmaxJz = ml.max_dict(Jz_dict)

vminJperpe = ml.min_dict(Jperpe_dict)
vmaxJperpe = ml.max_dict(Jperpe_dict)

for j, z_str in enumerate(iz_dict):
    i = 0
    im0 = ax[i,j].pcolormesh(x, y, DBz_dict[z_str].T, shading='gouraud',
                           vmin=vminDBz, vmax=vmaxDBz, cmap='PuOr')
    ax[i,j].contour(x, y, Psi_dict[z_str].T, 8, colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[i,j].set_title(z_str, pad=10.)
#
    i = 1
    im1 = ax[i,j].pcolormesh(x, y, Eparl_dict[z_str].T, shading='gouraud',
                             cmap='seismic',
                             vmin=vminEparl, vmax=vmaxEparl)
    ax[i,j].contour(x, y, Psi_dict[z_str].T, 8, colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_i]$')
#
    i = 2
    im2 = ax[i,j].pcolormesh(x, y, Jz_dict[z_str].T, shading='gouraud',
                             vmin=vminJz, vmax=vmaxJz, cmap='Reds_r')
    ax[i,j].contour(x, y, Psi_dict[z_str].T, 8, colors='black')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
#
    i = 3
    im3 = ax[i,j].pcolormesh(x, y, Jperpe_dict[z_str].T, shading='gouraud',
                          vmin=vminJperpe, vmax=vmaxJperpe, cmap='hot')
    ax[i,j].contour(x, y, Psi_dict[z_str].T, 8, colors='white')
    ax[i,j].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
    if j == 0:
        ax[i,j].set_ylabel('$y\quad [d_{\mathrm{p}}]$')

cb0 = plt.colorbar(im0, ax=ax[0,:], pad=.007)
if rms_flg:
    label_ = '$\Delta B_z / \overline{B_z}$'
else:
    label_ = '$\Delta B_z / B_0$'
cb0.set_label(label_, rotation=270, labelpad=20)

cb1 = plt.colorbar(im1, ax=ax[1,:], pad=.007)
if rms_flg:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
else:
    label_ = '$E_{\parallel} / \overline{E_{\parallel}}$'
cb1.set_label(label_, rotation=270, labelpad=35)

cb2 = plt.colorbar(im2, ax=ax[2,:], pad=.007)
if rms_flg:
    label_ = '$J_z / \overline{J_z}$'
else:
    label_ = '$J_z / ( e n_0 v_A )$'
cb2.set_label(label_, rotation=270, labelpad=25)

cb3 = plt.colorbar(im3, ax=ax[3,:], pad=.007)
if Jperpe_flg:
    if rms_flg:
        label_ = '$J_{\perp,\mathrm{e}} / \overline{J_{\perp,\mathrm{e}}}$'
    else:
        label_ = '$J_{\perp,\mathrm{e}} / ( e n_0 v_A )$'
else:
    if rms_flg:
        label_ = '$J_{\mathrm{e}}^{(\\text{plane})} / \overline{J_{\mathrm{e}}^{(\\text{plane})}}$'
    else:
        label_ = '$J_{\mathrm{e}}^{(\\text{plane})} / ( e n_0 v_A )$'
cb3.set_label(label_, rotation=270, labelpad=45)

if plt_show_flg:
    plt.show()
if plt_save_flg:
    out_dir = 'plot_3d_fields'
    ml.create_path(opath+'/'+out_dir)
    out_dir += '/comp'
    fig_name = 'DBz_'
    fig_name += 'Eparl_'
    fig_name += 'Jz_'
    if Jperpe_flg:
        fig_name += 'Jperpe'
    else:
        fig_name += 'Jeplane'
    fig_name += '__run_name'
    for z_str in iz_dict:
        out_dir += '__' + str(iz_dict[z_str])
        fig_name += '__' + str(iz_dict[z_str]) + '_ind%d'%ind

    if zoom_flg:
        fig_name += '__zoom'
    if rms_flg:
        fig_name += '__rms'
    fig_name += '.png'
    ml.create_path(opath+'/'+out_dir)
    fig.savefig(opath+'/'+out_dir+'/'+fig_name)
plt.close()
