"""
Reads data from Eparl_EDRvsBKG_zsplit.py outputs (.csv file) and plots them
"""
# coding: utf-8
from os.path import join as os_join

import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

# read dataframe
fpath = "/work2/finelli/Outputs/Eparl_EDRvsBKG_zsplit"
fname = "Eparl_stats_zsplit_LF_3d_DH_0_182.csv"
df = pd.read_csv(os_join(fpath, fname), index_col=0)
pname = fname.split('zsplit_')[1].split('.')[0]

# extract times and z values
t = sorted(list(set(df["t"].values)))
z = sorted(list(set(df["z"].values)))

# plot R = ( |<E_parl>| + 2*std(E_parl) ) / B_0 / v_A
# for each z-plane, for EDR, BKG, and EDR - BKG
colors = mpl.cm.hsv(np.linspace(0., 1., len(z)))

for iz, zz in enumerate(z):
    plt.plot(t,
             np.abs(df.loc[df["z"] == zz, ["EDR_avg"]].values[:, 0]) +
             2.*df.loc[df["z"] == zz, ["EDR_std"]].values[:, 0],
             c=colors[iz])
plt.yscale('log')
plt.xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
plt.ylabel('R(EDR) (z-plane)')
plt.colorbar(mpl.cm.ScalarMappable(
             norm=mpl.colors.Normalize(vmin=z[0], vmax=z[-1]),
             cmap=mpl.cm.hsv), label='$z\;[d_{\mathrm{p}}]$')
plt.savefig(os_join(fpath, 'R_EDR_zplanes_{}.png'.format(pname)))
plt.close()

plt.subplot(311)
for iz, zz in enumerate(z):
    plt.plot(t,
             np.abs(df.loc[df["z"] == zz, ["EDR_avg"]].values[:, 0]) +
             2.*df.loc[df["z"] == zz, ["EDR_std"]].values[:, 0],
             c=colors[iz])
plt.yscale('log')
plt.ylabel('R(EDR) (z-plane)')
plt.colorbar(mpl.cm.ScalarMappable(
             norm=mpl.colors.Normalize(vmin=z[0], vmax=z[-1]),
             cmap=mpl.cm.hsv), label='$z\;[d_{\mathrm{p}}]$')

plt.subplot(312)
for iz, zz in enumerate(z):
    plt.plot(t,
             np.abs(df.loc[df["z"] == zz, ["BKG_avg"]].values[:, 0]) +
             2.*df.loc[df["z"] == zz, ["BKG_std"]].values[:, 0],
             c=colors[iz])
plt.yscale('log')
plt.ylabel('R(BKG) (z-plane)')
plt.colorbar(mpl.cm.ScalarMappable(
             norm=mpl.colors.Normalize(vmin=z[0], vmax=z[-1]),
             cmap=mpl.cm.hsv), label='$z\;[d_{\mathrm{p}}]$')

plt.subplot(313)
for iz, zz in enumerate(z):
    plt.plot(t,
             (np.abs(df.loc[df["z"] == zz, ["EDR_avg"]].values[:, 0]) +
             2.*df.loc[df["z"] == zz, ["EDR_std"]].values[:, 0]) -
             (np.abs(df.loc[df["z"] == zz, ["BKG_avg"]].values[:, 0]) +
             0.*df.loc[df["z"] == zz, ["BKG_std"]].values[:, 0]),
             c=colors[iz])
plt.yscale('log')
plt.xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
plt.ylabel('R(EDR)-R(BKG) (z-plane)')
plt.colorbar(mpl.cm.ScalarMappable(
             norm=mpl.colors.Normalize(vmin=z[0], vmax=z[-1]),
             cmap=mpl.cm.hsv), label='$z\;[d_{\mathrm{p}}]$')

plt.savefig(os_join(fpath, 'R_EDR_BKG_EDR-BKG_zplanes_{}.png'.format(pname)))
plt.close()

# ------------------------------
# m = <x> = SUM x / N
# s = sqrt( <x2> - <x>2)
# s2 = <x2> - <x>2
# <x2> = SUM x2 / N = s2 + m2
# ------------------------------
# plot R = ( |<E_parl>| + 2*std(E_parl) ) / B_0 / v_A
# for each z-interval
for n_ints in [1, 2, 5, 10]:
    lenz = len(z)
    every = lenz//n_ints
    colors = mpl.cm.hsv(np.linspace(0., 1., n_ints+1))

    cnt = 0
    for iz, zz in enumerate(z):
        n = df.loc[df["z"] == zz, ["EDR_num"]].values[:, 0]
        m = df.loc[df["z"] == zz, ["EDR_avg"]].values[:, 0]
        s = df.loc[df["z"] == zz, ["EDR_std"]].values[:, 0]
        m[np.isnan(m)] = 0.
        s[np.isnan(s)] = 0.
        if (iz + 1)%every == 1:
            keep_where = np.where(n==0.)[0] ###
            z0 = zz
            n_acc = n
            m_acc = n*m
            s_acc = n*(s**2 + m**2)
        else:
            new_where = [] ###
            for where in np.where(n==0.)[0]: ###
                if where in keep_where: ###
                    new_where.append(where) ###
            keep_where = new_where.copy() ###
            n_acc += n
            m_acc += n*m
            s_acc += n*(s**2 + m**2)
        if ((iz + 1)%every == 0) or (iz == lenz - 1):
            print(keep_where) ###
            m_acc[n_acc!=0] = m_acc[n_acc!=0] / n_acc[n_acc!=0]
            m_acc[n_acc==0] = 0.
            m = m_acc
            s_acc[n_acc!=0] = s_acc[n_acc!=0] / n_acc[n_acc!=0]
            s_acc[n_acc==0] = 0.
            s = np.sqrt(s_acc - m**2)
            plt.plot(t, np.abs(m) + 2.*s, c=colors[cnt],
                     label='{}-{}'.format(round(z0, 2), round(zz, 2)))
            cnt += 1
    plt.yscale('log')
    plt.xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
    plt.ylabel('R(EDR) (z-interval)')
    plt.legend()
    plt.savefig(os_join(fpath, 'R_EDR_{}-zintervals_{}.png'.format(n_ints, pname)))
    plt.close()
