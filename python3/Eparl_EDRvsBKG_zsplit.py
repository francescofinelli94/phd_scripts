#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
import gc

import numpy as np
from matplotlib import use as mpl_use
from matplotlib import get_backend as mpl_get_backend
mpl_use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm as mplcol_LogNorm
from scipy.ndimage import binary_dilation
from pandas import DataFrame

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
import mylib as ml
from mod_calc import *
from mod_from import *

#------------------------------------------------------
#Intent
#------
print('\nComputes <Eparl> std max and min in the EDR for every z, at every time, the saves on a file\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------

run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'Eparl_EDRvsBKG_zsplit'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

n0 = 1.
Bx0 = 0.718722

vA = Bx0/np.sqrt(n0)

#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------

#---> loop over times <---
df = DataFrame({'it':[],
                't':[],
                'iz':[],
                'z':[],
                'EDR_min':[],
                'EDR_max':[],
                'EDR_std':[],
                'EDR_avg':[],
                'EDR_num':[],
                'BKG_min':[],
                'BKG_max':[],
                'BKG_std':[],
                'BKG_avg':[],
                'BKG_num':[]})

print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    #------------------------------------------------------
    # Get data
    #----------
    #-------
    t0 = t_time()
    #-------
    # -> magnetic fields and co.
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm

    del Bnorm

    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]

    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
        J = np.array([cx, cy, cz]) # J = rot(B)
        del cx, cy, cz

    # -> densities and currents
    if code_name == 'HVM':
        n_p, u_p = run.get_Ion(ind)
        u_e = np.empty((3, nx, ny, nz), dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0], n_p)
        u_e[1] = u_p[1] - np.divide(J[1], n_p)
        u_e[2] = u_p[2] - np.divide(J[2], n_p)
        del n_p, J, u_p
    elif code_name == 'iPIC':
        _, u_e = run.get_Ion(ind,qom=qom_e)

    # -> demag
    e_demag = np.empty(E.shape, dtype=np.float64)
    e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
    e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
    e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
    e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)
    del e_demag, B, E, u_e
    #-------
    print('\nData loading and computing: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # selecting regions and non-dimensionalize
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    th = np.max(e_demag_n)*.75

    mask_EDR = (e_demag_n>th)
    mask_BKG = (e_demag_n>th)
    mask_BKG = np.logical_not(binary_dilation(mask_BKG, iterations=10))

    Eparl = Eparl/(Bx0*vA)
    #-------
    print('Selecting regions: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # generatying dataframe
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    tt = time[ind]
    for iz, zz in enumerate(z):
        Eparl_EDR = Eparl[mask_EDR[..., iz], iz]
        Eparl_BKG = Eparl[mask_BKG[..., iz], iz]
        EDR_num = np.sum(mask_EDR[..., iz])
        BKG_num = np.sum(mask_BKG[..., iz])
        if EDR_num == 0:
            print('t={}\tz={}\tEDR_num=0'.format(tt, zz))
        if BKG_num == 0:
            print('t={}\tz={}\tBKG_num=0'.format(tt, zz))
        df = df.append({'it':ind,
                        't':tt,
                        'iz':iz,
                        'z':zz,
                        'EDR_min':np.min(Eparl_EDR) if EDR_num > 0 else np.nan,
                        'EDR_max':np.max(Eparl_EDR) if EDR_num > 0 else np.nan,
                        'EDR_std':np.std(Eparl_EDR) if EDR_num > 0 else np.nan,
                        'EDR_avg':np.mean(Eparl_EDR) if EDR_num > 0 else np.nan,
                        'EDR_num':EDR_num,
                        'BKG_min':np.min(Eparl_BKG) if BKG_num > 0 else np.nan,
                        'BKG_max':np.max(Eparl_BKG) if BKG_num > 0 else np.nan,
                        'BKG_std':np.std(Eparl_BKG) if BKG_num > 0 else np.nan,
                        'BKG_avg':np.mean(Eparl_BKG) if BKG_num > 0 else np.nan,
                        'BKG_num':BKG_num},
                        ignore_index=True)
    #-------
    print('Generating dataframe for time t=%f: %fs'%(tt, t_time() - t0))
    #-------

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
#------------------------

#------------------------------------------------------
# saving df on file
#-------------------------
#-------
t0 = t_time()
#-------

df.to_csv(opath+'/'+out_dir+'/Eparl_stats_zsplit_%s_%d_%d.csv'%(run_name, ind1, ind2))

#-------
print('Saving df on file: %fs'%(t_time() - t0,))
#-------
