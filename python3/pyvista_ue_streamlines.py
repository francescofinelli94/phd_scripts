"""
Testing pyvista library: plotting streamlines
"""

# imports
from sys import argv as sys_argv
from sys import exit as sys_exit
from time import time as ttime

import numpy as np
import pyvista as pv
from h5py import File as h5_file

# parameters
if len(sys_argv) < 5:
    print('ERROR: not enough arguments.')
    print('Arguments:\nFNAME\t\twhole path to data file\nplot_mode\tshow or save or both\ncs_focus\tfirst or second or both\nnum_pnts\tnumber of source points')
    sys_exit()
FNAME = sys_argv[1]  # 'EB_Ion_J_LF_3d_DH_18.h5'
plot_mode = sys_argv[2]  # 'show', 'save', 'both'
cs_focus = sys_argv[3]  # 'first', 'second', 'both'
num_pnts = int(sys_argv[4])

# load data
tic = ttime()

with h5_file(FNAME, 'r') as h5file:
    nx = h5file.attrs['nx']
    ny = h5file.attrs['ny']
    nz = h5file.attrs['nz']
    dx = h5file.attrs['dx']
    dy = h5file.attrs['dy']
    dz = h5file.attrs['dz']
    this_time = h5file.attrs['this_time']

    J2 = h5file['J'][0]**2 + h5file['J'][1]**2 + h5file['J'][2]**2
    ue = h5file['up'] - np.divide(h5file['J'], h5file['np'])

toc = ttime()
print(f'Data loading:\t\t{toc - tic}s')
first_tic = tic

# generate uniform grid mesh
tic = ttime()

mesh = pv.UniformGrid()
mesh.dimensions = [nx + 1, ny + 1, nz + 1]
mesh.spacing = [dx, dy, dz]
x = mesh.points[:, 0]
y = mesh.points[:, 1]
z = mesh.points[:, 2]

toc = ttime()
print(f'Mesh generation:\t{toc - tic}s')

# add data to mesh
tic = ttime()

mesh.cell_data["J2"] = J2.flatten(order="F")
if cs_focus == 'first':
    J2 = J2[:, :ny//2, :]
elif cs_focus == 'second':
    J2 = J2[:, ny//2:, :]
elif cs_focus != 'both':
    print(f'ERROR: invalid {cs_focus = }')
    sys_exit()
J2_threshold = float(np.mean(J2) + 3.*np.std(J2))
J2_threshed = mesh.threshold(value=J2_threshold, scalars="J2")

mesh.cell_data['ue'] = np.array([ue[0].flatten(order="F"),
                                 ue[1].flatten(order="F"),
                                 ue[2].flatten(order="F")]).T

mesh = mesh.cell_data_to_point_data()

toc = ttime()
print(f'Adding data:\t\t{toc - tic}s')

# generate source points
tic = ttime()

pnts = np.array(np.where(J2 > J2_threshold), dtype=float).T
pnts[:, 0] *= dx
if cs_focus == 'second':
    pnts[:, 1] += ny//2
pnts[:, 1] *= dy
pnts[:, 2] *= dz
removed_fraction = 1. - float(num_pnts)/float(pnts.shape[0])
if removed_fraction > 0.:
    pnts = pnts[np.random.rand(pnts.shape[0])>removed_fraction, :]
src = pv.PolyData(pnts)

toc = ttime()
print(f'Generate source:\t{toc - tic}s')

# generate streamlines
tic = ttime()

stream = mesh.streamlines_from_source(src,
                          vectors='ue',
                          integrator_type=45,
                          integration_direction='both',
                          initial_step_length=0.5*dx,
                          step_unit='cl',
                          min_step_length=0.01*dx,
                          max_step_length=1.0*dx,
                          max_steps=3000,
                          terminal_speed=0.0,
                          compute_vorticity=False,
                          interpolator_type='cell',
                          progress_bar=False)

toc = ttime()
print(f'Generate streamlines:\t{toc - tic}s')

# plot
tic = ttime()

if plot_mode == 'save':
    plotter = pv.Plotter(off_screen=True)
elif plot_mode in ['show', 'both']:
    plotter = pv.Plotter()
else:
    print(f'ERROR: invalid {plot_mode = }')
    sys_exit()

_ = plotter.add_mesh(mesh.slice_orthogonal(x=50., y=dy, z=dz),
                     opacity=1., scalars='ue', component=2, cmap='Reds',
                     show_scalar_bar=False)
_ = plotter.add_mesh(mesh.slice_orthogonal(x=dx, y=dy, z=dz),
                     opacity=1., scalars='ue', component=2, cmap='Reds',
                     show_scalar_bar=False)
_ = plotter.add_mesh(J2_threshed, opacity=.2, show_scalar_bar=False)
_ = plotter.add_mesh(stream.tube(radius=0.5*dx), scalars="ue", component=0, cmap='seismic',
                     scalar_bar_args={'height':.5, 'vertical':True,
                                      'position_x':0.05, 'position_y':0.05,
                                      'title':'ue_x'})
_ = plotter.show_bounds(grid='front', location='outer', all_edges=True)
_ = plotter.add_title(f't = {this_time}')

if plot_mode == 'show':
    plotter.show()
elif plot_mode == 'both':
    plotter.show(auto_close=False)
if plot_mode in ['save', 'both']:
    plotter.screenshot(f"ue_streamlines_{cs_focus}_{FNAME.split('.')[0].split('EB_Ion_J_')[-1]}.png")

toc = ttime()
print(f'Plotting:\t\t{toc - tic}s')
last_toc = toc
print(f'Total time:\t\t{last_toc - first_tic}s')
