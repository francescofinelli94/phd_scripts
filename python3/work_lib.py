#-----------------------------------------------------
#importing stuff
#---------------
import sys
import numpy as np
from numba import jit, njit
import gc
from pylab import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit as cfit
sys.path.insert(0,'/work2/finelli/phd_scripts/python3/my_branch')
import mylib as ml
from mod_calc import *
from mod_from import*
#from fibo_beta import *
#from iPIC_loader import *
#from HVM_loader import *

#---------------------------------------------------------------------------

def Init(arg1):
    #read input file
    try:
        input_file = open(arg1,'r')
    except:
        ml.error_msg('can not open the input_file.')
    lines = input_file.readlines()
    l0  = (lines[ 0]).split()
    l1  = (lines[ 1]).split()
    l2  = (lines[ 2]).split()
    l3  = (lines[ 3]).split()
    l4  = (lines[ 4]).split()
    l5  = (lines[ 5]).split()
    input_file.close()

    #loading metadata
    run_name = l0[0]
    code_name = l0[1]
    ipath = l1[0]
    opath = l2[0]

    if code_name == 'HVM':
        run = from_HVM(ipath)
    elif code_name == 'iPIC':
        run = from_iPIC(ipath)
    else:
        print('\nWhat code is this?\n')
    try:
        run.get_meta()
    except:
        run.get_meta(l5[0])

    run.meta['w_ele'] = (run.meta['model'] > 1) or (run.meta['model'] < 0)

    run.meta['opath'] = opath

    run.meta['run_name'] = run_name
    
    time = run.meta['time']

    x = np.linspace(0., run.meta['xl']-run.meta['dx'], run.meta['nx'])
    y = np.linspace(0., run.meta['yl']-run.meta['dy'], run.meta['ny'])
    z = np.linspace(0., run.meta['zl']-run.meta['dz'], run.meta['nz'])

    run.meta['x'] = x
    run.meta['y'] = y
    run.meta['z'] = z

    if code_name == 'HVM':
        tmp = 0
        del tmp
    elif code_name == 'iPIC':
        run.meta['mime'] = abs(run.meta['msQOM'][0]/run.meta['msQOM'][1])
    else:
        print('\nWhat code is this?\n')

    if code_name == 'HVM':
        tmp = 0
        del tmp
    elif code_name == 'iPIC':
        P0 = run.get_Pspec(1,'0') #HARDCODED stag
        P3 = run.get_Pspec(1,'3') #HARDCODED stag
        teti = np.mean([np.mean(np.divide(P0[0,0],P3[0,0])),
                        np.mean(np.divide(P0[1,1],P3[1,1])),
                        np.mean(np.divide(P0[2,2],P3[2,2]))])
        del P0,P3
        run.meta['teti'] = teti
    else:
        print('\nWhat code is this?\n')

    if code_name == 'iPIC':
        run.meta['tmult'] = 0.01
    else:
        run.meta['tmult'] = 1.

    run.meta['code_name'] = code_name

    print('\nMetadata loaded\n')

    #time and space intervals
    tbyval = bool(l3[0])
    if tbyval:
        t1 = float(l3[1])
        ind1 = ml.index_of_closest(t1,time)
        t2 = float(l3[2])
        ind2 = ml.index_of_closest(t2,time)
    else:
        ind1 = int(l3[1])
        ind2 = int(l3[2])
    print('Selected time interval is '+str(time[ind1])+' to '+str(time[ind2])+' (index '+str(ind1)+' to '+str(ind2)+')')
    ind_step = int(l3[3])

    xbyval = bool(l4[0])
    if xbyval:
        xmin  = float(l4[1])
        ixmin = ml.index_of_closest(xmin,x)
        xmax  = float(l4[2])
        ixmax = ml.index_of_closest(xmax,x)
        ymin  = float(l4[3])
        iymin = ml.index_of_closest(ymin,y)
        ymax  = float(l4[4])
        iymax = ml.index_of_closest(ymax,y)
    else:
        ixmin = float(l4[1])
        ixmax = float(l4[2])
        iymin = float(l4[3])
        iymax = float(l4[4])
    print('Selected space interval is '+str(x[ixmin])+' to '+str(x[ixmax])+' , '+str(y[iymin])+' to '+str(y[iymax])+' (index '+str(ixmin)+' to '+str(ixmax)+' , '+str(iymin)+' to '+str(iymax)+')')

    print('\nWill return: run,[ind1,ind2,ind_step],[ixmin,ixmax,iymin,iymax]\n')

    return run,[ind1,ind2,ind_step],[ixmin,ixmax,iymin,iymax]

#-------------------------------------------------------------------------------

def Can_I():
    ans = ''
    while (ans != 'y') and (ans != 'n'):
        ans = ml.secure_input('Continue? (y/n) ','',False)
    if ans == 'n':
        sys.exit(-1)

#--------------------------------------------------------------------------------

def get_Anis(run,ind):
    code_name = run.meta['code_name']
    w_ele = run.meta['w_ele']

    _,B = run.get_EB(ind)
    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    del Pp
    anisp = np.divide(pperpp,pparlp) - 1.
    del pperpp
    del pparlp
#
    if w_ele:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            rho,_ = run.get_Ion(ind)
            pparle = tparle*rho
            pperpe = tperpe*rho
            del tparle
            del tperpe
        elif code_name == 'iPIC':
            qom_e = run.meta['msQOM'][0]
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            del Pe
        else:
            print('\nWhat code is it?\n')
        anise = np.divide(pperpe,pparle) - 1.
        del pperpe
        del pparle
        del B
#
    if w_ele:
        return anisp,anise
    else:
        return anisp

#------------------------------------------------------------------------------------

def get_Entropies(run,ind,return_dens=False,return_press=False):
    code_name = run.meta['code_name']
    w_ele = run.meta['w_ele']

    #params
    g_iso = 1.
    g_adiab = 5./3.
    g_parl = 3.
    g_perp = 2.

    #get data
    n_p,_ = run.get_Ion(ind)
    if code_name == 'HVM':
        n_e = n_p
    elif code_name == 'iPIC':
        qom_e = run.meta['msQOM'][0]
        n_e,_ = run.get_Ion(ind,qom=qom_e)

    _,B = run.get_EB(ind)

    Pp = run.get_Press(ind)
    pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
    Pisop = (Pp[0,0] + Pp[1,1] + Pp[2,2])/3.
    del Pp

    if not w_ele:
        Pisoe = n_e*run.meta['beta']*0.5*run.meta['teti']
    else:
        if code_name == 'HVM':
            tparle,tperpe = run.get_Te(ind)
            pparle = tparle*n_e
            pperpe = tperpe*n_e
            del tparle
            del tperpe
            Pisoe = (pparle + 2.*pperpe)/3.
        elif code_name == 'iPIC':
            Pe = run.get_Press(ind,qom=qom_e)
            pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
            Pisoe = (Pe[0,0] + Pe[1,1] + Pe[2,2])/3.
            del Pe

    Bn = np.sqrt(B[0]**2+B[1]**2+B[2]**2)
    del B

    #get entropies
    if code_name == 'HVM':
        Skp,dSkp = run.get_Entropy(ind)

    Sfp = (3./2.)*np.log(Pisop/(n_p**g_adiab))
    Sfe = (3./2.)*np.log(Pisoe/(n_e**g_adiab))

    Sgp = (3./2.)*np.log((pparlp**(1./3.))*(pperpp**(2./3.))/(n_p**g_adiab))
    if w_ele:
        Sge = (3./2.)*np.log((pparle**(1./3.))*(pperpe**(2./3.))/(n_e**g_adiab))

    #outputs
    out = [Sfp,Sfe,Sgp]
    out_name = 'Sfp,Sfe,Sgp'
    if w_ele:
        out_name = out_name + ',Sge'
        out.append(Sge)
    if code_name == 'HVM':
        out_name = out_name + 'Skp,dSkp'
        out.append(Skp)
        out.append(dSkp)
    if return_dens:
        out_name = out_name + 'n_e,n_p'
        out.append(n_p)
        out.append(n_e)
    if return_press:
        out_name = out_name + 'Pisop,pparlp,pperpp,Pisoe,pparle,pperpe'
        out.append(Pisop)
        out.append(pparlp)
        out.append(pperpp)
        out.append(Pisoe)
        out.append(pparle)
        out.append(pperpe)

    print('OUTPUTS: '+out_name)
    return out

#--------------------------------------------------------------------------------

def calc_deriv(x,dy,periodic=False):
    #determine the coefficients
    nx = len(x)

    if nx == 1:
        return np.zeros(nx)

    else:
        #6th order of accuracy central finite difference coefficients
        cm3 =  -1./60.
        cm2 =   3./20.
        cm1 =  -3./4.
        cp1 =   3./4.
        cp2 =  -3./20.
        cp3 =   1./60.

        if not periodic:
            #6th order of accuracy forward finite difference coefficients
            f0  = -49./20.
            fp1 =   6.
            fp2 = -15./2.
            fp3 =  20./3.
            fp4 = -15./4.
            fp5 =   6./5.
            fp6 =  -1./6.

            #6th order of accuracy backward finite difference coefficients
            b0  =  49./20.
            bm1 =  -6.
            bm2 =  15./2.
            bm3 = -20./3.
            bm4 =  15./4.
            bm5 =  -6./5.
            bm6 =   1./6.

        #create the new vector, fill it
        dx = np.zeros((nx),dtype=np.float64)

        dx[3:nx-3]  = cp1*x[3+1:nx-3+1] + cm1*x[3-1:nx-3-1]
        dx[3:nx-3] += cp2*x[3+2:nx-3+2] + cm2*x[3-2:nx-3-2]
        dx[3:nx-3] += cp3*x[3+3:nx-3+3] + cm3*x[3-3:nx-3-3]

        #deal with boundaries
        if periodic:
            for i in range(3):
                dx[i]  = cp1*x[i+1] + cm1*x[(i-1+nx)%nx]
                dx[i] += cp2*x[i+2] + cm2*x[(i-2+nx)%nx]
                dx[i] += cp3*x[i+3] + cm3*x[(i-3+nx)%nx]

                dx[nx-3+i]  = cp1*x[(nx-3+i+1)%nx] + cm1*x[nx-3+i-1]
                dx[nx-3+i] += cp2*x[(nx-3+i+2)%nx] + cm2*x[nx-3+i-2]
                dx[nx-3+i] += cp3*x[(nx-3+i+3)%nx] + cm2*x[nx-3+i-3]

        else:
            dx[0:3]  = f0*x[0:3]
            dx[0:3] += fp1*x[0+1:3+1] + fp2*x[0+2:3+2] + fp3*x[0+3:3+3]
            dx[0:3] += fp4*x[0+4:3+4] + fp5*x[0+5:3+5] + fp6*x[0+6:3+6]

            dx[nx-3:nx]  = b0*x[nx-3:nx]
            dx[nx-3:nx] += bm1*x[nx-3-1:nx-1] + bm2*x[nx-3-2:nx-2] + bm3*x[nx-3-3:nx-3]
            dx[nx-3:nx] += bm4*x[nx-3-4:nx-4] + bm5*x[nx-3-5:nx-5] + bm6*x[nx-3-6:nx-6]

        #print('done with the new array!')
        return np.divide(dx,dy)

#--------------------------------------------------------------------------------

def psi_2d(V,meta):
    """
    psi: psi = -fft(Bx)/iky & psi = fft(By)/ikx

    V = (2,nx,ny)
    meta = metadata (from loader)
    """
    sh = np.shape(V)
    if (sh[0] != 2) or (len(sh) != 3):
        print('ERROR in psi_2d: vector shape is not (2,nx,ny)')
        return None

    nx = meta['nx']
    ny = meta['ny']
    xl = meta['xl']
    yl = meta['yl']

    nxm = nx//2
    nym = ny//2

    mtrc1 = xl / (2. * np.pi)
    mtrc2 = yl / (2. * np.pi)

    a1  = np.zeros([nx,ny],dtype=np.cdouble)
    a2  = np.zeros([nx,ny],dtype=np.cdouble)
    a3  = np.zeros([nx,ny],dtype=np.cdouble)
    Psi = np.zeros([nx,ny],dtype=np.float64)

    a1 = np.fft.fft2(V[0])
    a2 = np.fft.fft2(V[1])

    for ix in range(1,nxm+1):
        a3[ix,:] = -1j * a2[ix,:] / float(ix) *mtrc1

    for ix in range(nxm+1,nx):
        a3[ix,:] =  1j * a2[ix,:] / float(nx-ix) *mtrc1

    for iy in range(1,nym+1):
        a3[0,iy] =  1j * a1[0,iy] / float(iy) *mtrc2

    for iy in range(nym+1,ny):
        a3[0,iy] = -1j * a1[0,iy] / float(ny-iy) *mtrc2

    a3[0,0] = 0.0

    Psi[:,:] = np.real(np.fft.ifft2(a3))

    return Psi

#--------------------------------------------------------------------------------

def my_scatter_3d(field,x=[],y=[],z=[],
                  step=[1,1,1],th=None,
                  figsize=(12,9),xlabel='x',ylabel='y',zlabel='z',crop=False):
    if len(field.shape) != 3:
        print('ERROR: input data is not a 3D field.')
        return
    if len(step) != 3:
        print('ERROR: step vector has not length 3.')
        return
    nx,ny,nz = field.shape
    if x == []:
        x = np.arange(0,nx,1)
    if y == []:
        y = np.arange(0,ny,1)
    if z == []:
        z = np.arange(0,nz,1)
#   
    xs=[]
    yz=[]
    zs=[]
    ys=[]
    vs=[]
    for ix in range(nx//step[0]):
        for iy in range(ny//step[1]):
            for iz in range(nz//step[2]):
                val = field[ix*step[0],iy*step[1],iz*step[2]]
                if (th != None) and (val < th):
                    continue
                xs.append(x[ix*step[0]])
                ys.append(y[iy*step[1]])
                zs.append(z[iz*step[2]])
                vs.append(val)
#
    plt.close()
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111,projection='3d')
    cm = plt.cm.get_cmap('RdYlBu')
    sc = ax.scatter(xs, ys, zs, c=vs, vmin=np.min(vs), vmax=np.max(vs), cmap=cm)
    plt.colorbar(sc)
    if crop:
        ax.set_xlim(np.min(xs),np.max(xs))
        ax.set_ylim(np.min(ys),np.max(ys))
        ax.set_zlim(np.min(zs),np.max(zs))
    else:
        ax.set_xlim(np.min(x),np.max(x))
        ax.set_ylim(np.min(y),np.max(y))
        ax.set_zlim(np.min(z),np.max(z))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    plt.show()
    plt.close()
#
    return

#------------------------------------------------------------------------

#compute anisotropy
def compute_anis_and_betaparl(run,ind,
    B=None,pparlp=None,pperpp=None,pparle=None,pperpe=None,n_p=None,
    r_B=False,r_pparlp=False,r_pperpp=False,r_pparle=False,r_pperpe=False,r_n_p=False):
    """
    FUNCTION:
    compute_anis_and_betaparl -> compute anisotropies and
                                 parallel beta given a 'loader' object

    INPUTS:
    run -> loader object
    int -> integer, time index
    B,pparlp,pperpp,ppaele,pperpe         -> [optional] np.ndarray or None (default), some fields
    r_B,pparlp,r_pperpp,r_ppaele,r_pperpe -> [optional] bool (default False), if returning a field

    OUTPUTS:
    out_dict -> a dictionary containing the following data
    anisp  --> np.ndarray 3D (nx,ny,nz), proton anisotropy (p_perp/p_parl)
    anise  --> np.ndarray 3D (nx,ny,nz), electron anisotropy (p_perp/p_parl)
               OR None (if isotropic electrons)
    bparlp --> np.ndarray 3D (nx,ny,nz), proton parallel beta
    bparle --> np.ndarray 3D (nx,ny,nz), electron parallel beta
               OR None (if isotropic electrons)
    B,pparlp,pperpp,ppaele,pperpe -> [if relative r_* flag is true] np.ndarray, some fields
    """
    #init
    NoneType = type(None)
    c_B      = type(B     ) == NoneType
    c_pparlp = type(pparlp) == NoneType
    c_pperpp = type(pperpp) == NoneType
    c_pparle = type(pparle) == NoneType
    c_pperpe = type(pperpe) == NoneType
    c_n_p    = type(n_p   ) == NoneType
    d_B      = c_B      and (not r_B     )
    d_pparlp = c_pparlp and (not r_pparlp)
    d_pperpp = c_pperpp and (not r_pperpp)
    d_pparle = c_pparle and (not r_pparle)
    d_pperpe = c_pperpe and (not r_pperpe)
    d_n_p    = c_n_p     and (not r_n_p   )

    w_ele = run.meta['w_ele']
    code_name = run.meta['code_name']

    #proton anisotropy
    if c_B:
        _,B = run.get_EB(ind)

    if c_pparlp or c_pperpp:
        Pp = run.get_Press(ind)
        pparlp,pperpp = ml.proj_tens_on_vec(Pp,B,True)
        del Pp
    anisp = np.divide(pperpp,pparlp)
    if d_pperpp:
        del pperpp

    #electron anisotropy
    if w_ele:
        if c_pparle or c_pperpe:
            if code_name == 'HVM':
                if c_n_p:
                    n_p,_ = run.get_Ion(ind)
                tparle,tperpe = run.get_Te(ind)
                anise = np.divide(tperpe,tparle)
                if r_pperpe:
                    pperpe = tperpe*n_p
                del tperpe
                pparle = tparle*n_p
                del tparle
                if d_n_p:
                    del n_p
            elif code_name == 'iPIC':
                qom_e = run.meta['msQOM'][0]
                Pe = run.get_Press(ind,qom=qom_e)
                pparle,pperpe = ml.proj_tens_on_vec(Pe,B,True)
                del Pe
                anise = np.divide(pperpe,pparle)
                if d_pperpe:
                    del pperpe
                if c_n_p and r_n_p:
                    n_p,_ = run.get_Ion(ind)
            else:
                print('\nWhat code is it?\n')
                anise = None
                if c_n_p and r_n_p:
                    n_p,_ = run.get_Ion(ind)
        else:
            anise = np.divide(pperpe,pparle)
            if c_n_p and r_n_p:
                n_p,_ = run.get_Ion(ind)
    else:
        anise = None
        if c_n_p and r_n_p:
            n_p,_ = run.get_Ion(ind)

    #parallel beta
    B2 = B[0]*B[0] + B[1]*B[1] + B[2]*B[2]
    if d_B:
        del B

    bparlp = 2.*np.divide(pparlp,B2)
    if d_pparlp:
        del pparlp
    if code_name == 'iPIC':
        bparlp = 4.*np.pi*bparlp

    if w_ele:
        bparle = 2.*np.divide(pparle,B2)
        if d_pparle:
            del pparle
        if code_name == 'iPIC':
            bparle = 4.*np.pi*bparle
    else:
        bparle = None

    del B2

    #returning
    out_dict = {'anisp':anisp,'anise':anise,'bparlp':bparlp,'bparle':bparle}
    if r_B     :
        out_dict['B'     ] = B
    if r_pparlp:
        out_dict['pparlp'] = pparlp
    if r_pperpp:
        out_dict['pperpp'] = pperpp
    if r_pparle:
        out_dict['pparle'] = pparle
    if r_pperpe:
        out_dict['pperpe'] = pperpe
    if r_n_p:
        out_dict['n_p'   ] = n_p

    return out_dict

