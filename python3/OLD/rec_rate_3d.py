#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
from glob import glob as g_glob

import numpy as np
from matplotlib import use as mpl_use
#mpl_use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm as mplcol_LogNorm

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
from fibo_beta import *
from iPIC_loader import *
from HVM_loader import *

#------------------------------------------------------
#Intent
#------
print('\nPlots reconnection rate from 3D line integration of Eparl\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------
run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr
# ixmin,ixmax,iymin,iymax = xr

run_name = run.meta['run_name']
out_dir = 'line_integration'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

calc = fibo('calc')
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------

#------------------------------------------------------
# Read from files
#-----------------
#-------
t0 = t_time()
#-------
fpaths = g_glob(opath+'/'+out_dir+'/R_lines_%s_*.dat'%(run_name,))
R_lines_all = []
t_all = []
R_min = []
R_max = []
R_std = []
R_avg = []
h_all = []
b_all = []
n_bins = 40
for fpath in fpaths:
    ind = int(fpath.split('_')[-1].split('.')[0])
    t_all.append( time[ind] )
    with open(fpath, 'r') as f:
        R_lines_all.append( list(map(float,f.readlines())) )
    R_min.append( np.min(R_lines_all[-1]) )
    R_max.append( np.max(R_lines_all[-1]) )
    R_std.append( np.std(R_lines_all[-1]) )
    R_avg.append( np.mean(R_lines_all[-1]) )
    b_all.append( np.linspace(R_min[-1], R_max[-1], n_bins+1) )
    h, _ = np.histogram(R_lines_all[-1], bins=b_all[-1], density=True)
    h_all.append( h )

inds_sorted = np.argsort(t_all)
R_lines_all = [np.array(R_lines_all[i]) for i in inds_sorted]
t_all = np.array([t_all[i] for i in inds_sorted])
R_min = np.array([R_min[i] for i in inds_sorted])
R_max = np.array([R_max[i] for i in inds_sorted])
R_std = np.array([R_std[i] for i in inds_sorted])
R_avg = np.array([R_avg[i] for i in inds_sorted])
b_all = [np.array(b_all[i]) for i in inds_sorted]
h_all = [np.array(h_all[i]) for i in inds_sorted]
#-------
print('Read files: %fs\n'%(t_time() - t0,))
#-------

#------------------------------------------------------
# Plot
#------
if 0:
    plt.close('all')
    plt.plot(t_all, [len(R_lines) for R_lines in R_lines_all])
    plt.xlabel('t')
    plt.ylabel('Num. lines')
    plt.show()

left_dt = t_all.copy()
left_dt[1:] = t_all[1:] - t_all[:-1]
left_dt[0] = 0.
right_dt = t_all.copy()
right_dt[:-1] = t_all[1:] - t_all[:-1]
right_dt[-1] = 0.

plt.close('all')
for i, (t, b, h) in enumerate(zip(t_all, b_all, h_all)):
    T, B = [t-left_dt[i]*.5, t+right_dt[i]*.5], b
    plt.pcolormesh(T, B, h.reshape(1,h.size).T, cmap='PuBuGn', shading='flat', norm=mplcol_LogNorm())
plt.plot(t_all, R_avg, 'k-', label='avg')
plt.plot(t_all, R_avg+R_std, 'k--', label='avg+-std')
plt.plot(t_all, R_avg-R_std, 'k--')
plt.plot(t_all, R_avg+2.*R_std, 'k-.', label='avg+-2std')
plt.plot(t_all, R_avg-2.*R_std, 'k-.')
plt.plot(t_all, R_avg+3.*R_std, 'k:', label='avg+-3std')
plt.plot(t_all, R_avg-3.*R_std, 'k:')
plt.plot(t_all, R_min, 'r-', label='min,max')
plt.plot(t_all, R_max, 'r-')
plt.xlabel('t')
plt.ylabel('R')
plt.legend()
plt.show()
plt.close()
