#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
from glob import glob as g_glob

import numpy as np
from matplotlib import use as mpl_use
from matplotlib import get_backend as mpl_get_backend
mpl_use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm as mplcol_LogNorm

import work_lib as wl

#------------------------------------------------------
#Intent
#------
print('\nPlots reconnection rate from 3D line integration of Eparl\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------
run,tr,xr = wl.Init(sys.argv[1])

run_name = run.meta['run_name']
out_dir = 'line_integration_2'
opath = run.meta['opath']

#flg_abs = False  # -> Eparl line integrals are taken with their sign
flg_abs = True  # -> Eparl line integrals are taken without their sign

#flg_log = False  # -> linear scale on y-axis
flg_log = True  # -> logarithmic scale on y-axis
#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------

#------------------------------------------------------
# Functions
#-----------

#------------------------------------------------------
# Read from files
#-----------------
#-------
t0 = t_time()
#-------
fpaths = g_glob(opath+'/'+out_dir+'/line_*_%s_*.dat'%(run_name,))
inds = []
for fpath in fpaths:
    inds.append(fpath.split('_')[-1].split('.')[0])
inds = sorted(list(map(int, list(set(inds)))))

R_lines_all = []
t_all = []
R_min = []
R_max = []
R_std = []
R_avg = []
h_all = []
b_all = []
n_bins = 40
for ind in inds:
    fpaths = g_glob(opath+'/'+out_dir+'/line_*_%s_%d.dat'%(run_name, ind))
    R_lines_all.append( [] )
    for i, fpath in enumerate(fpaths):
        with open(fpath, 'r') as f:
            t, r = f.readline().split()
        if i == 0: t_all.append( float(t) )
        if flg_abs:
            R_lines_all[-1].append( abs(float(r)) )
        else:
            R_lines_all[-1].append( float(r) )
    R_min.append( np.min(R_lines_all[-1]) )
    R_max.append( np.max(R_lines_all[-1]) )
    R_std.append( np.std(R_lines_all[-1]) )
    R_avg.append( np.mean(R_lines_all[-1]) )
    b_all.append( np.linspace(R_min[-1], R_max[-1], n_bins+1) )
    h, _ = np.histogram(R_lines_all[-1], bins=b_all[-1], density=True)
    h_all.append( h )

R_lines_all = [np.array(R_lines) for R_lines in R_lines_all]
t_all = np.array(t_all)
R_min = np.array(R_min)
R_max = np.array(R_max)
R_std = np.array(R_std)
R_avg = np.array(R_avg)
b_all = [np.array(b) for b in b_all]
h_all = [np.array(h) for h in h_all]
#-------
print('Read files: %fs\n'%(t_time() - t0,))
#-------

#------------------------------------------------------
# Plot
#------
#-------
t0 = t_time()
#-------
left_dt = t_all.copy()
left_dt[1:] = t_all[1:] - t_all[:-1]
left_dt[0] = 0.
right_dt = t_all.copy()
right_dt[:-1] = t_all[1:] - t_all[:-1]
right_dt[-1] = 0.

plt.close('all')
fig = plt.figure(figsize=(12,10))
for i, (t, b, h) in enumerate(zip(t_all, b_all, h_all)):
    T, B = [t-left_dt[i]*.5, t+right_dt[i]*.5], b
    plt.pcolormesh(T, B, h.reshape(1,h.size).T, cmap='PuBuGn', shading='flat', norm=mplcol_LogNorm())
plt.plot(t_all, R_avg, 'k-', label='avg')
plt.plot(t_all, R_avg+R_std, 'k--', label='avg+-std')
plt.plot(t_all, R_avg+2.*R_std, 'k-.', label='avg+-2std')
plt.plot(t_all, R_avg+3.*R_std, 'k:', label='avg+-3std')
plt.plot(t_all, R_max, 'r-', label=('max' if flg_abs else 'min, max'))
if not flg_abs:
    plt.plot(t_all, R_avg-R_std, 'k--')
    plt.plot(t_all, R_avg-2.*R_std, 'k-.')
    plt.plot(t_all, R_avg-3.*R_std, 'k:')
    plt.plot(t_all, R_min, 'r-')
plt.xlabel('t $[\Omega_{\mathrm{p}}^{-1}]$')
plt.ylabel('R $[???]$')
plt.xlim(t_all[0], t_all[-1])
if flg_log:
    plt.ylim(np.min(R_avg[R_avg>0.]), np.max(R_max))
    plt.yscale('log')
else:
    plt.ylim(np.min(R_min), np.max(R_max))
plt.legend()
plt.tight_layout()
if mpl_get_backend() == 'agg':
    fname = '/rec_rate_'
    if flg_abs:
        fname = fname + 'abs_'
    if flg_log:
        fname = fname + 'log_'
    plt.savefig(opath+'/'+out_dir+fname+'%s.png'%(run_name,))
else:
    plt.show()
plt.close()
#-------
print('Plotting: %fs\n'%(t_time() - t0,))
#-------
