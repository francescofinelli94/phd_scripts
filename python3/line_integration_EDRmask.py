#-----------------------------------------------------
#importing stuff
#---------------
import sys
from time import time as t_time
import gc

import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import use as mpl_use
from matplotlib import get_backend as mpl_get_backend
mpl_use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm as mplcol_LogNorm
from scipy.ndimage import binary_dilation
from pandas import DataFrame
from scipy.integrate import ode as si_ode
from scipy.interpolate import RegularGridInterpolator
from scipy.ndimage import map_coordinates as ndm_map_coordinates
from scipy.integrate import simps as si_simpson

import work_lib as wl
sys.path.insert(0,'/work2/finelli/Documents_NEW/my_branch')
import mylib as ml
from mod_calc import *
from mod_from import *

#latex font
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

#------------------------------------------------------
#Intent
#------
print('\nPlots statistics about Eparl\n')

#------------------------------------------------------
#Init
#------
#-------
t0 = t_time()
#-------
#if len(sys.argv) != 5:
#    print('\n4 inputs are required:\n\t- .dat file\n\t- flg_abs\n\t- flg_uns\n\t- flg_log\n')
#    sys.exit()

run,tr,xr = wl.Init(sys.argv[1])

ind1,ind2,ind_step = tr

run_name = run.meta['run_name']
out_dir = 'line_integrated_Eparl_EDR_EDRmask'
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']

ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

n0 = 1.
Bx0 = 0.718722

vA = Bx0/np.sqrt(n0)

#x0, x1, y0, y1 = 27.50, 32.50,  3.50,  4.50  # EDR1
x0, x1, y0, y1 = 27.34, 32.48,  3.79,  4.30  # EDR1
mask_EDR1 = np.zeros((nx, ny, nz), dtype=bool)
ix0 = np.argmin(np.abs(x-x0))
ix1 = np.argmin(np.abs(x-x1))
iy0 = np.argmin(np.abs(y-y0))
iy1 = np.argmin(np.abs(y-y1))
mask_EDR1[ix0:ix1+1, iy0:iy1+1, :] = True

#x0, x1, y0, y1 =  8.00, 13.00, 15.75, 16.75  # EDR2
x0, x1, y0, y1 =  7.35, 12.20, 15.99, 16.68  # EDR2
mask_EDR2 = np.zeros((nx, ny, nz), dtype=bool)
ix0 = np.argmin(np.abs(x-x0))
ix1 = np.argmin(np.abs(x-x1))
iy0 = np.argmin(np.abs(y-y0))
iy1 = np.argmin(np.abs(y-y1))
mask_EDR2[ix0:ix1+1, iy0:iy1+1, :] = True

flg_2pl = True  # -> EDR and BKG plots on different PNGs
#flg_2pl = False  # -> EDR and BKG plots on the same PNG

flg_abs = False  # -> Eparl is taken with its sign
#flg_abs = True  # -> Eparl is taken without its sign
#flg_abs = sys.argv[2].lower() in ['true', '1']

#flg_uns = False  # -> Eparl average is taken with its sign
flg_uns = True  # -> Eparl average is taken without its sign
#flg_uns = sys.argv[3].lower() in ['true', '1']

#flg_log = False  # -> linear scale on y-axis
flg_log = True  # -> logarithmic scale on y-axis
#flg_log = sys.argv[4].lower() in ['true', '1']

print(f'{flg_abs = }\n{flg_uns = }\n{flg_log = }')
#-------
print('Initialization: %fs\n'%(t_time() - t0,))
#-------
#------------------------------------------------------
# Functions
#-----------
def apply_threshold(field, th, dx=dz, dy=dy, dz=dx):
    pos_x, pos_y, pos_z = np.where(field > th)
    pos_x, pos_y, pos_z = ( pos_x.astype(np.float64)*dx,
                            pos_y.astype(np.float64)*dy,
                            pos_z.astype(np.float64)*dz )
    return pos_x, pos_y, pos_z

def generate_stream_seeds(pos_x, pos_y, pos_z, n_seed):
    n_seed = min(pos_x.size, n_seed)
    inds = np.arange(0, pos_x.size, pos_x.size//n_seed, dtype=int)
    return np.array([pos_x[inds], pos_y[inds], pos_z[inds]])

def construct_field_lines( interpolated_function, mask, seeds, dt,
                           max_cnt=10000, xl=xl, yl=yl, zl=zl, dx=dx, dy=dy, dz=dz):
    x_lines = []
    y_lines = []
    z_lines = []
    s_lines = []
    for ns in range(seeds.shape[1]):
        # calculate field lines starting from the seed
        # integrate in both directions
        x_line = [seeds[0, ns]]
        y_line = [seeds[1, ns]]
        z_line = [seeds[2, ns]]
        s_line = [0.]
        for d in [-1, 1]:
            # set the direction
            dt *= -1.
            r = si_ode(f_norm_interpolated)
            r.set_integrator('vode')
            r.set_f_params(xl, yl, zl, interpolated_function)
            # initial position of the magnetic field line
            r.set_initial_value(seeds[:, ns], 0)
            cnt = 0
            while r.successful():
                r.integrate(r.t+dt)
                x_line.append(r.y[0])
                y_line.append(r.y[1])
                z_line.append(r.y[2])
                s_line.append(r.t)
                # check if field line reached a given condition
                if not mask[int(r.y[0]/dx)%nx, int(r.y[1]/dy)%ny, int(r.y[2]/dz)%nz]: break
                # -> check if too many integration steps were done
                cnt += 1
                if cnt > max_cnt:
                    print( '\nWARNING: mx_cnt=%d '%(max_cnt,) +
                           'rached in directoin %d '%(d,) +
                           'from seed (%f, %f, %f)!\n'%(seeds[0, ns], seeds[1, ns], seeds[2, ns]) )
                    break
            x_line = x_line[::d]
            y_line = y_line[::d]
            z_line = z_line[::d]
            s_line = s_line[::d]
        x_lines.append(np.array(x_line))
        y_lines.append(np.array(y_line))
        z_lines.append(np.array(z_line))
        s_lines.append(np.array(s_line))
    return x_lines, y_lines, z_lines, s_lines

def f_norm_interpolated(t, p, xl, yl, zl, interpolated_function):
    x, y, z = p[0]%xl, p[1]%yl, p[2]%zl
    return interpolated_function([x, y, z])[0]


#---> loop over times <---
t_all = []
Eparl_EDR_min = []
Eparl_EDR_max = []
Eparl_EDR_std = []
Eparl_EDR_avg = []
Eparl_EDR_h_all = []
Eparl_EDR_b_all = []
Eparl_BKG_min = []
Eparl_BKG_max = []
Eparl_BKG_std = []
Eparl_BKG_avg = []
Eparl_BKG_h_all = []
Eparl_BKG_b_all = []
n_bins = 50

print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
#-------------------------
    #------------------------------------------------------
    # Get data
    #----------
    #-------
    t0 = t_time()
    #-------
    # -> magnetic fields and co.
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm

    del Bnorm

    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]

    #if code_name == 'HVM':
    #    cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
    #    J = np.array([cx, cy, cz]) # J = rot(B)
    #    del cx, cy, cz

    # -> densities and currents
    #if code_name == 'HVM':
    #    n_p, u_p = run.get_Ion(ind)
    #    u_e = np.empty((3, nx, ny, nz), dtype=np.float64) # u_e = u_p - J/n
    #    u_e[0] = u_p[0] - np.divide(J[0], n_p)
    #    u_e[1] = u_p[1] - np.divide(J[1], n_p)
    #    u_e[2] = u_p[2] - np.divide(J[2], n_p)
    #    del n_p, J, u_p
    #elif code_name == 'iPIC':
    #    _, u_e = run.get_Ion(ind,qom=qom_e)

    # -> demag
    #e_demag = np.empty(E.shape, dtype=np.float64)
    #e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
    #e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
    #e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
    #e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)
    #del e_demag, B, E, u_e
    #-------
    print('\nData loading and computing: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # apply threshold and generate seeds
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    n_seeds = 512
    pos_x, pos_y, pos_z = apply_threshold(mask_EDR1, .5)
    seeds = generate_stream_seeds(pos_x, pos_y, pos_z, n_seeds)

    #------------------------------------------------------
    # generate field lines
    #----------------------
    dt = min(dx, dy, dz)*.5
    interpolated_function = RegularGridInterpolator( (x, y, z), b, method='linear',
                                                     bounds_error=False, fill_value=None )
    x_lines, y_lines, z_lines, s_lines = construct_field_lines(interpolated_function, mask_EDR1, 
                                                               seeds, dt)
    #-------
    print('Generating field lines: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # interpolte parallel electric field
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    Eparl_lines = []
    for i in range(seeds.shape[1]):
        coords_x = (x_lines[i]/dx) % nx
        coords_y = (y_lines[i]/dy) % ny
        coords_z = (z_lines[i]/dz) % nz
        Eparl_lines.append( ndm_map_coordinates(
                            Eparl, np.vstack((coords_x, coords_y, coords_z)),
                            mode='grid-wrap', prefilter=False
                            ) )
    #-------
    print('Initerpolating Eparl: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # integrating Eparl
    #------------------------------------
    #-------
    t0 = t_time()
    #-------
    R_lines = []
    for i in range(seeds.shape[1]):
        #R_lines.append( si_simpson(Eparl_lines[i], x=s_lines[i]) )
        R_lines.append( np.mean(Eparl_lines[i])/(Bx0*vA) )
    #-------
    print('Integrating Eparl: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # write on file
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    if 1:
        for i in range(seeds.shape[1]):
            with open(opath+'/'+out_dir+'/line_%d_%s_%d.dat'%(i, run_name, ind), 'w') as f:
                f.write('%f\t%f\n'%(time[ind], R_lines[i]))
                for j in range(s_lines[i].size):
                    f.write('%f\t%f\t%f\t%f\t%f\n'%( x_lines[i][j], y_lines[i][j],
                                                         z_lines[i][j],
                                                         s_lines[i][j], Eparl_lines[i][j]) )
    #-------
    print('Writing on file: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # plot Eparl and e demag interpolated
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    if 1:
        plt.close('all')
        fig, ax0 = plt.subplots(1, 1, sharex=True)
        for i in range(seeds.shape[1]):
            ax0.plot(s_lines[i], Eparl_lines[i])
        ax0.set_ylabel('Eparl')
        plt.tight_layout()
        if mpl_get_backend() == 'agg':
            plt.savefig(opath+'/'+out_dir+'/Eparl_lines_%s_%d.png'%(run_name, ind))
        else:
            plt.show()
        plt.close()

    #------------------------------------------------------
    # plot in 3d box
    #-----------------------------
    if 1:
        plt.close('all')
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        n_points = 200
        n_points = min(pos_x.size, n_points)
        every = pos_x.size//n_points
        if 0:
            ax.scatter(pos_x[::every], pos_y[::every], pos_z[::every], c='r')

        if 0:
            ax.scatter(seeds[0], seeds[1], seeds[2], c='g')

        for i in range(seeds.shape[1]):
            ax.plot(x_lines[i], y_lines[i], z_lines[i], 'o', ms=.5)

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_xlim3d(x[0], x[-1])
        ax.set_ylim3d(y[0], y[-1])
        ax.set_zlim3d(z[0], z[-1])
        if mpl_get_backend() == 'agg':
            plt.savefig(opath+'/'+out_dir+'/B_lines_plot_3d_%s_%d.png'%(run_name, ind))
        else:
            plt.show()
        plt.close()
    #-------
    print('Plotting: %fs'%(t_time() - t0,))
    #-------

    #------------------------------------------------------
    # generatying histogram
    #-------------------------
    #-------
    t0 = t_time()
    #-------
    #############################################
    ### WARNING: BOTH Eparl_EDR AND Eparl_BKG ###
    ### ARE NOW R_lines #########################
    #############################################
    Eparl_EDR = R_lines
    Eparl_BKG = R_lines

    t_all.append(time[ind])
    Eparl_EDR_min.append(np.min(Eparl_EDR))
    Eparl_EDR_max.append(np.max(Eparl_EDR))
    Eparl_EDR_std.append(np.std(Eparl_EDR))
    Eparl_EDR_avg.append(np.mean(Eparl_EDR))
    Eparl_EDR_b_all.append(np.linspace(Eparl_EDR_min[-1], Eparl_EDR_max[-1], n_bins+1))
    h, _ = np.histogram(Eparl_EDR, bins=Eparl_EDR_b_all[-1], density=True)
    Eparl_EDR_h_all.append(h)
    Eparl_BKG_min.append(np.min(Eparl_BKG))
    Eparl_BKG_max.append(np.max(Eparl_BKG))
    Eparl_BKG_std.append(np.std(Eparl_BKG))
    Eparl_BKG_avg.append(np.mean(Eparl_BKG))
    Eparl_BKG_b_all.append(np.linspace(Eparl_BKG_min[-1], Eparl_BKG_max[-1], n_bins+1))
    h, _ = np.histogram(Eparl_BKG, bins=Eparl_BKG_b_all[-1], density=True)
    Eparl_BKG_h_all.append(h)
    #-------
    print('Generating histogram: %fs'%(t_time() - t0,))
    #-------

#---> loop over time <---
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")

t_all = np.array(t_all)
Eparl_EDR_min = np.array(Eparl_EDR_min)
Eparl_EDR_max = np.array(Eparl_EDR_max)
Eparl_EDR_std = np.array(Eparl_EDR_std)
Eparl_EDR_avg = np.array(Eparl_EDR_avg)
Eparl_BKG_min = np.array(Eparl_BKG_min)
Eparl_BKG_max = np.array(Eparl_BKG_max)
Eparl_BKG_std = np.array(Eparl_BKG_std)
Eparl_BKG_avg = np.array(Eparl_BKG_avg)
#------------------------

#------------------------------------------------------
# plotting
#-------------------------
#-------
t0 = t_time()
#-------
# -> initi time bins
left_dt = t_all.copy()
left_dt[1:] = t_all[1:] - t_all[:-1]
left_dt[0] = 0.
right_dt = t_all.copy()
right_dt[:-1] = t_all[1:] - t_all[:-1]
right_dt[-1] = 0.

# -> init figure(s)
plt.close('all')
if flg_2pl:
    fig0, ax0 = plt.subplots(1, 1, figsize=(16, 10))
    fig1, ax1 = plt.subplots(1, 1, figsize=(16, 10))
else:
    fig1, (ax0, ax1) = plt.subplots(2, 1, figsize=(16, 10))

# -> hist plot  (EDR and BKG)
for i, (t, EDR_b, EDR_h, EDR_a, BKG_b, BKG_h, BKG_a) in enumerate(zip(
            t_all, Eparl_EDR_b_all, Eparl_EDR_h_all, Eparl_EDR_avg,
            Eparl_BKG_b_all, Eparl_BKG_h_all, Eparl_BKG_avg )):
    if flg_uns:
        cmap_EDR = 'PuBuGn' if (EDR_a >= 0.) else 'YlOrRd'
        cmap_BKG = 'PuBuGn' if (BKG_a >= 0.) else 'YlOrRd'
        d_EDR = 1 if (EDR_a >= 0.) else -1
        d_BKG = 1 if (BKG_a >= 0.) else -1
    else:
        cmap_EDR = 'PuBuGn'
        cmap_BKG = 'PuBuGn'
        d_EDR = 1
        d_BKG = 1
    T, B = [t-left_dt[i]*.5, t+right_dt[i]*.5], d_EDR*EDR_b[::d_EDR]
    ax0.pcolormesh( T, B, EDR_h[::d_EDR].reshape(1,EDR_h.size).T,
                    cmap=cmap_EDR, shading='flat', norm=mplcol_LogNorm() )
    B = d_BKG*BKG_b[::d_BKG]
    ax1.pcolormesh( T, B, BKG_h[::d_BKG].reshape(1,BKG_h.size).T,
                    cmap=cmap_BKG, shading='flat', norm=mplcol_LogNorm() )

# -> deal with avg sign
if flg_uns:
    Eparl_EDR_max[Eparl_EDR_avg<0.], Eparl_EDR_min[Eparl_EDR_avg<0.] = (
            -Eparl_EDR_min[Eparl_EDR_avg<0.], -Eparl_EDR_max[Eparl_EDR_avg<0.] )
    Eparl_BKG_max[Eparl_BKG_avg<0.], Eparl_BKG_min[Eparl_BKG_avg<0.] = (
            -Eparl_BKG_min[Eparl_BKG_avg<0.], -Eparl_BKG_max[Eparl_BKG_avg<0.] )
    Eparl_EDR_avg = np.abs(Eparl_EDR_avg)
    Eparl_BKG_avg = np.abs(Eparl_BKG_avg)

# -> stat curves plot for EDR
ax0.plot(t_all, Eparl_EDR_avg, 'k-', label=('avg' if flg_uns else 'avg'))
ax0.plot(t_all, Eparl_EDR_avg+Eparl_EDR_std, 'k--', label='avg%sstd'%('+' if flg_log else '+-',))
ax0.plot(t_all, Eparl_EDR_avg+2.*Eparl_EDR_std, 'k-.', label='avg%s2std'%('+' if flg_log else '+-',))
ax0.plot(t_all, Eparl_EDR_avg+3.*Eparl_EDR_std, 'k:', label='avg%s3std'%('+' if flg_log else '+-',))
ax0.plot(t_all, Eparl_EDR_max, 'r-', label=('max' if flg_log else 'min, max'))
if not flg_log:
    ax0.plot(t_all, Eparl_EDR_avg-Eparl_EDR_std, 'k--')
    ax0.plot(t_all, Eparl_EDR_avg-2.*Eparl_EDR_std, 'k-.')
    ax0.plot(t_all, Eparl_EDR_avg-3.*Eparl_EDR_std, 'k:')
    ax0.plot(t_all, Eparl_EDR_min, 'r-')

# -> EDR labels, lims, and legend
ax0.set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
tmp = '\langle E_{\parallel} \\rangle_{B-line}'
if flg_abs:
    tmp = '|%s|'%(tmp,)
tmp = '$%s/B_0/v_A$'%(tmp,)
if flg_uns:
    tmp = '%s (unsigned average)'%(tmp,)
ax0.set_ylabel('%s - EDR1'%(tmp,))
ax0.set_xlim(t_all[0], t_all[-1])
if flg_log:
    ax0.set_ylim(max(np.min(Eparl_EDR_avg[Eparl_EDR_avg>0.]), 1.e-5), np.max(Eparl_EDR_max))
    ax0.set_yscale('log')
else:
    ax0.set_ylim(np.min(Eparl_EDR_min), np.max(Eparl_EDR_max))
ax0.legend()

# -> save/show EDR fig (for separate figs)
if flg_2pl:
    fig0.tight_layout()
    if mpl_get_backend() == 'agg':
        fname = '/line_integrated_Eparl_EDR1_adim_'
        if flg_abs:
            fname = fname + 'abs_'
        if flg_uns:
            fname = fname + 'uns_'
        if flg_log:
            fname = fname + 'log_'
        fig0.savefig(opath+'/'+out_dir+fname+'%s_%d_%d.png'%(run_name, ind1, ind2))
    else:
        fig0.show()
    plt.close(fig0)

# -> stat curves plot for BKG
ax1.plot(t_all, Eparl_BKG_avg, 'k-', label=('avg' if flg_uns else 'avg'))
ax1.plot(t_all, Eparl_BKG_avg+Eparl_BKG_std, 'k--', label='avg%sstd'%('+' if flg_log else '+-',))
ax1.plot(t_all, Eparl_BKG_avg+2.*Eparl_BKG_std, 'k-.', label='avg%s2std'%('+' if flg_log else '+-',))
ax1.plot(t_all, Eparl_BKG_avg+3.*Eparl_BKG_std, 'k:', label='avg%s3std'%('+' if flg_log else '+-',))
ax1.plot(t_all, Eparl_BKG_max, 'r-', label=('max' if flg_log else 'min, max'))
if not flg_log:
    ax1.plot(t_all, Eparl_BKG_avg-Eparl_BKG_std, 'k--')
    ax1.plot(t_all, Eparl_BKG_avg-2.*Eparl_BKG_std, 'k-.')
    ax1.plot(t_all, Eparl_BKG_avg-3.*Eparl_BKG_std, 'k:')
    ax1.plot(t_all, Eparl_BKG_min, 'r-')

# -> BKG labels, lims, and legend
ax1.set_xlabel('$t\;[\Omega_{\mathrm{p}}^{-1}]$')
tmp = '\langle E_{\parallel} \\rangle_{B-line}'
if flg_abs:
    tmp = '|%s|'%(tmp,)
tmp = '$%s/B_0/v_A$'%(tmp,)
if flg_uns:
    tmp = '%s (unsigned average)'%(tmp,)
ax1.set_ylabel('%s - EDR2'%(tmp,))
ax1.set_xlim(t_all[0], t_all[-1])
if flg_log:
    ax1.set_ylim(max(np.min(Eparl_BKG_avg[Eparl_BKG_avg>0.]), 1.e-5), np.max(Eparl_BKG_max))
    ax1.set_yscale('log')
else:
    ax1.set_ylim(np.min(Eparl_BKG_min), np.max(Eparl_BKG_max))
ax1.legend()

# -> save/show BKG fig (or both)
fig1.tight_layout()
if mpl_get_backend() == 'agg':
    if flg_2pl:
        fname = '/line_integrated_Eparl_EDR2_adim_'
    else:
        fname = '/line_integrated_Eparl_EDRvsBKG_adim_'
    if flg_abs:
        fname = fname + 'abs_'
    if flg_uns:
        fname = fname + 'uns_'
    if flg_log:
        fname = fname + 'log_'
    fig1.savefig(opath+'/'+out_dir+fname+'%s_%d_%d.png'%(run_name, ind1, ind2))
else:
    fig1.show()
plt.close(fig1)
#-------
print('Plotting: %fs'%(t_time() - t0,))
#-------

#-------
# Saving DataFrame
#-------
df = DataFrame({'t':t_all,
                'avg1':Eparl_EDR_avg,
                'std1':Eparl_EDR_std,
                'max1':Eparl_EDR_max,
                'min1':Eparl_EDR_min,
                'avg2':Eparl_BKG_avg,
                'std2':Eparl_BKG_std,
                'max2':Eparl_BKG_max,
                'min2':Eparl_BKG_min})
df.to_csv(opath+'/'+out_dir+fname+'line_integrated_Eparl_EDR1_EDR2_%s_%d_%d.csv'%(run_name, ind1, ind2))
#-------
