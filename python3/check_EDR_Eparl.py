"""
This scripts plots the top view of the region selected as EDR
and also the z-averaged Eparl.
"""

# imports
import sys
import gc
from os.path import join

import numpy as np
from matplotlib import use as mpl_use
#mpl_use('Agg')
import matplotlib.pyplot as plt
from scipy.ndimage import binary_dilation

import work_lib as wl
import mylib as ml
from mod_calc import *
from mod_from import *

#latex font
font = 28
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"
mpl.rc('font', family = 'serif', size = font)

# init
run,tr,xr = wl.Init(sys.argv[1])
ind1,ind2,ind_step = tr
run_name = run.meta['run_name']
opath = run.meta['opath']
code_name = run.meta['code_name']
w_ele = run.meta['w_ele']
nx, ny, nz = run.meta['nnn']
dx, dy, dz = run.meta['ddd']
xl, yl, zl = run.meta['lll']
x = run.meta['x']
y = run.meta['y']
z = run.meta['z']
time = run.meta['time']*run.meta['tmult']
if code_name == 'iPIC':
    qom_e = run.meta['msQOM'][0]

out_dir = 'check_EDR_Eparl'
ml.create_path(opath+'/'+out_dir)

calc = fibo_calc()
calc.meta=run.meta

n0 = 1.
Bx0 = 0.718722
vA = Bx0/np.sqrt(n0)

# time loop
print(" ", end=" ")
for ind in np.arange(ind1,ind2+ind_step,ind_step,dtype=int):
    # -> Eparl
    E, B = run.get_EB(ind)

    b = np.empty((nx, ny, nz, 3), dtype=np.float64)
    Bnorm = np.sqrt(B[0]**2 + B[1]**2 + B[2]**2)
    for i in range(3):
        b[..., i] = B[i]/Bnorm
    del Bnorm
    Eparl = E[0]*b[..., 0] + E[1]*b[..., 1] + E[2]*b[..., 2]
    del b

    # -> densities and currents
    if code_name == 'HVM':
        cx,cy,cz = calc.calc_curl(B[0], B[1], B[2])
        J = np.array([cx, cy, cz]) # J = rot(B)
        del cx, cy, cz

    if code_name == 'HVM':
        n_p, u_p = run.get_Ion(ind)
        u_e = np.empty((3, nx, ny, nz), dtype=np.float64) # u_e = u_p - J/n
        u_e[0] = u_p[0] - np.divide(J[0], n_p)
        u_e[1] = u_p[1] - np.divide(J[1], n_p)
        u_e[2] = u_p[2] - np.divide(J[2], n_p)
        del n_p, J, u_p
    elif code_name == 'iPIC':
        _, u_e = run.get_Ion(ind,qom=qom_e)

    # -> demag
    e_demag = np.empty(E.shape, dtype=np.float64)
    e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
    e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
    e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
    e_demag_n = np.sqrt(e_demag[0]**2 + e_demag[1]**2 + e_demag[2]**2)
    del e_demag, B, E, u_e

    # -> mask
    th = np.max(e_demag_n)*.75
    mask = (e_demag_n>th)
    del e_demag_n

    # -> z-avg/sum
    Eparl_ = np.mean(Eparl, axis=-1)/(Bx0*vA)
    mask_ = np.sum(mask, axis=-1)
    del Eparl, mask

    # -> plot
    plt.close('all')
    fig, ax = plt.subplots(2, 1, figsize=(16,12), sharex=True, sharey=True)
    
    vminmask = 0.
    vmaxmask = 1.
    vmin = np.min(Eparl_)
    vmax = np.max(Eparl_)
    vminEparl = -max(vmax,-vmin)
    vmaxEparl = max(vmax,-vmin)

    im0 = ax[0].pcolormesh(x, y, mask_.T>0., shading='gouraud',
                           vmin=vminmask, vmax=vmaxmask, cmap='pink_r')
    ax[0].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[0].set_title('mask $z$-view')
    plt.colorbar(im0, ax=ax[0])

    im1 = ax[1].pcolormesh(x, y, Eparl_.T, shading='gouraud',
                           vmin=vminEparl, vmax=vmaxEparl, cmap='seismic')
    ax[1].set_xlabel('$x\quad [d_{\mathrm{p}}]$')
    ax[1].set_ylabel('$y\quad [d_{\mathrm{p}}]$')
    ax[1].set_title('$E_{\parallel}/B_0/v_A$ $z$-avg')
    plt.colorbar(im1, ax=ax[1])

    fig.suptitle('$t=%.2f\;[\Omega_{\mathrm{p}}^{-1}]$'%(time[ind],))
    
    fig_name = "mask_Eparl_%s_ind%d.png"%(run_name, ind)
    #fig.savefig(join(opath, out_dir, fig_name))
    plt.show()
    plt.close()

# end time loop
    print("\r", end=" ")
    print("t = ", time[ind], end=" ")
    gc.collect()
    sys.stdout.flush()
print("\n")
