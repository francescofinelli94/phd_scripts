"""
Testing pyvista library: plotting streamlines
"""

# imports
from sys import argv as sys_argv
from sys import exit as sys_exit
from time import time as ttime

import numpy as np
import pyvista as pv
from h5py import File as h5_file

# parameters
if len(sys_argv) < 4:
    print('ERROR: not enough arguments.')
    print('Arguments:\nFNAME\t\twhole path to data file\nplot_mode\tshow or save or both\nth_percentage\tthreshold on Jz as percentage of max(Jz)')
    sys_exit()
FNAME = sys_argv[1]  # 'EB_Ion_J_LF_3d_DH_18.h5'
plot_mode = sys_argv[2]  # 'show', 'save', 'both'
th_perc = float(sys_argv[3])  # 0. -> 1.

# load data
tic = ttime()

with h5_file(FNAME, 'r') as h5file:
    nx = h5file.attrs['nx']
    ny = h5file.attrs['ny']
    nz = h5file.attrs['nz']
    dx = h5file.attrs['dx']
    dy = h5file.attrs['dy']
    dz = h5file.attrs['dz']
    this_time = h5file.attrs['this_time']

    J2 = h5file['J'][0]**2 + h5file['J'][1]**2 + h5file['J'][2]**2
    #u_e = h5file['up'] - np.divide(h5file['J'], h5file['np'])
    #E = h5file['E'][:]
    #B = h5file['B'][:]

#e_demag = np.empty(E.shape, dtype=np.float64)
#e_demag[0] = E[0] + u_e[1]*B[2] - u_e[2]*B[1]
#e_demag[1] = E[1] + u_e[2]*B[0] - u_e[0]*B[2]
#e_demag[2] = E[2] + u_e[0]*B[1] - u_e[1]*B[0]
#demag = np.sqrt(0.*e_demag[0]**2 + 0.*e_demag[1]**2 + e_demag[2]**2)
#del e_demag, B, E, u_e

toc = ttime()
print(f'Data loading:\t\t{toc - tic}s')
first_tic = tic

# generate uniform grid mesh
tic = ttime()

mesh = pv.UniformGrid()
mesh.dimensions = [nx + 1, ny + 1, nz + 1]
mesh.spacing = [dx, dy, dz]
x = mesh.points[:, 0]
y = mesh.points[:, 1]
z = mesh.points[:, 2]

toc = ttime()
print(f'Mesh generation:\t{toc - tic}s')

# add data to mesh
tic = ttime()

mesh.cell_data["J2"] = J2.flatten(order="F")
#mesh.cell_data["demag"] = demag.flatten(order="F")
J2_threshold = np.max(J2)*th_perc
J2_threshed = mesh.threshold(value=J2_threshold, scalars="J2")
#mask = np.empty(J2.shape, dtype=bool)
#for iz in range(nz):
#    J2_threshold = np.max(J2[:, :ny//2, iz])*th_perc
#    mask[:, :ny//2, iz] = (J2[:, :ny//2, iz] > J2_threshold)
#mesh.cell_data["mask"] = mask.flatten(order="F")
#mask_threshed = mesh.threshold(value=.5, scalars="mask")
#demag_threshold = np.max(demag)*th_perc
#demag_threshed = mesh.threshold(value=demag_threshold, scalars="demag")

bounds = [(nx//3)*dx, nx*dx,
          0., ny*dy,
          (nz//2)*dz, nz*dz]
clipped = mesh.clip_box(bounds)

toc = ttime()
print(f'Adding data:\t\t{toc - tic}s')

# plot
tic = ttime()

vmin = np.min(J2)
vmax = np.max(J2)
#vmax = max(abs(vmax), abs(vmin))
#vmin = - vmax

pv.set_plot_theme("document")

if plot_mode == 'save':
    plotter = pv.Plotter(off_screen=True)
elif plot_mode in ['show', 'both']:
    plotter = pv.Plotter()
else:
    print(f'ERROR: invalid {plot_mode = }')
    sys_exit()

#_ = plotter.add_mesh(demag_threshed, opacity=1., scalars="demag",
#                     cmap='Blues', clim=(np.min(demag),np.max(demag)))
_ = plotter.add_mesh(J2_threshed, opacity=1., show_scalar_bar=False,
                     scalars="J2", cmap='reds', clim=(vmin,vmax))
_ = plotter.add_mesh(clipped, scalars="J2", cmap='reds', opacity=1.,
                     scalar_bar_args={'height':.5, 'vertical':True,
                                      'position_x':0.05, 'position_y':0.05,
                                      'title':'J2'},
                                      clim=(vmin,vmax))
_ = plotter.show_bounds(grid='front', location='outer', all_edges=True)
_ = plotter.add_title(f't = {this_time}')

if plot_mode == 'show':
    plotter.show()
elif plot_mode == 'both':
    plotter.show(auto_close=False)
if plot_mode in ['save', 'both']:
    plotter.screenshot(f"J2_{FNAME.split('.')[0].split('EB_Ion_J_')[-1]}.png")

toc = ttime()
print(f'Plotting:\t\t{toc - tic}s')
last_toc = toc
print(f'Total time:\t\t{last_toc - first_tic}s')
